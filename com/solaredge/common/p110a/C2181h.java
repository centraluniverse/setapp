package com.solaredge.common.p110a;

import android.util.Log;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/* compiled from: RetryableCallback */
public class C2181h<T> implements Callback<T> {
    private static final String f5489b = "h";
    private int f5490a = 1;
    private final Call<T> f5491c;
    private int f5492d = 0;

    public void mo2503a(Call<T> call, Throwable th) {
    }

    public void mo2504a(Call<T> call, Response<T> response) {
    }

    public C2181h(Call<T> call, int i) {
        this.f5491c = call;
        this.f5490a = i;
    }

    public void onResponse(Call<T> call, Response<T> response) {
        mo2504a((Call) call, (Response) response);
    }

    public void onFailure(Call<T> call, Throwable th) {
        Log.e(f5489b, th.getMessage() != null ? th.getMessage() : "null message");
        if (call.isCanceled() || this.f5492d >= this.f5490a) {
            mo2503a((Call) call, th);
            return;
        }
        this.f5492d++;
        th = f5489b;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Retrying API Call -  (");
        stringBuilder.append(this.f5492d);
        stringBuilder.append(" / ");
        stringBuilder.append(this.f5490a);
        stringBuilder.append(")  -> ");
        stringBuilder.append(call.request().m751a());
        Log.v(th, stringBuilder.toString());
        m6621a();
    }

    private void m6621a() {
        this.f5491c.clone().enqueue(this);
    }
}
