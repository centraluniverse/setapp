package com.solaredge.common.p110a;

import com.google.p040a.C0671f;
import java.io.StringWriter;
import java.io.Writer;
import org.json.JSONException;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import p008b.p009a.p010a.p011a.p012a.p014b.C0356a;

/* compiled from: ResponseObject */
public class C1364g {
    private C1363a f3356a = C1363a.JSON;
    private String f3357b;
    private Object f3358c;
    private int f3359d;

    /* compiled from: ResponseObject */
    public enum C1363a {
        XML,
        JSON
    }

    public String m3735a() {
        return this.f3357b;
    }

    public int m3736b() {
        return this.f3359d;
    }

    public String m3737c() {
        return this.f3356a == C1363a.JSON ? C0356a.ACCEPT_JSON_VALUE : "application/xml";
    }

    public String m3738d() throws JSONException {
        if (this.f3357b != null && !this.f3357b.isEmpty()) {
            return m3735a();
        }
        if (this.f3358c == null) {
            return null;
        }
        String a;
        if (this.f3356a == C1363a.JSON) {
            a = new C0671f().m1190a(this.f3358c);
        } else {
            Serializer persister = new Persister();
            Writer stringWriter = new StringWriter();
            try {
                persister.write(this.f3358c, stringWriter);
                a = stringWriter.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return a;
    }
}
