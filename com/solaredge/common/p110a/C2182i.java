package com.solaredge.common.p110a;

import com.solaredge.common.C1366a;
import com.solaredge.common.p114e.C1381b;
import java.util.List;
import p021c.C0545l;
import p021c.C0546m;
import p021c.C0557t;

/* compiled from: SECookieJar */
public class C2182i implements C0546m {
    public void mo1332a(C0557t c0557t, List<C0545l> list) {
        C1381b.m3777a().m3779a(C1366a.m3749a().m3753b(), list);
    }

    public List<C0545l> mo1331a(C0557t c0557t) {
        return C1381b.m3777a().m3778a(C1366a.m3749a().m3753b());
    }
}
