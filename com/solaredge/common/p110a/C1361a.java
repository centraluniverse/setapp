package com.solaredge.common.p110a;

import com.solaredge.common.models.User;
import com.solaredge.common.models.response.LocalesListResponse;
import com.solaredge.common.models.response.TranslationResponse;
import com.solaredge.common.models.response.UserResponse;
import p021c.ab;
import p021c.ad;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/* compiled from: API */
public class C1361a {

    /* compiled from: API */
    public interface C1345a {
    }

    /* compiled from: API */
    public interface C1346b {
        @GET("logout")
        Call<Void> m3720a();

        @GET("mapperdemologin")
        Call<UserResponse> m3721a(@Query("locale") String str);

        @FormUrlEncoded
        @POST("login")
        Call<UserResponse> m3722a(@Field("j_username") String str, @Field("j_password") String str2);

        @GET("demologin")
        Call<UserResponse> m3723b(@Query("locale") String str);
    }

    /* compiled from: API */
    public interface C1347c {
    }

    /* compiled from: API */
    public interface C1348d {
    }

    /* compiled from: API */
    public interface C1349e {
    }

    /* compiled from: API */
    public interface C1350f {
        @GET("i18n/localelist")
        Call<LocalesListResponse> m3724a();

        @GET("i18n/fetchtranslations/{localeCode}/API")
        Call<TranslationResponse> m3725a(@Path("localeCode") String str);
    }

    /* compiled from: API */
    public interface C1351g {
    }

    /* compiled from: API */
    public interface C1352h {
        @GET("/mobile/commissioning/v1/status")
        @Headers({"Content-Type: application/x-protobuf"})
        Call<ad> m3726a();

        @POST("/mobile/commissioning/v1/prepare")
        @Headers({"Content-Type: application/x-protobuf"})
        Call<ad> m3727a(@Body ab abVar);

        @POST("/mobile/commissioning/v1/file/{fileID}")
        @Headers({"Content-Type: application/x-protobuf"})
        Call<ad> m3728a(@Path("fileID") Integer num, @Body ab abVar);

        @GET("/mobile/commissioning/v1/identity")
        @Headers({"Content-Type: application/x-protobuf"})
        Call<ad> m3729b();

        @POST("/mobile/commissioning/v1/execute")
        @Headers({"Content-Type: application/x-protobuf"})
        Call<ad> m3730b(@Body ab abVar);
    }

    /* compiled from: API */
    public interface C1353i {
    }

    /* compiled from: API */
    public interface C1354j {
    }

    /* compiled from: API */
    public interface C1355k {
    }

    /* compiled from: API */
    public interface C1356l {
    }

    /* compiled from: API */
    public interface C1357m {
    }

    /* compiled from: API */
    public interface C1358n {
    }

    /* compiled from: API */
    public interface C1359o {
        @GET("user/details")
        Call<User> m3731a();

        @PUT("user/updatelocale")
        Call<Void> m3732a(@Query("locale") String str, @Body String str2);
    }

    /* compiled from: API */
    public interface C1360p {
    }
}
