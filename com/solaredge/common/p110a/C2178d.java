package com.solaredge.common.p110a;

import com.google.android.gms.analytics.C0697c.C1971a;
import com.google.android.gms.analytics.C2372e;
import com.solaredge.common.C1366a;
import com.solaredge.common.C1368b;
import com.solaredge.common.p114e.C1381b;
import com.solaredge.common.p114e.C1396h;
import com.solaredge.common.p116g.C1402e;
import java.io.IOException;
import p021c.C0554s.C0553a;
import p021c.C0559u;
import p021c.C0559u.C0558a;
import p021c.ac;

/* compiled from: ErrorInterceptor */
public class C2178d implements C0559u {
    public ac mo1270a(C0558a c0558a) throws IOException {
        c0558a = c0558a.mo1276a(c0558a.mo1275a());
        if (!(c0558a.m778b() == 200 || c0558a.m778b() == 302)) {
            C1402e.m3834a().m3835b();
        }
        if (c0558a.m778b() != 401 || c0558a.m775a().m751a().toString().contains("/api/logout") || c0558a.m775a().m751a().toString().contains("/api/login") || c0558a.m775a().m751a().toString().contains("/api/mapperdemologin") || C1381b.m3777a().m3778a(C1366a.m3749a().m3753b()).size() <= 0) {
            return c0558a;
        }
        C2372e b = C1396h.m3827a().m3828b();
        C1971a a = new C1971a().m5569a("Error");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("API Request denied - ");
        stringBuilder.append(c0558a.m775a().m751a());
        b.m7058a(((C1971a) a.m5570b(stringBuilder.toString()).m1235a("reason - ", c0558a.m780d())).m1236a());
        C1368b.m3756a().m3762c().mo1773a();
        return c0558a.m784h().m767a(new C0553a().m914a()).m771a();
    }
}
