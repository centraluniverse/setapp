package com.solaredge.common.p110a;

import com.solaredge.common.C1366a;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import p021c.C0559u;
import p021c.C0559u.C0558a;
import p021c.C0560v;
import p021c.C0564y;
import p021c.ac;
import p021c.ac.C0523a;
import p021c.ad;

/* compiled from: FakeInterceptor */
public class C2179e implements C0559u {
    private static Map<String, C1364g> f5488a = new HashMap();

    public ac mo1270a(C0558a c0558a) throws IOException {
        if (!C1366a.m3750c()) {
            return c0558a.mo1276a(c0558a.mo1275a());
        }
        C1364g c1364g;
        String c0557t = c0558a.mo1275a().m751a().toString();
        for (String str : f5488a.keySet()) {
            if (c0557t.matches(str)) {
                c1364g = (C1364g) f5488a.get(str);
                break;
            }
        }
        c1364g = null;
        if (c1364g == null) {
            return c0558a.mo1276a(c0558a.mo1275a());
        }
        String d;
        try {
            d = c1364g.m3738d();
        } catch (Exception e) {
            e.printStackTrace();
            d = null;
        }
        if (d != null) {
            return new C0523a().m761a(c1364g.m3736b()).m763a(c0558a.mo1275a()).m768a(C0564y.HTTP_1_0).m765a(ad.create(C0560v.m986a(c1364g.m3737c()), d.getBytes())).m770a("content-type", c1364g.m3737c()).m771a();
        }
        return c0558a.mo1276a(c0558a.mo1275a());
    }
}
