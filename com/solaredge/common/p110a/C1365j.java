package com.solaredge.common.p110a;

import com.google.p040a.C0671f;
import com.google.p040a.C0672g;
import com.p134a.p135a.C1886a;
import com.solaredge.common.C1366a;
import com.solaredge.common.p110a.C1361a.C1345a;
import com.solaredge.common.p110a.C1361a.C1346b;
import com.solaredge.common.p110a.C1361a.C1347c;
import com.solaredge.common.p110a.C1361a.C1348d;
import com.solaredge.common.p110a.C1361a.C1349e;
import com.solaredge.common.p110a.C1361a.C1350f;
import com.solaredge.common.p110a.C1361a.C1351g;
import com.solaredge.common.p110a.C1361a.C1352h;
import com.solaredge.common.p110a.C1361a.C1353i;
import com.solaredge.common.p110a.C1361a.C1354j;
import com.solaredge.common.p110a.C1361a.C1355k;
import com.solaredge.common.p110a.C1361a.C1356l;
import com.solaredge.common.p110a.C1361a.C1357m;
import com.solaredge.common.p110a.C1361a.C1358n;
import com.solaredge.common.p110a.C1361a.C1359o;
import com.solaredge.common.p110a.C1361a.C1360p;
import com.solaredge.common.p116g.C2189b;
import com.solaredge.common.ui.LoginActivity;
import com.squareup.p124b.C1592t;
import com.squareup.p124b.C1592t.C1585a;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.TreeStrategy;
import org.simpleframework.xml.transform.Matcher;
import org.simpleframework.xml.transform.RegistryMatcher;
import p021c.C0559u;
import p021c.C1883x;
import p021c.C1883x.C0563a;
import p021c.p032b.C1875a;
import p021c.p032b.C1875a.C0525a;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.protobuf.ProtoConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/* compiled from: ServiceClient */
public class C1365j {
    private static C1365j f3360b = null;
    private static C1592t f3361c = null;
    private static int f3362h = 250;
    public String f3363a = "https://api.solaredge.com/solaredge-apigw/api/";
    private Retrofit f3364d;
    private Retrofit f3365e;
    private Retrofit f3366f;
    private C1883x f3367g;
    private C1346b f3368i;
    private C1354j f3369j;
    private C1351g f3370k;
    private C1356l f3371l;
    private C1349e f3372m;
    private C1347c f3373n;
    private C1357m f3374o;
    private C1352h f3375p;
    private C1353i f3376q;
    private C1359o f3377r;
    private C1360p f3378s;
    private C1358n f3379t;
    private C1345a f3380u;
    private C1350f f3381v;
    private C1355k f3382w;
    private C1348d f3383x;

    public static C1365j m3739a() {
        if (f3360b == null) {
            f3360b = new C1365j();
        }
        return f3360b;
    }

    public static void m3740a(int i) {
        f3362h = i;
    }

    public void m3742b() {
        if (this.f3367g != null) {
            this.f3367g.m5220t().m878b();
        }
    }

    public void m3741a(String str) {
        this.f3363a = str;
        C0559u c1875a = new C1875a();
        c1875a.m5181a(C0525a.BODY);
        C0563a c = new C0563a().m1001b(new C2178d()).m1001b(c1875a).m996a(new C2182i()).m1002b(true).m998a(true).m995a((long) (f3362h * 1000), TimeUnit.MILLISECONDS).m1000b((long) (f3362h * 1000), TimeUnit.MILLISECONDS).m1003c((long) (f3362h * 1000), TimeUnit.MILLISECONDS).m1004c(true);
        if (C1366a.m3750c()) {
            c = c.m997a(new C2179e());
        }
        this.f3367g = c.m999a();
        if (f3361c != null) {
            f3361c.m4727a();
        }
        f3361c = new C1585a(C1366a.m3749a().m3753b()).m4717a(new C1886a(this.f3367g)).m4718a();
        Matcher registryMatcher = new RegistryMatcher();
        registryMatcher.bind(GregorianCalendar.class, new C2177c());
        Serializer persister = new Persister(new TreeStrategy("sql-timestamp", "myLength"), registryMatcher);
        C0671f b = new C0672g().m1198a("yyyy-MM-dd HH:mm:ss").m1199a(GregorianCalendar.class, new C2189b()).m1199a(GregorianCalendar.class, new C2180f()).m1197a().m1200b();
        this.f3364d = new Builder().baseUrl(str).addConverterFactory(SimpleXmlConverterFactory.create(persister)).addCallAdapterFactory(RxJavaCallAdapterFactory.create()).client(this.f3367g).build();
        this.f3365e = new Builder().baseUrl(this.f3363a).client(this.f3367g).addConverterFactory(GsonConverterFactory.create(b)).build();
        this.f3366f = new Builder().baseUrl(this.f3363a).client(this.f3367g).addConverterFactory(ProtoConverterFactory.create()).build();
    }

    public void m3743b(String str) {
        if (this.f3367g != null) {
            this.f3366f = new Builder().baseUrl(str).client(this.f3367g).addConverterFactory(ProtoConverterFactory.create()).build();
            this.f3375p = (C1352h) this.f3366f.create(C1352h.class);
        }
    }

    public void m3744c() {
        if (this.f3364d == null) {
            String string = C1366a.m3749a().m3753b().getSharedPreferences(LoginActivity.class.getName(), 0).getString(LoginActivity.f6691a, "https://api.solaredge.com/solaredge-apigw/api/");
            if (!string.endsWith("/api/")) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(string);
                stringBuilder.append("/api/");
                string = stringBuilder.toString();
            }
            C1365j.m3739a().f3363a = string;
            m3741a(this.f3363a != null ? this.f3363a : "https://api.solaredge.com/solaredge-apigw/api/");
        }
        this.f3368i = (C1346b) this.f3364d.create(C1346b.class);
        this.f3369j = (C1354j) this.f3364d.create(C1354j.class);
        this.f3370k = (C1351g) this.f3364d.create(C1351g.class);
        this.f3371l = (C1356l) this.f3365e.create(C1356l.class);
        this.f3377r = (C1359o) this.f3364d.create(C1359o.class);
        this.f3378s = (C1360p) this.f3364d.create(C1360p.class);
        this.f3379t = (C1358n) this.f3364d.create(C1358n.class);
        this.f3380u = (C1345a) this.f3364d.create(C1345a.class);
        this.f3381v = (C1350f) this.f3364d.create(C1350f.class);
        this.f3382w = (C1355k) this.f3364d.create(C1355k.class);
        this.f3372m = (C1349e) this.f3365e.create(C1349e.class);
        this.f3373n = (C1347c) this.f3365e.create(C1347c.class);
        this.f3374o = (C1357m) this.f3365e.create(C1357m.class);
        this.f3375p = (C1352h) this.f3366f.create(C1352h.class);
        this.f3376q = (C1353i) this.f3365e.create(C1353i.class);
        this.f3383x = (C1348d) this.f3365e.create(C1348d.class);
    }

    public C1346b m3745d() {
        if (this.f3368i == null) {
            m3744c();
        }
        return this.f3368i;
    }

    public C1352h m3746e() {
        if (this.f3375p == null) {
            m3744c();
        }
        return this.f3375p;
    }

    public C1359o m3747f() {
        if (this.f3377r == null) {
            m3744c();
        }
        return this.f3377r;
    }

    public C1350f m3748g() {
        if (this.f3381v == null) {
            m3744c();
        }
        return this.f3381v;
    }
}
