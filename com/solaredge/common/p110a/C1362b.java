package com.solaredge.common.p110a;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/* compiled from: APIHelper */
public class C1362b {
    public static <T> void m3733a(Call<T> call, int i, final Callback<T> callback) {
        call.enqueue(new C2181h<T>(call, i) {
            public void mo2504a(Call<T> call, Response<T> response) {
                callback.onResponse(call, response);
            }

            public void mo2503a(Call<T> call, Throwable th) {
                callback.onFailure(call, th);
            }
        });
    }

    public static <T> void m3734a(Call<T> call, Callback<T> callback) {
        C1362b.m3733a(call, 1, callback);
    }
}
