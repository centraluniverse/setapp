package com.solaredge.common.p110a;

import com.solaredge.common.p116g.C1399c;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import org.simpleframework.xml.transform.Transform;

/* compiled from: DateFormatTransformer */
public class C2177c implements Transform<GregorianCalendar> {
    public /* synthetic */ Object read(String str) throws Exception {
        return m6616a(str);
    }

    public /* synthetic */ String write(Object obj) throws Exception {
        return m6615a((GregorianCalendar) obj);
    }

    public GregorianCalendar m6616a(String str) throws Exception {
        return C1399c.m3832a(str);
    }

    public String m6615a(GregorianCalendar gregorianCalendar) throws Exception {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(gregorianCalendar.getTime());
    }
}
