package com.solaredge.common.p110a;

import com.google.p040a.C0674j;
import com.google.p040a.C0675k;
import com.google.p040a.C0676l;
import com.google.p040a.C0677p;
import com.solaredge.common.p116g.C1399c;
import java.lang.reflect.Type;
import java.util.GregorianCalendar;

/* compiled from: JsonMillisecDeserializer */
public class C2180f implements C0675k<GregorianCalendar> {
    public /* synthetic */ Object mo1787a(C0676l c0676l, Type type, C0674j c0674j) throws C0677p {
        return m6620b(c0676l, type, c0674j);
    }

    public GregorianCalendar m6620b(C0676l c0676l, Type type, C0674j c0674j) throws C0677p {
        return C1399c.m3833b(Long.toString(c0676l.m1215m().mo1448d()));
    }
}
