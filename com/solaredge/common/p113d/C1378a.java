package com.solaredge.common.p113d;

/* compiled from: TranslationUpdatedEvent */
public class C1378a {
    private String f3399a;
    private boolean f3400b;

    public C1378a(String str) {
        this.f3399a = str;
        this.f3400b = true;
    }

    public C1378a(boolean z) {
        this.f3400b = z;
    }

    public String m3773a() {
        return this.f3399a;
    }

    public boolean m3774b() {
        return this.f3400b;
    }
}
