package com.solaredge.common;

import java.util.ArrayList;
import java.util.List;

/* compiled from: LoginHandler */
public class C1368b {
    public static List<Integer> f3388a = new ArrayList();
    public static boolean f3389b;
    private static C1368b f3390c;
    private String f3391d;
    private C1371c f3392e;
    private Boolean f3393f = Boolean.valueOf(false);
    private Boolean f3394g = Boolean.valueOf(false);

    private C1368b() {
    }

    public static C1368b m3756a() {
        if (f3390c == null) {
            f3390c = new C1368b();
        }
        return f3390c;
    }

    public String m3760b() {
        return this.f3391d;
    }

    public void m3759a(String str) {
        this.f3391d = str;
    }

    public C1371c m3762c() {
        return this.f3392e;
    }

    public void m3757a(C1371c c1371c) {
        this.f3392e = c1371c;
    }

    public Boolean m3763d() {
        return this.f3393f;
    }

    public void m3758a(Boolean bool) {
        this.f3393f = bool;
    }

    public Boolean m3764e() {
        return this.f3394g;
    }

    public void m3761b(Boolean bool) {
        this.f3394g = bool;
    }
}
