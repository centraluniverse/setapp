package com.solaredge.common.p114e;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.analytics.C0697c.C1971a;
import com.solaredge.common.C1366a;
import com.solaredge.common.models.User;
import com.solaredge.common.models.response.LocalesListResponse;
import com.solaredge.common.models.response.TranslationResponse;
import com.solaredge.common.p110a.C1362b;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p113d.C1378a;
import com.solaredge.common.p116g.C1403f;
import p000a.p001a.p002a.C0005c;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/* compiled from: LocalizationSyncHelper */
public class C1384d {
    private static C1384d f3444a;
    private Call<LocalesListResponse> f3445b;

    /* compiled from: LocalizationSyncHelper */
    class C21862 implements Callback<User> {
        final /* synthetic */ C1384d f5499a;

        C21862(C1384d c1384d) {
            this.f5499a = c1384d;
        }

        public void onResponse(Call<User> call, final Response<User> response) {
            if (response.isSuccessful() != null && TextUtils.isEmpty(((User) response.body()).getLocale()) == null) {
                boolean z = false;
                call = C1366a.m3749a().m3753b().getSharedPreferences("user_type", 0).edit();
                String str = "is_solaredge_user";
                if (((User) response.body()).getAccount() != null && ((User) response.body()).getAccount().getAccountId() == 32) {
                    z = true;
                }
                call.putBoolean(str, z);
                call.commit();
                call = C1394f.m3816a();
                Context b = C1366a.m3749a().m3753b();
                String email = ((User) response.body()).getEmail();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(((User) response.body()).getFirstName());
                stringBuilder.append(" ");
                stringBuilder.append(((User) response.body()).getLastName());
                call.m3819a(b, email, stringBuilder.toString());
                C1394f.m3816a().m3818a(C1366a.m3749a().m3753b(), ((User) response.body()).getLocale());
                C1394f.m3816a().m3821b(C1366a.m3749a().m3753b(), ((User) response.body()).getSi());
                call = C1396h.m3827a().m3828b();
                C1971a c1971a = new C1971a("Site List", "Refresh");
                email = ((User) response.body()).getAccount() != null ? ((User) response.body()).getAccount().getId() == 32 ? "Solaredge" : "Installer" : "Owner";
                call.m7058a(((C1971a) c1971a.m1234a(1, email)).m1236a());
                new Thread(new Runnable(this) {
                    final /* synthetic */ C21862 f3443b;

                    public void run() {
                        C1382c.m3782a().m3790b(C1366a.m3749a().m3753b(), ((User) response.body()).getLocale());
                    }
                }).start();
                C0005c.m3a().m20e(response.body());
                this.f5499a.m3796d();
            }
        }

        public void onFailure(Call<User> call, Throwable th) {
            th.printStackTrace();
            this.f5499a.m3796d();
        }
    }

    public static synchronized C1384d m3793a() {
        C1384d c1384d;
        synchronized (C1384d.class) {
            if (f3444a == null) {
                f3444a = new C1384d();
            }
            c1384d = f3444a;
        }
        return c1384d;
    }

    private void m3795c() {
        if (!C0005c.m3a().m17b(this)) {
            C0005c.m3a().m15a((Object) this);
        }
    }

    private void m3796d() {
        if (C0005c.m3a().m17b(this)) {
            C0005c.m3a().m18c(this);
        }
    }

    public void onEvent(User user) {
        m3798a(user.getLocale());
    }

    public void m3798a(final String str) {
        C1365j.m3739a().m3748g().m3725a(str).enqueue(new Callback<TranslationResponse>(this) {
            final /* synthetic */ C1384d f5498b;

            public void onResponse(Call<TranslationResponse> call, Response<TranslationResponse> response) {
                if (response.isSuccessful() != null && ((TranslationResponse) response.body()).getTranslations() != null && ((TranslationResponse) response.body()).getTranslations().size() > null) {
                    C1382c.m3782a().m3791b(((TranslationResponse) response.body()).getTranslations());
                    C0005c.m3a().m20e(new C1378a(str));
                }
            }

            public void onFailure(Call<TranslationResponse> call, Throwable th) {
                th.printStackTrace();
                C0005c.m3a().m20e(new C1378a(false));
            }
        });
    }

    public void m3799b() {
        m3795c();
        C1365j.m3739a().m3747f().m3731a().enqueue(new C21862(this));
    }

    public void m3797a(final C1403f c1403f) {
        C1366a.m3749a().m3753b();
        if (this.f3445b != null) {
            this.f3445b.cancel();
        }
        this.f3445b = C1365j.m3739a().m3748g().m3724a();
        C1362b.m3734a(this.f3445b, new Callback<LocalesListResponse>(this) {
            final /* synthetic */ C1384d f5501b;

            public void onResponse(Call<LocalesListResponse> call, Response<LocalesListResponse> response) {
                if (response.isSuccessful() && call.isCanceled() == null && ((LocalesListResponse) response.body()).getLocales() != null && ((LocalesListResponse) response.body()).getLocales().size() > null) {
                    C1382c.m3782a().m3787a(((LocalesListResponse) response.body()).getLocales());
                    if (c1403f != null) {
                        c1403f.mo1788b();
                    }
                }
            }

            public void onFailure(Call<LocalesListResponse> call, Throwable th) {
                if (call.isCanceled() == null) {
                    th.printStackTrace();
                }
            }
        });
    }
}
