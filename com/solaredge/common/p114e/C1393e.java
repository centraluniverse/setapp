package com.solaredge.common.p114e;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.solaredge.common.C1366a;
import com.solaredge.common.models.Inverter3rdParty;
import com.solaredge.common.models.Map;
import com.solaredge.common.models.SolarSite;
import com.solaredge.common.models.map.Group;
import com.solaredge.common.models.map.Group.ModuleOrientation;
import com.solaredge.common.models.map.Inverter;
import com.solaredge.common.models.map.Rectangle;
import com.solaredge.common.models.map.Rectangle.TableColumns;
import com.solaredge.common.models.map.SMI;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p117h.C2192e;
import com.solaredge.common.p117h.C2439c;
import com.solaredge.common.p117h.C2440f;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

/* compiled from: MapManager */
public class C1393e {
    private static C1393e f3478d;
    private int f3479A = 0;
    private int f3480B = 0;
    private String f3481a = "";
    private final String f3482b = "MapManager";
    private final String f3483c = "PREFS_CURRENTLY_OPEN_SITE_ID";
    private Map f3484e;
    private SolarSite f3485f;
    private float f3486g = 1.0f;
    private int f3487h = 0;
    private int f3488i = 0;
    private int f3489j;
    private Context f3490k;
    private int f3491l = 0;
    private int f3492m = 0;
    private int f3493n = 0;
    private int f3494o = 0;
    private int f3495p = 0;
    private int f3496q = 0;
    private C1389c f3497r = C1389c.HORIZONTAL;
    private C1388b f3498s = C1388b.HORIZONTAL;
    private C1387a f3499t = C1387a.RIGHT;
    private C1387a f3500u = C1387a.DOWN;
    private int f3501v = 0;
    private Set<String> f3502w = new HashSet();
    private C1390d f3503x = null;
    private C1390d f3504y = null;
    private C1391e f3505z = C1391e.HORIZONTAL;

    /* compiled from: MapManager */
    public enum C1387a {
        NONE,
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    /* compiled from: MapManager */
    public enum C1388b {
        HORIZONTAL,
        VERTICAL
    }

    /* compiled from: MapManager */
    public enum C1389c {
        HORIZONTAL,
        VERTICAL
    }

    /* compiled from: MapManager */
    public enum C1390d {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    /* compiled from: MapManager */
    public enum C1391e {
        HORIZONTAL,
        VERTICAL
    }

    /* compiled from: MapManager */
    private class C1392f extends AsyncTask<Void, Void, Void> {
        final /* synthetic */ C1393e f3472a;
        private Rectangle f3473b;
        private String f3474c;
        private String f3475d;
        private Object f3476e;
        private String f3477f;

        protected /* synthetic */ Object doInBackground(Object[] objArr) {
            return m3800a((Void[]) objArr);
        }

        public C1392f(C1393e c1393e, String str, String str2, Object obj, Rectangle rectangle, String str3) {
            this.f3472a = c1393e;
            this.f3473b = rectangle;
            this.f3474c = str;
            this.f3475d = str2;
            this.f3476e = obj;
            this.f3477f = str3;
        }

        protected Void m3800a(Void... voidArr) {
            if (!(this.f3476e == null || this.f3477f == null)) {
                voidArr = new ContentValues();
                voidArr.put(TableColumns.POS_X, Double.valueOf(this.f3473b.getPosX()));
                voidArr.put(TableColumns.POS_Y, Double.valueOf(this.f3473b.getPosY()));
                SQLiteDatabase b = C1369a.m3766b(this.f3472a.f3490k);
                String str = Rectangle.TABLE_NAME;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(this.f3474c);
                stringBuilder.append(" = ?");
                b.update(str, voidArr, stringBuilder.toString(), new String[]{this.f3475d});
                this.f3472a.m3809a(this.f3477f);
            }
            return null;
        }
    }

    public static synchronized C1393e m3801a() {
        C1393e c1393e;
        synchronized (C1393e.class) {
            if (f3478d == null) {
                f3478d = new C1393e();
            }
            c1393e = f3478d;
        }
        return c1393e;
    }

    public SolarSite m3804a(Context context, long j) {
        Cursor query = C1369a.m3766b(context).query(SolarSite.TABLE_NAME, null, "_id = ?", new String[]{String.valueOf(j)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst() != null) {
                    j = new SolarSite(query);
                    return j;
                }
            } finally {
                query.close();
            }
        }
        query.close();
        return null;
    }

    public Group m3805a(long j) {
        if (this.f3484e == null || this.f3484e.getLayout() == null) {
            return null;
        }
        return (Group) this.f3484e.getLayout().getGroups().get(Long.valueOf(j));
    }

    public Inverter m3812b(long j) {
        if (this.f3484e == null || this.f3484e.getLayout() == null) {
            return null;
        }
        Inverter inverter = (Inverter) this.f3484e.getLayout().getInverters().get(Long.valueOf(j));
        if (inverter != null) {
            return inverter;
        }
        return m3814c(j);
    }

    public Inverter3rdParty m3814c(long j) {
        if (this.f3484e == null || this.f3484e.getLayout() == null) {
            return null;
        }
        return (Inverter3rdParty) this.f3484e.getLayout().get3rdPartyInverters().get(Long.valueOf(j));
    }

    public SMI m3815d(long j) {
        if (this.f3484e == null || this.f3484e.getLayout() == null) {
            return null;
        }
        return (SMI) this.f3484e.getLayout().getSMIs().get(Long.valueOf(j));
    }

    public SolarSite m3811b() {
        if (this.f3485f != null) {
            return this.f3485f;
        }
        long j = C1366a.m3749a().m3753b().getSharedPreferences("MapManager", 0).getLong("PREFS_CURRENTLY_OPEN_SITE_ID", -1);
        if (j == -1) {
            return null;
        }
        this.f3485f = C1393e.m3801a().m3804a(C1366a.m3749a().m3753b(), j);
        return this.f3485f;
    }

    public int m3813c() {
        return this.f3489j;
    }

    public void m3809a(String str) {
        m3810a(str, Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis());
    }

    public void m3810a(String str, long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SolarSite.TableColumns.MAP_LAST_MODIFIED, Long.valueOf(j));
        C1369a.m3766b(this.f3490k).update(SolarSite.TABLE_NAME, contentValues, "site_id = ?", new String[]{str});
    }

    public void m3806a(View view, int i) {
        String l;
        Group inverter;
        String str;
        String str2;
        Object obj;
        Rectangle rectangle;
        String str3 = null;
        switch (i) {
            case 0:
                i = "inverter_id";
                C2439c c2439c = (C2439c) view;
                l = Long.toString(c2439c.getInverter().getId());
                inverter = c2439c.getInverter();
                view = c2439c.getInverter().getRectangle();
                break;
            case 1:
                i = "smi_id";
                C2440f c2440f = (C2440f) view;
                l = Long.toString(c2440f.getSmi().getId());
                inverter = c2440f.getSmi();
                view = c2440f.getSmi().getRectangle();
                break;
            case 2:
                i = "group_id";
                C2192e c2192e = (C2192e) view;
                l = Long.toString(c2192e.getGroup().getId());
                inverter = c2192e.getGroup();
                view = c2192e.getGroup().getRectangle();
                break;
            default:
                str = null;
                str2 = str;
                obj = str2;
                rectangle = obj;
                break;
        }
        rectangle = view;
        str = i;
        str2 = l;
        obj = inverter;
        if (this.f3485f != null) {
            str3 = this.f3485f.getSiteId();
        }
        new C1392f(this, str, str2, obj, rectangle, str3).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    public void m3807a(View view, int i, int i2, int i3) {
        switch (i) {
            case 0:
                C2439c c2439c = (C2439c) view;
                c2439c.getInverter().getRectangle().setPosX((double) i2);
                c2439c.getInverter().getRectangle().setPosY((double) i3);
                break;
            case 1:
                C2440f c2440f = (C2440f) view;
                c2440f.getSmi().getRectangle().setPosX((double) i2);
                c2440f.getSmi().getRectangle().setPosY((double) i3);
                break;
            case 2:
                C2192e c2192e = (C2192e) view;
                c2192e.getGroup().getRectangle().setPosX((double) i2);
                c2192e.getGroup().getRectangle().setPosY((double) i3);
                break;
            default:
                break;
        }
        view.setX((float) i2);
        view.setY((float) i3);
    }

    public void m3808a(final View view, int i, final ModuleOrientation moduleOrientation) {
        if (i == 2) {
            i = ((C2192e) view).getGroup();
            if (i.getModuleOrientation() != moduleOrientation) {
                i.setModuleOrientation(moduleOrientation);
                view.requestLayout();
                ViewCompat.postInvalidateOnAnimation((ViewGroup) view.getParent());
                view.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener(this) {
                    final /* synthetic */ C1393e f3448c;

                    @TargetApi(16)
                    public void onGlobalLayout() {
                        if (VERSION.SDK_INT >= 16) {
                            view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                        float rotation = view.getRotation();
                        i.getRectangle().setHeight((double) view.getHeight());
                        i.getRectangle().setWidth((double) view.getWidth());
                        view.setPivotX((float) (i.getRectangle().getWidth() / 2.0d));
                        view.setPivotY((float) (i.getRectangle().getHeight() / 2.0d));
                        view.setRotation(0.0f);
                        float posY = (float) i.getRectangle().getPosY();
                        view.setX((float) i.getRectangle().getPosX());
                        view.setY(posY);
                        view.setRotation(rotation);
                    }
                });
                new Thread(new Runnable(this) {
                    final /* synthetic */ C1393e f3451c;

                    public void run() {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(Group.TableColumns.MODULE_ORIENTATION, Integer.valueOf(moduleOrientation == ModuleOrientation.horizontal ? 0 : 1));
                        C1369a.m3766b(this.f3451c.f3490k).update(Group.TABLE_NAME, contentValues, "_id = ?", new String[]{Long.toString(i.getId())});
                        this.f3451c.m3809a(this.f3451c.f3485f.getSiteId());
                    }
                }).start();
            }
        }
    }
}
