package com.solaredge.common.p114e;

import android.widget.Toast;

/* compiled from: ToastManager */
public class C1395g {
    private static C1395g f3509a;
    private Toast f3510b;

    public static synchronized C1395g m3824a() {
        C1395g c1395g;
        synchronized (C1395g.class) {
            if (f3509a == null) {
                f3509a = new C1395g();
            }
            c1395g = f3509a;
        }
        return c1395g;
    }

    public void m3825a(String str, int i) {
        m3826a(str, i, true);
    }

    public void m3826a(java.lang.String r2, int r3, boolean r4) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r1 = this;
        if (r4 == 0) goto L_0x0019;
    L_0x0002:
        r4 = com.solaredge.common.p114e.C1381b.m3777a();
        r0 = com.solaredge.common.C1366a.m3749a();
        r0 = r0.m3753b();
        r4 = r4.m3778a(r0);
        r4 = r4.size();
        if (r4 > 0) goto L_0x0019;
    L_0x0018:
        return;
    L_0x0019:
        r4 = r1.f3510b;
        if (r4 == 0) goto L_0x0022;
    L_0x001d:
        r4 = r1.f3510b;
        r4.cancel();
    L_0x0022:
        r4 = com.solaredge.common.C1366a.m3749a();
        r4 = r4.m3753b();
        r2 = android.widget.Toast.makeText(r4, r2, r3);
        r1.f3510b = r2;
        r2 = r1.f3510b;	 Catch:{ Exception -> 0x0046 }
        r2 = r2.getView();	 Catch:{ Exception -> 0x0046 }
        r2 = (android.view.ViewGroup) r2;	 Catch:{ Exception -> 0x0046 }
        r3 = 0;	 Catch:{ Exception -> 0x0046 }
        r2 = r2.getChildAt(r3);	 Catch:{ Exception -> 0x0046 }
        r2 = (android.widget.TextView) r2;	 Catch:{ Exception -> 0x0046 }
        r3 = 1;	 Catch:{ Exception -> 0x0046 }
        r4 = 1096810496; // 0x41600000 float:14.0 double:5.41896386E-315;	 Catch:{ Exception -> 0x0046 }
        r2.setTextSize(r3, r4);	 Catch:{ Exception -> 0x0046 }
        goto L_0x004b;
    L_0x0046:
        r2 = "failed to set toast text size";
        com.solaredge.common.p116g.C1398a.m3831a(r2);
    L_0x004b:
        r2 = r1.f3510b;
        r2.show();
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.common.e.g.a(java.lang.String, int, boolean):void");
    }
}
