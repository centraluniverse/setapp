package com.solaredge.common.p114e;

import android.content.Context;
import android.text.TextUtils;
import com.google.p040a.p045c.C0664a;
import com.solaredge.common.C1366a;
import com.solaredge.common.C1379d.C1377f;
import com.solaredge.common.models.LocaleInfo;
import com.solaredge.common.models.Translation;
import com.solaredge.common.p116g.C1403f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: LocalizationManager */
public class C1382c {
    public static String f3405A = "API_Energy_Consumption_Month";
    public static String f3406B = "API_Energy_Consumption_Today";
    public static String f3407C = "API_Estimated_Production_Title";
    public static String f3408D = "API_Estimated_Production_Estimated_Energy";
    public static String f3409E = "API_Estimated_Production_Electric_Bill_Reduction";
    public static String f3410F = "API_Estimated_Production_CO2";
    public static String f3411G = "API_Estimated_Production_Explanation";
    private static C1382c f3412H = null;
    public static String f3413a = "API_LoadControl_Details_Device_Name_Empty";
    public static String f3414b = "API_LoadControl_Details_Short_Minute";
    public static String f3415c = "API_LoadControl_AccessLevel_Dialog_Title";
    public static String f3416d = "API_LoadControl_AccessLevel_Dialog_Warning";
    public static String f3417e = "API_LoadControl_AccessLevel_Dialog_Text";
    public static String f3418f = "API_LoadControl_AccessLevel_Dialog_Continue";
    public static String f3419g = "API_LoadControl_GainAccess_Dialog_Warning";
    public static String f3420h = "API_LoadControl_GainAccess_Dialog_GainAccess";
    public static String f3421i = "API_LoadControl_GainAccess_Dialog_Text";
    public static String f3422j = "API_LoadControl_View_Only_mode";
    public static String f3423k = "API_LoadControl_Gain_Control";
    public static String f3424l = "API_LoadControl_Control_Access_Granted";
    public static String f3425m = "API_LoadControl_Access_Level_Description_None";
    public static String f3426n = "API_LoadControl_Access_Level_Description_View_Only";
    public static String f3427o = "API_LoadControl_Access_Level_Description_View_And_Control";
    public static String f3428p = "API_LoadControl_Access_Level_Text";
    public static String f3429q = "API_LoadControl_Access_Level_Title";
    public static String f3430r = "API_LoadControl_Access_Level_None";
    public static String f3431s = "API_LoadControl_Access_Level_View_Only";
    public static String f3432t = "API_LoadControl_Access_Level_View_And_Control";
    public static String f3433u = "API_LoadControl_Access_Level_Menu_Title";
    public static String f3434v = "API_LoadControl_Device_Excess_Solar_Power_Priority_Text";
    public static String f3435w = "API_LoadControl_Device_Excess_Solar_Power_Enable";
    public static String f3436x = "API_LoadControl_Device_Excess_Solar_Power_Disable";
    public static String f3437y = "API_Energy_Consumption_Title";
    public static String f3438z = "API_Energy_Consumption_Total";
    private String f3439I;
    private Map<String, Translation> f3440J = new HashMap();
    private List<LocaleInfo> f3441K = new ArrayList();

    /* compiled from: LocalizationManager */
    class C21831 extends C0664a<Map<String, Translation>> {
        final /* synthetic */ C1382c f5493d;

        C21831(C1382c c1382c) {
            this.f5493d = c1382c;
        }
    }

    public static synchronized C1382c m3782a() {
        C1382c c1382c;
        synchronized (C1382c.class) {
            if (f3412H == null) {
                f3412H = new C1382c();
            }
            c1382c = f3412H;
        }
        return c1382c;
    }

    public String m3783a(String str) {
        if (this.f3440J == null || this.f3440J.get(str) == null) {
            Object obj = -1;
            switch (str.hashCode()) {
                case -1988769313:
                    if (str.equals("API_SITE_Menu_Reload_From_Server")) {
                        obj = 8;
                        break;
                    }
                    break;
                case -1664836588:
                    if (str.equals("API_MAP_Serial_Number_Reassigned")) {
                        obj = 3;
                        break;
                    }
                    break;
                case -1519595607:
                    if (str.equals("API_About_Terms_Link")) {
                        obj = 5;
                        break;
                    }
                    break;
                case -1225411412:
                    if (str.equals("API_SITE_Delete_Notification")) {
                        obj = 9;
                        break;
                    }
                    break;
                case -1160057324:
                    if (str.equals("API_NMG_Orientation")) {
                        obj = null;
                        break;
                    }
                    break;
                case -535661432:
                    if (str.equals("API_About_Privacy_Link")) {
                        obj = 4;
                        break;
                    }
                    break;
                case -43385745:
                    if (str.equals("API_Site_Name_InValid_Length")) {
                        obj = 6;
                        break;
                    }
                    break;
                case 239182353:
                    if (str.equals("API_Activator_Searching_For_Wifi_Move_And_Release")) {
                        obj = 11;
                        break;
                    }
                    break;
                case 300880272:
                    if (str.equals("API_Activator_Processing_Time_Left_Text")) {
                        obj = 10;
                        break;
                    }
                    break;
                case 774888934:
                    if (str.equals("API_MAP_Serial_Number_Deleted")) {
                        obj = 1;
                        break;
                    }
                    break;
                case 1290693508:
                    if (str.equals("API_SITE_Menu_Delete_Local")) {
                        obj = 7;
                        break;
                    }
                    break;
                case 1694438060:
                    if (str.equals("API_MAP_UNDO")) {
                        obj = 2;
                        break;
                    }
                    break;
                default:
                    break;
            }
            switch (obj) {
                case null:
                    return "Orientation";
                case 1:
                    return "Serial Number unassigned";
                case 2:
                    return "Undo";
                case 3:
                    return "Serial Number Reassigned";
                case 4:
                    return "<a href=\"http://www.solaredge.com/groups/terms-and-conditions/privacy-policy\">Privacy Policy</a>";
                case 5:
                    return "<a href=\"http://www.solaredge.com/groups/terms-and-conditions\">Terms and Conditions</a>";
                case 6:
                    return "Site Name must have between 3 and 48 characters";
                case 7:
                    return "Delete local copy";
                case 8:
                    return "Reload map from server";
                case 9:
                    return "Local copy of ”{0}” deleted";
                case 10:
                    return "Estimated time left: {0} min";
                case 11:
                    return "Move and release";
                default:
                    return str.replace("API_", "");
            }
        }
        return ((Translation) this.f3440J.get(str)).getValue() != null ? ((Translation) this.f3440J.get(str)).getValue() : str.replace("API_", "");
    }

    public String m3784a(String str, String... strArr) {
        str = m3783a(str);
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        for (int i = 0; i < strArr.length; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{");
            stringBuilder.append(i);
            stringBuilder.append("}");
            CharSequence stringBuilder2 = stringBuilder.toString();
            while (str.contains(stringBuilder2)) {
                str = str.replace(stringBuilder2, strArr[i]);
            }
        }
        return str;
    }

    public String m3788b(String str) {
        str = m3783a(str);
        return "API_List_No_Internet_Connection".contains(str) ? C1366a.m3749a().m3753b().getResources().getString(C1377f.lbl_err_no_connection) : str;
    }

    public void m3786a(android.content.Context r3, java.lang.String r4) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r2 = this;
        r4 = android.text.TextUtils.isEmpty(r4);
        if (r4 == 0) goto L_0x0007;
    L_0x0006:
        return;
    L_0x0007:
        r4 = r2.f3440J;
        if (r4 == 0) goto L_0x0013;
    L_0x000b:
        r4 = r2.f3440J;
        r4 = r4.size();
        if (r4 != 0) goto L_0x0046;
    L_0x0013:
        if (r3 == 0) goto L_0x0046;
    L_0x0015:
        r4 = "translations_prefs";
        r0 = 0;
        r3 = r3.getSharedPreferences(r4, r0);
        r4 = new com.google.a.f;
        r4.<init>();
        r0 = "translations";
        r1 = "";
        r3 = r3.getString(r0, r1);
        r0 = android.text.TextUtils.isEmpty(r3);	 Catch:{ Exception -> 0x0041 }
        if (r0 != 0) goto L_0x0046;	 Catch:{ Exception -> 0x0041 }
    L_0x002f:
        r0 = new com.solaredge.common.e.c$1;	 Catch:{ Exception -> 0x0041 }
        r0.<init>(r2);	 Catch:{ Exception -> 0x0041 }
        r0 = r0.m1108b();	 Catch:{ Exception -> 0x0041 }
        r3 = r4.m1188a(r3, r0);	 Catch:{ Exception -> 0x0041 }
        r3 = (java.util.Map) r3;	 Catch:{ Exception -> 0x0041 }
        r2.f3440J = r3;	 Catch:{ Exception -> 0x0041 }
        goto L_0x0046;
    L_0x0041:
        r3 = "failed to read json of translations";
        com.solaredge.common.p116g.C1398a.m3831a(r3);
    L_0x0046:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.common.e.c.a(android.content.Context, java.lang.String):void");
    }

    public void m3790b(Context context, String str) {
        if (TextUtils.isEmpty(this.f3439I) || !(TextUtils.isEmpty(this.f3439I) || this.f3439I.equals(str))) {
            this.f3439I = str;
            m3786a(context, this.f3439I);
        }
    }

    public void m3785a(final Context context) {
        final String a = C1394f.m3816a().m3817a(context);
        C1384d.m3793a().m3797a(new C1403f(this) {
            final /* synthetic */ C1382c f5496c;

            public void mo1788b() {
                Object obj;
                for (LocaleInfo code : this.f5496c.m3792c(context)) {
                    if (code.getCode().equals(a)) {
                        obj = 1;
                        break;
                    }
                }
                obj = null;
                if (obj != null) {
                    this.f5496c.m3786a(context, C1394f.m3816a().m3817a(context));
                    return;
                }
                C1394f.m3816a().m3818a(context, "en_US");
                this.f5496c.m3790b(context, "en_US");
            }
        });
    }

    public void m3789b(Context context) {
        if (this.f3440J == null || (this.f3440J != null && this.f3440J.size() == 0)) {
            m3785a(context);
        }
    }

    public List<LocaleInfo> m3792c(Context context) {
        return this.f3441K;
    }

    public void m3787a(List<LocaleInfo> list) {
        this.f3441K = list;
    }

    public void m3791b(java.util.List<com.solaredge.common.models.Translation> r4) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r3 = this;
        r0 = r3.f3440J;
        r0.clear();
        r4 = r4.iterator();
    L_0x0009:
        r0 = r4.hasNext();
        if (r0 == 0) goto L_0x001f;
    L_0x000f:
        r0 = r4.next();
        r0 = (com.solaredge.common.models.Translation) r0;
        r1 = r3.f3440J;
        r2 = r0.getKey();
        r1.put(r2, r0);
        goto L_0x0009;
    L_0x001f:
        r4 = com.solaredge.common.C1366a.m3749a();
        r4 = r4.m3753b();
        r0 = "translations_prefs";
        r1 = 0;
        r4 = r4.getSharedPreferences(r0, r1);
        r4 = r4.edit();
        r0 = new com.google.a.f;
        r0.<init>();
        r1 = r3.f3440J;	 Catch:{ Exception -> 0x0046 }
        r0 = r0.m1190a(r1);	 Catch:{ Exception -> 0x0046 }
        r1 = "translations";	 Catch:{ Exception -> 0x0046 }
        r4.putString(r1, r0);	 Catch:{ Exception -> 0x0046 }
        r4.commit();	 Catch:{ Exception -> 0x0046 }
        goto L_0x004b;
    L_0x0046:
        r4 = "failed to save json of translations to sharedPrefernces";
        com.solaredge.common.p116g.C1398a.m3831a(r4);
    L_0x004b:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.common.e.c.b(java.util.List):void");
    }
}
