package com.solaredge.common.p114e;

import android.content.Context;
import android.net.ConnectivityManager;

/* compiled from: ConnectionManager */
public class C1380a {
    private static C1380a f3401a;

    public static synchronized C1380a m3775a() {
        C1380a c1380a;
        synchronized (C1380a.class) {
            if (f3401a == null) {
                f3401a = new C1380a();
            }
            c1380a = f3401a;
        }
        return c1380a;
    }

    public boolean m3776a(Context context) {
        if (context == null) {
            return false;
        }
        context = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (context != null) {
            return context.isConnectedOrConnecting();
        }
        return false;
    }
}
