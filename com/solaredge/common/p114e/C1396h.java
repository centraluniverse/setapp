package com.solaredge.common.p114e;

import com.google.android.gms.analytics.C2370b;
import com.google.android.gms.analytics.C2372e;
import com.solaredge.common.C1366a;

/* compiled from: TrackerManager */
public class C1396h {
    private static C1396h f3511a;
    private static String f3512b;
    private C2372e f3513c;

    public static synchronized C1396h m3827a() {
        C1396h c1396h;
        synchronized (C1396h.class) {
            if (f3511a == null) {
                f3511a = new C1396h();
            }
            c1396h = f3511a;
        }
        return c1396h;
    }

    public synchronized C2372e m3828b() {
        if (this.f3513c == null) {
            this.f3513c = C2370b.m7022a(C1366a.m3749a().m3753b()).m7024a(f3512b);
            this.f3513c.m7059a(true);
        }
        return this.f3513c;
    }
}
