package com.solaredge.common.p114e;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import p021c.C0545l;
import p021c.C0545l.C0544a;

/* compiled from: CookieStoreManager */
public class C1381b {
    private static C1381b f3402a;
    private final String f3403b = "cookie_store";
    private boolean f3404c = true;

    public static synchronized C1381b m3777a() {
        C1381b c1381b;
        synchronized (C1381b.class) {
            if (f3402a == null) {
                f3402a = new C1381b();
            }
            c1381b = f3402a;
        }
        return c1381b;
    }

    public void m3779a(Context context, List<C0545l> list) {
        if (this.f3404c && list != null) {
            if (list.size() != 0) {
                context = context.getSharedPreferences("cookie_store", 0).edit();
                for (C0545l c0545l : list) {
                    context.putString(c0545l.m867a(), c0545l.m869b());
                }
                if (VERSION.SDK_INT < 9) {
                    context.commit();
                } else {
                    context.apply();
                }
            }
        }
    }

    public List<C0545l> m3778a(Context context) {
        context = context.getSharedPreferences("cookie_store", 0);
        List<C0545l> arrayList = new ArrayList();
        for (Entry entry : context.getAll().entrySet()) {
            arrayList.add(new C0544a().m858c("solaredge.com").m855a((String) entry.getKey()).m857b(entry.getValue().toString()).m856a());
        }
        return arrayList;
    }

    @SuppressLint({"NewApi"})
    public void m3781b(Context context) {
        context = context.getSharedPreferences("cookie_store", 0).edit();
        context.clear();
        if (VERSION.SDK_INT < 9) {
            context.commit();
        } else {
            context.apply();
        }
    }

    public void m3780a(boolean z) {
        this.f3404c = z;
    }
}
