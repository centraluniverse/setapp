package com.solaredge.common.p114e;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.crashlytics.android.core.CrashlyticsCore.Builder;
import p008b.p009a.p010a.p011a.C0452c;

/* compiled from: SettingsManager */
public class C1394f {
    private static C1394f f3506a;
    private final String f3507b = "settings_prefs";
    private String f3508c = null;

    public static synchronized C1394f m3816a() {
        C1394f c1394f;
        synchronized (C1394f.class) {
            if (f3506a == null) {
                f3506a = new C1394f();
            }
            c1394f = f3506a;
        }
        return c1394f;
    }

    @SuppressLint({"NewApi"})
    public void m3818a(Context context, String str) {
        this.f3508c = str;
        if (context != null) {
            context = context.getSharedPreferences("settings_prefs", 0).edit();
            context.putString("pref_key_locale", str);
            if (VERSION.SDK_INT < 9) {
                context.commit();
            } else {
                context.apply();
            }
        }
    }

    public String m3817a(Context context) {
        return context.getSharedPreferences("settings_prefs", 0).getString("pref_key_locale", "en_US");
    }

    @SuppressLint({"NewApi"})
    public void m3821b(Context context, String str) {
        if (context != null) {
            context = context.getSharedPreferences("settings_prefs", 0).edit();
            context.putString("pref_key_metrics", str);
            if (VERSION.SDK_INT < 9) {
                context.commit();
            } else {
                context.apply();
            }
        }
    }

    @SuppressLint({"NewApi"})
    public void m3820a(Context context, boolean z) {
        if (context != null) {
            context = context.getSharedPreferences("settings_prefs", 0).edit();
            context.putBoolean("pref_key_is_demo", z);
            if (VERSION.SDK_INT < true) {
                context.commit();
            } else {
                context.apply();
            }
        }
    }

    public boolean m3822b(Context context) {
        return context.getSharedPreferences("settings_prefs", 0).getBoolean("pref_key_is_demo", false);
    }

    @SuppressLint({"NewApi"})
    public void m3819a(Context context, String str, String str2) {
        if (!C0452c.m369j()) {
            CrashlyticsCore build = new Builder().disabled(false).build();
            C0452c.m358a(context, new Crashlytics.Builder().core(build).build());
        }
        Crashlytics.setUserEmail(str);
        Crashlytics.setUserName(str2);
        if (context != null && !m3822b(context)) {
            context = context.getSharedPreferences("settings_prefs", 0).edit();
            context.putString("pref_key_email", str);
            context.putString("pref_key_username", str2);
            if (VERSION.SDK_INT < 9) {
                context.commit();
            } else {
                context.apply();
            }
        }
    }

    public String m3823c(Context context) {
        context = context.getSharedPreferences("settings_prefs", 0).getString("pref_key_email", "");
        return context.contains("@") ? context : "";
    }
}
