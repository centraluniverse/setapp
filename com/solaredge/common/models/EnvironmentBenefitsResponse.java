package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EnvironmentBenefitsResponse {
    @C0631a
    @C0633c(a = "envBenefits")
    private EnvironmentBenefits envBenefits;

    public EnvironmentBenefits getEnvBenefits() {
        return this.envBenefits;
    }

    public void setEnvBenefits(EnvironmentBenefits environmentBenefits) {
        this.envBenefits = environmentBenefits;
    }
}
