package com.solaredge.common.models;

import android.net.Uri;
import android.text.TextUtils;
import com.solaredge.common.C1366a;
import com.solaredge.common.models.response.CreateUpdateSiteModule;
import com.solaredge.common.p116g.C1404g;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "updateSite", strict = false)
public class UpdateSite {
    @Element(name = "accountID", required = true)
    private long accountId;
    private Uri imageUri;
    @Element(name = "installationDate", required = true)
    private String installationDate;
    private Calendar installationDateCalendar;
    @Element(name = "location", required = true)
    private SiteLocation location;
    @Element(name = "name", required = true)
    private String name;
    @Element(name = "notes", required = false)
    private String notes;
    @Element(name = "peakPower", required = true)
    private float peakPower;
    @Element(name = "primaryModule", required = true)
    private CreateUpdateSiteModule primaryModule;

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public SiteLocation getLocation() {
        return this.location;
    }

    public void setLocation(SiteLocation siteLocation) {
        this.location = siteLocation;
        if (TextUtils.isEmpty(siteLocation.getState()) == null) {
            this.location.setState(C1404g.m3838a(this.location.getCountryCode(), this.location.getState(), C1366a.m3749a().m3753b()));
        }
    }

    public float getPeakPower() {
        return this.peakPower;
    }

    public void setPeakPower(float f) {
        this.peakPower = f;
    }

    public String getInstallationDate() {
        return this.installationDate;
    }

    public void setInstallationDate(String str) {
        this.installationDate = str;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String str) {
        this.notes = str;
    }

    public long getAccountId() {
        return this.accountId;
    }

    public void setAccountId(long j) {
        this.accountId = j;
    }

    public CreateUpdateSiteModule getPrimaryModule() {
        return this.primaryModule;
    }

    public void setPrimaryModule(CreateUpdateSiteModule createUpdateSiteModule) {
        this.primaryModule = createUpdateSiteModule;
    }

    public Calendar getInstallationDateCalendar() {
        return this.installationDateCalendar;
    }

    public void setInstallationDateCalendar(Calendar calendar) {
        if (this.installationDateCalendar == null) {
            this.installationDateCalendar = Calendar.getInstance(calendar.getTimeZone());
        }
        this.installationDateCalendar.setTimeInMillis(calendar.getTimeInMillis());
        this.installationDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(calendar.getTime());
    }

    public Uri getImageUri() {
        return this.imageUri;
    }

    public void setImageUri(Uri uri) {
        this.imageUri = uri;
    }
}
