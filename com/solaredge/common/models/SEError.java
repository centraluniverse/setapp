package com.solaredge.common.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ErrorResponse", strict = false)
public class SEError {
    @Element(name = "errorCode", required = false)
    private String errorCode;
    @Element(name = "errorMessage", required = false)
    private String errorMessage;
    @Element(name = "errorType", required = false)
    private String errorType;

    public SEError(String str, String str2, String str3) {
        this.errorCode = str;
        this.errorMessage = str2;
        this.errorType = str3;
    }

    public String getErrorType() {
        return this.errorType;
    }

    public void setErrorType(String str) {
        this.errorType = str;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String str) {
        this.errorCode = str;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }
}
