package com.solaredge.common.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.text.TextUtils;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "siteDetails", strict = false)
public class SolarSite implements Parcelable, C1370b {
    public static final Creator<SolarSite> CREATOR = ParcelableCompat.newCreator(new C22001());
    public static final String DEMO_TABLE_NAME = "Demo_SolarSites";
    public static final String FIELD_TYPE_SEI = "SEI";
    public static final String FIELD_TYPE_SMI = "SMI";
    public static final String REAL_TABLE_NAME = "SolarSites";
    private static final String SQL_CREATE_TABLE = "CREATE TABLE SolarSites( _id INTEGER PRIMARY KEY AUTOINCREMENT, site_id nvarchar(10), name nvarchar(60), field_type nvarchar(5), peak_pwoer number, tariff number, installation_date integer, notes nvarchar(300), field_image_url nvarchar(60), field_account_url nvarchar(60), map_last_modified integer, currency nvarchar(3), pm_manufacturer_name nvarchar(15), pm_model_name nvarchar(15), pm_maximum_power number, adr_country nvarchar(30), adr_state nvarchar(30), adr_city nvarchar(30), adr_zip_code nvarchar(30), adr_address_1 nvarchar(30), adr_address_2 nvarchar(10), adr_latitude number, adr_longitude number, adr_elevation number, adr_time_zone nvarchar(10), is_local number, last_modified_draft integer, last_modified_published integer, last_physical_map_update integer, account_id integer, account_name nvarchar(30) ); ";
    public static String TABLE_NAME = "SolarSites";
    private long _id;
    @Element(name = "account", required = false)
    private Account account;
    private long accountId;
    @Element(name = "currency", required = false)
    private String currency;
    @Element(name = "fieldAccountURL", required = false)
    private String fieldAccountUrl;
    @Element(name = "imageName", required = false)
    private String fieldImageUrl;
    @Element(name = "type", required = false)
    private String fieldType;
    @Element(name = "installationDate", required = false)
    private GregorianCalendar installationDate;
    private boolean isLocal = false;
    private boolean isTariffEnabled = false;
    @Element(name = "lastModifiedDraft", required = false)
    private GregorianCalendar lastModifiedDraft;
    @Element(name = "lastModifiedPublished", required = false)
    private GregorianCalendar lastModifiedPublished;
    private long lastPhysicalMapUpdate;
    @Element(name = "location", required = false)
    private Location location;
    @Element(name = "lastPhysicalMapUpdate", required = false)
    private GregorianCalendar mapLastModified;
    @Element(name = "name", required = false)
    private String name;
    @Element(name = "notes", required = false)
    private String notes;
    @Element(name = "peakPower", required = false)
    private float peakPower;
    @Element(name = "primaryModule", required = false)
    private ModuleResponse primaryModule;
    @Element(name = "id", required = false)
    private String siteId;
    @Element(name = "Tariff", required = false)
    private float tariff;

    public interface TableColumns extends BaseColumns {
        public static final String ACCOUNT_ID = "account_id";
        public static final String ACCOUNT_NAME = "account_name";
        public static final String ADDRESS_1 = "adr_address_1";
        public static final String ADDRESS_2 = "adr_address_2";
        public static final String CITY = "adr_city";
        public static final String COUNTRY = "adr_country";
        public static final String CURRENCY = "currency";
        public static final String ELEVATION = "adr_elevation";
        public static final String FIELD_ACCOUNT_URL = "field_account_url";
        public static final String FIELD_IMAGE_URL = "field_image_url";
        public static final String FIELD_TYPE = "field_type";
        public static final String INSTALLATION_DATE = "installation_date";
        public static final String IS_LOCAL = "is_local";
        public static final String LAST_MODIFIED_DRAFT = "last_modified_draft";
        public static final String LAST_MODIFIED_PUBLISHED = "last_modified_published";
        public static final String LAST_PHYSICAL_MAP_UPDATE = "last_physical_map_update";
        public static final String LATITUDE = "adr_latitude";
        public static final String LONGITUDE = "adr_longitude";
        public static final String MAP_LAST_MODIFIED = "map_last_modified";
        public static final String NAME = "name";
        public static final String NOTES = "notes";
        public static final String PEAK_POWER = "peak_pwoer";
        public static final String PM_MANUFACTURER_NAME = "pm_manufacturer_name";
        public static final String PM_MAXIMUM_POWER = "pm_maximum_power";
        public static final String PM_MODEL_NAME = "pm_model_name";
        public static final String SITE_ID = "site_id";
        public static final String STATE = "adr_state";
        public static final String TARIFF = "tariff";
        public static final String TIME_ZONE = "adr_time_zone";
        public static final String ZIP_CODE = "adr_zip_code";
    }

    static class C22001 implements ParcelableCompatCreatorCallbacks<SolarSite> {
        C22001() {
        }

        public SolarSite createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new SolarSite(parcel);
        }

        public SolarSite[] newArray(int i) {
            return new SolarSite[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public static String[] getAddFieldsStatements() {
        String[] strArr = new String[4];
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ALTER TABLE ");
        stringBuilder.append(TABLE_NAME);
        stringBuilder.append(" ADD COLUMN ");
        stringBuilder.append("account_id");
        stringBuilder.append(" INTEGER;");
        strArr[0] = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append("ALTER TABLE ");
        stringBuilder.append(TABLE_NAME);
        stringBuilder.append(" ADD COLUMN ");
        stringBuilder.append(TableColumns.ACCOUNT_NAME);
        stringBuilder.append(" nvarchar(30);");
        strArr[1] = stringBuilder.toString();
        strArr[2] = "ALTER TABLE Demo_SolarSites ADD COLUMN account_id INTEGER;";
        strArr[3] = "ALTER TABLE Demo_SolarSites ADD COLUMN account_name nvarchar(30);";
        return strArr;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATE_TABLE, getInitDemoDbStatement()};
    }

    public static String getInitDemoDbStatement() {
        return SQL_CREATE_TABLE.replace(REAL_TABLE_NAME, DEMO_TABLE_NAME);
    }

    public SolarSite(CreateSite createSite) {
        this.name = createSite.getName();
        this.fieldType = createSite.getType();
        this.peakPower = createSite.getPeakPower();
        this.tariff = createSite.getTariff();
        setAccountId(createSite.getAccountId());
        this.installationDate = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        if (createSite.getInstallationDateCalendar() != null) {
            this.installationDate.setTimeInMillis(createSite.getInstallationDateCalendar().getTimeInMillis());
        }
        this.notes = createSite.getNotes();
        this.mapLastModified = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        this.currency = createSite.getCurrencyCode();
        if (createSite.getImageUri() != null) {
            this.fieldImageUrl = createSite.getImageUri().toString();
        }
        this.location = new Location(createSite);
        this.primaryModule = new ModuleResponse(createSite);
        this.lastPhysicalMapUpdate = 0;
    }

    public SolarSite(UpdateSite updateSite) {
        this.name = updateSite.getName();
        this.peakPower = updateSite.getPeakPower();
        this.installationDate = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        if (updateSite.getInstallationDateCalendar() != null) {
            this.installationDate.setTimeInMillis(updateSite.getInstallationDateCalendar().getTimeInMillis());
        }
        setAccountId(updateSite.getAccountId());
        this.notes = updateSite.getNotes();
        this.mapLastModified = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        if (updateSite.getImageUri() != null) {
            this.fieldImageUrl = updateSite.getImageUri().toString();
        }
        this.location = new Location(updateSite);
        this.primaryModule = new ModuleResponse(updateSite);
        this.lastPhysicalMapUpdate = 0;
    }

    public SolarSite(Cursor cursor) {
        boolean z = false;
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.siteId = cursor.getString(cursor.getColumnIndex("site_id"));
        this.name = cursor.getString(cursor.getColumnIndex("name"));
        this.fieldType = cursor.getString(cursor.getColumnIndex("field_type"));
        this.peakPower = cursor.getFloat(cursor.getColumnIndex(TableColumns.PEAK_POWER));
        this.tariff = cursor.getFloat(cursor.getColumnIndex(TableColumns.TARIFF));
        this.installationDate = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        this.installationDate.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(TableColumns.INSTALLATION_DATE)));
        this.notes = cursor.getString(cursor.getColumnIndex(TableColumns.NOTES));
        this.fieldImageUrl = cursor.getString(cursor.getColumnIndex(TableColumns.FIELD_IMAGE_URL));
        this.fieldAccountUrl = cursor.getString(cursor.getColumnIndex(TableColumns.FIELD_ACCOUNT_URL));
        long j = cursor.getLong(cursor.getColumnIndex(TableColumns.MAP_LAST_MODIFIED));
        if (j > 0) {
            this.mapLastModified = new GregorianCalendar();
            this.mapLastModified.setTimeInMillis(j);
        } else {
            this.mapLastModified = null;
        }
        this.currency = cursor.getString(cursor.getColumnIndex(TableColumns.CURRENCY));
        this.location = new Location();
        this.location.setAddressLine1(cursor.getString(cursor.getColumnIndex(TableColumns.ADDRESS_1)));
        this.location.setAddressLine2(cursor.getString(cursor.getColumnIndex(TableColumns.ADDRESS_2)));
        this.location.setCountry(cursor.getString(cursor.getColumnIndex(TableColumns.COUNTRY)));
        this.location.setState(cursor.getString(cursor.getColumnIndex(TableColumns.STATE)));
        this.location.setCity(cursor.getString(cursor.getColumnIndex(TableColumns.CITY)));
        this.location.setZipCode(cursor.getString(cursor.getColumnIndex(TableColumns.ZIP_CODE)));
        this.location.setElevation(cursor.getDouble(cursor.getColumnIndex(TableColumns.ELEVATION)));
        this.location.setTimeZone(cursor.getString(cursor.getColumnIndex(TableColumns.TIME_ZONE)));
        this.location.setLatitude(cursor.getDouble(cursor.getColumnIndex(TableColumns.LATITUDE)));
        this.location.setLongitude(cursor.getDouble(cursor.getColumnIndex(TableColumns.LONGITUDE)));
        this.primaryModule = new ModuleResponse();
        this.primaryModule.setManufacturerName(cursor.getString(cursor.getColumnIndex(TableColumns.PM_MANUFACTURER_NAME)));
        this.primaryModule.setModelName(cursor.getString(cursor.getColumnIndex(TableColumns.PM_MODEL_NAME)));
        this.primaryModule.setMaximumPower(cursor.getFloat(cursor.getColumnIndex(TableColumns.PM_MAXIMUM_POWER)));
        if (cursor.getInt(cursor.getColumnIndex(TableColumns.IS_LOCAL)) != 0) {
            z = true;
        }
        this.isLocal = z;
        long j2 = cursor.getLong(cursor.getColumnIndex(TableColumns.LAST_MODIFIED_DRAFT));
        if (j2 > 0) {
            this.lastModifiedDraft = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
            this.lastModifiedDraft.setTimeInMillis(j2);
        } else {
            this.lastModifiedDraft = null;
        }
        j2 = cursor.getLong(cursor.getColumnIndex(TableColumns.LAST_MODIFIED_PUBLISHED));
        if (j2 > 0) {
            this.lastModifiedPublished = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
            this.lastModifiedPublished.setTimeInMillis(j2);
        } else {
            this.lastModifiedPublished = null;
        }
        int columnIndex = cursor.getColumnIndex(TableColumns.LAST_PHYSICAL_MAP_UPDATE);
        if (columnIndex > 0) {
            this.lastPhysicalMapUpdate = cursor.getLong(columnIndex);
        }
        this.account = new Account();
        this.account.setAccountId((long) cursor.getInt(cursor.getColumnIndex("account_id")));
        this.account.setName(cursor.getString(cursor.getColumnIndex(TableColumns.ACCOUNT_NAME)));
        setAccountId(this.account.getAccountId());
    }

    protected SolarSite(Parcel parcel) {
        boolean z = false;
        this._id = parcel.readLong();
        this.siteId = parcel.readString();
        this.name = parcel.readString();
        this.fieldType = parcel.readString();
        this.peakPower = parcel.readFloat();
        this.tariff = parcel.readFloat();
        this.installationDate = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        this.installationDate.setTimeInMillis(parcel.readLong());
        if (this.installationDate.getTimeInMillis() == 0) {
            this.installationDate = null;
        }
        this.notes = parcel.readString();
        this.fieldImageUrl = parcel.readString();
        this.fieldAccountUrl = parcel.readString();
        this.mapLastModified = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        this.mapLastModified.setTimeInMillis(parcel.readLong());
        this.currency = parcel.readString();
        this.location = (Location) parcel.readParcelable(Location.class.getClassLoader());
        this.primaryModule = (ModuleResponse) parcel.readParcelable(ModuleResponse.class.getClassLoader());
        if (parcel.readInt() != 0) {
            z = true;
        }
        this.isLocal = z;
        this.lastModifiedDraft = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        this.lastModifiedDraft.setTimeInMillis(parcel.readLong());
        if (this.lastModifiedDraft.getTimeInMillis() == 0) {
            this.lastModifiedDraft = null;
        }
        this.lastModifiedPublished = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        this.lastModifiedPublished.setTimeInMillis(parcel.readLong());
        if (this.lastModifiedPublished.getTimeInMillis() == 0) {
            this.lastModifiedPublished = null;
        }
        this.lastPhysicalMapUpdate = parcel.readLong();
        this.account = (Account) parcel.readParcelable(Account.class.getClassLoader());
        this.accountId = parcel.readLong();
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("site_id", this.siteId);
        contentValues.put("name", this.name);
        contentValues.put("field_type", this.fieldType);
        contentValues.put(TableColumns.PEAK_POWER, Float.valueOf(this.peakPower));
        contentValues.put(TableColumns.TARIFF, Float.valueOf(this.tariff));
        if (this.installationDate != null) {
            contentValues.put(TableColumns.INSTALLATION_DATE, Long.valueOf(this.installationDate.getTimeInMillis()));
        }
        contentValues.put(TableColumns.NOTES, this.notes);
        contentValues.put(TableColumns.FIELD_IMAGE_URL, this.fieldImageUrl);
        contentValues.put(TableColumns.FIELD_ACCOUNT_URL, this.fieldAccountUrl);
        if (this.mapLastModified != null) {
            contentValues.put(TableColumns.MAP_LAST_MODIFIED, Long.valueOf(this.mapLastModified.getTimeInMillis()));
        }
        contentValues.put(TableColumns.CURRENCY, this.currency);
        if (this.primaryModule != null) {
            contentValues.put(TableColumns.PM_MANUFACTURER_NAME, this.primaryModule.getManufacturerName());
            contentValues.put(TableColumns.PM_MODEL_NAME, this.primaryModule.getModelName());
            contentValues.put(TableColumns.PM_MAXIMUM_POWER, Float.valueOf(this.primaryModule.getMaximumPower()));
        }
        if (this.location != null) {
            contentValues.put(TableColumns.COUNTRY, this.location.getCountry());
            contentValues.put(TableColumns.STATE, this.location.getState());
            contentValues.put(TableColumns.CITY, this.location.getCity());
            contentValues.put(TableColumns.ZIP_CODE, this.location.getZipCode());
            contentValues.put(TableColumns.ADDRESS_1, this.location.getAddressLine1());
            contentValues.put(TableColumns.ADDRESS_2, this.location.getAddressLine2());
            contentValues.put(TableColumns.LATITUDE, Double.valueOf(this.location.getLatitude()));
            contentValues.put(TableColumns.LONGITUDE, Double.valueOf(this.location.getLongitude()));
            contentValues.put(TableColumns.ELEVATION, Double.valueOf(this.location.getElevation()));
            contentValues.put(TableColumns.TIME_ZONE, this.location.getTimeZone());
        }
        contentValues.put(TableColumns.IS_LOCAL, Boolean.valueOf(this.isLocal));
        if (this.lastModifiedDraft != null) {
            contentValues.put(TableColumns.LAST_MODIFIED_DRAFT, Long.valueOf(this.lastModifiedDraft.getTimeInMillis()));
        }
        if (this.lastModifiedPublished != null) {
            contentValues.put(TableColumns.LAST_MODIFIED_PUBLISHED, Long.valueOf(this.lastModifiedPublished.getTimeInMillis()));
        }
        contentValues.put(TableColumns.LAST_PHYSICAL_MAP_UPDATE, Long.valueOf(this.lastPhysicalMapUpdate));
        if (this.account == null || TextUtils.isEmpty(this.account.getName())) {
            contentValues.put("account_id", Long.valueOf(getAccountId()));
        } else {
            contentValues.put("account_id", Long.valueOf(this.account.getAccountId()));
            contentValues.put(TableColumns.ACCOUNT_NAME, this.account.getName());
        }
        return contentValues;
    }

    public long save(Context context) {
        if (isLocal()) {
            this._id = C1369a.m3765a(context).m3769a(this, false);
        } else {
            this._id = C1369a.m3765a(context).m3769a(this, true);
        }
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void update(SolarSite solarSite) {
        setName(solarSite.getName());
        setNotes(solarSite.getNotes());
        setPeakPower(solarSite.getPeakPower());
        this.location = solarSite.getLocation();
        setFieldAccountUrl(solarSite.getFieldAccountUrl());
        setFieldImageUrl(solarSite.getFieldImageUrl());
        setInstallationDate(solarSite.getInstallationDate());
        setPrimaryModule(solarSite.getPrimaryModule());
        setAccountId(solarSite.getAccountId());
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeString(this.siteId);
        parcel.writeString(this.name);
        parcel.writeString(this.fieldType);
        parcel.writeFloat(this.peakPower);
        parcel.writeFloat(this.tariff);
        long j = 0;
        if (this.installationDate != null) {
            parcel.writeLong(this.installationDate.getTimeInMillis());
        } else {
            parcel.writeLong(0);
        }
        parcel.writeString(this.notes);
        parcel.writeString(this.fieldImageUrl);
        parcel.writeString(this.fieldAccountUrl);
        parcel.writeLong(this.mapLastModified != null ? this.mapLastModified.getTimeInMillis() : 0);
        parcel.writeString(this.currency);
        parcel.writeParcelable(this.location, i);
        parcel.writeParcelable(this.primaryModule, i);
        parcel.writeInt(this.isLocal);
        parcel.writeLong(this.lastModifiedDraft != null ? this.lastModifiedDraft.getTimeInMillis() : 0);
        if (this.lastModifiedPublished != null) {
            j = this.lastModifiedPublished.getTimeInMillis();
        }
        parcel.writeLong(j);
        parcel.writeLong(this.lastPhysicalMapUpdate);
        parcel.writeParcelable(this.account, i);
        parcel.writeLong(this.accountId);
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public String getSiteId() {
        return this.siteId;
    }

    public void setSiteId(String str) {
        this.siteId = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getFieldType() {
        return this.fieldType;
    }

    public void setFieldType(String str) {
        this.fieldType = str;
    }

    public float getPeakPower() {
        return this.peakPower;
    }

    public void setPeakPower(float f) {
        this.peakPower = f;
    }

    public float getTariff() {
        return this.tariff;
    }

    public void setTariff(float f) {
        this.tariff = f;
    }

    public Calendar getInstallationDate() {
        return this.installationDate;
    }

    public void setInstallationDate(Calendar calendar) {
        this.installationDate = new GregorianCalendar(calendar.getTimeZone(), Locale.getDefault());
        this.installationDate.setTimeInMillis(calendar.getTimeInMillis());
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String str) {
        this.notes = str;
    }

    public String getFieldImageUrl() {
        return this.fieldImageUrl;
    }

    public void setFieldImageUrl(String str) {
        this.fieldImageUrl = str;
    }

    public String getFieldAccountUrl() {
        return this.fieldAccountUrl;
    }

    public void setFieldAccountUrl(String str) {
        this.fieldAccountUrl = str;
    }

    public Calendar getMapLastModified() {
        return getMapLastModified(true);
    }

    public Calendar getMapLastModified(boolean z) {
        if (z && this.mapLastModified) {
            return this.mapLastModified;
        }
        if (this.lastModifiedDraft && this.lastModifiedPublished) {
            if (this.lastModifiedDraft.getTimeInMillis() > this.lastModifiedPublished.getTimeInMillis()) {
                return this.lastModifiedDraft;
            }
            return this.lastModifiedPublished;
        } else if (this.lastModifiedDraft) {
            return this.lastModifiedDraft;
        } else {
            return this.lastModifiedPublished ? this.lastModifiedPublished : false;
        }
    }

    public void setMapLastModified(Calendar calendar) {
        this.mapLastModified = new GregorianCalendar(calendar.getTimeZone(), Locale.getDefault());
        this.mapLastModified.setTimeInMillis(calendar.getTimeInMillis());
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String str) {
        this.currency = str;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ModuleResponse getPrimaryModule() {
        return this.primaryModule;
    }

    public void setPrimaryModule(ModuleResponse moduleResponse) {
        this.primaryModule = moduleResponse;
    }

    public boolean isLocal() {
        return this.isLocal;
    }

    public void setLocal(boolean z) {
        this.isLocal = z;
    }

    public GregorianCalendar getLastModifiedDraft() {
        return this.lastModifiedDraft;
    }

    public void setLastModifiedDraft(GregorianCalendar gregorianCalendar) {
        this.lastModifiedDraft = gregorianCalendar;
    }

    public GregorianCalendar getLastModifiedPublished() {
        return this.lastModifiedPublished;
    }

    public void setLastModifiedPublished(GregorianCalendar gregorianCalendar) {
        this.lastModifiedPublished = gregorianCalendar;
    }

    public long getLastPhysicalMapUpdate() {
        return this.lastPhysicalMapUpdate;
    }

    public void setLastPhysicalMapUpdate(long j) {
        this.lastPhysicalMapUpdate = j;
    }

    public long getAccountId() {
        return this.accountId;
    }

    public void setAccountId(long j) {
        this.accountId = j;
    }

    public boolean isTariffEnabled() {
        return this.isTariffEnabled;
    }

    public void setTariffEnabled(boolean z) {
        this.isTariffEnabled = z;
    }

    public Account getAccount() {
        return this.account;
    }
}
