package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class TermsAndConditionsResponse {
    @C0631a
    @C0633c(a = "gracePeriodRemaningDays")
    private Integer gracePeriodRemaningDays;
    @C0631a
    @C0633c(a = "showBar")
    private Boolean showBar;
    @C0631a
    @C0633c(a = "showDialog")
    private Boolean showDialog;
    @C0631a
    @C0633c(a = "version")
    private String version;
    @C0631a
    @C0633c(a = "versionTS")
    private Long versionTS;

    public Boolean shouldShowBar() {
        return Boolean.valueOf(this.showBar == null ? false : this.showBar.booleanValue());
    }

    public Long getVersionTS() {
        return this.versionTS;
    }

    public String getVersion() {
        return this.version;
    }
}
