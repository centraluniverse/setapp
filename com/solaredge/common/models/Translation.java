package com.solaredge.common.models;

import android.provider.BaseColumns;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "property", strict = false)
public class Translation {
    private static final String SQL_CREATETE_TABLE = "CREATE TABLE Translations( _id INTEGER PRIMARY KEY AUTOINCREMENT, code nvarchar(10), key nvarchar(40), value nvarchar(50) ); ";
    public static final String TABLE_NAME = "Translations";
    private long _id;
    private String code;
    @Attribute(name = "name", required = false)
    private String key;
    @Attribute(name = "value", required = false)
    private String value;

    public interface TableColumns extends BaseColumns {
        public static final String CODE = "code";
        public static final String KEY = "key";
        public static final String VALUE = "value";
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String str) {
        this.code = str;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String str) {
        this.value = str;
    }
}
