package com.solaredge.common.models.layout;

public enum LayoutDataType {
    METER("METER"),
    INVERTER("INVERTER"),
    INVERTER_3PHASE("INVERTER_3PHASE"),
    INVERTER_3RDPARTY("INVERTER_3RDPARTY"),
    INVERTER_3PHASE_3RDPARTY("INVERTER_3PHASE_3RDPARTY"),
    INVERTER_3RDPARTY_DUMMY("INVERTER_3RDPARTY_DUMMY"),
    STRING("STRING"),
    POWER_BOX("POWER_BOX");
    
    private final String type;

    private LayoutDataType(String str) {
        this.type = str;
    }

    public boolean equals(String str) {
        return (this.type == null || this.type.equalsIgnoreCase(str) == null) ? null : true;
    }

    public String toString() {
        return this.type;
    }
}
