package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.ArrayList;
import java.util.List;

public class LayoutEnergyRequest {
    @C0631a
    @C0633c(a = "reporterIds")
    private List<Long> reporterIds = new ArrayList();

    public LayoutEnergyRequest(List<Long> list) {
        this.reporterIds = list;
    }
}
