package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class ComponentEnergy {
    @C0631a
    @C0633c(a = "color")
    private String color;
    @C0631a
    @C0633c(a = "energy")
    private float energy;
    @C0631a
    @C0633c(a = "groupColor")
    private String groupColor;
    @C0631a
    @C0633c(a = "moduleEnergy")
    private Float moduleEnergy;
    @C0631a
    @C0633c(a = "units")
    private String units;

    public float getEnergy() {
        return this.energy;
    }

    public void setEnergy(float f) {
        this.energy = f;
    }

    public String getUnits() {
        return this.units;
    }

    public void setUnits(String str) {
        this.units = str;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String str) {
        this.color = str;
    }

    public String getGroupColor() {
        return this.groupColor;
    }

    public void setGroupColor(String str) {
        this.groupColor = str;
    }

    public Float getModuleEnergy() {
        return this.moduleEnergy;
    }
}
