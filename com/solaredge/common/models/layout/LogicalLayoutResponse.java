package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.List;
import java.util.Map;

public class LogicalLayoutResponse {
    @C0631a
    @C0633c(a = "hasPhysical")
    private Boolean mHasPhysical;
    @C0631a
    @C0633c(a = "hierarchyColors")
    private List<String> mHierarchyColors;
    @C0631a
    @C0633c(a = "expanded")
    private Boolean mIsExpanded;
    @C0631a
    @C0633c(a = "logicalTree")
    private LogicalTreeNode mLogicalTree;
    @C0631a
    @C0633c(a = "reportersData")
    private Map<Long, ComponentEnergy> mReportersData;
    @C0631a
    @C0633c(a = "siteId")
    private Long mSiteId;

    public Long getSiteId() {
        return this.mSiteId;
    }

    public void setSiteId(Long l) {
        this.mSiteId = l;
    }

    public Boolean isExpanded() {
        return this.mIsExpanded;
    }

    public void setExpanded(Boolean bool) {
        this.mIsExpanded = bool;
    }

    public Boolean getHasPhysical() {
        return this.mHasPhysical;
    }

    public void setHasPhysical(Boolean bool) {
        this.mHasPhysical = bool;
    }

    public LogicalTreeNode getLogicalTree() {
        return this.mLogicalTree;
    }

    public void setLogicalTree(LogicalTreeNode logicalTreeNode) {
        this.mLogicalTree = logicalTreeNode;
    }

    public Map<Long, ComponentEnergy> getReportersData() {
        return this.mReportersData;
    }

    public void setReportersData(Map<Long, ComponentEnergy> map) {
        this.mReportersData = map;
    }

    public List<String> getHierarchyColors() {
        return this.mHierarchyColors;
    }

    public void setHierarchyColors(List<String> list) {
        this.mHierarchyColors = list;
    }
}
