package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.models.map.Dimension;
import java.util.Map;

public class SiteDefaultDimensionResponse {
    @C0631a
    @C0633c(a = "dimensionMap")
    private Map<String, Dimension> mDimensionMap;
    @C0631a
    @C0633c(a = "hSpacing")
    private double mGroupHorizontalSpacing;
    @C0631a
    @C0633c(a = "vSpacing")
    private double mGroupVerticalSpacing;

    public double getGroupVerticalSpacing() {
        return this.mGroupVerticalSpacing;
    }

    public void setGroupVerticalSpacing(double d) {
        this.mGroupVerticalSpacing = d;
    }

    public double getGroupHorizontalSpacing() {
        return this.mGroupHorizontalSpacing;
    }

    public void setGroupHorizontalSpacing(double d) {
        this.mGroupHorizontalSpacing = d;
    }
}
