package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class BaseComponent {
    private ComponentEnergy mComponentEnergy;
    @C0631a
    @C0633c(a = "displayName")
    private String mDisplayName;
    @C0631a
    @C0633c(a = "id")
    private long mId;
    @C0631a
    @C0633c(a = "name")
    private String mName;
    @C0631a
    @C0633c(a = "relativeOrder")
    private long mRelativeOrder;
    @C0631a
    @C0633c(a = "serialNumber")
    private String mSerialNumber;
    @C0631a
    @C0633c(a = "type")
    private LayoutDataType mType;

    public BaseComponent(BaseComponent baseComponent) {
        this.mId = baseComponent.getId();
        this.mSerialNumber = baseComponent.getSerialNumber();
        this.mName = baseComponent.getName();
        this.mDisplayName = baseComponent.getDisplayName();
        this.mRelativeOrder = baseComponent.getRelativeOrder();
        if (baseComponent.getComponentEnergy() != null) {
            this.mComponentEnergy = baseComponent.getComponentEnergy();
        }
    }

    public BaseComponent(long j) {
        this.mComponentEnergy = new ComponentEnergy();
        this.mId = j;
    }

    public ComponentEnergy getComponentEnergy() {
        return this.mComponentEnergy;
    }

    public void setComponentEnergy(ComponentEnergy componentEnergy) {
        this.mComponentEnergy = componentEnergy;
    }

    public long getId() {
        return this.mId;
    }

    public String getSerialNumber() {
        return this.mSerialNumber;
    }

    public String getName() {
        return this.mName;
    }

    public String getDisplayName() {
        return this.mDisplayName;
    }

    public LayoutDataType getType() {
        return this.mType;
    }

    public void setDisplayName(String str) {
        this.mDisplayName = str;
    }

    public long getRelativeOrder() {
        return this.mRelativeOrder;
    }
}
