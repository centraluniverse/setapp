package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.ArrayList;
import java.util.List;

public class PhysicalLayoutResponse {
    @C0631a
    @C0633c(a = "groups")
    private List<PhysicalGroupResponse> mGroups = new ArrayList();
    @C0631a
    @C0633c(a = "inverters")
    private List<PhysicalInverterResponse> mInverters = new ArrayList();
    @C0631a
    @C0633c(a = "siteDimensions")
    private SiteDefaultDimensionResponse mSiteDefaultDimension;
    @C0631a
    @C0633c(a = "fieldId")
    private Long mSiteId;

    public Long getSiteId() {
        return this.mSiteId;
    }

    public void setSiteId(Long l) {
        this.mSiteId = l;
    }

    public SiteDefaultDimensionResponse getSiteDefaultDimension() {
        return this.mSiteDefaultDimension;
    }

    public void setSiteDefaultDimension(SiteDefaultDimensionResponse siteDefaultDimensionResponse) {
        this.mSiteDefaultDimension = siteDefaultDimensionResponse;
    }

    public List<PhysicalGroupResponse> getGroups() {
        return this.mGroups;
    }

    public List<PhysicalInverterResponse> getInverters() {
        return this.mInverters;
    }
}
