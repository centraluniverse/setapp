package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class PhysicalModuleResponse {
    @C0631a
    @C0633c(a = "column")
    private int mColumn;
    @C0631a
    @C0633c(a = "id")
    private long mId;
    @C0631a
    @C0633c(a = "inverterId")
    private long mInverterId;
    @C0631a
    @C0633c(a = "row")
    private int mRow;

    public void setId(long j) {
        this.mId = j;
    }

    public void setRow(int i) {
        this.mRow = i;
    }

    public void setColumn(int i) {
        this.mColumn = i;
    }

    public long getId() {
        return this.mId;
    }

    public int getRow() {
        return this.mRow;
    }

    public int getColumn() {
        return this.mColumn;
    }

    public long getInverterId() {
        return this.mInverterId;
    }
}
