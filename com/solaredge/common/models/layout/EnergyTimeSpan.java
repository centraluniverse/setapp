package com.solaredge.common.models.layout;

import com.solaredge.common.p114e.C1382c;

public enum EnergyTimeSpan {
    DAY("DAY"),
    WEEK("WEEK"),
    MONTH("MONTH"),
    YEAR("YEAR"),
    ALL("ALL");
    
    private final String energyTimeSpan;

    private EnergyTimeSpan(String str) {
        this.energyTimeSpan = str;
    }

    public String toString() {
        return this.energyTimeSpan;
    }

    public String toPrettyString() {
        if (DAY.toString().equals(this.energyTimeSpan)) {
            return C1382c.m3782a().m3783a("API_LAYOUT_DAILY");
        }
        if (WEEK.toString().equalsIgnoreCase(this.energyTimeSpan)) {
            return C1382c.m3782a().m3783a("API_LAYOUT_WEEKLY");
        }
        if (MONTH.toString().equalsIgnoreCase(this.energyTimeSpan)) {
            return C1382c.m3782a().m3783a("API_LAYOUT_MONTHLY");
        }
        if (YEAR.toString().equalsIgnoreCase(this.energyTimeSpan)) {
            return C1382c.m3782a().m3783a("API_LAYOUT_YEARLY");
        }
        return ALL.toString().equalsIgnoreCase(this.energyTimeSpan) ? C1382c.m3782a().m3783a("API_LAYOUT_TOTAL") : "";
    }
}
