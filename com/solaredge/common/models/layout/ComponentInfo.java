package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.Map;

public class ComponentInfo {
    @C0631a
    @C0633c(a = "lastMeasurement")
    public String lastMeasurement;
    @C0631a
    @C0633c(a = "localizedMeasurements")
    public Map<String, String> localizedMeasurements;
    @C0631a
    @C0633c(a = "localizedPhase1Measurements")
    public Map<String, String> localizedPhase1Measurements;
    @C0631a
    @C0633c(a = "localizedPhase2Measurements")
    public Map<String, String> localizedPhase2Measurements;
    @C0631a
    @C0633c(a = "localizedPhase3Measurements")
    public Map<String, String> localizedPhase3Measurements;
    @C0631a
    @C0633c(a = "manufacturer")
    public String manufacturer;
    @C0631a
    @C0633c(a = "model")
    public String model;
    @C0631a
    @C0633c(a = "name")
    public String name;
    @C0631a
    @C0633c(a = "serialNumber")
    public String serialNumber;
}
