package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.models.map.Rectangle;

public class PhysicalInverterResponse {
    @C0631a
    @C0633c(a = "id")
    private long mId;
    @C0631a
    @C0633c(a = "rectangle")
    private Rectangle mRectangle;

    public PhysicalInverterResponse() {
        this.mId = -1;
        this.mId = -1;
        this.mRectangle = new Rectangle();
    }

    public long getId() {
        return this.mId;
    }

    public void setId(long j) {
        this.mId = j;
    }

    public Rectangle getRectangle() {
        return this.mRectangle;
    }

    public void setRect(Rectangle rectangle) {
        this.mRectangle = rectangle;
    }

    public boolean equals(Object obj) {
        if ((obj instanceof PhysicalInverterResponse) && ((PhysicalInverterResponse) obj).getId() == getId()) {
            return true;
        }
        return false;
    }
}
