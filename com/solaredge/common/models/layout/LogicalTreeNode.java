package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.List;

public class LogicalTreeNode {
    @C0631a
    @C0633c(a = "childIds")
    private List<Long> mChildIds;
    @C0631a
    @C0633c(a = "children")
    private List<LogicalTreeNode> mChildren;
    @C0631a
    @C0633c(a = "data")
    private BaseComponent mData;
    @C0631a
    @C0633c(a = "numberOfChilds")
    private int mNumberOfChilds;

    public BaseComponent getData() {
        return this.mData;
    }

    public void setData(BaseComponent baseComponent) {
        this.mData = baseComponent;
    }

    public List<LogicalTreeNode> getChildren() {
        return this.mChildren;
    }

    public void setChildren(List<LogicalTreeNode> list) {
        this.mChildren = list;
    }

    public int getChildIdsSize() {
        return this.mChildIds == null ? 0 : this.mChildIds.size();
    }

    public List<Long> getChildIds() {
        return this.mChildIds;
    }

    public void setChildIds(List<Long> list) {
        this.mChildIds = list;
    }

    public int getNumberOfChilds() {
        return this.mNumberOfChilds;
    }

    public void setNumberOfChilds(int i) {
        this.mNumberOfChilds = i;
    }
}
