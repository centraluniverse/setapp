package com.solaredge.common.models.layout;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.models.map.Rectangle;
import java.util.ArrayList;
import java.util.List;

public class PhysicalGroupResponse {
    @C0631a
    @C0633c(a = "columns")
    private int mColumns;
    @C0631a
    @C0633c(a = "hSpacing")
    private double mHorizontalSpacing;
    @C0631a
    @C0633c(a = "id")
    private Long mId;
    @C0631a
    @C0633c(a = "invertersIds")
    private List<Long> mInvertersIds = new ArrayList();
    @C0631a
    @C0633c(a = "moduleHeight")
    private double mModuleHeight;
    @C0631a
    @C0633c(a = "moduleOrientation")
    private ModuleOrientation mModuleOrientation;
    @C0631a
    @C0633c(a = "moduleTilt")
    private double mModuleTilt;
    @C0631a
    @C0633c(a = "moduleWidth")
    private double mModuleWidth;
    @C0631a
    @C0633c(a = "modules")
    private List<PhysicalModuleResponse> mModules;
    @C0631a
    @C0633c(a = "numOfOptimizers")
    private int mNumOfOptimizers;
    @C0631a
    @C0633c(a = "rectangle")
    private Rectangle mRectangle;
    @C0631a
    @C0633c(a = "rows")
    private int mRows;
    @C0631a
    @C0633c(a = "vSpacing")
    private double mVerticalSpacing;

    public enum ModuleOrientation {
        HORIZONTAL("HORIZONTAL"),
        VERTICAL("VERTICAL");
        
        private String orientation;

        private ModuleOrientation(String str) {
            this.orientation = str;
        }

        public String getOrientation() {
            return this.orientation;
        }
    }

    public Long getId() {
        return this.mId;
    }

    public void setId(Long l) {
        this.mId = l;
    }

    public Rectangle getRectangle() {
        return this.mRectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.mRectangle = rectangle;
    }

    public ModuleOrientation getModuleOrientation() {
        return this.mModuleOrientation;
    }

    public void setModuleOrientation(ModuleOrientation moduleOrientation) {
        this.mModuleOrientation = moduleOrientation;
    }

    public double getModuleTilt() {
        return this.mModuleTilt;
    }

    public void setModuleTilt(double d) {
        this.mModuleTilt = d;
    }

    public double getModuleWidth() {
        return this.mModuleWidth;
    }

    public void setModuleWidth(double d) {
        this.mModuleWidth = d;
    }

    public double getModuleHeight() {
        return this.mModuleHeight;
    }

    public void setModuleHeight(double d) {
        this.mModuleHeight = d;
    }

    public double getVerticalSpacing() {
        return this.mVerticalSpacing;
    }

    public void setVerticalSpacing(double d) {
        this.mVerticalSpacing = d;
    }

    public double getHorizontalSpacing() {
        return this.mHorizontalSpacing;
    }

    public void setHorizontalSpacing(double d) {
        this.mHorizontalSpacing = d;
    }

    public int getRows() {
        return this.mRows;
    }

    public void setRows(int i) {
        this.mRows = i;
    }

    public int getColumns() {
        return this.mColumns;
    }

    public void setColumns(int i) {
        this.mColumns = i;
    }

    public List<PhysicalModuleResponse> getModules() {
        return this.mModules;
    }

    public void setModules(List<PhysicalModuleResponse> list) {
        this.mModules = list;
    }

    public List<Long> getInvertersIds() {
        return this.mInvertersIds;
    }

    public void setInvertersIds(List<Long> list) {
        this.mInvertersIds = list;
    }

    public int getNumOfOptimizers() {
        return this.mNumOfOptimizers;
    }

    public void setNumOfOptimizers(int i) {
        this.mNumOfOptimizers = i;
    }
}
