package com.solaredge.common.models;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.solaredge.common.models.fieldOverview.FieldsData;
import com.solaredge.common.models.fieldOverview.LocationJson;
import com.solaredge.common.models.response.FieldOverviewResponse;
import com.solaredge.common.models.response.RevenueResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "solarField", strict = false)
public class SolarField implements Parcelable {
    public static final Creator<SolarField> CREATOR = new C14151();
    public static final String LOW_COST_SITE = "LOW_COST";
    public static final String TABLE_NAME = "SolarFields";
    public static final Uri URI_FIELDS_LIST = Uri.parse("sqlite://com.solaredge.monitor/fields/list");
    private long _id;
    @Element(name = "city", required = false)
    @Path("Address")
    private String addressCity;
    @Element(name = "country", required = false)
    @Path("Whereabouts")
    private String addressCountry;
    @Element(name = "detailedAddress", required = false)
    @Path("Address")
    private String addressDetailed;
    @Element(name = "state", required = false)
    @Path("Whereabouts")
    private String addressState;
    @Element(name = "zipCode", required = false)
    @Path("Address")
    private String addressZipCode;
    private String evChargerUrl = null;
    @Element(name = "imageItemName", required = false)
    private String imageName;
    @Element(name = "installationType", required = false)
    private String installationType;
    @Element(name = "isLowCost", required = false)
    private boolean isLowCost;
    private float lastDayEnergyProduction;
    private float lastMonthEnergyProduction;
    private long lastUpdateTime;
    private float lastYearEnergyProduction;
    private String layoutUrl = null;
    private float lifetimeEnergyProduction;
    private RevenueResponse lifetimeRevenue;
    private String loadDevicesUrl = null;
    @Element(name = "location", required = false)
    private Location location;
    private float mCurrentPower;
    private Calendar mDataSpanEnd;
    private Calendar mDataSpanStart;
    @Element(name = "name", required = false)
    private String name;
    private String newLifetimeRevenue;
    @Element(name = "peakPower", required = false)
    private float peakPower;
    private String powerFlowUrl = null;
    @Element(name = "id", required = false)
    private long siteId;
    private String siteType;
    @Element(name = "status", required = false)
    private String status;

    static class C14151 implements Creator<SolarField> {
        C14151() {
        }

        public SolarField createFromParcel(Parcel parcel) {
            return new SolarField(parcel);
        }

        public SolarField[] newArray(int i) {
            return new SolarField[i];
        }
    }

    public String getInstallationType() {
        return this.installationType;
    }

    private void setInstallationType(String str) {
        this.installationType = str;
    }

    public boolean isLowCost() {
        return this.isLowCost;
    }

    public void setLowCost(boolean z) {
        this.isLowCost = z;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public SolarField(String str, String str2, float f, String str3, String str4, String str5, String str6, String str7, String str8) {
        this.status = str;
        this.name = str2;
        this.peakPower = f;
        this.imageName = str3;
        this.addressCountry = str4;
        this.addressState = str5;
        this.addressCity = str6;
        this.addressZipCode = str7;
        this.addressDetailed = str8;
    }

    public SolarField(Parcel parcel) {
        this._id = parcel.readLong();
        this.siteId = parcel.readLong();
        this.status = parcel.readString();
        this.name = parcel.readString();
        this.peakPower = parcel.readFloat();
        this.lastUpdateTime = parcel.readLong();
        this.imageName = parcel.readString();
        this.isLowCost = parcel.readInt() != 0;
        this.addressCountry = parcel.readString();
        this.addressState = parcel.readString();
        this.addressCity = parcel.readString();
        this.addressZipCode = parcel.readString();
        this.addressDetailed = parcel.readString();
        this.layoutUrl = parcel.readString();
        this.powerFlowUrl = parcel.readString();
        this.loadDevicesUrl = parcel.readString();
        this.evChargerUrl = parcel.readString();
        this.lifetimeEnergyProduction = parcel.readFloat();
        this.lastYearEnergyProduction = parcel.readFloat();
        this.lastMonthEnergyProduction = parcel.readFloat();
        this.lastDayEnergyProduction = parcel.readFloat();
        this.location = (Location) parcel.readParcelable(Location.class.getClassLoader());
        this.siteType = parcel.readString();
    }

    public void update(FieldOverviewResponse fieldOverviewResponse) {
        if (!TextUtils.isEmpty(fieldOverviewResponse.getLastUpdateTime())) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
            Calendar instance = Calendar.getInstance();
            try {
                instance.setTime(simpleDateFormat.parse(fieldOverviewResponse.getLastUpdateTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            setLastUpdateTime(instance.getTimeInMillis());
        }
        setLifetimeEnergyProduction(fieldOverviewResponse.getLifeTimeEnergy());
        setLastYearEnergyProduction(fieldOverviewResponse.getLastYearEnergy());
        setLastMonthEnergyProduction(fieldOverviewResponse.getLastMonthEnergy());
        setLastDayEnergyProduction(fieldOverviewResponse.getLastDayEnergy());
        setName(fieldOverviewResponse.getSolarField().getName());
        setPeakPower(fieldOverviewResponse.getSolarField().getPeakPower());
        setCurrentPower(fieldOverviewResponse.getCurrentPower());
        setLocation(fieldOverviewResponse.getSolarField().getLocation());
        setLifetimeRevenue(fieldOverviewResponse.getLifetimeRevenue());
        setLowCost(fieldOverviewResponse.getSolarField().isLowCost());
        setInstallationType(fieldOverviewResponse.getSolarField().getInstallationType());
        for (SEUri sEUri : fieldOverviewResponse.getSeUris()) {
            if (sEUri.getType().equalsIgnoreCase("layout")) {
                setLayoutUrl(sEUri.getValue());
            } else if (sEUri.getType().equalsIgnoreCase("currentPowerFlow")) {
                setPowerFlowUrl(sEUri.getValue());
            } else if (sEUri.getType().equalsIgnoreCase("loadDevices")) {
                setLoadDevices(sEUri.getValue());
            } else if (sEUri.getType().equalsIgnoreCase("evChargers")) {
                setEvChargers(sEUri.getValue());
            }
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.siteId);
        parcel.writeString(this.status);
        parcel.writeString(this.name);
        parcel.writeFloat(this.peakPower);
        parcel.writeLong(this.lastUpdateTime);
        parcel.writeString(this.imageName);
        parcel.writeInt(this.isLowCost);
        parcel.writeString(this.addressCountry);
        parcel.writeString(this.addressState);
        parcel.writeString(this.addressCity);
        parcel.writeString(this.addressZipCode);
        parcel.writeString(this.addressDetailed);
        parcel.writeString(this.layoutUrl);
        parcel.writeString(this.powerFlowUrl);
        parcel.writeString(this.loadDevicesUrl);
        parcel.writeString(this.evChargerUrl);
        parcel.writeFloat(this.lifetimeEnergyProduction);
        parcel.writeFloat(this.lastYearEnergyProduction);
        parcel.writeFloat(this.lastMonthEnergyProduction);
        parcel.writeFloat(this.lastDayEnergyProduction);
        if (this.location != null) {
            parcel.writeParcelable(this.location, i);
        }
        parcel.writeString(this.siteType);
    }

    public int describeContents() {
        return hashCode();
    }

    public long getSiteId() {
        return this.siteId;
    }

    public void setSiteId(long j) {
        this.siteId = j;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public float getPeakPower() {
        return this.peakPower;
    }

    public void setPeakPower(float f) {
        this.peakPower = f;
    }

    public String getImageName() {
        return this.imageName;
    }

    public void setImageName(String str) {
        this.imageName = str;
    }

    public String getAddressCountry() {
        return this.addressCountry;
    }

    public void setAddressCountry(String str) {
        this.addressCountry = str;
    }

    public String getAddressState() {
        return this.addressState;
    }

    public void setAddressState(String str) {
        this.addressState = str;
    }

    public String getAddressCity() {
        return this.addressCity;
    }

    public void setAddressCity(String str) {
        this.addressCity = str;
    }

    public String getAddressZipCode() {
        return this.addressZipCode;
    }

    public void setAddressZipCode(String str) {
        this.addressZipCode = str;
    }

    public String getAddressDetailed() {
        return this.addressDetailed;
    }

    public void setAddressDetailed(String str) {
        this.addressDetailed = str;
    }

    public long getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public void setLastUpdateTime(long j) {
        this.lastUpdateTime = j;
    }

    public float getLifetimeEnergyProduction() {
        return this.lifetimeEnergyProduction;
    }

    public void setLifetimeEnergyProduction(float f) {
        this.lifetimeEnergyProduction = f;
    }

    public float getLastYearEnergyProduction() {
        return this.lastYearEnergyProduction;
    }

    public void setLastYearEnergyProduction(float f) {
        this.lastYearEnergyProduction = f;
    }

    public float getLastMonthEnergyProduction() {
        return this.lastMonthEnergyProduction;
    }

    public void setLastMonthEnergyProduction(float f) {
        this.lastMonthEnergyProduction = f;
    }

    public float getLastDayEnergyProduction() {
        return this.lastDayEnergyProduction;
    }

    public void setLastDayEnergyProduction(float f) {
        this.lastDayEnergyProduction = f;
    }

    public Calendar getDataSpanStart() {
        return this.mDataSpanStart;
    }

    public void setDataSpanStart(Calendar calendar) {
        if (this.mDataSpanStart == null) {
            this.mDataSpanStart = Calendar.getInstance();
        }
        this.mDataSpanStart.setTimeInMillis(calendar.getTimeInMillis());
    }

    public Calendar getDataSpanEnd() {
        return this.mDataSpanEnd;
    }

    public void setDataSpanEnd(Calendar calendar) {
        if (this.mDataSpanEnd == null) {
            this.mDataSpanEnd = Calendar.getInstance();
        }
        this.mDataSpanEnd.setTimeInMillis(calendar.getTimeInMillis());
    }

    public float getCurrentPower() {
        return this.mCurrentPower;
    }

    public void setCurrentPower(float f) {
        this.mCurrentPower = f;
    }

    public String getLayoutUrl() {
        return this.layoutUrl;
    }

    public void setLayoutUrl(String str) {
        this.layoutUrl = str;
    }

    public RevenueResponse getLifetimeRevenue() {
        return this.lifetimeRevenue;
    }

    public void setLifetimeRevenue(RevenueResponse revenueResponse) {
        this.lifetimeRevenue = revenueResponse;
    }

    public String getPowerFlowUrl() {
        return this.powerFlowUrl;
    }

    public void setPowerFlowUrl(String str) {
        this.powerFlowUrl = str;
    }

    public void setLoadDevices(String str) {
        this.loadDevicesUrl = str;
    }

    public String getLoadDevices() {
        return this.loadDevicesUrl;
    }

    public String getEvChargerUrl() {
        return this.evChargerUrl;
    }

    public void setEvChargers(String str) {
        this.evChargerUrl = str;
    }

    public void update(com.solaredge.common.models.fieldOverview.FieldOverviewResponse fieldOverviewResponse) {
        FieldsData fieldOverviewData = fieldOverviewResponse.getFieldOverviewData().getFieldOverviewData();
        if (!TextUtils.isEmpty(fieldOverviewData.getLastUpdateTime())) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
            Calendar instance = Calendar.getInstance();
            try {
                instance.setTime(simpleDateFormat.parse(fieldOverviewData.getLastUpdateTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            setLastUpdateTime(instance.getTimeInMillis());
        }
        setSiteType(fieldOverviewResponse.getSiteClassType());
        setLifetimeEnergyProduction(fieldOverviewData.getLifeTimeData().getEnergy().floatValue());
        setLastYearEnergyProduction(fieldOverviewData.getLastYearData().getEnergy().floatValue());
        setLastMonthEnergyProduction(fieldOverviewData.getLastMonthData().getEnergy().floatValue());
        setLastDayEnergyProduction(fieldOverviewData.getLastDayData().getEnergy().floatValue());
        setName(fieldOverviewData.getSolarField().getName());
        setPeakPower(fieldOverviewData.getSolarField().getPeakPower().floatValue());
        setCurrentPower(fieldOverviewData.getCurrentPower().getCurrentPower().floatValue());
        setLocation(fieldOverviewData.getSolarField().getLocation());
        setNewLifetimeRevenue(fieldOverviewData.getLifeTimeData().getRevenue());
        setLowCost(LOW_COST_SITE.equalsIgnoreCase(getSiteType()));
        setInstallationType(fieldOverviewData.getSolarField().getFieldType());
        if (!TextUtils.isEmpty(fieldOverviewResponse.getFieldOverviewData().getUris().getCurrentPowerFlow())) {
            setPowerFlowUrl(fieldOverviewResponse.getFieldOverviewData().getUris().getCurrentPowerFlow());
        }
        if (!TextUtils.isEmpty(fieldOverviewResponse.getFieldOverviewData().getUris().getLayout())) {
            setLayoutUrl(fieldOverviewResponse.getFieldOverviewData().getUris().getLayout());
        }
        if (!TextUtils.isEmpty(fieldOverviewResponse.getFieldOverviewData().getUris().getEvChargers())) {
            setEvChargers(fieldOverviewResponse.getFieldOverviewData().getUris().getEvChargers());
        }
        if (!TextUtils.isEmpty(fieldOverviewResponse.getFieldOverviewData().getUris().getLoadDevices())) {
            setLoadDevices(fieldOverviewResponse.getFieldOverviewData().getUris().getLoadDevices());
        }
    }

    private void setLocation(LocationJson locationJson) {
        this.location = new Location(locationJson);
    }

    public void setNewLifetimeRevenue(String str) {
        this.newLifetimeRevenue = str;
    }

    public String getSiteType() {
        return this.siteType;
    }

    public void setSiteType(String str) {
        this.siteType = str;
    }

    public String getNewLifetimeRevenue() {
        return this.newLifetimeRevenue;
    }
}
