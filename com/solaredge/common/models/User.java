package com.solaredge.common.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "solarField", strict = false)
public class User {
    @Element(name = "account", required = false)
    private Account account;
    @Element(name = "email", required = false)
    private String email;
    @Element(name = "firstName", required = false)
    private String firstName;
    @Element(name = "lastName", required = false)
    private String lastName;
    @Element(name = "locale", required = false)
    private String locale;
    @Element(name = "si", required = false)
    private String si;

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String str) {
        this.firstName = str;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String str) {
        this.lastName = str;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String str) {
        this.email = str;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setLocale(String str) {
        this.locale = str;
    }

    public String getSi() {
        return this.si;
    }

    public void setSi(String str) {
        this.si = str;
    }

    public Account getAccount() {
        return this.account;
    }
}
