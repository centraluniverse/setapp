package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.solaredge.common.p114e.C1382c;
import java.util.ArrayList;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ValidationResultEntry implements Parcelable {
    public static final Creator<ValidationResultEntry> CREATOR = new C14161();
    @Element(name = "messageKey", required = false)
    private String messageKey;
    @ElementList(entry = "param", inline = true, required = false)
    private List<String> params;

    static class C14161 implements Creator<ValidationResultEntry> {
        C14161() {
        }

        public ValidationResultEntry createFromParcel(Parcel parcel) {
            return new ValidationResultEntry(parcel);
        }

        public ValidationResultEntry[] newArray(int i) {
            return new ValidationResultEntry[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getMessageKey() {
        return this.messageKey;
    }

    public void setMessageKey(String str) {
        this.messageKey = str;
    }

    public List<String> getParams() {
        return this.params;
    }

    public void setParams(List<String> list) {
        this.params = list;
    }

    protected ValidationResultEntry(Parcel parcel) {
        this.messageKey = parcel.readString();
        if (parcel.readByte() == (byte) 1) {
            this.params = new ArrayList();
            parcel.readList(this.params, String.class.getClassLoader());
            return;
        }
        this.params = null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.messageKey);
        if (this.params == 0) {
            parcel.writeByte(0);
            return;
        }
        parcel.writeByte(1);
        parcel.writeList(this.params);
    }

    public String getLocalizedMessage() {
        String a;
        C1382c a2;
        StringBuilder stringBuilder;
        if (getParams() == null || getParams().size() <= 0) {
            a2 = C1382c.m3782a();
            stringBuilder = new StringBuilder();
            stringBuilder.append("API_");
            stringBuilder.append(getMessageKey());
            a = a2.m3783a(stringBuilder.toString());
        } else {
            a2 = C1382c.m3782a();
            stringBuilder = new StringBuilder();
            stringBuilder.append("API_");
            stringBuilder.append(getMessageKey());
            a = a2.m3784a(stringBuilder.toString(), (String[]) getParams().toArray(new String[getParams().size()]));
        }
        return TextUtils.isEmpty(a) ? getMessageKey() : a;
    }
}
