package com.solaredge.common.models;

import android.content.ContentValues;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import java.util.GregorianCalendar;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "solarField", strict = false)
public class FieldEnergy implements Parcelable, C1370b {
    public static final Creator<SolarField> CREATOR = new C14131();
    private static final String SQL_CREATETE_TABLE = "CREATE TABLE FieldEnergy( _id INTEGER PRIMARY KEY AUTOINCREMENT, time_unit nvarchar(6), unit nvarchar(7), energy float, energy_date integer  ); ";
    public static final String TABLE_NAME = "FieldEnergy";
    private long _id;
    @Element(name = "date", required = false)
    private GregorianCalendar date;
    @Element(name = "value", required = false)
    private Float energy;
    private String timeUnit;
    private String unit;

    static class C14131 implements Creator<SolarField> {
        C14131() {
        }

        public SolarField createFromParcel(Parcel parcel) {
            return new SolarField(parcel);
        }

        public SolarField[] newArray(int i) {
            return new SolarField[i];
        }
    }

    public interface TableColumns extends BaseColumns {
        public static final String DATE = "energy_date";
        public static final String ENERGY = "energy";
        public static final String TIME_UNIT = "time_unit";
        public static final String UNIT = "unit";
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public FieldEnergy(String str, String str2, float f, GregorianCalendar gregorianCalendar) {
        this.timeUnit = str;
        this.unit = str2;
        this.energy = Float.valueOf(f);
        this.date.setTimeInMillis(gregorianCalendar.getTimeInMillis());
    }

    public FieldEnergy(Parcel parcel) {
        this._id = parcel.readLong();
        this.timeUnit = parcel.readString();
        this.unit = parcel.readString();
        this.energy = Float.valueOf(parcel.readFloat());
        this.date.setTimeInMillis(parcel.readLong());
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableColumns.TIME_UNIT, this.timeUnit);
        contentValues.put(TableColumns.UNIT, this.unit);
        contentValues.put(TableColumns.ENERGY, this.energy);
        contentValues.put(TableColumns.DATE, Long.valueOf(this.date.getTimeInMillis()));
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeString(this.timeUnit);
        parcel.writeString(this.unit);
        parcel.writeFloat(this.energy.floatValue());
        parcel.writeLong(this.date.getTimeInMillis());
    }

    public int describeContents() {
        return hashCode();
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public String getTimeUnit() {
        return this.timeUnit;
    }

    public void setTimeUnit(String str) {
        this.timeUnit = str;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String str) {
        this.unit = str;
    }

    public Float getEnergy() {
        return this.energy;
    }

    public void setEnergy(float f) {
        this.energy = Float.valueOf(f);
    }

    public GregorianCalendar getDate() {
        return this.date;
    }

    public void setDate(GregorianCalendar gregorianCalendar) {
        this.date.setTimeInMillis(gregorianCalendar.getTimeInMillis());
    }
}
