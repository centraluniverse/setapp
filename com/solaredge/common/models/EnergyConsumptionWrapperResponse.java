package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EnergyConsumptionWrapperResponse {
    @C0631a
    @C0633c(a = "EnergyConsumption")
    private EnergyConsumption energyConsumption;

    public EnergyConsumption getEnergyConsumption() {
        return this.energyConsumption;
    }

    public void setEnergyConsumption(EnergyConsumption energyConsumption) {
        this.energyConsumption = energyConsumption;
    }
}
