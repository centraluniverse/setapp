package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.models.evCharger.PowerFlowCharger;
import java.util.List;

public class SiteCurrentPowerFlow {
    @C0631a
    @C0633c(a = "connections")
    private List<PowerFlowConnection> connections;
    @C0631a
    @C0633c(a = "GRID")
    private PowerFlowElement gridElement;
    @C0631a
    @C0633c(a = "LOAD")
    private PowerFlowElement loadElement;
    @C0631a
    @C0633c(a = "loadType")
    private LoadType loadType;
    @C0631a
    @C0633c(a = "EVCHARGER")
    private PowerFlowCharger powerFLowCharger;
    @C0631a
    @C0633c(a = "PV")
    private PowerFlowElement pvElement;
    @C0631a
    @C0633c(a = "STORAGE")
    private PowerFlowElement storageElement;
    @C0631a
    @C0633c(a = "unit")
    private String unit;
    @C0631a
    @C0633c(a = "updateRefreshRate")
    private Long updateRefreshRate;

    public enum LoadType {
        Residential("Residential"),
        Commercial("Commercial");
        
        private final String loadType;

        private LoadType(String str) {
            this.loadType = str;
        }

        public boolean equalType(String str) {
            return (this.loadType == null || this.loadType.equalsIgnoreCase(str) == null) ? null : true;
        }

        public String toString() {
            return this.loadType;
        }
    }

    public LoadType getLoadType() {
        return this.loadType == null ? LoadType.Residential : this.loadType;
    }

    public PowerFlowElement getGridElement() {
        return this.gridElement;
    }

    public void setGridElement(PowerFlowElement powerFlowElement) {
        this.gridElement = powerFlowElement;
    }

    public PowerFlowElement getLoadElement() {
        return this.loadElement;
    }

    public void setLoadElement(PowerFlowElement powerFlowElement) {
        this.loadElement = powerFlowElement;
    }

    public PowerFlowElement getPvElement() {
        return this.pvElement;
    }

    public void setPvElement(PowerFlowElement powerFlowElement) {
        this.pvElement = powerFlowElement;
    }

    public PowerFlowElement getStorageElement() {
        return this.storageElement;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String str) {
        this.unit = str;
    }

    public void setStorageElement(PowerFlowElement powerFlowElement) {
        this.storageElement = powerFlowElement;
    }

    public PowerFlowCharger getPowerFLowCharger() {
        return this.powerFLowCharger;
    }

    public void setPowerFLowCharger(PowerFlowCharger powerFlowCharger) {
        this.powerFLowCharger = powerFlowCharger;
    }

    public List<PowerFlowConnection> getConnections() {
        return this.connections;
    }

    public void setConnections(List<PowerFlowConnection> list) {
        this.connections = list;
    }

    public Long getUpdateRefreshRate() {
        return this.updateRefreshRate;
    }
}
