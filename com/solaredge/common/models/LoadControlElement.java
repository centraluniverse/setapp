package com.solaredge.common.models;

import android.text.TextUtils;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.p116g.C1404g;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoadControlElement implements Serializable {
    @C0631a
    @C0633c(a = "config")
    private LoadDeviceConfig config = new LoadDeviceConfig();
    @C0631a
    @C0633c(a = "deviceStatus")
    private String deviceActiveStatus;
    @C0631a
    @C0633c(a = "excessPVConfiguration")
    private LoadDeviceExcessPVConfiguration deviceExcessPVConfiguration;
    @C0631a
    @C0633c(a = "name")
    private String deviceName;
    @C0631a
    @C0633c(a = "deviceTriggers")
    private List<LoadDeviceTrigger> deviceTriggers = new ArrayList();
    @C0631a
    @C0633c(a = "type")
    private String deviceType;
    @C0631a
    @C0633c(a = "iconName")
    private String iconName;
    @C0631a
    @C0633c(a = "lastUpdated")
    private String lastUpdated;
    private String loadDeviceBackgroundLayout;
    @C0631a
    @C0633c(a = "macAddress")
    private String macAddress;
    @C0631a
    @C0633c(a = "manufacturer")
    private String manufacturer;
    @C0631a
    @C0633c(a = "model")
    private String model;
    @C0631a
    @C0633c(a = "remoteOpInfo")
    private LoadRemoteOpInfo remoteOpInfo;
    @C0631a
    @C0633c(a = "reporterId")
    private Long reporterId;
    @C0631a
    @C0633c(a = "serialNumber")
    private String serialNumber;
    @C0631a
    @C0633c(a = "status")
    private DeviceStatus status = new DeviceStatus();
    @C0631a
    @C0633c(a = "swVersion")
    private String swVersion;
    @C0631a
    @C0633c(a = "totalEnergy")
    private Float totalEnergy;

    public enum DeviceType {
        UNDEFINED("undefined"),
        RELAY("relay"),
        SOCKET("socket"),
        LEVEL_CTRL("level");
        
        private final String type;

        private DeviceType(String str) {
            this.type = str;
        }

        public boolean equalType(String str) {
            return (this.type == null || this.type.equalsIgnoreCase(str) == null) ? null : true;
        }

        public String toString() {
            return this.type;
        }
    }

    public boolean isEqualState(LoadControlElement loadControlElement) {
        boolean z = false;
        if (loadControlElement == null) {
            return false;
        }
        if (TextUtils.equals(loadControlElement.getIconName(), getIconName()) && TextUtils.equals(loadControlElement.getDeviceName(), getDeviceName()) && TextUtils.equals(loadControlElement.getDeviceActiveStatus(), getDeviceActiveStatus()) && C1404g.m3842a(loadControlElement.getRemoteOpInfo(), getRemoteOpInfo()) && C1404g.m3842a(loadControlElement.getStatus(), getStatus()) && C1404g.m3842a(loadControlElement.getDeviceExcessPVConfiguration(), getDeviceExcessPVConfiguration()) && isEqualListOfTriggers(loadControlElement.getDeviceTriggers()) != null) {
            z = true;
        }
        return z;
    }

    public boolean isEqualPowerUsage(LoadControlElement loadControlElement) {
        if (loadControlElement == null) {
            return null;
        }
        if ((loadControlElement.getStatus() == null && getStatus() == null) || (loadControlElement.getStatus().getActivePowerMeter() == null && getStatus().getActivePowerMeter() == null)) {
            return true;
        }
        return C1404g.m3842a(loadControlElement.getStatus().getActivePowerMeter(), getStatus().getActivePowerMeter());
    }

    public void setTotalEnergy(Float f) {
        this.totalEnergy = f;
    }

    public String getLoadDeviceBackgroundLayout() {
        return this.loadDeviceBackgroundLayout;
    }

    public void setLoadDeviceBackgroundLayout(String str) {
        this.loadDeviceBackgroundLayout = str;
    }

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(String str) {
        this.serialNumber = str;
    }

    public String getIconName() {
        return this.iconName;
    }

    public void setIconName(String str) {
        this.iconName = str;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String str) {
        this.deviceName = str;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String str) {
        this.model = str;
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    public String getSwVersion() {
        return this.swVersion;
    }

    public void setSwVersion(String str) {
        this.swVersion = str;
    }

    public String getDeviceType() {
        return this.deviceType;
    }

    public void setDeviceType(String str) {
        this.deviceType = str;
    }

    public String getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(String str) {
        this.lastUpdated = str;
    }

    public float getTotalEnergy() {
        return this.totalEnergy.floatValue();
    }

    public void setTotalEnergy(float f) {
        this.totalEnergy = Float.valueOf(f);
    }

    public Long getReporterId() {
        return this.reporterId;
    }

    public void setReporterId(Long l) {
        this.reporterId = l;
    }

    public DeviceStatus getStatus() {
        return this.status;
    }

    public void setStatus(DeviceStatus deviceStatus) {
        this.status = deviceStatus;
    }

    public LoadDeviceConfig getConfig() {
        return this.config;
    }

    public void setConfig(LoadDeviceConfig loadDeviceConfig) {
        this.config = loadDeviceConfig;
    }

    public LoadRemoteOpInfo getRemoteOpInfo() {
        return this.remoteOpInfo;
    }

    public void setRemoteOpInfo(LoadRemoteOpInfo loadRemoteOpInfo) {
        this.remoteOpInfo = loadRemoteOpInfo;
    }

    public List<LoadDeviceTrigger> getDeviceTriggers() {
        return this.deviceTriggers;
    }

    public void setDeviceTriggers(List<LoadDeviceTrigger> list) {
        this.deviceTriggers = list;
    }

    public String getDeviceActiveStatus() {
        return this.deviceActiveStatus;
    }

    public void setDeviceActiveStatus(String str) {
        this.deviceActiveStatus = str;
    }

    public LoadDeviceExcessPVConfiguration getDeviceExcessPVConfiguration() {
        return this.deviceExcessPVConfiguration;
    }

    public void setDeviceExcessPVConfiguration(LoadDeviceExcessPVConfiguration loadDeviceExcessPVConfiguration) {
        this.deviceExcessPVConfiguration = loadDeviceExcessPVConfiguration;
    }

    public boolean isEqualListOfTriggers(List<LoadDeviceTrigger> list) {
        if (getDeviceTriggers().size() != list.size()) {
            return false;
        }
        for (int i = 0; i < getDeviceTriggers().size(); i++) {
            if (!C1404g.m3842a(getDeviceTriggers().get(i), list.get(i))) {
                return false;
            }
        }
        return true;
    }
}
