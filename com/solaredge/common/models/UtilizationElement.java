package com.solaredge.common.models;

public class UtilizationElement {
    public static final String BATTERY_SELF_CONSUMPTION = "batterySelfConsumption";
    public static final String CONSUMPTION = "consumption";
    public static final String EXPORT = "export";
    public static final String IMPORT = "import";
    public static final String PRODUCTION = "production";
    public static final String SELF_CONSUMPTION_FOR_CONSUMPTION = "selfConsumptionForConsumption";
    public static final String SELF_CONSUMPTION_FOR_PRODUCTION = "selfConsumptionForProduction";
    private Float percentage;
    private String unit;
    private Float value;

    public UtilizationElement(Float f, String str, Float f2) {
        this.value = f;
        this.unit = str;
        this.percentage = f2;
    }

    public Float getPercentage() {
        return this.percentage;
    }

    public void setPercentage(Float f) {
        this.percentage = f;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String str) {
        this.unit = str;
    }

    public Float getValue() {
        return this.value;
    }

    public void setValue(Float f) {
        this.value = f;
    }
}
