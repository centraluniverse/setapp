package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EstimatedDataModel implements Parcelable {
    public static final Creator<EstimatedDataModel> CREATOR = new C14091();
    @C0631a
    @C0633c(a = "unit")
    private String unit;
    @C0631a
    @C0633c(a = "value")
    private String value;

    static class C14091 implements Creator<EstimatedDataModel> {
        C14091() {
        }

        public EstimatedDataModel createFromParcel(Parcel parcel) {
            return new EstimatedDataModel(parcel);
        }

        public EstimatedDataModel[] newArray(int i) {
            return new EstimatedDataModel[i];
        }
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String str) {
        this.value = str;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String str) {
        this.unit = str;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.value);
        parcel.writeString(this.unit);
    }

    protected EstimatedDataModel(Parcel parcel) {
        this.value = parcel.readString();
        this.unit = parcel.readString();
    }
}
