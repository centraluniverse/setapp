package com.solaredge.common.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "utilizationMeasures", strict = false)
public class UtilizationMeasures {
    @Element(name = "consumptionMeasures", required = false)
    private ConsumptionMeasures mConsumptionMeasures;
    @Element(name = "productionMeasures", required = false)
    private ProductionMeasures mProductionMeasures;

    public ProductionMeasures getProductionMeasures() {
        return this.mProductionMeasures;
    }

    public void setProductionMeasures(ProductionMeasures productionMeasures) {
        this.mProductionMeasures = productionMeasures;
    }

    public ConsumptionMeasures getConsumptionMeasures() {
        return this.mConsumptionMeasures;
    }

    public void setConsumptionMeasures(ConsumptionMeasures consumptionMeasures) {
        this.mConsumptionMeasures = consumptionMeasures;
    }
}
