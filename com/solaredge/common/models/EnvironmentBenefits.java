package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EnvironmentBenefits {
    @C0631a
    @C0633c(a = "gasEmissionSaved")
    private EnvironmentBenefits_GasEmissionSaved gasEmissionSaved;
    @C0631a
    @C0633c(a = "lightBulbs")
    private float lightBulbs = -1.0f;
    @C0631a
    @C0633c(a = "treesPlanted")
    private float treesPlanted = -1.0f;

    public float getLightBulbs() {
        return this.lightBulbs;
    }

    public float getTreesPlanted() {
        return this.treesPlanted;
    }

    public EnvironmentBenefits_GasEmissionSaved getGasEmissionSaved() {
        return this.gasEmissionSaved;
    }

    public void setGasEmissionSaved(EnvironmentBenefits_GasEmissionSaved environmentBenefits_GasEmissionSaved) {
        this.gasEmissionSaved = environmentBenefits_GasEmissionSaved;
    }

    public void setTreesPlanted(float f) {
        this.treesPlanted = f;
    }

    public void setLightBulbs(float f) {
        this.lightBulbs = f;
    }
}
