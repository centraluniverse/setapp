package com.solaredge.common.models;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.C1379d.C1372a;
import com.solaredge.common.models.fieldOverview.LocationJson;
import java.util.ArrayList;
import java.util.Arrays;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class Location implements Parcelable {
    public static final Creator<Location> CREATOR = ParcelableCompat.newCreator(new C21981());
    @Element(name = "address", required = false)
    private String addressLine1;
    @Element(name = "address2", required = false)
    private String addressLine2;
    @Element(name = "city", required = false)
    private String city;
    @Element(name = "country", required = false)
    private String country;
    @Element(name = "elevation", required = false)
    private double elevation;
    @Element(name = "latitude", required = false)
    private double latitude;
    @Element(name = "longitude", required = false)
    private double longitude;
    @Element(name = "state", required = false)
    private String state;
    @Element(name = "timeZone", required = false)
    private String timeZone;
    @Element(name = "zip", required = false)
    private String zipCode;

    static class C21981 implements ParcelableCompatCreatorCallbacks<Location> {
        C21981() {
        }

        public Location createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Location(parcel);
        }

        public Location[] newArray(int i) {
            return new Location[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public Location(LocationJson locationJson) {
        if (locationJson != null) {
            this.country = locationJson.getWhereabouts().getCountry();
            this.state = locationJson.getWhereabouts().getState();
            this.city = locationJson.getWhereabouts().getCity();
            this.latitude = locationJson.getLatitude().doubleValue();
            this.longitude = locationJson.getLongitude().doubleValue();
            this.elevation = (double) locationJson.getElevation().intValue();
            this.timeZone = locationJson.getTimeZone();
        }
    }

    public Location(CreateSite createSite) {
        if (createSite.getLocation() != null) {
            this.country = createSite.getLocation().getCountryCode();
            this.state = createSite.getLocation().getState();
            this.city = createSite.getLocation().getCity();
            this.zipCode = createSite.getLocation().getZipCode();
            this.addressLine1 = createSite.getLocation().getAddress();
            this.addressLine2 = createSite.getLocation().getAddress2();
            this.latitude = createSite.getLocation().getLatitude();
            this.longitude = createSite.getLocation().getLongitude();
        }
    }

    public Location(UpdateSite updateSite) {
        if (updateSite.getLocation() != null) {
            this.country = updateSite.getLocation().getCountryCode();
            this.state = updateSite.getLocation().getState();
            this.city = updateSite.getLocation().getCity();
            this.zipCode = updateSite.getLocation().getZipCode();
            this.addressLine1 = updateSite.getLocation().getAddress();
            this.addressLine2 = updateSite.getLocation().getAddress2();
            this.latitude = updateSite.getLocation().getLatitude();
            this.longitude = updateSite.getLocation().getLongitude();
        }
    }

    protected Location(Parcel parcel) {
        this.country = parcel.readString();
        this.state = parcel.readString();
        this.city = parcel.readString();
        this.zipCode = parcel.readString();
        this.addressLine1 = parcel.readString();
        this.addressLine2 = parcel.readString();
        this.latitude = parcel.readDouble();
        this.longitude = parcel.readDouble();
        this.elevation = parcel.readDouble();
        this.timeZone = parcel.readString();
    }

    public Location(Location location) {
        this.country = location.getCountry();
        this.state = location.getState();
        this.city = location.getCity();
        this.zipCode = location.getZipCode();
        this.addressLine1 = location.getAddressLine1();
        this.addressLine2 = location.getAddressLine2();
        this.latitude = location.latitude;
    }

    public SiteLocation getSiteLocation(Context context) {
        SiteLocation siteLocation = new SiteLocation();
        siteLocation.setAddress(getAddressLine1());
        siteLocation.setAddress2(getAddressLine2());
        siteLocation.setCity(getCity());
        ArrayList arrayList = new ArrayList(Arrays.asList(context.getResources().getStringArray(C1372a.countries_codes)));
        context = new ArrayList(Arrays.asList(context.getResources().getStringArray(C1372a.countries_names))).indexOf(getCountry());
        siteLocation.setCountryCode(context > -1 ? (String) arrayList.get(context) : getCountry());
        siteLocation.setState(getState());
        siteLocation.setZipCode(getZipCode());
        siteLocation.setLongitude(getLongitude());
        siteLocation.setLatitude(getLatitude());
        return siteLocation;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.country);
        parcel.writeString(this.state);
        parcel.writeString(this.city);
        parcel.writeString(this.zipCode);
        parcel.writeString(this.addressLine1);
        parcel.writeString(this.addressLine2);
        parcel.writeDouble(this.latitude);
        parcel.writeDouble(this.longitude);
        parcel.writeDouble(this.elevation);
        parcel.writeString(this.timeZone);
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String str) {
        this.country = str;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String str) {
        this.state = str;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(String str) {
        this.zipCode = str;
    }

    public String getAddressLine1() {
        return this.addressLine1;
    }

    public void setAddressLine1(String str) {
        this.addressLine1 = str;
    }

    public String getAddressLine2() {
        return this.addressLine2;
    }

    public void setAddressLine2(String str) {
        this.addressLine2 = str;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double d) {
        this.latitude = d;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double d) {
        this.longitude = d;
    }

    public double getElevation() {
        return this.elevation;
    }

    public void setElevation(double d) {
        this.elevation = d;
    }

    public String getTimeZone() {
        return this.timeZone;
    }

    public void setTimeZone(String str) {
        this.timeZone = str;
    }
}
