package com.solaredge.common.models;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ValidationResult {
    public static final String RESULT_FAILED = "Failed";
    public static final String RESULT_PASSED = "Passed";
    public static final String RESULT_WARNING = "Warning";
    @ElementList(entry = "errorMessage", inline = true, required = false)
    private List<ValidationResultEntry> errorMessages;
    @ElementList(entry = "infoMessage", inline = true, required = false)
    private List<String> infoMessages;
    @Element(name = "status", required = false)
    private String status;
    @ElementList(entry = "warningMessage", inline = true, required = false)
    private List<ValidationResultEntry> warningMessages;

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public List<String> getInfoMessages() {
        return this.infoMessages;
    }

    public void setInfoMessages(List<String> list) {
        this.infoMessages = list;
    }

    public List<ValidationResultEntry> getWarningMessages() {
        return this.warningMessages;
    }

    public void setWarningMessages(List<ValidationResultEntry> list) {
        this.warningMessages = list;
    }

    public List<ValidationResultEntry> getErrorMessages() {
        return this.errorMessages;
    }

    public void setErrorMessages(List<ValidationResultEntry> list) {
        this.errorMessages = list;
    }

    public String getLocalizedErrorDescription() {
        String str = "";
        if (getErrorMessages() != null) {
            for (ValidationResultEntry validationResultEntry : getErrorMessages()) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append(validationResultEntry.getLocalizedMessage());
                stringBuilder.append("\n");
                str = stringBuilder.toString();
            }
        }
        return str;
    }
}
