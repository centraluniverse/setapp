package com.solaredge.common.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "productionMeasures", strict = false)
public class ProductionMeasures {
    @Element(name = "feedInPercentage", required = false)
    private float mFeedInPercentage = -1.0f;
    @Element(name = "feedInValue", required = false)
    private float mFeedInValue = -1.0f;
    @Element(name = "measurementUnit", required = false)
    private String mMeasurementUnit;
    @Element(name = "selfConsumptionPercentage", required = false)
    private float mSelfConsumptionPercentage = -1.0f;
    @Element(name = "selfConsumptionValue", required = false)
    private float mSelfConsumptionValue = -1.0f;
    @Element(name = "value", required = false)
    private float mValue;

    public String getMeasurementUnit() {
        return this.mMeasurementUnit;
    }

    public void setMeasurementUnit(String str) {
        this.mMeasurementUnit = str;
    }

    public float getValue() {
        return this.mValue;
    }

    public void setValue(float f) {
        this.mValue = f;
    }

    public float getFeedInValue() {
        return this.mFeedInValue;
    }

    public void setFeedInValue(float f) {
        this.mFeedInValue = f;
    }

    public float getFeedInPercentage() {
        return this.mFeedInPercentage;
    }

    public void setFeedInPercentage(float f) {
        this.mFeedInPercentage = f;
    }

    public float getSelfConsumptionValue() {
        return this.mSelfConsumptionValue;
    }

    public void setSelfConsumptionValue(float f) {
        this.mSelfConsumptionValue = f;
    }

    public float getSelfConsumptionPercentage() {
        return this.mSelfConsumptionPercentage;
    }

    public void setSelfConsumptionPercentage(float f) {
        this.mSelfConsumptionPercentage = f;
    }
}
