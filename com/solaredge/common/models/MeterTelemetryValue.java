package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.GregorianCalendar;

public class MeterTelemetryValue {
    @C0631a
    @C0633c(a = "date")
    private GregorianCalendar date;
    @C0631a
    @C0633c(a = "value")
    private float value = -1.0f;

    public float getValue() {
        return this.value;
    }

    public void setValue(float f) {
        this.value = f;
    }

    public GregorianCalendar getDate() {
        return this.date;
    }

    public void setDate(GregorianCalendar gregorianCalendar) {
        this.date = gregorianCalendar;
    }
}
