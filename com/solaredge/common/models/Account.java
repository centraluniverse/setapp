package com.solaredge.common.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "account", strict = false)
public class Account implements Parcelable, C1370b {
    public static final Creator<Account> CREATOR = ParcelableCompat.newCreator(new C21941());
    public static final String SQL_CREATETE_TABLE = "CREATE TABLE Accounts( _id INTEGER PRIMARY KEY AUTOINCREMENT, account_id integer, name nvarchar(30) ); ";
    public static final String TABLE_NAME = "Accounts";
    private long _id;
    @Element(name = "id", required = false)
    private long accountId;
    @Element(name = "name", required = false)
    private String name;

    public interface TableColumns extends BaseColumns {
        public static final String ACCOUNT_ID = "account_id";
        public static final String NAME = "name";
    }

    static class C21941 implements ParcelableCompatCreatorCallbacks<Account> {
        C21941() {
        }

        public Account createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Account(parcel);
        }

        public Account[] newArray(int i) {
            return new Account[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public Account(Parcel parcel) {
        readFromParcel(parcel);
    }

    public Account(Cursor cursor) {
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.accountId = cursor.getLong(cursor.getColumnIndex("account_id"));
        this.name = cursor.getString(cursor.getColumnIndex("name"));
    }

    public String toString() {
        return this.name;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("account_id", Long.valueOf(this.accountId));
        contentValues.put("name", this.name);
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.accountId);
        parcel.writeString(this.name);
    }

    private void readFromParcel(Parcel parcel) {
        this._id = parcel.readLong();
        this.accountId = parcel.readLong();
        this.name = parcel.readString();
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public long getAccountId() {
        return this.accountId;
    }

    public void setAccountId(long j) {
        this.accountId = j;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }
}
