package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.models.response.CreateUpdateSiteModule;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class ModuleResponse implements Parcelable {
    public static final Creator<ModuleResponse> CREATOR = ParcelableCompat.newCreator(new C21991());
    @Element(name = "manufacturerName", required = false)
    private String manufacturerName;
    @Element(name = "maximumPower", required = false)
    private float maximumPower;
    @Element(name = "modelName", required = false)
    private String modelName;
    @Element(name = "temperatureCoef", required = false)
    private float temperatureCoef;

    static class C21991 implements ParcelableCompatCreatorCallbacks<ModuleResponse> {
        C21991() {
        }

        public ModuleResponse createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new ModuleResponse(parcel);
        }

        public ModuleResponse[] newArray(int i) {
            return new ModuleResponse[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    protected ModuleResponse(Parcel parcel) {
        this.manufacturerName = parcel.readString();
        this.modelName = parcel.readString();
        this.maximumPower = parcel.readFloat();
        this.temperatureCoef = parcel.readFloat();
    }

    public ModuleResponse(CreateSite createSite) {
        if (createSite.getPrimaryModule() != null) {
            this.manufacturerName = createSite.getPrimaryModule().getManufacturerName();
            this.modelName = createSite.getPrimaryModule().getModelName();
            this.maximumPower = createSite.getPrimaryModule().getMaximumPower();
            this.temperatureCoef = createSite.getPrimaryModule().getTemperatureCoef();
        }
    }

    public ModuleResponse(UpdateSite updateSite) {
        if (updateSite.getPrimaryModule() != null) {
            this.manufacturerName = updateSite.getPrimaryModule().getManufacturerName();
            this.modelName = updateSite.getPrimaryModule().getModelName();
            this.maximumPower = updateSite.getPrimaryModule().getMaximumPower();
            this.temperatureCoef = updateSite.getPrimaryModule().getTemperatureCoef();
        }
    }

    public ModuleResponse(CreateUpdateSiteModule createUpdateSiteModule) {
        if (createUpdateSiteModule != null) {
            this.manufacturerName = createUpdateSiteModule.getManufacturerName();
            this.modelName = createUpdateSiteModule.getModelName();
            this.maximumPower = createUpdateSiteModule.getMaximumPower();
            this.temperatureCoef = createUpdateSiteModule.getTemperatureCoef();
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.manufacturerName);
        parcel.writeString(this.modelName);
        parcel.writeFloat(this.maximumPower);
        parcel.writeFloat(this.temperatureCoef);
    }

    public String getManufacturerName() {
        return this.manufacturerName;
    }

    public void setManufacturerName(String str) {
        this.manufacturerName = str;
    }

    public String getModelName() {
        return this.modelName;
    }

    public void setModelName(String str) {
        this.modelName = str;
    }

    public float getMaximumPower() {
        return this.maximumPower;
    }

    public void setMaximumPower(float f) {
        this.maximumPower = f;
    }

    public float getTemperatureCoef() {
        return this.temperatureCoef;
    }

    public void setTemperatureCoef(float f) {
        this.temperatureCoef = f;
    }
}
