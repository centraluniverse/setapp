package com.solaredge.common.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "consumptionMeasures", strict = false)
public class ConsumptionMeasures {
    @Element(name = "measurementUnit", required = false)
    private String mMeasurementUnit;
    @Element(name = "purchasedPercentage", required = false)
    private float mPurchasedPercentage = -1.0f;
    @Element(name = "purchasedValue", required = false)
    private float mPurchasedValue = -1.0f;
    @Element(name = "selfConsumptionPercentage", required = false)
    private float mSelfConsumptionPercentage = -1.0f;
    @Element(name = "selfConsumptionValue", required = false)
    private float mSelfConsumptionValue = -1.0f;
    @Element(name = "value", required = false)
    private float mValue;

    public String getMeasurementUnit() {
        return this.mMeasurementUnit;
    }

    public void setMeasurementUnit(String str) {
        this.mMeasurementUnit = str;
    }

    public float getValue() {
        return this.mValue;
    }

    public void setValue(float f) {
        this.mValue = f;
    }

    public float getPurchasedValue() {
        return this.mPurchasedValue;
    }

    public void setPurchasedValue(float f) {
        this.mPurchasedValue = f;
    }

    public float getPurchasedPercentage() {
        return this.mPurchasedPercentage;
    }

    public void setPurchasedPercentage(float f) {
        this.mPurchasedPercentage = f;
    }

    public float getSelfConsumptionValue() {
        return this.mSelfConsumptionValue;
    }

    public void setSelfConsumptionValue(float f) {
        this.mSelfConsumptionValue = f;
    }

    public float getSelfConsumptionPercentage() {
        return this.mSelfConsumptionPercentage;
    }

    public void setSelfConsumptionPercentage(float f) {
        this.mSelfConsumptionPercentage = f;
    }
}
