package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EnvironmentBenefits_GasEmissionSaved {
    @C0631a
    @C0633c(a = "co2")
    private float co2 = -1.0f;
    @C0631a
    @C0633c(a = "nox")
    private float nox = -1.0f;
    @C0631a
    @C0633c(a = "so2")
    private float so2 = -1.0f;
    @C0631a
    @C0633c(a = "units")
    private String units;

    public EnvironmentBenefits_GasEmissionSaved(String str, float f, float f2, float f3) {
        this.units = str;
        this.co2 = f;
        this.so2 = f2;
        this.nox = f3;
    }

    public float getCo2() {
        return this.co2;
    }

    public String getUnits() {
        return this.units;
    }
}
