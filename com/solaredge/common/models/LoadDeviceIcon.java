package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class LoadDeviceIcon {
    @C0631a
    @C0633c(a = "iconURL")
    private String Url;
    @C0631a
    @C0633c(a = "iconName")
    private String name;

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.Url;
    }
}
