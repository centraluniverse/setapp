package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class LowCostPowerView {
    @C0631a
    @C0633c(a = "mobile")
    private LowCostInnerObject mobile;
    @C0631a
    @C0633c(a = "tablet")
    private LowCostInnerObject tablet;

    public class LowCostInnerObject {
        @C0631a
        @C0633c(a = "image")
        private String image;
        @C0631a
        @C0633c(a = "url")
        private String url;

        public String getImage() {
            return this.image;
        }

        public String getUrl() {
            return this.url;
        }
    }

    public LowCostInnerObject getMobile() {
        return this.mobile;
    }

    public LowCostInnerObject getTablet() {
        return this.tablet;
    }
}
