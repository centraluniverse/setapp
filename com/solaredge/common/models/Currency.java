package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementUnion;
import org.simpleframework.xml.Root;

@Root(name = "currency", strict = false)
public class Currency implements Parcelable {
    public static final Creator<Currency> CREATOR = ParcelableCompat.newCreator(new C21951());
    @ElementUnion({@Element(name = "currency", required = false), @Element(name = "code", required = false)})
    private String currency;
    @Element(name = "Symbol", required = false)
    private String currencySymbol;
    @Element(name = "fullCurrency", required = false)
    private String fullCurrency;

    static class C21951 implements ParcelableCompatCreatorCallbacks<Currency> {
        C21951() {
        }

        public Currency createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Currency(parcel);
        }

        public Currency[] newArray(int i) {
            return new Currency[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public Currency(String str, String str2, String str3) {
        this.currency = str;
        this.fullCurrency = str2;
        this.currencySymbol = str3;
    }

    protected Currency(Parcel parcel) {
        this.currency = parcel.readString();
        this.fullCurrency = parcel.readString();
        this.currencySymbol = parcel.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.currency);
        parcel.writeString(this.fullCurrency);
        parcel.writeString(this.currencySymbol);
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String str) {
        this.currency = str;
    }

    public String getFullCurrency() {
        return this.fullCurrency;
    }

    public void setFullCurrency(String str) {
        this.fullCurrency = str;
    }

    public String getCurrencySymbol() {
        return this.currencySymbol;
    }

    public void setCurrencySymbol(String str) {
        this.currencySymbol = str;
    }

    public String toString() {
        return this.currency;
    }
}
