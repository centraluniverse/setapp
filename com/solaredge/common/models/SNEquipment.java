package com.solaredge.common.models;

import android.content.ContentValues;
import android.content.Context;
import android.provider.BaseColumns;
import com.solaredge.common.p112c.C1369a;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "equipment", strict = false)
public class SNEquipment {
    public static final String SQL_CREATETE_TABLE = "CREATE TABLE SNEquipments( type nvarchar(20), site_type nvarchar(5), prefix nvarchar(5) ); ";
    public static final String TABLE_NAME = "SNEquipments";
    @ElementList(entry = "prefix", inline = true, required = false)
    private List<String> prefixes;
    private String siteType;
    @Attribute(name = "value", required = false)
    private String type;

    public interface TableColumns extends BaseColumns {
        public static final String PREFIX = "prefix";
        public static final String SITE_TYPE = "site_type";
        public static final String TYPE = "type";
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public long save(Context context) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableColumns.TYPE, this.type);
        C1369a.m3766b(context).beginTransaction();
        try {
            for (String put : this.prefixes) {
                contentValues.put(TableColumns.PREFIX, put);
                contentValues.put(TableColumns.SITE_TYPE, this.siteType);
                C1369a.m3766b(context).insert(TABLE_NAME, null, contentValues);
            }
            C1369a.m3766b(context).setTransactionSuccessful();
            return 0;
        } finally {
            C1369a.m3766b(context).endTransaction();
        }
    }

    public String getType() {
        return this.type;
    }

    public void setType(String str) {
        this.type = str;
    }

    public List<String> getPrefixes() {
        return this.prefixes;
    }

    public void setPrefixes(List<String> list) {
        this.prefixes = list;
    }

    public String getSiteType() {
        return this.siteType;
    }

    public void setSiteType(String str) {
        this.siteType = str;
    }
}
