package com.solaredge.common.models;

import java.io.Serializable;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class UnintentionalDeletionConflict implements Serializable {
    @Element(name = "clientSerialNumber", required = false)
    public String clientSerialNumber;
    @Element(name = "column", required = false)
    public long column;
    @Element(name = "row", required = false)
    public long row;
    @Element(name = "serverSerialNumber", required = false)
    public String serverSerialNumber;
}
