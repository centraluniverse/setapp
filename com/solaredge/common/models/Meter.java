package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.List;

public class Meter {
    @C0631a
    @C0633c(a = "type")
    private String type;
    @C0631a
    @C0633c(a = "values")
    private List<MeterTelemetryValue> values;

    public String getType() {
        return this.type;
    }

    public void setType(String str) {
        this.type = str;
    }

    public List<MeterTelemetryValue> getValues() {
        return this.values;
    }

    public void setValues(List<MeterTelemetryValue> list) {
        this.values = list;
    }
}
