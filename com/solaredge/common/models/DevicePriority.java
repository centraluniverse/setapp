package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class DevicePriority implements Parcelable {
    public static final Creator<DevicePriority> CREATOR = new C14071();
    @C0631a
    @C0633c(a = "excessPVEnabled")
    private Boolean excessPVEnabled;
    @C0631a
    @C0633c(a = "iconName")
    private String iconName;
    @C0631a
    @C0633c(a = "maxChargePercent")
    private Integer maxChargePercent;
    @C0631a
    @C0633c(a = "maxSolarDuration")
    private Integer maxSolarDuration;
    @C0631a
    @C0633c(a = "minChargePercent")
    private Integer minChargePercent;
    @C0631a
    @C0633c(a = "name")
    private String name;
    private int selectedDurationPosition;
    @C0631a
    @C0633c(a = "serialNumber")
    private String serialNumber;
    @C0631a
    @C0633c(a = "targetChargePercent")
    private Integer targetChargePercent;
    @C0631a
    @C0633c(a = "type")
    private String type;

    static class C14071 implements Creator<DevicePriority> {
        C14071() {
        }

        public DevicePriority createFromParcel(Parcel parcel) {
            return new DevicePriority(parcel);
        }

        public DevicePriority[] newArray(int i) {
            return new DevicePriority[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public DevicePriority(DevicePriority devicePriority) {
        this.type = devicePriority.getType();
        this.serialNumber = devicePriority.getSerialNumber();
        this.iconName = devicePriority.getIconName();
        this.name = devicePriority.getName();
        this.minChargePercent = devicePriority.getMinChargePercent();
        this.maxChargePercent = devicePriority.getMaxChargePercent();
        this.targetChargePercent = devicePriority.getTargetChargePercent();
        this.excessPVEnabled = devicePriority.getExcessPVEnabled();
        this.maxSolarDuration = devicePriority.getMaxSolarDuration();
        this.selectedDurationPosition = devicePriority.getSelectedDurationPosition();
    }

    public DevicePriority(String str, String str2, String str3, Integer num, Integer num2, Integer num3, Boolean bool, Integer num4) {
        this.type = str;
        this.serialNumber = str2;
        this.name = str3;
        this.minChargePercent = num;
        this.maxChargePercent = num2;
        this.targetChargePercent = num3;
        this.excessPVEnabled = bool;
        this.maxSolarDuration = num4;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String str) {
        this.type = str;
    }

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(String str) {
        this.serialNumber = str;
    }

    public String getIconName() {
        return this.iconName;
    }

    public void setIconName(String str) {
        this.iconName = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public Integer getMinChargePercent() {
        return this.minChargePercent;
    }

    public void setMinChargePercent(Integer num) {
        this.minChargePercent = num;
    }

    public Integer getMaxChargePercent() {
        return this.maxChargePercent;
    }

    public void setMaxChargePercent(Integer num) {
        this.maxChargePercent = num;
    }

    public Integer getTargetChargePercent() {
        return this.targetChargePercent;
    }

    public void setTargetChargePercent(Integer num) {
        this.targetChargePercent = num;
    }

    public Boolean getExcessPVEnabled() {
        return this.excessPVEnabled;
    }

    public void setExcessPVEnabled(Boolean bool) {
        this.excessPVEnabled = bool;
    }

    public Integer getMaxSolarDuration() {
        return this.maxSolarDuration;
    }

    public void setMaxSolarDuration(Integer num) {
        this.maxSolarDuration = num;
    }

    public void setSelectedDurationPosition(int i) {
        this.selectedDurationPosition = i;
    }

    public int getSelectedDurationPosition() {
        return this.selectedDurationPosition;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.type);
        parcel.writeString(this.serialNumber);
        parcel.writeString(this.iconName);
        parcel.writeString(this.name);
        parcel.writeValue(this.minChargePercent);
        parcel.writeValue(this.maxChargePercent);
        parcel.writeValue(this.targetChargePercent);
        parcel.writeValue(this.excessPVEnabled);
        parcel.writeValue(this.maxSolarDuration);
        parcel.writeInt(this.selectedDurationPosition);
    }

    protected DevicePriority(Parcel parcel) {
        this.type = parcel.readString();
        this.serialNumber = parcel.readString();
        this.iconName = parcel.readString();
        this.name = parcel.readString();
        this.minChargePercent = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.maxChargePercent = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.targetChargePercent = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.excessPVEnabled = (Boolean) parcel.readValue(Boolean.class.getClassLoader());
        this.maxSolarDuration = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.selectedDurationPosition = parcel.readInt();
    }
}
