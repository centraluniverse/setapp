package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.p116g.C1404g;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoadDeviceExcessPVConfiguration implements Serializable {
    @C0631a
    @C0633c(a = "excessPVActiveDays")
    private List<String> excessPVActiveDays = new ArrayList();
    @C0631a
    @C0633c(a = "useExcessPV")
    private Boolean useExcessPV;

    public LoadDeviceExcessPVConfiguration(LoadDeviceExcessPVConfiguration loadDeviceExcessPVConfiguration) {
        this.useExcessPV = loadDeviceExcessPVConfiguration.useExcessPV;
        this.excessPVActiveDays = new ArrayList();
        if (loadDeviceExcessPVConfiguration.excessPVActiveDays != null) {
            for (String add : loadDeviceExcessPVConfiguration.excessPVActiveDays) {
                this.excessPVActiveDays.add(add);
            }
        }
    }

    public Boolean isUseExcessPV() {
        return this.useExcessPV;
    }

    public void setUseExcessPV(boolean z) {
        this.useExcessPV = Boolean.valueOf(z);
    }

    public List<String> getExcessPVActiveDays() {
        return this.excessPVActiveDays;
    }

    public void setExcessPVActiveDays(List<String> list) {
        this.excessPVActiveDays = list;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null) {
            return false;
        }
        LoadDeviceExcessPVConfiguration loadDeviceExcessPVConfiguration = (LoadDeviceExcessPVConfiguration) obj;
        if (C1404g.m3842a(isUseExcessPV(), loadDeviceExcessPVConfiguration.isUseExcessPV()) && C1404g.m3845a(getExcessPVActiveDays(), loadDeviceExcessPVConfiguration.getExcessPVActiveDays()) != null) {
            z = true;
        }
        return z;
    }
}
