package com.solaredge.common.models.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.C1366a;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rectangle", strict = false)
public class Rectangle implements Parcelable, C1370b {
    public static final Creator<Rectangle> CREATOR = ParcelableCompat.newCreator(new C22071());
    public static final String SQL_CREATETE_TABLE = "CREATE TABLE Rectangle( _id INTEGER PRIMARY KEY AUTOINCREMENT, inverter_id integer, smi_id integer, group_id integer, pos_x double, pos_y double, width double, height double, azimuth double ); ";
    public static final String TABLE_NAME = "Rectangle";
    private long _id;
    @Element(name = "azimuth", required = false)
    @C0631a
    @C0633c(a = "azimuth")
    private double mAzimuth;
    private long mGroupId;
    @Element(name = "height", required = false)
    @C0631a
    @C0633c(a = "height")
    private double mHeight;
    private long mInverterId;
    @Element(name = "x", required = false)
    @C0631a
    @C0633c(a = "x")
    private double mPosX;
    @Element(name = "y", required = false)
    @C0631a
    @C0633c(a = "y")
    private double mPosY;
    private long mSMIId;
    @Element(name = "width", required = false)
    @C0631a
    @C0633c(a = "width")
    private double mWidth;

    public interface TableColumns extends BaseColumns {
        public static final String AZIMUTH = "azimuth";
        public static final String GROUP_ID = "group_id";
        public static final String HEIGHT = "height";
        public static final String INVERTER_ID = "inverter_id";
        public static final String POS_X = "pos_x";
        public static final String POS_Y = "pos_y";
        public static final String SMI_ID = "smi_id";
        public static final String WIDTH = "width";
    }

    static class C22071 implements ParcelableCompatCreatorCallbacks<Rectangle> {
        C22071() {
        }

        public Rectangle createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Rectangle(parcel);
        }

        public Rectangle[] newArray(int i) {
            return new Rectangle[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public Rectangle(Rectangle rectangle) {
        this.mInverterId = rectangle.mInverterId;
        this.mSMIId = rectangle.mSMIId;
        this.mGroupId = rectangle.mGroupId;
        this.mPosX = rectangle.mPosX;
        this.mPosY = rectangle.mPosY;
        this.mWidth = rectangle.mWidth;
        this.mHeight = rectangle.mHeight;
        this.mAzimuth = rectangle.mAzimuth;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public Rectangle() {
        this.mInverterId = -1;
        this.mSMIId = -1;
        this.mGroupId = -1;
        this.mPosX = -1.0d;
        this.mPosY = -1.0d;
        this.mWidth = 0.0d;
        this.mHeight = 0.0d;
        this.mAzimuth = 0.0d;
    }

    public Rectangle(Parcel parcel) {
        readFromParcel(parcel);
    }

    public Rectangle(Cursor cursor) {
        this();
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.mInverterId = cursor.getLong(cursor.getColumnIndex("inverter_id"));
        this.mSMIId = cursor.getLong(cursor.getColumnIndex("smi_id"));
        this.mGroupId = cursor.getLong(cursor.getColumnIndex("group_id"));
        this.mPosX = cursor.getDouble(cursor.getColumnIndex(TableColumns.POS_X));
        this.mPosY = cursor.getDouble(cursor.getColumnIndex(TableColumns.POS_Y));
        this.mWidth = cursor.getDouble(cursor.getColumnIndex("width"));
        this.mHeight = cursor.getDouble(cursor.getColumnIndex("height"));
        this.mAzimuth = cursor.getDouble(cursor.getColumnIndex(TableColumns.AZIMUTH));
    }

    public void delete() {
        C1369a.m3766b(C1366a.m3749a().m3753b()).delete(TABLE_NAME, "_id = ?", new String[]{String.valueOf(this._id)});
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("inverter_id", Long.valueOf(this.mInverterId));
        contentValues.put("smi_id", Long.valueOf(this.mSMIId));
        contentValues.put("group_id", Long.valueOf(this.mGroupId));
        contentValues.put(TableColumns.POS_X, Double.valueOf(this.mPosX));
        contentValues.put(TableColumns.POS_Y, Double.valueOf(this.mPosY));
        contentValues.put("width", Double.valueOf(this.mWidth));
        contentValues.put("height", Double.valueOf(this.mHeight));
        contentValues.put(TableColumns.AZIMUTH, Double.valueOf(this.mAzimuth));
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.mInverterId);
        parcel.writeLong(this.mSMIId);
        parcel.writeLong(this.mGroupId);
        parcel.writeDouble(this.mPosX);
        parcel.writeDouble(this.mPosY);
        parcel.writeDouble(this.mWidth);
        parcel.writeDouble(this.mHeight);
        parcel.writeDouble(this.mAzimuth);
    }

    private void readFromParcel(Parcel parcel) {
        this._id = parcel.readLong();
        this.mInverterId = parcel.readLong();
        this.mSMIId = parcel.readLong();
        this.mGroupId = parcel.readLong();
        this.mPosX = parcel.readDouble();
        this.mPosY = parcel.readDouble();
        this.mWidth = parcel.readDouble();
        this.mHeight = parcel.readDouble();
        this.mAzimuth = parcel.readDouble();
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public double getPosX() {
        return this.mPosX;
    }

    public void setPosX(double d) {
        this.mPosX = d;
    }

    public double getPosY() {
        return this.mPosY;
    }

    public void setPosY(double d) {
        this.mPosY = d;
    }

    public double getWidth() {
        return this.mWidth;
    }

    public void setWidth(double d) {
        this.mWidth = d;
    }

    public double getHeight() {
        return this.mHeight;
    }

    public void setHeight(double d) {
        this.mHeight = d;
    }

    public double getAzimuth() {
        return this.mAzimuth;
    }

    public void setAzimuth(double d) {
        this.mAzimuth = d;
    }

    public long getInverterId() {
        return this.mInverterId;
    }

    public void setInverterId(long j) {
        this.mInverterId = j;
    }

    public long getSMIId() {
        return this.mSMIId;
    }

    public void setSMIId(long j) {
        this.mSMIId = j;
    }

    public long getGroupId() {
        return this.mGroupId;
    }

    public void setGroupId(long j) {
        this.mGroupId = j;
    }
}
