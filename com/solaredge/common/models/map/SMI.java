package com.solaredge.common.models.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.C1366a;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"rectangle", "serialNumber", "inverterBindingId"})
@Root(name = "SMI", strict = false)
public class SMI implements Parcelable, C1370b {
    public static final Creator<SMI> CREATOR = ParcelableCompat.newCreator(new C22081());
    public static final String SQL_CREATETE_TABLE = "CREATE TABLE SMI( _id INTEGER PRIMARY KEY AUTOINCREMENT, map_id integer, layout_id integer, inverter_id integer, serial_number nvarchar(30) ); ";
    public static final String TABLE_NAME = "SMI";
    public static final Uri URI_MAPS_LIST = Uri.parse("sqlite://com.developica.solaredge.monitor/maps/list");
    private long _id;
    @Element(name = "inverterBindingId", required = false)
    private long mInverterId;
    private long mLayoutId;
    private long mMapId;
    @Element(name = "rectangle", required = false)
    private Rectangle mRectangle;
    @Element(name = "serialNumber", required = false)
    private String mSerialNumber;

    public interface TableColumns extends BaseColumns {
        public static final String INVERTER_ID = "inverter_id";
        public static final String LAYOUT_ID = "layout_id";
        public static final String MAP_ID = "map_id";
        public static final String SERIAL_NUMBER = "serial_number";
    }

    static class C22081 implements ParcelableCompatCreatorCallbacks<SMI> {
        C22081() {
        }

        public SMI createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new SMI(parcel);
        }

        public SMI[] newArray(int i) {
            return new SMI[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return "SMI";
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public SMI() {
        this.mMapId = -1;
        this.mLayoutId = -1;
        this.mInverterId = -1;
        this.mSerialNumber = "";
        this.mRectangle = new Rectangle();
    }

    public SMI(SMI smi) {
        this.mMapId = smi.mMapId;
        this.mLayoutId = smi.mLayoutId;
        this.mInverterId = smi.mInverterId;
        this.mSerialNumber = smi.mSerialNumber;
        this.mRectangle = new Rectangle(smi.mRectangle);
    }

    public SMI(Cursor cursor) {
        this();
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.mMapId = cursor.getLong(cursor.getColumnIndex("map_id"));
        this.mLayoutId = cursor.getLong(cursor.getColumnIndex("layout_id"));
        this.mInverterId = cursor.getLong(cursor.getColumnIndex("inverter_id"));
        this.mSerialNumber = cursor.getString(cursor.getColumnIndex("serial_number"));
    }

    public SMI(Parcel parcel) {
        readFromParcel(parcel);
    }

    public void delete() {
        if (this.mRectangle != null) {
            this.mRectangle.delete();
        }
        C1369a.m3766b(C1366a.m3749a().m3753b()).delete("SMI", "_id = ?", new String[]{String.valueOf(this._id)});
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("map_id", Long.valueOf(this.mMapId));
        contentValues.put("layout_id", Long.valueOf(this.mLayoutId));
        contentValues.put("inverter_id", Long.valueOf(this.mInverterId));
        contentValues.put("serial_number", this.mSerialNumber);
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        this.mRectangle.setSMIId(this._id);
        this.mRectangle.save(context);
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query("SMI", null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update("SMI", getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    this.mRectangle.setSMIId(this._id);
                    this.mRectangle.update(context);
                    context.getContentResolver().notifyChange(URI_MAPS_LIST, null);
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.mMapId);
        parcel.writeLong(this.mLayoutId);
        parcel.writeLong(this.mInverterId);
        parcel.writeString(this.mSerialNumber);
        parcel.writeParcelable(this.mRectangle, i);
    }

    private void readFromParcel(Parcel parcel) {
        this._id = parcel.readLong();
        this.mMapId = parcel.readLong();
        this.mLayoutId = parcel.readLong();
        this.mInverterId = parcel.readLong();
        this.mSerialNumber = parcel.readString();
        this.mRectangle = (Rectangle) parcel.readParcelable(Rectangle.class.getClassLoader());
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public String getSerialNumber() {
        return this.mSerialNumber;
    }

    public void setSerialNumber(String str) {
        this.mSerialNumber = str;
    }

    public Rectangle getRectangle() {
        return this.mRectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.mRectangle = rectangle;
    }

    public long getInverterId() {
        return this.mInverterId;
    }

    public void setInverterId(long j) {
        this.mInverterId = j;
    }

    public long getMapId() {
        return this.mMapId;
    }

    public void setMapId(long j) {
        this.mMapId = j;
    }

    public long getLayoutId() {
        return this.mLayoutId;
    }

    public void setLayoutId(long j) {
        this.mLayoutId = j;
    }
}
