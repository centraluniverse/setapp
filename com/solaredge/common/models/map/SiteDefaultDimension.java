package com.solaredge.common.models.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.C1366a;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"groupVSpacing", "groupHSpacing", "inverterDimension", "inverter3rdPartyDimension", "smiDimension", "moduleDimension"})
@Root(name = "siteDefaultDimension", strict = false)
public class SiteDefaultDimension implements Parcelable, C1370b {
    public static final Creator<SiteDefaultDimension> CREATOR = ParcelableCompat.newCreator(new C22091());
    public static final String SQL_CREATETE_TABLE = "CREATE TABLE SiteDefaultDimension( _id INTEGER PRIMARY KEY AUTOINCREMENT, map_id integer, layout_id integer, group_vertical_spacing float, group_horizontal_spacing float, inverter_width float, inverter_height float, inverter_3rd_part_width float, inverter_3rd_part_height float, smi_width float, smi_height float, module_width float, module_height float ); ";
    public static final String TABLE_NAME = "SiteDefaultDimension";
    public static final Uri URI_MAPS_LIST = Uri.parse("sqlite://com.developica.solaredge.monitor/maps/list");
    private long _id;
    @Element(name = "groupHSpacing", required = false)
    private double mGroupHorizontalSpacing;
    @Element(name = "groupVSpacing", required = false)
    private double mGroupVerticalSpacing;
    @Element(name = "inverter3rdPartyDimension", required = false)
    private Dimension mInverter3rdPartyDimension;
    @Element(name = "inverterDimension", required = false)
    private Dimension mInverterDimension;
    private long mLayoutId;
    private long mMapId;
    @Element(name = "moduleDimension", required = false)
    private Dimension mModuleDimension;
    @Element(name = "smiDimension", required = false)
    private Dimension mSMIDimension;

    public interface TableColumns extends BaseColumns {
        public static final String GROUP_HORIZONTAL_SPACING = "group_horizontal_spacing";
        public static final String GROUP_VERTICAL_SPACING = "group_vertical_spacing";
        public static final String INVERTER_3RD_PARTY_HEIGHT = "inverter_3rd_part_height";
        public static final String INVERTER_3RD_PARTY_WIDTH = "inverter_3rd_part_width";
        public static final String INVERTER_HEIGHT = "inverter_height";
        public static final String INVERTER_WIDTH = "inverter_width";
        public static final String LAYOUT_ID = "layout_id";
        public static final String MAP_ID = "map_id";
        public static final String MODULE_HEIGHT = "module_height";
        public static final String MODULE_WIDTH = "module_width";
        public static final String SMI_HEIGHT = "smi_height";
        public static final String SMI_WIDTH = "smi_width";
    }

    static class C22091 implements ParcelableCompatCreatorCallbacks<SiteDefaultDimension> {
        C22091() {
        }

        public SiteDefaultDimension createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new SiteDefaultDimension(parcel);
        }

        public SiteDefaultDimension[] newArray(int i) {
            return new SiteDefaultDimension[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public void delete() {
        C1369a.m3766b(C1366a.m3749a().m3753b()).delete(TABLE_NAME, "_id = ?", new String[]{String.valueOf(this._id)});
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public SiteDefaultDimension() {
        this.mMapId = -1;
        this.mLayoutId = -1;
        this.mGroupVerticalSpacing = 4.0d;
        this.mGroupHorizontalSpacing = 4.0d;
        this.mInverterDimension = new Dimension();
        this.mSMIDimension = new Dimension();
        this.mModuleDimension = new Dimension();
        this.mModuleDimension.setDimension(60.0d, 90.0d);
        this.mSMIDimension.setDimension(75.0d, 69.0d);
        this.mInverterDimension.setDimension(75.0d, 69.0d);
    }

    public SiteDefaultDimension(SiteDefaultDimension siteDefaultDimension) {
        this.mMapId = siteDefaultDimension.mMapId;
        this.mLayoutId = siteDefaultDimension.mLayoutId;
        this.mGroupVerticalSpacing = siteDefaultDimension.mGroupVerticalSpacing;
        this.mGroupHorizontalSpacing = siteDefaultDimension.mGroupHorizontalSpacing;
        if (siteDefaultDimension.mInverterDimension != null) {
            this.mInverterDimension = new Dimension(siteDefaultDimension.mInverterDimension);
        }
        if (siteDefaultDimension.mInverter3rdPartyDimension != null) {
            this.mInverter3rdPartyDimension = new Dimension(siteDefaultDimension.mInverter3rdPartyDimension);
        }
        if (siteDefaultDimension.mSMIDimension != null) {
            this.mSMIDimension = new Dimension(siteDefaultDimension.mSMIDimension);
        }
        if (siteDefaultDimension.mModuleDimension != null) {
            this.mModuleDimension = new Dimension(siteDefaultDimension.mModuleDimension);
        }
    }

    public SiteDefaultDimension(Cursor cursor) {
        this();
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.mMapId = cursor.getLong(cursor.getColumnIndex("map_id"));
        this.mLayoutId = cursor.getLong(cursor.getColumnIndex("layout_id"));
        this.mGroupVerticalSpacing = cursor.getDouble(cursor.getColumnIndex(TableColumns.GROUP_VERTICAL_SPACING));
        this.mGroupHorizontalSpacing = cursor.getDouble(cursor.getColumnIndex(TableColumns.GROUP_HORIZONTAL_SPACING));
        this.mInverterDimension.setHeight(cursor.getDouble(cursor.getColumnIndex(TableColumns.INVERTER_HEIGHT)));
        this.mInverterDimension.setWidth(cursor.getDouble(cursor.getColumnIndex(TableColumns.INVERTER_WIDTH)));
        if (cursor.getDouble(cursor.getColumnIndex(TableColumns.INVERTER_3RD_PARTY_HEIGHT)) > 0.0d) {
            if (this.mInverter3rdPartyDimension == null) {
                this.mInverter3rdPartyDimension = new Dimension();
            }
            this.mInverter3rdPartyDimension.setHeight(cursor.getDouble(cursor.getColumnIndex(TableColumns.INVERTER_3RD_PARTY_HEIGHT)));
            this.mInverter3rdPartyDimension.setWidth(cursor.getDouble(cursor.getColumnIndex(TableColumns.INVERTER_3RD_PARTY_WIDTH)));
        }
        this.mSMIDimension.setHeight(cursor.getDouble(cursor.getColumnIndex(TableColumns.SMI_HEIGHT)));
        this.mSMIDimension.setWidth(cursor.getDouble(cursor.getColumnIndex(TableColumns.SMI_WIDTH)));
        this.mModuleDimension.setHeight(cursor.getDouble(cursor.getColumnIndex("module_height")));
        this.mModuleDimension.setWidth(cursor.getDouble(cursor.getColumnIndex("module_width")));
    }

    public SiteDefaultDimension(Parcel parcel) {
        readFromParcel(parcel);
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("map_id", Long.valueOf(this.mMapId));
        contentValues.put("layout_id", Long.valueOf(this.mLayoutId));
        contentValues.put(TableColumns.GROUP_VERTICAL_SPACING, Double.valueOf(this.mGroupVerticalSpacing));
        contentValues.put(TableColumns.GROUP_HORIZONTAL_SPACING, Double.valueOf(this.mGroupHorizontalSpacing));
        contentValues.put(TableColumns.INVERTER_WIDTH, Double.valueOf(this.mInverterDimension.getWidth()));
        contentValues.put(TableColumns.INVERTER_HEIGHT, Double.valueOf(this.mInverterDimension.getHeight()));
        if (this.mInverter3rdPartyDimension != null) {
            contentValues.put(TableColumns.INVERTER_3RD_PARTY_WIDTH, Double.valueOf(this.mInverter3rdPartyDimension.getWidth()));
            contentValues.put(TableColumns.INVERTER_3RD_PARTY_HEIGHT, Double.valueOf(this.mInverter3rdPartyDimension.getHeight()));
        }
        contentValues.put(TableColumns.SMI_WIDTH, Double.valueOf(this.mSMIDimension.getWidth()));
        contentValues.put(TableColumns.SMI_HEIGHT, Double.valueOf(this.mSMIDimension.getHeight()));
        contentValues.put("module_width", Double.valueOf(this.mModuleDimension.getWidth()));
        contentValues.put("module_height", Double.valueOf(this.mModuleDimension.getHeight()));
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    context.getContentResolver().notifyChange(URI_MAPS_LIST, null);
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.mMapId);
        parcel.writeLong(this.mLayoutId);
        parcel.writeDouble(this.mGroupVerticalSpacing);
        parcel.writeDouble(this.mGroupHorizontalSpacing);
        parcel.writeParcelable(this.mInverterDimension, i);
        parcel.writeParcelable(this.mInverter3rdPartyDimension, i);
        parcel.writeParcelable(this.mSMIDimension, i);
        parcel.writeParcelable(this.mModuleDimension, i);
    }

    private void readFromParcel(Parcel parcel) {
        this._id = parcel.readLong();
        this.mMapId = parcel.readLong();
        this.mLayoutId = parcel.readLong();
        this.mGroupVerticalSpacing = parcel.readDouble();
        this.mGroupHorizontalSpacing = parcel.readDouble();
        this.mInverterDimension = (Dimension) parcel.readParcelable(Dimension.class.getClassLoader());
        this.mInverter3rdPartyDimension = (Dimension) parcel.readParcelable(Dimension.class.getClassLoader());
        this.mSMIDimension = (Dimension) parcel.readParcelable(Dimension.class.getClassLoader());
        this.mModuleDimension = (Dimension) parcel.readParcelable(Dimension.class.getClassLoader());
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public long getMapId() {
        return this.mMapId;
    }

    public void setMapId(long j) {
        this.mMapId = j;
    }

    public double getGroupVerticalSpacing() {
        return this.mGroupVerticalSpacing;
    }

    public void setGroupVerticalSpacing(double d) {
        this.mGroupVerticalSpacing = d;
    }

    public double getGroupHorizontalSpacing() {
        return this.mGroupHorizontalSpacing;
    }

    public void setGroupHorizontalSpacing(double d) {
        this.mGroupHorizontalSpacing = d;
    }

    public Dimension getInverterDimension() {
        return this.mInverterDimension;
    }

    public void setInverterDimension(double d, double d2) {
        if (this.mInverterDimension == null) {
            this.mInverterDimension = new Dimension();
        }
        this.mInverterDimension.setDimension(d, d2);
    }

    public Dimension getInverter3rdPartyDimension() {
        return this.mInverter3rdPartyDimension;
    }

    public void setInverter3rdPartyDimension(double d, double d2) {
        if (this.mInverter3rdPartyDimension == null) {
            this.mInverter3rdPartyDimension = new Dimension();
        }
        this.mInverter3rdPartyDimension.setDimension(d, d2);
    }

    public Dimension getSMIDimension() {
        return this.mSMIDimension;
    }

    public void setSMIDimension(double d, double d2) {
        if (this.mSMIDimension == null) {
            this.mSMIDimension = new Dimension();
        }
        this.mSMIDimension.setDimension(d, d2);
    }

    public Dimension getModuleDimension() {
        return this.mModuleDimension;
    }

    public void setModuleDimension(double d, double d2) {
        if (this.mModuleDimension == null) {
            this.mModuleDimension = new Dimension();
        }
        this.mModuleDimension.setDimension(d, d2);
    }

    public long getLayoutId() {
        return this.mLayoutId;
    }

    public void setLayoutId(long j) {
        this.mLayoutId = j;
    }
}
