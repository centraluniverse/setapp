package com.solaredge.common.models.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"width", "height"})
@Root(strict = false)
public class Dimension implements Parcelable, C1370b {
    public static final Creator<Dimension> CREATOR = ParcelableCompat.newCreator(new C22011());
    public static final String SQL_CREATETE_TABLE = "CREATE TABLE Dimension( _id INTEGER PRIMARY KEY AUTOINCREMENT, width integer, height integer  ); ";
    public static final String TABLE_NAME = "Dimension";
    private long _id;
    @Element(name = "height", required = false)
    private double mHeight;
    @Element(name = "width", required = false)
    private double mWidth;

    public interface TableColumns extends BaseColumns {
        public static final String HEIGHT = "height";
        public static final String WIDTH = "width";
    }

    static class C22011 implements ParcelableCompatCreatorCallbacks<Dimension> {
        C22011() {
        }

        public Dimension createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Dimension(parcel);
        }

        public Dimension[] newArray(int i) {
            return new Dimension[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public Dimension() {
        this.mWidth = 0.0d;
        this.mHeight = 0.0d;
    }

    public Dimension(Dimension dimension) {
        setDimension(dimension.getWidth(), dimension.getHeight());
    }

    public Dimension(Cursor cursor) {
        this();
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.mWidth = cursor.getDouble(cursor.getColumnIndex("width"));
        this.mHeight = cursor.getDouble(cursor.getColumnIndex("height"));
    }

    public Dimension(Parcel parcel) {
        readFromParcel(parcel);
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("width", Double.valueOf(this.mWidth));
        contentValues.put("height", Double.valueOf(this.mHeight));
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void setDimension(double d, double d2) {
        setWidth(d);
        setHeight(d2);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeDouble(this.mWidth);
        parcel.writeDouble(this.mHeight);
    }

    private void readFromParcel(Parcel parcel) {
        this._id = parcel.readLong();
        this.mWidth = parcel.readDouble();
        this.mHeight = parcel.readDouble();
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public double getWidth() {
        return this.mWidth;
    }

    public void setWidth(double d) {
        this.mWidth = d;
    }

    public double getHeight() {
        return this.mHeight;
    }

    public void setHeight(double d) {
        this.mHeight = d;
    }
}
