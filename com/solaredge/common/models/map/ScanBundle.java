package com.solaredge.common.models.map;

import com.solaredge.common.p117h.C2191d;

public class ScanBundle {
    private C2191d mModule1;
    private C2191d mModule2;

    public ScanBundle(C2191d c2191d, C2191d c2191d2) {
        setModule1(c2191d);
        setModule2(c2191d2);
    }

    public C2191d getModule1() {
        return this.mModule1;
    }

    public void setModule1(C2191d c2191d) {
        this.mModule1 = c2191d;
    }

    public C2191d getModule2() {
        return this.mModule2;
    }

    public void setModule2(C2191d c2191d) {
        this.mModule2 = c2191d;
    }
}
