package com.solaredge.common.models.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.text.TextUtils;
import com.solaredge.common.C1366a;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"rectangle", "identifier", "serialNumber", "id"})
@Root(strict = false)
public class Inverter implements Parcelable, C1370b {
    public static final Creator<Inverter> CREATOR = ParcelableCompat.newCreator(new C22031());
    public static final String SQL_CREATE_TABLE = "CREATE TABLE Inverter( _id INTEGER PRIMARY KEY AUTOINCREMENT, map_id integer, inverter_id integer, layout_id integer, smi_id integer, is_3rd_party integer, serial_number nvarchar(30), third_party_manufacturer nvarchar(50), third_party_model nvarchar(30) );";
    public static final String TABLE_NAME = "Inverter";
    public static final Uri URI_MAPS_LIST = Uri.parse("sqlite://com.developica.solaredge.monitor/maps/list");
    protected long _id;
    @Element(name = "identifier", required = false)
    protected Inverter3rdPartyIdentifier inverter3rdPartyIdentifier;
    @Element(name = "id", required = false)
    protected Long mInverterId;
    protected boolean mIs3rdParty;
    protected long mLayoutId;
    protected long mMapId;
    @Element(name = "rectangle", required = false)
    protected Rectangle mRectangle;
    @Element(name = "serialNumber", required = false)
    protected String mSerialNumber;

    public interface TableColumns extends BaseColumns {
        public static final String INVERTER_ID = "inverter_id";
        public static final String IS_3RD_PARTY = "is_3rd_party";
        public static final String LAYOUT_ID = "layout_id";
        public static final String MAP_ID = "map_id";
        public static final String SERIAL_NUMBER = "serial_number";
        public static final String SMI_ID = "smi_id";
        public static final String THIRD_PARTY_MANUFACTURER = "third_party_manufacturer";
        public static final String THIRD_PARTY_MODEL = "third_party_model";
    }

    static class C22031 implements ParcelableCompatCreatorCallbacks<Inverter> {
        C22031() {
        }

        public Inverter createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Inverter(parcel);
        }

        public Inverter[] newArray(int i) {
            return new Inverter[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATE_TABLE};
    }

    public Inverter() {
        this.mSerialNumber = null;
        this._id = -1;
        this.mMapId = -1;
        this.mLayoutId = -1;
        this.mInverterId = null;
        this.mRectangle = new Rectangle();
        this.mIs3rdParty = false;
        this.mSerialNumber = null;
    }

    public Inverter(Inverter inverter) {
        this.mSerialNumber = null;
        this.mMapId = inverter.mMapId;
        this.mLayoutId = inverter.mLayoutId;
        if (inverter.mRectangle != null) {
            this.mRectangle = new Rectangle(inverter.mRectangle);
        }
        if (inverter.mInverterId == null || inverter.mInverterId.longValue() == 0) {
            this.mInverterId = null;
        } else {
            this.mInverterId = new Long(inverter.mInverterId.longValue());
        }
        this.mIs3rdParty = inverter.mIs3rdParty;
        this.mSerialNumber = inverter.mSerialNumber;
        if (inverter.inverter3rdPartyIdentifier != null) {
            this.inverter3rdPartyIdentifier = new Inverter3rdPartyIdentifier(inverter.inverter3rdPartyIdentifier);
        }
    }

    public Inverter(Cursor cursor) {
        this();
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.mMapId = cursor.getLong(cursor.getColumnIndex("map_id"));
        this.mLayoutId = cursor.getLong(cursor.getColumnIndex("layout_id"));
        boolean z = true;
        if (cursor.getInt(cursor.getColumnIndex(TableColumns.IS_3RD_PARTY)) != 1) {
            z = false;
        }
        this.mIs3rdParty = z;
        if (this.mIs3rdParty) {
            this.inverter3rdPartyIdentifier.setSerialNumber(cursor.getString(cursor.getColumnIndex("serial_number")));
            this.inverter3rdPartyIdentifier.setManufacturer3rdParty(cursor.getString(cursor.getColumnIndex(TableColumns.THIRD_PARTY_MANUFACTURER)));
            this.inverter3rdPartyIdentifier.setModel3rdParty(cursor.getString(cursor.getColumnIndex(TableColumns.THIRD_PARTY_MODEL)));
        } else {
            this.mSerialNumber = cursor.getString(cursor.getColumnIndex("serial_number"));
        }
        nullifySerialNumber();
        this.mInverterId = Long.valueOf(cursor.getLong(cursor.getColumnIndex("inverter_id")));
        if (this.mInverterId.longValue() == 0) {
            this.mInverterId = null;
        }
    }

    public Inverter(Parcel parcel) {
        this.mSerialNumber = null;
        readFromParcel(parcel);
    }

    public static String[] getAddFieldsStatements() {
        return new String[]{"ALTER TABLE Inverter ADD COLUMN inverter_id integer;"};
    }

    public void delete() {
        if (this.mRectangle != null) {
            this.mRectangle.delete();
        }
        C1369a.m3766b(C1366a.m3749a().m3753b()).delete(TABLE_NAME, "_id = ?", new String[]{String.valueOf(this._id)});
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("map_id", Long.valueOf(this.mMapId));
        contentValues.put("layout_id", Long.valueOf(this.mLayoutId));
        contentValues.put(TableColumns.IS_3RD_PARTY, Boolean.valueOf(this.mIs3rdParty));
        if (this.mIs3rdParty) {
            contentValues.put("serial_number", this.inverter3rdPartyIdentifier.getSerialNumber());
            contentValues.put(TableColumns.THIRD_PARTY_MANUFACTURER, this.inverter3rdPartyIdentifier.getManufacturer3rdParty());
            contentValues.put(TableColumns.THIRD_PARTY_MODEL, this.inverter3rdPartyIdentifier.getModel3rdParty());
        } else {
            contentValues.put("serial_number", this.mSerialNumber);
        }
        contentValues.put("inverter_id", this.mInverterId);
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        this.mRectangle.setInverterId(this._id);
        this.mRectangle.save(context);
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    this.mRectangle.setInverterId(this._id);
                    this.mRectangle.update(context);
                    context.getContentResolver().notifyChange(URI_MAPS_LIST, null);
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.mMapId);
        parcel.writeLong(this.mLayoutId);
        parcel.writeInt(this.mIs3rdParty);
        if (this.mIs3rdParty) {
            parcel.writeString(this.inverter3rdPartyIdentifier.getSerialNumber());
            parcel.writeString(this.inverter3rdPartyIdentifier.getManufacturer3rdParty());
            parcel.writeString(this.inverter3rdPartyIdentifier.getModel3rdParty());
        } else {
            parcel.writeString(this.mSerialNumber);
        }
        parcel.writeParcelable(this.mRectangle, i);
        parcel.writeLong(this.mInverterId == 0 ? 0 : this.mInverterId.longValue());
    }

    protected void readFromParcel(Parcel parcel) {
        this._id = parcel.readLong();
        this.mMapId = parcel.readLong();
        this.mLayoutId = parcel.readLong();
        boolean z = true;
        if (parcel.readInt() != 1) {
            z = false;
        }
        this.mIs3rdParty = z;
        if (this.mIs3rdParty) {
            this.inverter3rdPartyIdentifier = new Inverter3rdPartyIdentifier();
            this.inverter3rdPartyIdentifier.setSerialNumber(parcel.readString());
            this.inverter3rdPartyIdentifier.setManufacturer3rdParty(parcel.readString());
            this.inverter3rdPartyIdentifier.setModel3rdParty(parcel.readString());
        } else {
            this.mSerialNumber = parcel.readString();
        }
        this.mRectangle = (Rectangle) parcel.readParcelable(Rectangle.class.getClassLoader());
        nullifySerialNumber();
        this.mInverterId = Long.valueOf(parcel.readLong());
        if (this.mInverterId.longValue() == 0) {
            this.mInverterId = null;
        }
    }

    private void nullifySerialNumber() {
        if (TextUtils.isEmpty(this.mSerialNumber)) {
            this.mSerialNumber = null;
        }
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public boolean is3rdParty() {
        return this.mIs3rdParty;
    }

    public void setIs3rdParty(boolean z) {
        this.mIs3rdParty = z;
    }

    public String getSerialNumber() {
        return this.mSerialNumber;
    }

    public void setSerialNumber(String str) {
        this.mSerialNumber = str;
        nullifySerialNumber();
    }

    public Rectangle getRectangle() {
        return this.mRectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.mRectangle = rectangle;
    }

    public long getLayoutId() {
        return this.mLayoutId;
    }

    public void setLayoutId(long j) {
        this.mLayoutId = j;
    }

    public long getMapId() {
        return this.mMapId;
    }

    public void setMapId(long j) {
        this.mMapId = j;
    }

    public Inverter3rdPartyIdentifier getInverter3rdPartyIdentifier() {
        return this.inverter3rdPartyIdentifier;
    }

    public void setInverter3rdPartyIdentifier(Inverter3rdPartyIdentifier inverter3rdPartyIdentifier) {
        this.inverter3rdPartyIdentifier = inverter3rdPartyIdentifier;
    }

    public long getInverterId() {
        return this.mInverterId.longValue();
    }

    public String toString() {
        String stringBuilder;
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("InverterId: ");
        stringBuilder2.append(this.mInverterId);
        if (this.mSerialNumber != null) {
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append(" -> ");
            stringBuilder3.append(this.mSerialNumber);
            stringBuilder = stringBuilder3.toString();
        } else {
            stringBuilder = "";
        }
        stringBuilder2.append(stringBuilder);
        stringBuilder2.append("\n");
        return stringBuilder2.toString();
    }
}
