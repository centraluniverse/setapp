package com.solaredge.common.models.map;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.models.map.Inverter.TableColumns;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;

@Order(elements = {"serialNumber", "manufacturer", "model"})
public class Inverter3rdPartyIdentifier implements Parcelable {
    public static final Creator<Inverter3rdPartyIdentifier> CREATOR = ParcelableCompat.newCreator(new C22041());
    @Element(name = "manufacturer", required = false)
    private String manufacturer3rdParty;
    @Element(name = "model", required = false)
    private String model3rdParty;
    @Element(name = "serialNumber", required = false)
    private String serialNumber;

    static class C22041 implements ParcelableCompatCreatorCallbacks<Inverter3rdPartyIdentifier> {
        C22041() {
        }

        public Inverter3rdPartyIdentifier createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Inverter3rdPartyIdentifier(parcel);
        }

        public Inverter3rdPartyIdentifier[] newArray(int i) {
            return new Inverter3rdPartyIdentifier[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public Inverter3rdPartyIdentifier(Parcel parcel) {
        readFromParcel(parcel);
    }

    public Inverter3rdPartyIdentifier(Cursor cursor) {
        this.serialNumber = cursor.getString(cursor.getColumnIndex("serial_number"));
        this.manufacturer3rdParty = cursor.getString(cursor.getColumnIndex(TableColumns.THIRD_PARTY_MANUFACTURER));
        this.model3rdParty = cursor.getString(cursor.getColumnIndex(TableColumns.THIRD_PARTY_MODEL));
    }

    public Inverter3rdPartyIdentifier(Inverter3rdPartyIdentifier inverter3rdPartyIdentifier) {
        this.serialNumber = inverter3rdPartyIdentifier.serialNumber;
        this.manufacturer3rdParty = inverter3rdPartyIdentifier.manufacturer3rdParty;
        this.model3rdParty = inverter3rdPartyIdentifier.model3rdParty;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.serialNumber);
        parcel.writeString(this.manufacturer3rdParty);
        parcel.writeString(this.model3rdParty);
    }

    private void readFromParcel(Parcel parcel) {
        this.serialNumber = parcel.readString();
        this.manufacturer3rdParty = parcel.readString();
        this.model3rdParty = parcel.readString();
    }

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(String str) {
        this.serialNumber = str;
    }

    public String getManufacturer3rdParty() {
        return this.manufacturer3rdParty;
    }

    public void setManufacturer3rdParty(String str) {
        this.manufacturer3rdParty = str;
    }

    public String getModel3rdParty() {
        return this.model3rdParty;
    }

    public void setModel3rdParty(String str) {
        this.model3rdParty = str;
    }
}
