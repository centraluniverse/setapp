package com.solaredge.common.models.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.C1366a;
import com.solaredge.common.models.Inverter3rdParty;
import com.solaredge.common.models.SolarSite;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import com.solaredge.common.p114e.C1393e;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"fieldType", "siteDefaultDimension", "inverter", "inverter3rdParty", "smi", "group", "versionId"})
@Root(name = "physicalLayout", strict = false)
public class PhysicalLayout implements Parcelable, C1370b {
    public static final Creator<PhysicalLayout> CREATOR = ParcelableCompat.newCreator(new C22061());
    public static final String FIELD_TYPE_SMI = "smi";
    public static final String FIELD_TYPE_SOLAREDGE = "solaredge";
    public static final String SQL_CREATE_TABLE = "CREATE TABLE PhysicalLayout( _id INTEGER PRIMARY KEY AUTOINCREMENT, map_id integer, field_type nvarchar(20), is_base integer DEFAULT 0, versionId integer DEFAULT 0); ";
    public static final String TABLE_NAME = "PhysicalLayout";
    private long _id;
    private HashMap<Long, Inverter3rdParty> m3rdPartyInverters;
    @Element(name = "fieldType", required = false)
    private String mFieldType;
    private HashMap<Long, Group> mGroups;
    private HashMap<Long, Inverter> mInverters;
    private int mIsBase;
    private long mMapId;
    private HashMap<Long, SMI> mSMIs;
    @Element(name = "siteDefaultDimension", required = false)
    private SiteDefaultDimension mSiteDefaultDimension;
    @Element(name = "versionId", required = false)
    private Long mVersionId;
    @ElementList(entry = "inverter3rdParty", inline = true, required = false)
    private List<Inverter3rdParty> ml3rdPartyInverters;
    @ElementList(entry = "group", inline = true, required = false)
    private List<Group> mlGroups;
    @ElementList(entry = "inverter", inline = true, required = false)
    private List<Inverter> mlInverters;
    @ElementList(entry = "smi", inline = true, required = false)
    private List<SMI> mlSMIs;

    public interface TableColumns extends BaseColumns {
        public static final String FIELD_TYPE = "field_type";
        public static final String IS_BASE = "is_base";
        public static final String MAP_ID = "map_id";
        public static final String VERSION_ID = "versionId";
    }

    static class C22061 implements ParcelableCompatCreatorCallbacks<PhysicalLayout> {
        C22061() {
        }

        public PhysicalLayout createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new PhysicalLayout(parcel);
        }

        public PhysicalLayout[] newArray(int i) {
            return new PhysicalLayout[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public static String[] getAddFieldsStatements() {
        return new String[]{"ALTER TABLE PhysicalLayout ADD COLUMN versionId integer DEFAULT 0;", "ALTER TABLE PhysicalLayout ADD COLUMN is_base integer DEFAULT 0;"};
    }

    public void delete() {
        C1369a.m3766b(C1366a.m3749a().m3753b()).beginTransaction();
        try {
            Collection<Inverter> values = (this.mInverters == null || this.mInverters.size() <= 0) ? this.mlInverters : this.mInverters.values();
            if (values != null && values.size() > 0) {
                for (Inverter delete : values) {
                    delete.delete();
                }
            }
            values = (this.m3rdPartyInverters == null || this.m3rdPartyInverters.size() <= 0) ? this.ml3rdPartyInverters : this.m3rdPartyInverters.values();
            if (values != null && values.size() > 0) {
                for (Inverter delete2 : values) {
                    delete2.delete();
                }
            }
            Collection<SMI> values2 = (this.mSMIs == null || this.mSMIs.size() <= 0) ? this.mlSMIs : this.mSMIs.values();
            if (values2 != null && values2.size() > 0) {
                for (SMI delete3 : values2) {
                    delete3.delete();
                }
            }
            Collection<Group> values3 = (this.mGroups == null || this.mGroups.size() <= 0) ? this.mlGroups : this.mGroups.values();
            if (values3 != null && values3.size() > 0) {
                for (Group delete4 : values3) {
                    delete4.delete();
                }
            }
            if (this.mSiteDefaultDimension != null) {
                this.mSiteDefaultDimension.delete();
            }
            C1369a.m3766b(C1366a.m3749a().m3753b()).delete(TABLE_NAME, "_id = ?", new String[]{String.valueOf(this._id)});
            C1369a.m3766b(C1366a.m3749a().m3753b()).setTransactionSuccessful();
        } finally {
            C1369a.m3766b(C1366a.m3749a().m3753b()).endTransaction();
        }
    }

    public void setVersionId(long j) {
        this.mVersionId = Long.valueOf(j);
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATE_TABLE};
    }

    public PhysicalLayout() {
        this.mInverters = new HashMap();
        this.m3rdPartyInverters = new HashMap();
        this.mSMIs = new HashMap();
        this.mGroups = new HashMap();
        this.mFieldType = FIELD_TYPE_SOLAREDGE;
        this.mVersionId = null;
        this.mSiteDefaultDimension = new SiteDefaultDimension();
        this.mInverters = new HashMap();
        this.m3rdPartyInverters = new HashMap();
        this.mSMIs = new HashMap();
        this.mGroups = new HashMap();
        this.mIsBase = 0;
    }

    public PhysicalLayout(PhysicalLayout physicalLayout) {
        this.mInverters = new HashMap();
        this.m3rdPartyInverters = new HashMap();
        this.mSMIs = new HashMap();
        this.mGroups = new HashMap();
        if (physicalLayout.mVersionId == null || physicalLayout.mVersionId.longValue() == 0) {
            this.mVersionId = null;
        } else {
            this.mVersionId = new Long(physicalLayout.mVersionId.longValue());
        }
        this.mFieldType = physicalLayout.mFieldType;
        if (physicalLayout.mSiteDefaultDimension != null) {
            this.mSiteDefaultDimension = new SiteDefaultDimension(physicalLayout.mSiteDefaultDimension);
        }
        this.mMapId = physicalLayout.mMapId;
        this.mIsBase = physicalLayout.mIsBase;
        if (physicalLayout.mlInverters != null) {
            this.mlInverters = new ArrayList();
            for (Inverter inverter : physicalLayout.mlInverters) {
                this.mlInverters.add(new Inverter(inverter));
            }
        }
        if (physicalLayout.mlGroups != null) {
            this.mlGroups = new ArrayList();
            for (Group group : physicalLayout.mlGroups) {
                this.mlGroups.add(new Group(group));
            }
        }
        if (physicalLayout.ml3rdPartyInverters != null) {
            this.ml3rdPartyInverters = new ArrayList();
            for (Inverter3rdParty inverter3rdParty : physicalLayout.ml3rdPartyInverters) {
                this.ml3rdPartyInverters.add(new Inverter3rdParty(inverter3rdParty));
            }
        }
        if (physicalLayout.mlSMIs != null) {
            this.mlSMIs = new ArrayList();
            for (SMI smi : physicalLayout.mlSMIs) {
                this.mlSMIs.add(new SMI(smi));
            }
        }
    }

    public PhysicalLayout(Cursor cursor) {
        this();
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.mMapId = cursor.getLong(cursor.getColumnIndex("map_id"));
        this.mFieldType = cursor.getString(cursor.getColumnIndex("field_type"));
        this.mVersionId = Long.valueOf(cursor.getLong(cursor.getColumnIndex(TableColumns.VERSION_ID)));
        this.mIsBase = cursor.getInt(cursor.getColumnIndex(TableColumns.IS_BASE));
    }

    public PhysicalLayout(Parcel parcel) {
        this();
        readFromParcel(parcel);
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("map_id", Long.valueOf(this.mMapId));
        contentValues.put("field_type", this.mFieldType);
        contentValues.put(TableColumns.VERSION_ID, this.mVersionId);
        contentValues.put(TableColumns.IS_BASE, Integer.valueOf(this.mIsBase));
        return contentValues;
    }

    public long save(Context context) {
        C1369a.m3766b(context).beginTransaction();
        try {
            this._id = C1369a.m3765a(context).m3768a((C1370b) this);
            C1393e.m3801a().m3811b().setMapLastModified(Calendar.getInstance());
            C1393e.m3801a().m3811b().update(context);
            this.mSiteDefaultDimension.setLayoutId(this._id);
            this.mSiteDefaultDimension.setMapId(this.mMapId);
            if (this.mSiteDefaultDimension != null) {
                this.mSiteDefaultDimension.save(context);
            }
            if (this.mInverters != null && this.mInverters.size() > 0) {
                for (Inverter inverter : this.mInverters.values()) {
                    inverter.setMapId(this.mMapId);
                    inverter.setLayoutId(this._id);
                    inverter.save(context);
                }
            }
            if (this.m3rdPartyInverters != null && this.m3rdPartyInverters.size() > 0) {
                for (Inverter inverter2 : this.m3rdPartyInverters.values()) {
                    inverter2.setMapId(this.mMapId);
                    inverter2.setLayoutId(this._id);
                    inverter2.save(context);
                }
            }
            if (this.mSMIs != null && this.mSMIs.size() > 0) {
                for (SMI smi : this.mSMIs.values()) {
                    smi.setMapId(this.mMapId);
                    smi.setLayoutId(this._id);
                    smi.save(context);
                }
            }
            if (this.mGroups != null && this.mGroups.size() > 0) {
                for (Group group : this.mGroups.values()) {
                    group.setMapId(this.mMapId);
                    group.setLayoutId(this._id);
                    group.save(context);
                }
            }
            C1369a.m3766b(context).setTransactionSuccessful();
            return this._id;
        } finally {
            C1369a.m3766b(context).endTransaction();
        }
    }

    public void createHiddenModules() {
        if (this.mlGroups != null) {
            for (Group group : this.mlGroups) {
                for (int i = 0; i < group.getRows(); i++) {
                    int i2 = 0;
                    while (i2 < group.getColumns()) {
                        boolean z;
                        if (group.getMlModules() != null) {
                            for (Module module : group.getMlModules()) {
                                if (module.getRow() == i && module.getColumn() == i2) {
                                    z = false;
                                    break;
                                }
                            }
                        }
                        z = true;
                        if (z) {
                            Module module2 = new Module();
                            module2.setColumn(i2);
                            module2.setRow(i);
                            module2.setEnabled(false);
                            group.addMlModule(module2);
                        }
                        i2++;
                    }
                }
            }
        }
    }

    public void update(Context context, Calendar calendar) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).beginTransaction();
                    SolarSite b = C1393e.m3801a().m3811b();
                    if (calendar == null) {
                        calendar = Calendar.getInstance();
                    }
                    b.setMapLastModified(calendar);
                    C1393e.m3801a().m3811b().update(context);
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    this.mSiteDefaultDimension.setLayoutId(this._id);
                    this.mSiteDefaultDimension.setMapId(this.mMapId);
                    if (this.mSiteDefaultDimension != null) {
                        this.mSiteDefaultDimension.save(context);
                    }
                    if (this.mInverters != null && this.mInverters.size() > null) {
                        for (Inverter update : this.mInverters.values()) {
                            update.update(context);
                        }
                    }
                    if (this.m3rdPartyInverters != null && this.m3rdPartyInverters.size() > null) {
                        for (Inverter update2 : this.m3rdPartyInverters.values()) {
                            update2.update(context);
                        }
                    }
                    if (this.mSMIs != null && this.mSMIs.size() > null) {
                        for (SMI update3 : this.mSMIs.values()) {
                            update3.update(context);
                        }
                    }
                    if (this.mGroups != null && this.mGroups.size() > null) {
                        for (Group update4 : this.mGroups.values()) {
                            update4.update(context);
                        }
                    }
                    C1369a.m3766b(context).setTransactionSuccessful();
                    C1369a.m3766b(context).endTransaction();
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void update(Context context) {
        update(context, null);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.mMapId);
        parcel.writeString(this.mFieldType);
        parcel.writeParcelable(this.mSiteDefaultDimension, i);
        parcel.writeSerializable(this.mInverters);
        parcel.writeTypedList(this.mlInverters);
        parcel.writeSerializable(this.m3rdPartyInverters);
        parcel.writeSerializable(this.mSMIs);
        parcel.writeSerializable(this.mGroups);
        parcel.writeTypedList(this.mlGroups);
        parcel.writeLong(this.mVersionId == 0 ? 0 : this.mVersionId.longValue());
        parcel.writeInt(this.mIsBase);
    }

    private void readFromParcel(Parcel parcel) {
        this._id = parcel.readLong();
        this.mMapId = parcel.readLong();
        this.mFieldType = parcel.readString();
        this.mSiteDefaultDimension = (SiteDefaultDimension) parcel.readParcelable(SiteDefaultDimension.class.getClassLoader());
        this.mInverters = (HashMap) parcel.readSerializable();
        this.mlInverters = new ArrayList();
        parcel.readTypedList(this.mlInverters, Inverter.CREATOR);
        this.m3rdPartyInverters = (HashMap) parcel.readSerializable();
        this.mSMIs = (HashMap) parcel.readSerializable();
        this.mGroups = (HashMap) parcel.readSerializable();
        this.mlGroups = new ArrayList();
        parcel.readTypedList(this.mlGroups, Group.CREATOR);
        this.mVersionId = Long.valueOf(parcel.readLong());
        this.mIsBase = parcel.readInt();
    }

    public long saveFromAPI(Context context) {
        C1369a.m3766b(context).beginTransaction();
        try {
            this._id = C1369a.m3765a(context).m3768a((C1370b) this);
            this.mSiteDefaultDimension.setLayoutId(this._id);
            this.mSiteDefaultDimension.setMapId(this.mMapId);
            if (this.mSiteDefaultDimension != null) {
                this.mSiteDefaultDimension.save(context);
            }
            if (this.mlInverters != null && this.mlInverters.size() > 0) {
                for (Inverter inverter : this.mlInverters) {
                    inverter.setMapId(this.mMapId);
                    inverter.setLayoutId(this._id);
                    inverter.save(context);
                    this.mInverters.put(Long.valueOf(inverter.getId()), inverter);
                }
            }
            if (this.ml3rdPartyInverters != null && this.ml3rdPartyInverters.size() > 0) {
                for (Inverter3rdParty inverter3rdParty : this.ml3rdPartyInverters) {
                    inverter3rdParty.setMapId(this.mMapId);
                    inverter3rdParty.setLayoutId(this._id);
                    inverter3rdParty.setIs3rdParty(true);
                    inverter3rdParty.save(context);
                    this.m3rdPartyInverters.put(Long.valueOf(inverter3rdParty.getId()), inverter3rdParty);
                }
            }
            if (this.mlSMIs != null && this.mlSMIs.size() > 0) {
                for (SMI smi : this.mlSMIs) {
                    smi.setMapId(this.mMapId);
                    smi.setLayoutId(this._id);
                    smi.save(context);
                    this.mSMIs.put(Long.valueOf(smi.getId()), smi);
                }
            }
            if (this.mlGroups != null && this.mlGroups.size() > 0) {
                for (Group group : this.mlGroups) {
                    group.setMapId(this.mMapId);
                    group.setLayoutId(this._id);
                    group.saveFromAPI(context);
                    this.mGroups.put(Long.valueOf(group.getId()), group);
                }
            }
            C1369a.m3766b(context).setTransactionSuccessful();
            return this._id;
        } finally {
            C1369a.m3766b(context).endTransaction();
        }
    }

    public void prepareForPublish() {
        if (this.mInverters != null && this.mInverters.size() > 0) {
            if (this.mlInverters == null) {
                this.mlInverters = new ArrayList(this.mInverters.size());
            } else {
                this.mlInverters.clear();
            }
            for (Inverter add : this.mInverters.values()) {
                this.mlInverters.add(add);
            }
        }
        if (this.m3rdPartyInverters != null && this.m3rdPartyInverters.size() > 0) {
            if (this.ml3rdPartyInverters == null) {
                this.ml3rdPartyInverters = new ArrayList(this.m3rdPartyInverters.size());
            } else {
                this.ml3rdPartyInverters.clear();
            }
            for (Inverter3rdParty add2 : this.m3rdPartyInverters.values()) {
                this.ml3rdPartyInverters.add(add2);
            }
        }
        if (this.mSMIs != null && this.mSMIs.size() > 0) {
            if (this.mlSMIs == null) {
                this.mlSMIs = new ArrayList(this.mSMIs.size());
            } else {
                this.mlSMIs.clear();
            }
            for (SMI add3 : this.mSMIs.values()) {
                this.mlSMIs.add(add3);
            }
        }
        if (this.mGroups != null && this.mGroups.size() > 0) {
            if (this.mlGroups == null) {
                this.mlGroups = new ArrayList(this.mGroups.size());
            } else {
                this.mlGroups.clear();
            }
            for (Group group : this.mGroups.values()) {
                if (group.prepareForPublish()) {
                    this.mlGroups.add(group);
                }
            }
        }
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public long getMapId() {
        return this.mMapId;
    }

    public void setMapId(long j) {
        this.mMapId = j;
    }

    public int getIsBase() {
        return this.mIsBase;
    }

    public void setIsBase(int i) {
        this.mIsBase = i;
    }

    public SiteDefaultDimension getSiteDefaultDimension() {
        return this.mSiteDefaultDimension;
    }

    public void setSiteDefaultDimension(SiteDefaultDimension siteDefaultDimension) {
        this.mSiteDefaultDimension = siteDefaultDimension;
    }

    public HashMap<Long, Inverter> getInverters() {
        return this.mInverters;
    }

    public void addInverter(Inverter inverter) {
        if (this.mInverters == null) {
            this.mInverters = new HashMap();
        }
        this.mInverters.put(Long.valueOf(inverter.getId()), inverter);
    }

    public void removeInverter(Inverter inverter) {
        if (this.mInverters != null) {
            this.mInverters.remove(Long.valueOf(inverter.getId()));
        }
        if (this.mlInverters != null) {
            this.mlInverters.remove(inverter);
        }
        inverter.delete();
    }

    public void setInverters(HashMap<Long, Inverter> hashMap) {
        this.mInverters = new HashMap(hashMap);
    }

    public HashMap<Long, Inverter3rdParty> get3rdPartyInverters() {
        return this.m3rdPartyInverters;
    }

    public void add3rdPartyInverter(Inverter3rdParty inverter3rdParty) {
        if (this.m3rdPartyInverters == null) {
            this.m3rdPartyInverters = new HashMap();
        }
        this.m3rdPartyInverters.put(Long.valueOf(inverter3rdParty.getId()), inverter3rdParty);
    }

    public void remove3rdPartyInverter(Inverter inverter) {
        if (this.m3rdPartyInverters != null) {
            this.m3rdPartyInverters.remove(Long.valueOf(inverter.getId()));
        }
        if (this.ml3rdPartyInverters != null) {
            this.ml3rdPartyInverters.remove(inverter);
        }
        inverter.delete();
    }

    public void set3rdPartyInverters(HashMap<Long, Inverter3rdParty> hashMap) {
        this.m3rdPartyInverters = new HashMap(hashMap);
    }

    public HashMap<Long, SMI> getSMIs() {
        return this.mSMIs;
    }

    public void addSMI(SMI smi) {
        if (this.mSMIs == null) {
            this.mSMIs = new HashMap();
        }
        this.mSMIs.put(Long.valueOf(smi.getId()), smi);
    }

    public void removeSMI(SMI smi) {
        if (this.mSMIs != null) {
            this.mSMIs.remove(Long.valueOf(smi.getId()));
        }
        if (this.mlSMIs != null) {
            this.mlSMIs.remove(smi);
        }
        smi.delete();
    }

    public void setSMIs(HashMap<Long, SMI> hashMap) {
        this.mSMIs = new HashMap(hashMap);
    }

    public HashMap<Long, Group> getGroups() {
        return this.mGroups;
    }

    public void addGroup(Group group) {
        if (this.mGroups == null) {
            this.mGroups = new HashMap();
        }
        this.mGroups.put(Long.valueOf(group.getId()), group);
    }

    public void removeGroup(Group group) {
        if (this.mGroups != null) {
            this.mGroups.remove(Long.valueOf(group.getId()));
        }
        if (this.mlGroups != null) {
            this.mlGroups.remove(group);
        }
        group.delete();
    }

    public void setGroups(HashMap<Long, Group> hashMap) {
        this.mGroups = new HashMap(hashMap);
    }

    public String getFieldType() {
        return this.mFieldType;
    }

    public void setFieldType(String str) {
        this.mFieldType = str;
    }

    public List<Inverter> getMlInverters() {
        return this.mlInverters;
    }

    public void setMlInverters(List<Inverter> list) {
        this.mlInverters = list;
    }

    public List<Inverter3rdParty> getMl3rdPartyInverters() {
        return this.ml3rdPartyInverters;
    }

    public void setMl3rdPartyInverters(List<Inverter3rdParty> list) {
        this.ml3rdPartyInverters = list;
    }

    public List<SMI> getMlSMIs() {
        return this.mlSMIs;
    }

    public void setMlSMIs(List<SMI> list) {
        this.mlSMIs = list;
    }

    public List<Group> getMlGroups() {
        return this.mlGroups;
    }

    public void setMlGroups(List<Group> list) {
        this.mlGroups = list;
    }

    public Long getVersionId() {
        return Long.valueOf(this.mVersionId == null ? 0 : this.mVersionId.longValue());
    }

    public String toString() {
        String str = "";
        if (this.mlGroups != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("Groups: ");
            stringBuilder.append(this.mlGroups.size());
            stringBuilder.append("\n");
            str = stringBuilder.toString();
            for (Group group : this.mlGroups) {
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append(str);
                stringBuilder2.append(group.toString());
                str = stringBuilder2.toString();
            }
        }
        if (this.mlInverters != null) {
            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("Inverters: ");
            stringBuilder.append(this.mlInverters.size());
            stringBuilder.append("\n");
            str = stringBuilder.toString();
            for (Inverter inverter : this.mlInverters) {
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append(str);
                stringBuilder2.append(inverter.toString());
                str = stringBuilder2.toString();
            }
        }
        return str;
    }
}
