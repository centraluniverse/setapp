package com.solaredge.common.models.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.text.TextUtils;
import com.solaredge.common.C1366a;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import java.io.Serializable;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"serialNumber", "row", "column"})
@Root(strict = false)
public class Module implements Parcelable, C1370b, Serializable {
    public static final Creator<Module> CREATOR = ParcelableCompat.newCreator(new C22051());
    public static final String SQL_CREATETE_TABLE = "CREATE TABLE Module( _id INTEGER PRIMARY KEY AUTOINCREMENT, group_id integer, serial_number nvarchar(30), row integer, column integer, enabled integer ); ";
    public static final String TABLE_NAME = "Module";
    private static final long serialVersionUID = 0;
    private long _id;
    @Element(name = "column", required = false)
    private int mColumn;
    private boolean mEnabled;
    private long mGroupId;
    private boolean mIsSelected;
    @Element(name = "row", required = false)
    private int mRow;
    @Element(name = "serialNumber", required = false)
    private String mSerialNumber;

    public interface TableColumns extends BaseColumns {
        public static final String COLUMN = "column";
        public static final String ENABLED = "enabled";
        public static final String GROUP_ID = "group_id";
        public static final String ROW = "row";
        public static final String SERIAL_NUMBER = "serial_number";
    }

    static class C22051 implements ParcelableCompatCreatorCallbacks<Module> {
        C22051() {
        }

        public Module createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Module(parcel);
        }

        public Module[] newArray(int i) {
            return new Module[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public void setIsSelected(boolean z) {
        this.mIsSelected = z;
    }

    public boolean isSelected() {
        return this.mIsSelected;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public Module() {
        this.mIsSelected = false;
        this.mSerialNumber = null;
        this.mRow = -1;
        this.mColumn = -1;
        this.mEnabled = true;
    }

    public Module(String str, int i, int i2) {
        this.mIsSelected = false;
        this.mSerialNumber = str;
        this.mRow = i;
        this.mColumn = i2;
        this.mEnabled = true;
        nullifySerialNumber();
    }

    public Module(Module module) {
        this.mIsSelected = false;
        this.mSerialNumber = module.mSerialNumber;
        this.mRow = module.mRow;
        this.mColumn = module.mColumn;
        this.mEnabled = module.mEnabled;
        this.mGroupId = module.mGroupId;
    }

    public Module(Parcel parcel) {
        boolean z = false;
        this.mIsSelected = false;
        this._id = parcel.readLong();
        this.mGroupId = parcel.readLong();
        this.mSerialNumber = parcel.readString();
        this.mRow = parcel.readInt();
        this.mColumn = parcel.readInt();
        if (parcel.readInt() == 1) {
            z = true;
        }
        this.mEnabled = z;
        nullifySerialNumber();
    }

    public Module(Cursor cursor) {
        this();
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.mGroupId = cursor.getLong(cursor.getColumnIndex("group_id"));
        this.mSerialNumber = cursor.getString(cursor.getColumnIndex("serial_number"));
        this.mRow = cursor.getInt(cursor.getColumnIndex(TableColumns.ROW));
        this.mColumn = cursor.getInt(cursor.getColumnIndex(TableColumns.COLUMN));
        boolean z = true;
        if (cursor.getInt(cursor.getColumnIndex(TableColumns.ENABLED)) != 1) {
            z = false;
        }
        this.mEnabled = z;
        nullifySerialNumber();
    }

    public void delete() {
        C1369a.m3766b(C1366a.m3749a().m3753b()).delete(TABLE_NAME, "_id = ?", new String[]{String.valueOf(this._id)});
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("group_id", Long.valueOf(this.mGroupId));
        contentValues.put("serial_number", this.mSerialNumber);
        contentValues.put(TableColumns.ROW, Integer.valueOf(this.mRow));
        contentValues.put(TableColumns.COLUMN, Integer.valueOf(this.mColumn));
        contentValues.put(TableColumns.ENABLED, Integer.valueOf(this.mEnabled));
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.mGroupId);
        parcel.writeString(this.mSerialNumber);
        parcel.writeInt(this.mRow);
        parcel.writeInt(this.mColumn);
        parcel.writeInt(this.mEnabled);
    }

    public String toString() {
        String stringBuilder;
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("(");
        stringBuilder2.append(this.mRow);
        stringBuilder2.append(",");
        stringBuilder2.append(this.mColumn);
        stringBuilder2.append(")");
        if (this.mSerialNumber != null) {
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("->");
            stringBuilder3.append(this.mSerialNumber);
            stringBuilder = stringBuilder3.toString();
        } else {
            stringBuilder = ";";
        }
        stringBuilder2.append(stringBuilder);
        return stringBuilder2.toString();
    }

    private void nullifySerialNumber() {
        if (TextUtils.isEmpty(this.mSerialNumber)) {
            this.mSerialNumber = null;
        }
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public String getSerialNumber() {
        return this.mSerialNumber;
    }

    public void setSerialNumber(String str) {
        this.mSerialNumber = str;
        nullifySerialNumber();
    }

    public int getRow() {
        return this.mRow;
    }

    public void setRow(int i) {
        this.mRow = i;
    }

    public int getColumn() {
        return this.mColumn;
    }

    public void setColumn(int i) {
        this.mColumn = i;
    }

    public boolean isEnabled() {
        return this.mEnabled;
    }

    public void setEnabled(boolean z) {
        this.mEnabled = z;
    }

    public long getGroupId() {
        return this.mGroupId;
    }

    public void setGroupId(long j) {
        this.mGroupId = j;
    }
}
