package com.solaredge.common.models.map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.text.TextUtils;
import com.solaredge.common.C1366a;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"rectangle", "moduleOrientation", "moduleTilt", "moduleWidth", "moduleHeight", "vSpacing", "hSpacing", "rows", "columns", "module", "id"})
@Root(name = "group", strict = false)
public class Group implements Parcelable, C1370b {
    public static final Creator<Group> CREATOR = ParcelableCompat.newCreator(new C22021());
    public static final String SQL_CREATE_TABLE = "CREATE TABLE ModuleGroup( _id INTEGER PRIMARY KEY AUTOINCREMENT, map_id integer, layout_id integer, group_id integer, module_orientation integer, module_tilt double, module_width double, module_height double, vertical_spacing double, horizontal_spacing double, rows integer, columns integer  ); ";
    public static final String TABLE_NAME = "ModuleGroup";
    private long _id;
    @Element(name = "columns", required = false)
    private int mColumns;
    @Element(name = "id", required = false)
    private Long mGroupId;
    @Element(name = "hSpacing", required = false)
    private double mHorizontalSpacing;
    private long mLayoutId;
    private long mMapId;
    @Element(name = "moduleHeight", required = false)
    private double mModuleHeight;
    @Element(name = "moduleOrientation", required = false)
    private ModuleOrientation mModuleOrientation;
    @Element(name = "moduleTilt", required = false)
    private double mModuleTilt;
    @Element(name = "moduleWidth", required = false)
    private double mModuleWidth;
    private HashMap<Long, Module> mModules;
    private Module[][] mModulesGrid;
    @Element(name = "rectangle", required = false)
    private Rectangle mRectangle;
    @Element(name = "rows", required = false)
    private int mRows;
    @Element(name = "vSpacing", required = false)
    private double mVerticalSpacing;
    @ElementList(entry = "module", inline = true, required = false)
    private List<Module> mlModules;

    public enum ModuleOrientation {
        horizontal("horizontal"),
        vertical("vertical");
        
        private String orientation;

        private ModuleOrientation(String str) {
            this.orientation = str;
        }

        public String getOrientation() {
            return this.orientation;
        }
    }

    public interface TableColumns extends BaseColumns {
        public static final String COLUMNS = "columns";
        public static final String GROUP_ID = "group_id";
        public static final String HORIZONTAL_SPACING = "horizontal_spacing";
        public static final String LAYOUT_ID = "layout_id";
        public static final String MAP_ID = "map_id";
        public static final String MODULE_HEIGHT = "module_height";
        public static final String MODULE_ORIENTATION = "module_orientation";
        public static final String MODULE_TILT = "module_tilt";
        public static final String MODULE_WIDTH = "module_width";
        public static final String ROWS = "rows";
        public static final String VERTICAL_SPACING = "vertical_spacing";
    }

    static class C22021 implements ParcelableCompatCreatorCallbacks<Group> {
        C22021() {
        }

        public Group createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Group(parcel);
        }

        public Group[] newArray(int i) {
            return new Group[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public double getHorizontalSpacing() {
        return 4.0d;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public double getVerticalSpacing() {
        return 4.0d;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATE_TABLE};
    }

    public Group() {
        this.mModules = new HashMap();
        this.mModulesGrid = (Module[][]) null;
        this.mGroupId = null;
        this.mMapId = -1;
        this.mLayoutId = -1;
        this.mRectangle = new Rectangle();
        this.mModuleOrientation = ModuleOrientation.horizontal;
        this.mModuleTilt = 0.0d;
        this.mModuleWidth = 60.0d;
        this.mModuleHeight = 90.0d;
        this.mVerticalSpacing = 4.0d;
        this.mHorizontalSpacing = 4.0d;
        this.mRows = 0;
        this.mColumns = 0;
        this.mModules = new HashMap();
    }

    public Group(Group group) {
        this.mModules = new HashMap();
        this.mModulesGrid = (Module[][]) null;
        this.mMapId = group.mMapId;
        this.mLayoutId = group.mLayoutId;
        if (group.mRectangle != null) {
            this.mRectangle = new Rectangle(group.mRectangle);
        }
        if (group.mGroupId == null || group.mGroupId.longValue() == 0) {
            this.mGroupId = null;
        } else {
            this.mGroupId = new Long(group.mGroupId.longValue());
        }
        this.mModuleOrientation = group.mModuleOrientation;
        this.mModuleTilt = group.mModuleTilt;
        this.mModuleWidth = group.mModuleWidth;
        this.mModuleHeight = group.mModuleHeight;
        this.mVerticalSpacing = group.mVerticalSpacing;
        this.mHorizontalSpacing = group.mHorizontalSpacing;
        this.mRows = group.mRows;
        this.mColumns = group.mColumns;
        if (group.mlModules != null) {
            this.mlModules = new ArrayList();
            for (Module module : group.mlModules) {
                this.mlModules.add(new Module(module));
            }
        }
    }

    public Group(Parcel parcel) {
        this.mModules = new HashMap();
        this.mModulesGrid = (Module[][]) null;
        this.mModules = new HashMap();
        this._id = parcel.readLong();
        this.mMapId = parcel.readLong();
        this.mLayoutId = parcel.readLong();
        this.mRectangle = (Rectangle) parcel.readParcelable(Rectangle.class.getClassLoader());
        this.mModuleOrientation = parcel.readInt() == 0 ? ModuleOrientation.horizontal : ModuleOrientation.vertical;
        this.mModuleTilt = parcel.readDouble();
        this.mModuleWidth = parcel.readDouble();
        this.mModuleHeight = parcel.readDouble();
        this.mVerticalSpacing = parcel.readDouble();
        this.mHorizontalSpacing = parcel.readDouble();
        this.mRows = parcel.readInt();
        this.mColumns = parcel.readInt();
        this.mModules = (HashMap) parcel.readSerializable();
        this.mlModules = new ArrayList();
        parcel.readTypedList(this.mlModules, Module.CREATOR);
        this.mGroupId = Long.valueOf(parcel.readLong());
        if (this.mGroupId.longValue() == 0) {
            this.mGroupId = null;
        }
    }

    public Group(Cursor cursor) {
        this();
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.mMapId = cursor.getLong(cursor.getColumnIndex("map_id"));
        this.mLayoutId = cursor.getLong(cursor.getColumnIndex("layout_id"));
        this.mModuleOrientation = cursor.getInt(cursor.getColumnIndex(TableColumns.MODULE_ORIENTATION)) == 0 ? ModuleOrientation.horizontal : ModuleOrientation.vertical;
        this.mModuleTilt = cursor.getDouble(cursor.getColumnIndex(TableColumns.MODULE_TILT));
        this.mModuleWidth = cursor.getDouble(cursor.getColumnIndex("module_width"));
        this.mModuleHeight = cursor.getDouble(cursor.getColumnIndex("module_height"));
        this.mVerticalSpacing = cursor.getDouble(cursor.getColumnIndex(TableColumns.VERTICAL_SPACING));
        this.mHorizontalSpacing = cursor.getDouble(cursor.getColumnIndex(TableColumns.HORIZONTAL_SPACING));
        this.mRows = cursor.getInt(cursor.getColumnIndex(TableColumns.ROWS));
        this.mColumns = cursor.getInt(cursor.getColumnIndex(TableColumns.COLUMNS));
        this.mGroupId = Long.valueOf(cursor.getLong(cursor.getColumnIndex("group_id")));
        if (this.mGroupId.longValue() == 0) {
            this.mGroupId = null;
        }
    }

    public static String[] getAddFieldsStatements() {
        return new String[]{"ALTER TABLE ModuleGroup ADD COLUMN group_id integer;"};
    }

    public void delete() {
        C1369a.m3766b(C1366a.m3749a().m3753b()).beginTransaction();
        try {
            if (this.mRectangle != null) {
                this.mRectangle.delete();
            }
            Collection<Module> values = (this.mModules == null || this.mModules.size() <= 0) ? this.mlModules : this.mModules.values();
            if (values != null && values.size() > 0) {
                for (Module delete : values) {
                    delete.delete();
                }
            }
            C1369a.m3766b(C1366a.m3749a().m3753b()).delete(TABLE_NAME, "_id = ?", new String[]{String.valueOf(this._id)});
            C1369a.m3766b(C1366a.m3749a().m3753b()).setTransactionSuccessful();
        } finally {
            C1369a.m3766b(C1366a.m3749a().m3753b()).endTransaction();
        }
    }

    public boolean hasUnscannedModules() {
        for (Module module : this.mModules.values()) {
            if (module.isEnabled() && TextUtils.isEmpty(module.getSerialNumber())) {
                return true;
            }
        }
        return false;
    }

    public Module getModuleAt(int i, int i2) {
        if (i >= 0 && i2 >= 0 && i < getModulesGrid().length) {
            if (i2 < getModulesGrid()[i].length) {
                return getModulesGrid()[i][i2];
            }
        }
        return 0;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("map_id", Long.valueOf(this.mMapId));
        contentValues.put("layout_id", Long.valueOf(this.mLayoutId));
        contentValues.put(TableColumns.MODULE_ORIENTATION, Integer.valueOf(this.mModuleOrientation == ModuleOrientation.horizontal ? 0 : 1));
        contentValues.put(TableColumns.MODULE_TILT, Double.valueOf(this.mModuleTilt));
        contentValues.put("module_width", Double.valueOf(this.mModuleWidth));
        contentValues.put("module_height", Double.valueOf(this.mModuleHeight));
        contentValues.put(TableColumns.VERTICAL_SPACING, Double.valueOf(this.mVerticalSpacing));
        contentValues.put(TableColumns.HORIZONTAL_SPACING, Double.valueOf(this.mHorizontalSpacing));
        contentValues.put(TableColumns.ROWS, Integer.valueOf(this.mRows));
        contentValues.put(TableColumns.COLUMNS, Integer.valueOf(this.mColumns));
        contentValues.put("group_id", this.mGroupId);
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        this.mRectangle.setGroupId(this._id);
        this.mRectangle.save(context);
        for (Module module : this.mModules.values()) {
            module.setGroupId(this._id);
            module.update(context);
        }
        return this._id;
    }

    public long saveFromAPI(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        this.mRectangle.setGroupId(this._id);
        this.mRectangle.save(context);
        if (this.mlModules != null) {
            for (Module module : this.mlModules) {
                module.setGroupId(this._id);
                module.update(context);
                this.mModules.put(Long.valueOf(module.getId()), module);
            }
        }
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    this.mRectangle.setGroupId(this._id);
                    this.mRectangle.update(context);
                    for (Module module : this.mModules.values()) {
                        module.setGroupId(this._id);
                        module.update(context);
                    }
                    query.close();
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        save(context);
        query.close();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeLong(this.mMapId);
        parcel.writeLong(this.mLayoutId);
        parcel.writeParcelable(this.mRectangle, i);
        parcel.writeInt(this.mModuleOrientation == ModuleOrientation.horizontal ? 0 : 1);
        parcel.writeDouble(this.mModuleTilt);
        parcel.writeDouble(this.mModuleWidth);
        parcel.writeDouble(this.mModuleHeight);
        parcel.writeDouble(this.mVerticalSpacing);
        parcel.writeDouble(this.mHorizontalSpacing);
        parcel.writeInt(this.mRows);
        parcel.writeInt(this.mColumns);
        parcel.writeSerializable(this.mModules);
        parcel.writeTypedList(this.mlModules);
        parcel.writeLong(this.mGroupId == 0 ? 0 : this.mGroupId.longValue());
    }

    public boolean prepareForPublish() {
        int i;
        if (this.mModules == null || this.mModules.size() <= 0) {
            i = 1;
        } else {
            if (this.mlModules == null) {
                this.mlModules = new ArrayList(this.mModules.size());
            } else {
                this.mlModules.clear();
            }
            i = 1;
            for (Module module : this.mModules.values()) {
                if (module.isEnabled()) {
                    i = 0;
                    this.mlModules.add(module);
                }
            }
        }
        return i ^ 1;
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public Rectangle getRectangle() {
        return this.mRectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.mRectangle = rectangle;
    }

    public ModuleOrientation getModuleOrientation() {
        return this.mModuleOrientation;
    }

    public void setModuleOrientation(ModuleOrientation moduleOrientation) {
        this.mModuleOrientation = moduleOrientation;
    }

    public double getModuleTilt() {
        return this.mModuleTilt;
    }

    public void setModuleTilt(double d) {
        this.mModuleTilt = d;
    }

    public double getModuleWidth() {
        return this.mModuleOrientation == ModuleOrientation.vertical ? this.mModuleWidth : this.mModuleHeight;
    }

    public void setModuleWidth(double d) {
        this.mModuleWidth = d;
    }

    public double getModuleHeight() {
        return this.mModuleOrientation == ModuleOrientation.vertical ? this.mModuleHeight : this.mModuleWidth;
    }

    public void setModuleHeight(double d) {
        this.mModuleHeight = d;
    }

    public void setVerticalSpacing(double d) {
        this.mVerticalSpacing = d;
    }

    public void setHorizontalSpacing(double d) {
        this.mHorizontalSpacing = d;
    }

    public int getRows() {
        return this.mRows;
    }

    public void setRows(int i) {
        this.mRows = i;
    }

    public int getColumns() {
        return this.mColumns;
    }

    public void setColumns(int i) {
        this.mColumns = i;
    }

    public HashMap<Long, Module> getModules() {
        return this.mModules;
    }

    public void addModule(Module module) {
        if (this.mModules == null) {
            this.mModules = new HashMap();
        }
        this.mModules.put(Long.valueOf(module.getId()), module);
    }

    public void removeModule(Module module) {
        if (this.mModules != null && this.mModules.get(Long.valueOf(module.getId())) != null) {
            ((Module) this.mModules.get(Long.valueOf(module.getId()))).setEnabled(module.isEnabled());
        }
    }

    public void setModules(HashMap<Long, Module> hashMap) {
        this.mModules = new HashMap(hashMap);
    }

    public long getMapId() {
        return this.mMapId;
    }

    public long getGroupId() {
        return this.mGroupId.longValue();
    }

    public void setMapId(long j) {
        this.mMapId = j;
    }

    public long getLayoutId() {
        return this.mLayoutId;
    }

    public void setLayoutId(long j) {
        this.mLayoutId = j;
    }

    public List<Module> getMlModules() {
        return this.mlModules;
    }

    public void addMlModule(Module module) {
        if (this.mlModules == null) {
            this.mlModules = new ArrayList();
        }
        this.mlModules.add(module);
    }

    public Module[][] getModulesGrid() {
        if (this.mModulesGrid == null) {
            this.mModulesGrid = (Module[][]) Array.newInstance(Module.class, new int[]{getRows(), getColumns()});
            for (Module module : this.mModules.values()) {
                this.mModulesGrid[module.getRow()][module.getColumn()] = module;
            }
        }
        return this.mModulesGrid;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("");
        stringBuilder.append("GroupId: ");
        stringBuilder.append(this.mGroupId);
        stringBuilder.append(" : ");
        String stringBuilder2 = stringBuilder.toString();
        if (this.mlModules != null) {
            stringBuilder = new StringBuilder();
            stringBuilder.append(stringBuilder2);
            stringBuilder.append("Modules: ");
            stringBuilder.append(this.mlModules.size());
            stringBuilder.append(" : ");
            stringBuilder2 = stringBuilder.toString();
            for (Module module : this.mlModules) {
                StringBuilder stringBuilder3 = new StringBuilder();
                stringBuilder3.append(stringBuilder2);
                stringBuilder3.append(module.toString());
                stringBuilder3.append(" ");
                stringBuilder2 = stringBuilder3.toString();
            }
        }
        stringBuilder = new StringBuilder();
        stringBuilder.append(stringBuilder2);
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
