package com.solaredge.common.models.response;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "moduleModels", strict = false)
public class ModuleModelsResponse {
    @Element(name = "count", required = false)
    private int count;
    @Element(name = "manufacturer", required = false)
    private String manufacturer;
    @ElementList(name = "list", required = false)
    private List<CreateUpdateSiteModule> models;

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int i) {
        this.count = i;
    }

    public List<CreateUpdateSiteModule> getModels() {
        return this.models;
    }

    public void setModels(List<CreateUpdateSiteModule> list) {
        this.models = list;
    }
}
