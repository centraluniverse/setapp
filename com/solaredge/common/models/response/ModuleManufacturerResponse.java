package com.solaredge.common.models.response;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "moduleManufacturers", strict = false)
public class ModuleManufacturerResponse {
    @Element(name = "count", required = false)
    private int count;
    @ElementList(name = "list", required = false)
    private List<ModuleManfacturerItem> manufacturers;

    public int getCount() {
        return this.count;
    }

    public void setCount(int i) {
        this.count = i;
    }

    public List<ModuleManfacturerItem> getManufacturers() {
        return this.manufacturers;
    }

    public void setManufacturers(List<ModuleManfacturerItem> list) {
        this.manufacturers = list;
    }
}
