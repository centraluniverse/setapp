package com.solaredge.common.models.response;

import java.util.GregorianCalendar;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "DatePeriod", strict = false)
public class EnergySpanResponse {
    @Element(name = "endDate", required = false)
    private GregorianCalendar endDate;
    @Element(name = "startDate", required = false)
    private GregorianCalendar startDate;

    public GregorianCalendar getStartDate() {
        return this.startDate;
    }

    public void setStartDate(GregorianCalendar gregorianCalendar) {
        this.startDate = gregorianCalendar;
    }

    public GregorianCalendar getEndDate() {
        return this.endDate;
    }

    public void setEndDate(GregorianCalendar gregorianCalendar) {
        this.endDate = gregorianCalendar;
    }
}
