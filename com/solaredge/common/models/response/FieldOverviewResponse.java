package com.solaredge.common.models.response;

import com.solaredge.common.models.SEUri;
import com.solaredge.common.models.SolarField;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "FieldOverviewData", strict = false)
public class FieldOverviewResponse {
    @Element(name = "currentPower", required = false)
    @Path("currentPower")
    private float currentPower;
    @Element(name = "energy", required = false)
    @Path("lastDayData")
    private float lastDayEnergy;
    @Element(name = "energy", required = false)
    @Path("lastMonthData")
    private float lastMonthEnergy;
    @Element(name = "lastUpdateTime", required = false)
    private String lastUpdateTime;
    @Element(name = "energy", required = false)
    @Path("lastYearData")
    private float lastYearEnergy;
    @Element(name = "energy", required = false)
    @Path("lifeTimeData")
    private float lifeTimeEnergy;
    @Element(name = "revenue", required = false)
    @Path("lifeTimeData")
    private RevenueResponse lifetimeRevenue;
    @ElementList(name = "uris", required = false)
    private List<SEUri> seUris;
    @Element(name = "solarField", required = false)
    private SolarField solarField;

    public String getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public void setLastUpdateTime(String str) {
        this.lastUpdateTime = str;
    }

    public float getLifeTimeEnergy() {
        return this.lifeTimeEnergy;
    }

    public void setLifeTimeEnergy(float f) {
        this.lifeTimeEnergy = f;
    }

    public float getLastYearEnergy() {
        return this.lastYearEnergy;
    }

    public void setLastYearEnergy(float f) {
        this.lastYearEnergy = f;
    }

    public float getLastMonthEnergy() {
        return this.lastMonthEnergy;
    }

    public void setLastMonthEnergy(float f) {
        this.lastMonthEnergy = f;
    }

    public float getLastDayEnergy() {
        return this.lastDayEnergy;
    }

    public void setLastDayEnergy(float f) {
        this.lastDayEnergy = f;
    }

    public float getCurrentPower() {
        return this.currentPower;
    }

    public void setCurrentPower(float f) {
        this.currentPower = f;
    }

    public SolarField getSolarField() {
        return this.solarField;
    }

    public void setSolarField(SolarField solarField) {
        this.solarField = solarField;
    }

    public List<SEUri> getSeUris() {
        return this.seUris;
    }

    public void setSeUris(List<SEUri> list) {
        this.seUris = list;
    }

    public RevenueResponse getLifetimeRevenue() {
        return this.lifetimeRevenue;
    }

    public void setLifetimeRevenue(RevenueResponse revenueResponse) {
        this.lifetimeRevenue = revenueResponse;
    }
}
