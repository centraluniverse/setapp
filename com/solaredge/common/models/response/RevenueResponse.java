package com.solaredge.common.models.response;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "revenue", strict = false)
public class RevenueResponse {
    @Attribute(name = "localized", required = false)
    private String localized;
    @Text(required = false)
    private float value;

    public String getLocalized() {
        return this.localized;
    }

    public void setLocalized(String str) {
        this.localized = str;
    }

    public float getValue() {
        return this.value;
    }

    public void setValue(float f) {
        this.value = f;
    }
}
