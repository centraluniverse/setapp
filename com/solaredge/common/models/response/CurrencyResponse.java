package com.solaredge.common.models.response;

import com.solaredge.common.models.Currency;
import java.util.ArrayList;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "currenciesResult", strict = false)
public class CurrencyResponse {
    @Element(name = "count", required = false)
    private int count;
    @ElementList(name = "currencies")
    private List<Currency> currencies;
    @Element(name = "defaultCurrency", required = false)
    private Currency defaultCurrency;

    public int getCount() {
        return this.count;
    }

    public void setCount(int i) {
        this.count = i;
    }

    public Currency getDefaultCurrency() {
        return this.defaultCurrency;
    }

    public void setDefaultCurrency(Currency currency) {
        this.defaultCurrency = currency;
    }

    public List<Currency> getCurrencies() {
        return this.currencies == null ? new ArrayList() : this.currencies;
    }

    public void setCurrencies(List<Currency> list) {
        this.currencies = list;
    }
}
