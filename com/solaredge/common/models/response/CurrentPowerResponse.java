package com.solaredge.common.models.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "FieldCurrentPower", strict = false)
public class CurrentPowerResponse {
    @Element(name = "currentPower", required = false)
    private float currentPower;

    public float getCurrentPower() {
        return this.currentPower;
    }

    public void setCurrentPower(float f) {
        this.currentPower = f;
    }
}
