package com.solaredge.common.models.response;

import com.solaredge.common.models.Translation;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "LocalizationInfo", strict = false)
public class LocaleResponse {
    @ElementList(name = "translation", required = false)
    private List<Translation> translations;

    public List<Translation> getTranslations() {
        return this.translations;
    }

    public void setTranslations(List<Translation> list) {
        this.translations = list;
    }
}
