package com.solaredge.common.models.response;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.models.evCharger.EvCharger;
import java.util.List;

public class EVChargerResponse {
    @C0631a
    @C0633c(a = "evChargers")
    private List<EvCharger> evChargers = null;
    @C0631a
    @C0633c(a = "fieldLastUpdateTS")
    private Integer fieldLastUpdateTS;
    @C0631a
    @C0633c(a = "isUpdated")
    private Boolean isUpdated;
    @C0631a
    @C0633c(a = "operationProgressState")
    private String operationProgressState;
    @C0631a
    @C0633c(a = "updateRefreshRate")
    private Integer updateRefreshRate;

    public String getOperationProgressState() {
        return this.operationProgressState;
    }

    public void setOperationProgressState(String str) {
        this.operationProgressState = str;
    }

    public Integer getFieldLastUpdateTS() {
        return this.fieldLastUpdateTS;
    }

    public void setFieldLastUpdateTS(Integer num) {
        this.fieldLastUpdateTS = num;
    }

    public Boolean getIsUpdated() {
        return this.isUpdated;
    }

    public void setIsUpdated(Boolean bool) {
        this.isUpdated = bool;
    }

    public Integer getUpdateRefreshRate() {
        return this.updateRefreshRate;
    }

    public void setUpdateRefreshRate(Integer num) {
        this.updateRefreshRate = num;
    }

    public List<EvCharger> getEvChargers() {
        return this.evChargers;
    }

    public void setEvChargers(List<EvCharger> list) {
        this.evChargers = list;
    }
}
