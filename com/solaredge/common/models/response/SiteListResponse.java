package com.solaredge.common.models.response;

import com.solaredge.common.models.SolarField;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "FieldsResult", strict = false)
public class SiteListResponse {
    @ElementList(name = "fields", required = false)
    private List<SolarField> solarFields;
    @Element(name = "total", required = false)
    private int total;

    public int getTotal() {
        return this.total;
    }

    public void setTotal(int i) {
        this.total = i;
    }

    public List<SolarField> getSolarFields() {
        return this.solarFields;
    }

    public void setSolarFields(List<SolarField> list) {
        this.solarFields = list;
    }
}
