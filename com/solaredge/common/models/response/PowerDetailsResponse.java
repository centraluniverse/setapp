package com.solaredge.common.models.response;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class PowerDetailsResponse {
    @C0631a
    @C0633c(a = "powerDetails", b = {"energyDetails"})
    private PowerDetails powerDetails;

    public PowerDetails getPowerDetails() {
        return this.powerDetails;
    }

    public void setPowerDetails(PowerDetails powerDetails) {
        this.powerDetails = powerDetails;
    }
}
