package com.solaredge.common.models.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "siteName", strict = false)
public class SiteNameExistsResponse {
    @Element(name = "exists", required = false)
    private boolean exists;
    @Element(name = "name", required = false)
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public boolean isExists() {
        return this.exists;
    }

    public void setExists(boolean z) {
        this.exists = z;
    }
}
