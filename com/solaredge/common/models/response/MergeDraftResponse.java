package com.solaredge.common.models.response;

import com.solaredge.common.models.GroupConflicts;
import com.solaredge.common.models.InverterConflict;
import com.solaredge.common.models.ValidationResult;
import com.solaredge.common.models.map.PhysicalLayout;
import java.io.Serializable;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "mergeDraftResponse", strict = false)
public class MergeDraftResponse implements Serializable {
    public static final String RESULT_CONFLICTS_EXIST = "CONFLICTS_EXIST";
    public static final String RESULT_SUCESS = "SUCCESS";
    @ElementList(entry = "groupConflicts", inline = true, required = false)
    private List<GroupConflicts> groupConflicts;
    @ElementList(entry = "inverterConflicts", inline = true, required = false)
    private List<InverterConflict> inverterConflicts;
    @Element(name = "mergeStatus", required = true)
    private String mergeStatus;
    @Element(name = "mergedPhysicalLayout", required = false)
    private PhysicalLayout mergedPhysicalLayout;
    @Element(name = "serverPhysicalLayout", required = false)
    private PhysicalLayout serverPhysicalLayout;
    @Element(name = "serverVersion", required = true)
    private long serverVersion;
    @Element(name = "validationResult", required = false)
    private ValidationResult validationResult;

    public ValidationResult getValidationResult() {
        return this.validationResult;
    }

    public String getMergeStatus() {
        return this.mergeStatus;
    }

    public PhysicalLayout getMergedPhysicalLayout() {
        return this.mergedPhysicalLayout;
    }

    public PhysicalLayout getServerPhysicalLayout() {
        return this.serverPhysicalLayout;
    }

    public long getServerVersion() {
        return this.serverVersion;
    }

    public List<GroupConflicts> getGroupConflicts() {
        return this.groupConflicts;
    }

    public List<InverterConflict> getInverterConflicts() {
        return this.inverterConflicts;
    }
}
