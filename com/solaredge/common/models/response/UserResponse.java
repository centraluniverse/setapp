package com.solaredge.common.models.response;

import com.solaredge.common.models.Account;
import java.util.ArrayList;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "User", strict = false)
public class UserResponse {
    @Element(required = false)
    public Account account = null;
    @Element(required = false)
    public String accountRole;
    @Element(required = false)
    public String email;
    @Element(required = false)
    public String firstName;
    @Element(required = false)
    public String lastName;
    @Element(required = false)
    public String locale;
    @Element(name = "si", required = false)
    public String metrics;
    @ElementList(entry = "service", name = "services", required = true)
    public ArrayList<String> services;
}
