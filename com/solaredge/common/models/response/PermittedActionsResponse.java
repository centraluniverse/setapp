package com.solaredge.common.models.response;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.ArrayList;

public class PermittedActionsResponse {
    @C0631a
    @C0633c(a = "ArrayList")
    private ArrayList<String> permittedActions;

    public ArrayList<String> getPermittedActions() {
        return this.permittedActions;
    }
}
