package com.solaredge.common.models.response;

import com.solaredge.common.models.Translation;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "LocalizationInfo", strict = false)
public class TranslationResponse {
    private String localeCode;
    @Element(name = "longDateTimeFormat", required = false)
    private String longDateTimeFormat;
    @Element(name = "mediumDateTimeFormat", required = false)
    private String mediumDateTimeFormat;
    @Element(name = "shortDateTimeFormat", required = false)
    private String shortDateTimeFormat;
    @ElementList(name = "translation", required = false)
    private List<Translation> translations;

    public String getLocaleCode() {
        return this.localeCode;
    }

    public void setLocaleCode(String str) {
        this.localeCode = str;
    }

    public String getShortDateTimeFormat() {
        return this.shortDateTimeFormat;
    }

    public void setShortDateTimeFormat(String str) {
        this.shortDateTimeFormat = str;
    }

    public String getMediumDateTimeFormat() {
        return this.mediumDateTimeFormat;
    }

    public void setMediumDateTimeFormat(String str) {
        this.mediumDateTimeFormat = str;
    }

    public String getLongDateTimeFormat() {
        return this.longDateTimeFormat;
    }

    public void setLongDateTimeFormat(String str) {
        this.longDateTimeFormat = str;
    }

    public List<Translation> getTranslations() {
        return this.translations;
    }

    public void setTranslations(List<Translation> list) {
        this.translations = list;
    }
}
