package com.solaredge.common.models.response;

import com.solaredge.common.models.LocaleInfo;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "list", strict = false)
public class SupportedLocalesResponse {
    @ElementList(entry = "LocaleInfo", inline = true, required = false)
    private List<LocaleInfo> locales;

    public List<LocaleInfo> getLocales() {
        return this.locales;
    }

    public void setLocales(List<LocaleInfo> list) {
        this.locales = list;
    }
}
