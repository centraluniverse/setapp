package com.solaredge.common.models.response;

import com.solaredge.common.models.Account;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "accountsResult", strict = false)
public class AccountsListResponse {
    @ElementList(name = "accounts")
    private List<Account> accounts;
    @Element(name = "count", required = false)
    private int count;

    public int getCount() {
        return this.count;
    }

    public void setCount(int i) {
        this.count = i;
    }

    public List<Account> getAccounts() {
        return this.accounts;
    }

    public void setAccounts(List<Account> list) {
        this.accounts = list;
    }
}
