package com.solaredge.common.models.response;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "moduleModule", strict = false)
public class CreateUpdateSiteModule implements Parcelable {
    public static final Creator<CreateUpdateSiteModule> CREATOR = ParcelableCompat.newCreator(new C22101());
    private boolean mIsUserCreated = false;
    @Element(name = "manufacturerName", required = false)
    private String manufacturerName;
    @Element(name = "maximumPower", required = false)
    private float maximumPower;
    @Element(name = "modelName", required = false)
    private String modelName;
    @Element(name = "temperatureCoef", required = false)
    private float temperatureCoef;

    static class C22101 implements ParcelableCompatCreatorCallbacks<CreateUpdateSiteModule> {
        C22101() {
        }

        public CreateUpdateSiteModule createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new CreateUpdateSiteModule(parcel);
        }

        public CreateUpdateSiteModule[] newArray(int i) {
            return new CreateUpdateSiteModule[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public CreateUpdateSiteModule(Parcel parcel) {
        readFromParcel(parcel);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.manufacturerName);
        parcel.writeString(this.modelName);
        parcel.writeFloat(this.maximumPower);
        parcel.writeFloat(this.temperatureCoef);
        parcel.writeInt(this.mIsUserCreated);
    }

    private void readFromParcel(Parcel parcel) {
        this.manufacturerName = parcel.readString();
        this.modelName = parcel.readString();
        this.maximumPower = parcel.readFloat();
        this.temperatureCoef = parcel.readFloat();
        this.mIsUserCreated = parcel.readInt() != null ? true : null;
    }

    public String getManufacturerName() {
        return this.manufacturerName;
    }

    public void setManufacturerName(String str) {
        this.manufacturerName = str;
    }

    public String getModelName() {
        return this.modelName;
    }

    public void setModelName(String str) {
        this.modelName = str;
    }

    public float getMaximumPower() {
        return this.maximumPower;
    }

    public void setMaximumPower(float f) {
        this.maximumPower = f;
    }

    public float getTemperatureCoef() {
        return this.temperatureCoef;
    }

    public void setTemperatureCoef(float f) {
        this.temperatureCoef = f;
    }

    public boolean isUserCreated() {
        return this.mIsUserCreated;
    }

    public void setIsUserCreated(boolean z) {
        this.mIsUserCreated = z;
    }
}
