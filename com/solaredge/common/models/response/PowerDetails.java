package com.solaredge.common.models.response;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.models.Meter;
import java.util.ArrayList;
import java.util.List;

public class PowerDetails {
    @C0631a
    @C0633c(a = "meters")
    private List<Meter> meters = new ArrayList();
    @C0631a
    @C0633c(a = "timeUnit")
    private String timeUnit;
    @C0631a
    @C0633c(a = "unit")
    private String unit;

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String str) {
        this.unit = str;
    }

    public String getTimeUnit() {
        return this.timeUnit;
    }

    public void setTimeUnit(String str) {
        this.timeUnit = str;
    }

    public List<Meter> getMeters() {
        return this.meters;
    }

    public void setMeters(List<Meter> list) {
        this.meters = list;
    }
}
