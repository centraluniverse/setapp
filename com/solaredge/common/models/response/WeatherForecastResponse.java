package com.solaredge.common.models.response;

import com.solaredge.common.models.WeatherForecast;
import java.util.ArrayList;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "list", strict = false)
public class WeatherForecastResponse {
    @ElementList(entry = "DailyWeatherForecastDto", inline = true, required = false)
    private ArrayList<WeatherForecast> forecastList;

    public ArrayList<WeatherForecast> getForecastList() {
        return this.forecastList;
    }

    public void setForecastList(ArrayList<WeatherForecast> arrayList) {
        this.forecastList = arrayList;
    }
}
