package com.solaredge.common.models.response;

import com.solaredge.common.models.SNSiteType;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "serialNumberPrefixes", strict = false)
public class SNPrefixesResponse {
    @ElementList(entry = "siteType", inline = true, required = false)
    private List<SNSiteType> siteTypes;

    public List<SNSiteType> getSiteTypes() {
        return this.siteTypes;
    }

    public void setSiteTypes(List<SNSiteType> list) {
        this.siteTypes = list;
    }
}
