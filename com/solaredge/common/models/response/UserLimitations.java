package com.solaredge.common.models.response;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class UserLimitations {
    @C0631a
    @C0633c(a = "shouldDisplayAccountApprovalButton")
    public Boolean shouldDisplayAccountApprovalButton;
    @C0631a
    @C0633c(a = "shouldDisplayLimitedAccountMessage")
    public Boolean shouldDisplayLimitedAccountMessage;
    @C0631a
    @C0633c(a = "shouldDisplayPendingApprovalAccountMessage")
    public Boolean shouldDisplayPendingApprovalAccountMessage;
}
