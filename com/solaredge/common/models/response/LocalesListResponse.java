package com.solaredge.common.models.response;

import com.solaredge.common.models.LocaleInfo;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class LocalesListResponse {
    @ElementList(inline = true, name = "list", required = false)
    private List<LocaleInfo> locales;

    public List<LocaleInfo> getLocales() {
        return this.locales;
    }

    public void setLocales(List<LocaleInfo> list) {
        this.locales = list;
    }
}
