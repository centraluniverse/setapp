package com.solaredge.common.models.response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "manufacturer", strict = false)
public class ModuleManfacturerItem {
    @Element(name = "name", required = false)
    private String name;

    public String getName() {
        return this.name;
    }
}
