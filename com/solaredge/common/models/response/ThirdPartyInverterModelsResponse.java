package com.solaredge.common.models.response;

import com.solaredge.common.models.InverterModel;
import java.util.ArrayList;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "invertersModels", strict = false)
public class ThirdPartyInverterModelsResponse {
    @Element(name = "count", required = false)
    private int count;
    @ElementList(entry = "inverterModel", name = "list", required = false)
    private ArrayList<InverterModel> inverterModels;

    public int getCount() {
        return this.count;
    }

    public void setCount(int i) {
        this.count = i;
    }

    public ArrayList<InverterModel> getInverterModels() {
        return this.inverterModels;
    }

    public void setInverterModels(ArrayList<InverterModel> arrayList) {
        this.inverterModels = arrayList;
    }
}
