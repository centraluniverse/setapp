package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "DailyWeatherForecastDto", strict = false)
public class WeatherForecast implements Parcelable {
    public static final Creator<WeatherForecast> CREATOR = new C14181();
    @Element(name = "condition", required = false)
    private String condition;
    @Element(name = "imageName", required = false)
    private String conditionIcon;
    @Element(name = "date", required = false)
    private GregorianCalendar date;
    @Element(name = "feelsLike", required = false)
    private String feelsLike;
    @Element(name = "humidity", required = false)
    private String humidity;
    @Element(name = "temperatureHigh", required = false)
    private float maxTemperature;
    @Element(name = "temperatureLow", required = false)
    private float minTemperature;
    @Element(name = "sunrise", required = false)
    private String sunrise;
    @Element(name = "sunset", required = false)
    private String sunset;
    @Element(name = "windSpeed", required = false)
    private String windSpeed;

    static class C14181 implements Creator<WeatherForecast> {
        C14181() {
        }

        public WeatherForecast createFromParcel(Parcel parcel) {
            return new WeatherForecast(parcel);
        }

        public WeatherForecast[] newArray(int i) {
            return new WeatherForecast[i];
        }
    }

    public WeatherForecast(WeatherData weatherData) {
        setCondition(weatherData.getCurrentCondition());
        setConditionIcon(weatherData.getCurrentConditionIcon());
        setSunrise(weatherData.getSunrise());
        setSunset(weatherData.getSunset());
        setMaxTemperature(weatherData.getCurrentTemperature());
        setFeelsLike(weatherData.getFeelsLike());
        setWindSpeed(weatherData.getWindSpeed());
        setHumidity(weatherData.getHumidity());
        setMinTemperature(-500.0f);
        setDate(System.currentTimeMillis());
    }

    public WeatherForecast(Parcel parcel) {
        this.date = (GregorianCalendar) parcel.readSerializable();
        this.minTemperature = parcel.readFloat();
        this.maxTemperature = parcel.readFloat();
        this.condition = parcel.readString();
        this.conditionIcon = parcel.readString();
        this.sunrise = parcel.readString();
        this.sunset = parcel.readString();
        this.feelsLike = parcel.readString();
        this.windSpeed = parcel.readString();
        this.humidity = parcel.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.date);
        parcel.writeFloat(this.minTemperature);
        parcel.writeFloat(this.maxTemperature);
        parcel.writeString(this.condition);
        parcel.writeString(this.conditionIcon);
        parcel.writeString(this.sunrise);
        parcel.writeString(this.sunset);
        parcel.writeString(this.feelsLike);
        parcel.writeString(this.windSpeed);
        parcel.writeString(this.humidity);
    }

    public int describeContents() {
        return hashCode();
    }

    public Calendar getDate() {
        return this.date;
    }

    public void setDate(long j) {
        if (this.date == null) {
            this.date = new GregorianCalendar();
        }
        this.date.setTimeInMillis(j);
    }

    public float getMinTemperature() {
        return this.minTemperature;
    }

    public void setMinTemperature(float f) {
        this.minTemperature = f;
    }

    public float getMaxTemperature() {
        return this.maxTemperature;
    }

    public void setMaxTemperature(float f) {
        this.maxTemperature = f;
    }

    public String getCondition() {
        return this.condition;
    }

    public void setCondition(String str) {
        this.condition = str;
    }

    public String getConditionIcon() {
        return this.conditionIcon;
    }

    public void setConditionIcon(String str) {
        this.conditionIcon = str;
    }

    public String getSunrise() {
        return this.sunrise;
    }

    public void setSunrise(String str) {
        this.sunrise = str;
    }

    public String getSunset() {
        return this.sunset;
    }

    public void setSunset(String str) {
        this.sunset = str;
    }

    public String getFeelsLike() {
        return this.feelsLike;
    }

    public void setFeelsLike(String str) {
        this.feelsLike = str;
    }

    public String getWindSpeed() {
        return this.windSpeed;
    }

    public void setWindSpeed(String str) {
        this.windSpeed = str;
    }

    public String getHumidity() {
        return this.humidity;
    }

    public void setHumidity(String str) {
        this.humidity = str;
    }
}
