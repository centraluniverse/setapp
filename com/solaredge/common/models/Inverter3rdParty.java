package com.solaredge.common.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.models.map.Inverter;
import org.simpleframework.xml.Element;

public class Inverter3rdParty extends Inverter {
    public static final Creator<Inverter3rdParty> CREATOR = ParcelableCompat.newCreator(new C21961());
    @Element(name = "bindingId", required = false)
    private long mSMIId;

    static class C21961 implements ParcelableCompatCreatorCallbacks<Inverter3rdParty> {
        C21961() {
        }

        public Inverter3rdParty createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new Inverter3rdParty(parcel);
        }

        public Inverter3rdParty[] newArray(int i) {
            return new Inverter3rdParty[i];
        }
    }

    public Inverter3rdParty() {
        this.mSMIId = -1;
        this.mIs3rdParty = true;
    }

    public Inverter3rdParty(Cursor cursor) {
        super(cursor);
        this.mSMIId = cursor.getLong(cursor.getColumnIndex("smi_id"));
    }

    public Inverter3rdParty(Parcel parcel) {
        readFromParcel(parcel);
    }

    public Inverter3rdParty(Inverter3rdParty inverter3rdParty) {
        super((Inverter) inverter3rdParty);
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = super.getContentValues();
        contentValues.put("smi_id", Long.valueOf(this.mSMIId));
        return contentValues;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeLong(this.mSMIId);
    }

    protected void readFromParcel(Parcel parcel) {
        super.readFromParcel(parcel);
        this.mSMIId = parcel.readLong();
    }

    public long getSMIId() {
        return this.mSMIId;
    }

    public void setSMIId(long j) {
        this.mSMIId = j;
    }
}
