package com.solaredge.common.models;

import android.text.TextUtils;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.models.evCharger.EvCharger;
import com.solaredge.common.p116g.C1404g;
import java.io.Serializable;

public class DeviceStatus implements Serializable {
    @C0631a
    @C0633c(a = "activePowerMeter")
    private Long activePowerMeter;
    @C0631a
    @C0633c(a = "activeTrigger")
    private String activeTrigger;
    @C0631a
    @C0633c(a = "endTime")
    private Long endTime;
    @C0631a
    @C0633c(a = "level")
    private Integer level;
    @C0631a
    @C0633c(a = "scheduleType")
    private String scheduleType;
    @C0631a
    @C0633c(a = "startTime")
    private Long startTime;

    public enum ScheduleType {
        SMART("SMART"),
        SIMPLE("SIMPLE"),
        EXCESS_PV(EvCharger.ExcessPV);
        
        private final String type;

        private ScheduleType(String str) {
            this.type = str;
        }

        public boolean equalType(String str) {
            return (this.type == null || this.type.equalsIgnoreCase(str) == null) ? null : true;
        }

        public String toString() {
            return this.type;
        }
    }

    public enum triggerType {
        MANUAL("MANUAL"),
        SCHEDULE("SCHEDULE"),
        OVERRIDE("OVERRIDE");
        
        private final String type;

        private triggerType(String str) {
            this.type = str;
        }

        public boolean equalType(String str) {
            return (this.type == null || this.type.equalsIgnoreCase(str) == null) ? null : true;
        }

        public String toString() {
            return this.type;
        }
    }

    public Integer getLevel() {
        return Integer.valueOf(this.level != null ? this.level.intValue() : 0);
    }

    public void setLevel(Integer num) {
        this.level = num;
    }

    public Long getActivePowerMeter() {
        return this.activePowerMeter;
    }

    public void setActivePowerMeter(Long l) {
        this.activePowerMeter = l;
    }

    public String getActiveTrigger() {
        return this.activeTrigger;
    }

    public void setActiveTrigger(String str) {
        this.activeTrigger = str;
    }

    public Long getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Long l) {
        this.startTime = l;
    }

    public Long getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Long l) {
        this.endTime = l;
    }

    public String getScheduleType() {
        return this.scheduleType;
    }

    public void setScheduleType(String str) {
        this.scheduleType = str;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null) {
            return false;
        }
        DeviceStatus deviceStatus = (DeviceStatus) obj;
        if (C1404g.m3842a(getLevel(), deviceStatus.getLevel()) && C1404g.m3842a(getStartTime(), deviceStatus.getStartTime()) && C1404g.m3842a(getEndTime(), deviceStatus.getEndTime()) && TextUtils.equals(getScheduleType(), deviceStatus.getScheduleType()) && TextUtils.equals(getActiveTrigger(), deviceStatus.getActiveTrigger()) != null) {
            z = true;
        }
        return z;
    }
}
