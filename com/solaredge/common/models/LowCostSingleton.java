package com.solaredge.common.models;

public class LowCostSingleton {
    private static LowCostSingleton sLowCostSingleton;
    private SiteLowCostResponse mLowCostResponse;

    public static synchronized LowCostSingleton getInstance() {
        LowCostSingleton lowCostSingleton;
        synchronized (LowCostSingleton.class) {
            if (sLowCostSingleton == null) {
                sLowCostSingleton = new LowCostSingleton();
            }
            lowCostSingleton = sLowCostSingleton;
        }
        return lowCostSingleton;
    }

    public synchronized void init(SiteLowCostResponse siteLowCostResponse) {
        this.mLowCostResponse = siteLowCostResponse;
    }

    public synchronized SiteLowCostResponse getLowCostResponse() {
        return this.mLowCostResponse;
    }
}
