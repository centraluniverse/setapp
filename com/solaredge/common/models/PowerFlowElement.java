package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class PowerFlowElement {
    @C0631a
    @C0633c(a = "chargeLevel")
    private float chargeLevel;
    @C0631a
    @C0633c(a = "critical")
    private boolean critical;
    @C0631a
    @C0633c(a = "currentPower")
    private float currentPower = -1.0f;
    @C0631a
    @C0633c(a = "status")
    private String status;
    @C0631a
    @C0633c(a = "timeLeft")
    private int timeLeft = -1;

    public enum PowerWidgetStatus {
        STATUS_IDLE("Idle"),
        STATUS_ACTIVE("Active"),
        STATUS_DISABLED("Disabled"),
        STATUS_INACTIVE("Inactive"),
        STATUS_STORAGE_CHARGING("Charging"),
        STATUS_STORAGE_DISCHARGING("Discharging");
        
        private final String status;

        private PowerWidgetStatus(String str) {
            this.status = str;
        }

        public boolean equalsStatus(String str) {
            return (this.status == null || this.status.equalsIgnoreCase(str) == null) ? null : true;
        }

        public String toString() {
            return this.status;
        }
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public float getCurrentPower() {
        return this.currentPower;
    }

    public void setCurrentPower(float f) {
        this.currentPower = f;
    }

    public float getChargeLevel() {
        return this.chargeLevel;
    }

    public void setChargeLevel(float f) {
        this.chargeLevel = f;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public void setCritical(boolean z) {
        this.critical = z;
    }

    public int getTimeLeft() {
        return this.timeLeft;
    }

    public void setTimeLeft(int i) {
        this.timeLeft = i;
    }
}
