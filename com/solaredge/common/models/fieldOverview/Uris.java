package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class Uris implements Parcelable {
    public static final Creator<Uris> CREATOR = new C14321();
    @C0631a
    @C0633c(a = "currentPowerFlow")
    private String currentPowerFlow;
    @C0631a
    @C0633c(a = "evChargers")
    private String evChargers;
    @C0631a
    @C0633c(a = "layout")
    private String layout;
    @C0631a
    @C0633c(a = "liveWeather")
    private String liveWeather;
    @C0631a
    @C0633c(a = "loadDevices")
    private String loadDevices;
    @C0631a
    @C0633c(a = "weatherForecast")
    private String weatherForecast;

    static class C14321 implements Creator<Uris> {
        C14321() {
        }

        public Uris createFromParcel(Parcel parcel) {
            return new Uris(parcel);
        }

        public Uris[] newArray(int i) {
            return new Uris[i];
        }
    }

    public String getLayout() {
        return this.layout;
    }

    public void setLayout(String str) {
        this.layout = str;
    }

    public String getLiveWeather() {
        return this.liveWeather;
    }

    public void setLiveWeather(String str) {
        this.liveWeather = str;
    }

    public String getCurrentPowerFlow() {
        return this.currentPowerFlow;
    }

    public void setCurrentPowerFlow(String str) {
        this.currentPowerFlow = str;
    }

    public String getWeatherForecast() {
        return this.weatherForecast;
    }

    public void setWeatherForecast(String str) {
        this.weatherForecast = str;
    }

    public String getEvChargers() {
        return this.evChargers;
    }

    public void setEvChargers(String str) {
        this.evChargers = str;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.layout);
        parcel.writeString(this.liveWeather);
        parcel.writeString(this.currentPowerFlow);
        parcel.writeString(this.weatherForecast);
        parcel.writeString(this.evChargers);
        parcel.writeString(this.loadDevices);
    }

    protected Uris(Parcel parcel) {
        this.layout = parcel.readString();
        this.liveWeather = parcel.readString();
        this.currentPowerFlow = parcel.readString();
        this.weatherForecast = parcel.readString();
        this.evChargers = parcel.readString();
        this.loadDevices = parcel.readString();
    }

    public String getLoadDevices() {
        return this.loadDevices;
    }

    public void setLoadDevices(String str) {
        this.loadDevices = str;
    }
}
