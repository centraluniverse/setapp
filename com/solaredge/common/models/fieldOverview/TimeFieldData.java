package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class TimeFieldData implements Parcelable {
    public static final Creator<TimeFieldData> CREATOR = new C14311();
    @C0631a
    @C0633c(a = "energy")
    private Float energy;
    @C0631a
    @C0633c(a = "fieldId")
    private String fieldId;
    @C0631a
    @C0633c(a = "kwh2kWp")
    private Double kwh2kWp;
    @C0631a
    @C0633c(a = "power")
    private Double power;
    @C0631a
    @C0633c(a = "revenue")
    private String revenue;

    static class C14311 implements Creator<TimeFieldData> {
        C14311() {
        }

        public TimeFieldData createFromParcel(Parcel parcel) {
            return new TimeFieldData(parcel);
        }

        public TimeFieldData[] newArray(int i) {
            return new TimeFieldData[i];
        }
    }

    public String getFieldId() {
        return this.fieldId;
    }

    public void setFieldId(String str) {
        this.fieldId = str;
    }

    public Float getEnergy() {
        return this.energy;
    }

    public void setEnergy(Float f) {
        this.energy = f;
    }

    public String getRevenue() {
        return this.revenue;
    }

    public void setRevenue(String str) {
        this.revenue = str;
    }

    public Double getPower() {
        return this.power;
    }

    public void setPower(Double d) {
        this.power = d;
    }

    public Double getKwh2kWp() {
        return this.kwh2kWp;
    }

    public void setKwh2kWp(Double d) {
        this.kwh2kWp = d;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.fieldId);
        parcel.writeValue(this.energy);
        parcel.writeString(this.revenue);
        parcel.writeValue(this.power);
        parcel.writeValue(this.kwh2kWp);
    }

    protected TimeFieldData(Parcel parcel) {
        this.fieldId = parcel.readString();
        this.energy = (Float) parcel.readValue(Float.class.getClassLoader());
        this.revenue = parcel.readString();
        this.power = (Double) parcel.readValue(Double.class.getClassLoader());
        this.kwh2kWp = (Double) parcel.readValue(Double.class.getClassLoader());
    }
}
