package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class LocationJson implements Parcelable {
    public static final Creator<LocationJson> CREATOR = new C14291();
    @C0631a
    @C0633c(a = "elevation")
    private Integer elevation;
    @C0631a
    @C0633c(a = "id")
    private Integer id;
    @C0631a
    @C0633c(a = "latitude")
    private Double latitude;
    @C0631a
    @C0633c(a = "longitude")
    private Double longitude;
    @C0631a
    @C0633c(a = "timeZone")
    private String timeZone;
    @C0631a
    @C0633c(a = "weatherCode")
    private String weatherCode;
    @C0631a
    @C0633c(a = "whereabouts")
    private Whereabouts whereabouts;

    static class C14291 implements Creator<LocationJson> {
        C14291() {
        }

        public LocationJson createFromParcel(Parcel parcel) {
            return new LocationJson(parcel);
        }

        public LocationJson[] newArray(int i) {
            return new LocationJson[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public Whereabouts getWhereabouts() {
        return this.whereabouts;
    }

    public void setWhereabouts(Whereabouts whereabouts) {
        this.whereabouts = whereabouts;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double d) {
        this.latitude = d;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Double d) {
        this.longitude = d;
    }

    public Integer getElevation() {
        return this.elevation;
    }

    public void setElevation(Integer num) {
        this.elevation = num;
    }

    public String getTimeZone() {
        return this.timeZone;
    }

    public void setTimeZone(String str) {
        this.timeZone = str;
    }

    public String getWeatherCode() {
        return this.weatherCode;
    }

    public void setWeatherCode(String str) {
        this.weatherCode = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.id);
        parcel.writeParcelable(this.whereabouts, i);
        parcel.writeValue(this.latitude);
        parcel.writeValue(this.longitude);
        parcel.writeValue(this.elevation);
        parcel.writeString(this.timeZone);
        parcel.writeString(this.weatherCode);
    }

    protected LocationJson(Parcel parcel) {
        this.id = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.whereabouts = (Whereabouts) parcel.readParcelable(Whereabouts.class.getClassLoader());
        this.latitude = (Double) parcel.readValue(Double.class.getClassLoader());
        this.longitude = (Double) parcel.readValue(Double.class.getClassLoader());
        this.elevation = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.timeZone = parcel.readString();
        this.weatherCode = parcel.readString();
    }
}
