package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class FieldOverviewData implements Parcelable {
    public static final Creator<FieldOverviewData> CREATOR = new C14261();
    @C0631a
    @C0633c(a = "fieldOverview")
    private FieldsData fieldOverviewData;
    @C0631a
    @C0633c(a = "uris")
    private Uris uris;

    static class C14261 implements Creator<FieldOverviewData> {
        C14261() {
        }

        public FieldOverviewData createFromParcel(Parcel parcel) {
            return new FieldOverviewData(parcel);
        }

        public FieldOverviewData[] newArray(int i) {
            return new FieldOverviewData[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public FieldsData getFieldOverviewData() {
        return this.fieldOverviewData;
    }

    public void setFieldOverviewData(FieldsData fieldsData) {
        this.fieldOverviewData = fieldsData;
    }

    public Uris getUris() {
        return this.uris;
    }

    public void setUris(Uris uris) {
        this.uris = uris;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.fieldOverviewData, i);
        parcel.writeParcelable(this.uris, i);
    }

    protected FieldOverviewData(Parcel parcel) {
        this.fieldOverviewData = (FieldsData) parcel.readParcelable(FieldsData.class.getClassLoader());
        this.uris = (Uris) parcel.readParcelable(Uris.class.getClassLoader());
    }
}
