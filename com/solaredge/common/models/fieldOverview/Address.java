package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class Address implements Parcelable {
    public static final Creator<Address> CREATOR = new C14241();
    @C0631a
    @C0633c(a = "city")
    private String city;
    @C0631a
    @C0633c(a = "country")
    private String country;
    @C0631a
    @C0633c(a = "fullAddress")
    private String fullAddress;
    @C0631a
    @C0633c(a = "secondaryAddress")
    private String secondaryAddress;
    @C0631a
    @C0633c(a = "state")
    private String state;
    @C0631a
    @C0633c(a = "zip")
    private String zip;

    static class C14241 implements Creator<Address> {
        C14241() {
        }

        public Address createFromParcel(Parcel parcel) {
            return new Address(parcel);
        }

        public Address[] newArray(int i) {
            return new Address[i];
        }
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public String getZip() {
        return this.zip;
    }

    public void setZip(String str) {
        this.zip = str;
    }

    public String getFullAddress() {
        return this.fullAddress;
    }

    public void setFullAddress(String str) {
        this.fullAddress = str;
    }

    public String getSecondaryAddress() {
        return this.secondaryAddress;
    }

    public void setSecondaryAddress(String str) {
        this.secondaryAddress = str;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String str) {
        this.state = str;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String str) {
        this.country = str;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.city);
        parcel.writeString(this.zip);
        parcel.writeString(this.fullAddress);
        parcel.writeString(this.secondaryAddress);
        parcel.writeString(this.state);
        parcel.writeString(this.country);
    }

    protected Address(Parcel parcel) {
        this.city = parcel.readString();
        this.zip = parcel.readString();
        this.fullAddress = parcel.readString();
        this.secondaryAddress = parcel.readString();
        this.state = parcel.readString();
        this.country = parcel.readString();
    }
}
