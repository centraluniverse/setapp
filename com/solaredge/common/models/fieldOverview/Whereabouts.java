package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class Whereabouts implements Parcelable {
    public static final Creator<Whereabouts> CREATOR = new C14331();
    @C0631a
    @C0633c(a = "city")
    private String city;
    @C0631a
    @C0633c(a = "country")
    private String country;
    @C0631a
    @C0633c(a = "countryCode")
    private String countryCode;
    @C0631a
    @C0633c(a = "state")
    private String state;
    @C0631a
    @C0633c(a = "stateCode")
    private String stateCode;

    static class C14331 implements Creator<Whereabouts> {
        C14331() {
        }

        public Whereabouts createFromParcel(Parcel parcel) {
            return new Whereabouts(parcel);
        }

        public Whereabouts[] newArray(int i) {
            return new Whereabouts[i];
        }
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String str) {
        this.country = str;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String str) {
        this.countryCode = str;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String str) {
        this.state = str;
    }

    public Object getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String str) {
        this.stateCode = str;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.country);
        parcel.writeString(this.countryCode);
        parcel.writeString(this.state);
        parcel.writeString(this.stateCode);
        parcel.writeString(this.city);
    }

    protected Whereabouts(Parcel parcel) {
        this.country = parcel.readString();
        this.countryCode = parcel.readString();
        this.state = parcel.readString();
        this.stateCode = parcel.readString();
        this.city = parcel.readString();
    }
}
