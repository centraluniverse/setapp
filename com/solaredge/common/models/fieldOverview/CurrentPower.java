package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class CurrentPower implements Parcelable {
    public static final Creator<CurrentPower> CREATOR = new C14251();
    @C0631a
    @C0633c(a = "currentPower")
    private Float currentPower;
    @C0631a
    @C0633c(a = "unit")
    private String unit;

    static class C14251 implements Creator<CurrentPower> {
        C14251() {
        }

        public CurrentPower createFromParcel(Parcel parcel) {
            return new CurrentPower(parcel);
        }

        public CurrentPower[] newArray(int i) {
            return new CurrentPower[i];
        }
    }

    public Float getCurrentPower() {
        return this.currentPower;
    }

    public void setCurrentPower(Float f) {
        this.currentPower = f;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String str) {
        this.unit = str;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.currentPower);
        parcel.writeString(this.unit);
    }

    protected CurrentPower(Parcel parcel) {
        this.currentPower = (Float) parcel.readValue(Float.class.getClassLoader());
        this.unit = parcel.readString();
    }
}
