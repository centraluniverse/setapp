package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.List;

public class AccountSecurityConstrains implements Parcelable {
    public static final Creator<AccountSecurityConstrains> CREATOR = new C14231();
    @C0631a
    @C0633c(a = "allowedIPs")
    private String allowedIPs;
    @C0631a
    @C0633c(a = "allowedServerIPs")
    private List<String> allowedServerIPs = null;
    @C0631a
    @C0633c(a = "b2bEnabled")
    private Boolean b2bEnabled;

    static class C14231 implements Creator<AccountSecurityConstrains> {
        C14231() {
        }

        public AccountSecurityConstrains createFromParcel(Parcel parcel) {
            return new AccountSecurityConstrains(parcel);
        }

        public AccountSecurityConstrains[] newArray(int i) {
            return new AccountSecurityConstrains[i];
        }
    }

    public Boolean getB2bEnabled() {
        return this.b2bEnabled;
    }

    public void setB2bEnabled(Boolean bool) {
        this.b2bEnabled = bool;
    }

    public String getAllowedIPs() {
        return this.allowedIPs;
    }

    public void setAllowedIPs(String str) {
        this.allowedIPs = str;
    }

    public List<String> getAllowedServerIPs() {
        return this.allowedServerIPs;
    }

    public void setAllowedServerIPs(List<String> list) {
        this.allowedServerIPs = list;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.b2bEnabled);
        parcel.writeString(this.allowedIPs);
        parcel.writeStringList(this.allowedServerIPs);
    }

    protected AccountSecurityConstrains(Parcel parcel) {
        this.b2bEnabled = (Boolean) parcel.readValue(Boolean.class.getClassLoader());
        this.allowedIPs = parcel.readString();
        this.allowedServerIPs = parcel.createStringArrayList();
    }
}
