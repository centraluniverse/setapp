package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class SolarFieldNew implements Parcelable {
    public static final Creator<SolarFieldNew> CREATOR = new C14301();
    @C0631a
    @C0633c(a = "address")
    private Address address;
    @C0631a
    @C0633c(a = "fieldType")
    private String fieldType;
    @C0631a
    @C0633c(a = "id")
    private Integer id;
    @C0631a
    @C0633c(a = "imageItemName")
    private String imageItemName;
    @C0631a
    @C0633c(a = "location")
    private LocationJson location;
    @C0631a
    @C0633c(a = "name")
    private String name;
    @C0631a
    @C0633c(a = "peakPower")
    private Float peakPower;

    static class C14301 implements Creator<SolarFieldNew> {
        C14301() {
        }

        public SolarFieldNew createFromParcel(Parcel parcel) {
            return new SolarFieldNew(parcel);
        }

        public SolarFieldNew[] newArray(int i) {
            return new SolarFieldNew[i];
        }
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public Float getPeakPower() {
        return this.peakPower;
    }

    public void setPeakPower(Float f) {
        this.peakPower = f;
    }

    public String getImageItemName() {
        return this.imageItemName;
    }

    public void setImageItemName(String str) {
        this.imageItemName = str;
    }

    public String getFieldType() {
        return this.fieldType;
    }

    public void setFieldType(String str) {
        this.fieldType = str;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public LocationJson getLocation() {
        return this.location;
    }

    public void setLocation(LocationJson locationJson) {
        this.location = locationJson;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.id);
        parcel.writeString(this.name);
        parcel.writeValue(this.peakPower);
        parcel.writeString(this.imageItemName);
        parcel.writeString(this.fieldType);
        parcel.writeParcelable(this.address, i);
        parcel.writeParcelable(this.location, i);
    }

    protected SolarFieldNew(Parcel parcel) {
        this.id = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.name = parcel.readString();
        this.peakPower = (Float) parcel.readValue(Float.class.getClassLoader());
        this.imageItemName = parcel.readString();
        this.fieldType = parcel.readString();
        this.address = (Address) parcel.readParcelable(Address.class.getClassLoader());
        this.location = (LocationJson) parcel.readParcelable(LocationJson.class.getClassLoader());
    }
}
