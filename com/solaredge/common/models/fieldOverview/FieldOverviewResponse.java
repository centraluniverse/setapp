package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class FieldOverviewResponse implements Parcelable {
    public static final Creator<FieldOverviewResponse> CREATOR = new C14271();
    @C0631a
    @C0633c(a = "fieldOverview")
    private FieldOverviewData fieldOverviewData;
    @C0631a
    @C0633c(a = "siteClassType")
    private String siteClassType;

    static class C14271 implements Creator<FieldOverviewResponse> {
        C14271() {
        }

        public FieldOverviewResponse createFromParcel(Parcel parcel) {
            return new FieldOverviewResponse(parcel);
        }

        public FieldOverviewResponse[] newArray(int i) {
            return new FieldOverviewResponse[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getSiteClassType() {
        return this.siteClassType;
    }

    public void setSiteClassType(String str) {
        this.siteClassType = str;
    }

    public FieldOverviewData getFieldOverviewData() {
        return this.fieldOverviewData;
    }

    public void setFieldOverviewData(FieldOverviewData fieldOverviewData) {
        this.fieldOverviewData = fieldOverviewData;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.siteClassType);
        parcel.writeParcelable(this.fieldOverviewData, i);
    }

    protected FieldOverviewResponse(Parcel parcel) {
        this.siteClassType = parcel.readString();
        this.fieldOverviewData = (FieldOverviewData) parcel.readParcelable(FieldOverviewData.class.getClassLoader());
    }
}
