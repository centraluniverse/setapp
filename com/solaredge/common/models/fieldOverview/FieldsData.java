package com.solaredge.common.models.fieldOverview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class FieldsData implements Parcelable {
    public static final Creator<FieldsData> CREATOR = new C14281();
    @C0631a
    @C0633c(a = "currentPower")
    private CurrentPower currentPower;
    @C0631a
    @C0633c(a = "isLowCost")
    private Boolean isLowCost;
    @C0631a
    @C0633c(a = "lastDayData")
    private TimeFieldData lastDayData;
    @C0631a
    @C0633c(a = "lastMonthData")
    private TimeFieldData lastMonthData;
    @C0631a
    @C0633c(a = "lastUpdateTime")
    private String lastUpdateTime;
    @C0631a
    @C0633c(a = "lastYearData")
    private TimeFieldData lastYearData;
    @C0631a
    @C0633c(a = "lifeTimeData")
    private TimeFieldData lifeTimeData;
    @C0631a
    @C0633c(a = "siteClassType")
    private String siteClassType;
    @C0631a
    @C0633c(a = "solarField")
    private SolarFieldNew solarField;

    static class C14281 implements Creator<FieldsData> {
        C14281() {
        }

        public FieldsData createFromParcel(Parcel parcel) {
            return new FieldsData(parcel);
        }

        public FieldsData[] newArray(int i) {
            return new FieldsData[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public void setLastUpdateTime(String str) {
        this.lastUpdateTime = str;
    }

    public TimeFieldData getLifeTimeData() {
        return this.lifeTimeData;
    }

    public void setLifeTimeData(TimeFieldData timeFieldData) {
        this.lifeTimeData = timeFieldData;
    }

    public TimeFieldData getLastYearData() {
        return this.lastYearData;
    }

    public void setLastYearData(TimeFieldData timeFieldData) {
        this.lastYearData = timeFieldData;
    }

    public TimeFieldData getLastMonthData() {
        return this.lastMonthData;
    }

    public void setLastMonthData(TimeFieldData timeFieldData) {
        this.lastMonthData = timeFieldData;
    }

    public TimeFieldData getLastDayData() {
        return this.lastDayData;
    }

    public void setLastDayData(TimeFieldData timeFieldData) {
        this.lastDayData = timeFieldData;
    }

    public SolarFieldNew getSolarField() {
        return this.solarField;
    }

    public void setSolarField(SolarFieldNew solarFieldNew) {
        this.solarField = solarFieldNew;
    }

    public CurrentPower getCurrentPower() {
        return this.currentPower;
    }

    public void setCurrentPower(CurrentPower currentPower) {
        this.currentPower = currentPower;
    }

    public Boolean getIsLowCost() {
        return this.isLowCost;
    }

    public void setIsLowCost(Boolean bool) {
        this.isLowCost = bool;
    }

    public Boolean getLowCost() {
        return this.isLowCost;
    }

    public void setLowCost(Boolean bool) {
        this.isLowCost = bool;
    }

    public String getSiteClassType() {
        return this.siteClassType;
    }

    public void setSiteClassType(String str) {
        this.siteClassType = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.lastUpdateTime);
        parcel.writeParcelable(this.lifeTimeData, i);
        parcel.writeParcelable(this.lastYearData, i);
        parcel.writeParcelable(this.lastMonthData, i);
        parcel.writeParcelable(this.lastDayData, i);
        parcel.writeParcelable(this.solarField, i);
        parcel.writeParcelable(this.currentPower, i);
        parcel.writeValue(this.isLowCost);
        parcel.writeString(this.siteClassType);
    }

    protected FieldsData(Parcel parcel) {
        this.lastUpdateTime = parcel.readString();
        this.lifeTimeData = (TimeFieldData) parcel.readParcelable(TimeFieldData.class.getClassLoader());
        this.lastYearData = (TimeFieldData) parcel.readParcelable(TimeFieldData.class.getClassLoader());
        this.lastMonthData = (TimeFieldData) parcel.readParcelable(TimeFieldData.class.getClassLoader());
        this.lastDayData = (TimeFieldData) parcel.readParcelable(TimeFieldData.class.getClassLoader());
        this.solarField = (SolarFieldNew) parcel.readParcelable(SolarFieldNew.class.getClassLoader());
        this.currentPower = (CurrentPower) parcel.readParcelable(CurrentPower.class.getClassLoader());
        this.isLowCost = (Boolean) parcel.readValue(Boolean.class.getClassLoader());
        this.siteClassType = parcel.readString();
    }
}
