package com.solaredge.common.models;

import android.text.TextUtils;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LoadDevice {
    public static Map<String, String> icons;
    @C0631a
    @C0633c(a = "devices")
    private List<LoadControlElement> Devices = new ArrayList();
    @C0631a
    @C0633c(a = "excessPvSupported")
    private Boolean excessPvSupported;
    @C0631a
    @C0633c(a = "fieldLastUpdateTS")
    private long fieldLastUpdateTS;
    @C0631a
    @C0633c(a = "globalCommunicationStatus")
    private GlobalCommunicationStatus globalCommunicationStatus;
    @C0631a
    @C0633c(a = "isUpdated")
    private Boolean isUpdated;
    @C0631a
    @C0633c(a = "updateRefreshRate")
    private Long updateRefreshRate;

    public enum GlobalCommunicationStatus {
        ACTIVE("ACTIVE"),
        INACTIVE("INACTIVE");
        
        private final String status;

        public String getStatus() {
            return this.status;
        }

        private GlobalCommunicationStatus(String str) {
            this.status = str;
        }

        public boolean equalsStatus(String str) {
            return (this.status == null || this.status.equalsIgnoreCase(str) == null) ? null : true;
        }

        public String toString() {
            return this.status;
        }
    }

    public Boolean getExcessPvSupported() {
        return this.excessPvSupported;
    }

    public void setExcessPvSupported(Boolean bool) {
        this.excessPvSupported = bool;
    }

    public Boolean getIsUpdated() {
        return this.isUpdated;
    }

    public void setIsUpdated(Boolean bool) {
        this.isUpdated = bool;
    }

    public List<LoadControlElement> getDevices() {
        return this.Devices;
    }

    public void setDevices(List<LoadControlElement> list) {
        this.Devices = list;
    }

    public long getFieldLastUpdateTS() {
        return this.fieldLastUpdateTS;
    }

    public void setFieldLastUpdateTS(long j) {
        this.fieldLastUpdateTS = j;
    }

    public Long getUpdateRefreshRate() {
        return this.updateRefreshRate;
    }

    public void setGlobalCommunicationStatus(GlobalCommunicationStatus globalCommunicationStatus) {
        this.globalCommunicationStatus = globalCommunicationStatus;
    }

    public GlobalCommunicationStatus getGlobalCommunicationStatus() {
        return this.globalCommunicationStatus;
    }

    public boolean isEqualListOfDevices(List<LoadControlElement> list) {
        boolean z = false;
        if (getDevices().size() != list.size()) {
            return false;
        }
        int i = 0;
        for (LoadControlElement loadControlElement : getDevices()) {
            for (LoadControlElement serialNumber : list) {
                if (TextUtils.equals(loadControlElement.getSerialNumber(), serialNumber.getSerialNumber())) {
                    i++;
                    break;
                }
            }
        }
        if (i == list.size()) {
            z = true;
        }
        return z;
    }
}
