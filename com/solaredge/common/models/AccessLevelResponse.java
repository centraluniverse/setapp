package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class AccessLevelResponse {
    @C0631a
    @C0633c(a = "accountAccessLevel")
    public AccessLevel accountAccessLevel;

    public AccessLevel getAccessLevel() {
        return this.accountAccessLevel;
    }
}
