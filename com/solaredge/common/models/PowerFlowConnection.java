package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class PowerFlowConnection {
    @C0631a
    @C0633c(a = "from")
    private String from;
    @C0631a
    @C0633c(a = "to")
    private String to;

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String str) {
        this.from = str;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String str) {
        this.to = str;
    }
}
