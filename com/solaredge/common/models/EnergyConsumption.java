package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EnergyConsumption implements Parcelable {
    public static final Creator<EnergyConsumption> CREATOR = new C14081();
    @C0631a
    @C0633c(a = "month")
    private ConsumptionTimeModel month;
    @C0631a
    @C0633c(a = "today")
    private ConsumptionTimeModel today;
    @C0631a
    @C0633c(a = "total")
    private ConsumptionTimeModel total;

    static class C14081 implements Creator<EnergyConsumption> {
        C14081() {
        }

        public EnergyConsumption createFromParcel(Parcel parcel) {
            return new EnergyConsumption(parcel);
        }

        public EnergyConsumption[] newArray(int i) {
            return new EnergyConsumption[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public ConsumptionTimeModel getToday() {
        return this.today;
    }

    public void setToday(ConsumptionTimeModel consumptionTimeModel) {
        this.today = consumptionTimeModel;
    }

    public ConsumptionTimeModel getMonth() {
        return this.month;
    }

    public void setMonth(ConsumptionTimeModel consumptionTimeModel) {
        this.month = consumptionTimeModel;
    }

    public ConsumptionTimeModel getTotal() {
        return this.total;
    }

    public void setTotal(ConsumptionTimeModel consumptionTimeModel) {
        this.total = consumptionTimeModel;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.today, i);
        parcel.writeParcelable(this.month, i);
        parcel.writeParcelable(this.total, i);
    }

    protected EnergyConsumption(Parcel parcel) {
        this.today = (ConsumptionTimeModel) parcel.readParcelable(ConsumptionTimeModel.class.getClassLoader());
        this.month = (ConsumptionTimeModel) parcel.readParcelable(ConsumptionTimeModel.class.getClassLoader());
        this.total = (ConsumptionTimeModel) parcel.readParcelable(ConsumptionTimeModel.class.getClassLoader());
    }
}
