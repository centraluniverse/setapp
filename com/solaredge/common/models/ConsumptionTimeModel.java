package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class ConsumptionTimeModel implements Parcelable {
    public static final Creator<ConsumptionTimeModel> CREATOR = new C14061();
    @C0631a
    @C0633c(a = "date")
    private String date;
    @C0631a
    @C0633c(a = "unit")
    private String unit;
    @C0631a
    @C0633c(a = "value")
    private String value;

    static class C14061 implements Creator<ConsumptionTimeModel> {
        C14061() {
        }

        public ConsumptionTimeModel createFromParcel(Parcel parcel) {
            return new ConsumptionTimeModel(parcel);
        }

        public ConsumptionTimeModel[] newArray(int i) {
            return new ConsumptionTimeModel[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String str) {
        this.value = str;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String str) {
        this.date = str;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String str) {
        this.unit = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.value);
        parcel.writeString(this.date);
        parcel.writeString(this.unit);
    }

    protected ConsumptionTimeModel(Parcel parcel) {
        this.value = parcel.readString();
        this.date = parcel.readString();
        this.unit = parcel.readString();
    }
}
