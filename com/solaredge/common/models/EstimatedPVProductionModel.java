package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EstimatedPVProductionModel implements Parcelable {
    public static final Creator<EstimatedPVProductionModel> CREATOR = new C14101();
    @C0631a
    @C0633c(a = "co2")
    private EstimatedDataModel co2;
    @C0631a
    @C0633c(a = "electricBillReduction")
    private EstimatedDataModel electricBillReduction;
    @C0631a
    @C0633c(a = "estimatedEnergy")
    private EstimatedDataModel estimatedEnergy;

    static class C14101 implements Creator<EstimatedPVProductionModel> {
        C14101() {
        }

        public EstimatedPVProductionModel createFromParcel(Parcel parcel) {
            return new EstimatedPVProductionModel(parcel);
        }

        public EstimatedPVProductionModel[] newArray(int i) {
            return new EstimatedPVProductionModel[i];
        }
    }

    public EstimatedDataModel getEstimatedEnergy() {
        return this.estimatedEnergy;
    }

    public void setEstimatedEnergy(EstimatedDataModel estimatedDataModel) {
        this.estimatedEnergy = estimatedDataModel;
    }

    public EstimatedDataModel getCo2() {
        return this.co2;
    }

    public void setCo2(EstimatedDataModel estimatedDataModel) {
        this.co2 = estimatedDataModel;
    }

    public EstimatedDataModel getElectricBillReduction() {
        return this.electricBillReduction;
    }

    public void setElectricBillReduction(EstimatedDataModel estimatedDataModel) {
        this.electricBillReduction = estimatedDataModel;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.estimatedEnergy, i);
        parcel.writeParcelable(this.co2, i);
        parcel.writeParcelable(this.electricBillReduction, i);
    }

    protected EstimatedPVProductionModel(Parcel parcel) {
        this.estimatedEnergy = (EstimatedDataModel) parcel.readParcelable(EstimatedDataModel.class.getClassLoader());
        this.co2 = (EstimatedDataModel) parcel.readParcelable(EstimatedDataModel.class.getClassLoader());
        this.electricBillReduction = (EstimatedDataModel) parcel.readParcelable(EstimatedDataModel.class.getClassLoader());
    }
}
