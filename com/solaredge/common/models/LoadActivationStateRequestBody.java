package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class LoadActivationStateRequestBody {
    @C0631a
    @C0633c(a = "duration")
    private Integer duration;
    @C0631a
    @C0633c(a = "level")
    private Integer level;
    @C0631a
    @C0633c(a = "mode")
    private String mode;

    public LoadActivationStateRequestBody(String str, Integer num, Integer num2) {
        this.duration = num2;
        this.level = num;
        this.mode = str;
    }

    public LoadActivationStateRequestBody(Integer num, Integer num2) {
        this.duration = num2;
        this.level = num;
    }

    public String getMode() {
        return this.mode;
    }
}
