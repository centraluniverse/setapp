package com.solaredge.common.models;

import android.content.ContentValues;
import android.content.Context;
import android.provider.BaseColumns;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "LocaleInfo", strict = false)
public class LocaleInfo implements C1370b {
    private static final String SQL_CREATETE_TABLE = "CREATE TABLE LocaleInfo( _id INTEGER PRIMARY KEY AUTOINCREMENT, code nvarchar(10), name nvarchar(10), image_url nvarchar(50) ); ";
    public static final String TABLE_NAME = "LocaleInfo";
    private long _id;
    @Element(name = "code", required = false)
    private String code;
    @Element(name = "imageURL", required = false)
    private String imageUrl;
    @Element(name = "name", required = false)
    private String name;

    public interface TableColumns extends BaseColumns {
        public static final String CODE = "code";
        public static final String IMAGE_URL = "image_url";
        public static final String NAME = "name";
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public LocaleInfo(String str, String str2, String str3) {
        this.code = str;
        this.name = str2;
        this.imageUrl = str3;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("code", this.code);
        contentValues.put("name", this.name);
        contentValues.put(TableColumns.IMAGE_URL, this.imageUrl);
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String str) {
        this.code = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String str) {
        this.imageUrl = str;
    }
}
