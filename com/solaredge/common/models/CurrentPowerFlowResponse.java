package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class CurrentPowerFlowResponse {
    @C0631a
    @C0633c(a = "siteCurrentPowerFlow")
    private SiteCurrentPowerFlow SiteCurrentPowerFlow;

    public SiteCurrentPowerFlow getSiteCurrentPowerFlow() {
        return this.SiteCurrentPowerFlow;
    }

    public void setSiteCurrentPowerFlow(SiteCurrentPowerFlow siteCurrentPowerFlow) {
        this.SiteCurrentPowerFlow = siteCurrentPowerFlow;
    }
}
