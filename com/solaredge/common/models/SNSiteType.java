package com.solaredge.common.models;

import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "siteType", strict = false)
public class SNSiteType {
    @ElementList(entry = "equipment", inline = true, required = false)
    private List<SNEquipment> equipments;
    @Attribute(name = "value", required = false)
    private String siteType;

    public List<SNEquipment> getEquipments() {
        return this.equipments;
    }

    public void setEquipments(List<SNEquipment> list) {
        this.equipments = list;
    }

    public String getSiteType() {
        return this.siteType;
    }

    public void setSiteType(String str) {
        this.siteType = str;
    }
}
