package com.solaredge.common.models;

import java.io.Serializable;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class InverterConflict implements Serializable {
    @Element(name = "clientSerialNumber", required = false)
    public String clientSerialNumber;
    @Element(name = "layoutElementId", required = true)
    public long layoutElementId;
    @Element(name = "serverSerialNumber", required = true)
    public String serverSerialNumber;
}
