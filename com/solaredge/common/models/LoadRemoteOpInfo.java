package com.solaredge.common.models;

import android.text.TextUtils;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.p116g.C1404g;
import java.io.Serializable;

public class LoadRemoteOpInfo implements Serializable {
    @C0631a
    @C0633c(a = "errorCode")
    private Integer errorCode;
    @C0631a
    @C0633c(a = "errorDescription")
    private String errorDescription;
    @C0631a
    @C0633c(a = "requestedAction")
    private String name;
    @C0631a
    @C0633c(a = "status")
    private String status;

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null) {
            return false;
        }
        LoadRemoteOpInfo loadRemoteOpInfo = (LoadRemoteOpInfo) obj;
        if (TextUtils.equals(getName(), loadRemoteOpInfo.getName()) && TextUtils.equals(getErrorDescription(), loadRemoteOpInfo.getErrorDescription()) && TextUtils.equals(getStatus(), loadRemoteOpInfo.getStatus()) && C1404g.m3842a(getErrorCode(), loadRemoteOpInfo.getErrorCode()) != null) {
            z = true;
        }
        return z;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public Integer getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(Integer num) {
        this.errorCode = num;
    }

    public String getErrorDescription() {
        return this.errorDescription;
    }

    public void setErrorDescription(String str) {
        this.errorDescription = str;
    }

    public String getStatus() {
        return this.status;
    }
}
