package com.solaredge.common.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import com.solaredge.common.models.map.PhysicalLayout;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;

public class Map implements Parcelable, C1370b {
    public static final Creator<Map> CREATOR = new C14141();
    public static final String FIELD_TYPE_SMI = "SMI";
    public static final String FIELD_TYPE_SOLAREDGE = "SEI";
    private static final String SQL_CREATETE_TABLE = "CREATE TABLE Maps( _id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id integer, name nvarchar(60), site_id nvarchar(20), site_internal_id integer, field_type nvarchar(10), is_published integer, is_draft integer, status integer ); ";
    public static final int STATUS_DOWNLOADED = 100;
    public static final int STATUS_EDITED = 101;
    public static final int STATUS_UPLOADED = 200;
    public static final String TABLE_NAME = "Maps";
    public static final Uri URI_MAPS_LIST = Uri.parse("sqlite://com.developica.solaredge.monitor/maps/list");
    private long _id;
    private PhysicalLayout baseLayout;
    private String fieldType;
    private int isDraft;
    private int isPublished;
    private PhysicalLayout layout;
    private String name;
    private long remoteId;
    private String siteId;
    private long siteInternalId;
    private int status;

    static class C14141 implements Creator<Map> {
        C14141() {
        }

        public Map createFromParcel(Parcel parcel) {
            return new Map(parcel);
        }

        public Map[] newArray(int i) {
            return new Map[i];
        }
    }

    public interface TableColumns extends BaseColumns {
        public static final String FIELD_TYPE = "field_type";
        public static final String IS_DRAFT = "is_draft";
        public static final String IS_PUBLISHED = "is_published";
        public static final String NAME = "name";
        public static final String REMOTE_ID = "remote_id";
        public static final String SITE_ID = "site_id";
        public static final String SITE_INTERNAL_ID = "site_internal_id";
        public static final String STATUS = "status";
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public Map() {
        this.layout = new PhysicalLayout();
    }

    public Map(String str, long j, String str2, String str3) {
        this(str, j, str2, str3, 0, 1, 101);
    }

    public Map(String str, long j, String str2, String str3, int i, int i2, int i3) {
        this.name = str2;
        this.layout = new PhysicalLayout();
        this._id = -1;
        this.remoteId = -1;
        this.siteId = str;
        this.siteInternalId = j;
        this.fieldType = str3;
        this.isPublished = i;
        this.isDraft = i2;
        this.status = i3;
    }

    public Map(Cursor cursor) {
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.name = cursor.getString(cursor.getColumnIndex("name"));
        this.remoteId = cursor.getLong(cursor.getColumnIndex(TableColumns.REMOTE_ID));
        this.siteId = cursor.getString(cursor.getColumnIndex("site_id"));
        this.siteInternalId = cursor.getLong(cursor.getColumnIndex(TableColumns.SITE_INTERNAL_ID));
        this.fieldType = cursor.getString(cursor.getColumnIndex("field_type"));
        this.isPublished = cursor.getInt(cursor.getColumnIndex(TableColumns.IS_PUBLISHED));
        this.isDraft = cursor.getInt(cursor.getColumnIndex(TableColumns.IS_DRAFT));
        this.status = cursor.getInt(cursor.getColumnIndex("status"));
    }

    public Map(Parcel parcel) {
        this._id = parcel.readLong();
        this.name = parcel.readString();
        this.remoteId = parcel.readLong();
        this.fieldType = parcel.readString();
        this.isPublished = parcel.readInt();
        this.isDraft = parcel.readInt();
        this.status = parcel.readInt();
        this.layout = (PhysicalLayout) parcel.readParcelable(PhysicalLayout.class.getClassLoader());
        this.siteId = parcel.readString();
        this.siteInternalId = parcel.readLong();
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", this.name);
        contentValues.put(TableColumns.REMOTE_ID, Long.valueOf(this.remoteId));
        contentValues.put("field_type", this.fieldType);
        contentValues.put(TableColumns.IS_PUBLISHED, Integer.valueOf(this.isPublished));
        contentValues.put(TableColumns.IS_DRAFT, Integer.valueOf(this.isDraft));
        contentValues.put("status", Integer.valueOf(this.status));
        contentValues.put("site_id", this.siteId);
        contentValues.put(TableColumns.SITE_INTERNAL_ID, Long.valueOf(this.siteInternalId));
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public void update(Context context) {
        Cursor query = C1369a.m3766b(context).query(TABLE_NAME, null, "_id=?", new String[]{String.valueOf(this._id)}, null, null, null);
        if (query != null) {
            try {
                if (query.getCount() > 0) {
                    C1369a.m3766b(context).update(TABLE_NAME, getContentValues(), "_id=?", new String[]{String.valueOf(this._id)});
                    context.getContentResolver().notifyChange(URI_MAPS_LIST, null);
                }
            } catch (Throwable th) {
                query.close();
            }
        }
        query.close();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeString(this.name);
        parcel.writeLong(this.remoteId);
        parcel.writeString(this.fieldType);
        parcel.writeInt(this.isPublished);
        parcel.writeInt(this.isDraft);
        parcel.writeInt(this.status);
        parcel.writeParcelable(this.layout, i);
        parcel.writeString(this.siteId);
        parcel.writeLong(this.siteInternalId);
    }

    public int describeContents() {
        return hashCode();
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public PhysicalLayout getLayout() {
        return this.layout;
    }

    public void setLayout(PhysicalLayout physicalLayout) {
        this.layout = physicalLayout;
    }

    public PhysicalLayout getBaseLayout() {
        return this.baseLayout;
    }

    public void setBaseLayout(PhysicalLayout physicalLayout) {
        this.baseLayout = physicalLayout;
    }

    public long getRemoteId() {
        return this.remoteId;
    }

    public void setRemoteId(long j) {
        this.remoteId = j;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getFieldType() {
        return this.fieldType;
    }

    public void setFieldType(String str) {
        this.fieldType = str;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int i) {
        this.status = i;
    }

    public int getIsPublished() {
        return this.isPublished;
    }

    public void setIsPublished(int i) {
        this.isPublished = i;
    }

    public int getIsDraft() {
        return this.isDraft;
    }

    public void setIsDraft(int i) {
        this.isDraft = i;
    }

    public String getSiteId() {
        return this.siteId;
    }

    public void setSiteId(String str) {
        this.siteId = str;
    }

    public long getSiteInternalId() {
        return this.siteInternalId;
    }

    public void setSiteInternalId(long j) {
        this.siteInternalId = j;
    }
}
