package com.solaredge.common.models;

import com.solaredge.common.models.map.PhysicalLayout;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"baseVersion", "base", "client"})
@Namespace(prefix = "ns2", reference = "http://www.solaredge.com/api/physicalLayout/")
@Root(name = "mergeDraftRequest", strict = false)
public class MergeDraftRequest {
    @Element(name = "base", required = true)
    private PhysicalLayout mBaseMap;
    @Element(name = "baseVersion", required = false)
    private Long mBaseVersion;
    @Element(name = "client", required = true)
    private PhysicalLayout mClientMap;

    public MergeDraftRequest(Long l, PhysicalLayout physicalLayout, PhysicalLayout physicalLayout2) {
        this.mBaseVersion = l;
        this.mBaseMap = physicalLayout;
        this.mClientMap = physicalLayout2;
    }
}
