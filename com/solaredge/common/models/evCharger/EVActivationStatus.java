package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EVActivationStatus {
    @C0631a
    @C0633c(a = "activationState")
    private String activationState;
    @C0631a
    @C0633c(a = "activationStatus")
    private String activationStatus;

    public String getActivationStatus() {
        return this.activationStatus;
    }

    public void setActivationStatus(String str) {
        this.activationStatus = str;
    }

    public String getActivationState() {
        return this.activationState;
    }

    public void setActivationState(String str) {
        this.activationState = str;
    }
}
