package com.solaredge.common.models.evCharger;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class SiteCar implements Parcelable {
    public static final Creator<SiteCar> CREATOR = new C14221();
    @C0631a
    @C0633c(a = "friendlyName")
    private String friendlyName;
    @C0631a
    @C0633c(a = "manufacturer")
    private String manufacturer;
    @C0631a
    @C0633c(a = "model")
    private String model;
    @C0631a
    @C0633c(a = "year")
    private String year;

    static class C14221 implements Creator<SiteCar> {
        C14221() {
        }

        public SiteCar createFromParcel(Parcel parcel) {
            return new SiteCar(parcel);
        }

        public SiteCar[] newArray(int i) {
            return new SiteCar[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public SiteCar(SiteCar siteCar) {
        if (siteCar != null) {
            this.friendlyName = siteCar.getFriendlyName();
            this.model = siteCar.getModel();
            this.year = siteCar.getYear();
            this.manufacturer = siteCar.getManufacturer();
        }
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof SiteCar)) {
            return super.equals(obj);
        }
        SiteCar siteCar = (SiteCar) obj;
        obj = (siteCar.getFriendlyName().equalsIgnoreCase(this.friendlyName) && siteCar.getManufacturer().equalsIgnoreCase(this.manufacturer) && siteCar.getModel().equalsIgnoreCase(this.model) && siteCar.getYear().equalsIgnoreCase(this.year) != null) ? true : null;
        return obj;
    }

    public String getFriendlyName() {
        return this.friendlyName;
    }

    public void setFriendlyName(String str) {
        this.friendlyName = str;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String str) {
        this.model = str;
    }

    public String getYear() {
        return this.year;
    }

    public void setYear(String str) {
        this.year = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.friendlyName);
        parcel.writeString(this.manufacturer);
        parcel.writeString(this.model);
        parcel.writeString(this.year);
    }

    protected SiteCar(Parcel parcel) {
        this.friendlyName = parcel.readString();
        this.manufacturer = parcel.readString();
        this.model = parcel.readString();
        this.year = parcel.readString();
    }
}
