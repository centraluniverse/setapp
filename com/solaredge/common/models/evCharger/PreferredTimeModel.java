package com.solaredge.common.models.evCharger;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.p116g.C1404g;
import java.util.List;

public class PreferredTimeModel implements Parcelable {
    public static final Creator<PreferredTimeModel> CREATOR = new C14211();
    @C0631a
    @C0633c(a = "excessPV")
    private Integer excessPV;
    @C0631a
    @C0633c(a = "mode")
    private String mode;
    @C0631a
    @C0633c(a = "schedules")
    private List<EVChargerSchedule> schedules;

    static class C14211 implements Creator<PreferredTimeModel> {
        C14211() {
        }

        public PreferredTimeModel createFromParcel(Parcel parcel) {
            return new PreferredTimeModel(parcel);
        }

        public PreferredTimeModel[] newArray(int i) {
            return new PreferredTimeModel[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public PreferredTimeModel(PreferredTimeModel preferredTimeModel) {
        if (preferredTimeModel != null) {
            this.mode = preferredTimeModel.getMode();
            this.excessPV = preferredTimeModel.getExcessPV();
            this.schedules = preferredTimeModel.getSchedules();
        }
    }

    public PreferredTimeModel(String str, Integer num, List<EVChargerSchedule> list) {
        this.mode = str;
        this.excessPV = num;
        this.schedules = list;
    }

    public String getMode() {
        return this.mode;
    }

    public void setMode(String str) {
        this.mode = str;
    }

    public Integer getExcessPV() {
        return this.excessPV;
    }

    public void setExcessPV(Integer num) {
        this.excessPV = num;
    }

    public List<EVChargerSchedule> getSchedules() {
        return this.schedules;
    }

    public void setSchedules(List<EVChargerSchedule> list) {
        this.schedules = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mode);
        parcel.writeValue(this.excessPV);
        parcel.writeTypedList(this.schedules);
    }

    protected PreferredTimeModel(Parcel parcel) {
        this.mode = parcel.readString();
        this.excessPV = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.schedules = parcel.createTypedArrayList(EVChargerSchedule.CREATOR);
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj != null && (obj instanceof PreferredTimeModel)) {
            PreferredTimeModel preferredTimeModel = (PreferredTimeModel) obj;
            if (!(getSchedules().get(0) == null || preferredTimeModel.getSchedules() == null || preferredTimeModel.getSchedules().size() <= 0 || preferredTimeModel.getSchedules().get(0) == null)) {
                if (C1404g.m3842a(getExcessPV(), preferredTimeModel.getExcessPV()) && C1404g.m3842a(getMode(), preferredTimeModel.getMode()) && C1404g.m3842a(((EVChargerSchedule) getSchedules().get(0)).getStart(), ((EVChargerSchedule) preferredTimeModel.getSchedules().get(0)).getStart()) && C1404g.m3842a(((EVChargerSchedule) getSchedules().get(0)).getEnd(), ((EVChargerSchedule) preferredTimeModel.getSchedules().get(0)).getEnd()) != null) {
                    z = true;
                }
                return z;
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Mode: ");
        stringBuilder.append(this.mode);
        stringBuilder.append("\nexcessPV: ");
        stringBuilder.append(this.excessPV);
        stringBuilder.append("\nschedule start: ");
        stringBuilder.append(((EVChargerSchedule) this.schedules.get(0)).getStart().toString());
        stringBuilder.append("\nschedule end: ");
        stringBuilder.append(((EVChargerSchedule) this.schedules.get(0)).getEnd().toString());
        return stringBuilder.toString();
    }
}
