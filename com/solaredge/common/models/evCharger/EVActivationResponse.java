package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EVActivationResponse {
    @C0631a
    @C0633c(a = "isApproved")
    private Boolean isApproved;
    @C0631a
    @C0633c(a = "verificationStatusCode")
    private String verificationStatusCode;

    public Boolean getApproved() {
        return this.isApproved;
    }

    public void setApproved(Boolean bool) {
        this.isApproved = bool;
    }

    public String getVerificationStatusCode() {
        return this.verificationStatusCode;
    }

    public void setVerificationStatusCode(String str) {
        this.verificationStatusCode = str;
    }
}
