package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class HistorySession {
    @C0631a
    @C0633c(a = "carName")
    private String carName;
    @C0631a
    @C0633c(a = "date")
    private String date;
    @C0631a
    @C0633c(a = "distance")
    private String distance;
    @C0631a
    @C0633c(a = "distanceUnits")
    private String distanceUnits;
    @C0631a
    @C0633c(a = "duration")
    private String duration;
    @C0631a
    @C0633c(a = "energy")
    private String energy;
    @C0631a
    @C0633c(a = "energyUnits")
    private String energyUnits;
    @C0631a
    @C0633c(a = "solarLevel")
    private Float solarLevel;
    @C0631a
    @C0633c(a = "time")
    private String time;
    private boolean whiteBackground;

    public String getDate() {
        return this.date;
    }

    public void setDate(String str) {
        this.date = str;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String str) {
        this.time = str;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String str) {
        this.duration = str;
    }

    public String getEnergy() {
        return this.energy;
    }

    public void setEnergy(String str) {
        this.energy = str;
    }

    public String getEnergyUnits() {
        return this.energyUnits;
    }

    public void setEnergyUnits(String str) {
        this.energyUnits = str;
    }

    public String getDistance() {
        return this.distance;
    }

    public void setDistance(String str) {
        this.distance = str;
    }

    public String getDistanceUnits() {
        return this.distanceUnits;
    }

    public void setDistanceUnits(String str) {
        this.distanceUnits = str;
    }

    public Float getSolarLevel() {
        return this.solarLevel;
    }

    public void setSolarLevel(Float f) {
        this.solarLevel = f;
    }

    public String getCarName() {
        return this.carName;
    }

    public void setCarName(String str) {
        this.carName = str;
    }

    public boolean isWhiteBackground() {
        return this.whiteBackground;
    }

    public void setWhiteBackground(boolean z) {
        this.whiteBackground = z;
    }
}
