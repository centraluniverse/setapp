package com.solaredge.common.models.evCharger;

import android.support.v4.util.ArrayMap;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.List;

public class CarModel {
    @C0631a
    @C0633c(a = "name")
    private String model;
    @C0631a
    @C0633c(a = "years")
    private ArrayMap<String, List<String>> yearsMap;
}
