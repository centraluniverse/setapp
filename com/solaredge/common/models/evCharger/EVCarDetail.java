package com.solaredge.common.models.evCharger;

import android.support.v4.util.ArrayMap;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EVCarDetail {
    @C0631a
    @C0633c(a = "manufacturers")
    private ArrayMap<String, CarManufacturer> manufacturers;

    public ArrayMap<String, CarManufacturer> getManufacturers() {
        return this.manufacturers;
    }

    public void setManufacturers(ArrayMap<String, CarManufacturer> arrayMap) {
        this.manufacturers = arrayMap;
    }
}
