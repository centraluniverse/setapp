package com.solaredge.common.models.evCharger;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.util.ArrayMap;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CarManufacturer implements Parcelable {
    public static final Creator<CarManufacturer> CREATOR = new C14191();
    @C0631a
    @C0633c(a = "models")
    private Map<String, List<String>> models;
    @C0631a
    @C0633c(a = "name")
    private String name;

    static class C14191 implements Creator<CarManufacturer> {
        C14191() {
        }

        public CarManufacturer createFromParcel(Parcel parcel) {
            return new CarManufacturer(parcel);
        }

        public CarManufacturer[] newArray(int i) {
            return new CarManufacturer[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public CarManufacturer(String str, ArrayMap<String, List<String>> arrayMap) {
        this.name = str;
        this.models = arrayMap;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public Map<String, List<String>> getModels() {
        return this.models;
    }

    public void setModels(Map<String, List<String>> map) {
        this.models = map;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeInt(this.models.size());
        for (Entry entry : this.models.entrySet()) {
            parcel.writeString((String) entry.getKey());
            parcel.writeStringList((List) entry.getValue());
        }
    }

    protected CarManufacturer(Parcel parcel) {
        this.name = parcel.readString();
        int readInt = parcel.readInt();
        this.models = new HashMap(readInt);
        for (int i = 0; i < readInt; i++) {
            this.models.put(parcel.readString(), parcel.createStringArrayList());
        }
    }
}
