package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.List;

public class ChargingHistory {
    @C0631a
    @C0633c(a = "sessions")
    private List<HistorySession> historySessionList;

    public List<HistorySession> getHistorySessionList() {
        return this.historySessionList;
    }

    public void setHistorySessionList(List<HistorySession> list) {
        this.historySessionList = list;
    }
}
