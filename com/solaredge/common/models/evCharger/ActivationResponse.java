package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class ActivationResponse {
    public static final String Busy = "BUSY";
    public static final String Failed = "FAILED";
    public static final String Passed = "PASSED";
    public static final String Warning = "WARNING";
    @C0631a
    @C0633c(a = "errorMessages")
    private List<Object> errorMessages = null;
    @C0631a
    @C0633c(a = "infoMessages")
    private List<Object> infoMessages = null;
    @C0631a
    @C0633c(a = "status")
    private String status;
    @C0631a
    @C0633c(a = "warningMessages")
    private List<Object> warningMessages = null;

    @Retention(RetentionPolicy.SOURCE)
    public @interface ResponseStatus {
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public List<Object> getWarningMessages() {
        return this.warningMessages;
    }

    public void setWarningMessages(List<Object> list) {
        this.warningMessages = list;
    }

    public List<Object> getErrorMessages() {
        return this.errorMessages;
    }

    public void setErrorMessages(List<Object> list) {
        this.errorMessages = list;
    }

    public List<Object> getInfoMessages() {
        return this.infoMessages;
    }

    public void setInfoMessages(List<Object> list) {
        this.infoMessages = list;
    }
}
