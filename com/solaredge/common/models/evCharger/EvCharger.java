package com.solaredge.common.models.evCharger;

import android.p007b.C1698a;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.BR;
import com.solaredge.common.models.AdditionalText;
import com.solaredge.common.models.SessionDuration;
import com.solaredge.common.models.SessionEnergy;
import com.solaredge.common.models.SessionMiles;
import com.solaredge.common.models.SessionStart;
import com.solaredge.common.models.StatusSubtitle;
import com.solaredge.common.models.StatusTitle;
import com.solaredge.common.models.response.SolarUsage;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class EvCharger extends C1698a {
    public static final String Charging = "CHARGING";
    public static final String Connected = "CONNECTED";
    public static final String Disconnected = "DISCONNECTED";
    public static final String ExcessPV = "EXCESS_PV";
    public static final String Fault = "FAULT";
    public static final String Initializing = "INITIALIZING";
    public static final String NoCommunication = "NO_COMM";
    public static final String ScheduleCharge = "SCHEDULE_CHARGE";
    public static final String SolarBoost = "SOLAR_BOOST";
    public static final String SubCharging = "CHARGING";
    public static final String Unknown = "UNKNOWN";
    public static final String Warning = "WARNING";
    @C0631a
    @C0633c(a = "actionOp")
    private List<ActionOpModel> actionOp;
    @C0631a
    @C0633c(a = "activationState")
    private String activationState;
    @C0631a
    @C0633c(a = "additionalText")
    private AdditionalText additionalText;
    @C0631a
    @C0633c(a = "opStatus")
    private String opStatus;
    @C0631a
    @C0633c(a = "reporterId")
    public Integer reporterId;
    @C0631a
    @C0633c(a = "scheduleInfo")
    private ScheduleInfo scheduleInfo;
    @C0631a
    @C0633c(a = "scheduleTitle")
    private ScheduleTitle scheduleTitle;
    @C0631a
    @C0633c(a = "serialNumber")
    private String serialNumber;
    @C0631a
    @C0633c(a = "sessionActive")
    private Boolean sessionActive;
    @C0631a
    @C0633c(a = "sessionDuration")
    private SessionDuration sessionDuration;
    @C0631a
    @C0633c(a = "sessionEnergy")
    private SessionEnergy sessionEnergy;
    @C0631a
    @C0633c(a = "sessionMiles")
    private SessionMiles sessionMiles;
    @C0631a
    @C0633c(a = "sessionSolarUsage")
    private SolarUsage sessionSolarUsage;
    @C0631a
    @C0633c(a = "sessionStart")
    private SessionStart sessionStart;
    @C0631a
    @C0633c(a = "status")
    private String status;
    @C0631a
    @C0633c(a = "statusSubtitle")
    private List<StatusSubtitle> statusSubtitle;
    @C0631a
    @C0633c(a = "statusTitle")
    public StatusTitle statusTitle;
    @C0631a
    @C0633c(a = "subStatus")
    public String subStatus;

    @Retention(RetentionPolicy.SOURCE)
    public @interface Status {
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface SubStatus {
    }

    public String getOpStatus() {
        return this.opStatus;
    }

    public void setOpStatus(String str) {
        this.opStatus = str;
    }

    public SessionMiles getSessionMiles() {
        return this.sessionMiles;
    }

    public void setSessionMiles(SessionMiles sessionMiles) {
        this.sessionMiles = sessionMiles;
        notifyPropertyChanged(BR.sessionMiles);
    }

    public SolarUsage getSessionSolarUsage() {
        return this.sessionSolarUsage;
    }

    public AdditionalText getAdditionalText() {
        return this.additionalText;
    }

    public void setAdditionalText(AdditionalText additionalText) {
        this.additionalText = additionalText;
        notifyPropertyChanged(BR.additionalText);
    }

    public Integer getReporterId() {
        return this.reporterId;
    }

    public void setReporterId(Integer num) {
        this.reporterId = num;
    }

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(String str) {
        this.serialNumber = str;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String str) {
        this.status = str;
        notifyPropertyChanged(BR.status);
    }

    public String getSubStatus() {
        return this.subStatus;
    }

    public void setSubStatus(String str) {
        this.subStatus = str;
        notifyPropertyChanged(BR.subStatus);
    }

    public StatusTitle getStatusTitle() {
        return this.statusTitle;
    }

    public void setStatusTitle(StatusTitle statusTitle) {
        this.statusTitle = statusTitle;
        notifyPropertyChanged(BR.statusTitle);
    }

    public List<StatusSubtitle> getStatusSubtitle() {
        return this.statusSubtitle;
    }

    public void setStatusSubtitle(List<StatusSubtitle> list) {
        this.statusSubtitle = list;
        notifyPropertyChanged(BR.statusSubtitle);
    }

    public ScheduleTitle getScheduleTitle() {
        return this.scheduleTitle;
    }

    public void setScheduleTitle(ScheduleTitle scheduleTitle) {
        this.scheduleTitle = scheduleTitle;
        notifyPropertyChanged(BR.scheduleTitle);
    }

    public ScheduleInfo getScheduleInfo() {
        return this.scheduleInfo;
    }

    public void setScheduleInfo(ScheduleInfo scheduleInfo) {
        this.scheduleInfo = scheduleInfo;
        notifyPropertyChanged(BR.scheduleInfo);
    }

    public SessionStart getSessionStart() {
        return this.sessionStart;
    }

    public void setSessionStart(SessionStart sessionStart) {
        this.sessionStart = sessionStart;
        notifyPropertyChanged(BR.sessionStart);
    }

    public Boolean getSessionActive() {
        return this.sessionActive;
    }

    public void setSessionActive(Boolean bool) {
        this.sessionActive = bool;
        notifyPropertyChanged(BR.sessionActive);
    }

    public SessionDuration getSessionDuration() {
        return this.sessionDuration;
    }

    public void setSessionDuration(SessionDuration sessionDuration) {
        this.sessionDuration = sessionDuration;
        notifyPropertyChanged(BR.sessionDuration);
    }

    public SessionEnergy getSessionEnergy() {
        return this.sessionEnergy;
    }

    public void setSessionEnergy(SessionEnergy sessionEnergy) {
        this.sessionEnergy = sessionEnergy;
        notifyPropertyChanged(BR.sessionEnergy);
    }

    public List<ActionOpModel> getActionOp() {
        return this.actionOp;
    }

    public void setActionOp(List<ActionOpModel> list) {
        this.actionOp = list;
        notifyPropertyChanged(BR.actionOp);
    }

    public String getActivationState() {
        return this.activationState;
    }

    public void setActivationState(String str) {
        this.activationState = str;
    }
}
