package com.solaredge.common.models.evCharger;

public interface EvStyleText {
    String getText();

    String getType();

    void setText(String str);

    void setType(String str);
}
