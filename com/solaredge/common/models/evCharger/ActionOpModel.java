package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class ActionOpModel {
    @C0631a
    @C0633c(a = "actionOp")
    private String actionOp;
    @C0631a
    @C0633c(a = "actionText")
    private ActionText actionText;
    @C0631a
    @C0633c(a = "primaryOp")
    private Boolean primaryOp;

    public ActionText getActionText() {
        return this.actionText;
    }

    public void setActionText(ActionText actionText) {
        this.actionText = actionText;
    }

    public String getActionOp() {
        return this.actionOp;
    }

    public void setActionOp(String str) {
        this.actionOp = str;
    }

    public Boolean getPrimaryOp() {
        return this.primaryOp;
    }

    public void setPrimaryOp(Boolean bool) {
        this.primaryOp = bool;
    }
}
