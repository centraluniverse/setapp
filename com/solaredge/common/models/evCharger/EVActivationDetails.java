package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EVActivationDetails {
    @C0631a
    @C0633c(a = "activationCode")
    private String activationCode;
    @C0631a
    @C0633c(a = "cableSN")
    private String cableSN;

    public EVActivationDetails(String str, String str2) {
        this.cableSN = str;
        this.activationCode = str2;
    }

    public String getCableSN() {
        return this.cableSN;
    }

    public void setCableSN(String str) {
        this.cableSN = str;
    }

    public String getActivationCode() {
        return this.activationCode;
    }

    public void setActivationCode(String str) {
        this.activationCode = str;
    }
}
