package com.solaredge.common.models.evCharger;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EVChargerSchedule implements Parcelable {
    public static final Creator<EVChargerSchedule> CREATOR = new C14201();
    @C0631a
    @C0633c(a = "end")
    private Integer end;
    @C0631a
    @C0633c(a = "start")
    private Integer start;

    static class C14201 implements Creator<EVChargerSchedule> {
        C14201() {
        }

        public EVChargerSchedule createFromParcel(Parcel parcel) {
            return new EVChargerSchedule(parcel);
        }

        public EVChargerSchedule[] newArray(int i) {
            return new EVChargerSchedule[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public EVChargerSchedule(Integer num, Integer num2) {
        this.start = num;
        this.end = num2;
    }

    public Integer getStart() {
        return this.start;
    }

    public void setStart(Integer num) {
        this.start = num;
    }

    public Integer getEnd() {
        return this.end;
    }

    public void setEnd(Integer num) {
        this.end = num;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.start);
        parcel.writeValue(this.end);
    }

    protected EVChargerSchedule(Parcel parcel) {
        this.start = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.end = (Integer) parcel.readValue(Integer.class.getClassLoader());
    }
}
