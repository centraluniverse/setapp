package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class PowerFlowCharger {
    @C0631a
    @C0633c(a = "currentPower")
    private Float currentPower;
    @C0631a
    @C0633c(a = "faultLabel")
    private String faultLabel;
    @C0631a
    @C0633c(a = "label")
    private String label;
    @C0631a
    @C0633c(a = "status")
    private String status;

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String str) {
        this.label = str;
    }

    public Float getCurrentPower() {
        return this.currentPower;
    }

    public void setCurrentPower(Float f) {
        this.currentPower = f;
    }

    public String getFaultLabel() {
        return this.faultLabel;
    }

    public void setFaultLabel(String str) {
        this.faultLabel = str;
    }
}
