package com.solaredge.common.models.evCharger;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EVChargerActionBody {
    @C0631a
    @C0633c(a = "actionOp")
    private String actionOp;

    public EVChargerActionBody(String str) {
        this.actionOp = str;
    }

    public String getActionOp() {
        return this.actionOp;
    }

    public void setActionOp(String str) {
        this.actionOp = str;
    }
}
