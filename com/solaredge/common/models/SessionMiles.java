package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.models.evCharger.EvStyleText;

public class SessionMiles implements EvStyleText {
    @C0631a
    @C0633c(a = "text")
    private String text;
    @C0631a
    @C0633c(a = "type")
    private String type;

    public String getText() {
        return this.text;
    }

    public void setText(String str) {
        this.text = str;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String str) {
        this.type = str;
    }
}
