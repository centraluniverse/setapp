package com.solaredge.common.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.provider.BaseColumns;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import com.solaredge.common.p112c.C1369a;
import com.solaredge.common.p112c.C1370b;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "inverterModel", strict = false)
public class InverterModel implements Parcelable, C1370b {
    public static final Creator<InverterModel> CREATOR = ParcelableCompat.newCreator(new C21971());
    private static final String SQL_CREATETE_TABLE = "CREATE TABLE InverterModels( _id INTEGER PRIMARY KEY AUTOINCREMENT, manufacturer_name nvarchar(60), model_name nvarchar(20), output_power number  ); ";
    public static final String TABLE_NAME = "InverterModels";
    private long _id;
    @Element(name = "manufacturerName", required = false)
    private String manufacturerName;
    @Element(name = "modelName", required = false)
    private String modelName;
    @Element(name = "outputPower", required = false)
    private float outputPower;

    public interface TableColumns extends BaseColumns {
        public static final String MANUFACTURER_NAME = "manufacturer_name";
        public static final String MODEL_NAME = "model_name";
        public static final String OUTPUT_POWER = "output_power";
    }

    static class C21971 implements ParcelableCompatCreatorCallbacks<InverterModel> {
        C21971() {
        }

        public InverterModel createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new InverterModel(parcel);
        }

        public InverterModel[] newArray(int i) {
            return new InverterModel[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getTableName() {
        return TABLE_NAME;
    }

    public static String[] getInitDbStatements() {
        return new String[]{SQL_CREATETE_TABLE};
    }

    public InverterModel(Cursor cursor) {
        this._id = cursor.getLong(cursor.getColumnIndex("_id"));
        this.manufacturerName = cursor.getString(cursor.getColumnIndex(TableColumns.MANUFACTURER_NAME));
        this.modelName = cursor.getString(cursor.getColumnIndex(TableColumns.MODEL_NAME));
        this.outputPower = cursor.getFloat(cursor.getColumnIndex(TableColumns.OUTPUT_POWER));
    }

    protected InverterModel(Parcel parcel) {
        this._id = parcel.readLong();
        this.manufacturerName = parcel.readString();
        this.modelName = parcel.readString();
        this.outputPower = parcel.readFloat();
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableColumns.MANUFACTURER_NAME, this.manufacturerName);
        contentValues.put(TableColumns.MODEL_NAME, this.modelName);
        contentValues.put(TableColumns.OUTPUT_POWER, Float.valueOf(this.outputPower));
        return contentValues;
    }

    public long save(Context context) {
        this._id = C1369a.m3765a(context).m3768a((C1370b) this);
        return this._id;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this._id);
        parcel.writeString(this.manufacturerName);
        parcel.writeString(this.modelName);
        parcel.writeFloat(this.outputPower);
    }

    public long getId() {
        return this._id;
    }

    public void setId(long j) {
        this._id = j;
    }

    public String getManufacturerName() {
        return this.manufacturerName;
    }

    public void setManufacturerName(String str) {
        this.manufacturerName = str;
    }

    public String getModelName() {
        return this.modelName;
    }

    public void setModelName(String str) {
        this.modelName = str;
    }

    public float getOutputPower() {
        return this.outputPower;
    }

    public void setOutputPower(float f) {
        this.outputPower = f;
    }
}
