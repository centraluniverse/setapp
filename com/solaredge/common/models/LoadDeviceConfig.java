package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.io.Serializable;

public class LoadDeviceConfig implements Serializable {
    @C0631a
    @C0633c(a = "deviceName")
    private String deviceName;
    @C0631a
    @C0633c(a = "iconName")
    private String iconName;
    @C0631a
    @C0633c(a = "maxOnTime")
    private Integer maxOnTime;
    @C0631a
    @C0633c(a = "minOnTime")
    private Integer minOnTime;
    @C0631a
    @C0633c(a = "ratedPower")
    private Float ratedPower;

    public String getIconName() {
        return this.iconName;
    }

    public void setIconName(String str) {
        this.iconName = str;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String str) {
        this.deviceName = str;
    }

    public Float getRatedPower() {
        return this.ratedPower;
    }

    public void setRatedPower(Float f) {
        this.ratedPower = f;
    }

    public Integer getMinOnTime() {
        return this.minOnTime;
    }

    public void setMinOnTime(Integer num) {
        this.minOnTime = num;
    }

    public Integer getMaxOnTime() {
        return this.maxOnTime;
    }

    public void setMaxOnTime(Integer num) {
        this.maxOnTime = num;
    }
}
