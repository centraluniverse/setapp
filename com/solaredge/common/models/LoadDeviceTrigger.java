package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.p116g.C1404g;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoadDeviceTrigger implements Serializable {
    @C0631a
    @C0633c(a = "enable")
    private Boolean enable;
    @C0631a
    @C0633c(a = "endTime")
    private Integer endTime;
    private int id;
    @C0631a
    @C0633c(a = "maxOnDuration")
    private Integer maxOnDuration;
    @C0631a
    @C0633c(a = "minOnDuration")
    private Integer minOnDuration;
    @C0631a
    @C0633c(a = "scheduledDays")
    private List<String> scheduledDays = new ArrayList();
    @C0631a
    @C0633c(a = "smartSaver")
    private Boolean smartSaver;
    @C0631a
    @C0633c(a = "startTime")
    private Integer startTime;

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public Boolean getEnable() {
        return this.enable;
    }

    public void setEnable(Boolean bool) {
        this.enable = bool;
    }

    public Integer getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Integer num) {
        this.startTime = num;
    }

    public Boolean getSmartSaver() {
        return this.smartSaver;
    }

    public void setSmartSaver(Boolean bool) {
        this.smartSaver = bool;
    }

    public Integer getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Integer num) {
        this.endTime = num;
    }

    public Integer getMinOnDuration() {
        return this.minOnDuration;
    }

    public void setMinOnDuration(Integer num) {
        this.minOnDuration = num;
    }

    public Integer getMaxOnDuration() {
        return this.maxOnDuration;
    }

    public void setMaxOnDuration(Integer num) {
        this.maxOnDuration = num;
    }

    public List<String> getScheduledDays() {
        return this.scheduledDays;
    }

    public void setScheduledDays(List<String> list) {
        this.scheduledDays = list;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null) {
            return false;
        }
        LoadDeviceTrigger loadDeviceTrigger = (LoadDeviceTrigger) obj;
        if (C1404g.m3842a(getEnable(), loadDeviceTrigger.getEnable()) && C1404g.m3842a(getSmartSaver(), loadDeviceTrigger.getSmartSaver()) && C1404g.m3842a(getEndTime(), loadDeviceTrigger.getEndTime()) && C1404g.m3842a(getStartTime(), loadDeviceTrigger.getStartTime()) && C1404g.m3842a(Integer.valueOf(getId()), Integer.valueOf(loadDeviceTrigger.getId())) && C1404g.m3842a(getMaxOnDuration(), loadDeviceTrigger.getMaxOnDuration()) && C1404g.m3842a(getMinOnDuration(), loadDeviceTrigger.getMinOnDuration()) && C1404g.m3845a(getScheduledDays(), loadDeviceTrigger.getScheduledDays()) != null) {
            z = true;
        }
        return z;
    }
}
