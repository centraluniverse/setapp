package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import com.solaredge.common.p114e.C1382c;

public enum AccessLevel {
    None("None"),
    View("View"),
    Control("Control");
    
    @C0631a
    @C0633c(a = "accountAccessLevel")
    private String accountAccessLevel;

    private AccessLevel(String str) {
        this.accountAccessLevel = str;
    }

    public boolean equalType(String str) {
        return (this.accountAccessLevel == null || this.accountAccessLevel.equalsIgnoreCase(str) == null) ? null : true;
    }

    public String toString() {
        return this.accountAccessLevel;
    }

    public String toPrettyString() {
        if (None.equalType(this.accountAccessLevel)) {
            return C1382c.m3782a().m3783a(C1382c.f3430r);
        }
        if (View.equalType(this.accountAccessLevel)) {
            return C1382c.m3782a().m3783a(C1382c.f3431s);
        }
        return Control.equalType(this.accountAccessLevel) ? C1382c.m3782a().m3783a(C1382c.f3432t) : null;
    }

    public String printDetailedDescription() {
        if (None.equalType(this.accountAccessLevel)) {
            return C1382c.m3782a().m3783a(C1382c.f3425m);
        }
        if (View.equalType(this.accountAccessLevel)) {
            return C1382c.m3782a().m3783a(C1382c.f3426n);
        }
        return Control.equalType(this.accountAccessLevel) ? C1382c.m3782a().m3783a(C1382c.f3427o) : null;
    }
}
