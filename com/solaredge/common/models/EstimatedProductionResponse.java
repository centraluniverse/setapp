package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class EstimatedProductionResponse implements Parcelable {
    public static final Creator<EstimatedProductionResponse> CREATOR = new C14111();
    @C0631a
    @C0633c(a = "EstimatedProduction")
    private EstimatedPVProductionModel estimatedPVProductionModel;

    static class C14111 implements Creator<EstimatedProductionResponse> {
        C14111() {
        }

        public EstimatedProductionResponse createFromParcel(Parcel parcel) {
            return new EstimatedProductionResponse(parcel);
        }

        public EstimatedProductionResponse[] newArray(int i) {
            return new EstimatedProductionResponse[i];
        }
    }

    public EstimatedPVProductionModel getEstimatedPVProductionModel() {
        return this.estimatedPVProductionModel;
    }

    public void setEstimatedPVProductionModel(EstimatedPVProductionModel estimatedPVProductionModel) {
        this.estimatedPVProductionModel = estimatedPVProductionModel;
    }

    public int describeContents() {
        return hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.estimatedPVProductionModel, i);
    }

    protected EstimatedProductionResponse(Parcel parcel) {
        this.estimatedPVProductionModel = (EstimatedPVProductionModel) parcel.readParcelable(EstimatedPVProductionModel.class.getClassLoader());
    }
}
