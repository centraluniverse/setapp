package com.solaredge.common.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "location", strict = false)
public class SiteLocation {
    @Element(name = "address1", required = true)
    private String address;
    @Element(name = "address2", required = false)
    private String address2;
    @Element(name = "city", required = false)
    private String city;
    @Element(name = "country", required = true)
    private String countryCode;
    @Element(name = "latitude", required = true)
    private double latitude;
    @Element(name = "longitude", required = true)
    private double longitude;
    @Element(name = "state", required = false)
    private String state;
    @Element(name = "zip", required = true)
    private String zipCode;

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String str) {
        this.countryCode = str;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String str) {
        this.state = str;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String str) {
        this.address = str;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String str) {
        this.address2 = str;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(String str) {
        this.zipCode = str;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double d) {
        this.latitude = d;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double d) {
        this.longitude = d;
    }
}
