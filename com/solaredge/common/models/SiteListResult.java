package com.solaredge.common.models;

import java.util.ArrayList;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "sitesResult", strict = false)
public class SiteListResult {
    @ElementList(entry = "mapperSite", name = "sites", required = false)
    private ArrayList<SolarSite> solarSites;
    @Element(name = "count", required = false)
    private int total;

    public int getTotal() {
        return this.total;
    }

    public void setTotal(int i) {
        this.total = i;
    }

    public ArrayList<SolarSite> getSolarSites() {
        return this.solarSites;
    }

    public void setSolarSites(ArrayList<SolarSite> arrayList) {
        this.solarSites = arrayList;
    }
}
