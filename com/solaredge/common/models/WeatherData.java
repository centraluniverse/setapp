package com.solaredge.common.models;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.crashlytics.android.Crashlytics;
import com.solaredge.common.C1366a;
import com.solaredge.common.C1379d.C1374c;
import java.util.HashMap;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "LiveWeatherDto", strict = false)
public class WeatherData implements Parcelable {
    public static final Creator<WeatherData> CREATOR = new C14171();
    private static HashMap<String, Integer> mWeatherImageMap;
    @Element(name = "currentCondition", required = false)
    private String currentCondition;
    @Element(name = "imageURL", required = false)
    private String currentConditionIcon;
    @Element(name = "currentTemperature", required = false)
    private float currentTemperature;
    @Element(name = "feelsLike", required = false)
    private String feelsLike;
    @Element(name = "humidity", required = false)
    private String humidity;
    @Element(name = "sunrise", required = false)
    private String sunrise;
    @Element(name = "sunset", required = false)
    private String sunset;
    @Element(name = "windSpeed", required = false)
    private String windSpeed;

    static class C14171 implements Creator<WeatherData> {
        C14171() {
        }

        public WeatherData createFromParcel(Parcel parcel) {
            return new WeatherData(parcel);
        }

        public WeatherData[] newArray(int i) {
            return new WeatherData[i];
        }
    }

    public WeatherData(Parcel parcel) {
        this.currentTemperature = parcel.readFloat();
        this.currentCondition = parcel.readString();
        this.currentConditionIcon = parcel.readString();
        this.sunrise = parcel.readString();
        this.sunset = parcel.readString();
        this.feelsLike = parcel.readString();
        this.windSpeed = parcel.readString();
        this.humidity = parcel.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.currentTemperature);
        parcel.writeString(this.currentCondition);
        parcel.writeString(this.currentConditionIcon);
        parcel.writeString(this.sunrise);
        parcel.writeString(this.sunset);
        parcel.writeString(this.feelsLike);
        parcel.writeString(this.windSpeed);
        parcel.writeString(this.humidity);
    }

    public int describeContents() {
        return hashCode();
    }

    public float getCurrentTemperature() {
        return this.currentTemperature;
    }

    public void setCurrentTemperature(float f) {
        this.currentTemperature = f;
    }

    public String getCurrentCondition() {
        return this.currentCondition;
    }

    public void setCurrentCondition(String str) {
        this.currentCondition = str;
    }

    public String getCurrentConditionIcon() {
        return this.currentConditionIcon;
    }

    public void setCurrentConditionIcon(String str) {
        this.currentConditionIcon = str;
    }

    public String getSunrise() {
        return this.sunrise;
    }

    public void setSunrise(String str) {
        this.sunrise = str;
    }

    public String getSunset() {
        return this.sunset;
    }

    public void setSunset(String str) {
        this.sunset = str;
    }

    public String getFeelsLike() {
        return this.feelsLike;
    }

    public void setFeelsLike(String str) {
        this.feelsLike = str;
    }

    public String getWindSpeed() {
        return this.windSpeed;
    }

    public void setWindSpeed(String str) {
        this.windSpeed = str;
    }

    public String getHumidity() {
        return this.humidity;
    }

    public void setHumidity(String str) {
        this.humidity = str;
    }

    private static void setWeatherImageMap() {
        mWeatherImageMap.put("Cloudy", Integer.valueOf(C1374c.ic_weather_big_cloud));
        mWeatherImageMap.put("Dust", Integer.valueOf(C1374c.ic_weather_big_dust));
        mWeatherImageMap.put("Fair", Integer.valueOf(C1374c.ic_weather_big_fair));
        mWeatherImageMap.put("Fog", Integer.valueOf(C1374c.ic_weather_big_fog));
        mWeatherImageMap.put("FrozenMix", Integer.valueOf(C1374c.ic_weather_big_frozen_mix));
        mWeatherImageMap.put("Hurricane", Integer.valueOf(C1374c.ic_weather_big_hurricane));
        mWeatherImageMap.put("Partly", Integer.valueOf(C1374c.ic_weather_big_partly_cloudy));
        mWeatherImageMap.put("Showers", Integer.valueOf(C1374c.ic_weather_big_showers));
        mWeatherImageMap.put("Sunny", Integer.valueOf(C1374c.ic_weather_big_sunny));
        mWeatherImageMap.put("Windy", Integer.valueOf(C1374c.ic_weather_big_wind));
        mWeatherImageMap.put("Rain", Integer.valueOf(C1374c.ic_weather_big_light_rain));
        mWeatherImageMap.put("LightRain", Integer.valueOf(C1374c.ic_weather_big_light_rain));
        mWeatherImageMap.put("Snow", Integer.valueOf(C1374c.ic_weather_big_snow));
    }

    private static HashMap<String, Integer> getWeatherImageMap() {
        if (mWeatherImageMap == null) {
            mWeatherImageMap = new HashMap();
            setWeatherImageMap();
        }
        return mWeatherImageMap;
    }

    public static Drawable getWeatherDrawable(String str) {
        str = getWeatherResource(str);
        if (str == null) {
            return null;
        }
        return C1366a.m3749a().m3753b().getResources().getDrawable(str.intValue());
    }

    public static Integer getWeatherResource(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf(47) + 1;
        int lastIndexOf2 = str.lastIndexOf(46);
        if (lastIndexOf >= 0 && lastIndexOf2 >= 0 && lastIndexOf2 < str.length() && lastIndexOf < str.length()) {
            if (lastIndexOf < lastIndexOf2) {
                return (Integer) getWeatherImageMap().get(str.substring(lastIndexOf, lastIndexOf2));
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("The  weather image string is illegal - ");
        stringBuilder.append(str);
        Crashlytics.logException(new IndexOutOfBoundsException(stringBuilder.toString()));
        return null;
    }
}
