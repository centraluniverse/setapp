package com.solaredge.common.models;

import java.io.Serializable;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class GroupConflicts implements Serializable {
    @Element(name = "deletionType", required = false)
    private DeletionType deletionType;
    @Element(name = "groupId", required = false)
    private long groupId;
    @ElementList(entry = "unintentionalDeletionConflict", inline = true, required = false)
    private List<UnintentionalDeletionConflict> unintentionalDeletionConflicts;

    public enum DeletionType implements Serializable {
        SERIAL_DELETION("SERIAL_DELETION"),
        GROUP_DELETION("GROUP_DELETION");
        
        private String value;

        private DeletionType(String str) {
            this.value = str;
        }

        public String getValue() {
            return this.value;
        }

        public String toString() {
            return getValue();
        }
    }

    public long getGroupId() {
        return this.groupId;
    }

    public DeletionType getDeletionType() {
        return this.deletionType;
    }

    public List<UnintentionalDeletionConflict> getUnintentionalDeletionConflicts() {
        return this.unintentionalDeletionConflicts;
    }

    public long getNumOfConflicts() {
        return DeletionType.GROUP_DELETION == this.deletionType ? 1 : (long) this.unintentionalDeletionConflicts.size();
    }
}
