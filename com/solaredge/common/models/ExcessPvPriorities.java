package com.solaredge.common.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;
import java.util.ArrayList;
import java.util.List;

public class ExcessPvPriorities implements Parcelable {
    public static final Creator<ExcessPvPriorities> CREATOR = new C14121();
    @C0631a
    @C0633c(a = "devices")
    private ArrayList<DevicePriority> devices = null;
    @C0631a
    @C0633c(a = "maxSolarDurations")
    private List<Integer> maxSolarDurations = null;
    @C0631a
    @C0633c(a = "mode")
    private String mode;

    static class C14121 implements Creator<ExcessPvPriorities> {
        C14121() {
        }

        public ExcessPvPriorities createFromParcel(Parcel parcel) {
            return new ExcessPvPriorities(parcel);
        }

        public ExcessPvPriorities[] newArray(int i) {
            return new ExcessPvPriorities[i];
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getMode() {
        return this.mode;
    }

    public void setMode(String str) {
        this.mode = str;
    }

    public List<Integer> getMaxSolarDurations() {
        return this.maxSolarDurations;
    }

    public void setMaxSolarDurations(List<Integer> list) {
        this.maxSolarDurations = list;
    }

    public ArrayList<DevicePriority> getDevices() {
        return this.devices;
    }

    public void setDevices(ArrayList<DevicePriority> arrayList) {
        this.devices = arrayList;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mode);
        parcel.writeList(this.maxSolarDurations);
        parcel.writeList(this.devices);
    }

    protected ExcessPvPriorities(Parcel parcel) {
        this.mode = parcel.readString();
        this.maxSolarDurations = new ArrayList();
        parcel.readList(this.maxSolarDurations, Integer.class.getClassLoader());
        this.devices = new ArrayList();
        parcel.readList(this.devices, DevicePriority.class.getClassLoader());
    }
}
