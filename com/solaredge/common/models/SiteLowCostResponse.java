package com.solaredge.common.models;

import com.google.p040a.p041a.C0631a;
import com.google.p040a.p041a.C0633c;

public class SiteLowCostResponse {
    @C0631a
    @C0633c(a = "layout")
    private LowCostLayout layout;
    @C0631a
    @C0633c(a = "powerView")
    private LowCostPowerView powerView;

    public LowCostPowerView getLowCostPowerView() {
        return this.powerView;
    }

    public LowCostLayout getLowCostLayout() {
        return this.layout;
    }
}
