package com.solaredge.common.p117h;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.solaredge.common.models.map.SMI;
import com.solaredge.common.p114e.C1393e;
import com.solaredge.common.p116g.C1401d.C1400a;

/* compiled from: SmiView */
public class C2440f extends C2190a {
    private GestureDetectorCompat f6346a;
    private C1400a f6347b;
    private long f6348c;
    private boolean f6349d;
    private boolean f6350e;
    private boolean f6351f;
    private int f6352g;
    private int f6353h;
    private int f6354i;
    private boolean f6355j;
    private Paint f6356k;
    private Paint f6357l;
    private Paint f6358m;
    private Paint f6359n;
    private Paint f6360o;
    private int f6361p;
    private int f6362q;

    public int getViewType() {
        return 1;
    }

    public void onDraw(Canvas canvas) {
        if (!this.f6355j) {
            if (TextUtils.isEmpty(getSmi().getSerialNumber())) {
                this.f6356k.setColor(this.f6361p);
            } else {
                this.f6356k.setColor(this.f6362q);
            }
        }
        if (this.f6351f) {
            canvas.drawRect((float) (-this.f6354i), (float) (-this.f6354i), (float) (getWidth() + this.f6354i), (float) (getHeight() + this.f6354i), this.f6359n);
        } else if (this.f6350e) {
            canvas.drawRect((float) (-this.f6354i), (float) (-this.f6354i), (float) (getWidth() + this.f6354i), (float) (getHeight() + this.f6354i), this.f6358m);
        } else {
            canvas.drawRect((float) (-this.f6354i), (float) (-this.f6354i), (float) (getWidth() + this.f6354i), (float) (getHeight() + this.f6354i), this.f6360o);
        }
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.f6357l);
        canvas.drawRect((float) this.f6352g, (float) this.f6352g, (float) (getWidth() - this.f6352g), (float) (getHeight() - this.f6353h), this.f6356k);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f6355j) {
            return super.onTouchEvent(motionEvent);
        }
        boolean onTouchEvent = this.f6346a.onTouchEvent(motionEvent);
        if (this.f6349d && MotionEventCompat.getActionMasked(motionEvent) == 1) {
            this.f6349d = false;
            return false;
        } else if (!onTouchEvent || this.f6349d == null) {
            return onTouchEvent ^ 1;
        } else {
            return true;
        }
    }

    public SMI getSmi() {
        return C1393e.m3801a().m3815d(this.f6348c);
    }

    public void setSMI(long j) {
        this.f6348c = j;
    }

    public void setGestureCallback(C1400a c1400a) {
        this.f6347b = c1400a;
    }

    public void setItemSelected(boolean z) {
        this.f6350e = z;
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public String getSerialNumber() {
        return getSmi() != null ? getSmi().getSerialNumber() : null;
    }

    public void setIsDoubleTapSelected(boolean z) {
        this.f6351f = z;
    }
}
