package com.solaredge.common.p117h;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.ViewCompat;
import android.view.View;
import com.solaredge.common.models.map.Group;
import com.solaredge.common.models.map.Group.ModuleOrientation;
import com.solaredge.common.models.map.Module;
import com.solaredge.common.p114e.C1393e;

/* compiled from: ModuleView */
public class C2191d extends View implements C1405b {
    private long f5520a;
    private long f5521b;
    private int f5522c;
    private int f5523d;
    private float f5524e;
    private float f5525f;
    private int f5526g;
    private boolean f5527h;
    private float f5528i;
    private Paint f5529j;
    private Paint f5530k;
    private Paint f5531l;
    private Paint f5532m;
    private int f5533n;
    private int f5534o;
    private int f5535p;
    private int f5536q;
    private boolean f5537r;
    private C2192e f5538s;

    public int getViewType() {
        return 3;
    }

    public void setHasSerial(boolean z) {
        if (z) {
            this.f5529j.setColor(this.f5535p);
            this.f5530k.setColor(this.f5536q);
            return;
        }
        this.f5529j.setColor(this.f5533n);
        this.f5530k.setColor(this.f5534o);
    }

    public void onDraw(Canvas canvas) {
        if (this.f5527h) {
            int i;
            float f;
            if (r0.f5537r) {
                canvas.drawRect((float) (-r0.f5526g), (float) (-r0.f5526g), (float) (getWidth() + r0.f5526g), (float) (getHeight() + r0.f5526g), r0.f5531l);
            } else {
                canvas.drawRect((float) (-r0.f5526g), (float) (-r0.f5526g), (float) (getWidth() + r0.f5526g), (float) (getHeight() + r0.f5526g), r0.f5532m);
            }
            canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), r0.f5529j);
            canvas.drawRect(0.0f, 0.0f, (float) getWidth(), r0.f5524e, r0.f5530k);
            canvas.drawRect(((float) getWidth()) - r0.f5524e, r0.f5524e, (float) getWidth(), (float) getHeight(), r0.f5530k);
            canvas.drawRect(0.0f, ((float) getHeight()) - r0.f5524e, ((float) getWidth()) - r0.f5524e, (float) getHeight(), r0.f5530k);
            canvas.drawRect(0.0f, r0.f5524e, r0.f5524e, ((float) getHeight()) - r0.f5524e, r0.f5530k);
            int i2 = 0;
            if (!r0.f5527h) {
                if (getModuleGroup().getModuleOrientation() != ModuleOrientation.horizontal) {
                    r0.f5528i = ((((float) getHeight()) - (r0.f5524e * 2.0f)) - (5.0f * r0.f5525f)) / 6.0f;
                    i = 0;
                    while (i < 5) {
                        int i3 = i + 1;
                        f = (float) i3;
                        canvas.drawRect(r0.f5524e, (r0.f5524e + (r0.f5528i * f)) + (r0.f5525f * ((float) i)), ((float) getWidth()) - r0.f5524e, (r0.f5524e + (r0.f5528i * f)) + (r0.f5525f * f), r0.f5530k);
                        i = i3;
                    }
                    while (i2 < 3) {
                        r0.f5528i = ((((float) getWidth()) - (r0.f5524e * 2.0f)) - (r0.f5525f * 3.0f)) / 4.0f;
                        int i4 = i2 + 1;
                        float f2 = (float) i4;
                        float f3 = (float) i2;
                        canvas.drawRect((r0.f5524e + (r0.f5528i * f2)) + (r0.f5525f * f3), r0.f5524e, ((r0.f5524e + (r0.f5528i * f2)) + r0.f5525f) + (r0.f5525f * f3), ((float) getHeight()) - r0.f5524e, r0.f5530k);
                        i2 = i4;
                    }
                }
            }
            i = 0;
            while (i < 2) {
                r0.f5528i = ((((float) getHeight()) - (r0.f5524e * 2.0f)) - (r0.f5525f * 2.0f)) / 3.0f;
                int i5 = i + 1;
                float f4 = (float) i5;
                Canvas canvas2 = canvas;
                canvas2.drawRect(r0.f5524e, (r0.f5524e + (r0.f5528i * f4)) + (r0.f5525f * ((float) i)), ((float) getWidth()) - r0.f5524e, (r0.f5524e + (r0.f5528i * f4)) + (r0.f5525f * f4), r0.f5530k);
                i = i5;
            }
            while (i2 < 5) {
                r0.f5528i = ((((float) getWidth()) - (r0.f5524e * 2.0f)) - (r0.f5525f * 5.0f)) / 6.0f;
                i2++;
                f = (float) i2;
                canvas.drawRect((r0.f5524e + (r0.f5528i * f)) + (r0.f5525f * f), r0.f5524e, ((r0.f5524e + (r0.f5528i * f)) + r0.f5525f) + (r0.f5525f * f), ((float) getHeight()) - r0.f5524e, r0.f5530k);
            }
        }
        super.onDraw(canvas);
    }

    public Module getModule() {
        if (C1393e.m3801a().m3805a(this.f5521b) == null || C1393e.m3801a().m3805a(this.f5521b).getModules() == null) {
            return null;
        }
        Module module = (Module) C1393e.m3801a().m3805a(this.f5521b).getModules().get(Long.valueOf(this.f5520a));
        if (module == null) {
            module = new Module();
            boolean z = getVisibility() == 0 && getAlpha() == 1.0f;
            module.setEnabled(z);
            module.setRow(this.f5522c);
            module.setColumn(this.f5523d);
        }
        return module;
    }

    public void setModule(long j) {
        this.f5520a = j;
    }

    public Group getModuleGroup() {
        return C1393e.m3801a().m3805a(this.f5521b);
    }

    public void setModuleGroup(long j) {
        this.f5521b = j;
    }

    public void setItemSelected(boolean z) {
        this.f5537r = z;
        Module module = getModule();
        if (getModule() != null) {
            module.setIsSelected(z);
        }
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public String getSerialNumber() {
        return getModule() != null ? getModule().getSerialNumber() : null;
    }

    public int getRow() {
        return this.f5522c;
    }

    public void setRow(int i) {
        this.f5522c = i;
    }

    public int getColumn() {
        return this.f5523d;
    }

    public void setColumn(int i) {
        this.f5523d = i;
    }

    public void setPanelGroupLayout(C2192e c2192e) {
        this.f5538s = c2192e;
    }
}
