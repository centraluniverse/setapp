package com.solaredge.common.p117h;

import android.annotation.SuppressLint;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View.MeasureSpec;
import android.widget.GridLayout;
import com.solaredge.common.models.map.Group;
import com.solaredge.common.models.map.Group.ModuleOrientation;
import com.solaredge.common.models.map.Module;
import com.solaredge.common.p114e.C1393e;
import com.solaredge.common.p115f.C2188a;
import com.solaredge.common.p116g.C1401d.C1400a;

/* compiled from: PanelGroupLayout */
public class C2192e extends GridLayout implements C1405b {
    private C2188a f5539a;
    private long f5540b;
    private GestureDetectorCompat f5541c;
    private C1400a f5542d;
    private boolean f5543e;
    private int f5544f;
    private int f5545g;
    private boolean f5546h;
    private Paint f5547i;
    private Paint f5548j;
    private float[] f5549k;
    private boolean f5550l;
    private Paint f5551m;
    private Paint f5552n;
    private Paint f5553o;
    private int f5554p;
    private int f5555q;
    private int f5556r;
    private int f5557s;
    private boolean f5558t;

    public String getSerialNumber() {
        return null;
    }

    public int getViewType() {
        return 2;
    }

    protected void onAttachedToWindow() {
        this.f5542d = (C1400a) getParent();
    }

    public Group getGroup() {
        return C1393e.m3801a().m3805a(this.f5540b);
    }

    public void setGroup(long j) {
        this.f5540b = j;
    }

    @SuppressLint({"NewApi"})
    protected void onDraw(Canvas canvas) {
        int i = 0;
        this.f5550l = false;
        Paint paint = this.f5548j;
        if (!(this.f5550l || r0.f5540b == 0 || !m6630a() || getGroup() == null)) {
            int rows = getGroup().getRows();
            int columns = getGroup().getColumns();
            ModuleOrientation moduleOrientation = getGroup().getModuleOrientation();
            int width = getWidth() / columns;
            int height = getHeight() / rows;
            int i2 = 2;
            if (r0.f5546h) {
                canvas.drawRect((float) ((-r0.f5544f) - 2), (float) ((-r0.f5544f) - 2), (float) ((getWidth() + r0.f5544f) + 2), (float) ((getHeight() + r0.f5544f) + 2), r0.f5547i);
            } else {
                canvas.drawRect((float) (-r0.f5544f), (float) (-r0.f5544f), (float) (getWidth() + r0.f5544f), (float) (getHeight() + r0.f5544f), paint);
            }
            int i3 = 0;
            while (i3 < rows) {
                int i4;
                int i5 = i;
                while (i5 < columns) {
                    Module moduleAt = getGroup().getModuleAt(i3, i5);
                    if (!moduleAt.isEnabled()) {
                        if (r0.f5558t) {
                        }
                        i4 = i2;
                        i5++;
                        i2 = i4;
                        i = 0;
                    }
                    if (moduleAt.getSerialNumber() == null) {
                        r0.f5551m.setColor(r0.f5554p);
                        r0.f5552n.setColor(r0.f5555q);
                    } else {
                        r0.f5551m.setColor(r0.f5556r);
                        r0.f5552n.setColor(r0.f5557s);
                    }
                    if (!moduleAt.isEnabled()) {
                        r0.f5551m.setAlpha(80);
                    }
                    int i6;
                    if (moduleAt.isSelected()) {
                        r0.f5544f = i;
                        int i7 = width * i5;
                        i6 = height * i3;
                        canvas.drawRect((float) ((i7 + i5) + 3), (float) ((i6 + i3) + 3), (float) (((i7 + width) + i5) - 3), (float) (((i6 + height) + i3) - 3), r0.f5553o);
                    } else {
                        i6 = width * i5;
                        int i8 = height * i3;
                        float f = (float) (((r0.f5545g + height) + i8) + i3);
                        Canvas canvas2 = canvas;
                        canvas2.drawRect((float) (((-r0.f5545g) + i6) + i5), (float) (((-r0.f5545g) + i8) + i3), (float) (((r0.f5545g + width) + i6) + i5), f, r0.f5548j);
                        float f2 = (float) ((i6 + i5) + 3);
                        float f3 = (float) ((i8 + i3) + 3);
                        float f4 = (float) (((i6 + width) + i5) - 3);
                        float f5 = (float) (((i8 + height) + i3) - 3);
                        canvas2.drawRect(f2, f3, f4, f5, r0.f5551m);
                        if (r0.f5539a.getScale() > 1.0f) {
                            canvas.drawRect(f2, f3, f4, f5, r0.f5552n);
                            float f6;
                            if (moduleOrientation == ModuleOrientation.horizontal) {
                                i4 = 0;
                                while (i4 < i2) {
                                    i4++;
                                    f = f3 + (((float) ((height - 6) / 3)) * ((float) i4));
                                    canvas.drawRect(f2, f, f4, f, r0.f5552n);
                                    i2 = 2;
                                }
                                i6 = 0;
                                while (i6 < 5) {
                                    i6++;
                                    f6 = f2 + (((float) ((width - 6) / 6)) * ((float) i6));
                                    canvas.drawRect(f6, f3, f6, f5, r0.f5552n);
                                }
                                i4 = 2;
                            } else {
                                float f7 = (float) ((height - 6) / 6);
                                int i9 = 0;
                                while (i9 < 5) {
                                    int i10 = i9 + 1;
                                    f = f3 + (((float) i10) * f7);
                                    int i11 = i10;
                                    canvas.drawRect(f2, f, f4, f, r0.f5552n);
                                    i9 = i11;
                                }
                                i6 = 0;
                                i4 = 2;
                                while (i6 < 2) {
                                    i6++;
                                    f6 = f2 + (((float) ((width - 6) / 3)) * ((float) i6));
                                    canvas.drawRect(f6, f3, f6, f5, r0.f5552n);
                                }
                            }
                            i5++;
                            i2 = i4;
                            i = 0;
                        }
                    }
                    i4 = i2;
                    i5++;
                    i2 = i4;
                    i = 0;
                }
                i4 = i2;
                i3++;
                i = 0;
            }
        }
        super.onDraw(canvas);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = this.f5541c.onTouchEvent(motionEvent);
        if (this.f5543e && MotionEventCompat.getActionMasked(motionEvent) == 1) {
            this.f5543e = false;
            return false;
        } else if (!onTouchEvent || this.f5543e == null) {
            return onTouchEvent ^ 1;
        } else {
            return true;
        }
    }

    protected void onMeasure(int i, int i2) {
        Group group = getGroup();
        int columns = (int) ((((double) group.getColumns()) * group.getModuleWidth()) + (((double) (group.getColumns() - 1)) * group.getHorizontalSpacing()));
        int rows = (int) ((((double) group.getRows()) * group.getModuleHeight()) + (((double) (group.getRows() - 1)) * group.getVerticalSpacing()));
        int mode = MeasureSpec.getMode(i);
        i = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        measureChildren(MeasureSpec.makeMeasureSpec(i, mode), MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(i2), mode2));
        setMeasuredDimension(columns, rows);
    }

    public void setGestureCallback(C1400a c1400a) {
        this.f5542d = c1400a;
    }

    public Matrix getViewMatrix() {
        Matrix matrix = new Matrix();
        Camera camera = new Camera();
        float width = (float) getWidth();
        float height = (float) getHeight();
        float pivotX = getPivotX();
        float pivotY = getPivotY();
        float rotationX = getRotationX();
        float rotationY = getRotationY();
        float rotation = getRotation();
        if (!(rotationX == 0.0f && rotationY == 0.0f && rotation == 0.0f)) {
            camera.save();
            camera.rotateX(rotationX);
            camera.rotateY(rotationY);
            camera.rotateZ(-rotation);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-pivotX, -pivotY);
            matrix.postTranslate(pivotX, pivotY);
        }
        float scaleX = getScaleX();
        rotationX = getScaleY();
        if (!(scaleX == 1.0f && rotationX == 1.0f)) {
            matrix.postScale(scaleX, rotationX);
            matrix.postTranslate((-(pivotX / width)) * ((scaleX * width) - width), (-(pivotY / height)) * ((rotationX * height) - height));
        }
        matrix.postTranslate(getTranslationX(), getTranslationY());
        return matrix;
    }

    public PointF[] getCorners() {
        r0 = new PointF[4];
        Matrix viewMatrix = getViewMatrix();
        float[] fArr = new float[]{0.0f, 0.0f};
        viewMatrix.mapPoints(fArr);
        r0[0] = new PointF(getX() + fArr[0], getY() + fArr[1]);
        fArr[0] = (float) getWidth();
        fArr[1] = 0.0f;
        viewMatrix.mapPoints(fArr);
        r0[1] = new PointF(getX() + fArr[0], getY() + fArr[1]);
        fArr[0] = (float) getWidth();
        fArr[1] = (float) getHeight();
        viewMatrix.mapPoints(fArr);
        r0[2] = new PointF(getX() + fArr[0], getY() + fArr[1]);
        fArr[0] = 0.0f;
        fArr[1] = (float) getHeight();
        viewMatrix.mapPoints(fArr);
        r0[3] = new PointF(getX() + fArr[0], getY() + fArr[1]);
        return r0;
    }

    public void setOrientation(ModuleOrientation moduleOrientation) {
        C1393e.m3801a().m3808a(this, C1393e.m3801a().m3813c(), moduleOrientation);
    }

    public void setIsDoubleTapSelected(boolean z) {
        this.f5546h = z;
    }

    private boolean m6630a() {
        if (this.f5539a == null) {
            return true;
        }
        getMatrix().getValues(this.f5549k);
        float f = this.f5549k[2];
        float f2 = this.f5549k[5];
        if (this.f5549k[0] < 0.0f) {
            f -= (float) getWidth();
            f2 -= (float) getHeight();
        }
        return this.f5539a.m6627a(f, f2, getWidth(), getHeight());
    }

    public void setShowHidden(boolean z) {
        this.f5558t = z;
    }
}
