package com.solaredge.common.p117h;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.solaredge.common.models.map.Inverter;
import com.solaredge.common.p114e.C1393e;
import com.solaredge.common.p116g.C1401d.C1400a;

@SuppressLint({"ClickableViewAccessibility"})
/* compiled from: InverterView */
public class C2439c extends C2190a {
    private GestureDetectorCompat f6325a;
    private C1400a f6326b;
    private float f6327c;
    private float f6328d;
    private float f6329e;
    private boolean f6330f;
    private RectF f6331g;
    private RectF f6332h;
    private Paint f6333i;
    private Paint f6334j;
    private Paint f6335k;
    private Paint f6336l;
    private Paint f6337m;
    private Paint f6338n;
    private int f6339o;
    private int f6340p;
    private long f6341q;
    private boolean f6342r;
    private boolean f6343s;
    private boolean f6344t;
    private boolean f6345u;

    public int getViewType() {
        return 0;
    }

    @SuppressLint({"NewApi"})
    public void onDraw(Canvas canvas) {
        this.f6327c = (float) Math.round(((float) getWidth()) / 18.75f);
        this.f6328d = (float) Math.round(((float) getHeight()) / 4.3125f);
        if (!this.f6330f) {
            if (getInverter() != null) {
                if (!TextUtils.isEmpty(getInverter().getSerialNumber())) {
                    r0.f6333i.setColor(r0.f6340p);
                }
            }
            r0.f6333i.setColor(r0.f6339o);
        }
        r0.f6331g.set(0.0f, 0.0f, (float) getWidth(), r0.f6328d * 0.7f);
        if (r0.f6345u) {
            canvas.drawRect(-r0.f6329e, -r0.f6329e, ((float) getWidth()) + r0.f6329e, ((float) getHeight()) + r0.f6329e, r0.f6336l);
        } else if (r0.f6344t) {
            canvas.drawRect(-r0.f6329e, -r0.f6329e, ((float) getWidth()) + r0.f6329e, ((float) getHeight()) + r0.f6329e, r0.f6335k);
        } else {
            canvas.drawRect(-r0.f6329e, -r0.f6329e, ((float) getWidth()) + r0.f6329e, ((float) getHeight()) + r0.f6329e, r0.f6337m);
        }
        Canvas canvas2 = canvas;
        canvas2.drawRect(0.0f, (r0.f6328d * 0.7f) / 2.0f, (float) getWidth(), (float) getHeight(), r0.f6334j);
        canvas2.drawOval(r0.f6331g, r0.f6334j);
        Canvas canvas3 = canvas2;
        canvas3.drawRect(r0.f6327c, ((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c / 2.0f), ((float) getWidth()) - r0.f6327c, ((float) getHeight()) - r0.f6328d, r0.f6333i);
        canvas3.drawLine(r0.f6327c + (r0.f6327c * 4.0f), ((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c * 2.0f), r0.f6327c + (r0.f6327c * 7.0f), ((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c * 2.0f), r0.f6338n);
        canvas3.drawLine(r0.f6327c + (r0.f6327c * 4.0f), (((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c * 2.0f)) + r0.f6327c, r0.f6327c + (r0.f6327c * 7.0f), (((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c * 2.0f)) + r0.f6327c, r0.f6338n);
        r0.f6332h.set(r0.f6327c + (r0.f6327c * 6.25f), (((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c * 2.0f)) + (r0.f6327c * 1.5f), r0.f6327c + (r0.f6327c * 8.25f), (((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c * 2.0f)) + (r0.f6327c * 3.0f));
        canvas2.drawArc(r0.f6332h, 10.0f, 180.0f, false, r0.f6338n);
        r0.f6332h.set(r0.f6327c + (r0.f6327c * 8.25f), ((((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c * 2.0f)) + (r0.f6327c * 1.5f)) + 1.0f, r0.f6327c + (r0.f6327c * 10.25f), ((((r0.f6328d * 0.7f) / 2.0f) + (r0.f6327c * 2.0f)) + (r0.f6327c * 3.0f)) + 1.0f);
        canvas2.drawArc(r0.f6332h, 180.0f, 190.0f, false, r0.f6338n);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f6330f) {
            return super.onTouchEvent(motionEvent);
        }
        boolean onTouchEvent = this.f6325a.onTouchEvent(motionEvent);
        if (this.f6342r && MotionEventCompat.getActionMasked(motionEvent) == 1) {
            this.f6342r = false;
            return false;
        } else if (!onTouchEvent || (this.f6343s == null && this.f6342r == null)) {
            return onTouchEvent ^ 1;
        } else {
            return true;
        }
    }

    public Inverter getInverter() {
        return C1393e.m3801a().m3812b(this.f6341q);
    }

    public void setInverter(long j) {
        this.f6341q = j;
    }

    public void setGestureCallback(C1400a c1400a) {
        this.f6326b = c1400a;
    }

    public void setItemSelected(boolean z) {
        this.f6344t = z;
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public String getSerialNumber() {
        return getInverter() != null ? getInverter().getSerialNumber() : null;
    }

    public void setIsDoubleTapSelected(boolean z) {
        if (getInverter() != null && getInverter().is3rdParty()) {
            this.f6345u = z;
        }
    }
}
