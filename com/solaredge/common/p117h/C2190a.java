package com.solaredge.common.p117h;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.support.v4.view.GestureDetectorCompat;
import android.view.MotionEvent;
import android.view.View;

/* compiled from: BaseMapItem */
public abstract class C2190a extends View implements C1405b {
    private GestureDetectorCompat f5519a;

    protected void onAttachedToWindow() {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.f5519a.onTouchEvent(motionEvent) ^ 1;
    }

    public Matrix getViewMatrix() {
        Matrix matrix = new Matrix();
        Camera camera = new Camera();
        float width = (float) getWidth();
        float height = (float) getHeight();
        float pivotX = getPivotX();
        float pivotY = getPivotY();
        float rotationX = getRotationX();
        float rotationY = getRotationY();
        float rotation = getRotation();
        if (!(rotationX == 0.0f && rotationY == 0.0f && rotation == 0.0f)) {
            camera.save();
            camera.rotateX(rotationX);
            camera.rotateY(rotationY);
            camera.rotateZ(-rotation);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-pivotX, -pivotY);
            matrix.postTranslate(pivotX, pivotY);
        }
        float scaleX = getScaleX();
        rotationX = getScaleY();
        if (!(scaleX == 1.0f && rotationX == 1.0f)) {
            matrix.postScale(scaleX, rotationX);
            matrix.postTranslate((-(pivotX / width)) * ((scaleX * width) - width), (-(pivotY / height)) * ((rotationX * height) - height));
        }
        matrix.postTranslate(getTranslationX(), getTranslationY());
        return matrix;
    }

    public PointF[] getCorners() {
        r0 = new PointF[4];
        Matrix viewMatrix = getViewMatrix();
        float[] fArr = new float[]{0.0f, 0.0f};
        viewMatrix.mapPoints(fArr);
        r0[0] = new PointF(getX() + fArr[0], getY() + fArr[1]);
        fArr[0] = (float) getWidth();
        fArr[1] = 0.0f;
        viewMatrix.mapPoints(fArr);
        r0[1] = new PointF(getX() + fArr[0], getY() + fArr[1]);
        fArr[0] = (float) getWidth();
        fArr[1] = (float) getHeight();
        viewMatrix.mapPoints(fArr);
        r0[2] = new PointF(getX() + fArr[0], getY() + fArr[1]);
        fArr[0] = 0.0f;
        fArr[1] = (float) getHeight();
        viewMatrix.mapPoints(fArr);
        r0[3] = new PointF(getX() + fArr[0], getY() + fArr[1]);
        return r0;
    }
}
