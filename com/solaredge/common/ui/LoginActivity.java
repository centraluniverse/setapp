package com.solaredge.common.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.avast.android.dialogs.p038b.C2493a.C1891a;
import com.avast.android.dialogs.p039c.C0585a;
import com.crashlytics.android.beta.Beta;
import com.google.android.gms.analytics.C0697c.C1971a;
import com.google.android.gms.analytics.C0697c.C1972c;
import com.google.android.gms.analytics.C2372e;
import com.google.android.gms.auth.api.C0717a;
import com.google.android.gms.auth.api.credentials.C1976b;
import com.google.android.gms.auth.api.credentials.C2375a;
import com.google.android.gms.auth.api.credentials.C2375a.C0722a;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.Credential.C0718a;
import com.google.android.gms.common.C2396b;
import com.google.android.gms.common.api.C0812f;
import com.google.android.gms.common.api.C0812f.C0809a;
import com.google.android.gms.common.api.C0812f.C0810b;
import com.google.android.gms.common.api.C0812f.C0811c;
import com.google.android.gms.common.api.C0840k;
import com.google.android.gms.common.api.C0841l;
import com.google.android.gms.common.api.Status;
import com.solaredge.common.C1366a;
import com.solaredge.common.C1368b;
import com.solaredge.common.C1379d.C1373b;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.C1379d.C1376e;
import com.solaredge.common.C1379d.C1377f;
import com.solaredge.common.models.response.UserResponse;
import com.solaredge.common.p110a.C1361a.C1346b;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p113d.C1378a;
import com.solaredge.common.p114e.C1380a;
import com.solaredge.common.p114e.C1381b;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1384d;
import com.solaredge.common.p114e.C1394f;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p114e.C1396h;
import com.solaredge.common.p116g.C1404g;
import java.util.Locale;
import p000a.p001a.p002a.C0005c;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements C0585a, C0810b, C0811c {
    public static String f6691a = C1368b.m3756a().m3760b();
    public static String f6692b = "CUSTOM_ENVIRONMENT_IP";
    private TextView f6693A;
    private TextView f6694B;
    private OnClickListener f6695C = new C14341(this);
    private C0812f f6696D;
    private C2375a f6697E;
    private OnClickListener f6698F = new C14409(this);
    private OnClickListener f6699G = new OnClickListener(this) {
        final /* synthetic */ LoginActivity f3529a;

        {
            this.f3529a = r1;
        }

        public void onClick(View view) {
            String str = (this.f3529a.f6711m == null || view.getId() != this.f3529a.f6711m.getId()) ? (this.f3529a.f6710l == null || view.getId() != this.f3529a.f6710l.getId()) ? (this.f3529a.f6712n == null || view.getId() != this.f3529a.f6712n.getId()) ? (this.f3529a.f6694B == null || view.getId() != this.f3529a.f6694B.getId()) ? null : "https://monitoringpublic.solaredge.com/solaredge-web/p/createSelfNewInstaller" : "https://monitoringpublic.solaredge.com/solaredge-web/p/createSelfNewInstaller" : "https://monitoringpublic.solaredge.com/solaredge-web/p/home/public" : "https://monitoring.solaredge.com/solaredge-web/p/login?sendMail=true";
            if (!TextUtils.isEmpty(str)) {
                C1404g.m3841a(str, this.f3529a);
            }
        }
    };
    private Callback<UserResponse> f6700H = new Callback<UserResponse>(this) {
        final /* synthetic */ LoginActivity f5562a;

        {
            this.f5562a = r1;
        }

        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
            if (response.isSuccessful()) {
                final UserResponse userResponse = (UserResponse) response.body();
                if (userResponse != null && !TextUtils.isEmpty(userResponse.locale)) {
                    this.f5562a.getSharedPreferences("sp_user_details", 0).edit().putString("user_name", userResponse.firstName).commit();
                    C1394f a = C1394f.m3816a();
                    Context l = this.f5562a.f6724z;
                    String str = userResponse.email;
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(userResponse.firstName);
                    stringBuilder.append(" ");
                    stringBuilder.append(userResponse.lastName);
                    a.m3819a(l, str, stringBuilder.toString());
                    C1394f.m3816a().m3821b(this.f5562a.f6724z, userResponse.metrics);
                    call = call.request().m751a().toString().contains("demologin");
                    if (call != null) {
                        C1394f.m3816a().m3818a(this.f5562a.f6724z, C1394f.m3816a().m3817a(this.f5562a));
                    } else {
                        C1394f.m3816a().m3818a(this.f5562a.f6724z, userResponse.locale);
                    }
                    if (userResponse.accountRole != null) {
                        if (userResponse.accountRole.equalsIgnoreCase("LimitedAccount")) {
                            C1368b.m3756a().m3758a(Boolean.valueOf(true));
                        }
                        if (userResponse.accountRole.equalsIgnoreCase("PendingApprovalAccount")) {
                            C1368b.m3756a().m3761b(Boolean.valueOf(true));
                        }
                        this.f5562a.m8839f();
                    }
                    if (call != null) {
                        C1384d.m3793a().m3798a(C1394f.m3816a().m3817a(this.f5562a.f6724z));
                        return;
                    } else if (this.f5562a.f6696D.mo1568j() != null) {
                        C0717a.f1182g.mo1467a(this.f5562a.f6696D, new C0718a(userResponse.email).m1292a(this.f5562a.f6707i.getText().toString()).m1293a()).mo1530a(new C0841l(this) {
                            final /* synthetic */ AnonymousClass13 f5561b;

                            public void mo1571a(com.google.android.gms.common.api.C0840k r3) {
                                /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
                                /*
                                r2 = this;
                                r3 = r3.mo2473b();
                                r0 = r3.m7415d();
                                if (r0 == 0) goto L_0x0016;
                            L_0x000a:
                                r3 = com.solaredge.common.p114e.C1384d.m3793a();
                                r0 = r7;
                                r0 = r0.locale;
                                r3.m3798a(r0);
                                goto L_0x003c;
                            L_0x0016:
                                r0 = r3.m7414c();
                                if (r0 == 0) goto L_0x0031;
                            L_0x001c:
                                r0 = r2.f5561b;	 Catch:{ SendIntentException -> 0x0025 }
                                r0 = r0.f5562a;	 Catch:{ SendIntentException -> 0x0025 }
                                r1 = 5;	 Catch:{ SendIntentException -> 0x0025 }
                                r3.m7412a(r0, r1);	 Catch:{ SendIntentException -> 0x0025 }
                                goto L_0x003c;
                            L_0x0025:
                                r3 = com.solaredge.common.p114e.C1384d.m3793a();
                                r0 = r7;
                                r0 = r0.locale;
                                r3.m3798a(r0);
                                goto L_0x003c;
                            L_0x0031:
                                r3 = com.solaredge.common.p114e.C1384d.m3793a();
                                r0 = r7;
                                r0 = r0.locale;
                                r3.m3798a(r0);
                            L_0x003c:
                                return;
                                */
                                throw new UnsupportedOperationException("Method not decompiled: com.solaredge.common.ui.LoginActivity.13.1.a(com.google.android.gms.common.api.k):void");
                            }
                        });
                        return;
                    } else {
                        C1384d.m3793a().m3798a(userResponse.locale);
                        return;
                    }
                }
                return;
            }
            Log.e("Login", "mLoginCallback - onResponse");
            this.f5562a.m8856a(this.f5562a.getString(C1377f.lbl_err_wrong_credentials));
        }

        public void onFailure(Call<UserResponse> call, Throwable th) {
            th.fillInStackTrace();
            Log.e("Login", "mLoginCallback - onFailure");
            if (!C1404g.m3846a(false)) {
                if (C1404g.m3844a(th.getMessage(), false) == null) {
                    this.f5562a.m8856a(this.f5562a.getString(C1377f.lbl_err_wrong_credentials));
                    return;
                }
            }
            this.f5562a.f6715q.setVisibility(8);
        }
    };
    private boolean f6701c;
    private Typeface f6702d;
    private Typeface f6703e;
    private TextInputLayout f6704f;
    private TextInputLayout f6705g;
    private EditText f6706h;
    private EditText f6707i;
    private Button f6708j;
    private View f6709k;
    private View f6710l;
    private View f6711m;
    private View f6712n;
    private TextView f6713o;
    private ProgressBar f6714p;
    private View f6715q;
    private View f6716r;
    private C2372e f6717s;
    private String f6718t;
    private int f6719u;
    private int f6720v = 0;
    private ImageView f6721w;
    private int f6722x = 0;
    private long f6723y = 0;
    private Context f6724z = C1366a.m3749a().m3753b();

    class C14341 implements OnClickListener {
        Toast f3530a = null;
        final /* synthetic */ LoginActivity f3531b;

        C14341(LoginActivity loginActivity) {
            this.f3531b = loginActivity;
        }

        public void onClick(View view) {
            if (this.f3531b.f6706h.getText().toString().endsWith("@solaredge.com") == null || (this.f3531b.f6723y != 0 && System.currentTimeMillis() - this.f3531b.f6723y >= 1000)) {
                this.f3531b.f6723y = 0;
                this.f3531b.f6722x = 0;
                return;
            }
            this.f3531b.f6722x = this.f3531b.f6722x + 1;
            this.f3531b.f6723y = System.currentTimeMillis();
            if (this.f3531b.f6722x > 4 && this.f3531b.f6722x < 8) {
                if (this.f3530a != null) {
                    this.f3530a.cancel();
                }
                view = this.f3531b;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(8 - this.f3531b.f6722x);
                stringBuilder.append(" more clicks...");
                this.f3530a = Toast.makeText(view, stringBuilder.toString(), 0);
                this.f3530a.show();
            }
            if (this.f3531b.f6722x == 8) {
                this.f3531b.f6722x = 0;
                this.f3531b.f6723y = 0;
                view = new C1891a(this.f3531b, this.f3531b.getSupportFragmentManager());
                view.m1010a(false);
                view.m1012b(false);
                view.m5248a((CharSequence) "Please Select your Running Env.");
                view.m5251b(C1382c.m3782a().m3783a("API_Cancel"));
                view.m5249a(new String[]{"Production", "Monitoringprod", Beta.TAG, "R&D", "Test", "NT", "R&D01", "R&D02", "Custom"});
                view.m5254e();
            }
        }
    }

    class C14352 extends AnimatorListenerAdapter {
        final /* synthetic */ LoginActivity f3532a;

        public void onAnimationEnd(Animator animator) {
        }

        C14352(LoginActivity loginActivity) {
            this.f3532a = loginActivity;
        }
    }

    class C14363 extends AnimatorListenerAdapter {
        final /* synthetic */ LoginActivity f3533a;

        C14363(LoginActivity loginActivity) {
            this.f3533a = loginActivity;
        }

        public void onAnimationEnd(Animator animator) {
            this.f3533a.f6715q.setVisibility(8);
        }
    }

    class C14376 implements TextWatcher {
        final /* synthetic */ LoginActivity f3534a;

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        C14376(LoginActivity loginActivity) {
            this.f3534a = loginActivity;
        }

        public void afterTextChanged(Editable editable) {
            if (editable.length() > null) {
                this.f3534a.f6704f.setError(null);
            }
        }
    }

    class C14387 implements TextWatcher {
        final /* synthetic */ LoginActivity f3535a;

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        C14387(LoginActivity loginActivity) {
            this.f3535a = loginActivity;
        }

        public void afterTextChanged(Editable editable) {
            if (editable.length() > null) {
                this.f3535a.f6705g.setError(null);
            }
        }
    }

    class C14398 implements OnEditorActionListener {
        final /* synthetic */ LoginActivity f3536a;

        C14398(LoginActivity loginActivity) {
            this.f3536a = loginActivity;
        }

        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            this.f3536a.m8829a((boolean) 0);
            return true;
        }
    }

    class C14409 implements OnClickListener {
        final /* synthetic */ LoginActivity f3537a;

        C14409(LoginActivity loginActivity) {
            this.f3537a = loginActivity;
        }

        public void onClick(View view) {
            this.f3537a.m8829a(view.getId() == C1375d.btn_demo_account ? true : null);
        }
    }

    class C22124 implements C0841l {
        final /* synthetic */ LoginActivity f5563a;

        C22124(LoginActivity loginActivity) {
            this.f5563a = loginActivity;
        }

        public void mo1571a(C0840k c0840k) {
            c0840k.mo2473b().m7415d();
        }
    }

    class C22135 implements C0841l<C1976b> {
        final /* synthetic */ LoginActivity f5564a;

        C22135(LoginActivity loginActivity) {
            this.f5564a = loginActivity;
        }

        public void m6635a(C1976b c1976b) {
            if (c1976b.mo2473b().m7415d()) {
                this.f5564a.m8824a(c1976b.mo2472a());
            } else {
                this.f5564a.m8825a(c1976b.mo2473b());
            }
        }
    }

    private void m8825a(Status status) {
    }

    public void mo1537a(int i) {
    }

    public void mo1539a(@NonNull C2396b c2396b) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (C1368b.f3389b != null) {
            setRequestedOrientation(1);
        }
        if (C1366a.m3749a().m3754d().equalsIgnoreCase("InverterActivator") != null) {
            setContentView((int) C1376e.activity_login_activator);
        } else {
            setContentView((int) C1376e.activity_login);
        }
        this.f6717s = C1396h.m3827a().m3828b();
        C1382c.m3782a().m3789b(this.f6724z);
        String string = getPreferences(0).getString(f6691a, "https://api.solaredge.com/solaredge-apigw/api/");
        if (!string.endsWith("/api/")) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(string);
            stringBuilder.append("/api/");
            string = stringBuilder.toString();
        }
        C1365j.m3739a().f3363a = string;
        C1365j.m3739a().m3744c();
        this.f6701c = getIntent().getBooleanExtra("arg_close_on_back", false);
        this.f6719u = getResources().getInteger(17694720);
        m8843h();
        m8831b();
        m8823a();
        if (C1381b.m3777a().m3778a(getApplicationContext()).size() <= null || C1394f.m3816a().m3822b(getApplicationContext()) != null) {
            C1368b.m3756a().m3762c().mo1775b(this);
        } else {
            C1368b.m3756a().m3762c().mo1774a(this);
        }
        this.f6696D = new C0809a(this).m1889a((C0810b) this).m1886a((FragmentActivity) this, (C0811c) this).m1887a(C0717a.f1179d).m1892b();
    }

    private void m8824a(Credential credential) {
        this.f6706h.setText(credential.m7063a());
        this.f6707i.setText(credential.m7067e());
        m8829a((boolean) null);
    }

    private void m8823a() {
        SharedPreferences sharedPreferences = getSharedPreferences("AccountLimitation", 0);
        C1368b.m3756a().m3758a(Boolean.valueOf(sharedPreferences.getBoolean("limitedAccount", false)));
        C1368b.m3756a().m3761b(Boolean.valueOf(sharedPreferences.getBoolean("pendingApprovalAccount", false)));
    }

    private void m8831b() {
        for (int i = 0; i < C1368b.f3388a.size(); i++) {
            findViewById(((Integer) C1368b.f3388a.get(i)).intValue()).setVisibility(8);
        }
        this.f6706h = (EditText) findViewById(C1375d.login_username);
        this.f6704f = (TextInputLayout) findViewById(C1375d.til_username);
        this.f6705g = (TextInputLayout) findViewById(C1375d.til_password);
        this.f6707i = (EditText) findViewById(C1375d.login_password);
        this.f6708j = (Button) findViewById(C1375d.btn_login);
        this.f6709k = findViewById(C1375d.btn_demo_account);
        this.f6713o = (TextView) findViewById(C1375d.login_error);
        this.f6714p = (ProgressBar) findViewById(C1375d.login_loading);
        this.f6711m = findViewById(C1375d.forgot_password);
        this.f6710l = findViewById(C1375d.btn_public_sites);
        this.f6712n = findViewById(C1375d.installer_signup);
        this.f6715q = findViewById(C1375d.login_loading_wrapper);
        this.f6721w = (ImageView) findViewById(C1375d.login_solaredge_logo);
        this.f6716r = findViewById(C1375d.fieldsWrapper);
        m8833c();
        if (this.f6708j != null) {
            this.f6708j.setOnClickListener(this.f6698F);
        }
        if (this.f6709k != null) {
            this.f6709k.setOnClickListener(this.f6698F);
        }
        if (this.f6711m != null) {
            this.f6711m.setOnClickListener(this.f6699G);
        }
        if (this.f6710l != null) {
            this.f6710l.setOnClickListener(this.f6699G);
        }
        if (this.f6712n != null) {
            this.f6712n.setOnClickListener(this.f6699G);
        }
        this.f6706h.setText(C1394f.m3816a().m3823c(getApplicationContext()));
        this.f6706h.addTextChangedListener(new C14376(this));
        this.f6707i.addTextChangedListener(new C14387(this));
        this.f6707i.setOnEditorActionListener(new C14398(this));
        if (this.f6721w != null) {
            this.f6721w.setOnClickListener(this.f6695C);
        }
    }

    private void m8833c() {
        this.f6694B = (TextView) findViewById(C1375d.sign_up_text_view);
        if (this.f6694B != null) {
            CharSequence spannableString = new SpannableString(this.f6694B.getText());
            spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(C1373b.dark_gray_text)), 0, 11, 0);
            this.f6694B.setText(spannableString);
            this.f6694B.setOnClickListener(this.f6699G);
        }
        this.f6693A = (TextView) findViewById(C1375d.login_activator_title);
        if (this.f6693A != null) {
            this.f6693A.setOnClickListener(this.f6695C);
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getWindow().setFormat(1);
    }

    public void onBackPressed() {
        if (this.f6701c) {
            moveTaskToBack(true);
        } else {
            super.onBackPressed();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo2812a(java.lang.CharSequence r1, int r2, int r3) {
        /*
        r0 = this;
        r1 = r1.toString();
        r2 = r1.hashCode();
        switch(r2) {
            case -548483879: goto L_0x005d;
            case 2502: goto L_0x0053;
            case 80048: goto L_0x0049;
            case 2066960: goto L_0x003f;
            case 2603186: goto L_0x0035;
            case 76927665: goto L_0x002b;
            case 76927666: goto L_0x0021;
            case 1810149311: goto L_0x0017;
            case 2029746065: goto L_0x000c;
            default: goto L_0x000b;
        };
    L_0x000b:
        goto L_0x0067;
    L_0x000c:
        r2 = "Custom";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x0014:
        r1 = 8;
        goto L_0x0068;
    L_0x0017:
        r2 = "Monitoringprod";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x001f:
        r1 = 7;
        goto L_0x0068;
    L_0x0021:
        r2 = "R&D02";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x0029:
        r1 = 6;
        goto L_0x0068;
    L_0x002b:
        r2 = "R&D01";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x0033:
        r1 = 5;
        goto L_0x0068;
    L_0x0035:
        r2 = "Test";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x003d:
        r1 = 0;
        goto L_0x0068;
    L_0x003f:
        r2 = "Beta";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x0047:
        r1 = 3;
        goto L_0x0068;
    L_0x0049:
        r2 = "R&D";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x0051:
        r1 = 1;
        goto L_0x0068;
    L_0x0053:
        r2 = "NT";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x005b:
        r1 = 4;
        goto L_0x0068;
    L_0x005d:
        r2 = "Production";
        r1 = r1.equals(r2);
        if (r1 == 0) goto L_0x0067;
    L_0x0065:
        r1 = 2;
        goto L_0x0068;
    L_0x0067:
        r1 = -1;
    L_0x0068:
        switch(r1) {
            case 0: goto L_0x00af;
            case 1: goto L_0x00a6;
            case 2: goto L_0x009d;
            case 3: goto L_0x0094;
            case 4: goto L_0x008b;
            case 5: goto L_0x0082;
            case 6: goto L_0x0079;
            case 7: goto L_0x0070;
            case 8: goto L_0x006c;
            default: goto L_0x006b;
        };
    L_0x006b:
        goto L_0x00b7;
    L_0x006c:
        r0.m8837e();
        goto L_0x00b7;
    L_0x0070:
        r1 = com.solaredge.common.p110a.C1365j.m3739a();
        r2 = "https://monitoringprod.solaredge.com/solaredge-apigw/api/";
        r1.f3363a = r2;
        goto L_0x00b7;
    L_0x0079:
        r1 = com.solaredge.common.p110a.C1365j.m3739a();
        r2 = "http://rnd02-fe:8080/solaredge-apigw/api/";
        r1.f3363a = r2;
        goto L_0x00b7;
    L_0x0082:
        r1 = com.solaredge.common.p110a.C1365j.m3739a();
        r2 = "http://rnd01-fe:8080/solaredge-apigw/api/";
        r1.f3363a = r2;
        goto L_0x00b7;
    L_0x008b:
        r1 = com.solaredge.common.p110a.C1365j.m3739a();
        r2 = "https://nt1.solaredge.com/solaredge-apigw/api/";
        r1.f3363a = r2;
        goto L_0x00b7;
    L_0x0094:
        r1 = com.solaredge.common.p110a.C1365j.m3739a();
        r2 = "https://monitoringbeta.solaredge.com/solaredge-apigw/api/";
        r1.f3363a = r2;
        goto L_0x00b7;
    L_0x009d:
        r1 = com.solaredge.common.p110a.C1365j.m3739a();
        r2 = "https://api.solaredge.com/solaredge-apigw/api/";
        r1.f3363a = r2;
        goto L_0x00b7;
    L_0x00a6:
        r1 = com.solaredge.common.p110a.C1365j.m3739a();
        r2 = "https://rnd.solaredge.com/solaredge-apigw/api/";
        r1.f3363a = r2;
        goto L_0x00b7;
    L_0x00af:
        r1 = com.solaredge.common.p110a.C1365j.m3739a();
        r2 = "https://testbeta.solaredge.com/solaredge-apigw/api/";
        r1.f3363a = r2;
    L_0x00b7:
        r0.m8835d();
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.common.ui.LoginActivity.a(java.lang.CharSequence, int, int):void");
    }

    private void m8835d() {
        C1365j.m3739a().m3741a(C1365j.m3739a().f3363a);
        C1365j.m3739a().m3744c();
        Editor edit = getPreferences(0).edit();
        edit.putString(f6691a, C1365j.m3739a().f3363a);
        edit.commit();
    }

    private void m8837e() {
        CharSequence string = getPreferences(0).getString(f6692b, null);
        Builder builder = new Builder(this);
        View inflate = getLayoutInflater().inflate(C1376e.custom_environment, null);
        builder.setView(inflate);
        final EditText editText = (EditText) inflate.findViewById(C1375d.dialog_input);
        if (string != null) {
            editText.setText(string);
        }
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(C1375d.dialog_yes);
        LinearLayout linearLayout2 = (LinearLayout) inflate.findViewById(C1375d.dialog_no);
        final AlertDialog create = builder.create();
        create.setCancelable(false);
        linearLayout2.setOnClickListener(new OnClickListener(this) {
            final /* synthetic */ LoginActivity f3525b;

            public void onClick(View view) {
                create.dismiss();
            }
        });
        linearLayout.setOnClickListener(new OnClickListener(this) {
            final /* synthetic */ LoginActivity f3528c;

            public void onClick(View view) {
                String trim = editText.getText().toString().trim();
                if (C1404g.m3843a(trim)) {
                    C1365j.m3739a().f3363a = String.format("http://%s:8080/solaredge-apigw/api/", new Object[]{trim});
                    Editor edit = this.f3528c.getPreferences(0).edit();
                    edit.putString(LoginActivity.f6692b, trim);
                    edit.commit();
                    this.f3528c.m8835d();
                    create.dismiss();
                    C1395g a = C1395g.m3824a();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Custom Environment updated to: ");
                    stringBuilder.append(trim);
                    a.m3826a(stringBuilder.toString(), 1, false);
                    return;
                }
                C1395g.m3824a().m3826a("Please insert a valid IP address", 1, false);
            }
        });
        create.show();
    }

    private void m8829a(boolean z) {
        boolean z2;
        String str = "";
        String str2 = "";
        this.f6704f.setError(null);
        this.f6705g.setError(null);
        C1381b.m3777a().m3780a(true);
        C1381b.m3777a().m3781b(C1366a.m3749a().m3753b());
        if (z) {
            if (z) {
                C1394f.m3816a().m3820a(this.f6724z, true);
                this.f6717s.m7058a(new C1971a("UiAction", "ButtonPress").m5571c("DemoButton").m1236a());
            }
            z2 = true;
        } else {
            str = this.f6706h.getText().toString();
            str2 = this.f6707i.getText().toString();
            if (str.length() == 0) {
                this.f6704f.setError(getString(C1377f.lbl_err_empty_username));
                z2 = false;
            } else {
                C1394f.m3816a().m3820a(this.f6724z, false);
                z2 = true;
            }
            if (str2.length() == 0) {
                this.f6705g.setError(getString(C1377f.lbl_err_empty_password));
                z2 = false;
            } else {
                C1394f.m3816a().m3820a(this.f6724z, false);
            }
        }
        if (z2) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f6707i.getWindowToken(), 0);
            if (C1380a.m3775a().m3776a(this.f6724z)) {
                m8840g();
                C1346b d = C1365j.m3739a().m3745d();
                if (!z) {
                    z = d.m3722a(str, str2);
                } else if (C1366a.m3749a().m3754d().equalsIgnoreCase("MonitoringApplication")) {
                    z = d.m3723b(Locale.getDefault().toString());
                } else {
                    z = d.m3721a(Locale.getDefault().toString());
                }
                z.enqueue(this.f6700H);
                return;
            }
            Toast.makeText(this, C1377f.lbl_err_no_connection, 1).show();
        }
    }

    protected void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 5) {
            C1384d.m3793a().m3798a(C1394f.m3816a().m3817a(getApplicationContext()));
        }
    }

    private void m8839f() {
        Editor edit = getSharedPreferences("AccountLimitation", 0).edit();
        edit.putBoolean("limitedAccount", C1368b.m3756a().m3763d().booleanValue());
        edit.putBoolean("pendingApprovalAccount", C1368b.m3756a().m3764e().booleanValue());
        edit.commit();
    }

    private void m8840g() {
        Log.e("loginFragment", "onLoginStarted");
        this.f6715q.setVisibility(0);
        this.f6715q.setAlpha(0.0f);
        this.f6715q.animate().alpha(1.0f).setDuration((long) this.f6719u).setListener(null).start();
        if (this.f6713o.getVisibility() == 0) {
            this.f6713o.animate().alpha(0.0f).setDuration((long) this.f6719u).setListener(new C14352(this));
        }
    }

    public void onStop() {
        if (C0005c.m3a().m17b(this)) {
            C0005c.m3a().m18c(this);
        }
        super.onStop();
    }

    public void onStart() {
        super.onStart();
        if (!C0005c.m3a().m17b(this)) {
            C0005c.m3a().m15a((Object) this);
        }
    }

    public void onEvent(C1378a c1378a) {
        C0005c.m3a().m21f(c1378a);
        if (c1378a.m3774b() != null) {
            if (C0005c.m3a().m17b(this) != null) {
                C0005c.m3a().m18c(this);
            }
            C1368b.m3756a().m3762c().mo1774a(this);
            return;
        }
        this.f6720v++;
        if (this.f6720v <= 3) {
            C1384d.m3793a().m3798a(C1394f.m3816a().m3817a(this.f6724z));
        } else {
            m8856a(getString(C1377f.lbl_err_translation_sync));
        }
    }

    public void m8856a(String str) {
        this.f6718t = str;
        if (TextUtils.isEmpty(this.f6718t) == null) {
            this.f6713o.setText(this.f6718t);
            this.f6715q.animate().alpha(0.0f).setDuration((long) this.f6719u).setListener(new C14363(this));
            this.f6713o.setVisibility(0);
            this.f6713o.setAlpha(0.0f);
            this.f6713o.animate().alpha(1.0f).setDuration((long) this.f6719u).start();
            return;
        }
        this.f6713o.setText("");
        this.f6713o.setVisibility(4);
        this.f6715q.setVisibility(8);
    }

    protected void onResume() {
        super.onResume();
        this.f6717s.m7056a("Login");
        this.f6717s.m7058a(new C1972c().m1236a());
    }

    private void m8843h() {
        this.f6702d = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Light.ttf");
        this.f6703e = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Regular.ttf");
    }

    public void mo1538a(@Nullable Bundle bundle) {
        if (this.f6701c == null || this.f6706h == null || TextUtils.isEmpty(this.f6706h.getText()) != null) {
            this.f6697E = new C0722a().m1310a(true).m1311a();
            C0717a.f1182g.mo1468a(this.f6696D, this.f6697E).mo1530a(new C22135(this));
            return;
        }
        C0717a.f1182g.mo1469b(this.f6696D, new C0718a(this.f6706h.getText().toString()).m1293a()).mo1530a(new C22124(this));
    }
}
