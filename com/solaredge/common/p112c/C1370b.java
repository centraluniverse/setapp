package com.solaredge.common.p112c;

import android.content.ContentValues;

/* compiled from: Persistable */
public interface C1370b {
    ContentValues getContentValues();

    String getTableName();
}
