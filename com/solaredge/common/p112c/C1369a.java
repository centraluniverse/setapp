package com.solaredge.common.p112c;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.solaredge.common.C1366a;
import com.solaredge.common.p114e.C1394f;

/* compiled from: DBHelper */
public class C1369a extends SQLiteOpenHelper {
    private static int f3395a;
    private static String f3396b;
    private static C1369a f3397c;
    private static SQLiteDatabase f3398d;

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public static synchronized C1369a m3765a(Context context) {
        synchronized (C1369a.class) {
            C1369a.m3767c(context);
            context = f3397c;
        }
        return context;
    }

    public static synchronized SQLiteDatabase m3766b(Context context) {
        synchronized (C1369a.class) {
            C1369a.m3767c(context);
            context = f3398d;
        }
        return context;
    }

    private static void m3767c(Context context) {
        if (f3397c == null) {
            f3397c = new C1369a(context.getApplicationContext());
        }
        if (f3398d == null || (f3398d != null && f3398d.isOpen() == null)) {
            f3398d = f3397c.getWritableDatabase();
        }
    }

    public C1369a(Context context) {
        super(context, f3396b, null, f3395a);
    }

    public long m3768a(C1370b c1370b) throws SQLException {
        return f3398d.insertOrThrow(c1370b.getTableName(), null, c1370b.getContentValues());
    }

    public long m3769a(C1370b c1370b, boolean z) throws SQLException {
        if (!C1394f.m3816a().m3822b(C1366a.m3749a().m3753b()) || z) {
            return f3398d.insertOrThrow(c1370b.getTableName(), null, c1370b.getContentValues());
        }
        return -1;
    }
}
