package com.solaredge.common;

import android.content.Context;

/* compiled from: CommonInitializer */
public class C1366a {
    private static C1366a f3384a = null;
    private static boolean f3385b = false;
    private Context f3386c;
    private String f3387d;

    private C1366a() {
    }

    public static synchronized C1366a m3749a() {
        C1366a c1366a;
        synchronized (C1366a.class) {
            if (f3384a == null) {
                f3384a = new C1366a();
            }
            c1366a = f3384a;
        }
        return c1366a;
    }

    public Context m3753b() {
        return this.f3386c;
    }

    public void m3751a(Context context) {
        this.f3386c = context;
    }

    public static boolean m3750c() {
        return f3385b;
    }

    public void m3752a(String str) {
        this.f3387d = str;
    }

    public String m3754d() {
        return this.f3387d;
    }
}
