package com.solaredge.common.p115f;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.RelativeLayout;
import com.solaredge.common.p114e.C1393e;
import com.solaredge.common.p116g.C1401d.C1400a;
import com.solaredge.common.p117h.C1405b;

/* compiled from: PhysicalLayoutView */
public class C2188a extends RelativeLayout implements C1400a {
    private C1397a f5502a;
    private C1400a f5503b;
    private RectF f5504c;
    private Rect f5505d;
    private float f5506e;
    private ScaleGestureDetector f5507f;
    private GestureDetectorCompat f5508g;
    private boolean f5509h;
    private boolean f5510i;
    private boolean f5511j;
    private float[] f5512k;
    private Matrix f5513l;
    private View f5514m;
    private float f5515n;
    private float f5516o;
    private View f5517p;
    private int f5518q;

    /* compiled from: PhysicalLayoutView */
    public interface C1397a {
        void m3829a(View view, MotionEvent motionEvent);

        void m3830b(View view, MotionEvent motionEvent);
    }

    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        i = new Rect();
        getLocalVisibleRect(i);
        this.f5504c.set(0.0f, 0.0f, (float) i.width(), (float) i.height());
        ViewCompat.postInvalidateOnAnimation(this);
    }

    @SuppressLint({"NewApi"})
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.concat(this.f5513l);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        this.f5507f.onTouchEvent(motionEvent);
        boolean onTouchEvent = this.f5508g.onTouchEvent(motionEvent);
        this.f5513l.getValues(this.f5512k);
        motionEvent.offsetLocation(this.f5512k[2] * -1.0f, -1.0f * this.f5512k[5]);
        motionEvent.offsetLocation((motionEvent.getX() / this.f5512k[0]) - motionEvent.getX(), (motionEvent.getY() / this.f5512k[4]) - motionEvent.getY());
        return (motionEvent.getPointerCount() != 1 || this.f5514m == null) ? onTouchEvent : true;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        awakenScrollBars();
        this.f5513l.getValues(this.f5512k);
        boolean z = false;
        if (motionEvent.getActionMasked() == 0) {
            this.f5509h = true;
            motionEvent.offsetLocation((motionEvent.getX() * this.f5512k[0]) - motionEvent.getX(), (motionEvent.getY() * this.f5512k[4]) - motionEvent.getY());
            motionEvent.offsetLocation(this.f5512k[2], this.f5512k[5]);
        }
        if (this.f5514m != null) {
            if (motionEvent.getActionMasked() == 1) {
                C1393e.m3801a().m3806a(this.f5514m, ((C1405b) this.f5514m).getViewType());
                if (this.f5502a != null) {
                    this.f5502a.m3829a(this.f5514m, motionEvent);
                }
                this.f5514m.animate().alpha(1.0f).setDuration(100).start();
                ViewCompat.postInvalidateOnAnimation(this);
                this.f5514m = null;
            } else if (motionEvent.getActionMasked() == 2) {
                int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent, MotionEventCompat.getPointerId(motionEvent, MotionEventCompat.getActionIndex(motionEvent)));
                float x = MotionEventCompat.getX(motionEvent, findPointerIndex);
                float y = MotionEventCompat.getY(motionEvent, findPointerIndex);
                if (this.f5512k[2] > 0.0f) {
                    x -= this.f5512k[2];
                } else {
                    x += Math.abs(this.f5512k[2]);
                }
                if (this.f5512k[5] > 0.0f) {
                    y -= this.f5512k[5];
                } else {
                    y += Math.abs(this.f5512k[5]);
                }
                C1393e.m3801a().m3807a(this.f5514m, ((C1405b) this.f5514m).getViewType(), (int) (((x + ((x / this.f5512k[0]) - x)) - this.f5515n) - ((float) (this.f5514m.getWidth() / 2))), (int) (((y + ((y / this.f5512k[4]) - y)) - this.f5516o) - ((float) (this.f5514m.getHeight() / 2))));
                ViewCompat.postInvalidateOnAnimation(this);
                if (this.f5502a != null) {
                    this.f5502a.m3830b(this.f5514m, motionEvent);
                }
            }
            return true;
        }
        boolean onTouchEvent = this.f5507f.onTouchEvent(motionEvent);
        if (!(motionEvent.getActionMasked() != 0 ? this.f5508g.onTouchEvent(motionEvent) : false)) {
            if (!onTouchEvent) {
                onTouchEvent = false;
                if (this.f5509h && motionEvent.getActionMasked() == 1) {
                    this.f5509h = false;
                }
                motionEvent = super.onTouchEvent(motionEvent);
                if (onTouchEvent || motionEvent != null) {
                    z = true;
                }
                return z;
            }
        }
        onTouchEvent = true;
        this.f5509h = false;
        motionEvent = super.onTouchEvent(motionEvent);
        z = true;
        return z;
    }

    public float getScale() {
        this.f5513l.getValues(this.f5512k);
        return this.f5512k[0];
    }

    public boolean m6627a(float f, float f2, int i, int i2) {
        this.f5513l.getValues(this.f5512k);
        float f3 = this.f5512k[5];
        float f4 = this.f5512k[2];
        float f5 = this.f5512k[0];
        f3 *= -1.0f;
        float f6 = -1.0f * f4;
        RectF rectF = new RectF(f6 / f5, f3 / f5, (f6 + this.f5504c.right) / f5, (f3 + this.f5504c.bottom) / f5);
        i = (float) i;
        i2 = (float) i2;
        RectF rectF2 = new RectF((f - i) - i2, (f2 - i) - i2, (f + i) + i2, (f2 + i) + i2);
        if (rectF.right < rectF2.left || rectF.left > rectF2.right || rectF.bottom < rectF2.top || rectF.top > rectF2.bottom) {
            return false;
        }
        return true;
    }

    public void setMaxZoom(float f) {
        this.f5506e = f;
    }

    public float getMaxZoom() {
        return this.f5506e;
    }

    public float[] getMatrixValues() {
        return this.f5512k;
    }

    public C1397a getMapCallbacks() {
        return this.f5502a;
    }

    public void setMapCallbacks(C1397a c1397a) {
        this.f5502a = c1397a;
    }

    public C1400a getGestureCallbacks() {
        return this.f5503b;
    }

    public void setGestureCallbacks(C1400a c1400a) {
        this.f5503b = c1400a;
    }

    public View getLastSelectedView() {
        return this.f5517p;
    }

    public void setLastSelectedView(View view) {
        this.f5517p = view;
    }

    public int getLastSelectedViewType() {
        return this.f5518q;
    }

    public void setLastSelectedViewType(int i) {
        this.f5518q = i;
    }

    public RectF getCurrentlyVisibleRect() {
        this.f5513l.getValues(this.f5512k);
        float f = this.f5512k[5];
        float f2 = this.f5512k[2];
        float f3 = this.f5512k[0];
        if (f3 == 0.0f) {
            f3 = 0.01f;
        }
        float height = this.f5504c.height() / f3;
        float width = this.f5504c.width() / f3;
        f2 = Math.abs(Math.min(f2, 0.0f) / f3);
        f = Math.abs(Math.min(f, 0.0f) / f3);
        return new RectF(f2, f, width + f2, height + f);
    }

    public Rect getContentRect() {
        return this.f5505d;
    }

    public void setModuleInteractionEnabled(boolean z) {
        this.f5510i = z;
    }

    public void setIsInScanMode(boolean z) {
        this.f5511j = z;
    }
}
