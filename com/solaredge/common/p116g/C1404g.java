package com.solaredge.common.p116g;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.solaredge.common.C1366a;
import com.solaredge.common.C1379d.C1372a;
import com.solaredge.common.p114e.C1380a;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1395g;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/* compiled from: Utils */
public class C1404g {
    private static final AtomicInteger f3523a = new AtomicInteger(1);

    public static float m3837a(float f, Context context) {
        return f * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f);
    }

    public static boolean m3842a(Object obj, Object obj2) {
        return (!(obj == null && obj2 == null) && (obj == null || obj2 == null || obj.equals(obj2) == null)) ? null : true;
    }

    public static boolean m3845a(List<String> list, List<String> list2) {
        return (!(list == null && list2 == null) && (list == null || list2 == null || !list.containsAll(list2) || list2.containsAll(list) == null)) ? null : true;
    }

    public static void m3841a(String str, Context context) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static String m3838a(String str, String str2, Context context) {
        if (str.equalsIgnoreCase("AU")) {
            str = Arrays.asList(context.getApplicationContext().getResources().getStringArray(C1372a.austrialian_state_names));
            context = Arrays.asList(context.getApplicationContext().getResources().getStringArray(C1372a.austrialian_state_codes));
            str = str.indexOf(str2);
            if (str != -1) {
                return (String) context.get(str);
            }
        } else if (str.equalsIgnoreCase("CA")) {
            str = Arrays.asList(context.getApplicationContext().getResources().getStringArray(C1372a.canadian_state_names));
            context = Arrays.asList(context.getApplicationContext().getResources().getStringArray(C1372a.canadian_state_codes));
            str = str.indexOf(str2);
            if (str != -1) {
                return (String) context.get(str);
            }
        } else if (str.equalsIgnoreCase("US") != null) {
            str = Arrays.asList(context.getApplicationContext().getResources().getStringArray(C1372a.usa_state_names));
            context = Arrays.asList(context.getApplicationContext().getResources().getStringArray(C1372a.usa_state_codes));
            str = str.indexOf(str2);
            if (str != -1) {
                return (String) context.get(str);
            }
        }
        return str2;
    }

    public static void m3839a() {
        if (!C1404g.m3847b()) {
            C1395g.m3824a().m3825a(C1382c.m3782a().m3783a("API_Unknown_Error"), 0);
        }
    }

    public static boolean m3846a(boolean z) {
        if (C1380a.m3775a().m3776a(C1366a.m3749a().m3753b())) {
            return false;
        }
        C1395g.m3824a().m3826a(C1382c.m3782a().m3783a("API_List_No_Internet_Connection"), 0, z);
        return true;
    }

    public static boolean m3847b() {
        return C1404g.m3846a(true);
    }

    public static boolean m3844a(String str, boolean z) {
        String str2 = "Unable to resolve host";
        if (str == null || str.startsWith(str2) == null) {
            return false;
        }
        C1395g.m3824a().m3826a(C1382c.m3782a().m3788b("API_List_No_Internet_Connection"), 0, z);
        return true;
    }

    public static Boolean m3848c() {
        try {
            return Boolean.valueOf(Runtime.getRuntime().exec("ping -c 1 www.google.com").waitFor() == 0);
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.valueOf(false);
        }
    }

    public static void m3840a(android.app.Activity r4) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        if (r4 == 0) goto L_0x0064;
    L_0x0002:
        r0 = r4.isFinishing();
        if (r0 != 0) goto L_0x0064;
    L_0x0008:
        r0 = new java.lang.StringBuilder;
        r0.<init>();
        r1 = "market://details?id=";
        r0.append(r1);
        r1 = com.solaredge.common.C1366a.m3749a();
        r1 = r1.m3753b();
        r1 = r1.getPackageName();
        r0.append(r1);
        r0 = r0.toString();
        r0 = android.net.Uri.parse(r0);
        r1 = new android.content.Intent;
        r2 = "android.intent.action.VIEW";
        r1.<init>(r2, r0);
        r0 = 1208483840; // 0x48080000 float:139264.0 double:5.97070349E-315;
        r1.addFlags(r0);
        r4.startActivity(r1);	 Catch:{ ActivityNotFoundException -> 0x0039 }
        goto L_0x0064;
    L_0x0039:
        r0 = new android.content.Intent;
        r1 = "android.intent.action.VIEW";
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = "http://play.google.com/store/apps/details?id=";
        r2.append(r3);
        r3 = com.solaredge.common.C1366a.m3749a();
        r3 = r3.m3753b();
        r3 = r3.getPackageName();
        r2.append(r3);
        r2 = r2.toString();
        r2 = android.net.Uri.parse(r2);
        r0.<init>(r1, r2);
        r4.startActivity(r0);
    L_0x0064:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.common.g.g.a(android.app.Activity):void");
    }

    public static boolean m3843a(String str) {
        return Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$").matcher(str).find();
    }
}
