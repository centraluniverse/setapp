package com.solaredge.common.p116g;

import android.text.TextUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/* compiled from: DateHelper */
public class C1399c {
    public static final String[] f3514a = new String[]{"yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss.S", "yyyy-MM-dd"};

    public static GregorianCalendar m3832a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.getDefault());
        String[] strArr = f3514a;
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strArr[i], Locale.getDefault());
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                gregorianCalendar.setTimeInMillis(simpleDateFormat.parse(str).getTime());
                return gregorianCalendar;
            } catch (ParseException e) {
                C1398a.m3831a(e.getMessage());
                i++;
            }
        }
        return null;
    }

    public static GregorianCalendar m3833b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.getDefault());
        String[] strArr = f3514a;
        if (strArr.length <= 0) {
            return null;
        }
        new SimpleDateFormat(strArr[0], Locale.getDefault()).setTimeZone(TimeZone.getTimeZone("UTC"));
        gregorianCalendar.setTimeInMillis(Long.parseLong(str));
        return gregorianCalendar;
    }
}
