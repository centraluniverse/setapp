package com.solaredge.common.p116g;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.support.v7.app.AlertDialog;
import android.view.View;
import com.solaredge.common.C1366a;

/* compiled from: RatingAppHandler */
public class C1402e {
    private static C1402e f3515a;
    private boolean f3516b;
    private boolean f3517c;
    private boolean f3518d;
    private View f3519e;
    private AlertDialog f3520f;
    private boolean f3521g;
    private Context f3522h;

    public static synchronized C1402e m3834a() {
        C1402e c1402e;
        synchronized (C1402e.class) {
            if (f3515a == null) {
                f3515a = new C1402e();
            }
            c1402e = f3515a;
        }
        return c1402e;
    }

    public C1402e() {
        this.f3516b = false;
        this.f3517c = false;
        this.f3518d = false;
        this.f3519e = null;
        this.f3520f = null;
        this.f3521g = false;
        this.f3522h = null;
        this.f3522h = C1366a.m3749a().m3753b();
        this.f3516b = this.f3522h.getSharedPreferences("RATING_APP", 0).getBoolean("SKIP_DIALOG", false);
    }

    public void m3835b() {
        Editor edit = this.f3522h.getSharedPreferences("RATING_APP", 0).edit();
        edit.putInt("SUCCESS_COUNTER", 0);
        edit.commit();
    }
}
