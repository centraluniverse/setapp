package com.solaredge.common.p116g;

import com.google.p040a.C0674j;
import com.google.p040a.C0675k;
import com.google.p040a.C0676l;
import com.google.p040a.C0677p;
import java.lang.reflect.Type;
import java.util.GregorianCalendar;

/* compiled from: DateDeserializer */
public class C2189b implements C0675k<GregorianCalendar> {
    public /* synthetic */ Object mo1787a(C0676l c0676l, Type type, C0674j c0674j) throws C0677p {
        return m6629b(c0676l, type, c0674j);
    }

    public GregorianCalendar m6629b(C0676l c0676l, Type type, C0674j c0674j) throws C0677p {
        return C1399c.m3832a(c0676l.mo1446b());
    }
}
