package com.solaredge.common.p137i;

import android.app.Activity;
import com.journeyapps.barcodescanner.C1239b;
import com.journeyapps.barcodescanner.C1251d;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.solaredge.common.p111b.C1367a;

/* compiled from: CameraCaptureManager */
public class C2193a extends C1251d {
    C1367a f5559a;

    protected void mo1790a() {
    }

    public C2193a(Activity activity, DecoratedBarcodeView decoratedBarcodeView, C1367a c1367a) {
        super(activity, decoratedBarcodeView);
        this.f5559a = c1367a;
    }

    protected void mo1791a(C1239b c1239b) {
        this.f5559a.mo2828a(c1239b.m3475b());
    }
}
