package com.solaredge.apps.activator.proto.general_types;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.WireField.Label;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import java.util.List;
import p125d.C1624f;

public final class XPacket extends Message<XPacket, Builder> {
    public static final ProtoAdapter<XPacket> ADAPTER = new ProtoAdapter_XPacket();
    public static final Integer DEFAULT_CODE = Integer.valueOf(0);
    public static final Integer DEFAULT_DST = Integer.valueOf(-4);
    public static final Integer DEFAULT_ID = Integer.valueOf(0);
    public static final Integer DEFAULT_LEN = Integer.valueOf(0);
    public static final Integer DEFAULT_SRC = Integer.valueOf(-3);
    private static final long serialVersionUID = 0;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 4)
    public final Integer code;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#BYTES", label = Label.REPEATED, tag = 6)
    public final List<C1624f> data;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 2)
    public final Integer dst;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 3)
    public final Integer id;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 5)
    public final Integer len;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 1)
    public final Integer src;

    public static final class Builder extends com.squareup.wire.Message.Builder<XPacket, Builder> {
        public Integer code;
        public List<C1624f> data = Internal.newMutableList();
        public Integer dst;
        public Integer id;
        public Integer len;
        public Integer src;

        public Builder src(Integer num) {
            this.src = num;
            return this;
        }

        public Builder dst(Integer num) {
            this.dst = num;
            return this;
        }

        public Builder id(Integer num) {
            this.id = num;
            return this;
        }

        public Builder code(Integer num) {
            this.code = num;
            return this;
        }

        public Builder len(Integer num) {
            this.len = num;
            return this;
        }

        public Builder data(List<C1624f> list) {
            Internal.checkElementsNotNull((List) list);
            this.data = list;
            return this;
        }

        public XPacket build() {
            return new XPacket(this.src, this.dst, this.id, this.code, this.len, this.data, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_XPacket extends ProtoAdapter<XPacket> {
        public ProtoAdapter_XPacket() {
            super(FieldEncoding.LENGTH_DELIMITED, XPacket.class);
        }

        public int encodedSize(XPacket xPacket) {
            return (((((ProtoAdapter.UINT32.encodedSizeWithTag(1, xPacket.src) + ProtoAdapter.UINT32.encodedSizeWithTag(2, xPacket.dst)) + ProtoAdapter.UINT32.encodedSizeWithTag(3, xPacket.id)) + ProtoAdapter.UINT32.encodedSizeWithTag(4, xPacket.code)) + ProtoAdapter.UINT32.encodedSizeWithTag(5, xPacket.len)) + ProtoAdapter.BYTES.asRepeated().encodedSizeWithTag(6, xPacket.data)) + xPacket.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, XPacket xPacket) throws IOException {
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 1, xPacket.src);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 2, xPacket.dst);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 3, xPacket.id);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 4, xPacket.code);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 5, xPacket.len);
            ProtoAdapter.BYTES.asRepeated().encodeWithTag(protoWriter, 6, xPacket.data);
            protoWriter.writeBytes(xPacket.unknownFields());
        }

        public XPacket decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.src((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 2:
                            builder.dst((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 3:
                            builder.id((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 4:
                            builder.code((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 5:
                            builder.len((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 6:
                            builder.data.add(ProtoAdapter.BYTES.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public XPacket redact(XPacket xPacket) {
            xPacket = xPacket.newBuilder();
            xPacket.clearUnknownFields();
            return xPacket.build();
        }
    }

    public XPacket(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, List<C1624f> list) {
        this(num, num2, num3, num4, num5, list, C1624f.f4400b);
    }

    public XPacket(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, List<C1624f> list, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.src = num;
        this.dst = num2;
        this.id = num3;
        this.code = num4;
        this.len = num5;
        this.data = Internal.immutableCopyOf("data", (List) list);
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.src = this.src;
        builder.dst = this.dst;
        builder.id = this.id;
        builder.code = this.code;
        builder.len = this.len;
        builder.data = Internal.copyOf("data", this.data);
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof XPacket)) {
            return false;
        }
        XPacket xPacket = (XPacket) obj;
        if (!unknownFields().equals(xPacket.unknownFields()) || !Internal.equals(this.src, xPacket.src) || !Internal.equals(this.dst, xPacket.dst) || !Internal.equals(this.id, xPacket.id) || !Internal.equals(this.code, xPacket.code) || !Internal.equals(this.len, xPacket.len) || this.data.equals(xPacket.data) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((((((((unknownFields().hashCode() * 37) + (this.src != null ? this.src.hashCode() : 0)) * 37) + (this.dst != null ? this.dst.hashCode() : 0)) * 37) + (this.id != null ? this.id.hashCode() : 0)) * 37) + (this.code != null ? this.code.hashCode() : 0)) * 37;
        if (this.len != null) {
            i2 = this.len.hashCode();
        }
        i = ((i + i2) * 37) + this.data.hashCode();
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.src != null) {
            stringBuilder.append(", src=");
            stringBuilder.append(this.src);
        }
        if (this.dst != null) {
            stringBuilder.append(", dst=");
            stringBuilder.append(this.dst);
        }
        if (this.id != null) {
            stringBuilder.append(", id=");
            stringBuilder.append(this.id);
        }
        if (this.code != null) {
            stringBuilder.append(", code=");
            stringBuilder.append(this.code);
        }
        if (this.len != null) {
            stringBuilder.append(", len=");
            stringBuilder.append(this.len);
        }
        if (!this.data.isEmpty()) {
            stringBuilder.append(", data=");
            stringBuilder.append(this.data);
        }
        stringBuilder = stringBuilder.replace(0, 2, "XPacket{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
