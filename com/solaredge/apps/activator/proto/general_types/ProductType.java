package com.solaredge.apps.activator.proto.general_types;

import com.squareup.wire.EnumAdapter;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.WireEnum;

public enum ProductType implements WireEnum {
    VENUS(0),
    JUPITER(1),
    VEGA(2),
    CCG(3),
    GEMINI(4),
    SATURN(5),
    MARS(6),
    VOYAGER(7),
    VENUS3(8),
    JUMBO_JUPITER(9),
    CCG_UNUSED(11),
    FFG(12),
    ZIGBEE_GATEWAY(13);
    
    public static final ProtoAdapter<ProductType> ADAPTER = null;
    private final int value;

    private static final class ProtoAdapter_ProductType extends EnumAdapter<ProductType> {
        ProtoAdapter_ProductType() {
            super(ProductType.class);
        }

        protected ProductType fromValue(int i) {
            return ProductType.fromValue(i);
        }
    }

    static {
        ADAPTER = new ProtoAdapter_ProductType();
    }

    private ProductType(int i) {
        this.value = i;
    }

    public static ProductType fromValue(int i) {
        switch (i) {
            case 0:
                return VENUS;
            case 1:
                return JUPITER;
            case 2:
                return VEGA;
            case 3:
                return CCG;
            case 4:
                return GEMINI;
            case 5:
                return SATURN;
            case 6:
                return MARS;
            case 7:
                return VOYAGER;
            case 8:
                return VENUS3;
            case 9:
                return JUMBO_JUPITER;
            case 11:
                return CCG_UNUSED;
            case 12:
                return FFG;
            case 13:
                return ZIGBEE_GATEWAY;
            default:
                return 0;
        }
    }

    public int getValue() {
        return this.value;
    }
}
