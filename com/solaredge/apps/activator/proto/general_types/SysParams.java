package com.solaredge.apps.activator.proto.general_types;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import java.util.Map;
import p125d.C1624f;

public final class SysParams extends Message<SysParams, Builder> {
    public static final ProtoAdapter<SysParams> ADAPTER = new ProtoAdapter_SysParams();
    private static final long serialVersionUID = 0;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#FLOAT", keyAdapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 2)
    public final Map<Integer, Float> float_params;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", keyAdapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 3)
    public final Map<Integer, String> string_params;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", keyAdapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 1)
    public final Map<Integer, Integer> uint32_params;

    public static final class Builder extends com.squareup.wire.Message.Builder<SysParams, Builder> {
        public Map<Integer, Float> float_params = Internal.newMutableMap();
        public Map<Integer, String> string_params = Internal.newMutableMap();
        public Map<Integer, Integer> uint32_params = Internal.newMutableMap();

        public Builder uint32_params(Map<Integer, Integer> map) {
            Internal.checkElementsNotNull((Map) map);
            this.uint32_params = map;
            return this;
        }

        public Builder float_params(Map<Integer, Float> map) {
            Internal.checkElementsNotNull((Map) map);
            this.float_params = map;
            return this;
        }

        public Builder string_params(Map<Integer, String> map) {
            Internal.checkElementsNotNull((Map) map);
            this.string_params = map;
            return this;
        }

        public SysParams build() {
            return new SysParams(this.uint32_params, this.float_params, this.string_params, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_SysParams extends ProtoAdapter<SysParams> {
        private final ProtoAdapter<Map<Integer, Float>> float_params = ProtoAdapter.newMapAdapter(ProtoAdapter.UINT32, ProtoAdapter.FLOAT);
        private final ProtoAdapter<Map<Integer, String>> string_params = ProtoAdapter.newMapAdapter(ProtoAdapter.UINT32, ProtoAdapter.STRING);
        private final ProtoAdapter<Map<Integer, Integer>> uint32_params = ProtoAdapter.newMapAdapter(ProtoAdapter.UINT32, ProtoAdapter.UINT32);

        public ProtoAdapter_SysParams() {
            super(FieldEncoding.LENGTH_DELIMITED, SysParams.class);
        }

        public int encodedSize(SysParams sysParams) {
            return ((this.uint32_params.encodedSizeWithTag(1, sysParams.uint32_params) + this.float_params.encodedSizeWithTag(2, sysParams.float_params)) + this.string_params.encodedSizeWithTag(3, sysParams.string_params)) + sysParams.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, SysParams sysParams) throws IOException {
            this.uint32_params.encodeWithTag(protoWriter, 1, sysParams.uint32_params);
            this.float_params.encodeWithTag(protoWriter, 2, sysParams.float_params);
            this.string_params.encodeWithTag(protoWriter, 3, sysParams.string_params);
            protoWriter.writeBytes(sysParams.unknownFields());
        }

        public SysParams decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.uint32_params.putAll((Map) this.uint32_params.decode(protoReader));
                            break;
                        case 2:
                            builder.float_params.putAll((Map) this.float_params.decode(protoReader));
                            break;
                        case 3:
                            builder.string_params.putAll((Map) this.string_params.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public SysParams redact(SysParams sysParams) {
            sysParams = sysParams.newBuilder();
            sysParams.clearUnknownFields();
            return sysParams.build();
        }
    }

    public SysParams(Map<Integer, Integer> map, Map<Integer, Float> map2, Map<Integer, String> map3) {
        this(map, map2, map3, C1624f.f4400b);
    }

    public SysParams(Map<Integer, Integer> map, Map<Integer, Float> map2, Map<Integer, String> map3, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.uint32_params = Internal.immutableCopyOf("uint32_params", (Map) map);
        this.float_params = Internal.immutableCopyOf("float_params", (Map) map2);
        this.string_params = Internal.immutableCopyOf("string_params", (Map) map3);
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.uint32_params = Internal.copyOf("uint32_params", this.uint32_params);
        builder.float_params = Internal.copyOf("float_params", this.float_params);
        builder.string_params = Internal.copyOf("string_params", this.string_params);
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SysParams)) {
            return false;
        }
        SysParams sysParams = (SysParams) obj;
        if (!unknownFields().equals(sysParams.unknownFields()) || !this.uint32_params.equals(sysParams.uint32_params) || !this.float_params.equals(sysParams.float_params) || this.string_params.equals(sysParams.string_params) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        i = (((((unknownFields().hashCode() * 37) + this.uint32_params.hashCode()) * 37) + this.float_params.hashCode()) * 37) + this.string_params.hashCode();
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (!this.uint32_params.isEmpty()) {
            stringBuilder.append(", uint32_params=");
            stringBuilder.append(this.uint32_params);
        }
        if (!this.float_params.isEmpty()) {
            stringBuilder.append(", float_params=");
            stringBuilder.append(this.float_params);
        }
        if (!this.string_params.isEmpty()) {
            stringBuilder.append(", string_params=");
            stringBuilder.append(this.string_params);
        }
        stringBuilder = stringBuilder.replace(0, 2, "SysParams{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
