package com.solaredge.apps.activator.proto.general_types;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import p125d.C1624f;

public final class Version extends Message<Version, Builder> {
    public static final ProtoAdapter<Version> ADAPTER = new ProtoAdapter_Version();
    public static final Integer DEFAULT_BUILD = Integer.valueOf(0);
    public static final Integer DEFAULT_MAJOR = Integer.valueOf(0);
    public static final Integer DEFAULT_MINOR = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 3)
    public final Integer build;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 1)
    public final Integer major;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 2)
    public final Integer minor;

    public static final class Builder extends com.squareup.wire.Message.Builder<Version, Builder> {
        public Integer build;
        public Integer major;
        public Integer minor;

        public Builder major(Integer num) {
            this.major = num;
            return this;
        }

        public Builder minor(Integer num) {
            this.minor = num;
            return this;
        }

        public Builder build(Integer num) {
            this.build = num;
            return this;
        }

        public Version build() {
            return new Version(this.major, this.minor, this.build, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_Version extends ProtoAdapter<Version> {
        public ProtoAdapter_Version() {
            super(FieldEncoding.LENGTH_DELIMITED, Version.class);
        }

        public int encodedSize(Version version) {
            return ((ProtoAdapter.UINT32.encodedSizeWithTag(1, version.major) + ProtoAdapter.UINT32.encodedSizeWithTag(2, version.minor)) + ProtoAdapter.UINT32.encodedSizeWithTag(3, version.build)) + version.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, Version version) throws IOException {
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 1, version.major);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 2, version.minor);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 3, version.build);
            protoWriter.writeBytes(version.unknownFields());
        }

        public Version decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.major((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 2:
                            builder.minor((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 3:
                            builder.build((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public Version redact(Version version) {
            version = version.newBuilder();
            version.clearUnknownFields();
            return version.build();
        }
    }

    public Version(Integer num, Integer num2, Integer num3) {
        this(num, num2, num3, C1624f.f4400b);
    }

    public Version(Integer num, Integer num2, Integer num3, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.major = num;
        this.minor = num2;
        this.build = num3;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.major = this.major;
        builder.minor = this.minor;
        builder.build = this.build;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Version)) {
            return false;
        }
        Version version = (Version) obj;
        if (!unknownFields().equals(version.unknownFields()) || !Internal.equals(this.major, version.major) || !Internal.equals(this.minor, version.minor) || Internal.equals(this.build, version.build) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((((unknownFields().hashCode() * 37) + (this.major != null ? this.major.hashCode() : 0)) * 37) + (this.minor != null ? this.minor.hashCode() : 0)) * 37;
        if (this.build != null) {
            i2 = this.build.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.major != null) {
            stringBuilder.append(", major=");
            stringBuilder.append(this.major);
        }
        if (this.minor != null) {
            stringBuilder.append(", minor=");
            stringBuilder.append(this.minor);
        }
        if (this.build != null) {
            stringBuilder.append(", build=");
            stringBuilder.append(this.build);
        }
        stringBuilder = stringBuilder.replace(0, 2, "Version{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
