package com.solaredge.apps.activator.proto.general_types;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import p125d.C1624f;

public final class Battery extends Message<Battery, Builder> {
    public static final ProtoAdapter<Battery> ADAPTER = new ProtoAdapter_Battery();
    public static final String DEFAULT_HWID = "";
    public static final String DEFAULT_MANUFACTURER = "";
    public static final String DEFAULT_SERIAL_NUMBER = "";
    public static final String DEFAULT_VERSION = "";
    private static final long serialVersionUID = 0;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 2)
    public final String hwid;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 4)
    public final String manufacturer;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 1)
    public final String serial_number;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 3)
    public final String version;

    public static final class Builder extends com.squareup.wire.Message.Builder<Battery, Builder> {
        public String hwid;
        public String manufacturer;
        public String serial_number;
        public String version;

        public Builder serial_number(String str) {
            this.serial_number = str;
            return this;
        }

        public Builder hwid(String str) {
            this.hwid = str;
            return this;
        }

        public Builder version(String str) {
            this.version = str;
            return this;
        }

        public Builder manufacturer(String str) {
            this.manufacturer = str;
            return this;
        }

        public Battery build() {
            return new Battery(this.serial_number, this.hwid, this.version, this.manufacturer, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_Battery extends ProtoAdapter<Battery> {
        public ProtoAdapter_Battery() {
            super(FieldEncoding.LENGTH_DELIMITED, Battery.class);
        }

        public int encodedSize(Battery battery) {
            return (((ProtoAdapter.STRING.encodedSizeWithTag(1, battery.serial_number) + ProtoAdapter.STRING.encodedSizeWithTag(2, battery.hwid)) + ProtoAdapter.STRING.encodedSizeWithTag(3, battery.version)) + ProtoAdapter.STRING.encodedSizeWithTag(4, battery.manufacturer)) + battery.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, Battery battery) throws IOException {
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 1, battery.serial_number);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 2, battery.hwid);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 3, battery.version);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 4, battery.manufacturer);
            protoWriter.writeBytes(battery.unknownFields());
        }

        public Battery decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.serial_number((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        case 2:
                            builder.hwid((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        case 3:
                            builder.version((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        case 4:
                            builder.manufacturer((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public Battery redact(Battery battery) {
            battery = battery.newBuilder();
            battery.clearUnknownFields();
            return battery.build();
        }
    }

    public Battery(String str, String str2, String str3, String str4) {
        this(str, str2, str3, str4, C1624f.f4400b);
    }

    public Battery(String str, String str2, String str3, String str4, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.serial_number = str;
        this.hwid = str2;
        this.version = str3;
        this.manufacturer = str4;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.serial_number = this.serial_number;
        builder.hwid = this.hwid;
        builder.version = this.version;
        builder.manufacturer = this.manufacturer;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Battery)) {
            return false;
        }
        Battery battery = (Battery) obj;
        if (!unknownFields().equals(battery.unknownFields()) || !Internal.equals(this.serial_number, battery.serial_number) || !Internal.equals(this.hwid, battery.hwid) || !Internal.equals(this.version, battery.version) || Internal.equals(this.manufacturer, battery.manufacturer) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((((((unknownFields().hashCode() * 37) + (this.serial_number != null ? this.serial_number.hashCode() : 0)) * 37) + (this.hwid != null ? this.hwid.hashCode() : 0)) * 37) + (this.version != null ? this.version.hashCode() : 0)) * 37;
        if (this.manufacturer != null) {
            i2 = this.manufacturer.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.serial_number != null) {
            stringBuilder.append(", serial_number=");
            stringBuilder.append(this.serial_number);
        }
        if (this.hwid != null) {
            stringBuilder.append(", hwid=");
            stringBuilder.append(this.hwid);
        }
        if (this.version != null) {
            stringBuilder.append(", version=");
            stringBuilder.append(this.version);
        }
        if (this.manufacturer != null) {
            stringBuilder.append(", manufacturer=");
            stringBuilder.append(this.manufacturer);
        }
        stringBuilder = stringBuilder.replace(0, 2, "Battery{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
