package com.solaredge.apps.activator.proto.general_types;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoAdapter.EnumConstantNotFoundException;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import p125d.C1624f;

public final class Controller extends Message<Controller, Builder> {
    public static final ProtoAdapter<Controller> ADAPTER = new ProtoAdapter_Controller();
    public static final ControllerType DEFAULT_CONTROLLER_TYPE = ControllerType.PORTIA;
    public static final Integer DEFAULT_SW_TYPE = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @WireField(adapter = "general_types.ControllerType#ADAPTER", tag = 1)
    public final ControllerType controller_type;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 2)
    public final Integer sw_type;
    @WireField(adapter = "general_types.Version#ADAPTER", tag = 3)
    public final Version version;

    public static final class Builder extends com.squareup.wire.Message.Builder<Controller, Builder> {
        public ControllerType controller_type;
        public Integer sw_type;
        public Version version;

        public Builder controller_type(ControllerType controllerType) {
            this.controller_type = controllerType;
            return this;
        }

        public Builder sw_type(Integer num) {
            this.sw_type = num;
            return this;
        }

        public Builder version(Version version) {
            this.version = version;
            return this;
        }

        public Controller build() {
            return new Controller(this.controller_type, this.sw_type, this.version, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_Controller extends ProtoAdapter<Controller> {
        public ProtoAdapter_Controller() {
            super(FieldEncoding.LENGTH_DELIMITED, Controller.class);
        }

        public int encodedSize(Controller controller) {
            return ((ControllerType.ADAPTER.encodedSizeWithTag(1, controller.controller_type) + ProtoAdapter.UINT32.encodedSizeWithTag(2, controller.sw_type)) + Version.ADAPTER.encodedSizeWithTag(3, controller.version)) + controller.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, Controller controller) throws IOException {
            ControllerType.ADAPTER.encodeWithTag(protoWriter, 1, controller.controller_type);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 2, controller.sw_type);
            Version.ADAPTER.encodeWithTag(protoWriter, 3, controller.version);
            protoWriter.writeBytes(controller.unknownFields());
        }

        public Controller decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            try {
                                builder.controller_type((ControllerType) ControllerType.ADAPTER.decode(protoReader));
                                break;
                            } catch (EnumConstantNotFoundException e) {
                                builder.addUnknownField(nextTag, FieldEncoding.VARINT, Long.valueOf((long) e.value));
                                break;
                            }
                        case 2:
                            builder.sw_type((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 3:
                            builder.version((Version) Version.ADAPTER.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public Controller redact(Controller controller) {
            controller = controller.newBuilder();
            if (controller.version != null) {
                controller.version = (Version) Version.ADAPTER.redact(controller.version);
            }
            controller.clearUnknownFields();
            return controller.build();
        }
    }

    public Controller(ControllerType controllerType, Integer num, Version version) {
        this(controllerType, num, version, C1624f.f4400b);
    }

    public Controller(ControllerType controllerType, Integer num, Version version, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.controller_type = controllerType;
        this.sw_type = num;
        this.version = version;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.controller_type = this.controller_type;
        builder.sw_type = this.sw_type;
        builder.version = this.version;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Controller)) {
            return false;
        }
        Controller controller = (Controller) obj;
        if (!unknownFields().equals(controller.unknownFields()) || !Internal.equals(this.controller_type, controller.controller_type) || !Internal.equals(this.sw_type, controller.sw_type) || Internal.equals(this.version, controller.version) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((((unknownFields().hashCode() * 37) + (this.controller_type != null ? this.controller_type.hashCode() : 0)) * 37) + (this.sw_type != null ? this.sw_type.hashCode() : 0)) * 37;
        if (this.version != null) {
            i2 = this.version.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.controller_type != null) {
            stringBuilder.append(", controller_type=");
            stringBuilder.append(this.controller_type);
        }
        if (this.sw_type != null) {
            stringBuilder.append(", sw_type=");
            stringBuilder.append(this.sw_type);
        }
        if (this.version != null) {
            stringBuilder.append(", version=");
            stringBuilder.append(this.version);
        }
        stringBuilder = stringBuilder.replace(0, 2, "Controller{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
