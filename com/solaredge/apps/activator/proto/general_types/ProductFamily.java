package com.solaredge.apps.activator.proto.general_types;

import com.squareup.wire.EnumAdapter;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.WireEnum;

public enum ProductFamily implements WireEnum {
    SINGLE_PHASE_INVERTER(0),
    THREE_PHASE_INVERTER(1),
    SAFETY_AND_MONITORING_INTERFACE(2),
    GATEWAY(3),
    STRING_COMBINER(4);
    
    public static final ProtoAdapter<ProductFamily> ADAPTER = null;
    private final int value;

    private static final class ProtoAdapter_ProductFamily extends EnumAdapter<ProductFamily> {
        ProtoAdapter_ProductFamily() {
            super(ProductFamily.class);
        }

        protected ProductFamily fromValue(int i) {
            return ProductFamily.fromValue(i);
        }
    }

    static {
        ADAPTER = new ProtoAdapter_ProductFamily();
    }

    private ProductFamily(int i) {
        this.value = i;
    }

    public static ProductFamily fromValue(int i) {
        switch (i) {
            case 0:
                return SINGLE_PHASE_INVERTER;
            case 1:
                return THREE_PHASE_INVERTER;
            case 2:
                return SAFETY_AND_MONITORING_INTERFACE;
            case 3:
                return GATEWAY;
            case 4:
                return STRING_COMBINER;
            default:
                return 0;
        }
    }

    public int getValue() {
        return this.value;
    }
}
