package com.solaredge.apps.activator.proto.general_types;

import com.squareup.wire.EnumAdapter;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.WireEnum;

public enum ControllerType implements WireEnum {
    PORTIA(0),
    VENUS_DSP1(1),
    VENUS_DSP2(2),
    JUPITER_DSP1(3),
    JUPITER_DSP2(4),
    VEGA_DSP(5),
    DISP_CTRL(6),
    FAN_CTRL(7),
    VENUS3_DSP1(8),
    VENUS3_DSP2(9);
    
    public static final ProtoAdapter<ControllerType> ADAPTER = null;
    private final int value;

    private static final class ProtoAdapter_ControllerType extends EnumAdapter<ControllerType> {
        ProtoAdapter_ControllerType() {
            super(ControllerType.class);
        }

        protected ControllerType fromValue(int i) {
            return ControllerType.fromValue(i);
        }
    }

    static {
        ADAPTER = new ProtoAdapter_ControllerType();
    }

    private ControllerType(int i) {
        this.value = i;
    }

    public static ControllerType fromValue(int i) {
        switch (i) {
            case 0:
                return PORTIA;
            case 1:
                return VENUS_DSP1;
            case 2:
                return VENUS_DSP2;
            case 3:
                return JUPITER_DSP1;
            case 4:
                return JUPITER_DSP2;
            case 5:
                return VEGA_DSP;
            case 6:
                return DISP_CTRL;
            case 7:
                return FAN_CTRL;
            case 8:
                return VENUS3_DSP1;
            case 9:
                return VENUS3_DSP2;
            default:
                return 0;
        }
    }

    public int getValue() {
        return this.value;
    }
}
