package com.solaredge.apps.activator.proto.sw_package;

import com.solaredge.apps.activator.proto.general_types.ControllerType;
import com.solaredge.apps.activator.proto.general_types.SysParams;
import com.solaredge.apps.activator.proto.general_types.Version;
import com.solaredge.apps.activator.proto.general_types.XPacket;
import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoAdapter.EnumConstantNotFoundException;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.WireField.Label;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import java.util.List;
import p125d.C1624f;

public final class SWPackageHeader extends Message<SWPackageHeader, Builder> {
    public static final ProtoAdapter<SWPackageHeader> ADAPTER = new ProtoAdapter_SWPackageHeader();
    public static final Integer DEFAULT_SIZE = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @WireField(adapter = "sw_package.SWPackageHeader$SoftwareConfig#ADAPTER", tag = 2)
    public final SoftwareConfig config;
    @WireField(adapter = "sw_package.SWPackageHeader$MetaData#ADAPTER", tag = 1)
    public final MetaData metadata;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 3)
    public final Integer size;

    public static final class Builder extends com.squareup.wire.Message.Builder<SWPackageHeader, Builder> {
        public SoftwareConfig config;
        public MetaData metadata;
        public Integer size;

        public Builder metadata(MetaData metaData) {
            this.metadata = metaData;
            return this;
        }

        public Builder config(SoftwareConfig softwareConfig) {
            this.config = softwareConfig;
            return this;
        }

        public Builder size(Integer num) {
            this.size = num;
            return this;
        }

        public SWPackageHeader build() {
            return new SWPackageHeader(this.metadata, this.config, this.size, super.buildUnknownFields());
        }
    }

    public static final class MetaData extends Message<MetaData, Builder> {
        public static final ProtoAdapter<MetaData> ADAPTER = new ProtoAdapter_MetaData();
        public static final ControllerType DEFAULT_TARGET = ControllerType.PORTIA;
        private static final long serialVersionUID = 0;
        @WireField(adapter = "general_types.Version#ADAPTER", tag = 4)
        public final Version max_version;
        @WireField(adapter = "general_types.Version#ADAPTER", tag = 3)
        public final Version min_version;
        @WireField(adapter = "general_types.ControllerType#ADAPTER", tag = 1)
        public final ControllerType target;
        @WireField(adapter = "general_types.Version#ADAPTER", tag = 2)
        public final Version version;

        public static final class Builder extends com.squareup.wire.Message.Builder<MetaData, Builder> {
            public Version max_version;
            public Version min_version;
            public ControllerType target;
            public Version version;

            public Builder target(ControllerType controllerType) {
                this.target = controllerType;
                return this;
            }

            public Builder version(Version version) {
                this.version = version;
                return this;
            }

            public Builder min_version(Version version) {
                this.min_version = version;
                return this;
            }

            public Builder max_version(Version version) {
                this.max_version = version;
                return this;
            }

            public MetaData build() {
                return new MetaData(this.target, this.version, this.min_version, this.max_version, super.buildUnknownFields());
            }
        }

        private static final class ProtoAdapter_MetaData extends ProtoAdapter<MetaData> {
            public ProtoAdapter_MetaData() {
                super(FieldEncoding.LENGTH_DELIMITED, MetaData.class);
            }

            public int encodedSize(MetaData metaData) {
                return (((ControllerType.ADAPTER.encodedSizeWithTag(1, metaData.target) + Version.ADAPTER.encodedSizeWithTag(2, metaData.version)) + Version.ADAPTER.encodedSizeWithTag(3, metaData.min_version)) + Version.ADAPTER.encodedSizeWithTag(4, metaData.max_version)) + metaData.unknownFields().mo1912g();
            }

            public void encode(ProtoWriter protoWriter, MetaData metaData) throws IOException {
                ControllerType.ADAPTER.encodeWithTag(protoWriter, 1, metaData.target);
                Version.ADAPTER.encodeWithTag(protoWriter, 2, metaData.version);
                Version.ADAPTER.encodeWithTag(protoWriter, 3, metaData.min_version);
                Version.ADAPTER.encodeWithTag(protoWriter, 4, metaData.max_version);
                protoWriter.writeBytes(metaData.unknownFields());
            }

            public MetaData decode(ProtoReader protoReader) throws IOException {
                Builder builder = new Builder();
                long beginMessage = protoReader.beginMessage();
                while (true) {
                    int nextTag = protoReader.nextTag();
                    if (nextTag != -1) {
                        switch (nextTag) {
                            case 1:
                                try {
                                    builder.target((ControllerType) ControllerType.ADAPTER.decode(protoReader));
                                    break;
                                } catch (EnumConstantNotFoundException e) {
                                    builder.addUnknownField(nextTag, FieldEncoding.VARINT, Long.valueOf((long) e.value));
                                    break;
                                }
                            case 2:
                                builder.version((Version) Version.ADAPTER.decode(protoReader));
                                break;
                            case 3:
                                builder.min_version((Version) Version.ADAPTER.decode(protoReader));
                                break;
                            case 4:
                                builder.max_version((Version) Version.ADAPTER.decode(protoReader));
                                break;
                            default:
                                FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                                builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                                break;
                        }
                    }
                    protoReader.endMessage(beginMessage);
                    return builder.build();
                }
            }

            public MetaData redact(MetaData metaData) {
                metaData = metaData.newBuilder();
                if (metaData.version != null) {
                    metaData.version = (Version) Version.ADAPTER.redact(metaData.version);
                }
                if (metaData.min_version != null) {
                    metaData.min_version = (Version) Version.ADAPTER.redact(metaData.min_version);
                }
                if (metaData.max_version != null) {
                    metaData.max_version = (Version) Version.ADAPTER.redact(metaData.max_version);
                }
                metaData.clearUnknownFields();
                return metaData.build();
            }
        }

        public MetaData(ControllerType controllerType, Version version, Version version2, Version version3) {
            this(controllerType, version, version2, version3, C1624f.f4400b);
        }

        public MetaData(ControllerType controllerType, Version version, Version version2, Version version3, C1624f c1624f) {
            super(ADAPTER, c1624f);
            this.target = controllerType;
            this.version = version;
            this.min_version = version2;
            this.max_version = version3;
        }

        public Builder newBuilder() {
            Builder builder = new Builder();
            builder.target = this.target;
            builder.version = this.version;
            builder.min_version = this.min_version;
            builder.max_version = this.max_version;
            builder.addUnknownFields(unknownFields());
            return builder;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MetaData)) {
                return false;
            }
            MetaData metaData = (MetaData) obj;
            if (!unknownFields().equals(metaData.unknownFields()) || !Internal.equals(this.target, metaData.target) || !Internal.equals(this.version, metaData.version) || !Internal.equals(this.min_version, metaData.min_version) || Internal.equals(this.max_version, metaData.max_version) == null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = this.hashCode;
            if (i != 0) {
                return i;
            }
            int i2 = 0;
            i = ((((((unknownFields().hashCode() * 37) + (this.target != null ? this.target.hashCode() : 0)) * 37) + (this.version != null ? this.version.hashCode() : 0)) * 37) + (this.min_version != null ? this.min_version.hashCode() : 0)) * 37;
            if (this.max_version != null) {
                i2 = this.max_version.hashCode();
            }
            i += i2;
            this.hashCode = i;
            return i;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            if (this.target != null) {
                stringBuilder.append(", target=");
                stringBuilder.append(this.target);
            }
            if (this.version != null) {
                stringBuilder.append(", version=");
                stringBuilder.append(this.version);
            }
            if (this.min_version != null) {
                stringBuilder.append(", min_version=");
                stringBuilder.append(this.min_version);
            }
            if (this.max_version != null) {
                stringBuilder.append(", max_version=");
                stringBuilder.append(this.max_version);
            }
            stringBuilder = stringBuilder.replace(0, 2, "MetaData{");
            stringBuilder.append('}');
            return stringBuilder.toString();
        }
    }

    private static final class ProtoAdapter_SWPackageHeader extends ProtoAdapter<SWPackageHeader> {
        public ProtoAdapter_SWPackageHeader() {
            super(FieldEncoding.LENGTH_DELIMITED, SWPackageHeader.class);
        }

        public int encodedSize(SWPackageHeader sWPackageHeader) {
            return ((MetaData.ADAPTER.encodedSizeWithTag(1, sWPackageHeader.metadata) + SoftwareConfig.ADAPTER.encodedSizeWithTag(2, sWPackageHeader.config)) + ProtoAdapter.UINT32.encodedSizeWithTag(3, sWPackageHeader.size)) + sWPackageHeader.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, SWPackageHeader sWPackageHeader) throws IOException {
            MetaData.ADAPTER.encodeWithTag(protoWriter, 1, sWPackageHeader.metadata);
            SoftwareConfig.ADAPTER.encodeWithTag(protoWriter, 2, sWPackageHeader.config);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 3, sWPackageHeader.size);
            protoWriter.writeBytes(sWPackageHeader.unknownFields());
        }

        public SWPackageHeader decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.metadata((MetaData) MetaData.ADAPTER.decode(protoReader));
                            break;
                        case 2:
                            builder.config((SoftwareConfig) SoftwareConfig.ADAPTER.decode(protoReader));
                            break;
                        case 3:
                            builder.size((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public SWPackageHeader redact(SWPackageHeader sWPackageHeader) {
            sWPackageHeader = sWPackageHeader.newBuilder();
            if (sWPackageHeader.metadata != null) {
                sWPackageHeader.metadata = (MetaData) MetaData.ADAPTER.redact(sWPackageHeader.metadata);
            }
            if (sWPackageHeader.config != null) {
                sWPackageHeader.config = (SoftwareConfig) SoftwareConfig.ADAPTER.redact(sWPackageHeader.config);
            }
            sWPackageHeader.clearUnknownFields();
            return sWPackageHeader.build();
        }
    }

    public static final class SoftwareConfig extends Message<SoftwareConfig, Builder> {
        public static final ProtoAdapter<SoftwareConfig> ADAPTER = new ProtoAdapter_SoftwareConfig();
        private static final long serialVersionUID = 0;
        @WireField(adapter = "general_types.XPacket#ADAPTER", label = Label.REPEATED, tag = 4)
        public final List<XPacket> packets_after_upgrade;
        @WireField(adapter = "general_types.XPacket#ADAPTER", label = Label.REPEATED, tag = 3)
        public final List<XPacket> packets_before_upgrade;
        @WireField(adapter = "general_types.SysParams#ADAPTER", tag = 2)
        public final SysParams params_after_upgrade;
        @WireField(adapter = "general_types.SysParams#ADAPTER", tag = 1)
        public final SysParams params_before_upgrade;

        public static final class Builder extends com.squareup.wire.Message.Builder<SoftwareConfig, Builder> {
            public List<XPacket> packets_after_upgrade = Internal.newMutableList();
            public List<XPacket> packets_before_upgrade = Internal.newMutableList();
            public SysParams params_after_upgrade;
            public SysParams params_before_upgrade;

            public Builder params_before_upgrade(SysParams sysParams) {
                this.params_before_upgrade = sysParams;
                return this;
            }

            public Builder params_after_upgrade(SysParams sysParams) {
                this.params_after_upgrade = sysParams;
                return this;
            }

            public Builder packets_before_upgrade(List<XPacket> list) {
                Internal.checkElementsNotNull((List) list);
                this.packets_before_upgrade = list;
                return this;
            }

            public Builder packets_after_upgrade(List<XPacket> list) {
                Internal.checkElementsNotNull((List) list);
                this.packets_after_upgrade = list;
                return this;
            }

            public SoftwareConfig build() {
                return new SoftwareConfig(this.params_before_upgrade, this.params_after_upgrade, this.packets_before_upgrade, this.packets_after_upgrade, super.buildUnknownFields());
            }
        }

        private static final class ProtoAdapter_SoftwareConfig extends ProtoAdapter<SoftwareConfig> {
            public ProtoAdapter_SoftwareConfig() {
                super(FieldEncoding.LENGTH_DELIMITED, SoftwareConfig.class);
            }

            public int encodedSize(SoftwareConfig softwareConfig) {
                return (((SysParams.ADAPTER.encodedSizeWithTag(1, softwareConfig.params_before_upgrade) + SysParams.ADAPTER.encodedSizeWithTag(2, softwareConfig.params_after_upgrade)) + XPacket.ADAPTER.asRepeated().encodedSizeWithTag(3, softwareConfig.packets_before_upgrade)) + XPacket.ADAPTER.asRepeated().encodedSizeWithTag(4, softwareConfig.packets_after_upgrade)) + softwareConfig.unknownFields().mo1912g();
            }

            public void encode(ProtoWriter protoWriter, SoftwareConfig softwareConfig) throws IOException {
                SysParams.ADAPTER.encodeWithTag(protoWriter, 1, softwareConfig.params_before_upgrade);
                SysParams.ADAPTER.encodeWithTag(protoWriter, 2, softwareConfig.params_after_upgrade);
                XPacket.ADAPTER.asRepeated().encodeWithTag(protoWriter, 3, softwareConfig.packets_before_upgrade);
                XPacket.ADAPTER.asRepeated().encodeWithTag(protoWriter, 4, softwareConfig.packets_after_upgrade);
                protoWriter.writeBytes(softwareConfig.unknownFields());
            }

            public SoftwareConfig decode(ProtoReader protoReader) throws IOException {
                Builder builder = new Builder();
                long beginMessage = protoReader.beginMessage();
                while (true) {
                    int nextTag = protoReader.nextTag();
                    if (nextTag != -1) {
                        switch (nextTag) {
                            case 1:
                                builder.params_before_upgrade((SysParams) SysParams.ADAPTER.decode(protoReader));
                                break;
                            case 2:
                                builder.params_after_upgrade((SysParams) SysParams.ADAPTER.decode(protoReader));
                                break;
                            case 3:
                                builder.packets_before_upgrade.add(XPacket.ADAPTER.decode(protoReader));
                                break;
                            case 4:
                                builder.packets_after_upgrade.add(XPacket.ADAPTER.decode(protoReader));
                                break;
                            default:
                                FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                                builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                                break;
                        }
                    }
                    protoReader.endMessage(beginMessage);
                    return builder.build();
                }
            }

            public SoftwareConfig redact(SoftwareConfig softwareConfig) {
                softwareConfig = softwareConfig.newBuilder();
                if (softwareConfig.params_before_upgrade != null) {
                    softwareConfig.params_before_upgrade = (SysParams) SysParams.ADAPTER.redact(softwareConfig.params_before_upgrade);
                }
                if (softwareConfig.params_after_upgrade != null) {
                    softwareConfig.params_after_upgrade = (SysParams) SysParams.ADAPTER.redact(softwareConfig.params_after_upgrade);
                }
                Internal.redactElements(softwareConfig.packets_before_upgrade, XPacket.ADAPTER);
                Internal.redactElements(softwareConfig.packets_after_upgrade, XPacket.ADAPTER);
                softwareConfig.clearUnknownFields();
                return softwareConfig.build();
            }
        }

        public SoftwareConfig(SysParams sysParams, SysParams sysParams2, List<XPacket> list, List<XPacket> list2) {
            this(sysParams, sysParams2, list, list2, C1624f.f4400b);
        }

        public SoftwareConfig(SysParams sysParams, SysParams sysParams2, List<XPacket> list, List<XPacket> list2, C1624f c1624f) {
            super(ADAPTER, c1624f);
            this.params_before_upgrade = sysParams;
            this.params_after_upgrade = sysParams2;
            this.packets_before_upgrade = Internal.immutableCopyOf("packets_before_upgrade", (List) list);
            this.packets_after_upgrade = Internal.immutableCopyOf("packets_after_upgrade", (List) list2);
        }

        public Builder newBuilder() {
            Builder builder = new Builder();
            builder.params_before_upgrade = this.params_before_upgrade;
            builder.params_after_upgrade = this.params_after_upgrade;
            builder.packets_before_upgrade = Internal.copyOf("packets_before_upgrade", this.packets_before_upgrade);
            builder.packets_after_upgrade = Internal.copyOf("packets_after_upgrade", this.packets_after_upgrade);
            builder.addUnknownFields(unknownFields());
            return builder;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof SoftwareConfig)) {
                return false;
            }
            SoftwareConfig softwareConfig = (SoftwareConfig) obj;
            if (!unknownFields().equals(softwareConfig.unknownFields()) || !Internal.equals(this.params_before_upgrade, softwareConfig.params_before_upgrade) || !Internal.equals(this.params_after_upgrade, softwareConfig.params_after_upgrade) || !this.packets_before_upgrade.equals(softwareConfig.packets_before_upgrade) || this.packets_after_upgrade.equals(softwareConfig.packets_after_upgrade) == null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = this.hashCode;
            if (i != 0) {
                return i;
            }
            int i2 = 0;
            i = ((unknownFields().hashCode() * 37) + (this.params_before_upgrade != null ? this.params_before_upgrade.hashCode() : 0)) * 37;
            if (this.params_after_upgrade != null) {
                i2 = this.params_after_upgrade.hashCode();
            }
            i = ((((i + i2) * 37) + this.packets_before_upgrade.hashCode()) * 37) + this.packets_after_upgrade.hashCode();
            this.hashCode = i;
            return i;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            if (this.params_before_upgrade != null) {
                stringBuilder.append(", params_before_upgrade=");
                stringBuilder.append(this.params_before_upgrade);
            }
            if (this.params_after_upgrade != null) {
                stringBuilder.append(", params_after_upgrade=");
                stringBuilder.append(this.params_after_upgrade);
            }
            if (!this.packets_before_upgrade.isEmpty()) {
                stringBuilder.append(", packets_before_upgrade=");
                stringBuilder.append(this.packets_before_upgrade);
            }
            if (!this.packets_after_upgrade.isEmpty()) {
                stringBuilder.append(", packets_after_upgrade=");
                stringBuilder.append(this.packets_after_upgrade);
            }
            stringBuilder = stringBuilder.replace(0, 2, "SoftwareConfig{");
            stringBuilder.append('}');
            return stringBuilder.toString();
        }
    }

    public SWPackageHeader(MetaData metaData, SoftwareConfig softwareConfig, Integer num) {
        this(metaData, softwareConfig, num, C1624f.f4400b);
    }

    public SWPackageHeader(MetaData metaData, SoftwareConfig softwareConfig, Integer num, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.metadata = metaData;
        this.config = softwareConfig;
        this.size = num;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.metadata = this.metadata;
        builder.config = this.config;
        builder.size = this.size;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SWPackageHeader)) {
            return false;
        }
        SWPackageHeader sWPackageHeader = (SWPackageHeader) obj;
        if (!unknownFields().equals(sWPackageHeader.unknownFields()) || !Internal.equals(this.metadata, sWPackageHeader.metadata) || !Internal.equals(this.config, sWPackageHeader.config) || Internal.equals(this.size, sWPackageHeader.size) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((((unknownFields().hashCode() * 37) + (this.metadata != null ? this.metadata.hashCode() : 0)) * 37) + (this.config != null ? this.config.hashCode() : 0)) * 37;
        if (this.size != null) {
            i2 = this.size.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.metadata != null) {
            stringBuilder.append(", metadata=");
            stringBuilder.append(this.metadata);
        }
        if (this.config != null) {
            stringBuilder.append(", config=");
            stringBuilder.append(this.config);
        }
        if (this.size != null) {
            stringBuilder.append(", size=");
            stringBuilder.append(this.size);
        }
        stringBuilder = stringBuilder.replace(0, 2, "SWPackageHeader{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
