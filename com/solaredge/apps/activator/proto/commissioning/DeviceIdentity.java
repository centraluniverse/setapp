package com.solaredge.apps.activator.proto.commissioning;

import com.solaredge.apps.activator.proto.general_types.Battery;
import com.solaredge.apps.activator.proto.general_types.Controller;
import com.solaredge.apps.activator.proto.general_types.ProductFamily;
import com.solaredge.apps.activator.proto.general_types.ProductType;
import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoAdapter.EnumConstantNotFoundException;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.WireField.Label;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import java.util.List;
import p125d.C1624f;

public final class DeviceIdentity extends Message<DeviceIdentity, Builder> {
    public static final ProtoAdapter<DeviceIdentity> ADAPTER = new ProtoAdapter_DeviceIdentity();
    public static final String DEFAULT_HOME_URL = "";
    public static final Boolean DEFAULT_IS_ACTIVATED = Boolean.valueOf(false);
    public static final ProductFamily DEFAULT_PRODUCT_FAMILY = ProductFamily.SINGLE_PHASE_INVERTER;
    public static final ProductType DEFAULT_PRODUCT_TYPE = ProductType.VENUS;
    public static final String DEFAULT_SERIAL_NUMBER = "";
    public static final String DEFAULT_STATUS_URL = "";
    private static final long serialVersionUID = 0;
    @WireField(adapter = "general_types.Battery#ADAPTER", label = Label.REPEATED, tag = 5)
    public final List<Battery> batteries;
    @WireField(adapter = "general_types.Controller#ADAPTER", label = Label.REPEATED, tag = 4)
    public final List<Controller> controllers;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 7)
    public final String home_url;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#BOOL", tag = 1)
    public final Boolean is_activated;
    @WireField(adapter = "general_types.ProductFamily#ADAPTER", tag = 2)
    public final ProductFamily product_family;
    @WireField(adapter = "general_types.ProductType#ADAPTER", tag = 3)
    public final ProductType product_type;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 6)
    public final String serial_number;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 8)
    public final String status_url;

    public static final class Builder extends com.squareup.wire.Message.Builder<DeviceIdentity, Builder> {
        public List<Battery> batteries = Internal.newMutableList();
        public List<Controller> controllers = Internal.newMutableList();
        public String home_url;
        public Boolean is_activated;
        public ProductFamily product_family;
        public ProductType product_type;
        public String serial_number;
        public String status_url;

        public Builder is_activated(Boolean bool) {
            this.is_activated = bool;
            return this;
        }

        public Builder product_family(ProductFamily productFamily) {
            this.product_family = productFamily;
            return this;
        }

        public Builder product_type(ProductType productType) {
            this.product_type = productType;
            return this;
        }

        public Builder controllers(List<Controller> list) {
            Internal.checkElementsNotNull((List) list);
            this.controllers = list;
            return this;
        }

        public Builder batteries(List<Battery> list) {
            Internal.checkElementsNotNull((List) list);
            this.batteries = list;
            return this;
        }

        public Builder serial_number(String str) {
            this.serial_number = str;
            return this;
        }

        public Builder home_url(String str) {
            this.home_url = str;
            return this;
        }

        public Builder status_url(String str) {
            this.status_url = str;
            return this;
        }

        public DeviceIdentity build() {
            return new DeviceIdentity(this.is_activated, this.product_family, this.product_type, this.controllers, this.batteries, this.serial_number, this.home_url, this.status_url, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_DeviceIdentity extends ProtoAdapter<DeviceIdentity> {
        public ProtoAdapter_DeviceIdentity() {
            super(FieldEncoding.LENGTH_DELIMITED, DeviceIdentity.class);
        }

        public int encodedSize(DeviceIdentity deviceIdentity) {
            return (((((((ProtoAdapter.BOOL.encodedSizeWithTag(1, deviceIdentity.is_activated) + ProductFamily.ADAPTER.encodedSizeWithTag(2, deviceIdentity.product_family)) + ProductType.ADAPTER.encodedSizeWithTag(3, deviceIdentity.product_type)) + Controller.ADAPTER.asRepeated().encodedSizeWithTag(4, deviceIdentity.controllers)) + Battery.ADAPTER.asRepeated().encodedSizeWithTag(5, deviceIdentity.batteries)) + ProtoAdapter.STRING.encodedSizeWithTag(6, deviceIdentity.serial_number)) + ProtoAdapter.STRING.encodedSizeWithTag(7, deviceIdentity.home_url)) + ProtoAdapter.STRING.encodedSizeWithTag(8, deviceIdentity.status_url)) + deviceIdentity.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, DeviceIdentity deviceIdentity) throws IOException {
            ProtoAdapter.BOOL.encodeWithTag(protoWriter, 1, deviceIdentity.is_activated);
            ProductFamily.ADAPTER.encodeWithTag(protoWriter, 2, deviceIdentity.product_family);
            ProductType.ADAPTER.encodeWithTag(protoWriter, 3, deviceIdentity.product_type);
            Controller.ADAPTER.asRepeated().encodeWithTag(protoWriter, 4, deviceIdentity.controllers);
            Battery.ADAPTER.asRepeated().encodeWithTag(protoWriter, 5, deviceIdentity.batteries);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 6, deviceIdentity.serial_number);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 7, deviceIdentity.home_url);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 8, deviceIdentity.status_url);
            protoWriter.writeBytes(deviceIdentity.unknownFields());
        }

        public DeviceIdentity decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.is_activated((Boolean) ProtoAdapter.BOOL.decode(protoReader));
                            break;
                        case 2:
                            try {
                                builder.product_family((ProductFamily) ProductFamily.ADAPTER.decode(protoReader));
                                break;
                            } catch (EnumConstantNotFoundException e) {
                                builder.addUnknownField(nextTag, FieldEncoding.VARINT, Long.valueOf((long) e.value));
                                break;
                            }
                        case 3:
                            try {
                                builder.product_type((ProductType) ProductType.ADAPTER.decode(protoReader));
                                break;
                            } catch (EnumConstantNotFoundException e2) {
                                builder.addUnknownField(nextTag, FieldEncoding.VARINT, Long.valueOf((long) e2.value));
                                break;
                            }
                        case 4:
                            builder.controllers.add(Controller.ADAPTER.decode(protoReader));
                            break;
                        case 5:
                            builder.batteries.add(Battery.ADAPTER.decode(protoReader));
                            break;
                        case 6:
                            builder.serial_number((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        case 7:
                            builder.home_url((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        case 8:
                            builder.status_url((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public DeviceIdentity redact(DeviceIdentity deviceIdentity) {
            deviceIdentity = deviceIdentity.newBuilder();
            Internal.redactElements(deviceIdentity.controllers, Controller.ADAPTER);
            Internal.redactElements(deviceIdentity.batteries, Battery.ADAPTER);
            deviceIdentity.clearUnknownFields();
            return deviceIdentity.build();
        }
    }

    public DeviceIdentity(Boolean bool, ProductFamily productFamily, ProductType productType, List<Controller> list, List<Battery> list2, String str, String str2, String str3) {
        this(bool, productFamily, productType, list, list2, str, str2, str3, C1624f.f4400b);
    }

    public DeviceIdentity(Boolean bool, ProductFamily productFamily, ProductType productType, List<Controller> list, List<Battery> list2, String str, String str2, String str3, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.is_activated = bool;
        this.product_family = productFamily;
        this.product_type = productType;
        this.controllers = Internal.immutableCopyOf("controllers", (List) list);
        this.batteries = Internal.immutableCopyOf("batteries", (List) list2);
        this.serial_number = str;
        this.home_url = str2;
        this.status_url = str3;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.is_activated = this.is_activated;
        builder.product_family = this.product_family;
        builder.product_type = this.product_type;
        builder.controllers = Internal.copyOf("controllers", this.controllers);
        builder.batteries = Internal.copyOf("batteries", this.batteries);
        builder.serial_number = this.serial_number;
        builder.home_url = this.home_url;
        builder.status_url = this.status_url;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof DeviceIdentity)) {
            return false;
        }
        DeviceIdentity deviceIdentity = (DeviceIdentity) obj;
        if (!unknownFields().equals(deviceIdentity.unknownFields()) || !Internal.equals(this.is_activated, deviceIdentity.is_activated) || !Internal.equals(this.product_family, deviceIdentity.product_family) || !Internal.equals(this.product_type, deviceIdentity.product_type) || !this.controllers.equals(deviceIdentity.controllers) || !this.batteries.equals(deviceIdentity.batteries) || !Internal.equals(this.serial_number, deviceIdentity.serial_number) || !Internal.equals(this.home_url, deviceIdentity.home_url) || Internal.equals(this.status_url, deviceIdentity.status_url) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((((((((((((((unknownFields().hashCode() * 37) + (this.is_activated != null ? this.is_activated.hashCode() : 0)) * 37) + (this.product_family != null ? this.product_family.hashCode() : 0)) * 37) + (this.product_type != null ? this.product_type.hashCode() : 0)) * 37) + this.controllers.hashCode()) * 37) + this.batteries.hashCode()) * 37) + (this.serial_number != null ? this.serial_number.hashCode() : 0)) * 37) + (this.home_url != null ? this.home_url.hashCode() : 0)) * 37;
        if (this.status_url != null) {
            i2 = this.status_url.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.is_activated != null) {
            stringBuilder.append(", is_activated=");
            stringBuilder.append(this.is_activated);
        }
        if (this.product_family != null) {
            stringBuilder.append(", product_family=");
            stringBuilder.append(this.product_family);
        }
        if (this.product_type != null) {
            stringBuilder.append(", product_type=");
            stringBuilder.append(this.product_type);
        }
        if (!this.controllers.isEmpty()) {
            stringBuilder.append(", controllers=");
            stringBuilder.append(this.controllers);
        }
        if (!this.batteries.isEmpty()) {
            stringBuilder.append(", batteries=");
            stringBuilder.append(this.batteries);
        }
        if (this.serial_number != null) {
            stringBuilder.append(", serial_number=");
            stringBuilder.append(this.serial_number);
        }
        if (this.home_url != null) {
            stringBuilder.append(", home_url=");
            stringBuilder.append(this.home_url);
        }
        if (this.status_url != null) {
            stringBuilder.append(", status_url=");
            stringBuilder.append(this.status_url);
        }
        stringBuilder = stringBuilder.replace(0, 2, "DeviceIdentity{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
