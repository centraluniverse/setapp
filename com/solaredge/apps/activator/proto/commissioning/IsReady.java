package com.solaredge.apps.activator.proto.commissioning;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import p125d.C1624f;

public final class IsReady extends Message<IsReady, Builder> {
    public static final ProtoAdapter<IsReady> ADAPTER = new ProtoAdapter_IsReady();
    public static final Integer DEFAULT_PROGRESS = Integer.valueOf(0);
    public static final Boolean DEFAULT_READY = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 1)
    public final Integer progress;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#BOOL", tag = 2)
    public final Boolean ready;

    public static final class Builder extends com.squareup.wire.Message.Builder<IsReady, Builder> {
        public Integer progress;
        public Boolean ready;

        public Builder progress(Integer num) {
            this.progress = num;
            return this;
        }

        public Builder ready(Boolean bool) {
            this.ready = bool;
            return this;
        }

        public IsReady build() {
            return new IsReady(this.progress, this.ready, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_IsReady extends ProtoAdapter<IsReady> {
        public ProtoAdapter_IsReady() {
            super(FieldEncoding.LENGTH_DELIMITED, IsReady.class);
        }

        public int encodedSize(IsReady isReady) {
            return (ProtoAdapter.UINT32.encodedSizeWithTag(1, isReady.progress) + ProtoAdapter.BOOL.encodedSizeWithTag(2, isReady.ready)) + isReady.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, IsReady isReady) throws IOException {
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 1, isReady.progress);
            ProtoAdapter.BOOL.encodeWithTag(protoWriter, 2, isReady.ready);
            protoWriter.writeBytes(isReady.unknownFields());
        }

        public IsReady decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.progress((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 2:
                            builder.ready((Boolean) ProtoAdapter.BOOL.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public IsReady redact(IsReady isReady) {
            isReady = isReady.newBuilder();
            isReady.clearUnknownFields();
            return isReady.build();
        }
    }

    public IsReady(Integer num, Boolean bool) {
        this(num, bool, C1624f.f4400b);
    }

    public IsReady(Integer num, Boolean bool, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.progress = num;
        this.ready = bool;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.progress = this.progress;
        builder.ready = this.ready;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof IsReady)) {
            return false;
        }
        IsReady isReady = (IsReady) obj;
        if (!unknownFields().equals(isReady.unknownFields()) || !Internal.equals(this.progress, isReady.progress) || Internal.equals(this.ready, isReady.ready) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((unknownFields().hashCode() * 37) + (this.progress != null ? this.progress.hashCode() : 0)) * 37;
        if (this.ready != null) {
            i2 = this.ready.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.progress != null) {
            stringBuilder.append(", progress=");
            stringBuilder.append(this.progress);
        }
        if (this.ready != null) {
            stringBuilder.append(", ready=");
            stringBuilder.append(this.ready);
        }
        stringBuilder = stringBuilder.replace(0, 2, "IsReady{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
