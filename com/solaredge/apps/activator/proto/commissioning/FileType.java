package com.solaredge.apps.activator.proto.commissioning;

import com.squareup.wire.EnumAdapter;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.WireEnum;

public enum FileType implements WireEnum {
    PORTIA(0),
    DSP1(1),
    DSP2(2),
    ACTIVATION(3),
    CONFIGURATION(4);
    
    public static final ProtoAdapter<FileType> ADAPTER = null;
    private final int value;

    private static final class ProtoAdapter_FileType extends EnumAdapter<FileType> {
        ProtoAdapter_FileType() {
            super(FileType.class);
        }

        protected FileType fromValue(int i) {
            return FileType.fromValue(i);
        }
    }

    static {
        ADAPTER = new ProtoAdapter_FileType();
    }

    private FileType(int i) {
        this.value = i;
    }

    public static FileType fromValue(int i) {
        switch (i) {
            case 0:
                return PORTIA;
            case 1:
                return DSP1;
            case 2:
                return DSP2;
            case 3:
                return ACTIVATION;
            case 4:
                return CONFIGURATION;
            default:
                return 0;
        }
    }

    public int getValue() {
        return this.value;
    }
}
