package com.solaredge.apps.activator.proto.commissioning;

import com.squareup.wire.EnumAdapter;
import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoAdapter.EnumConstantNotFoundException;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireEnum;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import p125d.C1624f;

public final class ErrorResponse extends Message<ErrorResponse, Builder> {
    public static final ProtoAdapter<ErrorResponse> ADAPTER = new ProtoAdapter_ErrorResponse();
    public static final String DEFAULT_DESCRIPTION = "";
    public static final ErrorCode DEFAULT_ERROR_CODE = ErrorCode.UNDEFINED;
    private static final long serialVersionUID = 0;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 2)
    public final String description;
    @WireField(adapter = "commissioning.ErrorResponse$ErrorCode#ADAPTER", tag = 1)
    public final ErrorCode error_code;

    public static final class Builder extends com.squareup.wire.Message.Builder<ErrorResponse, Builder> {
        public String description;
        public ErrorCode error_code;

        public Builder error_code(ErrorCode errorCode) {
            this.error_code = errorCode;
            return this;
        }

        public Builder description(String str) {
            this.description = str;
            return this;
        }

        public ErrorResponse build() {
            return new ErrorResponse(this.error_code, this.description, super.buildUnknownFields());
        }
    }

    public enum ErrorCode implements WireEnum {
        UNDEFINED(0),
        AUTHENTICATION_ERROR(1),
        COMMUNICATION_ERROR(2),
        INTERNAL_ERROR(3),
        INVALID_FILE(4);
        
        public static final ProtoAdapter<ErrorCode> ADAPTER = null;
        private final int value;

        private static final class ProtoAdapter_ErrorCode extends EnumAdapter<ErrorCode> {
            ProtoAdapter_ErrorCode() {
                super(ErrorCode.class);
            }

            protected ErrorCode fromValue(int i) {
                return ErrorCode.fromValue(i);
            }
        }

        static {
            ADAPTER = new ProtoAdapter_ErrorCode();
        }

        private ErrorCode(int i) {
            this.value = i;
        }

        public static ErrorCode fromValue(int i) {
            switch (i) {
                case 0:
                    return UNDEFINED;
                case 1:
                    return AUTHENTICATION_ERROR;
                case 2:
                    return COMMUNICATION_ERROR;
                case 3:
                    return INTERNAL_ERROR;
                case 4:
                    return INVALID_FILE;
                default:
                    return 0;
            }
        }

        public int getValue() {
            return this.value;
        }
    }

    private static final class ProtoAdapter_ErrorResponse extends ProtoAdapter<ErrorResponse> {
        public ProtoAdapter_ErrorResponse() {
            super(FieldEncoding.LENGTH_DELIMITED, ErrorResponse.class);
        }

        public int encodedSize(ErrorResponse errorResponse) {
            return (ErrorCode.ADAPTER.encodedSizeWithTag(1, errorResponse.error_code) + ProtoAdapter.STRING.encodedSizeWithTag(2, errorResponse.description)) + errorResponse.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, ErrorResponse errorResponse) throws IOException {
            ErrorCode.ADAPTER.encodeWithTag(protoWriter, 1, errorResponse.error_code);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 2, errorResponse.description);
            protoWriter.writeBytes(errorResponse.unknownFields());
        }

        public ErrorResponse decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            try {
                                builder.error_code((ErrorCode) ErrorCode.ADAPTER.decode(protoReader));
                                break;
                            } catch (EnumConstantNotFoundException e) {
                                builder.addUnknownField(nextTag, FieldEncoding.VARINT, Long.valueOf((long) e.value));
                                break;
                            }
                        case 2:
                            builder.description((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public ErrorResponse redact(ErrorResponse errorResponse) {
            errorResponse = errorResponse.newBuilder();
            errorResponse.clearUnknownFields();
            return errorResponse.build();
        }
    }

    public ErrorResponse(ErrorCode errorCode, String str) {
        this(errorCode, str, C1624f.f4400b);
    }

    public ErrorResponse(ErrorCode errorCode, String str, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.error_code = errorCode;
        this.description = str;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.error_code = this.error_code;
        builder.description = this.description;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ErrorResponse)) {
            return false;
        }
        ErrorResponse errorResponse = (ErrorResponse) obj;
        if (!unknownFields().equals(errorResponse.unknownFields()) || !Internal.equals(this.error_code, errorResponse.error_code) || Internal.equals(this.description, errorResponse.description) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((unknownFields().hashCode() * 37) + (this.error_code != null ? this.error_code.hashCode() : 0)) * 37;
        if (this.description != null) {
            i2 = this.description.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.error_code != null) {
            stringBuilder.append(", error_code=");
            stringBuilder.append(this.error_code);
        }
        if (this.description != null) {
            stringBuilder.append(", description=");
            stringBuilder.append(this.description);
        }
        stringBuilder = stringBuilder.replace(0, 2, "ErrorResponse{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
