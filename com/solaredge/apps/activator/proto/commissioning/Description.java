package com.solaredge.apps.activator.proto.commissioning;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import p125d.C1624f;

public final class Description extends Message<Description, Builder> {
    public static final ProtoAdapter<Description> ADAPTER = new ProtoAdapter_Description();
    public static final Integer DEFAULT_FILES_COUNT = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @WireField(adapter = "commissioning.Description$ActivationMetadata#ADAPTER", tag = 2)
    public final ActivationMetadata activation_metadata;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 1)
    public final Integer files_count;

    public static final class ActivationMetadata extends Message<ActivationMetadata, Builder> {
        public static final ProtoAdapter<ActivationMetadata> ADAPTER = new ProtoAdapter_ActivationMetadata();
        public static final String DEFAULT_PN = "";
        public static final C1624f DEFAULT_SPFF_HASH = C1624f.f4400b;
        private static final long serialVersionUID = 0;
        @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 2)
        public final String pn;
        @WireField(adapter = "com.squareup.wire.ProtoAdapter#BYTES", tag = 1)
        public final C1624f spff_hash;

        public static final class Builder extends com.squareup.wire.Message.Builder<ActivationMetadata, Builder> {
            public String pn;
            public C1624f spff_hash;

            public Builder spff_hash(C1624f c1624f) {
                this.spff_hash = c1624f;
                return this;
            }

            public Builder pn(String str) {
                this.pn = str;
                return this;
            }

            public ActivationMetadata build() {
                return new ActivationMetadata(this.spff_hash, this.pn, super.buildUnknownFields());
            }
        }

        private static final class ProtoAdapter_ActivationMetadata extends ProtoAdapter<ActivationMetadata> {
            public ProtoAdapter_ActivationMetadata() {
                super(FieldEncoding.LENGTH_DELIMITED, ActivationMetadata.class);
            }

            public int encodedSize(ActivationMetadata activationMetadata) {
                return (ProtoAdapter.BYTES.encodedSizeWithTag(1, activationMetadata.spff_hash) + ProtoAdapter.STRING.encodedSizeWithTag(2, activationMetadata.pn)) + activationMetadata.unknownFields().mo1912g();
            }

            public void encode(ProtoWriter protoWriter, ActivationMetadata activationMetadata) throws IOException {
                ProtoAdapter.BYTES.encodeWithTag(protoWriter, 1, activationMetadata.spff_hash);
                ProtoAdapter.STRING.encodeWithTag(protoWriter, 2, activationMetadata.pn);
                protoWriter.writeBytes(activationMetadata.unknownFields());
            }

            public ActivationMetadata decode(ProtoReader protoReader) throws IOException {
                Builder builder = new Builder();
                long beginMessage = protoReader.beginMessage();
                while (true) {
                    int nextTag = protoReader.nextTag();
                    if (nextTag != -1) {
                        switch (nextTag) {
                            case 1:
                                builder.spff_hash((C1624f) ProtoAdapter.BYTES.decode(protoReader));
                                break;
                            case 2:
                                builder.pn((String) ProtoAdapter.STRING.decode(protoReader));
                                break;
                            default:
                                FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                                builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                                break;
                        }
                    }
                    protoReader.endMessage(beginMessage);
                    return builder.build();
                }
            }

            public ActivationMetadata redact(ActivationMetadata activationMetadata) {
                activationMetadata = activationMetadata.newBuilder();
                activationMetadata.clearUnknownFields();
                return activationMetadata.build();
            }
        }

        public ActivationMetadata(C1624f c1624f, String str) {
            this(c1624f, str, C1624f.f4400b);
        }

        public ActivationMetadata(C1624f c1624f, String str, C1624f c1624f2) {
            super(ADAPTER, c1624f2);
            this.spff_hash = c1624f;
            this.pn = str;
        }

        public Builder newBuilder() {
            Builder builder = new Builder();
            builder.spff_hash = this.spff_hash;
            builder.pn = this.pn;
            builder.addUnknownFields(unknownFields());
            return builder;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ActivationMetadata)) {
                return false;
            }
            ActivationMetadata activationMetadata = (ActivationMetadata) obj;
            if (!unknownFields().equals(activationMetadata.unknownFields()) || !Internal.equals(this.spff_hash, activationMetadata.spff_hash) || Internal.equals(this.pn, activationMetadata.pn) == null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = this.hashCode;
            if (i != 0) {
                return i;
            }
            int i2 = 0;
            i = ((unknownFields().hashCode() * 37) + (this.spff_hash != null ? this.spff_hash.hashCode() : 0)) * 37;
            if (this.pn != null) {
                i2 = this.pn.hashCode();
            }
            i += i2;
            this.hashCode = i;
            return i;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            if (this.spff_hash != null) {
                stringBuilder.append(", spff_hash=");
                stringBuilder.append(this.spff_hash);
            }
            if (this.pn != null) {
                stringBuilder.append(", pn=");
                stringBuilder.append(this.pn);
            }
            stringBuilder = stringBuilder.replace(0, 2, "ActivationMetadata{");
            stringBuilder.append('}');
            return stringBuilder.toString();
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<Description, Builder> {
        public ActivationMetadata activation_metadata;
        public Integer files_count;

        public Builder files_count(Integer num) {
            this.files_count = num;
            return this;
        }

        public Builder activation_metadata(ActivationMetadata activationMetadata) {
            this.activation_metadata = activationMetadata;
            return this;
        }

        public Description build() {
            return new Description(this.files_count, this.activation_metadata, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_Description extends ProtoAdapter<Description> {
        public ProtoAdapter_Description() {
            super(FieldEncoding.LENGTH_DELIMITED, Description.class);
        }

        public int encodedSize(Description description) {
            return (ProtoAdapter.UINT32.encodedSizeWithTag(1, description.files_count) + ActivationMetadata.ADAPTER.encodedSizeWithTag(2, description.activation_metadata)) + description.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, Description description) throws IOException {
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 1, description.files_count);
            ActivationMetadata.ADAPTER.encodeWithTag(protoWriter, 2, description.activation_metadata);
            protoWriter.writeBytes(description.unknownFields());
        }

        public Description decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.files_count((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 2:
                            builder.activation_metadata((ActivationMetadata) ActivationMetadata.ADAPTER.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public Description redact(Description description) {
            description = description.newBuilder();
            if (description.activation_metadata != null) {
                description.activation_metadata = (ActivationMetadata) ActivationMetadata.ADAPTER.redact(description.activation_metadata);
            }
            description.clearUnknownFields();
            return description.build();
        }
    }

    public Description(Integer num, ActivationMetadata activationMetadata) {
        this(num, activationMetadata, C1624f.f4400b);
    }

    public Description(Integer num, ActivationMetadata activationMetadata, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.files_count = num;
        this.activation_metadata = activationMetadata;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.files_count = this.files_count;
        builder.activation_metadata = this.activation_metadata;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Description)) {
            return false;
        }
        Description description = (Description) obj;
        if (!unknownFields().equals(description.unknownFields()) || !Internal.equals(this.files_count, description.files_count) || Internal.equals(this.activation_metadata, description.activation_metadata) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((unknownFields().hashCode() * 37) + (this.files_count != null ? this.files_count.hashCode() : 0)) * 37;
        if (this.activation_metadata != null) {
            i2 = this.activation_metadata.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.files_count != null) {
            stringBuilder.append(", files_count=");
            stringBuilder.append(this.files_count);
        }
        if (this.activation_metadata != null) {
            stringBuilder.append(", activation_metadata=");
            stringBuilder.append(this.activation_metadata);
        }
        stringBuilder = stringBuilder.replace(0, 2, "Description{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
