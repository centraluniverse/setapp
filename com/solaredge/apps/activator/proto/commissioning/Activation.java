package com.solaredge.apps.activator.proto.commissioning;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import p125d.C1624f;

public final class Activation extends Message<Activation, Builder> {
    public static final ProtoAdapter<Activation> ADAPTER = new ProtoAdapter_Activation();
    public static final String DEFAULT_PN = "";
    public static final C1624f DEFAULT_SPFF = C1624f.f4400b;
    public static final C1624f DEFAULT_SPFF_HASH = C1624f.f4400b;
    private static final long serialVersionUID = 0;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 3)
    public final String pn;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#BYTES", tag = 1)
    public final C1624f spff;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#BYTES", tag = 2)
    public final C1624f spff_hash;

    public static final class Builder extends com.squareup.wire.Message.Builder<Activation, Builder> {
        public String pn;
        public C1624f spff;
        public C1624f spff_hash;

        public Builder spff(C1624f c1624f) {
            this.spff = c1624f;
            return this;
        }

        public Builder spff_hash(C1624f c1624f) {
            this.spff_hash = c1624f;
            return this;
        }

        public Builder pn(String str) {
            this.pn = str;
            return this;
        }

        public Activation build() {
            return new Activation(this.spff, this.spff_hash, this.pn, super.buildUnknownFields());
        }
    }

    private static final class ProtoAdapter_Activation extends ProtoAdapter<Activation> {
        public ProtoAdapter_Activation() {
            super(FieldEncoding.LENGTH_DELIMITED, Activation.class);
        }

        public int encodedSize(Activation activation) {
            return ((ProtoAdapter.BYTES.encodedSizeWithTag(1, activation.spff) + ProtoAdapter.BYTES.encodedSizeWithTag(2, activation.spff_hash)) + ProtoAdapter.STRING.encodedSizeWithTag(3, activation.pn)) + activation.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, Activation activation) throws IOException {
            ProtoAdapter.BYTES.encodeWithTag(protoWriter, 1, activation.spff);
            ProtoAdapter.BYTES.encodeWithTag(protoWriter, 2, activation.spff_hash);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 3, activation.pn);
            protoWriter.writeBytes(activation.unknownFields());
        }

        public Activation decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.spff((C1624f) ProtoAdapter.BYTES.decode(protoReader));
                            break;
                        case 2:
                            builder.spff_hash((C1624f) ProtoAdapter.BYTES.decode(protoReader));
                            break;
                        case 3:
                            builder.pn((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public Activation redact(Activation activation) {
            activation = activation.newBuilder();
            activation.clearUnknownFields();
            return activation.build();
        }
    }

    public Activation(C1624f c1624f, C1624f c1624f2, String str) {
        this(c1624f, c1624f2, str, C1624f.f4400b);
    }

    public Activation(C1624f c1624f, C1624f c1624f2, String str, C1624f c1624f3) {
        super(ADAPTER, c1624f3);
        this.spff = c1624f;
        this.spff_hash = c1624f2;
        this.pn = str;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.spff = this.spff;
        builder.spff_hash = this.spff_hash;
        builder.pn = this.pn;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Activation)) {
            return false;
        }
        Activation activation = (Activation) obj;
        if (!unknownFields().equals(activation.unknownFields()) || !Internal.equals(this.spff, activation.spff) || !Internal.equals(this.spff_hash, activation.spff_hash) || Internal.equals(this.pn, activation.pn) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((((unknownFields().hashCode() * 37) + (this.spff != null ? this.spff.hashCode() : 0)) * 37) + (this.spff_hash != null ? this.spff_hash.hashCode() : 0)) * 37;
        if (this.pn != null) {
            i2 = this.pn.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.spff != null) {
            stringBuilder.append(", spff=");
            stringBuilder.append(this.spff);
        }
        if (this.spff_hash != null) {
            stringBuilder.append(", spff_hash=");
            stringBuilder.append(this.spff_hash);
        }
        if (this.pn != null) {
            stringBuilder.append(", pn=");
            stringBuilder.append(this.pn);
        }
        stringBuilder = stringBuilder.replace(0, 2, "Activation{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
