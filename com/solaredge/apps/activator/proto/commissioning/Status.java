package com.solaredge.apps.activator.proto.commissioning;

import com.squareup.wire.EnumAdapter;
import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoAdapter.EnumConstantNotFoundException;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireEnum;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import java.util.Map;
import p125d.C1624f;

public final class Status extends Message<Status, Builder> {
    public static final ProtoAdapter<Status> ADAPTER = new ProtoAdapter_Status();
    public static final String DEFAULT_SERIAL_NUMBER = "";
    public static final Integer DEFAULT_TOTAL_PROGRESS = Integer.valueOf(0);
    public static final Integer DEFAULT_TOTAL_TIME = Integer.valueOf(0);
    public static final UpgradeStatus DEFAULT_UPGRADE_STATUS = UpgradeStatus.idle;
    private static final long serialVersionUID = 0;
    @WireField(adapter = "commissioning.Status$FileState#ADAPTER", keyAdapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 1)
    public final Map<String, FileState> file_state;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#STRING", tag = 2)
    public final String serial_number;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 5)
    public final Integer total_progress;
    @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 4)
    public final Integer total_time;
    @WireField(adapter = "commissioning.Status$UpgradeStatus#ADAPTER", tag = 3)
    public final UpgradeStatus upgrade_status;

    public static final class Builder extends com.squareup.wire.Message.Builder<Status, Builder> {
        public Map<String, FileState> file_state = Internal.newMutableMap();
        public String serial_number;
        public Integer total_progress;
        public Integer total_time;
        public UpgradeStatus upgrade_status;

        public Builder file_state(Map<String, FileState> map) {
            Internal.checkElementsNotNull((Map) map);
            this.file_state = map;
            return this;
        }

        public Builder serial_number(String str) {
            this.serial_number = str;
            return this;
        }

        public Builder upgrade_status(UpgradeStatus upgradeStatus) {
            this.upgrade_status = upgradeStatus;
            return this;
        }

        public Builder total_time(Integer num) {
            this.total_time = num;
            return this;
        }

        public Builder total_progress(Integer num) {
            this.total_progress = num;
            return this;
        }

        public Status build() {
            return new Status(this.file_state, this.serial_number, this.upgrade_status, this.total_time, this.total_progress, super.buildUnknownFields());
        }
    }

    public static final class FileState extends Message<FileState, Builder> {
        public static final ProtoAdapter<FileState> ADAPTER = new ProtoAdapter_FileState();
        public static final Integer DEFAULT_EXECUTION_PROGRESS = Integer.valueOf(0);
        public static final FileStatus DEFAULT_FILE_STATUS = FileStatus.none;
        public static final FileType DEFAULT_FILE_TYPE = FileType.PORTIA;
        public static final Integer DEFAULT_RETRY = Integer.valueOf(0);
        private static final long serialVersionUID = 0;
        @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 4)
        public final Integer execution_progress;
        @WireField(adapter = "commissioning.Status$FileStatus#ADAPTER", tag = 3)
        public final FileStatus file_status;
        @WireField(adapter = "commissioning.FileType#ADAPTER", tag = 1)
        public final FileType file_type;
        @WireField(adapter = "com.squareup.wire.ProtoAdapter#UINT32", tag = 2)
        public final Integer retry;

        public static final class Builder extends com.squareup.wire.Message.Builder<FileState, Builder> {
            public Integer execution_progress;
            public FileStatus file_status;
            public FileType file_type;
            public Integer retry;

            public Builder file_type(FileType fileType) {
                this.file_type = fileType;
                return this;
            }

            public Builder retry(Integer num) {
                this.retry = num;
                return this;
            }

            public Builder file_status(FileStatus fileStatus) {
                this.file_status = fileStatus;
                return this;
            }

            public Builder execution_progress(Integer num) {
                this.execution_progress = num;
                return this;
            }

            public FileState build() {
                return new FileState(this.file_type, this.retry, this.file_status, this.execution_progress, super.buildUnknownFields());
            }
        }

        private static final class ProtoAdapter_FileState extends ProtoAdapter<FileState> {
            public ProtoAdapter_FileState() {
                super(FieldEncoding.LENGTH_DELIMITED, FileState.class);
            }

            public int encodedSize(FileState fileState) {
                return (((FileType.ADAPTER.encodedSizeWithTag(1, fileState.file_type) + ProtoAdapter.UINT32.encodedSizeWithTag(2, fileState.retry)) + FileStatus.ADAPTER.encodedSizeWithTag(3, fileState.file_status)) + ProtoAdapter.UINT32.encodedSizeWithTag(4, fileState.execution_progress)) + fileState.unknownFields().mo1912g();
            }

            public void encode(ProtoWriter protoWriter, FileState fileState) throws IOException {
                FileType.ADAPTER.encodeWithTag(protoWriter, 1, fileState.file_type);
                ProtoAdapter.UINT32.encodeWithTag(protoWriter, 2, fileState.retry);
                FileStatus.ADAPTER.encodeWithTag(protoWriter, 3, fileState.file_status);
                ProtoAdapter.UINT32.encodeWithTag(protoWriter, 4, fileState.execution_progress);
                protoWriter.writeBytes(fileState.unknownFields());
            }

            public FileState decode(ProtoReader protoReader) throws IOException {
                Builder builder = new Builder();
                long beginMessage = protoReader.beginMessage();
                while (true) {
                    int nextTag = protoReader.nextTag();
                    if (nextTag != -1) {
                        switch (nextTag) {
                            case 1:
                                try {
                                    builder.file_type((FileType) FileType.ADAPTER.decode(protoReader));
                                    break;
                                } catch (EnumConstantNotFoundException e) {
                                    builder.addUnknownField(nextTag, FieldEncoding.VARINT, Long.valueOf((long) e.value));
                                    break;
                                }
                            case 2:
                                builder.retry((Integer) ProtoAdapter.UINT32.decode(protoReader));
                                break;
                            case 3:
                                try {
                                    builder.file_status((FileStatus) FileStatus.ADAPTER.decode(protoReader));
                                    break;
                                } catch (EnumConstantNotFoundException e2) {
                                    builder.addUnknownField(nextTag, FieldEncoding.VARINT, Long.valueOf((long) e2.value));
                                    break;
                                }
                            case 4:
                                builder.execution_progress((Integer) ProtoAdapter.UINT32.decode(protoReader));
                                break;
                            default:
                                FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                                builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                                break;
                        }
                    }
                    protoReader.endMessage(beginMessage);
                    return builder.build();
                }
            }

            public FileState redact(FileState fileState) {
                fileState = fileState.newBuilder();
                fileState.clearUnknownFields();
                return fileState.build();
            }
        }

        public FileState(FileType fileType, Integer num, FileStatus fileStatus, Integer num2) {
            this(fileType, num, fileStatus, num2, C1624f.f4400b);
        }

        public FileState(FileType fileType, Integer num, FileStatus fileStatus, Integer num2, C1624f c1624f) {
            super(ADAPTER, c1624f);
            this.file_type = fileType;
            this.retry = num;
            this.file_status = fileStatus;
            this.execution_progress = num2;
        }

        public Builder newBuilder() {
            Builder builder = new Builder();
            builder.file_type = this.file_type;
            builder.retry = this.retry;
            builder.file_status = this.file_status;
            builder.execution_progress = this.execution_progress;
            builder.addUnknownFields(unknownFields());
            return builder;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof FileState)) {
                return false;
            }
            FileState fileState = (FileState) obj;
            if (!unknownFields().equals(fileState.unknownFields()) || !Internal.equals(this.file_type, fileState.file_type) || !Internal.equals(this.retry, fileState.retry) || !Internal.equals(this.file_status, fileState.file_status) || Internal.equals(this.execution_progress, fileState.execution_progress) == null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = this.hashCode;
            if (i != 0) {
                return i;
            }
            int i2 = 0;
            i = ((((((unknownFields().hashCode() * 37) + (this.file_type != null ? this.file_type.hashCode() : 0)) * 37) + (this.retry != null ? this.retry.hashCode() : 0)) * 37) + (this.file_status != null ? this.file_status.hashCode() : 0)) * 37;
            if (this.execution_progress != null) {
                i2 = this.execution_progress.hashCode();
            }
            i += i2;
            this.hashCode = i;
            return i;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            if (this.file_type != null) {
                stringBuilder.append(", file_type=");
                stringBuilder.append(this.file_type);
            }
            if (this.retry != null) {
                stringBuilder.append(", retry=");
                stringBuilder.append(this.retry);
            }
            if (this.file_status != null) {
                stringBuilder.append(", file_status=");
                stringBuilder.append(this.file_status);
            }
            if (this.execution_progress != null) {
                stringBuilder.append(", execution_progress=");
                stringBuilder.append(this.execution_progress);
            }
            stringBuilder = stringBuilder.replace(0, 2, "FileState{");
            stringBuilder.append('}');
            return stringBuilder.toString();
        }
    }

    public enum FileStatus implements WireEnum {
        none(0),
        uploaded(1),
        processing(2);
        
        public static final ProtoAdapter<FileStatus> ADAPTER = null;
        private final int value;

        private static final class ProtoAdapter_FileStatus extends EnumAdapter<FileStatus> {
            ProtoAdapter_FileStatus() {
                super(FileStatus.class);
            }

            protected FileStatus fromValue(int i) {
                return FileStatus.fromValue(i);
            }
        }

        static {
            ADAPTER = new ProtoAdapter_FileStatus();
        }

        private FileStatus(int i) {
            this.value = i;
        }

        public static FileStatus fromValue(int i) {
            switch (i) {
                case 0:
                    return none;
                case 1:
                    return uploaded;
                case 2:
                    return processing;
                default:
                    return 0;
            }
        }

        public int getValue() {
            return this.value;
        }
    }

    private static final class ProtoAdapter_Status extends ProtoAdapter<Status> {
        private final ProtoAdapter<Map<String, FileState>> file_state = ProtoAdapter.newMapAdapter(ProtoAdapter.STRING, FileState.ADAPTER);

        public ProtoAdapter_Status() {
            super(FieldEncoding.LENGTH_DELIMITED, Status.class);
        }

        public int encodedSize(Status status) {
            return ((((this.file_state.encodedSizeWithTag(1, status.file_state) + ProtoAdapter.STRING.encodedSizeWithTag(2, status.serial_number)) + UpgradeStatus.ADAPTER.encodedSizeWithTag(3, status.upgrade_status)) + ProtoAdapter.UINT32.encodedSizeWithTag(4, status.total_time)) + ProtoAdapter.UINT32.encodedSizeWithTag(5, status.total_progress)) + status.unknownFields().mo1912g();
        }

        public void encode(ProtoWriter protoWriter, Status status) throws IOException {
            this.file_state.encodeWithTag(protoWriter, 1, status.file_state);
            ProtoAdapter.STRING.encodeWithTag(protoWriter, 2, status.serial_number);
            UpgradeStatus.ADAPTER.encodeWithTag(protoWriter, 3, status.upgrade_status);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 4, status.total_time);
            ProtoAdapter.UINT32.encodeWithTag(protoWriter, 5, status.total_progress);
            protoWriter.writeBytes(status.unknownFields());
        }

        public Status decode(ProtoReader protoReader) throws IOException {
            Builder builder = new Builder();
            long beginMessage = protoReader.beginMessage();
            while (true) {
                int nextTag = protoReader.nextTag();
                if (nextTag != -1) {
                    switch (nextTag) {
                        case 1:
                            builder.file_state.putAll((Map) this.file_state.decode(protoReader));
                            break;
                        case 2:
                            builder.serial_number((String) ProtoAdapter.STRING.decode(protoReader));
                            break;
                        case 3:
                            try {
                                builder.upgrade_status((UpgradeStatus) UpgradeStatus.ADAPTER.decode(protoReader));
                                break;
                            } catch (EnumConstantNotFoundException e) {
                                builder.addUnknownField(nextTag, FieldEncoding.VARINT, Long.valueOf((long) e.value));
                                break;
                            }
                        case 4:
                            builder.total_time((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        case 5:
                            builder.total_progress((Integer) ProtoAdapter.UINT32.decode(protoReader));
                            break;
                        default:
                            FieldEncoding peekFieldEncoding = protoReader.peekFieldEncoding();
                            builder.addUnknownField(nextTag, peekFieldEncoding, peekFieldEncoding.rawProtoAdapter().decode(protoReader));
                            break;
                    }
                }
                protoReader.endMessage(beginMessage);
                return builder.build();
            }
        }

        public Status redact(Status status) {
            status = status.newBuilder();
            Internal.redactElements(status.file_state, FileState.ADAPTER);
            status.clearUnknownFields();
            return status.build();
        }
    }

    public enum UpgradeStatus implements WireEnum {
        idle(0),
        upgrading(1);
        
        public static final ProtoAdapter<UpgradeStatus> ADAPTER = null;
        private final int value;

        private static final class ProtoAdapter_UpgradeStatus extends EnumAdapter<UpgradeStatus> {
            ProtoAdapter_UpgradeStatus() {
                super(UpgradeStatus.class);
            }

            protected UpgradeStatus fromValue(int i) {
                return UpgradeStatus.fromValue(i);
            }
        }

        static {
            ADAPTER = new ProtoAdapter_UpgradeStatus();
        }

        private UpgradeStatus(int i) {
            this.value = i;
        }

        public static UpgradeStatus fromValue(int i) {
            switch (i) {
                case 0:
                    return idle;
                case 1:
                    return upgrading;
                default:
                    return 0;
            }
        }

        public int getValue() {
            return this.value;
        }
    }

    public Status(Map<String, FileState> map, String str, UpgradeStatus upgradeStatus, Integer num, Integer num2) {
        this(map, str, upgradeStatus, num, num2, C1624f.f4400b);
    }

    public Status(Map<String, FileState> map, String str, UpgradeStatus upgradeStatus, Integer num, Integer num2, C1624f c1624f) {
        super(ADAPTER, c1624f);
        this.file_state = Internal.immutableCopyOf("file_state", (Map) map);
        this.serial_number = str;
        this.upgrade_status = upgradeStatus;
        this.total_time = num;
        this.total_progress = num2;
    }

    public Builder newBuilder() {
        Builder builder = new Builder();
        builder.file_state = Internal.copyOf("file_state", this.file_state);
        builder.serial_number = this.serial_number;
        builder.upgrade_status = this.upgrade_status;
        builder.total_time = this.total_time;
        builder.total_progress = this.total_progress;
        builder.addUnknownFields(unknownFields());
        return builder;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        if (!unknownFields().equals(status.unknownFields()) || !this.file_state.equals(status.file_state) || !Internal.equals(this.serial_number, status.serial_number) || !Internal.equals(this.upgrade_status, status.upgrade_status) || !Internal.equals(this.total_time, status.total_time) || Internal.equals(this.total_progress, status.total_progress) == null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = ((((((((unknownFields().hashCode() * 37) + this.file_state.hashCode()) * 37) + (this.serial_number != null ? this.serial_number.hashCode() : 0)) * 37) + (this.upgrade_status != null ? this.upgrade_status.hashCode() : 0)) * 37) + (this.total_time != null ? this.total_time.hashCode() : 0)) * 37;
        if (this.total_progress != null) {
            i2 = this.total_progress.hashCode();
        }
        i += i2;
        this.hashCode = i;
        return i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (!this.file_state.isEmpty()) {
            stringBuilder.append(", file_state=");
            stringBuilder.append(this.file_state);
        }
        if (this.serial_number != null) {
            stringBuilder.append(", serial_number=");
            stringBuilder.append(this.serial_number);
        }
        if (this.upgrade_status != null) {
            stringBuilder.append(", upgrade_status=");
            stringBuilder.append(this.upgrade_status);
        }
        if (this.total_time != null) {
            stringBuilder.append(", total_time=");
            stringBuilder.append(this.total_time);
        }
        if (this.total_progress != null) {
            stringBuilder.append(", total_progress=");
            stringBuilder.append(this.total_progress);
        }
        stringBuilder = stringBuilder.replace(0, 2, "Status{");
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
