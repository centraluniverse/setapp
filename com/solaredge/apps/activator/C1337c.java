package com.solaredge.apps.activator;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/* compiled from: FontManager */
public class C1337c {
    public static Typeface m3674a(Context context, String str) {
        return Typeface.createFromAsset(context.getAssets(), str);
    }

    public static void m3675a(View view, Typeface typeface) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                C1337c.m3675a(viewGroup.getChildAt(i), typeface);
            }
        } else if (view instanceof TextView) {
            ((TextView) view).setTypeface(typeface);
        }
    }
}
