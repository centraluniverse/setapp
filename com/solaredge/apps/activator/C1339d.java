package com.solaredge.apps.activator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NetworkHandler */
public class C1339d {
    private static C1339d f3330b = null;
    private static boolean f3331c = true;
    private List<C1340e> f3332a = new ArrayList();
    private BroadcastReceiver f3333d = new C13381(this);

    /* compiled from: NetworkHandler */
    class C13381 extends BroadcastReceiver {
        final /* synthetic */ C1339d f3329a;

        C13381(C1339d c1339d) {
            this.f3329a = c1339d;
        }

        public void onReceive(Context context, Intent intent) {
            context = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (context == null) {
                C1339d.f3331c = true;
            } else if (C1339d.f3331c != null) {
                if (context.isConnectedOrConnecting() != null && isInitialStickyBroadcast() == null && this.f3329a.f3332a != null && this.f3329a.f3332a.isEmpty() == null) {
                    for (C1340e a : this.f3329a.f3332a) {
                        a.mo2811a();
                    }
                }
                C1339d.f3331c = null;
            }
        }
    }

    public static synchronized C1339d m3676a() {
        C1339d c1339d;
        synchronized (C1339d.class) {
            if (f3330b == null) {
                f3330b = new C1339d();
            }
            c1339d = f3330b;
        }
        return c1339d;
    }

    private void m3680c() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        InverterActivator.m3590a().registerReceiver(this.f3333d, intentFilter);
    }

    private void m3681d() {
        if (this.f3333d != null) {
            InverterActivator.m3590a().unregisterReceiver(this.f3333d);
        }
    }

    public void m3682a(C1340e c1340e) {
        if (this.f3332a.isEmpty()) {
            m3680c();
        }
        if (!this.f3332a.contains(c1340e)) {
            this.f3332a.add(c1340e);
        }
    }

    public void m3683b(com.solaredge.apps.activator.C1340e r2) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r1 = this;
        r0 = r1.f3332a;
        r0 = r0.contains(r2);
        if (r0 == 0) goto L_0x000d;
    L_0x0008:
        r0 = r1.f3332a;
        r0.remove(r2);
    L_0x000d:
        r2 = r1.f3332a;
        r2 = r2.isEmpty();
        if (r2 == 0) goto L_0x0018;
    L_0x0015:
        r1.m3681d();	 Catch:{ IllegalArgumentException -> 0x0018 }
    L_0x0018:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.apps.activator.d.b(com.solaredge.apps.activator.e):void");
    }
}
