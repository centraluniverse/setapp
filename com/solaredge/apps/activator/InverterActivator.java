package com.solaredge.apps.activator;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v4.app.TaskStackBuilder;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.crashlytics.android.core.CrashlyticsCore.Builder;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.solaredge.apps.activator.Activity.WelcomeActivity;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.C1366a;
import com.solaredge.common.C1368b;
import com.solaredge.common.C1371c;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p114e.C1381b;
import com.solaredge.common.p114e.C1394f;
import com.solaredge.common.ui.LoginActivity;
import p008b.p009a.p010a.p011a.C0452c;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InverterActivator extends Application {
    private static InverterActivator f3277b;
    public FirebaseAnalytics f3278a;
    private C1371c f3279c = new C21721(this);

    class C21721 implements C1371c {
        final /* synthetic */ InverterActivator f5468a;

        public void mo1773a() {
        }

        C21721(InverterActivator inverterActivator) {
            this.f5468a = inverterActivator;
        }

        public void mo1774a(Activity activity) {
            this.f5468a.f3278a.logEvent("login", null);
            Intent intent = new Intent(activity, WelcomeActivity.class);
            intent.setFlags(268435456);
            intent.addFlags(67108864);
            this.f5468a.startActivity(intent);
            activity.finish();
        }

        public void mo1775b(Activity activity) {
            C1331d.m3670d();
        }
    }

    class C21732 implements Callback<Void> {
        final /* synthetic */ InverterActivator f5469a;

        public void onFailure(Call<Void> call, Throwable th) {
        }

        public void onResponse(Call<Void> call, Response<Void> response) {
        }

        C21732(InverterActivator inverterActivator) {
            this.f5469a = inverterActivator;
        }
    }

    public static InverterActivator m3590a() {
        return f3277b;
    }

    public void onCreate() {
        super.onCreate();
        CrashlyticsCore build = new Builder().disabled(false).build();
        C0452c.m358a((Context) this, new Crashlytics.Builder().core(build).build());
        f3277b = this;
        m3597b();
        m3591c();
        C1323a.m3615a().m3634l();
        this.f3278a = FirebaseAnalytics.getInstance(this);
    }

    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public void m3597b() {
        C1366a.m3749a().m3751a((Context) this);
        C1366a.m3749a().m3752a("InverterActivator");
        C1365j.m3740a(30);
    }

    private void m3591c() {
        C1368b a = C1368b.m3756a();
        C1368b.f3389b = true;
        a.m3759a("server_url");
        a.m3757a(this.f3279c);
    }

    public void m3592a(Context context) {
        C1365j.m3739a().m3742b();
        C1365j.m3739a().m3745d().m3720a().enqueue(new C21732(this));
        C1381b.m3777a().m3780a(false);
        C1381b.m3777a().m3781b(m3590a());
        if (VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().removeAllCookies(null);
        } else {
            CookieSyncManager.createInstance(this);
            if (CookieManager.getInstance().hasCookies()) {
                CookieManager.getInstance().removeAllCookie();
            }
        }
        Intent intent = new Intent(m3590a(), LoginActivity.class);
        intent.putExtra("arg_close_on_back", true);
        intent.setFlags(268468224);
        if (context != null) {
            context.startActivity(intent);
        } else {
            context = TaskStackBuilder.create(C1366a.m3749a().m3753b());
            context.addNextIntentWithParentStack(intent);
            context.startActivities();
        }
        C1331d.m3670d();
        m3594a("logout");
    }

    public void m3594a(String str) {
        m3593a(new Bundle(), str);
    }

    public void m3596a(String str, String str2) {
        Bundle bundle = new Bundle();
        bundle.putString("LABEL", str2);
        m3593a(bundle, str);
    }

    public void m3595a(String str, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("VALUE", i);
        m3593a(bundle, str);
    }

    public void m3593a(Bundle bundle, String str) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString("NAME", str);
        bundle.putString("EMAIL", C1394f.m3816a().m3823c(getApplicationContext()));
        this.f3278a.logEvent("EVENT", bundle);
    }
}
