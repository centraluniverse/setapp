package com.solaredge.apps.activator;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p116g.C1398a;
import java.io.InputStream;
import java.util.List;
import p021c.C0560v;
import retrofit2.Response;

public class UploadingService extends IntentService implements C1344h {
    public static String f5470a = "FILES_TO_UPLOAD";
    public static String f5471b = "UPLOAD_COMPLETED";
    public static String f5472c = "UPLOAD_FAILED";
    public static String f5473d = "ALL_DONE";
    public static String f5474e = "UPLOADING_ACTION";
    public static String f5475f = "UPLOADING_PROGRESS";
    public static String f5476g = "FILE_INDEX";
    public static String f5477h = "FILES_UPLOADED";
    public static String f5478i = "ACTIVATION_FILE_PRESENT";
    public static boolean f5479j = false;
    private final IBinder f5480k = new C1319a(this);

    public class C1319a extends Binder {
        final /* synthetic */ UploadingService f3280a;

        public C1319a(UploadingService uploadingService) {
            this.f3280a = uploadingService;
        }
    }

    public void mo1776a(int i, int i2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("   FileIndex: ");
        stringBuilder.append(i);
        stringBuilder.append(" , Upload progress: ");
        stringBuilder.append(i2);
        C1398a.m3831a(stringBuilder.toString());
        Intent intent = new Intent(f5474e);
        intent.putExtra(f5475f, i2);
        intent.putExtra(f5476g, i);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public UploadingService() {
        super("UploadingService");
    }

    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            intent = intent.getExtras();
            if (intent != null) {
                if (!f5479j) {
                    f5479j = true;
                }
                List<C1334b> list = (List) intent.getSerializable(f5470a);
                if (list != null) {
                    if (!list.isEmpty()) {
                        boolean z = false;
                        for (C1334b c1334b : list) {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append("Uploading file: ");
                            stringBuilder.append(c1334b.f3324b);
                            C1398a.m3831a(stringBuilder.toString());
                            if (C1343g.m3688a(c1334b.f3323a.intValue())) {
                                z = true;
                            }
                            try {
                                InputStream open = getAssets().open(c1334b.f3324b);
                                byte[] bArr = new byte[open.available()];
                                open.read(bArr);
                                open.close();
                                Response execute = C1365j.m3739a().m3746e().m3728a(c1334b.f3323a, new C2176f(C0560v.m986a("application/x-protobuf"), bArr, this, 1, c1334b.f3323a.intValue())).execute();
                                if (execute.isSuccessful()) {
                                    stringBuilder = new StringBuilder();
                                    stringBuilder.append("Finished Uploading File: ");
                                    stringBuilder.append(c1334b.f3324b);
                                    C1398a.m3831a(stringBuilder.toString());
                                    Intent intent2 = new Intent(f5474e);
                                    intent2.putExtra(f5474e, f5471b);
                                    intent2.putExtra(f5476g, c1334b.f3323a);
                                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
                                } else {
                                    intent = new StringBuilder();
                                    intent.append("Failed Uploading File: ");
                                    intent.append(c1334b.f3324b);
                                    intent.append("(");
                                    intent.append(execute.message());
                                    intent.append(")");
                                    C1398a.m3831a(intent.toString());
                                    InverterActivator.m3590a().m3596a("UPLOADING_FILES_FAILED", c1334b.f3324b);
                                    intent = new Intent(f5474e);
                                    intent.putExtra(f5474e, f5472c);
                                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                    f5479j = false;
                                    return;
                                }
                            } catch (Intent intent3) {
                                intent3.printStackTrace();
                                StringBuilder stringBuilder2 = new StringBuilder();
                                stringBuilder2.append("Failed Uploading File: ");
                                stringBuilder2.append(c1334b.f3324b);
                                stringBuilder2.append("(");
                                stringBuilder2.append(intent3.getMessage());
                                stringBuilder2.append(")");
                                C1398a.m3831a(stringBuilder2.toString());
                                InverterActivator.m3590a().m3596a("UPLOADING_FILES_FAILED", c1334b.f3324b);
                                intent3 = new Intent(f5474e);
                                intent3.putExtra(f5474e, f5472c);
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent3);
                                f5479j = false;
                                return;
                            }
                        }
                        InverterActivator.m3590a().m3595a("UPLOADING_FILES_SUCCESS", list.size());
                        Intent intent4 = new Intent(f5474e);
                        intent4.putExtra(f5474e, f5473d);
                        intent4.putExtra(f5477h, list.size());
                        intent4.putExtra(f5478i, z);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent4);
                        f5479j = false;
                        return;
                    }
                }
                f5479j = false;
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return this.f5480k;
    }
}
