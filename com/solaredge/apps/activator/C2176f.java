package com.solaredge.apps.activator;

import android.os.Handler;
import android.os.Looper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import p021c.C0560v;
import p021c.ab;
import p125d.C2283d;

/* compiled from: ProgressRequestBody */
public class C2176f extends ab {
    private static int f5481a = 2048;
    private C0560v f5482b;
    private byte[] f5483c;
    private C1344h f5484d;
    private int f5485e;
    private int f5486f = 0;
    private int f5487g;

    public C2176f(C0560v c0560v, byte[] bArr, C1344h c1344h, int i, int i2) {
        this.f5482b = c0560v;
        this.f5483c = bArr;
        this.f5484d = c1344h;
        this.f5485e = i;
        this.f5487g = i2;
    }

    public C0560v contentType() {
        return this.f5482b;
    }

    public long contentLength() throws IOException {
        return (long) this.f5483c.length;
    }

    public void writeTo(C2283d c2283d) throws IOException {
        this.f5486f++;
        long length = (long) this.f5483c.length;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.f5483c);
        byte[] bArr = new byte[f5481a];
        try {
            Handler handler = new Handler(Looper.getMainLooper());
            long j = 0;
            while (true) {
                int read = byteArrayInputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                if (j % 1000 == 0 && this.f5486f > this.f5485e) {
                    this.f5484d.mo1776a(this.f5487g, (int) ((100 * j) / length));
                }
                long j2 = j + ((long) read);
                c2283d.mo2521c(bArr, 0, read);
                j = j2;
            }
            if (this.f5486f > this.f5485e) {
                this.f5484d.mo1776a(this.f5487g, 100);
            }
            byteArrayInputStream.close();
        } catch (Throwable th) {
            byteArrayInputStream.close();
        }
    }
}
