package com.solaredge.apps.activator.p107b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.vending.p069a.p070a.C1010c;
import com.solaredge.apps.activator.XAPKDownloaderService;

/* compiled from: XAPKAlarmReceiver */
public class C1332e extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            C1010c.m2581a(context, intent, XAPKDownloaderService.class);
        } catch (Context context2) {
            context2.printStackTrace();
        }
    }
}
