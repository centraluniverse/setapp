package com.solaredge.apps.activator.p107b;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import com.google.p040a.p045c.C0664a;
import com.solaredge.apps.activator.Activity.FailureActivity;
import com.solaredge.apps.activator.Activity.ScanSerialActivity;
import com.solaredge.apps.activator.Activity.WebViewActivity;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.C1334b;
import com.solaredge.apps.activator.proto.general_types.Version;
import com.solaredge.common.C1366a;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1398a;
import com.solaredge.common.p116g.C1404g;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: Utils */
public class C1331d {
    private static final AtomicInteger f3319a = new AtomicInteger(1);

    /* compiled from: Utils */
    public static class C1330a {
        public Long f3318a;
    }

    /* compiled from: Utils */
    static class C21742 extends C0664a<Map<String, C1330a>> {
        C21742() {
        }
    }

    /* compiled from: Utils */
    static class C21753 extends C0664a<Map<String, C1330a>> {
        C21753() {
        }
    }

    public static void m3653a(String str, Context context) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra("WEB_VIEW_URL", str);
        context.startActivity(intent);
    }

    public static void m3650a(Context context) {
        Intent intent = new Intent(context, FailureActivity.class);
        intent.putExtra("ON_FAILURE", "LOST_WIFI_FAILURE");
        context.startActivity(intent);
    }

    public static boolean m3656a() {
        return C1331d.m3673g() && C1331d.m3666b();
    }

    public static boolean m3666b() {
        Context b = C1366a.m3749a().m3753b();
        ConnectivityManager connectivityManager = (ConnectivityManager) b.getSystemService("connectivity");
        WifiManager wifiManager = (WifiManager) b.getSystemService("wifi");
        boolean z = false;
        if (wifiManager != null) {
            WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && wifiManager.isWifiEnabled() && activeNetworkInfo != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getType() == 1 && connectionInfo != null && C1331d.m3660a(connectionInfo.getSSID(), C1323a.m3615a().m3620c())) {
                z = true;
            }
            if (z && TextUtils.isEmpty(C1323a.m3615a().m3631i())) {
                C1331d.m3651a(wifiManager);
            }
        }
        return z;
    }

    private static boolean m3673g() {
        List scanResults = ((WifiManager) C1366a.m3749a().m3753b().getSystemService("wifi")).getScanResults();
        if (scanResults != null) {
            for (int i = 0; i < scanResults.size(); i++) {
                if (((ScanResult) scanResults.get(i)).SSID.equals(C1323a.m3615a().m3620c())) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean m3660a(String str, String str2) {
        if (str != null) {
            if (str2 != null) {
                return TextUtils.equals(str.replace("\"", ""), str2.replace("\"", ""));
            }
        }
        return null;
    }

    public static void m3652a(C1325b c1325b) {
        C1398a.m3831a("Trying to connect to wifi..");
        WifiManager wifiManager = (WifiManager) C1366a.m3749a().m3753b().getSystemService("wifi");
        if (wifiManager != null) {
            if (!C1331d.m3673g()) {
                C1398a.m3831a("WiFi isn't broadcasting..");
                if (C1331d.m3666b()) {
                    C1398a.m3831a("We're connected to Wi-Fi even though it isn't broadcasting.. (Socket remained open) So we will force disconnect wifi and report that network isn't found.");
                    wifiManager.disconnect();
                }
                if (!C1331d.m3671e()) {
                    C1395g.m3824a().m3825a("Location Services needs to be turned on.", 1);
                }
                wifiManager.startScan();
                c1325b.mo2826j();
            } else if (C1331d.m3666b()) {
                c1325b.mo2827k();
            } else {
                wifiManager.setWifiEnabled(true);
                WifiConfiguration wifiConfiguration = new WifiConfiguration();
                wifiConfiguration.SSID = String.format("\"%s\"", new Object[]{C1323a.m3615a().m3620c()});
                wifiConfiguration.preSharedKey = String.format("\"%s\"", new Object[]{C1323a.m3615a().m3622d()});
                wifiConfiguration.hiddenSSID = false;
                wifiConfiguration.status = 2;
                wifiConfiguration.allowedGroupCiphers.set(2);
                wifiConfiguration.allowedGroupCiphers.set(3);
                wifiConfiguration.allowedKeyManagement.set(1);
                wifiConfiguration.allowedPairwiseCiphers.set(1);
                wifiConfiguration.allowedPairwiseCiphers.set(2);
                wifiConfiguration.allowedProtocols.set(1);
                wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfiguration), true);
                wifiManager.reconnect();
                c1325b.mo2825i();
            }
        }
    }

    private static void m3651a(WifiManager wifiManager) {
        wifiManager = wifiManager.getDhcpInfo();
        InetAddress a = C1331d.m3648a(wifiManager.gateway);
        InetAddress a2 = C1331d.m3648a(wifiManager.serverAddress);
        InetAddress a3 = C1331d.m3648a(wifiManager.ipAddress);
        wifiManager = C1331d.m3661a(a) ? a.getHostAddress() : C1331d.m3661a(a2) ? a2.getHostAddress() : C1331d.m3661a(a3) ? a3.getHostAddress() : null;
        if (wifiManager != null) {
            StringBuilder stringBuilder;
            if (!wifiManager.startsWith(C1324a.f3310c)) {
                stringBuilder = new StringBuilder();
                stringBuilder.append(C1324a.f3310c);
                stringBuilder.append(wifiManager);
                wifiManager = stringBuilder.toString();
            }
            if (wifiManager.endsWith(C1324a.f3311d)) {
                wifiManager.replace("ActivatorConstants.PORT_SUFFIX", "");
            }
            stringBuilder = new StringBuilder();
            stringBuilder.append("Updating IP Address To: ");
            stringBuilder.append(wifiManager);
            C1398a.m3831a(stringBuilder.toString());
            C1323a.m3615a().m3625e(wifiManager);
        }
    }

    private static boolean m3661a(InetAddress inetAddress) {
        return (inetAddress == null || inetAddress.getHostAddress() == null || inetAddress.getHostAddress().contains(C1324a.f3312e) != null) ? null : true;
    }

    public static void m3668c() {
        WifiManager wifiManager = (WifiManager) C1366a.m3749a().m3753b().getSystemService("wifi");
        if (wifiManager != null) {
            C1398a.m3831a("Trying to disconnect from existing network to allow for auto connect to new wifi.");
            List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
            if (configuredNetworks != null) {
                for (WifiConfiguration wifiConfiguration : configuredNetworks) {
                    if (wifiConfiguration.SSID != null) {
                        wifiManager.removeNetwork(wifiConfiguration.networkId);
                    }
                }
            }
            int i = 0;
            while (i < 3) {
                if (!wifiManager.disconnect()) {
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public static void m3670d() {
        if (C1331d.m3666b()) {
            WifiManager wifiManager = (WifiManager) C1366a.m3749a().m3753b().getSystemService("wifi");
            if (wifiManager != null) {
                List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
                if (configuredNetworks != null) {
                    for (WifiConfiguration wifiConfiguration : configuredNetworks) {
                        if (wifiConfiguration.SSID != null) {
                            String str = wifiConfiguration.SSID;
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append("\"");
                            stringBuilder.append(C1323a.m3615a().m3620c());
                            stringBuilder.append("\"");
                            if (str.equals(stringBuilder.toString())) {
                                wifiManager.disconnect();
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    public static void m3665b(String str, Context context) {
        if (context != null && TextUtils.isEmpty(str) == null) {
            C1365j.m3739a().m3743b(str);
        }
    }

    public static boolean m3658a(Long l, Long l2, Long l3) {
        return (l.longValue() > l2.longValue() || l.longValue() < l3.longValue()) ? null : true;
    }

    public static String m3646a(Version version) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(version.major);
        stringBuilder.append(".");
        stringBuilder.append(version.minor);
        stringBuilder.append(".");
        stringBuilder.append(version.build);
        return stringBuilder.toString();
    }

    public static long m3662b(Version version) {
        return C1331d.m3644a(version.major, version.minor, version.build);
    }

    public static long m3645a(String str) {
        str = str.split("\\.");
        return C1331d.m3644a(Integer.valueOf(Integer.parseInt(str[0])), Integer.valueOf(Integer.parseInt(str[1])), Integer.valueOf(Integer.parseInt(str[2])));
    }

    public static long m3644a(Integer num, Integer num2, Integer num3) {
        return (long) (((Math.pow(10.0d, 10.0d) * ((double) num.intValue())) + (Math.pow(10.0d, 5.0d) * ((double) num2.intValue()))) + ((double) num3.intValue()));
    }

    public static void m3664b(String str) {
        str = str.replace("WIFI:", "");
        C1323a.m3615a().m3639q();
        for (String split : str.split(";")) {
            String[] split2 = split.split(":");
            if (split2.length == 2) {
                if ("S".equals(split2[0])) {
                    C1323a.m3615a().m3619b(split2[1]);
                } else if ("P".equals(split2[0])) {
                    C1323a.m3615a().m3621c(split2[1]);
                } else if ("SN".equals(split2[0])) {
                    C1323a.m3615a().m3616a(split2[1]);
                } else if ("PN".equals(split2[0])) {
                    C1323a.m3615a().m3623d(split2[1]);
                } else if ("M".equals(split2[0])) {
                    try {
                        byte[] decode = Base64.decode(split2[1], 0);
                        if (decode != null) {
                            C1323a.m3615a().m3617a(decode);
                        }
                    } catch (Exception e) {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("Exception during qr hash decoding: ");
                        stringBuilder.append(e.getMessage());
                        C1398a.m3831a(stringBuilder.toString());
                    }
                }
            }
        }
    }

    public static java.net.InetAddress m3648a(int r4) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r0 = 4;
        r0 = new byte[r0];
        r1 = 255; // 0xff float:3.57E-43 double:1.26E-321;
        r2 = r1 & r4;
        r2 = (byte) r2;
        r3 = 0;
        r0[r3] = r2;
        r2 = r4 >> 8;
        r2 = r2 & r1;
        r2 = (byte) r2;
        r3 = 1;
        r0[r3] = r2;
        r2 = r4 >> 16;
        r2 = r2 & r1;
        r2 = (byte) r2;
        r3 = 2;
        r0[r3] = r2;
        r4 = r4 >> 24;
        r4 = r4 & r1;
        r4 = (byte) r4;
        r1 = 3;
        r0[r1] = r4;
        r4 = java.net.InetAddress.getByAddress(r0);	 Catch:{ UnknownHostException -> 0x0025 }
        return r4;
    L_0x0025:
        r4 = new java.lang.AssertionError;
        r4.<init>();
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.apps.activator.b.d.a(int):java.net.InetAddress");
    }

    public static String m3667c(String str) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(C1366a.m3749a().m3753b().getAssets().open(str)));
            str = new StringBuilder();
            for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                str.append(readLine);
            }
            bufferedReader.close();
            return str.toString();
        } catch (String str2) {
            str2.printStackTrace();
            return null;
        }
    }

    public static boolean m3657a(Activity activity) {
        if (activity == null) {
            return false;
        }
        int checkSelfPermission = ContextCompat.checkSelfPermission(activity, "android.permission.CAMERA");
        int checkSelfPermission2 = ContextCompat.checkSelfPermission(activity, "android.permission.ACCESS_COARSE_LOCATION");
        int checkSelfPermission3 = ContextCompat.checkSelfPermission(activity, "android.permission.READ_EXTERNAL_STORAGE");
        List arrayList = new ArrayList();
        if (checkSelfPermission2 != 0) {
            arrayList.add("android.permission.ACCESS_COARSE_LOCATION");
        }
        if (checkSelfPermission != 0) {
            arrayList.add("android.permission.CAMERA");
        }
        if (checkSelfPermission3 != 0) {
            arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
        }
        if (arrayList.isEmpty()) {
            return true;
        }
        ActivityCompat.requestPermissions(activity, (String[]) arrayList.toArray(new String[arrayList.size()]), 55);
        return false;
    }

    public static void m3649a(int i, String[] strArr, int[] iArr, final Activity activity, C1326c c1326c) {
        if (i == 55) {
            i = new HashMap();
            int i2 = 0;
            i.put("android.permission.CAMERA", Integer.valueOf(0));
            i.put("android.permission.ACCESS_COARSE_LOCATION", Integer.valueOf(0));
            i.put("android.permission.READ_EXTERNAL_STORAGE", Integer.valueOf(0));
            i.put("android.permission.WRITE_EXTERNAL_STORAGE", Integer.valueOf(0));
            if (iArr.length > 0) {
                while (i2 < strArr.length) {
                    i.put(strArr[i2], Integer.valueOf(iArr[i2]));
                    i2++;
                }
                if (((Integer) i.get("android.permission.CAMERA")).intValue() == null && ((Integer) i.get("android.permission.ACCESS_COARSE_LOCATION")).intValue() == null && ((Integer) i.get("android.permission.READ_EXTERNAL_STORAGE")).intValue() == null && ((Integer) i.get("android.permission.WRITE_EXTERNAL_STORAGE")).intValue() == 0) {
                    C1398a.m3831a("all permissions were granted");
                    c1326c.mo2809d();
                    return;
                }
                C1398a.m3831a("Some permissions are not granted ask again ");
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, "android.permission.CAMERA") == 0 && ActivityCompat.shouldShowRequestPermissionRationale(activity, "android.permission.ACCESS_COARSE_LOCATION") == 0 && ActivityCompat.shouldShowRequestPermissionRationale(activity, "android.permission.READ_EXTERNAL_STORAGE") == 0) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
                        C1395g.m3824a().m3825a(C1382c.m3782a().m3783a("API_Activator_Go_To_Settings_And_Enable_Permissions"), 1);
                        return;
                    }
                }
                C1331d.m3654a(C1382c.m3782a().m3783a("API_Activator_Permissions_Required"), new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case -2:
                                if (activity != null && activity.isFinishing() == null) {
                                    activity.finish();
                                    return;
                                }
                                return;
                            case -1:
                                C1331d.m3657a(activity);
                                return;
                            default:
                                return;
                        }
                    }
                }, activity);
            }
        }
    }

    private static void m3654a(String str, OnClickListener onClickListener, Activity activity) {
        new Builder(activity).setMessage((CharSequence) str).setPositiveButton(C1382c.m3782a().m3783a("API_OK"), onClickListener).setNegativeButton(C1382c.m3782a().m3783a("API_Cancel"), onClickListener).create().show();
    }

    public static void m3663b(Activity activity) {
        if (activity != null) {
            C1331d.m3668c();
            Intent intent = new Intent(activity, ScanSerialActivity.class);
            intent.addFlags(67108864);
            activity.startActivity(intent);
            activity.finish();
        }
    }

    public static boolean m3671e() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r0 = com.solaredge.common.C1366a.m3749a();
        r0 = r0.m3753b();
        r1 = "location";
        r0 = r0.getSystemService(r1);
        r0 = (android.location.LocationManager) r0;
        r1 = 0;
        r2 = "gps";	 Catch:{ Exception -> 0x0018 }
        r2 = r0.isProviderEnabled(r2);	 Catch:{ Exception -> 0x0018 }
        goto L_0x0019;
    L_0x0018:
        r2 = r1;
    L_0x0019:
        r3 = "network";	 Catch:{ Exception -> 0x0020 }
        r0 = r0.isProviderEnabled(r3);	 Catch:{ Exception -> 0x0020 }
        goto L_0x0021;
    L_0x0020:
        r0 = r1;
    L_0x0021:
        if (r2 != 0) goto L_0x0025;
    L_0x0023:
        if (r0 == 0) goto L_0x0026;
    L_0x0025:
        r1 = 1;
    L_0x0026:
        return r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.apps.activator.b.d.e():boolean");
    }

    public static String m3647a(List<C1334b> list) {
        String str = "";
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append("\t");
                stringBuilder.append(((C1334b) list.get(i)).toString());
                str = stringBuilder.toString();
                if (i < list.size() - 1) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append(str);
                    stringBuilder.append(" , ");
                    str = stringBuilder.toString();
                }
                stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append("\n");
                str = stringBuilder.toString();
            }
        }
        return str;
    }

    public static void m3655a(java.lang.String r6, java.lang.Long r7) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r0 = com.solaredge.common.C1366a.m3749a();
        r0 = r0.m3753b();
        r1 = "ACTIVATOR_SHARED";
        r2 = 0;
        r0 = r0.getSharedPreferences(r1, r2);
        r1 = r0.edit();
        r2 = new com.google.a.f;
        r2.<init>();
        r3 = new java.util.HashMap;
        r3.<init>();
        r4 = "ACTIVATION_TIMESTAMP";
        r5 = "";
        r0 = r0.getString(r4, r5);
        r4 = android.text.TextUtils.isEmpty(r0);	 Catch:{ Exception -> 0x0065 }
        if (r4 != 0) goto L_0x0042;	 Catch:{ Exception -> 0x0065 }
    L_0x002b:
        r3 = new com.solaredge.apps.activator.b.d$2;	 Catch:{ Exception -> 0x0065 }
        r3.<init>();	 Catch:{ Exception -> 0x0065 }
        r3 = r3.m1108b();	 Catch:{ Exception -> 0x0065 }
        r0 = r2.m1188a(r0, r3);	 Catch:{ Exception -> 0x0065 }
        r3 = r0;	 Catch:{ Exception -> 0x0065 }
        r3 = (java.util.Map) r3;	 Catch:{ Exception -> 0x0065 }
        if (r3 != 0) goto L_0x0042;	 Catch:{ Exception -> 0x0065 }
    L_0x003d:
        r3 = new java.util.HashMap;	 Catch:{ Exception -> 0x0065 }
        r3.<init>();	 Catch:{ Exception -> 0x0065 }
    L_0x0042:
        r0 = r3.get(r6);	 Catch:{ Exception -> 0x0065 }
        r0 = (com.solaredge.apps.activator.p107b.C1331d.C1330a) r0;	 Catch:{ Exception -> 0x0065 }
        if (r0 != 0) goto L_0x004f;	 Catch:{ Exception -> 0x0065 }
    L_0x004a:
        r0 = new com.solaredge.apps.activator.b.d$a;	 Catch:{ Exception -> 0x0065 }
        r0.<init>();	 Catch:{ Exception -> 0x0065 }
    L_0x004f:
        r0.f3318a = r7;	 Catch:{ Exception -> 0x0065 }
        r7 = "Initializing UpdateActivationTimeStamp. (showing summary activity)";	 Catch:{ Exception -> 0x0065 }
        com.solaredge.common.p116g.C1398a.m3831a(r7);	 Catch:{ Exception -> 0x0065 }
        r3.put(r6, r0);	 Catch:{ Exception -> 0x0065 }
        r6 = r2.m1190a(r3);	 Catch:{ Exception -> 0x0065 }
        r7 = "ACTIVATION_TIMESTAMP";	 Catch:{ Exception -> 0x0065 }
        r1.putString(r7, r6);	 Catch:{ Exception -> 0x0065 }
        r1.commit();	 Catch:{ Exception -> 0x0065 }
    L_0x0065:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.apps.activator.b.d.a(java.lang.String, java.lang.Long):void");
    }

    public static boolean m3672f() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r0 = com.solaredge.apps.activator.C1323a.m3615a();
        r0 = r0.m3618b();
        r1 = android.text.TextUtils.isEmpty(r0);
        r2 = 1;
        if (r1 == 0) goto L_0x0010;
    L_0x000f:
        return r2;
    L_0x0010:
        r3 = java.lang.System.currentTimeMillis();
        r1 = java.lang.Long.valueOf(r3);
        r3 = com.solaredge.common.C1366a.m3749a();
        r3 = r3.m3753b();
        r4 = "ACTIVATOR_SHARED";
        r5 = 0;
        r3 = r3.getSharedPreferences(r4, r5);
        r4 = new com.google.a.f;
        r4.<init>();
        r6 = new java.util.HashMap;
        r6.<init>();
        r7 = "ACTIVATION_TIMESTAMP";
        r8 = "";
        r3 = r3.getString(r7, r8);
        r7 = android.text.TextUtils.isEmpty(r3);	 Catch:{ Exception -> 0x0078 }
        if (r7 != 0) goto L_0x0056;	 Catch:{ Exception -> 0x0078 }
    L_0x003f:
        r6 = new com.solaredge.apps.activator.b.d$3;	 Catch:{ Exception -> 0x0078 }
        r6.<init>();	 Catch:{ Exception -> 0x0078 }
        r6 = r6.m1108b();	 Catch:{ Exception -> 0x0078 }
        r3 = r4.m1188a(r3, r6);	 Catch:{ Exception -> 0x0078 }
        r6 = r3;	 Catch:{ Exception -> 0x0078 }
        r6 = (java.util.Map) r6;	 Catch:{ Exception -> 0x0078 }
        if (r6 != 0) goto L_0x0056;	 Catch:{ Exception -> 0x0078 }
    L_0x0051:
        r6 = new java.util.HashMap;	 Catch:{ Exception -> 0x0078 }
        r6.<init>();	 Catch:{ Exception -> 0x0078 }
    L_0x0056:
        r0 = r6.get(r0);	 Catch:{ Exception -> 0x0078 }
        r0 = (com.solaredge.apps.activator.p107b.C1331d.C1330a) r0;	 Catch:{ Exception -> 0x0078 }
        if (r0 == 0) goto L_0x0077;	 Catch:{ Exception -> 0x0078 }
    L_0x005e:
        r3 = r0.f3318a;	 Catch:{ Exception -> 0x0078 }
        if (r3 != 0) goto L_0x0063;	 Catch:{ Exception -> 0x0078 }
    L_0x0062:
        goto L_0x0077;	 Catch:{ Exception -> 0x0078 }
    L_0x0063:
        r3 = r1.longValue();	 Catch:{ Exception -> 0x0078 }
        r0 = r0.f3318a;	 Catch:{ Exception -> 0x0078 }
        r0 = r0.longValue();	 Catch:{ Exception -> 0x0078 }
        r6 = r3 - r0;
        r0 = 86400000; // 0x5265c00 float:7.82218E-36 double:4.2687272E-316;
        r3 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1));
        if (r3 >= 0) goto L_0x0078;
    L_0x0076:
        return r2;
    L_0x0077:
        return r5;
    L_0x0078:
        return r5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.solaredge.apps.activator.b.d.f():boolean");
    }

    public static boolean m3669c(final Activity activity) {
        Calendar instance = Calendar.getInstance();
        Date time = instance.getTime();
        instance.set(2018, Math.max(7, 0), 1);
        Date time2 = instance.getTime();
        if (time == null || !time.after(time2)) {
            return false;
        }
        final AlertDialog create = new Builder(activity).setTitle(C1382c.m3782a().m3783a("API_Activator_Must_Update_App")).setPositiveButton(17039370, null).setCancelable(false).create();
        create.setOnShowListener(new OnShowListener() {

            /* compiled from: Utils */
            class C13281 implements View.OnClickListener {
                final /* synthetic */ C13294 f3315a;

                C13281(C13294 c13294) {
                    this.f3315a = c13294;
                }

                public void onClick(View view) {
                    C1404g.m3840a(activity);
                }
            }

            public void onShow(DialogInterface dialogInterface) {
                dialogInterface = create.getButton(-1);
                dialogInterface.setText(C1382c.m3782a().m3783a("API_Activator_Open_PlayStore_Button"));
                dialogInterface.setOnClickListener(new C13281(this));
            }
        });
        create.show();
        return true;
    }

    public static boolean m3659a(Object obj, Object obj2) {
        if (obj != obj2) {
            if (obj == null || obj.equals(obj2) == null) {
                return null;
            }
        }
        return true;
    }
}
