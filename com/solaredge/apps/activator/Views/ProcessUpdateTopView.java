package com.solaredge.apps.activator.Views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class ProcessUpdateTopView extends LinearLayout {
    private TextView f3281a;
    private MaterialProgressBar f3282b;
    private MaterialProgressBar f3283c;

    public ProcessUpdateTopView(Context context) {
        super(context);
        m3599a(context);
    }

    public ProcessUpdateTopView(Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        m3599a(context);
    }

    public ProcessUpdateTopView(Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m3599a(context);
    }

    public void m3599a(Context context) {
        context = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(2131427423, this);
        this.f3281a = (TextView) context.findViewById(2131296625);
        this.f3282b = (MaterialProgressBar) context.findViewById(2131296518);
        this.f3283c = (MaterialProgressBar) context.findViewById(2131296520);
    }

    public void m3600a(String str) {
        this.f3281a.setText(str);
    }

    public void m3602a(boolean z, int i, int i2) {
        int i3 = 8;
        this.f3283c.setVisibility(z ? 0 : 8);
        MaterialProgressBar materialProgressBar = this.f3282b;
        if (!z) {
            i3 = 0;
        }
        materialProgressBar.setVisibility(i3);
        this.f3282b.setProgress(i);
        this.f3282b.setMax(i2);
    }

    public void m3601a(boolean z) {
        m3602a(z, 0, 100);
    }

    public void m3598a(int i) {
        this.f3282b.setProgress(i);
    }
}
