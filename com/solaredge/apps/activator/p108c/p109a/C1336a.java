package com.solaredge.apps.activator.p108c.p109a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.solaredge.common.p114e.C1382c;

/* compiled from: LocalesListAdapter */
public class C1336a extends ArrayAdapter<String> {
    private String[] f3326a;
    private int f3327b;
    private int f3328c;

    /* compiled from: LocalesListAdapter */
    static class C1335a {
        public TextView f3325a;

        C1335a() {
        }
    }

    public C1336a(Context context, int i, int i2, String[] strArr) {
        super(context, i, i2, strArr);
        this.f3326a = strArr;
        this.f3327b = i;
        this.f3328c = i2;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.f3327b, null);
            viewGroup = new C1335a();
            viewGroup.f3325a = (TextView) view.findViewById(this.f3328c);
            view.setTag(viewGroup);
        } else {
            viewGroup = (C1335a) view.getTag();
        }
        viewGroup = viewGroup.f3325a;
        C1382c a = C1382c.m3782a();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("API_");
        stringBuilder.append(this.f3326a[i]);
        viewGroup.setText(a.m3783a(stringBuilder.toString()));
        return view;
    }
}
