package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class ConfigurationCompleteActivity extends AppCompatActivity {

    class C12671 implements OnClickListener {
        final /* synthetic */ ConfigurationCompleteActivity f3222a;

        C12671(ConfigurationCompleteActivity configurationCompleteActivity) {
            this.f3222a = configurationCompleteActivity;
        }

        public void onClick(View view) {
            this.f3222a.startActivity(new Intent(this.f3222a, InverterCommissioningActivity.class));
            this.f3222a.finish();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427358);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(2131296313)).setOnClickListener(new C12671(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        return true;
    }
}
