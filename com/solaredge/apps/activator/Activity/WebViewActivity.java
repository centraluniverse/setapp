package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.URLUtil;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.solaredge.apps.activator.Activity.C1317a.C1316a;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1398a;

public class WebViewActivity extends BaseActivatorActivity implements C1316a {
    private WebView f6816g;
    private boolean f6817h = false;
    private ProgressBar f6818i;
    private C1317a f6819j = new C1317a(this);
    private TextView f6820k;

    class C13021 extends WebViewClient {
        final /* synthetic */ WebViewActivity f3257a;

        C13021(WebViewActivity webViewActivity) {
            this.f3257a = webViewActivity;
        }

        public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
            super.onReceivedHttpError(webView, webResourceRequest, webResourceResponse);
            this.f3257a.f6817h = true;
            webView = "WebViewActivity onReceivedHttpError";
            if (webResourceResponse != null && VERSION.SDK_INT >= 21) {
                webResourceRequest = new StringBuilder();
                webResourceRequest.append(webView);
                webResourceRequest.append(" : ");
                webResourceRequest.append(webResourceResponse.getReasonPhrase());
                webView = webResourceRequest.toString();
            }
            C1398a.m3831a(webView);
        }

        public void onPageFinished(WebView webView, String str) {
            if (this.f3257a.f6817h == null) {
                webView.setVisibility(null);
                if (this.f3257a.f6818i != null) {
                    this.f3257a.f6818i.setVisibility(8);
                }
            }
        }
    }

    class C13032 implements OnClickListener {
        final /* synthetic */ WebViewActivity f3258a;

        C13032(WebViewActivity webViewActivity) {
            this.f3258a = webViewActivity;
        }

        public void onClick(View view) {
            if (this.f3258a.f6819j != null) {
                this.f3258a.f6819j.m3583b();
            }
            InverterActivator.m3590a().m3596a("ACTIVATE_ANOTHER_DEVICE_PRESSED", this.f3258a.mo2818c());
            this.f3258a.f6820k.setEnabled(false);
            C1331d.m3663b(this.f3258a);
        }
    }

    protected String mo2818c() {
        return "WebView";
    }

    public void onBackPressed() {
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C1398a.m3831a("WebViewActivity Started..");
        bundle = null;
        this.f6817h = false;
        setContentView(2131427441);
        this.f6816g = (WebView) findViewById(2131296689);
        this.f6816g.setWebViewClient(new C13021(this));
        this.f6816g.getSettings().setJavaScriptEnabled(true);
        this.f6818i = (ProgressBar) findViewById(2131296512);
        this.f6820k = (TextView) findViewById(2131296541);
        String str = "";
        if (getIntent() != null) {
            str = getIntent().getStringExtra("WEB_VIEW_URL");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("WebViewActivity: URL Received: ");
            stringBuilder.append(str);
            C1398a.m3831a(stringBuilder.toString());
            if (!TextUtils.isEmpty(str) && URLUtil.isValidUrl(str)) {
                this.f6816g.loadUrl(str);
                if (bundle != null) {
                    bundle = new StringBuilder();
                    bundle.append("URL invalid: ");
                    bundle.append(str);
                    bundle = bundle.toString();
                    C1398a.m3831a(bundle);
                    C1395g.m3824a().m3825a(bundle, 1);
                    finish();
                }
                this.f6820k.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Button"));
                this.f6820k.setOnClickListener(new C13032(this));
            }
        }
        bundle = 1;
        if (bundle != null) {
            bundle = new StringBuilder();
            bundle.append("URL invalid: ");
            bundle.append(str);
            bundle = bundle.toString();
            C1398a.m3831a(bundle);
            C1395g.m3824a().m3825a(bundle, 1);
            finish();
        }
        this.f6820k.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Button"));
        this.f6820k.setOnClickListener(new C13032(this));
    }

    public void mo2817b() {
        C1331d.m3650a((Context) this);
    }

    protected void onStart() {
        super.onStart();
        this.f6819j.m3582a();
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.f6819j != null) {
            this.f6819j.m3583b();
        }
    }
}
