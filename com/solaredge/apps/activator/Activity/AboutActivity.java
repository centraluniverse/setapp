package com.solaredge.apps.activator.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.widget.TextView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p114e.C1382c;

public class AboutActivity extends AppCompatActivity {
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427354);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        FirebaseAnalytics.getInstance(InverterActivator.m3590a()).setCurrentScreen(this, "About", getClass().getSimpleName());
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(C1382c.m3782a().m3783a("API_About"));
        try {
            bundle = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            int i = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            ((TextView) findViewById(2131296268)).setText(bundle);
            ((TextView) findViewById(2131296267)).setText(Integer.toString(i));
        } catch (Bundle bundle2) {
            bundle2.printStackTrace();
        }
        ((TextView) findViewById(2131296266)).setText(Html.fromHtml(C1382c.m3782a().m3783a("API_About_Terms_Link")));
        ((TextView) findViewById(2131296266)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView) findViewById(2131296264)).setText(Html.fromHtml(C1382c.m3782a().m3783a("API_About_Privacy_Link")));
        ((TextView) findViewById(2131296264)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView) findViewById(2131296262)).setVisibility(8);
    }

    protected void onResume() {
        super.onResume();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }
}
