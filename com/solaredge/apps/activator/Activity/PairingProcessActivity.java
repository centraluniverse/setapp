package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class PairingProcessActivity extends AppCompatActivity {
    AnimationDrawable f6669a;
    ImageView f6670b;
    private Handler f6671c = new Handler();
    private Runnable f6672d = new C12791(this);
    private Toolbar f6673e;
    private TextView f6674f;

    class C12791 implements Runnable {
        final /* synthetic */ PairingProcessActivity f3234a;

        C12791(PairingProcessActivity pairingProcessActivity) {
            this.f3234a = pairingProcessActivity;
        }

        public void run() {
            this.f3234a.f6669a.stop();
            this.f3234a.startActivity(new Intent(this.f3234a, PairingCompleteActivity.class));
            this.f3234a.finish();
        }
    }

    class C12802 implements OnClickListener {
        final /* synthetic */ PairingProcessActivity f3235a;

        C12802(PairingProcessActivity pairingProcessActivity) {
            this.f3235a = pairingProcessActivity;
        }

        public void onClick(View view) {
            this.f3235a.startActivity(new Intent(this.f3235a, InverterCommissioningActivity.class));
            this.f3235a.finish();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427370);
        this.f6673e = (Toolbar) findViewById(C1375d.toolbar);
        setSupportActionBar(this.f6673e);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.f6670b = (ImageView) findViewById(2131296523);
        this.f6670b.setBackgroundResource(2131231079);
        this.f6674f = (TextView) findViewById(2131296315);
        this.f6674f.setOnClickListener(new C12802(this));
    }

    protected void onStop() {
        super.onStop();
        if (this.f6671c != null) {
            this.f6671c.removeCallbacks(this.f6672d);
        }
        if (this.f6669a != null) {
            this.f6669a.stop();
        }
    }

    protected void onStart() {
        super.onStart();
        this.f6669a = (AnimationDrawable) this.f6670b.getBackground();
        this.f6669a.start();
        if (this.f6671c != null) {
            this.f6671c.removeCallbacks(this.f6672d);
            this.f6671c.postDelayed(this.f6672d, 8500);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        return true;
    }
}
