package com.solaredge.apps.activator.Activity;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.p075c.C1043a;
import com.journeyapps.barcodescanner.C2163i;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p111b.C1367a;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1404g;
import com.solaredge.common.p137i.C2193a;
import java.util.Arrays;
import java.util.Collection;

public class ScanSerialActivity extends BaseActivatorActivity implements C1367a {
    private boolean f6767g = false;
    private Handler f6768h = new Handler();
    private TextView f6769i;
    private TextView f6770j;
    private RelativeLayout f6771k;
    private LinearLayout f6772l;
    private TextView f6773m;
    private C2193a f6774n;
    private DecoratedBarcodeView f6775o;
    private int f6776p;

    class C12831 implements OnClickListener {
        final /* synthetic */ ScanSerialActivity f3238a;

        C12831(ScanSerialActivity scanSerialActivity) {
            this.f3238a = scanSerialActivity;
        }

        public void onClick(View view) {
            this.f3238a.m8912b();
        }
    }

    class C12842 implements OnClickListener {
        final /* synthetic */ ScanSerialActivity f3239a;

        C12842(ScanSerialActivity scanSerialActivity) {
            this.f3239a = scanSerialActivity;
        }

        public void onClick(View view) {
            this.f3239a.m8912b();
        }
    }

    class C12853 implements OnClickListener {
        final /* synthetic */ ScanSerialActivity f3240a;

        C12853(ScanSerialActivity scanSerialActivity) {
            this.f3240a = scanSerialActivity;
        }

        public void onClick(View view) {
            this.f3240a.startActivity(new Intent(this.f3240a, WriteSerialActivity.class));
        }
    }

    class C12864 implements AnimatorUpdateListener {
        final /* synthetic */ ScanSerialActivity f3241a;

        C12864(ScanSerialActivity scanSerialActivity) {
            this.f3241a = scanSerialActivity;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            Integer num = (Integer) valueAnimator.getAnimatedValue();
            this.f3241a.f6771k.getLayoutParams().height = num.intValue();
            this.f3241a.f6771k.requestLayout();
        }
    }

    class C12875 implements Runnable {
        final /* synthetic */ ScanSerialActivity f3242a;

        C12875(ScanSerialActivity scanSerialActivity) {
            this.f3242a = scanSerialActivity;
        }

        public void run() {
            C1395g.m3824a().m3825a(C1382c.m3782a().m3783a("API_Activator_Invalid_QR"), 0);
            this.f3242a.m8909f();
        }
    }

    protected String mo2818c() {
        return "Scan Serial";
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427374);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        boolean z = getSharedPreferences("WELCOME_SCREEN", 0).getBoolean("DO_NOT_SHOW_CHECK_BOX", false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(z ^ true);
        TextView textView = (TextView) findViewById(2131296585);
        CharSequence spannableString = new SpannableString(textView.getText());
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(2131099682)), 0, 2, 0);
        textView.setText(spannableString);
        this.f6776p = (int) C1404g.m3837a(250.0f, (Context) this);
        this.f6769i = (TextView) findViewById(2131296545);
        this.f6770j = (TextView) findViewById(2131296544);
        this.f6773m = (TextView) findViewById(2131296424);
        this.f6771k = (RelativeLayout) findViewById(2131296543);
        this.f6772l = (LinearLayout) findViewById(2131296542);
        this.f6773m.setOnClickListener(new C12831(this));
        this.f6772l.setOnClickListener(new C12842(this));
        this.f6775o = (DecoratedBarcodeView) findViewById(2131296370);
        this.f6775o.setStatusText("");
        this.f6775o.getViewFinder().setVisibility(8);
        Collection asList = Arrays.asList(new C1043a[]{C1043a.QR_CODE, C1043a.CODE_39});
        if (this.f6775o.getBarcodeView() != null) {
            this.f6775o.getBarcodeView().setDecoderFactory(new C2163i(asList));
        }
        this.f6774n = new C2193a(this, this.f6775o, this);
        this.f6774n.m3526a(getIntent(), bundle);
        textView.setOnClickListener(new C12853(this));
        mo2816a();
    }

    private synchronized void m8908e() {
        if (!this.f6767g) {
            this.f6767g = true;
            startActivityForResult(new Intent(this, SearchingWIFIActivity.class), 100);
        }
    }

    protected void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.f6767g = false;
    }

    public void mo2809d() {
        m8909f();
    }

    private void m8909f() {
        if (this.f6774n != null) {
            this.f6774n.m3530c();
            this.f6774n.m3529b();
        }
    }

    protected void onResume() {
        super.onResume();
        if (C1331d.m3657a((Activity) this)) {
            m8909f();
        }
    }

    protected void onPause() {
        super.onPause();
        if (this.f6774n != null) {
            this.f6774n.m3531d();
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.f6774n != null) {
            this.f6774n.m3532e();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.f6774n != null) {
            this.f6774n.m3527a(bundle);
        }
    }

    protected void mo2816a() {
        super.mo2816a();
        this.f6769i.setText(C1382c.m3782a().m3783a("API_Activator_Scan_Serial_Title"));
        this.f6770j.setText(C1382c.m3782a().m3783a("API_Activator_Scan_Serial_Text"));
        this.f6773m.setText(C1382c.m3782a().m3783a("API_Activator_Scan_Serial_Help_Text"));
    }

    public void m8912b() {
        int height = this.f6771k.getHeight();
        if (height == 0 || height == this.f6776p) {
            int i;
            if (height == 0) {
                i = 0;
            } else {
                i = this.f6776p;
            }
            height = height == 0 ? this.f6776p : 0;
            ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{i, height});
            ofInt.setDuration(200);
            ofInt.addUpdateListener(new C12864(this));
            ofInt.start();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu = super.onCreateOptionsMenu(menu);
        this.d.setVisible(true);
        this.e.setVisible(true);
        return menu;
    }

    public void mo2828a(String str) {
        if (!this.f6767g && !TextUtils.isEmpty(str)) {
            C1331d.m3664b(str);
            if (C1323a.m3615a().m3629g()) {
                InverterActivator.m3590a().m3594a("QR_SCANNED_SUCCESSFULLY");
                m8908e();
                return;
            }
            InverterActivator.m3590a().m3596a("INVALID_QR", str);
            this.f6768h.post(new C12875(this));
        }
    }
}
