package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class ConfigurationProgressActivity extends AppCompatActivity {
    private AnimationDrawable f6645a;
    private ImageView f6646b;
    private TextView f6647c;
    private Integer f6648d = Integer.valueOf(0);
    private Handler f6649e = new Handler();
    private Handler f6650f = new Handler();
    private Runnable f6651g = new C12681(this);
    private Runnable f6652h = new C12692(this);
    private Toolbar f6653i;

    class C12681 implements Runnable {
        final /* synthetic */ ConfigurationProgressActivity f3223a;

        C12681(ConfigurationProgressActivity configurationProgressActivity) {
            this.f3223a = configurationProgressActivity;
        }

        public void run() {
            this.f3223a.f6645a.stop();
            this.f3223a.startActivity(new Intent(this.f3223a, ConfigurationCompleteActivity.class));
            this.f3223a.finish();
        }
    }

    class C12692 implements Runnable {
        final /* synthetic */ ConfigurationProgressActivity f3224a;

        C12692(ConfigurationProgressActivity configurationProgressActivity) {
            this.f3224a = configurationProgressActivity;
        }

        public void run() {
            TextView c = this.f3224a.f6647c;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(Integer.toString(this.f3224a.f6648d.intValue()));
            stringBuilder.append(" / 10 inverters");
            c.setText(stringBuilder.toString());
            this.f3224a.f6648d;
            this.f3224a.f6648d = Integer.valueOf(this.f3224a.f6648d.intValue() + 1);
            this.f3224a.f6650f.postDelayed(this, 950);
        }
    }

    class C12703 implements OnClickListener {
        final /* synthetic */ ConfigurationProgressActivity f3225a;

        C12703(ConfigurationProgressActivity configurationProgressActivity) {
            this.f3225a = configurationProgressActivity;
        }

        public void onClick(View view) {
            if (this.f3225a.f6645a != null) {
                this.f3225a.f6645a.stop();
            }
            this.f3225a.startActivity(new Intent(this.f3225a, ConfigurationCompleteActivity.class));
            this.f3225a.finish();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427359);
        this.f6653i = (Toolbar) findViewById(C1375d.toolbar);
        setSupportActionBar(this.f6653i);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.f6646b = (ImageView) findViewById(2131296523);
        this.f6646b.setBackgroundResource(2131230883);
        this.f6646b.setOnClickListener(new C12703(this));
        this.f6647c = (TextView) findViewById(2131296354);
    }

    protected void onStart() {
        super.onStart();
        this.f6645a = (AnimationDrawable) this.f6646b.getBackground();
        this.f6645a.start();
        if (this.f6649e != null) {
            this.f6649e.removeCallbacks(this.f6651g);
            this.f6649e.postDelayed(this.f6651g, 10000);
        }
        if (this.f6650f != null) {
            this.f6650f.removeCallbacks(this.f6652h);
            this.f6650f.post(this.f6652h);
        }
    }

    protected void onStop() {
        super.onStop();
        if (this.f6649e != null) {
            this.f6649e.removeCallbacks(this.f6651g);
        }
        if (this.f6650f != null) {
            this.f6650f.removeCallbacks(this.f6652h);
        }
        if (this.f6645a != null) {
            this.f6645a.stop();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
    }
}
