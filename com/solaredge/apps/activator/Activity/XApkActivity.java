package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.vending.p035a.p036a.C0568a.C0567a;
import com.google.android.vending.p069a.p070a.C1007b;
import com.google.android.vending.p069a.p070a.C1010c;
import com.google.android.vending.p069a.p070a.C1012d;
import com.google.android.vending.p069a.p070a.C1013e;
import com.google.android.vending.p069a.p070a.C1014f;
import com.google.android.vending.p069a.p070a.C1015g;
import com.google.android.vending.p069a.p070a.C1016h;
import com.solaredge.apps.activator.XAPKDownloaderService;
import com.solaredge.apps.activator.p107b.C1333f;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1398a;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class XApkActivity extends Activity implements C1014f {
    private ProgressBar f5451a;
    private TextView f5452b;
    private TextView f5453c;
    private TextView f5454d;
    private TextView f5455e;
    private TextView f5456f;
    private View f5457g;
    private View f5458h;
    private Button f5459i;
    private Button f5460j;
    private boolean f5461k;
    private int f5462l;
    private C1015g f5463m;
    private C1016h f5464n;
    private List<C1333f> f5465o = new ArrayList();
    private boolean f5466p;

    class C13081 implements OnClickListener {
        final /* synthetic */ XApkActivity f3263a;

        C13081(XApkActivity xApkActivity) {
            this.f3263a = xApkActivity;
        }

        public void onClick(View view) {
            if (this.f3263a.f5461k != null) {
                this.f3263a.f5463m.mo1704c();
            } else {
                this.f3263a.f5463m.mo1703b();
            }
            this.f3263a.m6594a(this.f3263a.f5461k ^ 1);
        }
    }

    class C13092 implements OnClickListener {
        final /* synthetic */ XApkActivity f3264a;

        C13092(XApkActivity xApkActivity) {
            this.f3264a = xApkActivity;
        }

        public void onClick(View view) {
            this.f3264a.startActivity(new Intent("android.settings.WIFI_SETTINGS"));
        }
    }

    class C13103 implements OnClickListener {
        final /* synthetic */ XApkActivity f3265a;

        C13103(XApkActivity xApkActivity) {
            this.f3265a = xApkActivity;
        }

        public void onClick(View view) {
            this.f3265a.f5463m.mo1700a(1);
            this.f3265a.f5463m.mo1704c();
            this.f3265a.f5458h.setVisibility(8);
        }
    }

    class C13114 implements OnClickListener {
        final /* synthetic */ XApkActivity f3266a;

        C13114(XApkActivity xApkActivity) {
            this.f3266a = xApkActivity;
        }

        public void onClick(View view) {
            this.f3266a.finish();
        }
    }

    class C13125 implements OnClickListener {
        final /* synthetic */ XApkActivity f3267a;

        C13125(XApkActivity xApkActivity) {
            this.f3267a = xApkActivity;
        }

        public void onClick(View view) {
            this.f3267a.finish();
        }
    }

    class C13136 implements OnClickListener {
        final /* synthetic */ XApkActivity f3268a;

        C13136(XApkActivity xApkActivity) {
            this.f3268a = xApkActivity;
        }

        public void onClick(View view) {
            this.f3268a.f5466p = true;
        }
    }

    private static class C1314a extends AsyncTask<Object, C1007b, Boolean> {
        private WeakReference<XApkActivity> f3269a;
        private List<C1333f> f3270b;

        protected /* synthetic */ Object doInBackground(Object[] objArr) {
            return m3570a(objArr);
        }

        protected /* synthetic */ void onPostExecute(Object obj) {
            m3571a((Boolean) obj);
        }

        protected /* synthetic */ void onProgressUpdate(Object[] objArr) {
            m3572a((C1007b[]) objArr);
        }

        C1314a(XApkActivity xApkActivity, List<C1333f> list) {
            this.f3269a = new WeakReference(xApkActivity);
            this.f3270b = list;
        }

        protected void onPreExecute() {
            if (!(this.f3269a == null || this.f3269a.get() == null)) {
                ((XApkActivity) this.f3269a.get()).m6604e();
            }
            super.onPreExecute();
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        protected java.lang.Boolean m3570a(java.lang.Object... r36) {
            /*
            r35 = this;
            r1 = r35;
            r2 = r1.f3270b;
            r2 = r2.iterator();
        L_0x0008:
            r3 = r2.hasNext();
            if (r3 == 0) goto L_0x01d2;
        L_0x000e:
            r3 = r2.next();
            r3 = (com.solaredge.apps.activator.p107b.C1333f) r3;
            r5 = r1.f3269a;
            r5 = r5.get();
            r5 = (android.content.Context) r5;
            r6 = r3.f3320a;
            r7 = r3.f3321b;
            r5 = com.google.android.vending.p069a.p070a.C1013e.m2594a(r5, r6, r7);
            r6 = r1.f3269a;
            r6 = r6.get();
            r6 = (android.content.Context) r6;
            r7 = r3.f3322c;
            r3 = 0;
            r6 = com.google.android.vending.p069a.p070a.C1013e.m2596a(r6, r5, r7, r3);
            if (r6 != 0) goto L_0x003a;
        L_0x0035:
            r2 = java.lang.Boolean.valueOf(r3);
            return r2;
        L_0x003a:
            r6 = r1.f3269a;
            r6 = r6.get();
            r6 = (android.content.Context) r6;
            r5 = com.google.android.vending.p069a.p070a.C1013e.m2593a(r6, r5);
            r6 = 262144; // 0x40000 float:3.67342E-40 double:1.295163E-318;
            r6 = new byte[r6];
            r7 = new com.google.android.vending.a.b.a;	 Catch:{ IOException -> 0x01c7 }
            r7.<init>(r5);	 Catch:{ IOException -> 0x01c7 }
            r5 = r7.m2619a();	 Catch:{ IOException -> 0x01c7 }
            r8 = r5.length;	 Catch:{ IOException -> 0x01c7 }
            r11 = r3;
            r20 = 0;
        L_0x0057:
            if (r11 >= r8) goto L_0x0064;
        L_0x0059:
            r12 = r5[r11];	 Catch:{ IOException -> 0x01c7 }
            r12 = r12.f2387h;	 Catch:{ IOException -> 0x01c7 }
            r14 = r20 + r12;
            r11 = r11 + 1;
            r20 = r14;
            goto L_0x0057;
        L_0x0064:
            r8 = r5.length;	 Catch:{ IOException -> 0x01c7 }
            r15 = r3;
            r13 = r20;
            r12 = 0;
        L_0x0069:
            if (r15 >= r8) goto L_0x01c1;
        L_0x006b:
            r4 = r5[r15];	 Catch:{ IOException -> 0x01c7 }
            r16 = -1;
            r23 = r12;
            r11 = r4.f2386g;	 Catch:{ IOException -> 0x01c7 }
            r18 = (r16 > r11 ? 1 : (r16 == r11 ? 0 : -1));
            if (r18 == 0) goto L_0x01a2;
        L_0x0077:
            r11 = r4.f2388i;	 Catch:{ IOException -> 0x01c7 }
            r3 = new java.util.zip.CRC32;	 Catch:{ IOException -> 0x01c7 }
            r3.<init>();	 Catch:{ IOException -> 0x01c7 }
            r16 = 0;
            r9 = new java.io.DataInputStream;	 Catch:{ all -> 0x0198 }
            r10 = r4.f2381b;	 Catch:{ all -> 0x0198 }
            r10 = r7.m2618a(r10);	 Catch:{ all -> 0x0198 }
            r9.<init>(r10);	 Catch:{ all -> 0x0198 }
            r16 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x0195 }
            r18 = 0;
        L_0x0091:
            r10 = (r11 > r18 ? 1 : (r11 == r18 ? 0 : -1));
            if (r10 <= 0) goto L_0x0139;
        L_0x0095:
            r10 = r6.length;	 Catch:{ all -> 0x0195 }
            r26 = r7;
            r27 = r8;
            r7 = (long) r10;	 Catch:{ all -> 0x0195 }
            r10 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1));
            if (r10 <= 0) goto L_0x00a2;
        L_0x009f:
            r7 = r6.length;	 Catch:{ all -> 0x0195 }
            r7 = (long) r7;	 Catch:{ all -> 0x0195 }
            goto L_0x00a3;
        L_0x00a2:
            r7 = r11;
        L_0x00a3:
            r7 = (int) r7;	 Catch:{ all -> 0x0195 }
            r8 = 0;
            r9.readFully(r6, r8, r7);	 Catch:{ all -> 0x0195 }
            r3.update(r6, r8, r7);	 Catch:{ all -> 0x0195 }
            r29 = r5;
            r28 = r6;
            r5 = (long) r7;	 Catch:{ all -> 0x0195 }
            r30 = r11 - r5;
            r10 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x0195 }
            r32 = r2;
            r33 = r3;
            r2 = r10 - r16;
            r24 = 0;
            r8 = (r2 > r24 ? 1 : (r2 == r24 ? 0 : -1));
            if (r8 <= 0) goto L_0x00fc;
        L_0x00c2:
            r7 = (float) r7;	 Catch:{ all -> 0x0195 }
            r2 = (float) r2;	 Catch:{ all -> 0x0195 }
            r7 = r7 / r2;
            r2 = 0;
            r3 = (r2 > r23 ? 1 : (r2 == r23 ? 0 : -1));
            if (r3 == 0) goto L_0x00d5;
        L_0x00ca:
            r3 = 1000593162; // 0x3ba3d70a float:0.005 double:4.94358707E-315;
            r3 = r3 * r7;
            r7 = 1065269330; // 0x3f7eb852 float:0.995 double:5.263129795E-315;
            r7 = r7 * r23;
            r3 = r3 + r7;
            goto L_0x00d6;
        L_0x00d5:
            r3 = r7;
        L_0x00d6:
            r7 = r13 - r5;
            r5 = (float) r7;	 Catch:{ all -> 0x0195 }
            r5 = r5 / r3;
            r5 = (long) r5;	 Catch:{ all -> 0x0195 }
            r12 = 1;
            r13 = new com.google.android.vending.p069a.p070a.C1007b[r12];	 Catch:{ all -> 0x0195 }
            r22 = new com.google.android.vending.a.a.b;	 Catch:{ all -> 0x0195 }
            r16 = r20 - r7;
            r12 = r22;
            r2 = r13;
            r13 = r20;
            r34 = r15;
            r15 = r16;
            r17 = r5;
            r19 = r3;
            r12.<init>(r13, r15, r17, r19);	 Catch:{ all -> 0x0195 }
            r5 = 0;
            r2[r5] = r22;	 Catch:{ all -> 0x0195 }
            r1.publishProgress(r2);	 Catch:{ all -> 0x0195 }
            r23 = r3;
            r13 = r7;
            goto L_0x00fe;
        L_0x00fc:
            r34 = r15;
        L_0x00fe:
            r2 = r1.f3269a;	 Catch:{ all -> 0x0195 }
            if (r2 == 0) goto L_0x0123;
        L_0x0102:
            r2 = r1.f3269a;	 Catch:{ all -> 0x0195 }
            r2 = r2.get();	 Catch:{ all -> 0x0195 }
            if (r2 == 0) goto L_0x0123;
        L_0x010a:
            r2 = r1.f3269a;	 Catch:{ all -> 0x0195 }
            r2 = r2.get();	 Catch:{ all -> 0x0195 }
            r2 = (com.solaredge.apps.activator.Activity.XApkActivity) r2;	 Catch:{ all -> 0x0195 }
            r2 = r2.m6606f();	 Catch:{ all -> 0x0195 }
            if (r2 == 0) goto L_0x0123;
        L_0x0118:
            r2 = 1;
            r2 = java.lang.Boolean.valueOf(r2);	 Catch:{ all -> 0x0195 }
            if (r9 == 0) goto L_0x0122;
        L_0x011f:
            r9.close();	 Catch:{ IOException -> 0x01c7 }
        L_0x0122:
            return r2;
        L_0x0123:
            r16 = r10;
            r18 = r24;
            r7 = r26;
            r8 = r27;
            r6 = r28;
            r5 = r29;
            r11 = r30;
            r2 = r32;
            r3 = r33;
            r15 = r34;
            goto L_0x0091;
        L_0x0139:
            r32 = r2;
            r2 = r3;
            r29 = r5;
            r28 = r6;
            r26 = r7;
            r27 = r8;
            r34 = r15;
            r24 = 0;
            r2 = r2.getValue();	 Catch:{ all -> 0x0195 }
            r5 = r4.f2386g;	 Catch:{ all -> 0x0195 }
            r7 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1));
            if (r7 == 0) goto L_0x018f;
        L_0x0152:
            r2 = "LVLDL";
            r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0195 }
            r3.<init>();	 Catch:{ all -> 0x0195 }
            r5 = "CRC does not match for entry: ";
            r3.append(r5);	 Catch:{ all -> 0x0195 }
            r5 = r4.f2381b;	 Catch:{ all -> 0x0195 }
            r3.append(r5);	 Catch:{ all -> 0x0195 }
            r3 = r3.toString();	 Catch:{ all -> 0x0195 }
            android.util.Log.e(r2, r3);	 Catch:{ all -> 0x0195 }
            r2 = "LVLDL";
            r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0195 }
            r3.<init>();	 Catch:{ all -> 0x0195 }
            r5 = "In file: ";
            r3.append(r5);	 Catch:{ all -> 0x0195 }
            r4 = r4.m2614d();	 Catch:{ all -> 0x0195 }
            r3.append(r4);	 Catch:{ all -> 0x0195 }
            r3 = r3.toString();	 Catch:{ all -> 0x0195 }
            android.util.Log.e(r2, r3);	 Catch:{ all -> 0x0195 }
            r2 = 0;
            r3 = java.lang.Boolean.valueOf(r2);	 Catch:{ all -> 0x0195 }
            if (r9 == 0) goto L_0x018e;
        L_0x018b:
            r9.close();	 Catch:{ IOException -> 0x01c7 }
        L_0x018e:
            return r3;
        L_0x018f:
            if (r9 == 0) goto L_0x01b0;
        L_0x0191:
            r9.close();	 Catch:{ IOException -> 0x01c7 }
            goto L_0x01b0;
        L_0x0195:
            r0 = move-exception;
            r2 = r0;
            goto L_0x019c;
        L_0x0198:
            r0 = move-exception;
            r2 = r0;
            r9 = r16;
        L_0x019c:
            if (r9 == 0) goto L_0x01a1;
        L_0x019e:
            r9.close();	 Catch:{ IOException -> 0x01c7 }
        L_0x01a1:
            throw r2;	 Catch:{ IOException -> 0x01c7 }
        L_0x01a2:
            r32 = r2;
            r29 = r5;
            r28 = r6;
            r26 = r7;
            r27 = r8;
            r34 = r15;
            r24 = 0;
        L_0x01b0:
            r12 = r23;
            r15 = r34 + 1;
            r7 = r26;
            r8 = r27;
            r6 = r28;
            r5 = r29;
            r2 = r32;
            r3 = 0;
            goto L_0x0069;
        L_0x01c1:
            r32 = r2;
            r2 = r32;
            goto L_0x0008;
        L_0x01c7:
            r0 = move-exception;
            r2 = r0;
            r2.printStackTrace();
            r2 = 0;
            r2 = java.lang.Boolean.valueOf(r2);
            return r2;
        L_0x01d2:
            r2 = 1;
            r2 = java.lang.Boolean.valueOf(r2);
            return r2;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.solaredge.apps.activator.Activity.XApkActivity.a.a(java.lang.Object[]):java.lang.Boolean");
        }

        protected void m3572a(C1007b... c1007bArr) {
            if (!(this.f3269a == null || this.f3269a.get() == null)) {
                ((XApkActivity) this.f3269a.get()).mo1695a(c1007bArr[0]);
            }
            super.onProgressUpdate(c1007bArr);
        }

        protected void m3571a(Boolean bool) {
            if (!(this.f3269a == null || this.f3269a.get() == null)) {
                ((XApkActivity) this.f3269a.get()).m6593a(bool);
            }
            super.onPostExecute(bool);
        }
    }

    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setFinishOnTouchOutside(false);
        this.f5465o = m6602d();
        m6597b();
        if (m6610a()) {
            m6601c();
        } else {
            try {
                Intent intent = getIntent();
                Intent intent2 = new Intent(this, getClass());
                intent2.setFlags(335544320);
                intent2.setAction(intent.getAction());
                if (intent.getCategories() != null) {
                    for (String addCategory : intent.getCategories()) {
                        intent2.addCategory(addCategory);
                    }
                }
                if (C1010c.m2580a((Context) this, PendingIntent.getActivity(this, 0, intent2, 134217728), XAPKDownloaderService.class) != null) {
                    m6597b();
                }
            } catch (Bundle bundle2) {
                C1398a.m3831a("Cannot find own package! MAYDAY!");
                bundle2.printStackTrace();
            }
        }
    }

    private void m6597b() {
        this.f5464n = C1010c.m2583a(this, XAPKDownloaderService.class);
        setContentView(2131427442);
        this.f5451a = (ProgressBar) findViewById(2131296516);
        this.f5452b = (TextView) findViewById(2131296619);
        this.f5453c = (TextView) findViewById(2131296513);
        this.f5454d = (TextView) findViewById(2131296514);
        this.f5455e = (TextView) findViewById(2131296515);
        this.f5456f = (TextView) findViewById(2131296517);
        this.f5457g = findViewById(2131296392);
        this.f5458h = findViewById(2131296310);
        this.f5459i = (Button) findViewById(2131296506);
        this.f5460j = (Button) findViewById(2131296694);
        this.f5459i.setOnClickListener(new C13081(this));
        this.f5460j.setOnClickListener(new C13092(this));
        ((Button) findViewById(2131296532)).setOnClickListener(new C13103(this));
    }

    public void mo1694a(Messenger messenger) {
        this.f5463m = C1012d.m2584a(messenger);
        this.f5463m.mo1702a(this.f5464n.mo1710a());
    }

    public void mo1693a(int i) {
        boolean z;
        boolean z2;
        m6598b(i);
        int i2 = 0;
        int i3 = 1;
        switch (i) {
            case 1:
            case 2:
            case 3:
                i = 0;
                z = i;
                z2 = true;
                break;
            case 4:
                boolean z3 = 0;
                z2 = z3;
                break;
            case 5:
                m6601c();
                return;
            case 7:
            case 12:
            case 14:
                i = 0;
                z2 = i;
                z = true;
                break;
            case 8:
            case 9:
                z2 = false;
                i = 1;
                z = i;
                i3 = z2;
                break;
            case 15:
            case 16:
            case 18:
            case 19:
                C1395g.m3824a().m3825a("Error fetching expansion files..", 1);
                setResult(0);
                return;
            default:
                i = 0;
                z2 = true;
                break;
        }
        z = z2;
        i3 = i3 != 0 ? 0 : 8;
        if (this.f5457g.getVisibility() != i3) {
            this.f5457g.setVisibility(i3);
        }
        if (i == 0) {
            i2 = 8;
        }
        if (this.f5458h.getVisibility() != i2) {
            this.f5458h.setVisibility(i2);
        }
        this.f5451a.setIndeterminate(z2);
        m6594a(z);
    }

    public void mo1695a(C1007b c1007b) {
        this.f5455e.setText(getString(C0567a.kilobytes_per_second, new Object[]{C1013e.m2589a(c1007b.f2374d)}));
        this.f5456f.setText(getString(C0567a.time_remaining, new Object[]{C1013e.m2590a(c1007b.f2373c)}));
        c1007b.f2371a = c1007b.f2371a;
        this.f5451a.setMax((int) (c1007b.f2371a >> 8));
        this.f5451a.setProgress((int) (c1007b.f2372b >> 8));
        TextView textView = this.f5454d;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Long.toString((c1007b.f2372b * 100) / c1007b.f2371a));
        stringBuilder.append("%");
        textView.setText(stringBuilder.toString());
        this.f5453c.setText(C1013e.m2591a(c1007b.f2372b, c1007b.f2371a));
    }

    private void m6601c() {
        new C1314a(this, this.f5465o).execute(new Object[]{new Object()});
        setResult(-1);
        finish();
    }

    protected void onStart() {
        if (this.f5464n != null) {
            this.f5464n.mo1711a(this);
        }
        super.onStart();
    }

    protected void onStop() {
        if (this.f5464n != null) {
            this.f5464n.mo1712b(this);
        }
        super.onStop();
    }

    protected void onDestroy() {
        super.onDestroy();
        this.f5466p = true;
        super.onDestroy();
    }

    private void m6598b(int i) {
        if (this.f5462l != i) {
            this.f5462l = i;
            this.f5452b.setText(C1013e.m2586a(i));
        }
    }

    private void m6594a(boolean z) {
        this.f5461k = z;
        this.f5459i.setText(z ? true : true);
    }

    boolean m6610a() {
        for (C1333f c1333f : this.f5465o) {
            if (!C1013e.m2596a(this, C1013e.m2594a(this, c1333f.f3320a, c1333f.f3321b), c1333f.f3322c, false)) {
                return false;
            }
        }
        return true;
    }

    private List<C1333f> m6602d() {
        List<C1333f> arrayList = new ArrayList();
        arrayList.add(new C1333f(true, 10200, 20979677));
        return arrayList;
    }

    private void m6593a(Boolean bool) {
        if (bool.booleanValue() != null) {
            this.f5457g.setVisibility(0);
            this.f5458h.setVisibility(8);
            this.f5452b.setText(2131689644);
            this.f5459i.setOnClickListener(new C13114(this));
            this.f5459i.setText(17039370);
            return;
        }
        this.f5457g.setVisibility(0);
        this.f5458h.setVisibility(8);
        this.f5452b.setText(2131689645);
        this.f5459i.setOnClickListener(new C13125(this));
        this.f5459i.setText(17039360);
    }

    private void m6604e() {
        this.f5457g.setVisibility(0);
        this.f5458h.setVisibility(8);
        this.f5452b.setText(2131689646);
        this.f5459i.setOnClickListener(new C13136(this));
        this.f5459i.setText(2131689637);
    }

    private boolean m6606f() {
        return this.f5466p;
    }
}
