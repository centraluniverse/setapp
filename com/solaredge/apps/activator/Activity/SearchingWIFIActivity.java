package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.TextView;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.p107b.C1324a;
import com.solaredge.apps.activator.p107b.C1325b;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p116g.C1398a;

public class SearchingWIFIActivity extends BaseActivatorActivity implements C1325b {
    private WifiManager f6777g;
    private boolean f6778h;
    private int f6779i = 0;
    private Long f6780j = null;
    private Handler f6781k;
    private Runnable f6782l = new C12881(this);
    private TextView f6783m;
    private TextView f6784n;
    private TextView f6785o;
    private Runnable f6786p = new C12892(this);
    private Runnable f6787q = new C12903(this);

    class C12881 implements Runnable {
        final /* synthetic */ SearchingWIFIActivity f3243a;

        C12881(SearchingWIFIActivity searchingWIFIActivity) {
            this.f3243a = searchingWIFIActivity;
        }

        public void run() {
            this.f3243a.m8918b();
        }
    }

    class C12892 implements Runnable {
        final /* synthetic */ SearchingWIFIActivity f3244a;

        C12892(SearchingWIFIActivity searchingWIFIActivity) {
            this.f3244a = searchingWIFIActivity;
        }

        public void run() {
            if (!this.f3244a.isFinishing() && !this.f3244a.f6778h) {
                C1331d.m3652a(this.f3244a);
            }
        }
    }

    class C12903 implements Runnable {
        final /* synthetic */ SearchingWIFIActivity f3245a;

        C12903(SearchingWIFIActivity searchingWIFIActivity) {
            this.f3245a = searchingWIFIActivity;
        }

        public void run() {
            if (!this.f3245a.isFinishing() && !this.f3245a.f6778h) {
                if (C1331d.m3656a()) {
                    this.f3245a.f6781k.removeCallbacks(this.f3245a.f6782l);
                    this.f3245a.f6781k.post(this.f3245a.f6782l);
                    return;
                }
                this.f3245a.m8919b(true);
            }
        }
    }

    protected String mo2818c() {
        return "Searching Wifi";
    }

    private synchronized void m8918b() {
        if (!(isFinishing() || this.f6778h)) {
            this.f6778h = true;
            startActivity(new Intent(this, WIFIConnectedActivity.class));
        }
    }

    private void m8917a(boolean z) {
        if (this.f6781k != null && !this.f6778h) {
            if (this.f6777g != null) {
                this.f6777g.startScan();
            }
            this.f6781k.removeCallbacks(this.f6787q);
            this.f6781k.postDelayed(this.f6787q, z ? 0 : 5000);
        }
    }

    private void m8919b(boolean z) {
        if (this.f6781k != null) {
            if (this.f6777g != null) {
                this.f6777g.startScan();
            }
            this.f6781k.removeCallbacks(this.f6786p);
            this.f6781k.postDelayed(this.f6786p, z ? 0 : 5000);
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f6777g = (WifiManager) InverterActivator.m3590a().getApplicationContext().getSystemService("wifi");
        C1398a.m3831a("Starting To Search For WiFi..");
        setContentView(2131427375);
        this.f6781k = new Handler();
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.f6783m = (TextView) findViewById(2131296584);
        this.f6784n = (TextView) findViewById(2131296583);
        this.f6785o = (TextView) findViewById(2131296582);
        mo2816a();
        if (C1323a.m3615a().m3630h() == null) {
            C1331d.m3663b((Activity) this);
            return;
        }
        TextView textView = (TextView) findViewById(2131296603);
        TextView textView2 = (TextView) findViewById(2131296697);
        Object b = C1323a.m3615a().m3618b();
        Object c = C1323a.m3615a().m3620c();
        if (!(TextUtils.isEmpty(c) || TextUtils.isEmpty(b))) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("S/N: ");
            stringBuilder.append(b);
            textView.setText(stringBuilder.toString());
            bundle = new StringBuilder();
            bundle.append("Wi-Fi-SSID: ");
            bundle.append(c);
            textView2.setText(bundle.toString());
        }
        if (C1331d.m3656a() == null) {
            C1331d.m3668c();
        }
    }

    protected void onStart() {
        super.onStart();
        this.f6778h = false;
        m8917a(true);
    }

    protected void onStop() {
        super.onStop();
        if (this.f6781k != null) {
            this.f6781k.removeCallbacks(this.f6782l);
            this.f6781k.removeCallbacks(this.f6786p);
            this.f6781k.removeCallbacks(this.f6787q);
        }
    }

    public void mo2825i() {
        C1398a.m3831a("onConnectionAttempt");
        this.f6780j = null;
        this.f6779i++;
        if (this.f6779i > C1324a.f3309b) {
            startActivity(new Intent(this, ConnectToWifiManuallyActivity.class));
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("   -> Attempt number: ");
        stringBuilder.append(this.f6779i);
        C1398a.m3831a(stringBuilder.toString());
        m8917a(false);
    }

    public void mo2827k() {
        C1398a.m3831a("onConnectedSuccessfully");
        this.f6779i = 0;
        this.f6780j = null;
        m8917a(true);
    }

    public void mo2826j() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't find network SSID: ");
        stringBuilder.append(C1323a.m3615a().m3620c());
        stringBuilder.append(" in Wi-Fi list...");
        C1398a.m3831a(stringBuilder.toString());
        m8924f();
        if (m8923e()) {
            Intent intent = new Intent(this, FailureActivity.class);
            intent.putExtra("ON_FAILURE", "SEARCHING_WIFI_FAILURE");
            startActivity(intent);
            return;
        }
        m8919b(false);
    }

    private boolean m8923e() {
        boolean z = false;
        if (this.f6780j == null) {
            return false;
        }
        Long valueOf = Long.valueOf(Long.valueOf(System.currentTimeMillis()).longValue() - this.f6780j.longValue());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Time elapsed since error occurred: ");
        stringBuilder.append(valueOf);
        stringBuilder.append(" ms  (timeout after: 300000 ms)");
        C1398a.m3831a(stringBuilder.toString());
        if (valueOf.longValue() > 300000) {
            z = true;
        }
        return z;
    }

    private void m8924f() {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f6780j == null || currentTimeMillis < this.f6780j.longValue()) {
            this.f6780j = Long.valueOf(currentTimeMillis);
        }
    }

    protected void mo2816a() {
        super.mo2816a();
        if (this.f6783m != null) {
            this.f6783m.setText(C1382c.m3782a().m3783a("API_Activator_Searching_For_Wifi_Title"));
        }
        if (this.f6784n != null) {
            this.f6784n.setText(C1382c.m3782a().m3783a("API_Activator_Searching_For_Wifi_Text"));
        }
        if (this.f6785o != null) {
            this.f6785o.setText(C1382c.m3782a().m3783a("API_Activator_Searching_For_Wifi_Move_And_Release"));
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        onStop();
        C1331d.m3668c();
    }
}
