package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.p107b.C1326c;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1404g;

public class BaseActivatorActivity extends AppCompatActivity implements OnRequestPermissionsResultCallback, C1326c {
    protected boolean f6639a = true;
    protected Handler f6640b = new Handler();
    protected MenuItem f6641c;
    protected MenuItem f6642d;
    protected MenuItem f6643e;
    protected boolean f6644f;

    public void mo2809d() {
    }

    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        FirebaseAnalytics.getInstance(InverterActivator.m3590a()).setCurrentScreen(this, mo2818c(), getClass().getSimpleName());
    }

    protected String mo2818c() {
        return getLocalClassName();
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        C1331d.m3649a(i, strArr, iArr, this, this);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            switch (menuItem) {
                case 2131296478:
                    startActivity(new Intent(this, AboutActivity.class));
                    return true;
                case 2131296479:
                    InverterActivator.m3590a().m3592a((Context) this);
                    finish();
                    return true;
                case 2131296480:
                    if (C1404g.m3848c().booleanValue() != null) {
                        startActivityForResult(new Intent(this, SettingsActivity.class), 2);
                    } else {
                        C1395g.m3824a().m3825a(C1382c.m3782a().m3783a("API_List_No_Internet_Connection"), 0);
                    }
                    return true;
                default:
                    return true;
            }
        }
        onBackPressed();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        this.f6641c = menu.findItem(2131296479);
        this.f6642d = menu.findItem(2131296480);
        this.f6643e = menu.findItem(2131296478);
        mo2816a();
        return true;
    }

    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra("settingsChanged", this.f6644f);
        setResult(-1, intent);
        finish();
    }

    protected void onStart() {
        super.onStart();
        if (this.f6639a) {
            C1331d.m3657a((Activity) this);
        }
    }

    protected void onStop() {
        super.onStop();
    }

    protected void mo2816a() {
        if (this.f6641c != null) {
            this.f6641c.setTitle(C1382c.m3782a().m3783a("API_Configuration_Logout"));
        }
        if (this.f6642d != null) {
            this.f6642d.setTitle(C1382c.m3782a().m3783a("API_Tab_Configuration"));
        }
        if (this.f6643e != null) {
            this.f6643e.setTitle(C1382c.m3782a().m3783a("API_About"));
        }
    }

    protected void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 2) {
            this.f6644f = intent.getBooleanExtra("settingsChanged", 0);
            if (this.f6644f != 0) {
                mo2816a();
            }
        }
    }
}
