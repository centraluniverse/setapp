package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.solaredge.apps.activator.C1337c;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class PowerControlSettingsActivity extends AppCompatActivity {
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427371);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        C1337c.m3675a(findViewById(2131296448), C1337c.m3674a(getApplicationContext(), "fonts/fontawesome-webfont.ttf"));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        overridePendingTransition(2130771989, 2130771990);
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(2130771989, 2130771990);
    }
}
