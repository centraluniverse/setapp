package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class RegionLanguageActivity extends AppCompatActivity {
    private static final String[] f6675c = new String[]{"Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus+", "Czech", "Czech Republic", "Denmark+", "Europe EN50438", "Finland", "France+", "Germany", "Greece+", "Hungary", "Ireland", "Israel+", "Italy+", "Latvia", "Lithuania", "Netherlands", "Norway", "Poland", "Portugal", "Romania", "Slovakia+", "Slovenia", "Spain+", "Sweden", "Switzerland", "Turkey+", "United Kingdom+", "USA2"};
    private static final String[] f6676d = new String[]{"English", "Deutsch", "Español", "Français", "Italiano"};
    private Button f6677a;
    private Toolbar f6678b;

    class C12821 implements OnClickListener {
        final /* synthetic */ RegionLanguageActivity f3237a;

        C12821(RegionLanguageActivity regionLanguageActivity) {
            this.f3237a = regionLanguageActivity;
        }

        public void onClick(View view) {
            this.f3237a.startActivity(new Intent(this.f3237a, InverterCommissioningActivity.class));
            this.f3237a.finish();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427373);
        this.f6678b = (Toolbar) findViewById(C1375d.toolbar);
        setSupportActionBar(this.f6678b);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((LinearLayout) findViewById(C1375d.fieldsWrapper)).requestFocus();
        Spinner spinner = (Spinner) findViewById(2131296355);
        spinner.setAdapter(new ArrayAdapter(this, 17367050, f6675c));
        Spinner spinner2 = (Spinner) findViewById(2131296452);
        spinner2.setAdapter(new ArrayAdapter(this, 17367050, f6676d));
        spinner.setSelection(f6675c.length - 1);
        spinner2.setSelection(0);
        this.f6677a = (Button) findViewById(2131296596);
        this.f6677a.setText("Continue");
        this.f6677a.setOnClickListener(new C12821(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        overridePendingTransition(2130771989, 2130771990);
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(2130771989, 2130771990);
    }
}
