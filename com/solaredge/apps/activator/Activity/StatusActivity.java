package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.solaredge.apps.activator.C1337c;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class StatusActivity extends AppCompatActivity {

    class C12921 implements OnClickListener {
        final /* synthetic */ StatusActivity f3247a;

        C12921(StatusActivity statusActivity) {
            this.f3247a = statusActivity;
        }

        public void onClick(View view) {
            this.f3247a.onBackPressed();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427377);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView textView = (TextView) findViewById(2131296291);
        LinearLayout linearLayout = (LinearLayout) findViewById(2131296620);
        textView.setTypeface(C1337c.m3674a(getApplicationContext(), "fonts/fontawesome-webfont.ttf"));
        linearLayout.setOnClickListener(new C12921(this));
        TextView textView2 = (TextView) findViewById(2131296294);
        textView = (TextView) findViewById(2131296292);
        TextView textView3 = (TextView) findViewById(2131296293);
        CharSequence spannableString = new SpannableString(textView2.getText());
        spannableString.setSpan(new ForegroundColorSpan(Color.argb(180, 55, 55, 55)), 6, 14, 0);
        spannableString.setSpan(new StyleSpan(1), 0, 6, 0);
        textView2.setText(spannableString);
        bundle = new SpannableString(textView.getText());
        bundle.setSpan(new ForegroundColorSpan(Color.argb(180, 55, 55, 55)), 8, 12, 0);
        bundle.setSpan(new StyleSpan(1), 0, 7, 0);
        textView.setText(bundle);
        bundle = new SpannableString(textView3.getText());
        bundle.setSpan(new ForegroundColorSpan(Color.argb(180, 55, 55, 55)), 7, 12, 0);
        bundle.setSpan(new StyleSpan(1), 0, 7, 0);
        textView3.setText(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        overridePendingTransition(2130771989, 2130771990);
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(2130771989, 2130771990);
    }
}
