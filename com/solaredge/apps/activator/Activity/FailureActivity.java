package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p114e.C1382c;

public class FailureActivity extends BaseActivatorActivity {
    private Button f6751g;
    private String f6752h;
    private TextView f6753i;
    private TextView f6754j;
    private TextView f6755k;
    private TextView f6756l;
    private TextView f6757m;
    private TextView f6758n;
    private TextView f6759o;
    private LinearLayout f6760p;

    class C12751 implements OnClickListener {
        final /* synthetic */ FailureActivity f3230a;

        C12751(FailureActivity failureActivity) {
            this.f3230a = failureActivity;
        }

        public void onClick(View view) {
            InverterActivator.m3590a().m3594a("FAILURE_TRY_AGAIN_PRESSED");
            this.f3230a.f6751g.setEnabled(false);
            view = new Intent(this.f3230a, SearchingWIFIActivity.class);
            view.addFlags(67108864);
            this.f3230a.startActivity(view);
            this.f3230a.finish();
        }
    }

    protected String mo2818c() {
        return "Failure Screen";
    }

    public void onBackPressed() {
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427360);
        if (C1323a.m3615a().m3630h() == null) {
            C1331d.m3663b((Activity) this);
            return;
        }
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.f6751g = (Button) findViewById(2131296676);
        this.f6751g.setOnClickListener(new C12751(this));
        this.f6753i = (TextView) findViewById(2131296624);
        this.f6754j = (TextView) findViewById(2131296508);
        this.f6756l = (TextView) findViewById(2131296445);
        this.f6757m = (TextView) findViewById(2131296441);
        this.f6755k = (TextView) findViewById(2131296442);
        this.f6758n = (TextView) findViewById(2131296443);
        this.f6759o = (TextView) findViewById(2131296444);
        this.f6760p = (LinearLayout) findViewById(2131296531);
        if (getIntent() != null) {
            this.f6752h = getIntent().getStringExtra("ON_FAILURE");
            InverterActivator.m3590a().m3596a("FAILURE_OCCURRED", this.f6752h);
        }
        mo2816a();
    }

    protected void mo2816a() {
        super.mo2816a();
        this.f6754j.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Please_Reset_Inverter"));
        this.f6756l.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Instructions_Title"));
        this.f6757m.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Instructions_1"));
        this.f6755k.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Instructions_2"));
        this.f6758n.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Instructions_3"));
        this.f6759o.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Instructions_4"));
        this.f6751g.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Try_Again"));
        if (!TextUtils.isEmpty(this.f6752h)) {
            if ("UPGRADING_FAILURE".equals(this.f6752h)) {
                this.f6753i.setText(C1382c.m3782a().m3783a("API_Activator_Failure_FW_Update_Failed"));
            } else if ("SEARCHING_WIFI_FAILURE".equals(this.f6752h)) {
                this.f6753i.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Network_Not_Found"));
            } else if ("LOST_WIFI_FAILURE".equals(this.f6752h)) {
                this.f6753i.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Network_Lost"));
                this.f6751g.setText(C1382c.m3782a().m3783a("API_Activator_Failure_Reconnect"));
                this.f6760p.setVisibility(8);
            }
        }
    }
}
