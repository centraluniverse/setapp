package com.solaredge.apps.activator.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.C1334b;
import com.solaredge.apps.activator.C1343g;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.UploadingService;
import com.solaredge.apps.activator.Views.ProcessUpdateTopView;
import com.solaredge.apps.activator.p107b.C1324a;
import com.solaredge.apps.activator.p107b.C1325b;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.apps.activator.proto.commissioning.Description;
import com.solaredge.apps.activator.proto.commissioning.Description.ActivationMetadata;
import com.solaredge.apps.activator.proto.commissioning.DeviceIdentity;
import com.solaredge.apps.activator.proto.commissioning.FileType;
import com.solaredge.apps.activator.proto.commissioning.Status;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1398a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import p021c.C0560v;
import p021c.ab;
import p021c.ad;
import p125d.C1624f;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatingActivity extends BaseActivatorActivity implements C1318b, C1325b {
    private C1343g f6788g;
    private ProcessUpdateTopView f6789h;
    private BroadcastReceiver f6790i;
    private TextView f6791j;
    private TextView f6792k;
    private Button f6793l;
    private TextView f6794m;
    private Call<ad> f6795n;
    private Call<ad> f6796o;
    private int f6797p;
    private int f6798q = 0;
    private Runnable f6799r = new C12931(this);

    class C12931 implements Runnable {
        final /* synthetic */ UpdatingActivity f3248a;

        C12931(UpdatingActivity updatingActivity) {
            this.f3248a = updatingActivity;
        }

        public void run() {
            this.f3248a.m8954m();
        }
    }

    class C12942 implements OnClickListener {
        final /* synthetic */ UpdatingActivity f3249a;

        C12942(UpdatingActivity updatingActivity) {
            this.f3249a = updatingActivity;
        }

        public void onClick(View view) {
            InverterActivator.m3590a().m3596a("ACTIVATE_ANOTHER_DEVICE_PRESSED", this.f3249a.mo2818c());
            this.f3249a.f6793l.setEnabled(false);
            C1331d.m3663b(this.f3249a);
        }
    }

    class C12953 extends BroadcastReceiver {
        final /* synthetic */ UpdatingActivity f3250a;

        C12953(UpdatingActivity updatingActivity) {
            this.f3250a = updatingActivity;
        }

        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int intExtra = intent.getIntExtra(UploadingService.f5475f, -1);
                int intExtra2 = intent.getIntExtra(UploadingService.f5476g, -1);
                if (intExtra == -1 || intExtra2 == -1) {
                    context = intent.getStringExtra(UploadingService.f5474e);
                    if (context != null) {
                        if (UploadingService.f5471b.equals(context)) {
                            this.f3250a.f6788g.m3709h();
                        } else if (UploadingService.f5472c.equals(context)) {
                            this.f3250a.f6788g.m3708g();
                            this.f3250a.m8959r();
                        } else if (UploadingService.f5473d.equals(context) != null) {
                            this.f3250a.f6788g.m3709h();
                            intExtra = intent.getIntExtra(UploadingService.f5477h, -1);
                            if (intExtra > null) {
                                C1398a.m3831a("All of the files were uploaded successfully.");
                                if (intent.getBooleanExtra(UploadingService.f5478i, false) != null) {
                                    C1331d.m3655a(C1323a.m3615a().m3618b(), Long.valueOf(System.currentTimeMillis()));
                                }
                                this.f3250a.m8945c(intExtra);
                            }
                        }
                    }
                } else {
                    if (intExtra2 == ((Integer) C1343g.f3343c.get(FileType.PORTIA)).intValue()) {
                        this.f3250a.f6789h.m3598a(intExtra);
                    }
                }
            }
        }
    }

    class C12967 implements Runnable {
        final /* synthetic */ UpdatingActivity f3251a;

        C12967(UpdatingActivity updatingActivity) {
            this.f3251a = updatingActivity;
        }

        public void run() {
            this.f3251a.m8961t();
        }
    }

    class C21674 implements Callback<ad> {
        final /* synthetic */ UpdatingActivity f5446a;

        C21674(UpdatingActivity updatingActivity) {
            this.f5446a = updatingActivity;
        }

        public void onResponse(Call<ad> call, Response<ad> response) {
            if (this.f5446a.isFinishing() == null) {
                if (response.isSuccessful() != null) {
                    try {
                        DeviceIdentity deviceIdentity = (DeviceIdentity) DeviceIdentity.ADAPTER.decode(((ad) response.body()).bytes());
                        if (deviceIdentity != null) {
                            C1343g.m3692c(deviceIdentity.controllers);
                            if (deviceIdentity.serial_number != null) {
                                if (Pattern.compile(Pattern.quote(deviceIdentity.serial_number), 2).matcher(C1323a.m3615a().m3618b()).find() != null) {
                                    C1343g.m3687a(deviceIdentity);
                                    if (UploadingService.f5479j != null) {
                                        C1398a.m3831a("UploadingService already running.. Skipping");
                                        this.f5446a.m8943b((boolean) null);
                                        return;
                                    }
                                    if (this.f5446a.f6788g.m3711j() == null) {
                                        if (this.f5446a.f6788g.m3712k() == null) {
                                            call = new StringBuilder();
                                            call.append("GetIdentity not suppose to get called under state: ");
                                            call.append(this.f5446a.f6788g.m3704c());
                                            C1398a.m3831a(call.toString());
                                        }
                                    }
                                    List a = this.f5446a.f6788g.m3699a(deviceIdentity.controllers);
                                    C1343g.m3690b(a);
                                    if (this.f5446a.f6788g.m3711j()) {
                                        C1398a.m3831a("GetIdentity: IDLE State");
                                        List a2 = C1343g.m3685a(a, deviceIdentity.is_activated, new ArrayList());
                                        if (a2.isEmpty() == null) {
                                            this.f5446a.m8935a(a2);
                                        } else {
                                            this.f5446a.m8956o();
                                        }
                                    } else if (this.f5446a.f6788g.m3712k()) {
                                        C1398a.m3831a("GetIdentity: Uploading State");
                                        this.f5446a.m8942b(C1343g.m3685a(a, deviceIdentity.is_activated, this.f5446a.f6788g.m3710i()));
                                    }
                                }
                            }
                            this.f5446a.f6788g.m3702a(deviceIdentity.serial_number);
                            C1395g.m3824a().m3825a(C1382c.m3782a().m3783a("API_Activator_Serial_Validation_Failed"), 1);
                            this.f5446a.mo2824g();
                        }
                    } catch (Call<ad> call2) {
                        call2.printStackTrace();
                        this.f5446a.m8970h();
                    }
                } else {
                    call2 = new StringBuilder();
                    call2.append("GetIdentity Not successful: ");
                    call2.append(response.code());
                    call2.append(" ,");
                    call2.append(response.message());
                    C1398a.m3831a(call2.toString());
                    this.f5446a.m8970h();
                }
            }
        }

        public void onFailure(Call<ad> call, Throwable th) {
            if (call.isCanceled() == null) {
                call = new StringBuilder();
                call.append("GetIdentity: Failed -> ");
                call.append(th.getMessage());
                C1398a.m3831a(call.toString());
                this.f5446a.m8970h();
            }
        }
    }

    class C21696 implements Callback<ad> {
        final /* synthetic */ UpdatingActivity f5449a;

        C21696(UpdatingActivity updatingActivity) {
            this.f5449a = updatingActivity;
        }

        public void onResponse(Call<ad> call, Response<ad> response) {
            if (this.f5449a.isFinishing() == null) {
                if (response.isSuccessful() != null) {
                    this.f5449a.f6798q = null;
                    C1398a.m3831a("Execute: Successful. ");
                    this.f5449a.m8960s();
                } else {
                    this.f5449a.f6798q = this.f5449a.f6798q + 1;
                    call = new StringBuilder();
                    call.append("Execute: Unsuccessful.  Attempt Number: ");
                    call.append(this.f5449a.f6798q);
                    C1398a.m3831a(call.toString());
                    if (this.f5449a.f6798q <= 2) {
                        this.f5449a.m8960s();
                    } else {
                        this.f5449a.m8959r();
                    }
                }
            }
        }

        public void onFailure(Call<ad> call, Throwable th) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Execute: Failed -> ");
            stringBuilder.append(th.getMessage());
            C1398a.m3831a(stringBuilder.toString());
            if (call.isCanceled() == null) {
                this.f5449a.m8959r();
            }
        }
    }

    class C21708 implements Callback<ad> {
        final /* synthetic */ UpdatingActivity f5450a;

        C21708(UpdatingActivity updatingActivity) {
            this.f5450a = updatingActivity;
        }

        public void onResponse(Call<ad> call, Response<ad> response) {
            if (this.f5450a.isFinishing() == null && response.isSuccessful() != null) {
                try {
                    this.f5450a.f6788g.m3701a((Status) Status.ADAPTER.decode(((ad) response.body()).bytes()));
                } catch (Call<ad> call2) {
                    call2.printStackTrace();
                    this.f5450a.m8970h();
                }
            }
        }

        public void onFailure(Call<ad> call, Throwable th) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("getCurrentState: Failed  (Callback Error Message: -> ");
            stringBuilder.append(th.getMessage());
            stringBuilder.append(")");
            C1398a.m3831a(stringBuilder.toString());
            if (call.isCanceled() == null) {
                this.f5450a.m8970h();
            }
        }
    }

    protected String mo2818c() {
        return "Updating";
    }

    public void onBackPressed() {
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C1398a.m3831a("- Starting Updating Activity -");
        setContentView(2131427378);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.f6788g = new C1343g();
        this.f6788g.m3700a((C1318b) this);
        this.f6789h = (ProcessUpdateTopView) findViewById(2131296682);
        this.f6791j = (TextView) findViewById(2131296659);
        this.f6792k = (TextView) findViewById(2131296509);
        this.f6792k.setText(String.format("%s.", new Object[]{C1382c.m3782a().m3783a("API_Activator_Updating_Please_Wait")}));
        this.f6794m = (TextView) findViewById(2131296290);
        this.f6794m.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Text"));
        this.f6793l = (Button) findViewById(2131296289);
        this.f6793l.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Button"));
        this.f6793l.setOnClickListener(new C12942(this));
        this.f6790i = new C12953(this);
        if (C1323a.m3615a().m3630h() == null) {
            C1331d.m3663b((Activity) this);
        } else {
            mo2816a();
        }
    }

    public void mo2820b() {
        m8955n();
    }

    public void mo2822e() {
        m8957p();
        m8955n();
    }

    public void mo2823f() {
        m8958q();
        m8943b(false);
    }

    public void mo2819a(int i) {
        m8934a(Integer.valueOf(i));
        m8943b((boolean) 0);
    }

    public void mo2821b(int i) {
        m8941b(Integer.valueOf(i));
        m8943b((boolean) 0);
    }

    public void mo2824g() {
        C1331d.m3663b((Activity) this);
    }

    protected void onStop() {
        super.onStop();
        C1398a.m3831a("onStop Updating Activity");
        m8953l();
        this.b.removeCallbacks(this.f6799r);
        if (this.f6790i != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.f6790i);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private void m8953l() {
        if (this.f6796o != null) {
            this.f6796o.cancel();
        }
        if (this.f6795n != null) {
            this.f6795n.cancel();
        }
    }

    protected void onStart() {
        super.onStart();
        C1398a.m3831a("onStart Updating Activity");
        LocalBroadcastManager.getInstance(this).registerReceiver(this.f6790i, new IntentFilter(UploadingService.f5474e));
        m8943b(true);
    }

    private void m8954m() {
        C1398a.m3831a("starting AutoProcedure...");
        if (!isFinishing()) {
            if (this.f6788g.m3707f()) {
                C1398a.m3831a("We have waited too much time and weren't able to recover.. giving up..");
                m8959r();
            } else if (C1331d.m3656a()) {
                m8961t();
            } else {
                C1331d.m3652a((C1325b) this);
            }
        }
    }

    public void mo2825i() {
        C1398a.m3831a("onConnectionAttempt");
        this.f6797p++;
        if (this.f6797p > C1324a.f3309b) {
            startActivity(new Intent(this, ConnectToWifiManuallyActivity.class));
            finish();
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("   -> Attempt number: ");
        stringBuilder.append(this.f6797p);
        C1398a.m3831a(stringBuilder.toString());
        m8943b(false);
    }

    public void mo2826j() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't find network SSID: ");
        stringBuilder.append(C1323a.m3615a().m3620c());
        stringBuilder.append(" in Wi-Fi list...");
        C1398a.m3831a(stringBuilder.toString());
        m8970h();
    }

    public void mo2827k() {
        C1398a.m3831a("onConnectedSuccessfully");
        this.f6797p = 0;
        m8943b(true);
    }

    private void m8955n() {
        if (this.f6796o != null) {
            this.f6796o.cancel();
        }
        this.f6796o = C1365j.m3739a().m3746e().m3729b();
        this.f6796o.enqueue(new C21674(this));
    }

    private void m8956o() {
        Intent intent = new Intent(this, ProcessingActivity.class);
        intent.addFlags(65536);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    private void m8935a(final List<C1334b> list) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("prepareForUpload: \n");
        stringBuilder.append(C1331d.m3647a((List) list));
        C1398a.m3831a(stringBuilder.toString());
        try {
            C1365j.m3739a().m3746e().m3727a(ab.create(C0560v.m986a("application/x-protobuf"), Description.ADAPTER.encode(new Description(Integer.valueOf(list.size()), new ActivationMetadata(C1624f.m4821a(C1323a.m3615a().m3627f()), C1323a.m3615a().m3624e()))))).enqueue(new Callback<ad>(this) {
                final /* synthetic */ UpdatingActivity f5448b;

                public void onResponse(Call<ad> call, Response<ad> response) {
                    if (this.f5448b.isFinishing() == null) {
                        if (response.isSuccessful() != null) {
                            C1398a.m3831a("Prepare: Successful. ");
                            this.f5448b.m8942b(list);
                        } else {
                            C1398a.m3831a("Prepare: Unsuccessful. ");
                            this.f5448b.m8959r();
                        }
                    }
                }

                public void onFailure(Call<ad> call, Throwable th) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Prepare: Failed -> ");
                    stringBuilder.append(th.getMessage());
                    C1398a.m3831a(stringBuilder.toString());
                    if (call.isCanceled() == null) {
                        this.f5448b.m8959r();
                    }
                }
            });
        } catch (List<C1334b> list2) {
            list2.printStackTrace();
            stringBuilder = new StringBuilder();
            stringBuilder.append("Prepare: Failed -> ");
            stringBuilder.append(list2.getMessage());
            C1398a.m3831a(stringBuilder.toString());
            m8959r();
        }
    }

    private void m8945c(int i) {
        C1398a.m3831a("Execute...");
        try {
            C1365j.m3739a().m3746e().m3730b(ab.create(C0560v.m986a("application/x-protobuf"), Description.ADAPTER.encode(new Description(Integer.valueOf(i), new ActivationMetadata(C1624f.m4821a(C1323a.m3615a().m3627f()), C1323a.m3615a().m3624e()))))).enqueue(new C21696(this));
        } catch (int i2) {
            i2.printStackTrace();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Execute: Failed -> ");
            stringBuilder.append(i2.getMessage());
            C1398a.m3831a(stringBuilder.toString());
            m8959r();
        }
    }

    private void m8942b(List<C1334b> list) {
        if (list.isEmpty()) {
            C1398a.m3831a("-> No More Files To Upload");
            m8945c(this.f6788g.m3698a());
            return;
        }
        C1398a.m3831a("Starting uploading process...");
        m8957p();
        Intent intent = new Intent(this, UploadingService.class);
        intent.putExtra(UploadingService.f5470a, (Serializable) list);
        startService(intent);
    }

    private void m8957p() {
        this.f6789h.m3601a(false);
        if (this.f6788g.m3717p()) {
            this.f6789h.m3598a(100);
        }
        this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Uploading_Firmware"));
        this.f6792k.setVisibility(0);
        this.f6791j.setVisibility(8);
        m8936a(false);
    }

    private void m8958q() {
        this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Processing"));
        this.f6789h.m3601a(true);
        this.f6792k.setVisibility(8);
        this.f6791j.setVisibility(8);
        m8936a(false);
    }

    @SuppressLint({"DefaultLocale"})
    private void m8934a(Integer num) {
        List o = this.f6788g.m3716o();
        if (this.f6788g.m3705d() != null && o.contains(this.f6788g.m3705d().file_type)) {
            this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Processing_Installing_Firmware"));
            this.f6789h.m3601a(true);
            long max = (long) Math.max(num.intValue() / 60, 1);
            this.f6791j.setText(C1382c.m3782a().m3784a("API_Activator_Processing_Time_Left_Text", String.valueOf(max)));
            this.f6791j.setVisibility(0);
            this.f6792k.setVisibility(8);
            m8936a(true);
        }
    }

    private void m8941b(Integer num) {
        this.f6789h.m3601a(true);
        this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Activating_Inverter"));
        long max = (long) Math.max(num.intValue() / 60, 1);
        this.f6791j.setText(C1382c.m3782a().m3784a("API_Activator_Processing_Time_Left_Text", String.valueOf(max)));
        this.f6791j.setVisibility(0);
        this.f6792k.setVisibility(8);
        m8936a(true);
    }

    private void m8936a(boolean z) {
        int i = 8;
        this.f6794m.setVisibility(z ? 0 : 8);
        Button button = this.f6793l;
        if (z) {
            i = 0;
        }
        button.setVisibility(i);
    }

    private synchronized void m8959r() {
        if (!isFinishing()) {
            Intent intent = new Intent(this, FailureActivity.class);
            intent.putExtra("ON_FAILURE", "UPGRADING_FAILURE");
            startActivity(intent);
            finish();
        }
    }

    public void m8970h() {
        this.f6788g.m3708g();
        m8943b(false);
    }

    private void m8960s() {
        new Handler().postDelayed(new C12967(this), (long) C1324a.f3308a.intValue());
    }

    private void m8961t() {
        C1398a.m3831a("getCurrentState...");
        if (this.f6795n != null) {
            this.f6795n.cancel();
        }
        this.f6795n = C1365j.m3739a().m3746e().m3726a();
        this.f6795n.enqueue(new C21708(this));
    }

    private void m8943b(boolean z) {
        m8937a(z, null);
    }

    private void m8937a(boolean z, Integer num) {
        if (this.b != null) {
            this.b.removeCallbacks(this.f6799r);
            if (num == null) {
                num = C1324a.f3308a;
            }
            this.b.postDelayed(this.f6799r, z ? 0 : (long) num.intValue());
        }
    }

    protected void mo2816a() {
        super.mo2816a();
        if (this.f6788g.m3703b() != null) {
            if (!this.f6788g.m3711j()) {
                if (this.f6788g.m3712k()) {
                    this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Uploading_Firmware"));
                    this.f6792k.setText(C1382c.m3782a().m3783a("API_Activator_Uploading_Firmware_Waiting_Text"));
                } else if (this.f6788g.m3713l()) {
                    this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Processing"));
                } else if (this.f6788g.m3714m()) {
                    this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Processing_Installing_Firmware"));
                    r0 = this.f6791j;
                    if (this.f6788g.m3718q() != null) {
                        r1 = C1382c.m3782a().m3784a("API_Activator_Processing_Time_Left_Text", String.valueOf(this.f6788g.m3718q()));
                    } else {
                        r1 = "";
                    }
                    r0.setText(r1);
                } else if (this.f6788g.m3715n()) {
                    this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Activating_Inverter"));
                    r0 = this.f6791j;
                    if (this.f6788g.m3718q() != null) {
                        r1 = C1382c.m3782a().m3784a("API_Activator_Processing_Time_Left_Text", String.valueOf(this.f6788g.m3718q()));
                    } else {
                        r1 = "";
                    }
                    r0.setText(r1);
                }
                this.f6794m.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Text"));
                this.f6793l.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Button"));
            }
        }
        this.f6789h.m3600a(C1382c.m3782a().m3783a("API_Activator_Processing"));
        this.f6792k.setText(String.format("%s.", new Object[]{C1382c.m3782a().m3783a("API_Activator_Updating_Please_Wait")}));
        this.f6794m.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Text"));
        this.f6793l.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Button"));
    }
}
