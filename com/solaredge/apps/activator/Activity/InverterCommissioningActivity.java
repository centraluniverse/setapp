package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import com.solaredge.apps.activator.C1337c;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class InverterCommissioningActivity extends AppCompatActivity implements OnClickListener {
    private Context f6654a = this;
    private LinearLayout f6655b;
    private LinearLayout f6656c;
    private LinearLayout f6657d;
    private LinearLayout f6658e;
    private LinearLayout f6659f;
    private LinearLayout f6660g;
    private LinearLayout f6661h;
    private LinearLayout f6662i;

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427362);
        C1337c.m3675a(findViewById(2131296446), C1337c.m3674a(getApplicationContext(), "fonts/fontawesome-webfont.ttf"));
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.f6655b = (LinearLayout) findViewById(2131296530);
        this.f6656c = (LinearLayout) findViewById(2131296504);
        this.f6657d = (LinearLayout) findViewById(2131296343);
        this.f6658e = (LinearLayout) findViewById(2131296510);
        this.f6660g = (LinearLayout) findViewById(2131296474);
        this.f6659f = (LinearLayout) findViewById(2131296436);
        this.f6661h = (LinearLayout) findViewById(2131296344);
        this.f6662i = (LinearLayout) findViewById(2131296626);
        this.f6655b.setOnClickListener(this);
        this.f6656c.setOnClickListener(this);
        this.f6657d.setOnClickListener(this);
        this.f6658e.setOnClickListener(this);
        this.f6660g.setOnClickListener(this);
        this.f6659f.setOnClickListener(this);
        this.f6661h.setOnClickListener(this);
        this.f6662i.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case 2131296343:
                view = new Intent(this.f6654a, CommunicationSettingsActivity.class);
                break;
            case 2131296344:
                view = new Intent(this.f6654a, ConfigurationActivity.class);
                break;
            case 2131296436:
                view = new Intent(this.f6654a, InformationActivity.class);
                break;
            case 2131296474:
                view = new Intent(this.f6654a, MaintenanceActivity.class);
                break;
            case 2131296504:
                view = new Intent(this.f6654a, PairingActivity.class);
                break;
            case 2131296510:
                view = new Intent(this.f6654a, PowerControlSettingsActivity.class);
                break;
            case 2131296530:
                view = new Intent(this.f6654a, RegionLanguageActivity.class);
                break;
            case 2131296626:
                view = new Intent(this.f6654a, StatusActivity.class);
                break;
            default:
                return;
        }
        startActivity(view);
        overridePendingTransition(2130771989, 2130771990);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        return true;
    }
}
