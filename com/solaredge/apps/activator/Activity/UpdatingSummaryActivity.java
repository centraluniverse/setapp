package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.solaredge.apps.activator.Activity.C1317a.C1316a;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.p107b.C1324a;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.apps.activator.proto.general_types.ControllerType;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p116g.C1398a;

public class UpdatingSummaryActivity extends BaseActivatorActivity implements C1316a {
    private TextView f6800g;
    private TextView f6801h;
    private Button f6802i;
    private Button f6803j;
    private TextView f6804k;
    private TextView f6805l;
    private TextView f6806m;
    private C1317a f6807n = new C1317a(this);

    class C12971 implements OnClickListener {
        final /* synthetic */ UpdatingSummaryActivity f3252a;

        C12971(UpdatingSummaryActivity updatingSummaryActivity) {
            this.f3252a = updatingSummaryActivity;
        }

        public void onClick(View view) {
            if (this.f3252a.f6807n != null) {
                this.f3252a.f6807n.m3583b();
            }
            C1398a.m3831a("Start Commissioning Button pressed.");
            InverterActivator.m3590a().m3594a("START_COMMISSIONING_PRESSED");
            this.f3252a.f6802i.setEnabled(false);
            view = new StringBuilder();
            view.append(C1323a.m3615a().m3631i().replace(C1324a.f3311d, ""));
            view.append(C1323a.m3615a().m3633k());
            String stringBuilder = view.toString();
            if (C1331d.m3656a()) {
                C1331d.m3653a(stringBuilder, this.f3252a);
            } else {
                this.f3252a.mo2817b();
            }
        }
    }

    class C12982 implements OnClickListener {
        final /* synthetic */ UpdatingSummaryActivity f3253a;

        C12982(UpdatingSummaryActivity updatingSummaryActivity) {
            this.f3253a = updatingSummaryActivity;
        }

        public void onClick(View view) {
            if (this.f3253a.f6807n != null) {
                this.f3253a.f6807n.m3583b();
            }
            C1398a.m3831a("Activate Another Inverter Button pressed.");
            InverterActivator.m3590a().m3596a("ACTIVATE_ANOTHER_DEVICE_PRESSED", this.f3253a.mo2818c());
            this.f3253a.f6803j.setEnabled(false);
            C1331d.m3663b(this.f3253a);
        }
    }

    protected String mo2818c() {
        return "Updating Summary";
    }

    public void onBackPressed() {
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427440);
        C1398a.m3831a("Starting Updating Summary Activity");
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.f6802i = (Button) findViewById(2131296617);
        this.f6802i.setOnClickListener(new C12971(this));
        this.f6803j = (Button) findViewById(2131296288);
        this.f6803j.setOnClickListener(new C12982(this));
        this.f6800g = (TextView) findViewById(2131296624);
        this.f6801h = (TextView) findViewById(2131296623);
        this.f6804k = (TextView) findViewById(2131296358);
        this.f6805l = (TextView) findViewById(2131296395);
        this.f6806m = (TextView) findViewById(2131296398);
        TextView textView = (TextView) findViewById(2131296413);
        TextView textView2 = (TextView) findViewById(2131296415);
        TextView textView3 = (TextView) findViewById(2131296414);
        if (!(getIntent() == null || getIntent().getExtras() == null)) {
            Bundle extras = getIntent().getExtras();
            for (String str : extras.keySet()) {
                CharSequence string = extras.getString(str);
                if (!TextUtils.isEmpty(str) && str.equals(ControllerType.PORTIA.name())) {
                    textView.setText(string);
                } else if (!TextUtils.isEmpty(str) && str.contains("DSP1")) {
                    textView2.setText(string);
                } else if (!TextUtils.isEmpty(str) && str.contains("DSP2")) {
                    textView3.setText(string);
                }
            }
        }
        mo2816a();
    }

    protected void onStart() {
        super.onStart();
        this.f6807n.m3582a();
        this.f6802i.setEnabled(true);
    }

    protected void mo2816a() {
        super.mo2816a();
        this.f6802i.setText(C1382c.m3782a().m3783a("API_Activator_Start_Commissioning"));
        this.f6800g.setText(C1382c.m3782a().m3783a("API_Activator_Activation_Complete"));
        this.f6801h.setText(C1382c.m3782a().m3783a("API_Activator_Update_Summary_Text"));
        this.f6804k.setText(C1382c.m3782a().m3783a("API_Activator_Cpu_Version"));
        this.f6805l.setText(C1382c.m3782a().m3783a("API_Activator_Dsp1_Version"));
        this.f6806m.setText(C1382c.m3782a().m3783a("API_Activator_Dsp2_Version"));
        this.f6803j.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Button"));
    }

    public void mo2817b() {
        C1331d.m3650a((Context) this);
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.f6807n != null) {
            this.f6807n.m3583b();
        }
    }
}
