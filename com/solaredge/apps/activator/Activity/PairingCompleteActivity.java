package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class PairingCompleteActivity extends AppCompatActivity {
    private TextView f6668a;

    class C12781 implements OnClickListener {
        final /* synthetic */ PairingCompleteActivity f3233a;

        C12781(PairingCompleteActivity pairingCompleteActivity) {
            this.f3233a = pairingCompleteActivity;
        }

        public void onClick(View view) {
            this.f3233a.startActivity(new Intent(this.f3233a, InverterCommissioningActivity.class));
            this.f3233a.finish();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427369);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.f6668a = (TextView) findViewById(2131296313);
        this.f6668a.setOnClickListener(new C12781(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        return true;
    }
}
