package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class ConfigurationActivity extends AppCompatActivity {

    class C12641 implements OnClickListener {
        final /* synthetic */ ConfigurationActivity f3210a;

        C12641(ConfigurationActivity configurationActivity) {
            this.f3210a = configurationActivity;
        }

        public void onClick(View view) {
            this.f3210a.startActivity(new Intent(this.f3210a, ConfigurationProgressActivity.class));
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427357);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RadioButton radioButton = (RadioButton) findViewById(2131296526);
        RadioButton radioButton2 = (RadioButton) findViewById(2131296527);
        CheckBox checkBox = (CheckBox) findViewById(2131296336);
        CheckBox checkBox2 = (CheckBox) findViewById(2131296335);
        CheckBox checkBox3 = (CheckBox) findViewById(2131296334);
        ((Button) findViewById(2131296345)).setOnClickListener(new C12641(this));
        radioButton.setChecked(true);
        checkBox.setClickable(false);
        checkBox2.setClickable(false);
        checkBox3.setClickable(false);
        final RadioButton radioButton3 = radioButton;
        final CheckBox checkBox4 = checkBox;
        final CheckBox checkBox5 = checkBox2;
        final CheckBox checkBox6 = checkBox3;
        radioButton2.setOnClickListener(new OnClickListener(this) {
            final /* synthetic */ ConfigurationActivity f3215e;

            public void onClick(View view) {
                radioButton3.setChecked(false);
                checkBox4.setAlpha(1.0f);
                checkBox5.setAlpha(1.0f);
                checkBox6.setAlpha(1.0f);
                checkBox4.setClickable(true);
                checkBox5.setClickable(true);
                checkBox6.setClickable(true);
            }
        });
        radioButton3 = radioButton2;
        radioButton2 = radioButton;
        radioButton.setOnClickListener(new OnClickListener(this) {
            final /* synthetic */ ConfigurationActivity f3221f;

            public void onClick(View view) {
                radioButton3.setChecked(false);
                checkBox4.setAlpha(0.5f);
                checkBox5.setAlpha(0.5f);
                checkBox6.setAlpha(0.5f);
                radioButton2.setChecked(true);
                checkBox4.setClickable(false);
                checkBox5.setClickable(false);
                checkBox6.setClickable(false);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        overridePendingTransition(2130771989, 2130771990);
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(2130771989, 2130771990);
    }
}
