package com.solaredge.apps.activator.Activity;

import android.os.Handler;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p116g.C1398a;
import p021c.ad;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/* compiled from: KeepAliveManager */
public class C1317a {
    protected Handler f3272a = new Handler();
    private Call<ad> f3273b;
    private int f3274c = 0;
    private C1316a f3275d;
    private Runnable f3276e = new C13151(this);

    /* compiled from: KeepAliveManager */
    class C13151 implements Runnable {
        final /* synthetic */ C1317a f3271a;

        C13151(C1317a c1317a) {
            this.f3271a = c1317a;
        }

        public void run() {
            this.f3271a.m3580c();
        }
    }

    /* compiled from: KeepAliveManager */
    public interface C1316a {
        void mo2817b();
    }

    /* compiled from: KeepAliveManager */
    class C21712 implements Callback<ad> {
        final /* synthetic */ C1317a f5467a;

        C21712(C1317a c1317a) {
            this.f5467a = c1317a;
        }

        public void onResponse(Call<ad> call, Response<ad> response) {
            if (response.isSuccessful() != null) {
                C1398a.m3831a("   Keep Alive..");
                this.f5467a.f3274c = 0;
                this.f5467a.m3579b(false);
                return;
            }
            this.f5467a.m3581d();
        }

        public void onFailure(Call<ad> call, Throwable th) {
            if (call.isCanceled() == null) {
                call = new StringBuilder();
                call.append("   Keep Alive (getStatus) Failed  (Callback Error Message: -> ");
                call.append(th.getMessage());
                call.append(")");
                C1398a.m3831a(call.toString());
                this.f5467a.m3581d();
            }
        }
    }

    C1317a(C1316a c1316a) {
        this.f3275d = c1316a;
    }

    public void m3582a() {
        m3579b(true);
    }

    public void m3583b() {
        m3577a(false);
    }

    private void m3577a(boolean z) {
        if (!z) {
            C1398a.m3831a("   Stopping Keep Alive..");
        }
        if (this.f3272a) {
            this.f3272a.removeCallbacks(this.f3276e);
        }
        if (this.f3273b) {
            this.f3273b.cancel();
        }
    }

    private void m3579b(boolean z) {
        if (this.f3272a != null) {
            m3577a(true);
            this.f3272a.postDelayed(this.f3276e, z ? 0 : 30000);
        }
    }

    private void m3580c() {
        if (this.f3273b != null) {
            this.f3273b.cancel();
        }
        this.f3273b = C1365j.m3739a().m3746e().m3726a();
        this.f3273b.enqueue(new C21712(this));
    }

    private void m3581d() {
        this.f3274c++;
        if (this.f3274c > 2) {
            C1398a.m3831a("   Keep Alive - Giving up.");
            m3583b();
            this.f3275d.mo2817b();
            return;
        }
        C1398a.m3831a("   Keep Alive Retry..");
        m3579b(false);
    }
}
