package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p116g.C1398a;

public class WIFIConnectedActivity extends BaseActivatorActivity {
    private TextView f6808g;
    private Handler f6809h = new Handler();
    private ImageView f6810i;
    private AnimationDrawable f6811j;
    private boolean f6812k = false;
    private Runnable f6813l = new C12991(this);
    private TextView f6814m;
    private TextView f6815n;

    class C12991 implements Runnable {
        final /* synthetic */ WIFIConnectedActivity f3254a;

        C12991(WIFIConnectedActivity wIFIConnectedActivity) {
            this.f3254a = wIFIConnectedActivity;
        }

        public void run() {
            this.f3254a.m8985e();
        }
    }

    class C13002 implements OnClickListener {
        final /* synthetic */ WIFIConnectedActivity f3255a;

        C13002(WIFIConnectedActivity wIFIConnectedActivity) {
            this.f3255a = wIFIConnectedActivity;
        }

        public void onClick(View view) {
            if (this.f3255a.f6811j != null) {
                this.f3255a.f6811j.stop();
            }
            this.f3255a.startActivity(new Intent(this.f3255a, ProcessingActivity.class));
            this.f3255a.finish();
        }
    }

    class C13013 implements OnClickListener {
        final /* synthetic */ WIFIConnectedActivity f3256a;

        C13013(WIFIConnectedActivity wIFIConnectedActivity) {
            this.f3256a = wIFIConnectedActivity;
        }

        public void onClick(View view) {
            this.f3256a.m8982a(true);
        }
    }

    protected String mo2818c() {
        return "Wifi Connected";
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427380);
        if (C1323a.m3615a().m3630h() == null) {
            C1331d.m3663b((Activity) this);
            return;
        }
        C1398a.m3831a("Connected To Wifi");
        m8984b();
        if (TextUtils.isEmpty(C1323a.m3615a().m3631i()) == null) {
            C1331d.m3665b(C1323a.m3615a().m3631i(), this);
        }
        C1323a.m3615a().m3635m();
    }

    private void m8984b() {
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.f6814m = (TextView) findViewById(2131296696);
        this.f6815n = (TextView) findViewById(2131296695);
        this.f6808g = (TextView) findViewById(2131296351);
        TextView textView = (TextView) findViewById(2131296603);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("S/N: ");
        stringBuilder.append(C1323a.m3615a().m3618b());
        textView.setText(stringBuilder.toString());
        textView = (TextView) findViewById(2131296613);
        stringBuilder = new StringBuilder();
        stringBuilder.append("Wi-Fi-SSID: ");
        stringBuilder.append(C1323a.m3615a().m3620c());
        textView.setText(stringBuilder.toString());
        mo2816a();
        this.f6808g.setOnClickListener(new C13002(this));
        this.f6810i = (ImageView) findViewById(2131296653);
        this.f6810i.setBackgroundResource(2131231131);
        this.f6811j = (AnimationDrawable) this.f6810i.getBackground();
        mo2816a();
        m8982a(false);
    }

    private synchronized void m8985e() {
        if (!this.f6812k) {
            this.f6812k = true;
            startActivity(new Intent(this, ProcessingActivity.class));
            finish();
        }
    }

    private void m8982a(boolean z) {
        if (!isFinishing() && this.f6809h != null && !this.f6812k) {
            this.f6809h.removeCallbacks(this.f6813l);
            this.f6809h.postDelayed(this.f6813l, z ? 0 : 12000);
        }
    }

    protected void onStop() {
        super.onStop();
        if (this.f6809h != null) {
            this.f6809h.removeCallbacks(this.f6813l);
        }
        if (this.f6811j != null) {
            this.f6811j.stop();
        }
    }

    protected void onStart() {
        super.onStart();
        if (this.f6811j != null) {
            this.f6811j.start();
        }
        this.f6808g.setOnClickListener(new C13013(this));
    }

    protected void mo2816a() {
        super.mo2816a();
        if (this.f6814m != null) {
            this.f6814m.setText(C1382c.m3782a().m3783a("API_Activator_Wifi_Connected_Title"));
        }
        if (this.f6815n != null) {
            this.f6815n.setText(C1382c.m3782a().m3783a("API_Activator_Wifi_Connected_Text"));
        }
        if (this.f6808g != null) {
            this.f6808g.setText(C1382c.m3782a().m3783a("API_Activator_Continue"));
        }
    }

    public void onBackPressed() {
        C1331d.m3663b((Activity) this);
    }
}
