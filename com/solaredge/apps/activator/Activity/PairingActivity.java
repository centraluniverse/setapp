package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;

public class PairingActivity extends AppCompatActivity {
    private Button f6666a;
    private TextView f6667b;

    class C12761 implements OnClickListener {
        final /* synthetic */ PairingActivity f3231a;

        C12761(PairingActivity pairingActivity) {
            this.f3231a = pairingActivity;
        }

        public void onClick(View view) {
            this.f3231a.startActivity(new Intent(this.f3231a, PairingProcessActivity.class));
            this.f3231a.finish();
        }
    }

    class C12772 implements OnClickListener {
        final /* synthetic */ PairingActivity f3232a;

        C12772(PairingActivity pairingActivity) {
            this.f3232a = pairingActivity;
        }

        public void onClick(View view) {
            this.f3232a.startActivity(new Intent(this.f3232a, InverterCommissioningActivity.class));
            this.f3232a.finish();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427368);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.f6666a = (Button) findViewById(2131296618);
        this.f6666a.setText("Start Pairing");
        this.f6666a.setOnClickListener(new C12761(this));
        this.f6667b = (TextView) findViewById(2131296314);
        this.f6667b.setOnClickListener(new C12772(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
                finish();
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        overridePendingTransition(2130771989, 2130771990);
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(2130771989, 2130771990);
    }
}
