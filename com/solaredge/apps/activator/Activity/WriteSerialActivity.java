package com.solaredge.apps.activator.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.C1379d.C1377f;

public class WriteSerialActivity extends AppCompatActivity {
    EditText f6689a;
    private Toolbar f6690b;

    class C13071 implements OnEditorActionListener {
        final /* synthetic */ WriteSerialActivity f3262a;

        C13071(WriteSerialActivity writeSerialActivity) {
            this.f3262a = writeSerialActivity;
        }

        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            this.f3262a.m8818a();
            return true;
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427381);
        this.f6690b = (Toolbar) findViewById(C1375d.toolbar);
        setSupportActionBar(this.f6690b);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.f6689a = (EditText) findViewById(2131296592);
        this.f6689a.setText(C1323a.m3615a().m3618b());
        this.f6689a.setOnEditorActionListener(new C13071(this));
    }

    private void m8818a() {
        int i;
        if (this.f6689a.getText().toString().length() == 0) {
            this.f6689a.setError(getString(C1377f.lbl_err_empty_username));
            i = 0;
        } else {
            i = 1;
        }
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.f6689a.getWindowToken(), 0);
        if (i != 0) {
            startActivity(new Intent(this, SearchingWIFIActivity.class));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(2131492864, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem = menuItem.getItemId();
        if (menuItem != 16908332) {
            if (menuItem == 2131296479) {
                InverterActivator.m3590a().m3592a((Context) this);
            }
            return true;
        }
        setResult(-1, getIntent());
        finish();
        return true;
    }
}
