package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.C1343g;
import com.solaredge.apps.activator.Views.ProcessUpdateTopView;
import com.solaredge.apps.activator.p106a.C1320a;
import com.solaredge.apps.activator.p107b.C1324a;
import com.solaredge.apps.activator.p107b.C1325b;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.apps.activator.proto.commissioning.DeviceIdentity;
import com.solaredge.apps.activator.proto.commissioning.Status;
import com.solaredge.apps.activator.proto.general_types.Controller;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1398a;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import p021c.ad;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProcessingActivity extends BaseActivatorActivity implements C1318b, C1325b {
    private C1343g f6761g;
    private ProcessUpdateTopView f6762h;
    private Call<ad> f6763i;
    private Call<ad> f6764j;
    private int f6765k;
    private Runnable f6766l = new C12811(this);

    class C12811 implements Runnable {
        final /* synthetic */ ProcessingActivity f3236a;

        C12811(ProcessingActivity processingActivity) {
            this.f3236a = processingActivity;
        }

        public void run() {
            this.f3236a.m8889m();
        }
    }

    class C21642 implements Callback<ad> {
        final /* synthetic */ ProcessingActivity f5443a;

        C21642(ProcessingActivity processingActivity) {
            this.f5443a = processingActivity;
        }

        public void onResponse(Call<ad> call, Response<ad> response) {
            if (this.f5443a.isFinishing() == null) {
                if (response.isSuccessful() != null) {
                    try {
                        DeviceIdentity deviceIdentity = (DeviceIdentity) DeviceIdentity.ADAPTER.decode(((ad) response.body()).bytes());
                        if (deviceIdentity != null) {
                            C1343g.m3692c(deviceIdentity.controllers);
                            if (deviceIdentity.serial_number != null) {
                                if (Pattern.compile(Pattern.quote(deviceIdentity.serial_number), 2).matcher(C1323a.m3615a().m3618b()).find() != null) {
                                    C1343g.m3687a(deviceIdentity);
                                    if (this.f5443a.f6761g.m3711j() != null) {
                                        List a = this.f5443a.f6761g.m3699a(deviceIdentity.controllers);
                                        C1343g.m3690b(a);
                                        List a2 = C1343g.m3685a(a, deviceIdentity.is_activated, new ArrayList());
                                        if (!deviceIdentity.is_activated.booleanValue() || C1331d.m3672f()) {
                                            C1398a.m3831a("GetIdentity: IDLE State");
                                            if (a2.isEmpty() == null) {
                                                C1398a.m3831a("GetIdentity: We have files for upload, so start update activity");
                                                this.f5443a.m8893q();
                                            } else {
                                                C1398a.m3831a("GetIdentity: Nothing left to upload.. so start summary activity ");
                                                C1331d.m3655a(C1323a.m3615a().m3618b(), null);
                                                this.f5443a.m8882a(deviceIdentity.controllers);
                                            }
                                        } else {
                                            this.f5443a.m8883a(a, (List) deviceIdentity.controllers);
                                            return;
                                        }
                                    }
                                    call = new StringBuilder();
                                    call.append("GetIdentity not suppose to get called under state: ");
                                    call.append(this.f5443a.f6761g.m3704c());
                                    C1398a.m3831a(call.toString());
                                    this.f5443a.m8893q();
                                }
                            }
                            this.f5443a.f6761g.m3702a(deviceIdentity.serial_number);
                            C1395g.m3824a().m3825a(C1382c.m3782a().m3783a("API_Activator_Serial_Validation_Failed"), 1);
                            this.f5443a.mo2824g();
                        }
                    } catch (Call<ad> call2) {
                        call2.printStackTrace();
                        this.f5443a.m8902h();
                    }
                } else {
                    call2 = new StringBuilder();
                    call2.append("GetIdentity Not successful: ");
                    call2.append(response.code());
                    call2.append(" ,");
                    call2.append(response.message());
                    C1398a.m3831a(call2.toString());
                    this.f5443a.m8902h();
                }
            }
        }

        public void onFailure(Call<ad> call, Throwable th) {
            if (call.isCanceled() == null) {
                call = new StringBuilder();
                call.append("GetIdentity: Failed -> ");
                call.append(th.getMessage());
                C1398a.m3831a(call.toString());
                this.f5443a.m8902h();
            }
        }
    }

    class C21653 implements Callback<ad> {
        final /* synthetic */ ProcessingActivity f5444a;

        C21653(ProcessingActivity processingActivity) {
            this.f5444a = processingActivity;
        }

        public void onResponse(Call<ad> call, Response<ad> response) {
            if (this.f5444a.isFinishing() == null && response.isSuccessful() != null) {
                try {
                    this.f5444a.f6761g.m3701a((Status) Status.ADAPTER.decode(((ad) response.body()).bytes()));
                } catch (Call<ad> call2) {
                    call2.printStackTrace();
                    this.f5444a.m8902h();
                }
            }
        }

        public void onFailure(Call<ad> call, Throwable th) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("getCurrentState: Failed  (Callback Error Message: -> ");
            stringBuilder.append(th.getMessage());
            stringBuilder.append(")");
            C1398a.m3831a(stringBuilder.toString());
            if (call.isCanceled() == null) {
                this.f5444a.m8902h();
            }
        }
    }

    protected String mo2818c() {
        return "Processing";
    }

    public void onBackPressed() {
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C1398a.m3831a("- Starting Processing Activity -");
        setContentView(2131427372);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.f6762h = (ProcessUpdateTopView) findViewById(2131296511);
        this.f6761g = new C1343g();
        this.f6761g.m3700a((C1318b) this);
        if (C1323a.m3615a().m3630h() == null) {
            C1331d.m3663b((Activity) this);
        } else {
            mo2816a();
        }
    }

    protected void onStop() {
        super.onStop();
        C1398a.m3831a("onStop Processing Activity");
        m8888l();
        this.b.removeCallbacks(this.f6766l);
    }

    protected void onStart() {
        super.onStart();
        C1398a.m3831a("onStart Processing Activity");
        m8889m();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private void m8888l() {
        if (this.f6764j != null) {
            this.f6764j.cancel();
        }
        if (this.f6763i != null) {
            this.f6763i.cancel();
        }
    }

    private void m8889m() {
        C1398a.m3831a("Processing Activity: starting AutoProcedure...");
        if (!isFinishing()) {
            if (this.f6761g.m3707f()) {
                C1398a.m3831a("We have waited too much time and weren't able to recover.. giving up..");
                m8891o();
            } else if (C1331d.m3656a()) {
                m8892p();
            } else {
                C1331d.m3652a((C1325b) this);
            }
        }
    }

    private void m8890n() {
        if (this.f6764j != null) {
            this.f6764j.cancel();
        }
        this.f6764j = C1365j.m3739a().m3746e().m3729b();
        this.f6764j.enqueue(new C21642(this));
    }

    private void m8882a(List<Controller> list) {
        Intent intent = new Intent(this, UpdatingSummaryActivity.class);
        for (Controller controller : list) {
            intent.putExtra(controller.controller_type.name(), C1331d.m3646a(controller.version));
        }
        startActivity(intent);
        finish();
    }

    private synchronized void m8891o() {
        if (!isFinishing()) {
            Intent intent = new Intent(this, FailureActivity.class);
            intent.putExtra("ON_FAILURE", "UPGRADING_FAILURE");
            startActivity(intent);
            finish();
        }
    }

    private synchronized void m8883a(List<C1320a> list, List<Controller> list2) {
        if (!isFinishing()) {
            C1398a.m3831a("Inverter is already activated");
            Intent intent = new Intent(this, AlreadyActivatedActivity.class);
            intent.putExtra("UPDATE_AVAILABLE", list.isEmpty() ^ 1);
            for (Controller controller : list2) {
                intent.putExtra(controller.controller_type.name(), C1331d.m3646a(controller.version));
            }
            for (C1320a c1320a : list) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("NEW_FW-");
                stringBuilder.append(c1320a.m3605b().name());
                intent.putExtra(stringBuilder.toString(), c1320a.m3609f());
            }
            startActivity(intent);
            finish();
        }
    }

    private void m8884a(boolean z) {
        m8885a(z, null);
    }

    private void m8885a(boolean z, Integer num) {
        if (this.b != null) {
            this.b.removeCallbacks(this.f6766l);
            if (num == null) {
                num = C1324a.f3308a;
            }
            this.b.postDelayed(this.f6766l, z ? 0 : (long) num.intValue());
        }
    }

    private void m8892p() {
        C1398a.m3831a("getCurrentState...");
        if (this.f6763i != null) {
            this.f6763i.cancel();
        }
        this.f6763i = C1365j.m3739a().m3746e().m3726a();
        this.f6763i.enqueue(new C21653(this));
    }

    private void m8893q() {
        Intent intent = new Intent(this, UpdatingActivity.class);
        intent.addFlags(65536);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    protected void mo2816a() {
        super.mo2816a();
        this.f6762h.m3600a(C1382c.m3782a().m3783a("API_Activator_Processing"));
    }

    public void mo2820b() {
        m8890n();
    }

    public void mo2822e() {
        m8893q();
    }

    public void mo2823f() {
        m8893q();
    }

    public void mo2819a(int i) {
        m8893q();
    }

    public void mo2821b(int i) {
        m8893q();
    }

    public void mo2824g() {
        C1331d.m3663b((Activity) this);
    }

    public void m8902h() {
        this.f6761g.m3708g();
        m8884a(false);
    }

    public void mo2825i() {
        C1398a.m3831a("onConnectionAttempt");
        this.f6765k++;
        if (this.f6765k > C1324a.f3309b) {
            startActivity(new Intent(this, ConnectToWifiManuallyActivity.class));
            finish();
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("   -> Attempt number: ");
        stringBuilder.append(this.f6765k);
        C1398a.m3831a(stringBuilder.toString());
        m8884a(false);
    }

    public void mo2826j() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't find network SSID: ");
        stringBuilder.append(C1323a.m3615a().m3620c());
        stringBuilder.append(" in Wi-Fi list...");
        C1398a.m3831a(stringBuilder.toString());
        m8902h();
    }

    public void mo2827k() {
        C1398a.m3831a("onConnectedSuccessfully");
        this.f6765k = 0;
        m8884a(true);
    }
}
