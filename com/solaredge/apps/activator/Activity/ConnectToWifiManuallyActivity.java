package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.C1366a;
import com.solaredge.common.C1379d.C1373b;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p114e.C1382c;

public class ConnectToWifiManuallyActivity extends BaseActivatorActivity {
    private Button f6740g;
    private TextView f6741h;
    private TextView f6742i;
    private TextView f6743j;
    private Handler f6744k = new Handler();
    private boolean f6745l = false;
    private Runnable f6746m = new C12711(this);
    private Runnable f6747n = new C12722(this);
    private TextView f6748o;
    private TextView f6749p;
    private TextView f6750q;

    class C12711 implements Runnable {
        final /* synthetic */ ConnectToWifiManuallyActivity f3226a;

        C12711(ConnectToWifiManuallyActivity connectToWifiManuallyActivity) {
            this.f3226a = connectToWifiManuallyActivity;
        }

        public void run() {
            this.f3226a.f6745l = true;
            this.f3226a.startActivity(new Intent(this.f3226a, WIFIConnectedActivity.class));
            this.f3226a.finish();
        }
    }

    class C12722 implements Runnable {
        final /* synthetic */ ConnectToWifiManuallyActivity f3227a;

        C12722(ConnectToWifiManuallyActivity connectToWifiManuallyActivity) {
            this.f3227a = connectToWifiManuallyActivity;
        }

        public void run() {
            if (this.f3227a.isFinishing() || !C1331d.m3656a() || this.f3227a.f6745l) {
                this.f3227a.m8866a(false);
                return;
            }
            this.f3227a.f6745l = true;
            this.f3227a.f6744k.removeCallbacks(this.f3227a.f6746m);
            this.f3227a.f6744k.post(this.f3227a.f6746m);
        }
    }

    class C12733 implements OnClickListener {
        final /* synthetic */ ConnectToWifiManuallyActivity f3228a;

        C12733(ConnectToWifiManuallyActivity connectToWifiManuallyActivity) {
            this.f3228a = connectToWifiManuallyActivity;
        }

        public void onClick(View view) {
            this.f3228a.startActivity(new Intent("android.net.wifi.PICK_WIFI_NETWORK"));
        }
    }

    class C12744 implements OnClickListener {
        final /* synthetic */ ConnectToWifiManuallyActivity f3229a;

        C12744(ConnectToWifiManuallyActivity connectToWifiManuallyActivity) {
            this.f3229a = connectToWifiManuallyActivity;
        }

        public void onClick(View view) {
            this.f3229a.f6741h.setEnabled(false);
            this.f3229a.f6741h.setBackgroundColor(ContextCompat.getColor(C1366a.m3749a().m3753b(), C1373b.gray));
            this.f3229a.f6741h.setText(C1382c.m3782a().m3783a("API_Activator_Manual_Connecting_To_Wifi_Copied"));
            ((ClipboardManager) this.f3229a.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("password", C1323a.m3615a().m3622d()));
            this.f3229a.f6740g.setEnabled(true);
            this.f3229a.f6740g.setBackground(ContextCompat.getDrawable(C1366a.m3749a().m3753b(), 2131230815));
            this.f3229a.f6740g.setAlpha(1.0f);
        }
    }

    protected String mo2818c() {
        return "Connect Manually";
    }

    private void m8866a(boolean z) {
        this.f6744k.removeCallbacks(this.f6747n);
        this.f6744k.postDelayed(this.f6747n, z ? 0 : 1000);
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427367);
        if (C1323a.m3615a().m3630h() == null) {
            C1331d.m3663b((Activity) this);
            return;
        }
        this.f6748o = (TextView) findViewById(2131296348);
        this.f6749p = (TextView) findViewById(2131296346);
        this.f6750q = (TextView) findViewById(2131296347);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.f6742i = (TextView) findViewById(2131296489);
        this.f6742i.setText(C1323a.m3615a().m3620c());
        this.f6740g = (Button) findViewById(2131296351);
        this.f6740g.setOnClickListener(new C12733(this));
        this.f6741h = (TextView) findViewById(2131296353);
        this.f6741h.setOnClickListener(new C12744(this));
        this.f6743j = (TextView) findViewById(2131296505);
        this.f6743j.setText(C1323a.m3615a().m3622d());
        mo2816a();
    }

    protected void onStart() {
        super.onStart();
        if (!isFinishing() && !this.f6745l) {
            m8866a(true);
        }
    }

    protected void onStop() {
        super.onStop();
        this.f6744k.removeCallbacks(this.f6747n);
    }

    protected void mo2816a() {
        super.mo2816a();
        this.f6741h.setText(C1382c.m3782a().m3783a("API_Activator_Manual_Connecting_To_Wifi_Copy"));
        this.f6750q.setText(C1382c.m3782a().m3783a("API_Activator_Manual_Connecting_To_Wifi_Paragraph2"));
        this.f6749p.setText(C1382c.m3782a().m3783a("API_Activator_Manual_Connecting_To_Wifi_Paragraph1"));
        this.f6748o.setText(C1382c.m3782a().m3783a("API_Activator_Manual_Connecting_To_Wifi_Title"));
        this.f6740g.setText(C1382c.m3782a().m3783a("API_Activator_Continue"));
    }
}
