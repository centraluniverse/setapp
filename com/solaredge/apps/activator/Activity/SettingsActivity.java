package com.solaredge.apps.activator.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.C1379d.C1377f;
import com.solaredge.common.p110a.C1362b;
import com.solaredge.common.p110a.C1365j;
import com.solaredge.common.p113d.C1378a;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1384d;
import com.solaredge.common.p114e.C1394f;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1404g;
import p000a.p001a.p002a.C0005c;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity {
    private Typeface f6679a;
    private LinearLayout f6680b;
    private TextView f6681c;
    private TextView f6682d;
    private String f6683e;
    private boolean f6684f = false;
    private int f6685g = 0;
    private int f6686h;
    private Toolbar f6687i;
    private Callback<Void> f6688j = new C21661(this);

    class C12912 implements OnClickListener {
        final /* synthetic */ SettingsActivity f3246a;

        C12912(SettingsActivity settingsActivity) {
            this.f3246a = settingsActivity;
        }

        public void onClick(View view) {
            if (C1404g.m3848c().booleanValue() != null) {
                this.f3246a.startActivityForResult(new Intent(this.f3246a, LocaleListActivity.class), 10);
                return;
            }
            C1395g.m3824a().m3825a(C1382c.m3782a().m3783a("API_List_No_Internet_Connection"), 0);
        }
    }

    class C21661 implements Callback<Void> {
        final /* synthetic */ SettingsActivity f5445a;

        public void onResponse(Call<Void> call, Response<Void> response) {
        }

        C21661(SettingsActivity settingsActivity) {
            this.f5445a = settingsActivity;
        }

        public void onFailure(Call<Void> call, Throwable th) {
            th.fillInStackTrace();
            C1404g.m3839a();
        }
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427376);
        FirebaseAnalytics.getInstance(InverterActivator.m3590a()).setCurrentScreen(this, "Settings", getClass().getSimpleName());
        this.f6679a = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Regular.ttf");
        this.f6686h = getResources().getConfiguration().orientation;
        this.f6687i = (Toolbar) findViewById(C1375d.toolbar);
        setSupportActionBar(this.f6687i);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle((CharSequence) "");
        if (bundle != null) {
            if (bundle.containsKey("ARG_CHOSEN_LOCALE")) {
                this.f6683e = bundle.getString("ARG_CHOSEN_LOCALE");
            }
            if (bundle.containsKey("ARG_SETTINGS_CHANGED")) {
                this.f6684f = bundle.getBoolean("ARG_SETTINGS_CHANGED");
            }
        }
        C1382c.m3782a().m3789b(InverterActivator.m3590a());
        m8816a();
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("ARG_CHOSEN_LOCALE", this.f6683e);
        bundle.putBoolean("ARG_SETTINGS_CHANGED", this.f6684f);
        super.onSaveInstanceState(bundle);
    }

    private void m8816a() {
        this.f6681c = (TextView) findViewById(2131296455);
        this.f6682d = (TextView) findViewById(2131296594);
        this.f6680b = (LinearLayout) findViewById(2131296297);
        this.f6681c.setTypeface(this.f6679a);
        this.f6682d.setTypeface(this.f6679a);
        TextView textView = this.f6682d;
        C1382c a = C1382c.m3782a();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("API_");
        stringBuilder.append(C1394f.m3816a().m3817a(InverterActivator.m3590a()));
        textView.setText(a.m3783a(stringBuilder.toString()));
        m8817b();
        findViewById(2131296465).setOnClickListener(new C12912(this));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }

    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("settingsChanged", this.f6684f);
        setResult(-1, intent);
        finish();
    }

    private void m8817b() {
        this.f6681c.setText(C1382c.m3782a().m3783a("API_Configuration_Locale"));
    }

    public void onStart() {
        super.onStart();
        if (!C0005c.m3a().m17b(this)) {
            C0005c.m3a().m15a((Object) this);
        }
    }

    public void onStop() {
        if (C0005c.m3a().m17b(this)) {
            C0005c.m3a().m18c(this);
        }
        super.onStop();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1 && i == 10) {
            i = C1394f.m3816a().m3817a(this);
            this.f6683e = intent.getStringExtra("key_chosen_locale");
            if (i.equals(this.f6683e) == 0) {
                if (C1404g.m3847b() != 0) {
                    this.f6683e = i;
                    return;
                }
                this.f6684f = true;
                i = this.f6682d;
                i2 = C1382c.m3782a();
                intent = new StringBuilder();
                intent.append("API_");
                intent.append(this.f6683e);
                i.setText(i2.m3783a(intent.toString()));
                this.f6680b.setVisibility(0);
                C1362b.m3734a(C1365j.m3739a().m3747f().m3732a(this.f6683e, this.f6683e), this.f6688j);
                C1384d.m3793a().m3798a(this.f6683e);
            }
        }
    }

    public void onEvent(C1378a c1378a) {
        C0005c.m3a().m21f(c1378a);
        if (c1378a.m3774b()) {
            C1394f.m3816a().m3818a(InverterActivator.m3590a(), c1378a.m3773a());
            m8817b();
            this.f6680b.setVisibility(8);
            return;
        }
        this.f6685g++;
        if (this.f6685g <= 3) {
            C1384d.m3793a().m3798a(this.f6683e);
            return;
        }
        C1395g.m3824a().m3825a(getResources().getString(C1377f.lbl_err_translation_sync), 0);
        this.f6680b.setVisibility(8);
    }
}
