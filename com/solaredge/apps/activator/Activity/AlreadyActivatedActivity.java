package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.solaredge.apps.activator.Activity.C1317a.C1316a;
import com.solaredge.apps.activator.C1323a;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.p107b.C1324a;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.apps.activator.proto.general_types.ControllerType;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p116g.C1398a;

public class AlreadyActivatedActivity extends BaseActivatorActivity implements C1316a {
    private TextView f6725g;
    private Button f6726h;
    private Button f6727i;
    private LinearLayout f6728j;
    private TextView f6729k;
    private TextView f6730l;
    private Button f6731m;
    private TextView f6732n;
    private TextView f6733o;
    private TextView f6734p;
    private TextView f6735q;
    private TextView f6736r;
    private TextView f6737s;
    private TextView f6738t;
    private C1317a f6739u = new C1317a(this);

    class C12611 implements OnClickListener {
        final /* synthetic */ AlreadyActivatedActivity f3207a;

        C12611(AlreadyActivatedActivity alreadyActivatedActivity) {
            this.f3207a = alreadyActivatedActivity;
        }

        public void onClick(View view) {
            if (this.f3207a.f6739u != null) {
                this.f3207a.f6739u.m3583b();
            }
            C1398a.m3831a("Activate Another Inverter Button pressed.");
            InverterActivator.m3590a().m3596a("ACTIVATE_ANOTHER_DEVICE_PRESSED", this.f3207a.mo2818c());
            this.f3207a.f6731m.setEnabled(false);
            C1331d.m3663b(this.f3207a);
        }
    }

    class C12622 implements OnClickListener {
        final /* synthetic */ AlreadyActivatedActivity f3208a;

        C12622(AlreadyActivatedActivity alreadyActivatedActivity) {
            this.f3208a = alreadyActivatedActivity;
        }

        public void onClick(View view) {
            C1398a.m3831a("View Status Button pressed.");
            InverterActivator.m3590a().m3594a("VIEW_STATUS_PRESSED");
            if (this.f3208a.f6739u != null) {
                this.f3208a.f6739u.m3583b();
            }
            this.f3208a.f6726h.setEnabled(false);
            view = new StringBuilder();
            view.append(C1323a.m3615a().m3631i().replace(C1324a.f3311d, ""));
            view.append(C1323a.m3615a().m3632j());
            String stringBuilder = view.toString();
            if (C1331d.m3656a()) {
                C1331d.m3653a(stringBuilder, this.f3208a);
            } else {
                this.f3208a.mo2817b();
            }
        }
    }

    class C12633 implements OnClickListener {
        final /* synthetic */ AlreadyActivatedActivity f3209a;

        C12633(AlreadyActivatedActivity alreadyActivatedActivity) {
            this.f3209a = alreadyActivatedActivity;
        }

        public void onClick(View view) {
            if (this.f3209a.f6739u != null) {
                this.f3209a.f6739u.m3583b();
            }
            C1398a.m3831a("Update FW Button pressed.");
            InverterActivator.m3590a().m3594a("UPDATE_FIRMWARE_PRESSED");
            this.f3209a.f6727i.setEnabled(false);
            this.f3209a.startActivity(new Intent(this.f3209a, UpdatingActivity.class));
            this.f3209a.finish();
        }
    }

    protected String mo2818c() {
        return "Already Activated";
    }

    public void onBackPressed() {
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427397);
        C1398a.m3831a("Starting Already Activated Activity");
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.f6725g = (TextView) findViewById(2131296624);
        this.f6728j = (LinearLayout) findViewById(2131296681);
        this.f6732n = (TextView) findViewById(2131296417);
        this.f6730l = (TextView) findViewById(2131296439);
        this.f6729k = (TextView) findViewById(2131296491);
        this.f6734p = (TextView) findViewById(2131296356);
        this.f6733o = (TextView) findViewById(2131296393);
        this.f6735q = (TextView) findViewById(2131296396);
        this.f6738t = (TextView) findViewById(2131296357);
        this.f6737s = (TextView) findViewById(2131296394);
        this.f6736r = (TextView) findViewById(2131296397);
        this.f6731m = (Button) findViewById(2131296288);
        this.f6727i = (Button) findViewById(2131296680);
        this.f6726h = (Button) findViewById(2131296686);
        this.f6731m.setOnClickListener(new C12611(this));
        this.f6726h.setOnClickListener(new C12622(this));
        this.f6727i.setOnClickListener(new C12633(this));
        if (C1323a.m3615a().m3630h() == null) {
            C1331d.m3663b((Activity) this);
            return;
        }
        m8861e();
        mo2816a();
    }

    private void m8861e() {
        int i = 0;
        boolean booleanExtra = getIntent().getBooleanExtra("UPDATE_AVAILABLE", false);
        LinearLayout linearLayout = this.f6728j;
        if (!booleanExtra) {
            i = 8;
        }
        linearLayout.setVisibility(i);
        if (booleanExtra) {
            m8862f();
            if (getIntent() != null && getIntent().getExtras() != null) {
                Bundle extras = getIntent().getExtras();
                for (String str : extras.keySet()) {
                    if (!"UPDATE_AVAILABLE".equalsIgnoreCase(str)) {
                        CharSequence string = extras.getString(str);
                        if (!TextUtils.isEmpty(str) && !str.contains("NEW_FW-") && str.equals(ControllerType.PORTIA.name())) {
                            this.f6734p.setText(string);
                        } else if (!TextUtils.isEmpty(str) && !str.contains("NEW_FW-") && str.contains("DSP1")) {
                            this.f6733o.setText(string);
                        } else if (!TextUtils.isEmpty(str) && !str.contains("NEW_FW-") && str.contains("DSP2")) {
                            this.f6735q.setText(string);
                        } else if (!TextUtils.isEmpty(str) && str.contains("NEW_FW-") && str.contains(ControllerType.PORTIA.name())) {
                            this.f6738t.setText(string);
                        } else if (!TextUtils.isEmpty(str) && str.contains("NEW_FW-") && str.contains("DSP1")) {
                            this.f6737s.setText(string);
                        } else if (!TextUtils.isEmpty(str) && str.contains("NEW_FW-") && str.contains("DSP2")) {
                            this.f6736r.setText(string);
                        }
                    }
                }
            }
        }
    }

    private void m8862f() {
        CharSequence charSequence = "---";
        this.f6734p.setText(charSequence);
        this.f6733o.setText(charSequence);
        this.f6735q.setText(charSequence);
        this.f6738t.setText(charSequence);
        this.f6737s.setText(charSequence);
        this.f6736r.setText(charSequence);
    }

    protected void onStart() {
        super.onStart();
        this.f6739u.m3582a();
        this.f6726h.setEnabled(true);
        this.f6731m.setEnabled(true);
    }

    protected void mo2816a() {
        super.mo2816a();
        this.f6725g.setText(C1382c.m3782a().m3783a("API_Activator_Inverter_Activated"));
        this.f6729k.setText(C1382c.m3782a().m3783a("API_Activator_Inverter_Activated_New_Versions_Header"));
        this.f6730l.setText(C1382c.m3782a().m3783a("API_Activator_Inverter_Activated_Installed_Versions_Header"));
        this.f6732n.setText(C1382c.m3782a().m3783a("API_Activator_Inverter_Activated_Firmware_Versions_Table_Header"));
        this.f6727i.setText(C1382c.m3782a().m3783a("API_Activator_Inverter_Activated_Update_FW"));
        this.f6726h.setText(C1382c.m3782a().m3783a("API_Activator_Inverter_Activated_View_Status"));
        this.f6731m.setText(C1382c.m3782a().m3783a("API_Activator_Activate_Another_Inverter_Button"));
    }

    public void mo2817b() {
        C1331d.m3650a((Context) this);
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.f6739u != null) {
            this.f6739u.m3583b();
        }
    }
}
