package com.solaredge.apps.activator.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.p114e.C1382c;

public class WelcomeActivity extends BaseActivatorActivity {
    public Button f6821g;
    private CheckBox f6822h;
    private boolean f6823i = false;
    private boolean f6824j = false;
    private boolean f6825k = false;
    private TextView f6826l;
    private TextView f6827m;
    private TextView f6828n;
    private TextView f6829o;

    class C13041 implements OnClickListener {
        final /* synthetic */ WelcomeActivity f3259a;

        C13041(WelcomeActivity welcomeActivity) {
            this.f3259a = welcomeActivity;
        }

        public void onClick(View view) {
            if (C1331d.m3671e() == null) {
                this.f3259a.m9001b();
                return;
            }
            this.f3259a.f6824j = true;
            this.f3259a.f6821g.setEnabled(false);
            if (C1331d.m3657a(this.f3259a) != null) {
                this.f3259a.m8999f();
            }
        }
    }

    class C13052 implements DialogInterface.OnClickListener {
        final /* synthetic */ WelcomeActivity f3260a;

        C13052(WelcomeActivity welcomeActivity) {
            this.f3260a = welcomeActivity;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            this.f3260a.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            dialogInterface.dismiss();
        }
    }

    class C13063 implements DialogInterface.OnClickListener {
        final /* synthetic */ WelcomeActivity f3261a;

        C13063(WelcomeActivity welcomeActivity) {
            this.f3261a = welcomeActivity;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    protected String mo2818c() {
        return "Welcome";
    }

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427379);
        this.a = false;
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.f6828n = (TextView) findViewById(2131296692);
        this.f6829o = (TextView) findViewById(2131296690);
        this.f6827m = (TextView) findViewById(2131296691);
        this.f6821g = (Button) findViewById(2131296616);
        this.f6826l = (TextView) findViewById(2131296389);
        this.f6822h = (CheckBox) findViewById(2131296389);
        this.f6823i = getSharedPreferences("WELCOME_SCREEN", 0).getBoolean("DO_NOT_SHOW_CHECK_BOX", false);
        this.f6822h.setChecked(this.f6823i);
        mo2816a();
        if (C1331d.m3669c((Activity) this) == null) {
            if (C1331d.m3657a((Activity) this) == null || this.f6823i == null) {
                this.f6821g.setOnClickListener(new C13041(this));
            } else {
                m8999f();
            }
        }
    }

    public void mo2809d() {
        super.mo2809d();
        if (C1331d.m3657a((Activity) this) && this.f6824j) {
            m8999f();
        }
    }

    private void m8996a(boolean z) {
        startActivity(new Intent(this, ScanSerialActivity.class));
        if (z) {
            finish();
        }
    }

    private void m8998e() {
        Editor edit = getSharedPreferences("WELCOME_SCREEN", 0).edit();
        edit.putBoolean("DO_NOT_SHOW_CHECK_BOX", this.f6822h.isChecked());
        edit.apply();
    }

    private void m8999f() {
        m8998e();
        m8996a(this.f6822h.isChecked());
    }

    protected void onStart() {
        super.onStart();
        this.f6821g.setEnabled(true);
    }

    public void m9001b() {
        Builder builder = new Builder(this);
        builder.setMessage(C1382c.m3782a().m3783a("API_Activator_Location_Services_Not_Enabled"));
        builder.setPositiveButton(C1382c.m3782a().m3783a("API_Activator_Open_Location_Settings"), new C13052(this));
        builder.setNegativeButton(C1382c.m3782a().m3783a("API_Cancel"), new C13063(this));
        builder.show();
    }

    protected void mo2816a() {
        super.mo2816a();
        this.f6821g.setText(C1382c.m3782a().m3783a("API_Activator_Welcome_Start_Button"));
        this.f6826l.setText(C1382c.m3782a().m3783a("API_Activator_Welcome_Do_Not_Show_Again"));
        this.f6827m.setText(C1382c.m3782a().m3783a("API_Activator_Welcome_Text2"));
        this.f6829o.setText(C1382c.m3782a().m3783a("API_Activator_Welcome_Text1"));
        this.f6828n.setText(C1382c.m3782a().m3783a("API_Activator_Welcome_Title"));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu = super.onCreateOptionsMenu(menu);
        this.d.setVisible(true);
        this.e.setVisible(true);
        return menu;
    }
}
