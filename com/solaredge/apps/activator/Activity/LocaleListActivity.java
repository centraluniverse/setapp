package com.solaredge.apps.activator.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.solaredge.apps.activator.C1339d;
import com.solaredge.apps.activator.C1340e;
import com.solaredge.apps.activator.InverterActivator;
import com.solaredge.apps.activator.p108c.p109a.C1336a;
import com.solaredge.common.C1379d.C1375d;
import com.solaredge.common.models.LocaleInfo;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1384d;
import com.solaredge.common.p114e.C1394f;
import com.solaredge.common.p116g.C1403f;
import com.solaredge.common.p116g.C1404g;
import java.util.ArrayList;
import java.util.List;

public class LocaleListActivity extends AppCompatActivity implements OnItemClickListener, C1340e, C1403f {
    private String[] f6663a;
    private ListView f6664b;
    private ProgressBar f6665c;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131427363);
        FirebaseAnalytics.getInstance(InverterActivator.m3590a()).setCurrentScreen(this, "Locale List", getClass().getSimpleName());
        this.f6665c = (ProgressBar) findViewById(2131296464);
        setSupportActionBar((Toolbar) findViewById(C1375d.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        C1382c.m3782a().m3789b(getApplicationContext());
        this.f6663a = m8811d();
        bundle = new C1336a(this, 2131427398, 2131296454, this.f6663a);
        this.f6664b = (ListView) findViewById(2131296463);
        this.f6664b.setAdapter(bundle);
        this.f6664b.setOnItemClickListener(this);
    }

    private void m8810c() {
        if (!m8813f()) {
            return;
        }
        if (C1404g.m3847b()) {
            this.f6665c.setVisibility(8);
            return;
        }
        this.f6665c.setVisibility(0);
        m8812e();
    }

    private String[] m8811d() {
        String[] strArr = new String[C1382c.m3782a().m3792c(this).size()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = ((LocaleInfo) C1382c.m3782a().m3792c(this).get(i)).getCode();
        }
        return m8809a(strArr);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (C1394f.m3816a().m3817a(getApplicationContext()).equals(this.f6663a[i]) == null) {
            adapterView = new Intent();
            adapterView.putExtra("key_chosen_locale", this.f6663a[i]);
            setResult(-1, adapterView);
        } else {
            setResult(null);
        }
        finish();
    }

    private void m8812e() {
        C1384d.m3793a().m3799b();
        C1384d.m3793a().m3797a((C1403f) this);
    }

    private boolean m8813f() {
        if (this.f6663a != null) {
            if (this.f6663a.length != 0) {
                return false;
            }
        }
        return true;
    }

    public void onStart() {
        super.onStart();
        C1339d.m3676a().m3682a((C1340e) this);
        m8810c();
    }

    public void onStop() {
        super.onStop();
        C1339d.m3676a().m3683b(this);
    }

    public void mo2811a() {
        if (this.f6664b != null && m8813f()) {
            this.f6665c.setVisibility(0);
            m8812e();
        }
    }

    public void mo1788b() {
        this.f6663a = m8811d();
        this.f6665c.setVisibility(8);
        C1382c.m3782a().m3789b(getApplicationContext());
        ListAdapter c1336a = new C1336a(this, 2131427398, 2131296454, this.f6663a);
        this.f6664b = (ListView) findViewById(2131296463);
        this.f6664b.setAdapter(c1336a);
        this.f6664b.setOnItemClickListener(this);
    }

    private String[] m8809a(String[] strArr) {
        List arrayList = new ArrayList();
        if (strArr != null) {
            for (String str : strArr) {
                if (str.equals("en_US") || str.equals("de_DE") || str.equals("fr_FR") || str.equals("it_IT")) {
                    arrayList.add(str);
                }
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }
}
