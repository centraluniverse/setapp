package com.solaredge.apps.activator;

import android.text.TextUtils;
import com.solaredge.apps.activator.Activity.C1318b;
import com.solaredge.apps.activator.p106a.C1320a;
import com.solaredge.apps.activator.p106a.C1321b;
import com.solaredge.apps.activator.p106a.C1322c;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.apps.activator.proto.commissioning.DeviceIdentity;
import com.solaredge.apps.activator.proto.commissioning.FileType;
import com.solaredge.apps.activator.proto.commissioning.Status;
import com.solaredge.apps.activator.proto.commissioning.Status.FileState;
import com.solaredge.apps.activator.proto.commissioning.Status.FileStatus;
import com.solaredge.apps.activator.proto.commissioning.Status.UpgradeStatus;
import com.solaredge.apps.activator.proto.general_types.Battery;
import com.solaredge.apps.activator.proto.general_types.Controller;
import com.solaredge.apps.activator.proto.general_types.ControllerType;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1398a;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

/* compiled from: UpdateProcessManager */
public class C1343g {
    public static Integer f3341a = Integer.valueOf(400);
    public static Integer f3342b = Integer.valueOf(300);
    public static Map<FileType, Integer> f3343c = new HashMap();
    public static Map<ControllerType, Integer> f3344d = new HashMap();
    private Map<Integer, FileState> f3345e = new HashMap();
    private Status f3346f = null;
    private C1342a f3347g = null;
    private Long f3348h = null;
    private Long f3349i;
    private FileState f3350j = null;
    private Integer f3351k = null;
    private C1318b f3352l;

    /* compiled from: UpdateProcessManager */
    private enum C1342a {
        IDLE,
        UPLOADING,
        PROCESSING,
        INSTALLING,
        ACTIVATION
    }

    static {
        f3343c.put(FileType.PORTIA, Integer.valueOf(0));
        f3343c.put(FileType.DSP1, Integer.valueOf(1));
        f3343c.put(FileType.DSP2, Integer.valueOf(2));
        f3344d.put(ControllerType.PORTIA, f3343c.get(FileType.PORTIA));
        f3344d.put(ControllerType.JUPITER_DSP1, f3343c.get(FileType.DSP1));
        f3344d.put(ControllerType.VENUS_DSP1, f3343c.get(FileType.DSP1));
        f3344d.put(ControllerType.VENUS3_DSP1, f3343c.get(FileType.DSP1));
        f3344d.put(ControllerType.JUPITER_DSP2, f3343c.get(FileType.DSP2));
        f3344d.put(ControllerType.VENUS_DSP2, f3343c.get(FileType.DSP2));
        f3344d.put(ControllerType.VENUS3_DSP2, f3343c.get(FileType.DSP2));
    }

    public void m3700a(C1318b c1318b) {
        this.f3352l = c1318b;
    }

    public int m3698a() {
        return this.f3345e == null ? 0 : this.f3345e.size();
    }

    public C1342a m3703b() {
        return this.f3347g;
    }

    public String m3704c() {
        return this.f3347g != null ? this.f3347g.name() : "";
    }

    public void m3701a(Status status) {
        C1398a.m3831a("Processing Status..");
        this.f3349i = null;
        if (m3691b(status)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Status: \t");
            stringBuilder.append(status);
            C1398a.m3831a(stringBuilder.toString());
            this.f3346f = status;
            m3706e();
            m3695s();
            m3694r();
            return;
        }
        C1398a.m3831a("Status not valid.");
    }

    private void m3694r() {
        int i;
        Integer num = this.f3346f.total_progress;
        Integer num2 = this.f3346f.total_time;
        int i2 = 0;
        if (num == null) {
            i = 0;
        } else {
            i = num.intValue();
        }
        num = Integer.valueOf(i);
        if (num2 != null) {
            i2 = num2.intValue();
        }
        num2 = Integer.valueOf(i2);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("   Current State: ");
        stringBuilder.append(this.f3347g.name());
        C1398a.m3831a(stringBuilder.toString());
        if (!((this.f3347g != C1342a.INSTALLING && this.f3347g != C1342a.ACTIVATION) || this.f3350j == null || this.f3351k == null || this.f3345e.get(this.f3351k) == null)) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("       -> ");
            stringBuilder.append(this.f3350j.file_type.name());
            stringBuilder.append("(");
            stringBuilder.append(this.f3351k);
            stringBuilder.append(")  ");
            stringBuilder.append(((FileState) this.f3345e.get(this.f3351k)).execution_progress);
            stringBuilder.append("%");
            String stringBuilder2 = stringBuilder.toString();
            if (!(num == null || num2 == null)) {
                StringBuilder stringBuilder3 = new StringBuilder();
                stringBuilder3.append(stringBuilder2);
                stringBuilder3.append(" [ Total Progress: ");
                stringBuilder3.append(Integer.toString(num.intValue()));
                stringBuilder3.append("% , Time Left: ");
                stringBuilder3.append(num2);
                stringBuilder3.append(" seconds ]");
                stringBuilder2 = stringBuilder3.toString();
            }
            C1398a.m3831a(stringBuilder2);
        }
        switch (this.f3347g) {
            case IDLE:
                this.f3352l.mo2820b();
                return;
            case UPLOADING:
                this.f3352l.mo2822e();
                return;
            case PROCESSING:
                this.f3352l.mo2823f();
                return;
            case INSTALLING:
                this.f3352l.mo2819a(num2.intValue());
                return;
            case ACTIVATION:
                this.f3352l.mo2821b(num2.intValue());
                return;
            default:
                this.f3352l.mo2820b();
                return;
        }
    }

    public FileState m3705d() {
        return this.f3350j;
    }

    public void m3706e() {
        this.f3345e = new HashMap();
        if (this.f3346f != null && this.f3346f.file_state != null) {
            for (Entry entry : this.f3346f.file_state.entrySet()) {
                this.f3345e.put(Integer.valueOf((String) entry.getKey()), entry.getValue());
            }
        }
    }

    private void m3695s() {
        if (!this.f3345e.isEmpty()) {
            if (this.f3346f.upgrade_status != UpgradeStatus.idle) {
                Object obj = null;
                Object obj2 = (this.f3346f.total_progress == null || this.f3346f.total_progress.intValue() <= 0) ? null : 1;
                Object obj3 = null;
                for (Entry entry : this.f3345e.entrySet()) {
                    FileState fileState = (FileState) entry.getValue();
                    if (fileState != null) {
                        FileStatus fileStatus = fileState.file_status;
                        if (fileStatus == FileStatus.processing && fileState.execution_progress.intValue() > 0) {
                            this.f3350j = fileState;
                            this.f3351k = (Integer) entry.getKey();
                            obj2 = 1;
                        } else if (fileStatus == FileStatus.uploaded) {
                            obj3 = 1;
                        }
                    }
                }
                if (m3696t() || m3697u()) {
                    obj = 1;
                }
                if (obj != null) {
                    this.f3347g = C1342a.ACTIVATION;
                    return;
                } else if (obj2 != null) {
                    this.f3347g = C1342a.INSTALLING;
                    return;
                } else if (this.f3346f.upgrade_status == UpgradeStatus.upgrading) {
                    this.f3347g = C1342a.PROCESSING;
                    return;
                } else if (obj3 != null) {
                    this.f3347g = C1342a.UPLOADING;
                    return;
                } else {
                    this.f3347g = C1342a.IDLE;
                    return;
                }
            }
        }
        this.f3347g = C1342a.IDLE;
    }

    public static boolean m3688a(int i) {
        return i >= f3342b.intValue() && i < f3342b.intValue() + 100;
    }

    private boolean m3696t() {
        for (Entry entry : this.f3345e.entrySet()) {
            if (C1343g.m3688a(((Integer) entry.getKey()).intValue()) && entry.getValue() != null && ((FileState) entry.getValue()).file_type == FileType.ACTIVATION && ((FileState) entry.getValue()).file_status == FileStatus.processing && ((FileState) entry.getValue()).execution_progress.intValue() > 0) {
                return true;
            }
        }
        return false;
    }

    private boolean m3697u() {
        for (Entry entry : this.f3345e.entrySet()) {
            if (((Integer) entry.getKey()).intValue() >= f3341a.intValue() && ((Integer) entry.getKey()).intValue() < f3341a.intValue() + 100 && entry.getValue() != null && ((FileState) entry.getValue()).file_type == FileType.CONFIGURATION && ((FileState) entry.getValue()).file_status == FileStatus.processing && ((FileState) entry.getValue()).execution_progress.intValue() > 0) {
                return true;
            }
        }
        return false;
    }

    public boolean m3707f() {
        boolean z = false;
        if (this.f3349i == null && this.f3348h == null) {
            return false;
        }
        long longValue;
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        StringBuilder stringBuilder;
        if (this.f3349i != null) {
            longValue = valueOf.longValue() - this.f3349i.longValue();
            stringBuilder = new StringBuilder();
            stringBuilder.append("Time elapsed since error occurred: ");
            stringBuilder.append(longValue);
            stringBuilder.append(" ms  (timeout after: 300000 ms)");
            C1398a.m3831a(stringBuilder.toString());
        } else if (this.f3348h != null) {
            longValue = valueOf.longValue() - this.f3348h.longValue();
            stringBuilder = new StringBuilder();
            stringBuilder.append("Time elapsed since we got same status: ");
            stringBuilder.append(longValue);
            stringBuilder.append(" ms  (timeout after: 300000 ms)");
            C1398a.m3831a(stringBuilder.toString());
        } else {
            longValue = 0;
        }
        if (longValue > 300000) {
            z = true;
        }
        return z;
    }

    private boolean m3691b(Status status) {
        if (status == null) {
            return false;
        }
        if (status.serial_number != null) {
            if (Pattern.compile(Pattern.quote(status.serial_number), 2).matcher(C1323a.m3615a().m3618b()).find()) {
                if (C1331d.m3659a((Object) status, this.f3346f) == null) {
                    this.f3348h = null;
                } else if (this.f3348h == null) {
                    C1398a.m3831a("GetStatus: Status hasn't changed yet.. ");
                    this.f3348h = Long.valueOf(System.currentTimeMillis());
                }
                return true;
            }
        }
        m3702a(status.serial_number);
        C1395g.m3824a().m3825a("API_Activator_Serial_Validation_Failed", 1);
        this.f3352l.mo2824g();
        return false;
    }

    public void m3708g() {
        this.f3348h = null;
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f3349i == null || currentTimeMillis < this.f3349i.longValue()) {
            this.f3349i = Long.valueOf(currentTimeMillis);
            C1398a.m3831a("Error occurred..");
        }
    }

    public void m3709h() {
        this.f3349i = null;
        this.f3348h = null;
    }

    public List<C1320a> m3699a(List<Controller> list) {
        List i = m3710i();
        if (m3712k() && !i.isEmpty()) {
            list = C1343g.m3686a(list, i);
        }
        return C1343g.m3693d(list);
    }

    public static List<Controller> m3686a(List<Controller> list, List<Integer> list2) {
        List<Controller> arrayList = new ArrayList();
        if (list2.isEmpty()) {
            return list;
        }
        for (Controller controller : list) {
            if (!list2.contains(Integer.valueOf(((Integer) f3344d.get(controller.controller_type)).intValue()))) {
                arrayList.add(controller);
            }
        }
        return arrayList;
    }

    public List<Integer> m3710i() {
        List<Integer> arrayList = new ArrayList();
        for (Entry entry : this.f3345e.entrySet()) {
            FileState fileState = (FileState) entry.getValue();
            if (!(fileState == null || fileState.file_status == FileStatus.none)) {
                arrayList.add(entry.getKey());
            }
        }
        return arrayList;
    }

    public boolean m3711j() {
        return this.f3347g == C1342a.IDLE;
    }

    public boolean m3712k() {
        return this.f3347g == C1342a.UPLOADING;
    }

    public boolean m3713l() {
        return this.f3347g == C1342a.PROCESSING;
    }

    public boolean m3714m() {
        return this.f3347g == C1342a.INSTALLING;
    }

    public boolean m3715n() {
        return this.f3347g == C1342a.ACTIVATION;
    }

    public List<FileType> m3716o() {
        List<FileType> arrayList = new ArrayList();
        if (this.f3345e.containsKey(f3343c.get(FileType.PORTIA))) {
            arrayList.add(FileType.PORTIA);
        }
        if (this.f3345e.containsKey(f3343c.get(FileType.DSP1))) {
            arrayList.add(FileType.DSP1);
        }
        if (this.f3345e.containsKey(f3343c.get(FileType.DSP2))) {
            arrayList.add(FileType.DSP2);
        }
        return arrayList;
    }

    public boolean m3717p() {
        for (Entry value : this.f3345e.entrySet()) {
            FileState fileState = (FileState) value.getValue();
            if (fileState != null && fileState.file_status != FileStatus.none) {
                return false;
            }
        }
        return true;
    }

    public Integer m3718q() {
        return this.f3346f != null ? this.f3346f.total_time : null;
    }

    public static void m3687a(DeviceIdentity deviceIdentity) {
        if (!TextUtils.isEmpty(deviceIdentity.home_url)) {
            C1323a.m3615a().m3628g(deviceIdentity.home_url);
        }
        if (!TextUtils.isEmpty(deviceIdentity.status_url)) {
            C1323a.m3615a().m3626f(deviceIdentity.status_url);
        }
    }

    private static ArrayList<C1320a> m3693d(List<Controller> list) {
        Object arrayList = new ArrayList();
        C1321b n = C1323a.m3615a().m3636n();
        Map b = n.m3611b();
        Map a = n.m3610a();
        C1322c c1322c = (C1322c) b.get(C1323a.m3615a().m3624e());
        if (c1322c == null) {
            C1395g.m3824a().m3825a("Unable to find part number in mapping file, Continuing with activation", 1);
            return arrayList;
        }
        List<C1320a> list2 = (List) a.get(c1322c.m3612a());
        if (list2 == null) {
            C1395g.m3824a().m3825a("Unable to find sw package for part number in mapping file, Continuing with activation", 1);
            return arrayList;
        }
        if (!(c1322c == null || list2.isEmpty() || list == null)) {
            for (Controller controller : list) {
                C1320a c1320a = null;
                for (C1320a c1320a2 : list2) {
                    if (controller.controller_type == c1320a2.m3605b()) {
                        Long valueOf = Long.valueOf(C1331d.m3662b(controller.version));
                        if (C1331d.m3658a(valueOf, c1320a2.m3606c(), c1320a2.m3607d())) {
                            Long e = c1320a2.m3608e();
                            if (e.longValue() > valueOf.longValue()) {
                                if (c1320a != null) {
                                    if (e.longValue() <= c1320a.m3608e().longValue()) {
                                    }
                                }
                                c1320a = c1320a2;
                            }
                        }
                    }
                }
                if (!(c1320a == null || arrayList.contains(c1320a))) {
                    arrayList.add(c1320a);
                }
            }
            if (arrayList.isEmpty() == null) {
                Collections.sort(arrayList);
            }
        }
        return arrayList;
    }

    public static List<C1334b> m3685a(List<C1320a> list, Boolean bool, List<Integer> list2) {
        List<C1334b> arrayList = new ArrayList();
        Collection arrayList2 = new ArrayList();
        arrayList.addAll(C1343g.m3689b(list, null));
        if (bool.booleanValue() == null) {
            list = C1323a.m3615a().m3638p();
            bool = null;
            if (list != null) {
                for (Integer valueOf = Integer.valueOf(0); valueOf.intValue() < list.size(); valueOf = Integer.valueOf(valueOf.intValue() + 1)) {
                    Integer valueOf2 = Integer.valueOf(f3341a.intValue() + valueOf.intValue());
                    String str = (String) list.get(valueOf.intValue());
                    if (!list2.contains(valueOf2)) {
                        arrayList2.add(new C1334b(valueOf2, str));
                    }
                }
            }
            arrayList.addAll(arrayList2);
            list = C1323a.m3615a().m3637o();
            if (list != null) {
                while (true) {
                    bool = Integer.valueOf(bool);
                    if (bool.intValue() >= list.size()) {
                        break;
                    }
                    Integer valueOf3 = Integer.valueOf(f3342b.intValue() + bool.intValue());
                    String str2 = (String) list.get(bool.intValue());
                    if (!list2.contains(valueOf3)) {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("pns");
                        stringBuilder.append(File.separator);
                        stringBuilder.append(str2);
                        arrayList.add(new C1334b(valueOf3, stringBuilder.toString()));
                    }
                    bool = bool.intValue() + 1;
                }
            }
        }
        return arrayList;
    }

    public static List<C1334b> m3689b(List<C1320a> list, List<Battery> list2) {
        list2 = new ArrayList();
        for (C1320a c1320a : list) {
            C1334b c1334b = new C1334b((Integer) f3344d.get(c1320a.m3605b()), c1320a.m3604a());
            if (!list2.contains(c1334b)) {
                list2.add(c1334b);
            }
        }
        return list2;
    }

    public static void m3690b(List<C1320a> list) {
        C1398a.m3831a(!list.isEmpty() ? "New Firmware Versions For Upgrade:" : "No New Firmware Versions Found For Upgrade.");
        int i = 1;
        for (C1320a c1320a : list) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("\t(");
            stringBuilder.append(i);
            stringBuilder.append(") ");
            stringBuilder.append(c1320a.m3605b().name());
            stringBuilder.append(" : ");
            stringBuilder.append(c1320a.m3609f());
            C1398a.m3831a(stringBuilder.toString());
            i++;
        }
    }

    public static void m3692c(List<Controller> list) {
        C1398a.m3831a("GetIdentity: Controllers:");
        int i = 1;
        for (Controller controller : list) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("\t(");
            stringBuilder.append(i);
            stringBuilder.append(") ");
            stringBuilder.append(controller.controller_type.name());
            stringBuilder.append(" : ");
            stringBuilder.append(C1331d.m3646a(controller.version));
            C1398a.m3831a(stringBuilder.toString());
            i++;
        }
    }

    public void m3702a(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("QR:");
        stringBuilder.append(C1323a.m3615a().m3618b());
        stringBuilder.append(",PORTIA:");
        stringBuilder.append(str);
        InverterActivator.m3590a().m3596a("INVALID_SERIAL_NUMBER", stringBuilder.toString());
    }
}
