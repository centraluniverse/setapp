package com.solaredge.apps.activator;

import android.text.TextUtils;
import java.io.Serializable;

/* compiled from: FileInfo */
public class C1334b implements Serializable {
    public Integer f3323a;
    public String f3324b;

    public C1334b(Integer num, String str) {
        this.f3323a = num;
        this.f3324b = str;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null || !(obj instanceof C1334b)) {
            return false;
        }
        C1334b c1334b = (C1334b) obj;
        if (!(!TextUtils.equals(this.f3324b, c1334b.f3324b) || this.f3323a == null || this.f3323a.equals(c1334b.f3323a) == null)) {
            z = true;
        }
        return z;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ ");
        stringBuilder.append(this.f3323a == null ? "" : Integer.toString(this.f3323a.intValue()));
        stringBuilder.append(" : ");
        stringBuilder.append(this.f3324b == null ? "" : this.f3324b);
        stringBuilder.append(" ]");
        return stringBuilder.toString();
    }
}
