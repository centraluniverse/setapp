package com.solaredge.apps.activator;

import android.os.Environment;
import android.text.TextUtils;
import com.google.p040a.C0671f;
import com.solaredge.apps.activator.p106a.C1321b;
import com.solaredge.apps.activator.p106a.C1322c;
import com.solaredge.apps.activator.p107b.C1324a;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.common.p114e.C1382c;
import com.solaredge.common.p114e.C1395g;
import com.solaredge.common.p116g.C1398a;
import java.io.File;
import java.io.FileInputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ActivatorManager */
public class C1323a {
    private static C1323a f3297a;
    private String f3298b = "";
    private String f3299c = "";
    private String f3300d = "";
    private String f3301e = "";
    private byte[] f3302f = null;
    private String f3303g = "";
    private String f3304h;
    private String f3305i;
    private C1321b f3306j = new C1321b();
    private String f3307k = null;

    public static synchronized C1323a m3615a() {
        C1323a c1323a;
        synchronized (C1323a.class) {
            if (f3297a == null) {
                f3297a = new C1323a();
            }
            c1323a = f3297a;
        }
        return c1323a;
    }

    public void m3616a(String str) {
        this.f3298b = str;
    }

    public void m3619b(String str) {
        this.f3299c = str;
    }

    public void m3621c(String str) {
        this.f3300d = str;
    }

    public String m3618b() {
        return this.f3298b;
    }

    public String m3620c() {
        return this.f3299c;
    }

    public String m3622d() {
        return this.f3300d;
    }

    public void m3623d(String str) {
        this.f3301e = str;
    }

    public String m3624e() {
        return this.f3301e;
    }

    public void m3617a(byte[] bArr) {
        this.f3302f = bArr;
    }

    public byte[] m3627f() {
        return this.f3302f;
    }

    public boolean m3629g() {
        return (this.f3298b == null || TextUtils.isEmpty(this.f3299c) || TextUtils.isEmpty(this.f3300d) || TextUtils.isEmpty(this.f3301e) || this.f3302f == null) ? false : true;
    }

    public boolean m3630h() {
        if (m3629g()) {
            if (!(this.f3306j == null || this.f3306j.m3611b() == null)) {
                if (this.f3306j.m3611b().containsKey(this.f3301e)) {
                    return true;
                }
            }
            String a = C1382c.m3782a().m3783a("API_Activator_Unknown_Part_Number");
            C1398a.m3831a(a);
            C1395g.m3824a().m3825a(a, 1);
            InverterActivator.m3590a().m3596a("MISSING_PART_NUMBER", this.f3301e);
            return false;
        }
        a = C1382c.m3782a().m3783a("API_Unknown_Error");
        C1398a.m3831a(a);
        C1395g.m3824a().m3825a(a, 1);
        return false;
    }

    public String m3631i() {
        return this.f3303g;
    }

    public void m3625e(String str) {
        this.f3303g = str;
    }

    public void m3626f(String str) {
        this.f3304h = str;
    }

    public void m3628g(String str) {
        this.f3305i = str;
    }

    public String m3632j() {
        return this.f3304h;
    }

    public String m3633k() {
        return this.f3305i;
    }

    public void m3634l() {
        try {
            String c = C1331d.m3667c("mapping.txt");
            if (c != null) {
                this.f3306j = (C1321b) new C0671f().m1187a(c, C1321b.class);
            }
        } catch (Exception e) {
            e.getStackTrace();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Error parsing mapping file: ");
            stringBuilder.append(e.getMessage());
            C1398a.m3831a(stringBuilder.toString());
        }
    }

    public void m3635m() {
        File externalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (externalStoragePublicDirectory != null && externalStoragePublicDirectory.isDirectory()) {
            for (File file : externalStoragePublicDirectory.listFiles()) {
                if (file.getName() != null && file.getName().endsWith(C1324a.f3313f)) {
                    try {
                        FileChannel channel = new FileInputStream(file).getChannel();
                        this.f3306j = (C1321b) new C0671f().m1187a(Charset.defaultCharset().decode(channel.map(MapMode.READ_ONLY, 0, channel.size())).toString(), C1321b.class);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("Error parsing external mapping file: ");
                        stringBuilder.append(e.getMessage());
                        String stringBuilder2 = stringBuilder.toString();
                        C1398a.m3831a(stringBuilder2);
                        C1395g.m3824a().m3825a(stringBuilder2, 1);
                    }
                }
            }
        }
    }

    public C1321b m3636n() {
        return this.f3306j;
    }

    public List<String> m3637o() {
        if (this.f3306j != null) {
            C1322c c1322c = (C1322c) this.f3306j.m3611b().get(this.f3301e);
            if (c1322c != null) {
                return c1322c.m3613b();
            }
        }
        return null;
    }

    public ArrayList<String> m3638p() {
        if (this.f3306j != null) {
            C1322c c1322c = (C1322c) this.f3306j.m3611b().get(this.f3301e);
            if (c1322c != null) {
                return c1322c.m3614c();
            }
        }
        return null;
    }

    public void m3639q() {
        this.f3299c = "";
        this.f3298b = "";
        this.f3301e = "";
        this.f3300d = "";
        this.f3301e = "";
        this.f3302f = null;
    }
}
