package com.solaredge.apps.activator.p106a;

import android.support.annotation.NonNull;
import com.google.p040a.p041a.C0633c;
import com.solaredge.apps.activator.p107b.C1331d;
import com.solaredge.apps.activator.proto.general_types.ControllerType;

/* compiled from: FirmwareVersionInfo */
public class C1320a implements Comparable<C1320a> {
    @C0633c(a = "type")
    private ControllerType f3284a = null;
    @C0633c(a = "maxVersion")
    private String f3285b;
    @C0633c(a = "minVersion")
    private String f3286c;
    @C0633c(a = "version")
    private String f3287d;
    @C0633c(a = "upgradeFile")
    private String f3288e;
    private Long f3289f;
    private Long f3290g;
    private Long f3291h;

    public /* synthetic */ int compareTo(@NonNull Object obj) {
        return m3603a((C1320a) obj);
    }

    public String m3604a() {
        return this.f3288e;
    }

    public ControllerType m3605b() {
        return this.f3284a;
    }

    public Long m3606c() {
        if (this.f3290g == null) {
            this.f3290g = Long.valueOf(C1331d.m3645a(this.f3285b));
        }
        return this.f3290g;
    }

    public Long m3607d() {
        if (this.f3291h == null) {
            this.f3291h = Long.valueOf(C1331d.m3645a(this.f3286c));
        }
        return this.f3291h;
    }

    public Long m3608e() {
        if (this.f3289f == null) {
            this.f3289f = Long.valueOf(C1331d.m3645a(this.f3287d));
        }
        return this.f3289f;
    }

    public String m3609f() {
        return this.f3287d;
    }

    public int m3603a(@NonNull C1320a c1320a) {
        if (this.f3284a.getValue() > c1320a.m3605b().getValue()) {
            return 1;
        }
        return this.f3284a.getValue() < c1320a.m3605b().getValue() ? -1 : null;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (obj == null || !(obj instanceof C1320a)) {
            return false;
        }
        if (this.f3284a != null) {
            C1320a c1320a = (C1320a) obj;
            if (!(!this.f3284a.equals(c1320a.f3284a) || this.f3285b == null || !this.f3285b.equalsIgnoreCase(c1320a.f3285b) || this.f3286c == null || !this.f3286c.equalsIgnoreCase(c1320a.f3286c) || this.f3287d == null || !this.f3287d.equals(c1320a.f3287d) || this.f3288e == null || this.f3288e.equals(c1320a.f3288e) == null)) {
                z = true;
            }
        }
        return z;
    }
}
