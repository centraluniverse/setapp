package com.solaredge.apps.activator.p106a;

import com.google.p040a.p041a.C0633c;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: MappingObject */
public class C1321b {
    @C0633c(a = "sw_pkg")
    private Map<String, List<C1320a>> f3292a = new HashMap();
    @C0633c(a = "pns")
    private Map<String, C1322c> f3293b = new HashMap();

    public Map<String, List<C1320a>> m3610a() {
        return this.f3292a;
    }

    public Map<String, C1322c> m3611b() {
        return this.f3293b;
    }
}
