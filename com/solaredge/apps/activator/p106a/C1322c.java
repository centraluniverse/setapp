package com.solaredge.apps.activator.p106a;

import com.google.p040a.p041a.C0633c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PartNumberData */
public class C1322c {
    @C0633c(a = "sw_pkg")
    private String f3294a;
    @C0633c(a = "activationFile")
    private List<String> f3295b;
    @C0633c(a = "confFiles")
    private ArrayList<String> f3296c;

    public String m3612a() {
        return this.f3294a;
    }

    public List<String> m3613b() {
        return this.f3295b;
    }

    public ArrayList<String> m3614c() {
        return this.f3296c;
    }
}
