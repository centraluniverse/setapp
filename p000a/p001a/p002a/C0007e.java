package p000a.p001a.p002a;

/* compiled from: EventBusException */
public class C0007e extends RuntimeException {
    public C0007e(String str) {
        super(str);
    }

    public C0007e(String str, Throwable th) {
        super(str, th);
    }
}
