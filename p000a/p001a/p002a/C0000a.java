package p000a.p001a.p002a;

/* compiled from: AsyncPoster */
class C0000a implements Runnable {
    private final C0011i f0a = new C0011i();
    private final C0005c f1b;

    C0000a(C0005c c0005c) {
        this.f1b = c0005c;
    }

    public void m0a(C0015m c0015m, Object obj) {
        this.f0a.m27a(C0010h.m23a(c0015m, obj));
        this.f1b.m16b().execute(this);
    }

    public void run() {
        C0010h a = this.f0a.m25a();
        if (a == null) {
            throw new IllegalStateException("No pending post available");
        }
        this.f1b.m13a(a);
    }
}
