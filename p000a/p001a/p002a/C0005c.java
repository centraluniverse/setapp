package p000a.p001a.p002a;

import android.os.Looper;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

/* compiled from: EventBus */
public class C0005c {
    public static String f13a = "Event";
    static volatile C0005c f14b;
    private static final C0006d f15c = new C0006d();
    private static final Map<Class<?>, List<Class<?>>> f16d = new HashMap();
    private final Map<Class<?>, CopyOnWriteArrayList<C0015m>> f17e;
    private final Map<Object, List<Class<?>>> f18f;
    private final Map<Class<?>, Object> f19g;
    private final ThreadLocal<C0004a> f20h;
    private final C0008f f21i;
    private final C0001b f22j;
    private final C0000a f23k;
    private final C0014l f24l;
    private final ExecutorService f25m;
    private final boolean f26n;
    private final boolean f27o;
    private final boolean f28p;
    private final boolean f29q;
    private final boolean f30r;
    private final boolean f31s;

    /* compiled from: EventBus */
    class C00021 extends ThreadLocal<C0004a> {
        final /* synthetic */ C0005c f5a;

        C00021(C0005c c0005c) {
            this.f5a = c0005c;
        }

        protected /* synthetic */ Object initialValue() {
            return m2a();
        }

        protected C0004a m2a() {
            return new C0004a();
        }
    }

    /* compiled from: EventBus */
    static final class C0004a {
        final List<Object> f7a = new ArrayList();
        boolean f8b;
        boolean f9c;
        C0015m f10d;
        Object f11e;
        boolean f12f;

        C0004a() {
        }
    }

    public static C0005c m3a() {
        if (f14b == null) {
            synchronized (C0005c.class) {
                if (f14b == null) {
                    f14b = new C0005c();
                }
            }
        }
        return f14b;
    }

    public C0005c() {
        this(f15c);
    }

    C0005c(C0006d c0006d) {
        this.f20h = new C00021(this);
        this.f17e = new HashMap();
        this.f18f = new HashMap();
        this.f19g = new ConcurrentHashMap();
        this.f21i = new C0008f(this, Looper.getMainLooper(), 10);
        this.f22j = new C0001b(this);
        this.f23k = new C0000a(this);
        this.f24l = new C0014l(c0006d.f40h);
        this.f27o = c0006d.f33a;
        this.f28p = c0006d.f34b;
        this.f29q = c0006d.f35c;
        this.f30r = c0006d.f36d;
        this.f26n = c0006d.f37e;
        this.f31s = c0006d.f38f;
        this.f25m = c0006d.f39g;
    }

    public void m15a(Object obj) {
        m10a(obj, false, 0);
    }

    private synchronized void m10a(Object obj, boolean z, int i) {
        for (C0013k a : this.f24l.m29a(obj.getClass())) {
            m8a(obj, a, z, i);
        }
    }

    private void m8a(Object obj, C0013k c0013k, boolean z, int i) {
        Class cls = c0013k.f59c;
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.f17e.get(cls);
        C0015m c0015m = new C0015m(obj, c0013k, i);
        if (copyOnWriteArrayList == null) {
            copyOnWriteArrayList = new CopyOnWriteArrayList();
            this.f17e.put(cls, copyOnWriteArrayList);
        } else if (copyOnWriteArrayList.contains(c0015m) != null) {
            z = new StringBuilder();
            z.append("Subscriber ");
            z.append(obj.getClass());
            z.append(" already registered to event ");
            z.append(cls);
            throw new C0007e(z.toString());
        }
        c0013k = copyOnWriteArrayList.size();
        boolean z2 = false;
        int i2 = 0;
        while (i2 <= c0013k) {
            if (i2 != c0013k) {
                if (c0015m.f65c <= ((C0015m) copyOnWriteArrayList.get(i2)).f65c) {
                    i2++;
                }
            }
            copyOnWriteArrayList.add(i2, c0015m);
            break;
        }
        c0013k = (List) this.f18f.get(obj);
        if (c0013k == null) {
            c0013k = new ArrayList();
            this.f18f.put(obj, c0013k);
        }
        c0013k.add(cls);
        if (z) {
            Object obj2;
            synchronized (this.f19g) {
                obj2 = this.f19g.get(cls);
            }
            if (obj2 != null) {
                if (Looper.getMainLooper() == Looper.myLooper()) {
                    z2 = true;
                }
                m6a(c0015m, obj2, z2);
            }
        }
    }

    public synchronized boolean m17b(Object obj) {
        return this.f18f.containsKey(obj);
    }

    private void m9a(Object obj, Class<?> cls) {
        List list = (List) this.f17e.get(cls);
        if (list != null) {
            int size = list.size();
            int i = 0;
            while (i < size) {
                C0015m c0015m = (C0015m) list.get(i);
                if (c0015m.f63a == obj) {
                    c0015m.f66d = false;
                    list.remove(i);
                    i--;
                    size--;
                }
                i++;
            }
        }
    }

    public synchronized void m18c(Object obj) {
        List<Class> list = (List) this.f18f.get(obj);
        if (list != null) {
            for (Class a : list) {
                m9a(obj, a);
            }
            this.f18f.remove(obj);
        } else {
            String str = f13a;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Subscriber to unregister was not registered before: ");
            stringBuilder.append(obj.getClass());
            Log.w(str, stringBuilder.toString());
        }
    }

    public void m19d(Object obj) {
        C0004a c0004a = (C0004a) this.f20h.get();
        List list = c0004a.f7a;
        list.add(obj);
        if (c0004a.f8b == null) {
            c0004a.f9c = Looper.getMainLooper() == Looper.myLooper() ? 1 : null;
            c0004a.f8b = true;
            if (c0004a.f12f != null) {
                throw new C0007e("Internal error. Abort state was not reset");
            }
            while (list.isEmpty() == null) {
                try {
                    m7a(list.remove(0), c0004a);
                } finally {
                    c0004a.f8b = false;
                    c0004a.f9c = false;
                }
            }
        }
    }

    public void m20e(Object obj) {
        synchronized (this.f19g) {
            this.f19g.put(obj.getClass(), obj);
        }
        m19d(obj);
    }

    public boolean m21f(Object obj) {
        synchronized (this.f19g) {
            Class cls = obj.getClass();
            if (obj.equals(this.f19g.get(cls)) != null) {
                this.f19g.remove(cls);
                return true;
            }
            return null;
        }
    }

    private void m7a(Object obj, C0004a c0004a) throws Error {
        int i;
        Class cls = obj.getClass();
        if (this.f31s) {
            List a = m4a(cls);
            int i2 = 0;
            i = 0;
            while (i2 < a.size()) {
                i |= m12a(obj, c0004a, (Class) a.get(i2));
                i2++;
            }
        } else {
            i = m12a(obj, c0004a, cls);
        }
        if (i == 0) {
            if (this.f28p != null) {
                c0004a = f13a;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("No subscribers registered for event ");
                stringBuilder.append(cls);
                Log.d(c0004a, stringBuilder.toString());
            }
            if (this.f30r != null && cls != C0009g.class && cls != C0012j.class) {
                m19d(new C0009g(this, obj));
            }
        }
    }

    private boolean m12a(Object obj, C0004a c0004a, Class<?> cls) {
        synchronized (this) {
            CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.f17e.get(cls);
        }
        if (copyOnWriteArrayList == null || copyOnWriteArrayList.isEmpty()) {
            return false;
        }
        cls = copyOnWriteArrayList.iterator();
        loop0:
        while (cls.hasNext()) {
            boolean z = (C0015m) cls.next();
            c0004a.f11e = obj;
            c0004a.f10d = z;
            try {
                m6a((C0015m) z, obj, c0004a.f9c);
                z = c0004a.f12f;
                continue;
            } finally {
                c0004a.f11e = null;
                c0004a.f10d = null;
                c0004a.f12f = false;
            }
            if (z) {
                break loop0;
            }
        }
        return true;
    }

    private void m6a(C0015m c0015m, Object obj, boolean z) {
        switch (c0015m.f64b.f58b) {
            case PostThread:
                m14a(c0015m, obj);
                return;
            case MainThread:
                if (z) {
                    m14a(c0015m, obj);
                    return;
                } else {
                    this.f21i.m22a(c0015m, obj);
                    return;
                }
            case BackgroundThread:
                if (z) {
                    this.f22j.m1a(c0015m, obj);
                    return;
                } else {
                    m14a(c0015m, obj);
                    return;
                }
            case Async:
                this.f23k.m0a(c0015m, obj);
                return;
            default:
                z = new StringBuilder();
                z.append("Unknown thread mode: ");
                z.append(c0015m.f64b.f58b);
                throw new IllegalStateException(z.toString());
        }
    }

    private List<Class<?>> m4a(Class<?> cls) {
        List<Class<?>> list;
        synchronized (f16d) {
            list = (List) f16d.get(cls);
            if (list == null) {
                list = new ArrayList();
                for (Class cls2 = cls; cls2 != null; cls2 = cls2.getSuperclass()) {
                    list.add(cls2);
                    C0005c.m11a((List) list, cls2.getInterfaces());
                }
                f16d.put(cls, list);
            }
        }
        return list;
    }

    static void m11a(List<Class<?>> list, Class<?>[] clsArr) {
        for (Class cls : clsArr) {
            if (!list.contains(cls)) {
                list.add(cls);
                C0005c.m11a((List) list, cls.getInterfaces());
            }
        }
    }

    void m13a(C0010h c0010h) {
        Object obj = c0010h.f48a;
        C0015m c0015m = c0010h.f49b;
        C0010h.m24a(c0010h);
        if (c0015m.f66d != null) {
            m14a(c0015m, obj);
        }
    }

    void m14a(C0015m c0015m, Object obj) {
        try {
            c0015m.f64b.f57a.invoke(c0015m.f63a, new Object[]{obj});
        } catch (InvocationTargetException e) {
            m5a(c0015m, obj, e.getCause());
        } catch (C0015m c0015m2) {
            throw new IllegalStateException("Unexpected exception", c0015m2);
        }
    }

    private void m5a(C0015m c0015m, Object obj, Throwable th) {
        String str;
        StringBuilder stringBuilder;
        if (obj instanceof C0012j) {
            if (this.f27o) {
                str = f13a;
                stringBuilder = new StringBuilder();
                stringBuilder.append("SubscriberExceptionEvent subscriber ");
                stringBuilder.append(c0015m.f63a.getClass());
                stringBuilder.append(" threw an exception");
                Log.e(str, stringBuilder.toString(), th);
                C0012j c0012j = (C0012j) obj;
                c0015m = f13a;
                th = new StringBuilder();
                th.append("Initial event ");
                th.append(c0012j.f55c);
                th.append(" caused exception in ");
                th.append(c0012j.f56d);
                Log.e(c0015m, th.toString(), c0012j.f54b);
            }
        } else if (this.f26n) {
            throw new C0007e("Invoking subscriber failed", th);
        } else {
            if (this.f27o) {
                str = f13a;
                stringBuilder = new StringBuilder();
                stringBuilder.append("Could not dispatch event: ");
                stringBuilder.append(obj.getClass());
                stringBuilder.append(" to subscribing class ");
                stringBuilder.append(c0015m.f63a.getClass());
                Log.e(str, stringBuilder.toString(), th);
            }
            if (this.f29q) {
                m19d(new C0012j(this, th, obj, c0015m.f63a));
            }
        }
    }

    ExecutorService m16b() {
        return this.f25m;
    }
}
