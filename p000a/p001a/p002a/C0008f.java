package p000a.p001a.p002a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

/* compiled from: HandlerPoster */
final class C0008f extends Handler {
    private final C0011i f41a = new C0011i();
    private final int f42b;
    private final C0005c f43c;
    private boolean f44d;

    C0008f(C0005c c0005c, Looper looper, int i) {
        super(looper);
        this.f43c = c0005c;
        this.f42b = i;
    }

    void m22a(C0015m c0015m, Object obj) {
        C0010h a = C0010h.m23a(c0015m, obj);
        synchronized (this) {
            this.f41a.m27a(a);
            if (this.f44d == null) {
                this.f44d = true;
                if (sendMessage(obtainMessage()) == null) {
                    throw new C0007e("Could not send handler message");
                }
            }
        }
    }

    public void handleMessage(Message message) {
        try {
            long uptimeMillis = SystemClock.uptimeMillis();
            do {
                C0010h a = this.f41a.m25a();
                if (a == null) {
                    synchronized (this) {
                        a = this.f41a.m25a();
                        if (a == null) {
                            this.f44d = false;
                            this.f44d = false;
                            return;
                        }
                    }
                }
                this.f43c.m13a(a);
            } while (SystemClock.uptimeMillis() - uptimeMillis < ((long) this.f42b));
            if (sendMessage(obtainMessage())) {
                this.f44d = true;
                return;
            }
            throw new C0007e("Could not send handler message");
        } catch (Throwable th) {
            this.f44d = false;
        }
    }
}
