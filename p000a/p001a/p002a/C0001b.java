package p000a.p001a.p002a;

import android.util.Log;

/* compiled from: BackgroundPoster */
final class C0001b implements Runnable {
    private final C0011i f2a = new C0011i();
    private final C0005c f3b;
    private volatile boolean f4c;

    C0001b(C0005c c0005c) {
        this.f3b = c0005c;
    }

    public void m1a(C0015m c0015m, Object obj) {
        C0010h a = C0010h.m23a(c0015m, obj);
        synchronized (this) {
            this.f2a.m27a(a);
            if (this.f4c == null) {
                this.f4c = true;
                this.f3b.m16b().execute(this);
            }
        }
    }

    public void run() {
        while (true) {
            try {
                C0010h a = this.f2a.m26a(1000);
                if (a == null) {
                    synchronized (this) {
                        a = this.f2a.m25a();
                        if (a == null) {
                            this.f4c = false;
                            this.f4c = false;
                            return;
                        }
                    }
                }
                this.f3b.m13a(a);
            } catch (Throwable e) {
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(Thread.currentThread().getName());
                    stringBuilder.append(" was interruppted");
                    Log.w("Event", stringBuilder.toString(), e);
                    return;
                } finally {
                    this.f4c = false;
                }
            }
        }
    }
}
