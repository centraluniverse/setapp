package p126e;

import p126e.p132d.C1655a;
import p126e.p132d.C1656b;
import p126e.p132d.C1658e;

/* compiled from: Completable */
public class C1643a {
    static final C1656b f4424a = C1658e.m4902a().m4904b();
    static C1655a f4425b = C1658e.m4902a().m4907e();
    static final C1643a f4426c = C1643a.m4885a(new C22951());
    static final C1643a f4427d = C1643a.m4885a(new C22962());
    private final C1633a f4428e;

    /* compiled from: Completable */
    public interface C1633a {
    }

    /* compiled from: Completable */
    public interface C1634b {
        void m4873a();

        void m4874a(C1666h c1666h);

        void m4875a(Throwable th);
    }

    /* compiled from: Completable */
    static class C22951 implements C1633a {
        C22951() {
        }
    }

    /* compiled from: Completable */
    static class C22962 implements C1633a {
        C22962() {
        }
    }

    public static C1643a m4885a(C1633a c1633a) {
        C1643a.m4887a((Object) c1633a);
        try {
            return new C1643a(c1633a);
        } catch (C1633a c1633a2) {
            throw c1633a2;
        } catch (Throwable th) {
            f4424a.m4900a(th);
            c1633a2 = C1643a.m4886a(th);
        }
    }

    static <T> T m4887a(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    static NullPointerException m4886a(Throwable th) {
        NullPointerException nullPointerException = new NullPointerException("Actually not, but can't pass out an exception otherwise...");
        nullPointerException.initCause(th);
        return nullPointerException;
    }

    protected C1643a(C1633a c1633a) {
        this.f4428e = f4425b.m4899a(c1633a);
    }

    public final C1643a m4888a(final C1663e c1663e) {
        C1643a.m4887a((Object) c1663e);
        return C1643a.m4885a(new C1633a(this) {
            final /* synthetic */ C1643a f5774b;
        });
    }
}
