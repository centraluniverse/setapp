package p126e;

import p126e.p129c.p131c.C2310d;

/* compiled from: Subscriber */
public abstract class C2320g<T> implements C1654c<T>, C1666h {
    private static final long NOT_SET = Long.MIN_VALUE;
    private C1661d producer;
    private long requested;
    private final C2320g<?> subscriber;
    private final C2310d subscriptions;

    public void onStart() {
    }

    protected C2320g() {
        this(null, false);
    }

    protected C2320g(C2320g<?> c2320g) {
        this(c2320g, true);
    }

    protected C2320g(C2320g<?> c2320g, boolean z) {
        this.requested = NOT_SET;
        this.subscriber = c2320g;
        c2320g = (!z || c2320g == null) ? new C2310d() : c2320g.subscriptions;
        this.subscriptions = c2320g;
    }

    public final void add(C1666h c1666h) {
        this.subscriptions.m6985a(c1666h);
    }

    public final void unsubscribe() {
        this.subscriptions.unsubscribe();
    }

    public final boolean isUnsubscribed() {
        return this.subscriptions.isUnsubscribed();
    }

    protected final void request(long j) {
        if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("number requested cannot be negative: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        }
        synchronized (this) {
            if (this.producer != null) {
                C1661d c1661d = this.producer;
                c1661d.request(j);
                return;
            }
            addToRequested(j);
        }
    }

    private void addToRequested(long j) {
        if (this.requested == NOT_SET) {
            this.requested = j;
            return;
        }
        long j2 = this.requested + j;
        if (j2 < 0) {
            this.requested = Long.MAX_VALUE;
        } else {
            this.requested = j2;
        }
    }

    public void setProducer(C1661d c1661d) {
        synchronized (this) {
            long j = this.requested;
            this.producer = c1661d;
            c1661d = (this.subscriber == null || j != NOT_SET) ? null : true;
        }
        if (c1661d != null) {
            this.subscriber.setProducer(this.producer);
        } else if (j == NOT_SET) {
            this.producer.request(Long.MAX_VALUE);
        } else {
            this.producer.request(j);
        }
    }
}
