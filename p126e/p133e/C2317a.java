package p126e.p133e;

import java.util.concurrent.atomic.AtomicReference;
import p126e.C1666h;
import p126e.p128b.C1645a;

/* compiled from: BooleanSubscription */
public final class C2317a implements C1666h {
    static final C1645a f5801b = new C23161();
    final AtomicReference<C1645a> f5802a;

    /* compiled from: BooleanSubscription */
    static class C23161 implements C1645a {
        public void call() {
        }

        C23161() {
        }
    }

    public C2317a() {
        this.f5802a = new AtomicReference();
    }

    private C2317a(C1645a c1645a) {
        this.f5802a = new AtomicReference(c1645a);
    }

    public static C2317a m6988a(C1645a c1645a) {
        return new C2317a(c1645a);
    }

    public boolean isUnsubscribed() {
        return this.f5802a.get() == f5801b;
    }

    public final void unsubscribe() {
        if (((C1645a) this.f5802a.get()) != f5801b) {
            C1645a c1645a = (C1645a) this.f5802a.getAndSet(f5801b);
            if (c1645a != null && c1645a != f5801b) {
                c1645a.call();
            }
        }
    }
}
