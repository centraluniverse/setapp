package p126e.p133e;

import p126e.C1666h;
import p126e.p128b.C1645a;

/* compiled from: Subscriptions */
public final class C1662b {
    private static final C2318a f4445a = new C2318a();

    /* compiled from: Subscriptions */
    static final class C2318a implements C1666h {
        public boolean isUnsubscribed() {
            return true;
        }

        public void unsubscribe() {
        }

        C2318a() {
        }
    }

    public static C1666h m4910a(C1645a c1645a) {
        return C2317a.m6988a(c1645a);
    }
}
