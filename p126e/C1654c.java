package p126e;

/* compiled from: Observer */
public interface C1654c<T> {
    void onCompleted();

    void onError(Throwable th);

    void onNext(T t);
}
