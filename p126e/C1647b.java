package p126e;

import p126e.p128b.C1646b;
import p126e.p129c.p131c.C2309c;
import p126e.p129c.p138a.C2301a;
import p126e.p129c.p138a.C2302b;
import p126e.p129c.p138a.C2304e;
import p126e.p129c.p138a.C2457c;
import p126e.p129c.p138a.C2458d;
import p126e.p132d.C1657c;
import p126e.p132d.C1658e;

/* compiled from: Observable */
public class C1647b<T> {
    static final C1657c f4429b = C1658e.m4902a().m4905c();
    final C1644a<T> f4430a;

    /* compiled from: Observable */
    public interface C1644a<T> {
    }

    /* compiled from: Observable */
    public interface C2300b<R, T> extends C1646b<C2320g<? super R>, C2320g<? super T>> {
    }

    protected C1647b(C1644a<T> c1644a) {
        this.f4430a = c1644a;
    }

    public static <T> C1647b<T> m4889a(C1644a<T> c1644a) {
        return new C1647b(f4429b.m4901a(c1644a));
    }

    public final <R> C1647b<R> m4890a(C2300b<? extends R, ? super T> c2300b) {
        return new C1647b(new C2301a(this.f4430a, c2300b));
    }

    public C1665f<T> m4893a() {
        return new C1665f(C2302b.m6982a(this));
    }

    public final <R> C1647b<R> m4891a(C1646b<? super T, ? extends R> c1646b) {
        return m4890a(new C2457c(c1646b));
    }

    public final C1647b<T> m4894b(C1646b<Throwable, ? extends T> c1646b) {
        return m4890a(C2458d.m7825a(c1646b));
    }

    public final C1647b<T> m4892a(C1663e c1663e) {
        if (this instanceof C2309c) {
            return ((C2309c) this).m6983b(c1663e);
        }
        return C1647b.m4889a(new C2304e(this, c1663e));
    }
}
