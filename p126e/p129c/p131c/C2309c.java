package p126e.p129c.p131c;

import p126e.C1647b;
import p126e.C1647b.C1644a;
import p126e.C1663e;
import p126e.C1666h;
import p126e.p128b.C1645a;
import p126e.p128b.C1646b;
import p126e.p129c.p130b.C2305a;
import p126e.p132d.C1657c;
import p126e.p132d.C1658e;

/* compiled from: ScalarSynchronousObservable */
public final class C2309c<T> extends C1647b<T> {
    static C1657c f5793c = C1658e.m4902a().m4905c();
    static final boolean f5794d = Boolean.valueOf(System.getProperty("rx.just.strong-mode", "false")).booleanValue();
    final T f5795e;

    /* compiled from: ScalarSynchronousObservable */
    static final class C2308a<T> implements C1644a<T> {
        final T f5791a;
        final C1646b<C1645a, C1666h> f5792b;

        C2308a(T t, C1646b<C1645a, C1666h> c1646b) {
            this.f5791a = t;
            this.f5792b = c1646b;
        }
    }

    public C1647b<T> m6983b(final C1663e c1663e) {
        C1646b c23061;
        if (c1663e instanceof C2305a) {
            final C2305a c2305a = (C2305a) c1663e;
            c23061 = new C1646b<C1645a, C1666h>(this) {
                final /* synthetic */ C2309c f5788b;
            };
        } else {
            c23061 = new C1646b<C1645a, C1666h>(this) {
                final /* synthetic */ C2309c f5790b;
            };
        }
        return C1647b.m4889a((C1644a) new C2308a(this.f5795e, c23061));
    }
}
