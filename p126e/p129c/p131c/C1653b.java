package p126e.p129c.p131c;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: RxThreadFactory */
public final class C1653b extends AtomicLong implements ThreadFactory {
    public static final ThreadFactory f4435a = new C16521();
    final String f4436b;

    /* compiled from: RxThreadFactory */
    static class C16521 implements ThreadFactory {
        C16521() {
        }

        public Thread newThread(Runnable runnable) {
            throw new AssertionError("No threads allowed.");
        }
    }

    public C1653b(String str) {
        this.f4436b = str;
    }

    public Thread newThread(Runnable runnable) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.f4436b);
        stringBuilder.append(incrementAndGet());
        Thread thread = new Thread(runnable, stringBuilder.toString());
        thread.setDaemon(true);
        return thread;
    }
}
