package p126e.p129c.p131c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import p126e.C1666h;
import p126e.p127a.C1639b;

/* compiled from: SubscriptionList */
public final class C2310d implements C1666h {
    private LinkedList<C1666h> f5796a;
    private volatile boolean f5797b;

    public boolean isUnsubscribed() {
        return this.f5797b;
    }

    public void m6985a(C1666h c1666h) {
        if (!c1666h.isUnsubscribed()) {
            if (!this.f5797b) {
                synchronized (this) {
                    if (!this.f5797b) {
                        LinkedList linkedList = this.f5796a;
                        if (linkedList == null) {
                            linkedList = new LinkedList();
                            this.f5796a = linkedList;
                        }
                        linkedList.add(c1666h);
                        return;
                    }
                }
            }
            c1666h.unsubscribe();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void unsubscribe() {
        /*
        r2 = this;
        r0 = r2.f5797b;
        if (r0 != 0) goto L_0x001b;
    L_0x0004:
        monitor-enter(r2);
        r0 = r2.f5797b;	 Catch:{ all -> 0x0018 }
        if (r0 == 0) goto L_0x000b;
    L_0x0009:
        monitor-exit(r2);	 Catch:{ all -> 0x0018 }
        return;
    L_0x000b:
        r0 = 1;
        r2.f5797b = r0;	 Catch:{ all -> 0x0018 }
        r0 = r2.f5796a;	 Catch:{ all -> 0x0018 }
        r1 = 0;
        r2.f5796a = r1;	 Catch:{ all -> 0x0018 }
        monitor-exit(r2);	 Catch:{ all -> 0x0018 }
        p126e.p129c.p131c.C2310d.m6984a(r0);
        goto L_0x001b;
    L_0x0018:
        r0 = move-exception;
        monitor-exit(r2);	 Catch:{ all -> 0x0018 }
        throw r0;
    L_0x001b:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: e.c.c.d.unsubscribe():void");
    }

    private static void m6984a(Collection<C1666h> collection) {
        if (collection != null) {
            List list = null;
            for (C1666h unsubscribe : collection) {
                try {
                    unsubscribe.unsubscribe();
                } catch (Throwable th) {
                    if (list == null) {
                        list = new ArrayList();
                    }
                    list.add(th);
                }
            }
            C1639b.m4884a(list);
        }
    }
}
