package p126e.p129c.p131c;

import java.security.AccessController;
import java.security.PrivilegedAction;

/* compiled from: PlatformDependent */
public final class C1651a {
    private static final int f4433a = C1651a.m4898c();
    private static final boolean f4434b = (f4433a != 0);

    /* compiled from: PlatformDependent */
    static class C16501 implements PrivilegedAction<ClassLoader> {
        C16501() {
        }

        public /* synthetic */ Object run() {
            return m4895a();
        }

        public ClassLoader m4895a() {
            return ClassLoader.getSystemClassLoader();
        }
    }

    public static int m4896a() {
        return f4433a;
    }

    private static int m4898c() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r0 = "android.os.Build$VERSION";	 Catch:{ Exception -> 0x001d }
        r1 = 1;	 Catch:{ Exception -> 0x001d }
        r2 = p126e.p129c.p131c.C1651a.m4897b();	 Catch:{ Exception -> 0x001d }
        r0 = java.lang.Class.forName(r0, r1, r2);	 Catch:{ Exception -> 0x001d }
        r1 = "SDK_INT";	 Catch:{ Exception -> 0x001d }
        r0 = r0.getField(r1);	 Catch:{ Exception -> 0x001d }
        r1 = 0;	 Catch:{ Exception -> 0x001d }
        r0 = r0.get(r1);	 Catch:{ Exception -> 0x001d }
        r0 = (java.lang.Integer) r0;	 Catch:{ Exception -> 0x001d }
        r0 = r0.intValue();	 Catch:{ Exception -> 0x001d }
        return r0;
    L_0x001d:
        r0 = 0;
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: e.c.c.a.c():int");
    }

    static ClassLoader m4897b() {
        if (System.getSecurityManager() == null) {
            return ClassLoader.getSystemClassLoader();
        }
        return (ClassLoader) AccessController.doPrivileged(new C16501());
    }
}
