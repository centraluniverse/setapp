package p126e.p129c.p138a;

import p126e.C1647b;
import p126e.C1647b.C1644a;
import p126e.C1663e;

/* compiled from: OperatorSubscribeOn */
public final class C2304e<T> implements C1644a<T> {
    final C1663e f5782a;
    final C1647b<T> f5783b;

    public C2304e(C1647b<T> c1647b, C1663e c1663e) {
        this.f5782a = c1663e;
        this.f5783b = c1647b;
    }
}
