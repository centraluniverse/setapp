package p126e.p129c.p138a;

import p126e.C1647b;
import p126e.C1647b.C2300b;
import p126e.p128b.C1646b;

/* compiled from: OperatorOnErrorResumeNextViaFunction */
public final class C2458d<T> implements C2300b<T, T> {
    final C1646b<Throwable, ? extends C1647b<? extends T>> f6397a;

    public static <T> C2458d<T> m7825a(final C1646b<Throwable, ? extends T> c1646b) {
        return new C2458d(new C1646b<Throwable, C1647b<? extends T>>() {
        });
    }

    public C2458d(C1646b<Throwable, ? extends C1647b<? extends T>> c1646b) {
        this.f6397a = c1646b;
    }
}
