package p126e.p129c.p138a;

import p126e.C1647b.C1644a;
import p126e.C1647b.C2300b;
import p126e.p132d.C1657c;
import p126e.p132d.C1658e;

/* compiled from: OnSubscribeLift */
public final class C2301a<T, R> implements C1644a<R> {
    static final C1657c f5777a = C1658e.m4902a().m4905c();
    final C1644a<T> f5778b;
    final C2300b<? extends R, ? super T> f5779c;

    public C2301a(C1644a<T> c1644a, C2300b<? extends R, ? super T> c2300b) {
        this.f5778b = c1644a;
        this.f5779c = c2300b;
    }
}
