package p126e.p129c.p138a;

import p126e.C1647b;
import p126e.C1665f.C1664a;

/* compiled from: OnSubscribeSingle */
public class C2302b<T> implements C1664a<T> {
    private final C1647b<T> f5780a;

    public C2302b(C1647b<T> c1647b) {
        this.f5780a = c1647b;
    }

    public static <T> C2302b<T> m6982a(C1647b<T> c1647b) {
        return new C2302b(c1647b);
    }
}
