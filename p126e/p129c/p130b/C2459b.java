package p126e.p129c.p130b;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import p126e.C1663e.C2315a;
import p126e.C1666h;
import p126e.p127a.C1639b;
import p126e.p129c.p131c.C1651a;
import p126e.p129c.p131c.C1653b;
import p126e.p132d.C1658e;
import p126e.p132d.C1659f;

/* compiled from: NewThreadWorker */
public class C2459b extends C2315a implements C1666h {
    public static final int f6398b = Integer.getInteger("rx.scheduler.jdk6.purge-frequency-millis", 1000).intValue();
    private static final boolean f6399e;
    private static final ConcurrentHashMap<ScheduledThreadPoolExecutor, ScheduledThreadPoolExecutor> f6400f = new ConcurrentHashMap();
    private static final AtomicReference<ScheduledExecutorService> f6401g = new AtomicReference();
    private static volatile Object f6402h;
    private static final Object f6403i = new Object();
    volatile boolean f6404a;
    private final ScheduledExecutorService f6405c;
    private final C1659f f6406d;

    /* compiled from: NewThreadWorker */
    static class C16491 implements Runnable {
        C16491() {
        }

        public void run() {
            C2459b.m7826a();
        }
    }

    static {
        boolean z = Boolean.getBoolean("rx.scheduler.jdk6.purge-force");
        int a = C1651a.m4896a();
        z = !z && (a == 0 || a >= 21);
        f6399e = z;
    }

    public static void m7828a(ScheduledThreadPoolExecutor scheduledThreadPoolExecutor) {
        while (((ScheduledExecutorService) f6401g.get()) == null) {
            ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(1, new C1653b("RxSchedulerPurge-"));
            if (f6401g.compareAndSet(null, newScheduledThreadPool)) {
                newScheduledThreadPool.scheduleAtFixedRate(new C16491(), (long) f6398b, (long) f6398b, TimeUnit.MILLISECONDS);
                break;
            }
            newScheduledThreadPool.shutdownNow();
        }
        f6400f.putIfAbsent(scheduledThreadPoolExecutor, scheduledThreadPoolExecutor);
    }

    public static void m7827a(ScheduledExecutorService scheduledExecutorService) {
        f6400f.remove(scheduledExecutorService);
    }

    static void m7826a() {
        try {
            Iterator it = f6400f.keySet().iterator();
            while (it.hasNext()) {
                ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor) it.next();
                if (scheduledThreadPoolExecutor.isShutdown()) {
                    it.remove();
                } else {
                    scheduledThreadPoolExecutor.purge();
                }
            }
        } catch (Throwable th) {
            C1639b.m4883a(th);
            C1658e.m4902a().m4904b().m4900a(th);
        }
    }

    public static boolean m7829b(ScheduledExecutorService scheduledExecutorService) {
        if (f6399e) {
            Method c;
            if (scheduledExecutorService instanceof ScheduledThreadPoolExecutor) {
                Object obj = f6402h;
                if (obj == f6403i) {
                    return false;
                }
                if (obj == null) {
                    Object obj2;
                    c = C2459b.m7830c(scheduledExecutorService);
                    if (c != null) {
                        obj2 = c;
                    } else {
                        obj2 = f6403i;
                    }
                    f6402h = obj2;
                } else {
                    c = (Method) obj;
                }
            } else {
                c = C2459b.m7830c(scheduledExecutorService);
            }
            if (c != null) {
                try {
                    c.invoke(scheduledExecutorService, new Object[]{Boolean.valueOf(true)});
                    return true;
                } catch (ScheduledExecutorService scheduledExecutorService2) {
                    C1658e.m4902a().m4904b().m4900a(scheduledExecutorService2);
                }
            }
        }
        return false;
    }

    static Method m7830c(ScheduledExecutorService scheduledExecutorService) {
        for (Method method : scheduledExecutorService.getClass().getMethods()) {
            if (method.getName().equals("setRemoveOnCancelPolicy")) {
                Class[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length == 1 && parameterTypes[0] == Boolean.TYPE) {
                    return method;
                }
            }
        }
        return null;
    }

    public C2459b(ThreadFactory threadFactory) {
        threadFactory = Executors.newScheduledThreadPool(1, threadFactory);
        if (!C2459b.m7829b(threadFactory) && (threadFactory instanceof ScheduledThreadPoolExecutor)) {
            C2459b.m7828a((ScheduledThreadPoolExecutor) threadFactory);
        }
        this.f6406d = C1658e.m4902a().m4908f();
        this.f6405c = threadFactory;
    }

    public void unsubscribe() {
        this.f6404a = true;
        this.f6405c.shutdownNow();
        C2459b.m7827a(this.f6405c);
    }

    public boolean isUnsubscribed() {
        return this.f6404a;
    }
}
