package p126e.p129c.p130b;

import java.util.concurrent.ThreadFactory;
import p126e.C1663e;
import p126e.p129c.p131c.C1653b;

/* compiled from: EventLoopsScheduler */
public final class C2305a extends C1663e {
    static final int f5784b;
    static final C2492b f5785c = new C2492b(C1653b.f4435a);
    static final C1648a f5786d = new C1648a(null, 0);

    /* compiled from: EventLoopsScheduler */
    static final class C1648a {
        final int f4431a;
        final C2492b[] f4432b;

        C1648a(ThreadFactory threadFactory, int i) {
            this.f4431a = i;
            this.f4432b = new C2492b[i];
            for (int i2 = 0; i2 < i; i2++) {
                this.f4432b[i2] = new C2492b(threadFactory);
            }
        }
    }

    /* compiled from: EventLoopsScheduler */
    static final class C2492b extends C2459b {
        C2492b(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }

    static {
        int intValue = Integer.getInteger("rx.scheduler.max-computation-threads", 0).intValue();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        if (intValue <= 0 || intValue > availableProcessors) {
            intValue = availableProcessors;
        }
        f5784b = intValue;
        f5785c.unsubscribe();
    }
}
