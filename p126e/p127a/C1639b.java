package p126e.p127a;

import java.util.List;

/* compiled from: Exceptions */
public final class C1639b {
    public static void m4883a(Throwable th) {
        if (th instanceof C1642e) {
            throw ((C1642e) th);
        } else if (th instanceof C1641d) {
            throw ((C1641d) th);
        } else if (th instanceof C1640c) {
            throw ((C1640c) th);
        } else if (th instanceof StackOverflowError) {
            throw ((StackOverflowError) th);
        } else if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }

    public static void m4884a(List<? extends Throwable> list) {
        if (list != null && !list.isEmpty()) {
            if (list.size() == 1) {
                Throwable th = (Throwable) list.get(0);
                if (th instanceof RuntimeException) {
                    throw ((RuntimeException) th);
                } else if (th instanceof Error) {
                    throw ((Error) th);
                } else {
                    throw new RuntimeException(th);
                }
            }
            throw new C1638a(list);
        }
    }
}
