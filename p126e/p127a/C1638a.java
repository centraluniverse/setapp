package p126e.p127a;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

/* compiled from: CompositeException */
public final class C1638a extends RuntimeException {
    private final List<Throwable> f4421a;
    private final String f4422b;
    private Throwable f4423c;

    /* compiled from: CompositeException */
    static final class C1636a extends RuntimeException {
        static String f4420a = "Chain of Causes for CompositeException In Order Received =>";

        C1636a() {
        }

        public String getMessage() {
            return f4420a;
        }
    }

    /* compiled from: CompositeException */
    private static abstract class C1637b {
        abstract Object mo1916a();

        abstract void mo1917a(Object obj);

        private C1637b() {
        }
    }

    /* compiled from: CompositeException */
    private static class C2298c extends C1637b {
        private final PrintStream f5775a;

        C2298c(PrintStream printStream) {
            super();
            this.f5775a = printStream;
        }

        Object mo1916a() {
            return this.f5775a;
        }

        void mo1917a(Object obj) {
            this.f5775a.println(obj);
        }
    }

    /* compiled from: CompositeException */
    private static class C2299d extends C1637b {
        private final PrintWriter f5776a;

        C2299d(PrintWriter printWriter) {
            super();
            this.f5776a = printWriter;
        }

        Object mo1916a() {
            return this.f5776a;
        }

        void mo1917a(Object obj) {
            this.f5776a.println(obj);
        }
    }

    @Deprecated
    public C1638a(String str, Collection<? extends Throwable> collection) {
        this.f4423c = null;
        str = new LinkedHashSet();
        List arrayList = new ArrayList();
        if (collection != null) {
            for (Throwable th : collection) {
                if (th instanceof C1638a) {
                    str.addAll(((C1638a) th).m4882a());
                } else if (th != null) {
                    str.add(th);
                } else {
                    str.add(new NullPointerException());
                }
            }
        } else {
            str.add(new NullPointerException());
        }
        arrayList.addAll(str);
        this.f4421a = Collections.unmodifiableList(arrayList);
        str = new StringBuilder();
        str.append(this.f4421a.size());
        str.append(" exceptions occurred. ");
        this.f4422b = str.toString();
    }

    public C1638a(Collection<? extends Throwable> collection) {
        this(null, collection);
    }

    public List<Throwable> m4882a() {
        return this.f4421a;
    }

    public String getMessage() {
        return this.f4422b;
    }

    public synchronized java.lang.Throwable getCause() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r8 = this;
        monitor-enter(r8);
        r0 = r8.f4423c;	 Catch:{ all -> 0x0060 }
        if (r0 != 0) goto L_0x005c;	 Catch:{ all -> 0x0060 }
    L_0x0005:
        r0 = new e.a.a$a;	 Catch:{ all -> 0x0060 }
        r0.<init>();	 Catch:{ all -> 0x0060 }
        r1 = new java.util.HashSet;	 Catch:{ all -> 0x0060 }
        r1.<init>();	 Catch:{ all -> 0x0060 }
        r2 = r8.f4421a;	 Catch:{ all -> 0x0060 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0060 }
        r3 = r0;	 Catch:{ all -> 0x0060 }
    L_0x0016:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0060 }
        if (r4 == 0) goto L_0x005a;	 Catch:{ all -> 0x0060 }
    L_0x001c:
        r4 = r2.next();	 Catch:{ all -> 0x0060 }
        r4 = (java.lang.Throwable) r4;	 Catch:{ all -> 0x0060 }
        r5 = r1.contains(r4);	 Catch:{ all -> 0x0060 }
        if (r5 == 0) goto L_0x0029;	 Catch:{ all -> 0x0060 }
    L_0x0028:
        goto L_0x0016;	 Catch:{ all -> 0x0060 }
    L_0x0029:
        r1.add(r4);	 Catch:{ all -> 0x0060 }
        r5 = r8.m4878a(r4);	 Catch:{ all -> 0x0060 }
        r5 = r5.iterator();	 Catch:{ all -> 0x0060 }
    L_0x0034:
        r6 = r5.hasNext();	 Catch:{ all -> 0x0060 }
        if (r6 == 0) goto L_0x0052;	 Catch:{ all -> 0x0060 }
    L_0x003a:
        r6 = r5.next();	 Catch:{ all -> 0x0060 }
        r6 = (java.lang.Throwable) r6;	 Catch:{ all -> 0x0060 }
        r7 = r1.contains(r6);	 Catch:{ all -> 0x0060 }
        if (r7 == 0) goto L_0x004e;	 Catch:{ all -> 0x0060 }
    L_0x0046:
        r4 = new java.lang.RuntimeException;	 Catch:{ all -> 0x0060 }
        r6 = "Duplicate found in causal chain so cropping to prevent loop ...";	 Catch:{ all -> 0x0060 }
        r4.<init>(r6);	 Catch:{ all -> 0x0060 }
        goto L_0x0034;	 Catch:{ all -> 0x0060 }
    L_0x004e:
        r1.add(r6);	 Catch:{ all -> 0x0060 }
        goto L_0x0034;
    L_0x0052:
        r3.initCause(r4);	 Catch:{ Throwable -> 0x0055 }
    L_0x0055:
        r3 = r8.m4881b(r3);	 Catch:{ all -> 0x0060 }
        goto L_0x0016;	 Catch:{ all -> 0x0060 }
    L_0x005a:
        r8.f4423c = r0;	 Catch:{ all -> 0x0060 }
    L_0x005c:
        r0 = r8.f4423c;	 Catch:{ all -> 0x0060 }
        monitor-exit(r8);
        return r0;
    L_0x0060:
        r0 = move-exception;
        monitor-exit(r8);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.getCause():java.lang.Throwable");
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream printStream) {
        m4879a(new C2298c(printStream));
    }

    public void printStackTrace(PrintWriter printWriter) {
        m4879a(new C2299d(printWriter));
    }

    private void m4879a(C1637b c1637b) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this);
        stringBuilder.append("\n");
        for (Object obj : getStackTrace()) {
            stringBuilder.append("\tat ");
            stringBuilder.append(obj);
            stringBuilder.append("\n");
        }
        int i = 1;
        for (Throwable th : this.f4421a) {
            stringBuilder.append("  ComposedException ");
            stringBuilder.append(i);
            stringBuilder.append(" :");
            stringBuilder.append("\n");
            m4880a(stringBuilder, th, "\t");
            i++;
        }
        synchronized (c1637b.mo1916a()) {
            c1637b.mo1917a(stringBuilder.toString());
        }
    }

    private void m4880a(StringBuilder stringBuilder, Throwable th, String str) {
        stringBuilder.append(str);
        stringBuilder.append(th);
        stringBuilder.append("\n");
        for (Object obj : th.getStackTrace()) {
            stringBuilder.append("\t\tat ");
            stringBuilder.append(obj);
            stringBuilder.append("\n");
        }
        if (th.getCause() != null) {
            stringBuilder.append("\tCaused by: ");
            m4880a(stringBuilder, th.getCause(), "");
        }
    }

    private List<Throwable> m4878a(Throwable th) {
        List<Throwable> arrayList = new ArrayList();
        Throwable cause = th.getCause();
        if (cause != null) {
            if (cause != th) {
                while (true) {
                    arrayList.add(cause);
                    th = cause.getCause();
                    if (th == null) {
                        break;
                    } else if (th == cause) {
                        break;
                    } else {
                        cause = cause.getCause();
                    }
                }
                return arrayList;
            }
        }
        return arrayList;
    }

    private Throwable m4881b(Throwable th) {
        Throwable cause = th.getCause();
        if (cause != null) {
            if (cause != th) {
                while (true) {
                    th = cause.getCause();
                    if (th == null) {
                        break;
                    } else if (th == cause) {
                        break;
                    } else {
                        cause = cause.getCause();
                    }
                }
                return cause;
            }
        }
        return th;
    }
}
