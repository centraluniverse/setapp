package p126e.p132d;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: RxJavaPlugins */
public class C1658e {
    static final C1656b f4437a = new C23121();
    private static final C1658e f4438b = new C1658e();
    private final AtomicReference<C1656b> f4439c = new AtomicReference();
    private final AtomicReference<C1657c> f4440d = new AtomicReference();
    private final AtomicReference<C1660g> f4441e = new AtomicReference();
    private final AtomicReference<C1655a> f4442f = new AtomicReference();
    private final AtomicReference<C1659f> f4443g = new AtomicReference();

    /* compiled from: RxJavaPlugins */
    static class C23121 extends C1656b {
        C23121() {
        }
    }

    /* compiled from: RxJavaPlugins */
    class C23132 extends C1655a {
        final /* synthetic */ C1658e f5799a;

        C23132(C1658e c1658e) {
            this.f5799a = c1658e;
        }
    }

    public static C1658e m4902a() {
        return f4438b;
    }

    C1658e() {
    }

    public C1656b m4904b() {
        if (this.f4439c.get() == null) {
            Object a = C1658e.m4903a(C1656b.class, System.getProperties());
            if (a == null) {
                this.f4439c.compareAndSet(null, f4437a);
            } else {
                this.f4439c.compareAndSet(null, (C1656b) a);
            }
        }
        return (C1656b) this.f4439c.get();
    }

    public C1657c m4905c() {
        if (this.f4440d.get() == null) {
            Object a = C1658e.m4903a(C1657c.class, System.getProperties());
            if (a == null) {
                this.f4440d.compareAndSet(null, C2311d.m6986a());
            } else {
                this.f4440d.compareAndSet(null, (C1657c) a);
            }
        }
        return (C1657c) this.f4440d.get();
    }

    public C1660g m4906d() {
        if (this.f4441e.get() == null) {
            Object a = C1658e.m4903a(C1660g.class, System.getProperties());
            if (a == null) {
                this.f4441e.compareAndSet(null, C2314h.m6987a());
            } else {
                this.f4441e.compareAndSet(null, (C1660g) a);
            }
        }
        return (C1660g) this.f4441e.get();
    }

    public C1655a m4907e() {
        if (this.f4442f.get() == null) {
            Object a = C1658e.m4903a(C1655a.class, System.getProperties());
            if (a == null) {
                this.f4442f.compareAndSet(null, new C23132(this));
            } else {
                this.f4442f.compareAndSet(null, (C1655a) a);
            }
        }
        return (C1655a) this.f4442f.get();
    }

    static java.lang.Object m4903a(java.lang.Class<?> r6, java.util.Properties r7) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r7 = r7.clone();
        r7 = (java.util.Properties) r7;
        r0 = r6.getSimpleName();
        r1 = new java.lang.StringBuilder;
        r1.<init>();
        r2 = "rxjava.plugin.";
        r1.append(r2);
        r1.append(r0);
        r2 = ".implementation";
        r1.append(r2);
        r1 = r1.toString();
        r1 = r7.getProperty(r1);
        if (r1 != 0) goto L_0x00b5;
    L_0x0026:
        r2 = r7.entrySet();
        r2 = r2.iterator();
    L_0x002e:
        r3 = r2.hasNext();
        if (r3 == 0) goto L_0x00b5;
    L_0x0034:
        r3 = r2.next();
        r3 = (java.util.Map.Entry) r3;
        r4 = r3.getKey();
        r4 = r4.toString();
        r5 = "rxjava.plugin.";
        r5 = r4.startsWith(r5);
        if (r5 == 0) goto L_0x002e;
    L_0x004a:
        r5 = ".class";
        r5 = r4.endsWith(r5);
        if (r5 == 0) goto L_0x002e;
    L_0x0052:
        r3 = r3.getValue();
        r3 = r3.toString();
        r3 = r0.equals(r3);
        if (r3 == 0) goto L_0x002e;
    L_0x0060:
        r1 = 0;
        r2 = r4.length();
        r3 = ".class";
        r3 = r3.length();
        r2 = r2 - r3;
        r1 = r4.substring(r1, r2);
        r2 = "rxjava.plugin.";
        r2 = r2.length();
        r1 = r1.substring(r2);
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = "rxjava.plugin.";
        r2.append(r3);
        r2.append(r1);
        r1 = ".impl";
        r2.append(r1);
        r1 = r2.toString();
        r7 = r7.getProperty(r1);
        if (r7 != 0) goto L_0x00b6;
    L_0x0096:
        r6 = new java.lang.RuntimeException;
        r7 = new java.lang.StringBuilder;
        r7.<init>();
        r2 = "Implementing class declaration for ";
        r7.append(r2);
        r7.append(r0);
        r0 = " missing: ";
        r7.append(r0);
        r7.append(r1);
        r7 = r7.toString();
        r6.<init>(r7);
        throw r6;
    L_0x00b5:
        r7 = r1;
    L_0x00b6:
        if (r7 == 0) goto L_0x0138;
    L_0x00b8:
        r1 = java.lang.Class.forName(r7);	 Catch:{ ClassCastException -> 0x0116, ClassNotFoundException -> 0x00fb, InstantiationException -> 0x00e0, IllegalAccessException -> 0x00c5 }
        r6 = r1.asSubclass(r6);	 Catch:{ ClassCastException -> 0x0116, ClassNotFoundException -> 0x00fb, InstantiationException -> 0x00e0, IllegalAccessException -> 0x00c5 }
        r6 = r6.newInstance();	 Catch:{ ClassCastException -> 0x0116, ClassNotFoundException -> 0x00fb, InstantiationException -> 0x00e0, IllegalAccessException -> 0x00c5 }
        return r6;
    L_0x00c5:
        r6 = move-exception;
        r1 = new java.lang.RuntimeException;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r2.append(r0);
        r0 = " implementation not able to be accessed: ";
        r2.append(r0);
        r2.append(r7);
        r7 = r2.toString();
        r1.<init>(r7, r6);
        throw r1;
    L_0x00e0:
        r6 = move-exception;
        r1 = new java.lang.RuntimeException;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r2.append(r0);
        r0 = " implementation not able to be instantiated: ";
        r2.append(r0);
        r2.append(r7);
        r7 = r2.toString();
        r1.<init>(r7, r6);
        throw r1;
    L_0x00fb:
        r6 = move-exception;
        r1 = new java.lang.RuntimeException;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r2.append(r0);
        r0 = " implementation class not found: ";
        r2.append(r0);
        r2.append(r7);
        r7 = r2.toString();
        r1.<init>(r7, r6);
        throw r1;
    L_0x0116:
        r6 = new java.lang.RuntimeException;
        r1 = new java.lang.StringBuilder;
        r1.<init>();
        r1.append(r0);
        r2 = " implementation is not an instance of ";
        r1.append(r2);
        r1.append(r0);
        r0 = ": ";
        r1.append(r0);
        r1.append(r7);
        r7 = r1.toString();
        r6.<init>(r7);
        throw r6;
    L_0x0138:
        r6 = 0;
        return r6;
        */
        throw new UnsupportedOperationException("Method not decompiled: e.d.e.a(java.lang.Class, java.util.Properties):java.lang.Object");
    }

    public C1659f m4908f() {
        if (this.f4443g.get() == null) {
            Object a = C1658e.m4903a(C1659f.class, System.getProperties());
            if (a == null) {
                this.f4443g.compareAndSet(null, C1659f.m4909a());
            } else {
                this.f4443g.compareAndSet(null, (C1659f) a);
            }
        }
        return (C1659f) this.f4443g.get();
    }
}
