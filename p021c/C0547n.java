package p021c;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import p021c.C1885z.C1884a;
import p021c.p022a.C0488c;

/* compiled from: Dispatcher */
public final class C0547n {
    private int f824a = 64;
    private int f825b = 5;
    @Nullable
    private Runnable f826c;
    @Nullable
    private ExecutorService f827d;
    private final Deque<C1884a> f828e = new ArrayDeque();
    private final Deque<C1884a> f829f = new ArrayDeque();
    private final Deque<C1885z> f830g = new ArrayDeque();

    public synchronized ExecutorService m875a() {
        if (this.f827d == null) {
            this.f827d = new ThreadPoolExecutor(0, ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, 60, TimeUnit.SECONDS, new SynchronousQueue(), C0488c.m515a("OkHttp Dispatcher", false));
        }
        return this.f827d;
    }

    synchronized void m876a(C1884a c1884a) {
        if (this.f829f.size() >= this.f824a || m873c(c1884a) >= this.f825b) {
            this.f828e.add(c1884a);
        } else {
            this.f829f.add(c1884a);
            m875a().execute(c1884a);
        }
    }

    public synchronized void m878b() {
        for (C1884a b : this.f828e) {
            b.m5227b().mo1349c();
        }
        for (C1884a b2 : this.f829f) {
            b2.m5227b().mo1349c();
        }
        for (C1885z c : this.f830g) {
            c.mo1349c();
        }
    }

    private void m874d() {
        if (this.f829f.size() < this.f824a && !this.f828e.isEmpty()) {
            Iterator it = this.f828e.iterator();
            while (it.hasNext()) {
                C1884a c1884a = (C1884a) it.next();
                if (m873c(c1884a) < this.f825b) {
                    it.remove();
                    this.f829f.add(c1884a);
                    m875a().execute(c1884a);
                }
                if (this.f829f.size() >= this.f824a) {
                    return;
                }
            }
        }
    }

    private int m873c(C1884a c1884a) {
        int i = 0;
        for (C1884a a : this.f829f) {
            if (a.m5226a().equals(c1884a.m5226a())) {
                i++;
            }
        }
        return i;
    }

    synchronized void m877a(C1885z c1885z) {
        this.f830g.add(c1885z);
    }

    void m879b(C1884a c1884a) {
        m872a(this.f829f, c1884a, true);
    }

    void m880b(C1885z c1885z) {
        m872a(this.f830g, c1885z, false);
    }

    private <T> void m872a(Deque<T> deque, T t, boolean z) {
        synchronized (this) {
            if (deque.remove(t) == null) {
                throw new AssertionError("Call wasn't in-flight!");
            }
            if (z) {
                m874d();
            }
            deque = m881c();
            t = this.f826c;
        }
        if (deque == null && t != null) {
            t.run();
        }
    }

    public synchronized int m881c() {
        return this.f829f.size() + this.f830g.size();
    }
}
