package p021c;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import p021c.p022a.p025c.C0483e;

/* compiled from: CacheControl */
public final class C0530d {
    public static final C0530d f705a = new C0529a().m798a().m802d();
    public static final C0530d f706b = new C0529a().m801c().m799a(ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, TimeUnit.SECONDS).m802d();
    @Nullable
    String f707c;
    private final boolean f708d;
    private final boolean f709e;
    private final int f710f;
    private final int f711g;
    private final boolean f712h;
    private final boolean f713i;
    private final boolean f714j;
    private final int f715k;
    private final int f716l;
    private final boolean f717m;
    private final boolean f718n;
    private final boolean f719o;

    /* compiled from: CacheControl */
    public static final class C0529a {
        boolean f697a;
        boolean f698b;
        int f699c = -1;
        int f700d = -1;
        int f701e = -1;
        boolean f702f;
        boolean f703g;
        boolean f704h;

        public C0529a m798a() {
            this.f697a = true;
            return this;
        }

        public C0529a m800b() {
            this.f698b = true;
            return this;
        }

        public C0529a m799a(int i, TimeUnit timeUnit) {
            if (i < 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("maxStale < 0: ");
                stringBuilder.append(i);
                throw new IllegalArgumentException(stringBuilder.toString());
            }
            i = timeUnit.toSeconds((long) i);
            this.f700d = i > 2147483647L ? ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED : (int) i;
            return this;
        }

        public C0529a m801c() {
            this.f702f = true;
            return this;
        }

        public C0530d m802d() {
            return new C0530d(this);
        }
    }

    private C0530d(boolean z, boolean z2, int i, int i2, boolean z3, boolean z4, boolean z5, int i3, int i4, boolean z6, boolean z7, boolean z8, @Nullable String str) {
        this.f708d = z;
        this.f709e = z2;
        this.f710f = i;
        this.f711g = i2;
        this.f712h = z3;
        this.f713i = z4;
        this.f714j = z5;
        this.f715k = i3;
        this.f716l = i4;
        this.f717m = z6;
        this.f718n = z7;
        this.f719o = z8;
        this.f707c = str;
    }

    C0530d(C0529a c0529a) {
        this.f708d = c0529a.f697a;
        this.f709e = c0529a.f698b;
        this.f710f = c0529a.f699c;
        this.f711g = -1;
        this.f712h = false;
        this.f713i = false;
        this.f714j = false;
        this.f715k = c0529a.f700d;
        this.f716l = c0529a.f701e;
        this.f717m = c0529a.f702f;
        this.f718n = c0529a.f703g;
        this.f719o = c0529a.f704h;
    }

    public boolean m805a() {
        return this.f708d;
    }

    public boolean m806b() {
        return this.f709e;
    }

    public int m807c() {
        return this.f710f;
    }

    public boolean m808d() {
        return this.f712h;
    }

    public boolean m809e() {
        return this.f713i;
    }

    public boolean m810f() {
        return this.f714j;
    }

    public int m811g() {
        return this.f715k;
    }

    public int m812h() {
        return this.f716l;
    }

    public boolean m813i() {
        return this.f717m;
    }

    public boolean m814j() {
        return this.f719o;
    }

    public static C0530d m803a(C0554s c0554s) {
        C0554s c0554s2 = c0554s;
        int a = c0554s.m920a();
        Object obj = 1;
        String str = null;
        boolean z = false;
        boolean z2 = false;
        int i = -1;
        int i2 = -1;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        int i3 = -1;
        int i4 = -1;
        boolean z6 = false;
        boolean z7 = false;
        boolean z8 = false;
        for (int i5 = 0; i5 < a; i5++) {
            int i6;
            int a2;
            int a3;
            String trim;
            int a4;
            boolean z9;
            String trim2;
            String a5 = c0554s2.m921a(i5);
            String b = c0554s2.m924b(i5);
            if (a5.equalsIgnoreCase("Cache-Control")) {
                if (str == null) {
                    str = b;
                    for (i6 = 0; i6 < b.length(); i6 = a2) {
                        a3 = C0483e.m485a(b, i6, "=,;");
                        trim = b.substring(i6, a3).trim();
                        if (!(a3 == b.length() || b.charAt(a3) == ',')) {
                            if (b.charAt(a3) == ';') {
                                a4 = C0483e.m484a(b, a3 + 1);
                                if (a4 < b.length() || b.charAt(a4) != '\"') {
                                    z9 = true;
                                    a2 = C0483e.m485a(b, a4, ",;");
                                    trim2 = b.substring(a4, a2).trim();
                                } else {
                                    a4++;
                                    a2 = C0483e.m485a(b, a4, "\"");
                                    trim2 = b.substring(a4, a2);
                                    z9 = true;
                                    a2++;
                                }
                                if (!"no-cache".equalsIgnoreCase(trim)) {
                                    z = z9;
                                } else if ("no-store".equalsIgnoreCase(trim)) {
                                    if (!"max-age".equalsIgnoreCase(trim)) {
                                        i = C0483e.m490b(trim2, -1);
                                    } else if (!"s-maxage".equalsIgnoreCase(trim)) {
                                        i2 = C0483e.m490b(trim2, -1);
                                    } else if (!"private".equalsIgnoreCase(trim)) {
                                        z3 = z9;
                                    } else if (!"public".equalsIgnoreCase(trim)) {
                                        z4 = z9;
                                    } else if (!"must-revalidate".equalsIgnoreCase(trim)) {
                                        z5 = z9;
                                    } else if (!"max-stale".equalsIgnoreCase(trim)) {
                                        i3 = C0483e.m490b(trim2, ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
                                    } else if (!"min-fresh".equalsIgnoreCase(trim)) {
                                        i4 = C0483e.m490b(trim2, -1);
                                    } else if (!"only-if-cached".equalsIgnoreCase(trim)) {
                                        z6 = z9;
                                    } else if (!"no-transform".equalsIgnoreCase(trim)) {
                                        z7 = z9;
                                    } else if (!"immutable".equalsIgnoreCase(trim)) {
                                        z8 = z9;
                                    }
                                } else {
                                    z2 = z9;
                                }
                            }
                        }
                        z9 = true;
                        a2 = a3 + 1;
                        trim2 = null;
                        if (!"no-cache".equalsIgnoreCase(trim)) {
                            z = z9;
                        } else if ("no-store".equalsIgnoreCase(trim)) {
                            if (!"max-age".equalsIgnoreCase(trim)) {
                                i = C0483e.m490b(trim2, -1);
                            } else if (!"s-maxage".equalsIgnoreCase(trim)) {
                                i2 = C0483e.m490b(trim2, -1);
                            } else if (!"private".equalsIgnoreCase(trim)) {
                                z3 = z9;
                            } else if (!"public".equalsIgnoreCase(trim)) {
                                z4 = z9;
                            } else if (!"must-revalidate".equalsIgnoreCase(trim)) {
                                z5 = z9;
                            } else if (!"max-stale".equalsIgnoreCase(trim)) {
                                i3 = C0483e.m490b(trim2, ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
                            } else if (!"min-fresh".equalsIgnoreCase(trim)) {
                                i4 = C0483e.m490b(trim2, -1);
                            } else if (!"only-if-cached".equalsIgnoreCase(trim)) {
                                z6 = z9;
                            } else if (!"no-transform".equalsIgnoreCase(trim)) {
                                z7 = z9;
                            } else if (!"immutable".equalsIgnoreCase(trim)) {
                                z8 = z9;
                            }
                        } else {
                            z2 = z9;
                        }
                    }
                }
            } else if (!a5.equalsIgnoreCase("Pragma")) {
            }
            obj = null;
            while (i6 < b.length()) {
                a3 = C0483e.m485a(b, i6, "=,;");
                trim = b.substring(i6, a3).trim();
                if (b.charAt(a3) == ';') {
                    a4 = C0483e.m484a(b, a3 + 1);
                    if (a4 < b.length()) {
                    }
                    z9 = true;
                    a2 = C0483e.m485a(b, a4, ",;");
                    trim2 = b.substring(a4, a2).trim();
                    if (!"no-cache".equalsIgnoreCase(trim)) {
                        z = z9;
                    } else if ("no-store".equalsIgnoreCase(trim)) {
                        z2 = z9;
                    } else {
                        if (!"max-age".equalsIgnoreCase(trim)) {
                            i = C0483e.m490b(trim2, -1);
                        } else if (!"s-maxage".equalsIgnoreCase(trim)) {
                            i2 = C0483e.m490b(trim2, -1);
                        } else if (!"private".equalsIgnoreCase(trim)) {
                            z3 = z9;
                        } else if (!"public".equalsIgnoreCase(trim)) {
                            z4 = z9;
                        } else if (!"must-revalidate".equalsIgnoreCase(trim)) {
                            z5 = z9;
                        } else if (!"max-stale".equalsIgnoreCase(trim)) {
                            i3 = C0483e.m490b(trim2, ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
                        } else if (!"min-fresh".equalsIgnoreCase(trim)) {
                            i4 = C0483e.m490b(trim2, -1);
                        } else if (!"only-if-cached".equalsIgnoreCase(trim)) {
                            z6 = z9;
                        } else if (!"no-transform".equalsIgnoreCase(trim)) {
                            z7 = z9;
                        } else if (!"immutable".equalsIgnoreCase(trim)) {
                            z8 = z9;
                        }
                    }
                } else {
                    z9 = true;
                    a2 = a3 + 1;
                    trim2 = null;
                    if (!"no-cache".equalsIgnoreCase(trim)) {
                        z = z9;
                    } else if ("no-store".equalsIgnoreCase(trim)) {
                        if (!"max-age".equalsIgnoreCase(trim)) {
                            i = C0483e.m490b(trim2, -1);
                        } else if (!"s-maxage".equalsIgnoreCase(trim)) {
                            i2 = C0483e.m490b(trim2, -1);
                        } else if (!"private".equalsIgnoreCase(trim)) {
                            z3 = z9;
                        } else if (!"public".equalsIgnoreCase(trim)) {
                            z4 = z9;
                        } else if (!"must-revalidate".equalsIgnoreCase(trim)) {
                            z5 = z9;
                        } else if (!"max-stale".equalsIgnoreCase(trim)) {
                            i3 = C0483e.m490b(trim2, ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
                        } else if (!"min-fresh".equalsIgnoreCase(trim)) {
                            i4 = C0483e.m490b(trim2, -1);
                        } else if (!"only-if-cached".equalsIgnoreCase(trim)) {
                            z6 = z9;
                        } else if (!"no-transform".equalsIgnoreCase(trim)) {
                            z7 = z9;
                        } else if (!"immutable".equalsIgnoreCase(trim)) {
                            z8 = z9;
                        }
                    } else {
                        z2 = z9;
                    }
                }
            }
        }
        return new C0530d(z, z2, i, i2, z3, z4, z5, i3, i4, z6, z7, z8, obj == null ? null : str);
    }

    public String toString() {
        String str = this.f707c;
        if (str != null) {
            return str;
        }
        str = m804k();
        this.f707c = str;
        return str;
    }

    private String m804k() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.f708d) {
            stringBuilder.append("no-cache, ");
        }
        if (this.f709e) {
            stringBuilder.append("no-store, ");
        }
        if (this.f710f != -1) {
            stringBuilder.append("max-age=");
            stringBuilder.append(this.f710f);
            stringBuilder.append(", ");
        }
        if (this.f711g != -1) {
            stringBuilder.append("s-maxage=");
            stringBuilder.append(this.f711g);
            stringBuilder.append(", ");
        }
        if (this.f712h) {
            stringBuilder.append("private, ");
        }
        if (this.f713i) {
            stringBuilder.append("public, ");
        }
        if (this.f714j) {
            stringBuilder.append("must-revalidate, ");
        }
        if (this.f715k != -1) {
            stringBuilder.append("max-stale=");
            stringBuilder.append(this.f715k);
            stringBuilder.append(", ");
        }
        if (this.f716l != -1) {
            stringBuilder.append("min-fresh=");
            stringBuilder.append(this.f716l);
            stringBuilder.append(", ");
        }
        if (this.f717m) {
            stringBuilder.append("only-if-cached, ");
        }
        if (this.f718n) {
            stringBuilder.append("no-transform, ");
        }
        if (this.f719o) {
            stringBuilder.append("immutable, ");
        }
        if (stringBuilder.length() == 0) {
            return "";
        }
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
        return stringBuilder.toString();
    }
}
