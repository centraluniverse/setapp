package p021c;

import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import p021c.C0557t.C0556a;
import p021c.p022a.C0488c;

/* compiled from: Address */
public final class C0521a {
    final C0557t f627a;
    final C0548o f628b;
    final SocketFactory f629c;
    final C0527b f630d;
    final List<C0564y> f631e;
    final List<C0543k> f632f;
    final ProxySelector f633g;
    @Nullable
    final Proxy f634h;
    @Nullable
    final SSLSocketFactory f635i;
    @Nullable
    final HostnameVerifier f636j;
    @Nullable
    final C0536g f637k;

    public C0521a(String str, int i, C0548o c0548o, SocketFactory socketFactory, @Nullable SSLSocketFactory sSLSocketFactory, @Nullable HostnameVerifier hostnameVerifier, @Nullable C0536g c0536g, C0527b c0527b, @Nullable Proxy proxy, List<C0564y> list, List<C0543k> list2, ProxySelector proxySelector) {
        this.f627a = new C0556a().m939a(sSLSocketFactory != null ? "https" : "http").m946d(str).m938a(i).m945c();
        if (c0548o == null) {
            throw new NullPointerException("dns == null");
        }
        this.f628b = c0548o;
        if (socketFactory == null) {
            throw new NullPointerException("socketFactory == null");
        }
        this.f629c = socketFactory;
        if (c0527b == null) {
            throw new NullPointerException("proxyAuthenticator == null");
        }
        this.f630d = c0527b;
        if (list == null) {
            throw new NullPointerException("protocols == null");
        }
        this.f631e = C0488c.m513a((List) list);
        if (list2 == null) {
            throw new NullPointerException("connectionSpecs == null");
        }
        this.f632f = C0488c.m513a((List) list2);
        if (proxySelector == null) {
            throw new NullPointerException("proxySelector == null");
        }
        this.f633g = proxySelector;
        this.f634h = proxy;
        this.f635i = sSLSocketFactory;
        this.f636j = hostnameVerifier;
        this.f637k = c0536g;
    }

    public C0557t m730a() {
        return this.f627a;
    }

    public C0548o m732b() {
        return this.f628b;
    }

    public SocketFactory m733c() {
        return this.f629c;
    }

    public C0527b m734d() {
        return this.f630d;
    }

    public List<C0564y> m735e() {
        return this.f631e;
    }

    public List<C0543k> m736f() {
        return this.f632f;
    }

    public ProxySelector m737g() {
        return this.f633g;
    }

    @Nullable
    public Proxy m738h() {
        return this.f634h;
    }

    @Nullable
    public SSLSocketFactory m739i() {
        return this.f635i;
    }

    @Nullable
    public HostnameVerifier m740j() {
        return this.f636j;
    }

    @Nullable
    public C0536g m741k() {
        return this.f637k;
    }

    public boolean equals(@Nullable Object obj) {
        if (obj instanceof C0521a) {
            C0521a c0521a = (C0521a) obj;
            if (this.f627a.equals(c0521a.f627a) && m731a(c0521a) != null) {
                return true;
            }
        }
        return null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = 31 * (((((((((((((((((527 + this.f627a.hashCode()) * 31) + this.f628b.hashCode()) * 31) + this.f630d.hashCode()) * 31) + this.f631e.hashCode()) * 31) + this.f632f.hashCode()) * 31) + this.f633g.hashCode()) * 31) + (this.f634h != null ? this.f634h.hashCode() : 0)) * 31) + (this.f635i != null ? this.f635i.hashCode() : 0)) * 31) + (this.f636j != null ? this.f636j.hashCode() : 0));
        if (this.f637k != null) {
            i = this.f637k.hashCode();
        }
        return hashCode + i;
    }

    boolean m731a(C0521a c0521a) {
        return (this.f628b.equals(c0521a.f628b) && this.f630d.equals(c0521a.f630d) && this.f631e.equals(c0521a.f631e) && this.f632f.equals(c0521a.f632f) && this.f633g.equals(c0521a.f633g) && C0488c.m521a(this.f634h, c0521a.f634h) && C0488c.m521a(this.f635i, c0521a.f635i) && C0488c.m521a(this.f636j, c0521a.f636j) && C0488c.m521a(this.f637k, c0521a.f637k) && m730a().m970g() == c0521a.m730a().m970g()) ? true : null;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Address{");
        stringBuilder.append(this.f627a.m969f());
        stringBuilder.append(":");
        stringBuilder.append(this.f627a.m970g());
        if (this.f634h != null) {
            stringBuilder.append(", proxy=");
            stringBuilder.append(this.f634h);
        } else {
            stringBuilder.append(", proxySelector=");
            stringBuilder.append(this.f633g);
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }
}
