package p021c;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

/* compiled from: Dns */
public interface C0548o {
    public static final C0548o f831a = new C18771();

    /* compiled from: Dns */
    class C18771 implements C0548o {
        C18771() {
        }

        public List<InetAddress> mo1333a(String str) throws UnknownHostException {
            if (str != null) {
                return Arrays.asList(InetAddress.getAllByName(str));
            }
            throw new UnknownHostException("hostname == null");
        }
    }

    List<InetAddress> mo1333a(String str) throws UnknownHostException;
}
