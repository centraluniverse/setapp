package p021c;

import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import p008b.p009a.p010a.p011a.p012a.p014b.C0356a;
import p021c.C0532e.C0531a;
import p021c.C0550p.C0549a;
import p021c.C0554s.C0553a;
import p021c.ac.C0523a;
import p021c.p022a.C0469a;
import p021c.p022a.C0488c;
import p021c.p022a.p023a.C0468e;
import p021c.p022a.p024b.C0471d;
import p021c.p022a.p024b.C0476g;
import p021c.p022a.p024b.C1834c;
import p021c.p022a.p031i.C0517c;
import p021c.p022a.p031i.C0519e;

/* compiled from: OkHttpClient */
public class C1883x implements C0531a, Cloneable {
    static final List<C0564y> f4706a = C0488c.m514a(C0564y.HTTP_2, C0564y.HTTP_1_1);
    static final List<C0543k> f4707b = C0488c.m514a(C0543k.f793a, C0543k.f795c);
    final int f4708A;
    final int f4709B;
    final int f4710C;
    final C0547n f4711c;
    @Nullable
    final Proxy f4712d;
    final List<C0564y> f4713e;
    final List<C0543k> f4714f;
    final List<C0559u> f4715g;
    final List<C0559u> f4716h;
    final C0549a f4717i;
    final ProxySelector f4718j;
    final C0546m f4719k;
    @Nullable
    final C0528c f4720l;
    @Nullable
    final C0468e f4721m;
    final SocketFactory f4722n;
    @Nullable
    final SSLSocketFactory f4723o;
    @Nullable
    final C0517c f4724p;
    final HostnameVerifier f4725q;
    final C0536g f4726r;
    final C0527b f4727s;
    final C0527b f4728t;
    final C0541j f4729u;
    final C0548o f4730v;
    final boolean f4731w;
    final boolean f4732x;
    final boolean f4733y;
    final int f4734z;

    /* compiled from: OkHttpClient */
    public static final class C0563a {
        int f877A = 0;
        C0547n f878a = new C0547n();
        @Nullable
        Proxy f879b;
        List<C0564y> f880c = C1883x.f4706a;
        List<C0543k> f881d = C1883x.f4707b;
        final List<C0559u> f882e = new ArrayList();
        final List<C0559u> f883f = new ArrayList();
        C0549a f884g = C0550p.m884a(C0550p.f832a);
        ProxySelector f885h = ProxySelector.getDefault();
        C0546m f886i = C0546m.f823a;
        @Nullable
        C0528c f887j;
        @Nullable
        C0468e f888k;
        SocketFactory f889l = SocketFactory.getDefault();
        @Nullable
        SSLSocketFactory f890m;
        @Nullable
        C0517c f891n;
        HostnameVerifier f892o = C0519e.f626a;
        C0536g f893p = C0536g.f725a;
        C0527b f894q = C0527b.f694a;
        C0527b f895r = C0527b.f694a;
        C0541j f896s = new C0541j();
        C0548o f897t = C0548o.f831a;
        boolean f898u = true;
        boolean f899v = true;
        boolean f900w = true;
        int f901x = C0356a.DEFAULT_TIMEOUT;
        int f902y = C0356a.DEFAULT_TIMEOUT;
        int f903z = C0356a.DEFAULT_TIMEOUT;

        public C0563a m995a(long j, TimeUnit timeUnit) {
            this.f901x = C0488c.m505a("timeout", j, timeUnit);
            return this;
        }

        public C0563a m1000b(long j, TimeUnit timeUnit) {
            this.f902y = C0488c.m505a("timeout", j, timeUnit);
            return this;
        }

        public C0563a m1003c(long j, TimeUnit timeUnit) {
            this.f903z = C0488c.m505a("timeout", j, timeUnit);
            return this;
        }

        public C0563a m996a(C0546m c0546m) {
            if (c0546m == null) {
                throw new NullPointerException("cookieJar == null");
            }
            this.f886i = c0546m;
            return this;
        }

        public C0563a m998a(boolean z) {
            this.f898u = z;
            return this;
        }

        public C0563a m1002b(boolean z) {
            this.f899v = z;
            return this;
        }

        public C0563a m1004c(boolean z) {
            this.f900w = z;
            return this;
        }

        public C0563a m997a(C0559u c0559u) {
            if (c0559u == null) {
                throw new IllegalArgumentException("interceptor == null");
            }
            this.f882e.add(c0559u);
            return this;
        }

        public C0563a m1001b(C0559u c0559u) {
            if (c0559u == null) {
                throw new IllegalArgumentException("interceptor == null");
            }
            this.f883f.add(c0559u);
            return this;
        }

        public C1883x m999a() {
            return new C1883x(this);
        }
    }

    /* compiled from: OkHttpClient */
    class C18821 extends C0469a {
        C18821() {
        }

        public void mo1340a(C0553a c0553a, String str) {
            c0553a.m912a(str);
        }

        public void mo1341a(C0553a c0553a, String str, String str2) {
            c0553a.m916b(str, str2);
        }

        public boolean mo1343a(C0541j c0541j, C1834c c1834c) {
            return c0541j.m840b(c1834c);
        }

        public C1834c mo1336a(C0541j c0541j, C0521a c0521a, C0476g c0476g, ae aeVar) {
            return c0541j.m837a(c0521a, c0476g, aeVar);
        }

        public boolean mo1342a(C0521a c0521a, C0521a c0521a2) {
            return c0521a.m731a(c0521a2);
        }

        public Socket mo1338a(C0541j c0541j, C0521a c0521a, C0476g c0476g) {
            return c0541j.m838a(c0521a, c0476g);
        }

        public void mo1344b(C0541j c0541j, C1834c c1834c) {
            c0541j.m839a(c1834c);
        }

        public C0471d mo1337a(C0541j c0541j) {
            return c0541j.f783a;
        }

        public int mo1335a(C0523a c0523a) {
            return c0523a.f651c;
        }

        public void mo1339a(C0543k c0543k, SSLSocket sSLSocket, boolean z) {
            c0543k.m848a(sSLSocket, z);
        }
    }

    static {
        C0469a.f427a = new C18821();
    }

    public C1883x() {
        this(new C0563a());
    }

    C1883x(C0563a c0563a) {
        StringBuilder stringBuilder;
        this.f4711c = c0563a.f878a;
        this.f4712d = c0563a.f879b;
        this.f4713e = c0563a.f880c;
        this.f4714f = c0563a.f881d;
        this.f4715g = C0488c.m513a(c0563a.f882e);
        this.f4716h = C0488c.m513a(c0563a.f883f);
        this.f4717i = c0563a.f884g;
        this.f4718j = c0563a.f885h;
        this.f4719k = c0563a.f886i;
        this.f4720l = c0563a.f887j;
        this.f4721m = c0563a.f888k;
        this.f4722n = c0563a.f889l;
        loop0:
        while (true) {
            Object obj = null;
            for (C0543k c0543k : this.f4714f) {
                if (obj != null || c0543k.m849a()) {
                    obj = 1;
                }
            }
            break loop0;
        }
        if (c0563a.f890m == null) {
            if (obj != null) {
                X509TrustManager z = m5199z();
                this.f4723o = m5198a(z);
                this.f4724p = C0517c.m713a(z);
                this.f4725q = c0563a.f892o;
                this.f4726r = c0563a.f893p.m826a(this.f4724p);
                this.f4727s = c0563a.f894q;
                this.f4728t = c0563a.f895r;
                this.f4729u = c0563a.f896s;
                this.f4730v = c0563a.f897t;
                this.f4731w = c0563a.f898u;
                this.f4732x = c0563a.f899v;
                this.f4733y = c0563a.f900w;
                this.f4734z = c0563a.f901x;
                this.f4708A = c0563a.f902y;
                this.f4709B = c0563a.f903z;
                this.f4710C = c0563a.f877A;
                if (this.f4715g.contains(null) != null) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("Null interceptor: ");
                    stringBuilder.append(this.f4715g);
                    throw new IllegalStateException(stringBuilder.toString());
                } else if (this.f4716h.contains(null) != null) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("Null network interceptor: ");
                    stringBuilder.append(this.f4716h);
                    throw new IllegalStateException(stringBuilder.toString());
                }
            }
        }
        this.f4723o = c0563a.f890m;
        this.f4724p = c0563a.f891n;
        this.f4725q = c0563a.f892o;
        this.f4726r = c0563a.f893p.m826a(this.f4724p);
        this.f4727s = c0563a.f894q;
        this.f4728t = c0563a.f895r;
        this.f4729u = c0563a.f896s;
        this.f4730v = c0563a.f897t;
        this.f4731w = c0563a.f898u;
        this.f4732x = c0563a.f899v;
        this.f4733y = c0563a.f900w;
        this.f4734z = c0563a.f901x;
        this.f4708A = c0563a.f902y;
        this.f4709B = c0563a.f903z;
        this.f4710C = c0563a.f877A;
        if (this.f4715g.contains(null) != null) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Null interceptor: ");
            stringBuilder.append(this.f4715g);
            throw new IllegalStateException(stringBuilder.toString());
        } else if (this.f4716h.contains(null) != null) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Null network interceptor: ");
            stringBuilder.append(this.f4716h);
            throw new IllegalStateException(stringBuilder.toString());
        }
    }

    private X509TrustManager m5199z() {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init((KeyStore) null);
            TrustManager[] trustManagers = instance.getTrustManagers();
            if (trustManagers.length == 1) {
                if (trustManagers[0] instanceof X509TrustManager) {
                    return (X509TrustManager) trustManagers[0];
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected default trust managers:");
            stringBuilder.append(Arrays.toString(trustManagers));
            throw new IllegalStateException(stringBuilder.toString());
        } catch (Exception e) {
            throw C0488c.m507a("No System TLS", e);
        }
    }

    private SSLSocketFactory m5198a(X509TrustManager x509TrustManager) {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(null, new TrustManager[]{x509TrustManager}, null);
            return instance.getSocketFactory();
        } catch (Exception e) {
            throw C0488c.m507a("No System TLS", e);
        }
    }

    public int m5200a() {
        return this.f4734z;
    }

    public int m5202b() {
        return this.f4708A;
    }

    public int m5203c() {
        return this.f4709B;
    }

    public Proxy m5204d() {
        return this.f4712d;
    }

    public ProxySelector m5205e() {
        return this.f4718j;
    }

    public C0546m m5206f() {
        return this.f4719k;
    }

    public C0528c m5207g() {
        return this.f4720l;
    }

    C0468e m5208h() {
        return this.f4720l != null ? this.f4720l.f695a : this.f4721m;
    }

    public C0548o m5209i() {
        return this.f4730v;
    }

    public SocketFactory m5210j() {
        return this.f4722n;
    }

    public SSLSocketFactory m5211k() {
        return this.f4723o;
    }

    public HostnameVerifier m5212l() {
        return this.f4725q;
    }

    public C0536g m5213m() {
        return this.f4726r;
    }

    public C0527b m5214n() {
        return this.f4728t;
    }

    public C0527b m5215o() {
        return this.f4727s;
    }

    public C0541j m5216p() {
        return this.f4729u;
    }

    public boolean m5217q() {
        return this.f4731w;
    }

    public boolean m5218r() {
        return this.f4732x;
    }

    public boolean m5219s() {
        return this.f4733y;
    }

    public C0547n m5220t() {
        return this.f4711c;
    }

    public List<C0564y> m5221u() {
        return this.f4713e;
    }

    public List<C0543k> m5222v() {
        return this.f4714f;
    }

    public List<C0559u> m5223w() {
        return this.f4715g;
    }

    public List<C0559u> m5224x() {
        return this.f4716h;
    }

    public C0549a m5225y() {
        return this.f4717i;
    }

    public C0532e mo1345a(aa aaVar) {
        return C1885z.m5230a(this, aaVar, false);
    }
}
