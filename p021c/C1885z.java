package p021c;

import android.support.v4.app.NotificationCompat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import p021c.p022a.C0477b;
import p021c.p022a.p023a.C1832a;
import p021c.p022a.p024b.C1833a;
import p021c.p022a.p025c.C1835a;
import p021c.p022a.p025c.C1836b;
import p021c.p022a.p025c.C1837g;
import p021c.p022a.p025c.C1839j;
import p021c.p022a.p029g.C0515e;

/* compiled from: RealCall */
final class C1885z implements C0532e {
    final C1883x f4737a;
    final C1839j f4738b;
    final aa f4739c;
    final boolean f4740d;
    private C0550p f4741e;
    private boolean f4742f;

    /* compiled from: RealCall */
    final class C1884a extends C0477b {
        final /* synthetic */ C1885z f4735a;
        private final C0533f f4736c;

        C1884a(C1885z c1885z, C0533f c0533f) {
            this.f4735a = c1885z;
            super("OkHttp %s", c1885z.m5239g());
            this.f4736c = c0533f;
        }

        String m5226a() {
            return this.f4735a.f4739c.m751a().m969f();
        }

        C1885z m5227b() {
            return this.f4735a;
        }

        protected void mo1295c() {
            IOException e;
            C0515e b;
            StringBuilder stringBuilder;
            Object obj = 1;
            try {
                ac h = this.f4735a.m5240h();
                if (this.f4735a.f4738b.m5086b()) {
                    try {
                        this.f4736c.onFailure(this.f4735a, new IOException("Canceled"));
                    } catch (IOException e2) {
                        e = e2;
                        if (obj == null) {
                            this.f4735a.f4741e.m891a(this.f4735a, e);
                            this.f4736c.onFailure(this.f4735a, e);
                        } else {
                            try {
                                b = C0515e.m695b();
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("Callback failure for ");
                                stringBuilder.append(this.f4735a.m5238f());
                                b.mo1319a(4, stringBuilder.toString(), (Throwable) e);
                            } catch (Throwable th) {
                                this.f4735a.f4737a.m5220t().m879b(this);
                            }
                        }
                        this.f4735a.f4737a.m5220t().m879b(this);
                    }
                }
                this.f4736c.onResponse(this.f4735a, h);
            } catch (Throwable e3) {
                e = e3;
                obj = null;
                if (obj == null) {
                    b = C0515e.m695b();
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("Callback failure for ");
                    stringBuilder.append(this.f4735a.m5238f());
                    b.mo1319a(4, stringBuilder.toString(), (Throwable) e);
                } else {
                    this.f4735a.f4741e.m891a(this.f4735a, e);
                    this.f4736c.onFailure(this.f4735a, e);
                }
                this.f4735a.f4737a.m5220t().m879b(this);
            }
            this.f4735a.f4737a.m5220t().m879b(this);
        }
    }

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        return m5237e();
    }

    private C1885z(C1883x c1883x, aa aaVar, boolean z) {
        this.f4737a = c1883x;
        this.f4739c = aaVar;
        this.f4740d = z;
        this.f4738b = new C1839j(c1883x, z);
    }

    static C1885z m5230a(C1883x c1883x, aa aaVar, boolean z) {
        C1885z c1885z = new C1885z(c1883x, aaVar, z);
        c1885z.f4741e = c1883x.m5225y().mo1334a(c1885z);
        return c1885z;
    }

    public aa mo1346a() {
        return this.f4739c;
    }

    public ac mo1348b() throws IOException {
        synchronized (this) {
            if (this.f4742f) {
                throw new IllegalStateException("Already Executed");
            }
            this.f4742f = true;
        }
        m5231i();
        this.f4741e.m885a((C0532e) this);
        try {
            this.f4737a.m5220t().m877a(this);
            ac h = m5240h();
            if (h == null) {
                throw new IOException("Canceled");
            }
            this.f4737a.m5220t().m880b(this);
            return h;
        } catch (IOException e) {
            this.f4741e.m891a((C0532e) this, e);
            throw e;
        } catch (Throwable th) {
            this.f4737a.m5220t().m880b(this);
        }
    }

    private void m5231i() {
        this.f4738b.m5085a(C0515e.m695b().mo1317a("response.body().close()"));
    }

    public void mo1347a(C0533f c0533f) {
        synchronized (this) {
            if (this.f4742f) {
                throw new IllegalStateException("Already Executed");
            }
            this.f4742f = true;
        }
        m5231i();
        this.f4741e.m885a((C0532e) this);
        this.f4737a.m5220t().m876a(new C1884a(this, c0533f));
    }

    public void mo1349c() {
        this.f4738b.m5084a();
    }

    public boolean mo1350d() {
        return this.f4738b.m5086b();
    }

    public C1885z m5237e() {
        return C1885z.m5230a(this.f4737a, this.f4739c, this.f4740d);
    }

    String m5238f() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(mo1350d() ? "canceled " : "");
        stringBuilder.append(this.f4740d ? "web socket" : NotificationCompat.CATEGORY_CALL);
        stringBuilder.append(" to ");
        stringBuilder.append(m5239g());
        return stringBuilder.toString();
    }

    String m5239g() {
        return this.f4739c.m751a().m977n();
    }

    ac m5240h() throws IOException {
        List arrayList = new ArrayList();
        arrayList.addAll(this.f4737a.m5223w());
        arrayList.add(this.f4738b);
        arrayList.add(new C1835a(this.f4737a.m5206f()));
        arrayList.add(new C1832a(this.f4737a.m5208h()));
        arrayList.add(new C1833a(this.f4737a));
        if (!this.f4740d) {
            arrayList.addAll(this.f4737a.m5224x());
        }
        arrayList.add(new C1836b(this.f4740d));
        return new C1837g(arrayList, null, null, null, 0, this.f4739c, this, this.f4741e, this.f4737a.m5200a(), this.f4737a.m5202b(), this.f4737a.m5203c()).mo1276a(this.f4739c);
    }
}
