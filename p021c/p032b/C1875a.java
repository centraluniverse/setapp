package p021c.p032b;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;
import p021c.C0539i;
import p021c.C0554s;
import p021c.C0559u;
import p021c.C0559u.C0558a;
import p021c.C0560v;
import p021c.aa;
import p021c.ab;
import p021c.ac;
import p021c.ad;
import p021c.p022a.p025c.C0483e;
import p021c.p022a.p029g.C0515e;
import p125d.C2284e;
import p125d.C2453c;

/* compiled from: HttpLoggingInterceptor */
public final class C1875a implements C0559u {
    private static final Charset f4686a = Charset.forName("UTF-8");
    private final C0526b f4687b;
    private volatile C0525a f4688c;

    /* compiled from: HttpLoggingInterceptor */
    public enum C0525a {
        NONE,
        BASIC,
        HEADERS,
        BODY
    }

    /* compiled from: HttpLoggingInterceptor */
    public interface C0526b {
        public static final C0526b f693a = new C18741();

        /* compiled from: HttpLoggingInterceptor */
        class C18741 implements C0526b {
            C18741() {
            }

            public void mo1330a(String str) {
                C0515e.m695b().mo1319a(4, str, null);
            }
        }

        void mo1330a(String str);
    }

    public C1875a() {
        this(C0526b.f693a);
    }

    public C1875a(C0526b c0526b) {
        this.f4688c = C0525a.NONE;
        this.f4687b = c0526b;
    }

    public C1875a m5181a(C0525a c0525a) {
        if (c0525a == null) {
            throw new NullPointerException("level == null. Use Level.NONE instead.");
        }
        this.f4688c = c0525a;
        return this;
    }

    public ac mo1270a(C0558a c0558a) throws IOException {
        C0558a c0558a2 = c0558a;
        C0525a c0525a = this.f4688c;
        aa a = c0558a.mo1275a();
        if (c0525a == C0525a.NONE) {
            return c0558a2.mo1276a(a);
        }
        Object obj;
        ab d;
        C0539i b;
        StringBuilder stringBuilder;
        String stringBuilder2;
        C0526b c0526b;
        StringBuilder stringBuilder3;
        C0554s c;
        int a2;
        int i;
        String a3;
        C0526b c0526b2;
        C2453c c2453c;
        Charset charset;
        C0560v contentType;
        StringBuilder stringBuilder4;
        long nanoTime;
        ac a4;
        ad g;
        long contentLength;
        String stringBuilder5;
        char c2;
        String stringBuilder6;
        String stringBuilder7;
        C0554s f;
        int a5;
        int i2;
        C2453c b2;
        Charset charset2;
        C0560v contentType2;
        C0526b c0526b3;
        StringBuilder stringBuilder8;
        Object obj2 = 1;
        Object obj3 = c0525a == C0525a.BODY ? 1 : null;
        if (obj3 == null) {
            if (c0525a != C0525a.HEADERS) {
                obj = null;
                d = a.m755d();
                if (d != null) {
                    obj2 = null;
                }
                b = c0558a.mo1277b();
                stringBuilder = new StringBuilder();
                stringBuilder.append("--> ");
                stringBuilder.append(a.m753b());
                stringBuilder.append(' ');
                stringBuilder.append(a.m751a());
                if (b == null) {
                    StringBuilder stringBuilder9 = new StringBuilder();
                    stringBuilder9.append(" ");
                    stringBuilder9.append(b.mo1274b());
                    stringBuilder2 = stringBuilder9.toString();
                } else {
                    stringBuilder2 = "";
                }
                stringBuilder.append(stringBuilder2);
                stringBuilder2 = stringBuilder.toString();
                if (obj == null && obj2 != null) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append(stringBuilder2);
                    stringBuilder.append(" (");
                    stringBuilder.append(d.contentLength());
                    stringBuilder.append("-byte body)");
                    stringBuilder2 = stringBuilder.toString();
                }
                r1.f4687b.mo1330a(stringBuilder2);
                if (obj != null) {
                    if (obj2 != null) {
                        if (d.contentType() != null) {
                            c0526b = r1.f4687b;
                            stringBuilder3 = new StringBuilder();
                            stringBuilder3.append("Content-Type: ");
                            stringBuilder3.append(d.contentType());
                            c0526b.mo1330a(stringBuilder3.toString());
                        }
                        if (d.contentLength() != -1) {
                            c0526b = r1.f4687b;
                            stringBuilder3 = new StringBuilder();
                            stringBuilder3.append("Content-Length: ");
                            stringBuilder3.append(d.contentLength());
                            c0526b.mo1330a(stringBuilder3.toString());
                        }
                    }
                    c = a.m754c();
                    a2 = c.m920a();
                    for (i = 0; i < a2; i++) {
                        a3 = c.m921a(i);
                        if (!("Content-Type".equalsIgnoreCase(a3) || "Content-Length".equalsIgnoreCase(a3))) {
                            c0526b2 = r1.f4687b;
                            StringBuilder stringBuilder10 = new StringBuilder();
                            stringBuilder10.append(a3);
                            stringBuilder10.append(": ");
                            stringBuilder10.append(c.m924b(i));
                            c0526b2.mo1330a(stringBuilder10.toString());
                        }
                    }
                    if (obj3 != null) {
                        if (obj2 == null) {
                            if (m5178a(a.m754c())) {
                                c2453c = new C2453c();
                                d.writeTo(c2453c);
                                charset = f4686a;
                                contentType = d.contentType();
                                if (contentType != null) {
                                    charset = contentType.m988a(f4686a);
                                }
                                r1.f4687b.mo1330a("");
                                if (C1875a.m5179a(c2453c)) {
                                    c0526b2 = r1.f4687b;
                                    stringBuilder4 = new StringBuilder();
                                    stringBuilder4.append("--> END ");
                                    stringBuilder4.append(a.m753b());
                                    stringBuilder4.append(" (binary ");
                                    stringBuilder4.append(d.contentLength());
                                    stringBuilder4.append("-byte body omitted)");
                                    c0526b2.mo1330a(stringBuilder4.toString());
                                } else {
                                    r1.f4687b.mo1330a(c2453c.mo2512a(charset));
                                    c0526b2 = r1.f4687b;
                                    stringBuilder4 = new StringBuilder();
                                    stringBuilder4.append("--> END ");
                                    stringBuilder4.append(a.m753b());
                                    stringBuilder4.append(" (");
                                    stringBuilder4.append(d.contentLength());
                                    stringBuilder4.append("-byte body)");
                                    c0526b2.mo1330a(stringBuilder4.toString());
                                }
                            } else {
                                c0526b2 = r1.f4687b;
                                stringBuilder4 = new StringBuilder();
                                stringBuilder4.append("--> END ");
                                stringBuilder4.append(a.m753b());
                                stringBuilder4.append(" (encoded body omitted)");
                                c0526b2.mo1330a(stringBuilder4.toString());
                            }
                        }
                    }
                    c0526b2 = r1.f4687b;
                    stringBuilder4 = new StringBuilder();
                    stringBuilder4.append("--> END ");
                    stringBuilder4.append(a.m753b());
                    c0526b2.mo1330a(stringBuilder4.toString());
                }
                nanoTime = System.nanoTime();
                a4 = c0558a2.mo1276a(a);
                nanoTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanoTime);
                g = a4.m783g();
                contentLength = g.contentLength();
                if (contentLength == -1) {
                    StringBuilder stringBuilder11 = new StringBuilder();
                    stringBuilder11.append(contentLength);
                    stringBuilder11.append("-byte");
                    stringBuilder5 = stringBuilder11.toString();
                } else {
                    stringBuilder5 = "unknown-length";
                }
                C0526b c0526b4 = r1.f4687b;
                stringBuilder = new StringBuilder();
                stringBuilder.append("<-- ");
                stringBuilder.append(a4.m778b());
                if (a4.m780d().isEmpty()) {
                    stringBuilder3 = new StringBuilder();
                    c2 = ' ';
                    stringBuilder3.append(' ');
                    stringBuilder3.append(a4.m780d());
                    stringBuilder6 = stringBuilder3.toString();
                } else {
                    stringBuilder6 = "";
                    c2 = ' ';
                }
                stringBuilder.append(stringBuilder6);
                stringBuilder.append(c2);
                stringBuilder.append(a4.m775a().m751a());
                stringBuilder.append(" (");
                stringBuilder.append(nanoTime);
                stringBuilder.append("ms");
                if (obj != null) {
                    StringBuilder stringBuilder12 = new StringBuilder();
                    stringBuilder12.append(", ");
                    stringBuilder12.append(stringBuilder5);
                    stringBuilder12.append(" body");
                    stringBuilder7 = stringBuilder12.toString();
                } else {
                    stringBuilder7 = "";
                }
                stringBuilder.append(stringBuilder7);
                stringBuilder.append(')');
                c0526b4.mo1330a(stringBuilder.toString());
                if (obj != null) {
                    f = a4.m782f();
                    a5 = f.m920a();
                    for (i2 = 0; i2 < a5; i2++) {
                        C0526b c0526b5 = r1.f4687b;
                        StringBuilder stringBuilder13 = new StringBuilder();
                        stringBuilder13.append(f.m921a(i2));
                        stringBuilder13.append(": ");
                        stringBuilder13.append(f.m924b(i2));
                        c0526b5.mo1330a(stringBuilder13.toString());
                    }
                    if (obj3 != null) {
                        if (!C0483e.m491b(a4)) {
                            if (m5178a(a4.m782f())) {
                                C2284e source = g.source();
                                source.mo2519b(Long.MAX_VALUE);
                                b2 = source.mo2516b();
                                charset2 = f4686a;
                                contentType2 = g.contentType();
                                if (contentType2 != null) {
                                    charset2 = contentType2.m988a(f4686a);
                                }
                                if (C1875a.m5179a(b2)) {
                                    r1.f4687b.mo1330a("");
                                    c0526b3 = r1.f4687b;
                                    stringBuilder8 = new StringBuilder();
                                    stringBuilder8.append("<-- END HTTP (binary ");
                                    stringBuilder8.append(b2.m7705a());
                                    stringBuilder8.append("-byte body omitted)");
                                    c0526b3.mo1330a(stringBuilder8.toString());
                                    return a4;
                                }
                                if (contentLength != 0) {
                                    r1.f4687b.mo1330a("");
                                    r1.f4687b.mo1330a(b2.m7777x().mo2512a(charset2));
                                }
                                c0526b3 = r1.f4687b;
                                stringBuilder8 = new StringBuilder();
                                stringBuilder8.append("<-- END HTTP (");
                                stringBuilder8.append(b2.m7705a());
                                stringBuilder8.append("-byte body)");
                                c0526b3.mo1330a(stringBuilder8.toString());
                            } else {
                                r1.f4687b.mo1330a("<-- END HTTP (encoded body omitted)");
                            }
                        }
                    }
                    r1.f4687b.mo1330a("<-- END HTTP");
                }
                return a4;
            }
        }
        obj = 1;
        d = a.m755d();
        if (d != null) {
            obj2 = null;
        }
        b = c0558a.mo1277b();
        stringBuilder = new StringBuilder();
        stringBuilder.append("--> ");
        stringBuilder.append(a.m753b());
        stringBuilder.append(' ');
        stringBuilder.append(a.m751a());
        if (b == null) {
            stringBuilder2 = "";
        } else {
            StringBuilder stringBuilder92 = new StringBuilder();
            stringBuilder92.append(" ");
            stringBuilder92.append(b.mo1274b());
            stringBuilder2 = stringBuilder92.toString();
        }
        stringBuilder.append(stringBuilder2);
        stringBuilder2 = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(stringBuilder2);
        stringBuilder.append(" (");
        stringBuilder.append(d.contentLength());
        stringBuilder.append("-byte body)");
        stringBuilder2 = stringBuilder.toString();
        r1.f4687b.mo1330a(stringBuilder2);
        if (obj != null) {
            if (obj2 != null) {
                if (d.contentType() != null) {
                    c0526b = r1.f4687b;
                    stringBuilder3 = new StringBuilder();
                    stringBuilder3.append("Content-Type: ");
                    stringBuilder3.append(d.contentType());
                    c0526b.mo1330a(stringBuilder3.toString());
                }
                if (d.contentLength() != -1) {
                    c0526b = r1.f4687b;
                    stringBuilder3 = new StringBuilder();
                    stringBuilder3.append("Content-Length: ");
                    stringBuilder3.append(d.contentLength());
                    c0526b.mo1330a(stringBuilder3.toString());
                }
            }
            c = a.m754c();
            a2 = c.m920a();
            for (i = 0; i < a2; i++) {
                a3 = c.m921a(i);
                c0526b2 = r1.f4687b;
                StringBuilder stringBuilder102 = new StringBuilder();
                stringBuilder102.append(a3);
                stringBuilder102.append(": ");
                stringBuilder102.append(c.m924b(i));
                c0526b2.mo1330a(stringBuilder102.toString());
            }
            if (obj3 != null) {
                if (obj2 == null) {
                    if (m5178a(a.m754c())) {
                        c2453c = new C2453c();
                        d.writeTo(c2453c);
                        charset = f4686a;
                        contentType = d.contentType();
                        if (contentType != null) {
                            charset = contentType.m988a(f4686a);
                        }
                        r1.f4687b.mo1330a("");
                        if (C1875a.m5179a(c2453c)) {
                            c0526b2 = r1.f4687b;
                            stringBuilder4 = new StringBuilder();
                            stringBuilder4.append("--> END ");
                            stringBuilder4.append(a.m753b());
                            stringBuilder4.append(" (binary ");
                            stringBuilder4.append(d.contentLength());
                            stringBuilder4.append("-byte body omitted)");
                            c0526b2.mo1330a(stringBuilder4.toString());
                        } else {
                            r1.f4687b.mo1330a(c2453c.mo2512a(charset));
                            c0526b2 = r1.f4687b;
                            stringBuilder4 = new StringBuilder();
                            stringBuilder4.append("--> END ");
                            stringBuilder4.append(a.m753b());
                            stringBuilder4.append(" (");
                            stringBuilder4.append(d.contentLength());
                            stringBuilder4.append("-byte body)");
                            c0526b2.mo1330a(stringBuilder4.toString());
                        }
                    } else {
                        c0526b2 = r1.f4687b;
                        stringBuilder4 = new StringBuilder();
                        stringBuilder4.append("--> END ");
                        stringBuilder4.append(a.m753b());
                        stringBuilder4.append(" (encoded body omitted)");
                        c0526b2.mo1330a(stringBuilder4.toString());
                    }
                }
            }
            c0526b2 = r1.f4687b;
            stringBuilder4 = new StringBuilder();
            stringBuilder4.append("--> END ");
            stringBuilder4.append(a.m753b());
            c0526b2.mo1330a(stringBuilder4.toString());
        }
        nanoTime = System.nanoTime();
        try {
            a4 = c0558a2.mo1276a(a);
            nanoTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanoTime);
            g = a4.m783g();
            contentLength = g.contentLength();
            if (contentLength == -1) {
                stringBuilder5 = "unknown-length";
            } else {
                StringBuilder stringBuilder112 = new StringBuilder();
                stringBuilder112.append(contentLength);
                stringBuilder112.append("-byte");
                stringBuilder5 = stringBuilder112.toString();
            }
            C0526b c0526b42 = r1.f4687b;
            stringBuilder = new StringBuilder();
            stringBuilder.append("<-- ");
            stringBuilder.append(a4.m778b());
            if (a4.m780d().isEmpty()) {
                stringBuilder3 = new StringBuilder();
                c2 = ' ';
                stringBuilder3.append(' ');
                stringBuilder3.append(a4.m780d());
                stringBuilder6 = stringBuilder3.toString();
            } else {
                stringBuilder6 = "";
                c2 = ' ';
            }
            stringBuilder.append(stringBuilder6);
            stringBuilder.append(c2);
            stringBuilder.append(a4.m775a().m751a());
            stringBuilder.append(" (");
            stringBuilder.append(nanoTime);
            stringBuilder.append("ms");
            if (obj != null) {
                stringBuilder7 = "";
            } else {
                StringBuilder stringBuilder122 = new StringBuilder();
                stringBuilder122.append(", ");
                stringBuilder122.append(stringBuilder5);
                stringBuilder122.append(" body");
                stringBuilder7 = stringBuilder122.toString();
            }
            stringBuilder.append(stringBuilder7);
            stringBuilder.append(')');
            c0526b42.mo1330a(stringBuilder.toString());
            if (obj != null) {
                f = a4.m782f();
                a5 = f.m920a();
                for (i2 = 0; i2 < a5; i2++) {
                    C0526b c0526b52 = r1.f4687b;
                    StringBuilder stringBuilder132 = new StringBuilder();
                    stringBuilder132.append(f.m921a(i2));
                    stringBuilder132.append(": ");
                    stringBuilder132.append(f.m924b(i2));
                    c0526b52.mo1330a(stringBuilder132.toString());
                }
                if (obj3 != null) {
                    if (!C0483e.m491b(a4)) {
                        if (m5178a(a4.m782f())) {
                            C2284e source2 = g.source();
                            source2.mo2519b(Long.MAX_VALUE);
                            b2 = source2.mo2516b();
                            charset2 = f4686a;
                            contentType2 = g.contentType();
                            if (contentType2 != null) {
                                charset2 = contentType2.m988a(f4686a);
                            }
                            if (C1875a.m5179a(b2)) {
                                if (contentLength != 0) {
                                    r1.f4687b.mo1330a("");
                                    r1.f4687b.mo1330a(b2.m7777x().mo2512a(charset2));
                                }
                                c0526b3 = r1.f4687b;
                                stringBuilder8 = new StringBuilder();
                                stringBuilder8.append("<-- END HTTP (");
                                stringBuilder8.append(b2.m7705a());
                                stringBuilder8.append("-byte body)");
                                c0526b3.mo1330a(stringBuilder8.toString());
                            } else {
                                r1.f4687b.mo1330a("");
                                c0526b3 = r1.f4687b;
                                stringBuilder8 = new StringBuilder();
                                stringBuilder8.append("<-- END HTTP (binary ");
                                stringBuilder8.append(b2.m7705a());
                                stringBuilder8.append("-byte body omitted)");
                                c0526b3.mo1330a(stringBuilder8.toString());
                                return a4;
                            }
                        }
                        r1.f4687b.mo1330a("<-- END HTTP (encoded body omitted)");
                    }
                }
                r1.f4687b.mo1330a("<-- END HTTP");
            }
            return a4;
        } catch (Exception e) {
            C0526b c0526b6 = r1.f4687b;
            StringBuilder stringBuilder14 = new StringBuilder();
            stringBuilder14.append("<-- HTTP FAILED: ");
            stringBuilder14.append(e);
            c0526b6.mo1330a(stringBuilder14.toString());
            throw e;
        }
    }

    static boolean m5179a(p125d.C2453c r8) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r0 = 0;
        r7 = new d.c;	 Catch:{ EOFException -> 0x0040 }
        r7.<init>();	 Catch:{ EOFException -> 0x0040 }
        r1 = r8.m7705a();	 Catch:{ EOFException -> 0x0040 }
        r3 = 64;	 Catch:{ EOFException -> 0x0040 }
        r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1));	 Catch:{ EOFException -> 0x0040 }
        if (r5 >= 0) goto L_0x0016;	 Catch:{ EOFException -> 0x0040 }
    L_0x0010:
        r1 = r8.m7705a();	 Catch:{ EOFException -> 0x0040 }
        r5 = r1;	 Catch:{ EOFException -> 0x0040 }
        goto L_0x0017;	 Catch:{ EOFException -> 0x0040 }
    L_0x0016:
        r5 = r3;	 Catch:{ EOFException -> 0x0040 }
    L_0x0017:
        r3 = 0;	 Catch:{ EOFException -> 0x0040 }
        r1 = r8;	 Catch:{ EOFException -> 0x0040 }
        r2 = r7;	 Catch:{ EOFException -> 0x0040 }
        r1.m7711a(r2, r3, r5);	 Catch:{ EOFException -> 0x0040 }
        r8 = r0;	 Catch:{ EOFException -> 0x0040 }
    L_0x001f:
        r1 = 16;	 Catch:{ EOFException -> 0x0040 }
        if (r8 >= r1) goto L_0x003e;	 Catch:{ EOFException -> 0x0040 }
    L_0x0023:
        r1 = r7.mo2525f();	 Catch:{ EOFException -> 0x0040 }
        if (r1 == 0) goto L_0x002a;	 Catch:{ EOFException -> 0x0040 }
    L_0x0029:
        goto L_0x003e;	 Catch:{ EOFException -> 0x0040 }
    L_0x002a:
        r1 = r7.m7774u();	 Catch:{ EOFException -> 0x0040 }
        r2 = java.lang.Character.isISOControl(r1);	 Catch:{ EOFException -> 0x0040 }
        if (r2 == 0) goto L_0x003b;	 Catch:{ EOFException -> 0x0040 }
    L_0x0034:
        r1 = java.lang.Character.isWhitespace(r1);	 Catch:{ EOFException -> 0x0040 }
        if (r1 != 0) goto L_0x003b;
    L_0x003a:
        return r0;
    L_0x003b:
        r8 = r8 + 1;
        goto L_0x001f;
    L_0x003e:
        r8 = 1;
        return r8;
    L_0x0040:
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.b.a.a(d.c):boolean");
    }

    private boolean m5178a(C0554s c0554s) {
        c0554s = c0554s.m922a("Content-Encoding");
        return (c0554s == null || c0554s.equalsIgnoreCase("identity") != null) ? null : true;
    }
}
