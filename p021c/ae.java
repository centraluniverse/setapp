package p021c;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import javax.annotation.Nullable;

/* compiled from: Route */
public final class ae {
    final C0521a f678a;
    final Proxy f679b;
    final InetSocketAddress f680c;

    public ae(C0521a c0521a, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (c0521a == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else {
            this.f678a = c0521a;
            this.f679b = proxy;
            this.f680c = inetSocketAddress;
        }
    }

    public C0521a m790a() {
        return this.f678a;
    }

    public Proxy m791b() {
        return this.f679b;
    }

    public InetSocketAddress m792c() {
        return this.f680c;
    }

    public boolean m793d() {
        return this.f678a.f635i != null && this.f679b.type() == Type.HTTP;
    }

    public boolean equals(@Nullable Object obj) {
        if (obj instanceof ae) {
            ae aeVar = (ae) obj;
            if (aeVar.f678a.equals(this.f678a) && aeVar.f679b.equals(this.f679b) && aeVar.f680c.equals(this.f680c) != null) {
                return true;
            }
        }
        return null;
    }

    public int hashCode() {
        return (31 * (((527 + this.f678a.hashCode()) * 31) + this.f679b.hashCode())) + this.f680c.hashCode();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Route{");
        stringBuilder.append(this.f680c);
        stringBuilder.append("}");
        return stringBuilder.toString();
    }
}
