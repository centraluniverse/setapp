package p021c;

import java.io.IOException;

/* compiled from: Protocol */
public enum C0564y {
    HTTP_1_0("http/1.0"),
    HTTP_1_1("http/1.1"),
    SPDY_3("spdy/3.1"),
    HTTP_2("h2");
    
    private final String f909e;

    private C0564y(String str) {
        this.f909e = str;
    }

    public static C0564y m1005a(String str) throws IOException {
        if (str.equals(HTTP_1_0.f909e)) {
            return HTTP_1_0;
        }
        if (str.equals(HTTP_1_1.f909e)) {
            return HTTP_1_1;
        }
        if (str.equals(HTTP_2.f909e)) {
            return HTTP_2;
        }
        if (str.equals(SPDY_3.f909e)) {
            return SPDY_3;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected protocol: ");
        stringBuilder.append(str);
        throw new IOException(stringBuilder.toString());
    }

    public String toString() {
        return this.f909e;
    }
}
