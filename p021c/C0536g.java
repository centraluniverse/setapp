package p021c;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import p021c.p022a.C0488c;
import p021c.p022a.p031i.C0517c;
import p125d.C1624f;

/* compiled from: CertificatePinner */
public final class C0536g {
    public static final C0536g f725a = new C0534a().m821a();
    private final Set<C0535b> f726b;
    @Nullable
    private final C0517c f727c;

    /* compiled from: CertificatePinner */
    public static final class C0534a {
        private final List<C0535b> f720a = new ArrayList();

        public C0536g m821a() {
            return new C0536g(new LinkedHashSet(this.f720a), null);
        }
    }

    /* compiled from: CertificatePinner */
    static final class C0535b {
        final String f721a;
        final String f722b;
        final String f723c;
        final C1624f f724d;

        boolean m822a(String str) {
            if (!this.f721a.startsWith("*.")) {
                return str.equals(this.f722b);
            }
            int indexOf = str.indexOf(46);
            boolean z = true;
            if ((str.length() - indexOf) - 1 == this.f722b.length()) {
                if (str.regionMatches(false, indexOf + 1, this.f722b, 0, this.f722b.length()) != null) {
                    return z;
                }
            }
            z = false;
            return z;
        }

        public boolean equals(Object obj) {
            if (obj instanceof C0535b) {
                C0535b c0535b = (C0535b) obj;
                if (this.f721a.equals(c0535b.f721a) && this.f723c.equals(c0535b.f723c) && this.f724d.equals(c0535b.f724d) != null) {
                    return true;
                }
            }
            return null;
        }

        public int hashCode() {
            return (31 * (((527 + this.f721a.hashCode()) * 31) + this.f723c.hashCode())) + this.f724d.hashCode();
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(this.f723c);
            stringBuilder.append(this.f724d.mo1906b());
            return stringBuilder.toString();
        }
    }

    C0536g(Set<C0535b> set, @Nullable C0517c c0517c) {
        this.f726b = set;
        this.f727c = c0517c;
    }

    public boolean equals(@Nullable Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (obj instanceof C0536g) {
            C0536g c0536g = (C0536g) obj;
            if (C0488c.m521a(this.f727c, c0536g.f727c) && this.f726b.equals(c0536g.f726b) != null) {
                return z;
            }
        }
        z = false;
        return z;
    }

    public int hashCode() {
        return (31 * (this.f727c != null ? this.f727c.hashCode() : 0)) + this.f726b.hashCode();
    }

    public void m828a(String str, List<Certificate> list) throws SSLPeerUnverifiedException {
        List a = m827a(str);
        if (!a.isEmpty()) {
            int i;
            if (this.f727c != null) {
                list = this.f727c.mo1314a(list, str);
            }
            int size = list.size();
            int i2 = 0;
            for (i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = a.size();
                Object obj = null;
                Object obj2 = obj;
                for (int i3 = 0; i3 < size2; i3++) {
                    C0535b c0535b = (C0535b) a.get(i3);
                    if (c0535b.f723c.equals("sha256/")) {
                        if (obj == null) {
                            obj = C0536g.m825b(x509Certificate);
                        }
                        if (c0535b.f724d.equals(obj)) {
                            return;
                        }
                    } else if (c0535b.f723c.equals("sha1/")) {
                        if (obj2 == null) {
                            obj2 = C0536g.m823a(x509Certificate);
                        }
                        if (c0535b.f724d.equals(obj2)) {
                            return;
                        }
                    } else {
                        list = new StringBuilder();
                        list.append("unsupported hashAlgorithm: ");
                        list.append(c0535b.f723c);
                        throw new AssertionError(list.toString());
                    }
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Certificate pinning failure!");
            stringBuilder.append("\n  Peer certificate chain:");
            i = list.size();
            for (int i4 = 0; i4 < i; i4++) {
                Certificate certificate = (X509Certificate) list.get(i4);
                stringBuilder.append("\n    ");
                stringBuilder.append(C0536g.m824a(certificate));
                stringBuilder.append(": ");
                stringBuilder.append(certificate.getSubjectDN().getName());
            }
            stringBuilder.append("\n  Pinned certificates for ");
            stringBuilder.append(str);
            stringBuilder.append(":");
            str = a.size();
            while (i2 < str) {
                C0535b c0535b2 = (C0535b) a.get(i2);
                stringBuilder.append("\n    ");
                stringBuilder.append(c0535b2);
                i2++;
            }
            throw new SSLPeerUnverifiedException(stringBuilder.toString());
        }
    }

    List<C0535b> m827a(String str) {
        List<C0535b> emptyList = Collections.emptyList();
        for (C0535b c0535b : this.f726b) {
            if (c0535b.m822a(str)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList();
                }
                emptyList.add(c0535b);
            }
        }
        return emptyList;
    }

    C0536g m826a(@Nullable C0517c c0517c) {
        if (C0488c.m521a(this.f727c, (Object) c0517c)) {
            return this;
        }
        return new C0536g(this.f726b, c0517c);
    }

    public static String m824a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("sha256/");
            stringBuilder.append(C0536g.m825b((X509Certificate) certificate).mo1906b());
            return stringBuilder.toString();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    static C1624f m823a(X509Certificate x509Certificate) {
        return C1624f.m4821a(x509Certificate.getPublicKey().getEncoded()).mo1907c();
    }

    static C1624f m825b(X509Certificate x509Certificate) {
        return C1624f.m4821a(x509Certificate.getPublicKey().getEncoded()).mo1908d();
    }
}
