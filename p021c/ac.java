package p021c;

import java.io.Closeable;
import javax.annotation.Nullable;
import p021c.C0554s.C0553a;

/* compiled from: Response */
public final class ac implements Closeable {
    final aa f661a;
    final C0564y f662b;
    final int f663c;
    final String f664d;
    @Nullable
    final C0552r f665e;
    final C0554s f666f;
    @Nullable
    final ad f667g;
    @Nullable
    final ac f668h;
    @Nullable
    final ac f669i;
    @Nullable
    final ac f670j;
    final long f671k;
    final long f672l;
    private volatile C0530d f673m;

    /* compiled from: Response */
    public static class C0523a {
        aa f649a;
        C0564y f650b;
        int f651c;
        String f652d;
        @Nullable
        C0552r f653e;
        C0553a f654f;
        ad f655g;
        ac f656h;
        ac f657i;
        ac f658j;
        long f659k;
        long f660l;

        public C0523a() {
            this.f651c = -1;
            this.f654f = new C0553a();
        }

        C0523a(ac acVar) {
            this.f651c = -1;
            this.f649a = acVar.f661a;
            this.f650b = acVar.f662b;
            this.f651c = acVar.f663c;
            this.f652d = acVar.f664d;
            this.f653e = acVar.f665e;
            this.f654f = acVar.f666f.m923b();
            this.f655g = acVar.f667g;
            this.f656h = acVar.f668h;
            this.f657i = acVar.f669i;
            this.f658j = acVar.f670j;
            this.f659k = acVar.f671k;
            this.f660l = acVar.f672l;
        }

        public C0523a m763a(aa aaVar) {
            this.f649a = aaVar;
            return this;
        }

        public C0523a m768a(C0564y c0564y) {
            this.f650b = c0564y;
            return this;
        }

        public C0523a m761a(int i) {
            this.f651c = i;
            return this;
        }

        public C0523a m769a(String str) {
            this.f652d = str;
            return this;
        }

        public C0523a m766a(@Nullable C0552r c0552r) {
            this.f653e = c0552r;
            return this;
        }

        public C0523a m770a(String str, String str2) {
            this.f654f.m913a(str, str2);
            return this;
        }

        public C0523a m767a(C0554s c0554s) {
            this.f654f = c0554s.m923b();
            return this;
        }

        public C0523a m765a(@Nullable ad adVar) {
            this.f655g = adVar;
            return this;
        }

        public C0523a m764a(@Nullable ac acVar) {
            if (acVar != null) {
                m759a("networkResponse", acVar);
            }
            this.f656h = acVar;
            return this;
        }

        public C0523a m773b(@Nullable ac acVar) {
            if (acVar != null) {
                m759a("cacheResponse", acVar);
            }
            this.f657i = acVar;
            return this;
        }

        private void m759a(String str, ac acVar) {
            StringBuilder stringBuilder;
            if (acVar.f667g != null) {
                stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append(".body != null");
                throw new IllegalArgumentException(stringBuilder.toString());
            } else if (acVar.f668h != null) {
                stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append(".networkResponse != null");
                throw new IllegalArgumentException(stringBuilder.toString());
            } else if (acVar.f669i != null) {
                stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append(".cacheResponse != null");
                throw new IllegalArgumentException(stringBuilder.toString());
            } else if (acVar.f670j != null) {
                stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append(".priorResponse != null");
                throw new IllegalArgumentException(stringBuilder.toString());
            }
        }

        public C0523a m774c(@Nullable ac acVar) {
            if (acVar != null) {
                m760d(acVar);
            }
            this.f658j = acVar;
            return this;
        }

        private void m760d(ac acVar) {
            if (acVar.f667g != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        public C0523a m762a(long j) {
            this.f659k = j;
            return this;
        }

        public C0523a m772b(long j) {
            this.f660l = j;
            return this;
        }

        public ac m771a() {
            if (this.f649a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f650b == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.f651c < 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("code < 0: ");
                stringBuilder.append(this.f651c);
                throw new IllegalStateException(stringBuilder.toString());
            } else if (this.f652d != null) {
                return new ac(this);
            } else {
                throw new IllegalStateException("message == null");
            }
        }
    }

    ac(C0523a c0523a) {
        this.f661a = c0523a.f649a;
        this.f662b = c0523a.f650b;
        this.f663c = c0523a.f651c;
        this.f664d = c0523a.f652d;
        this.f665e = c0523a.f653e;
        this.f666f = c0523a.f654f.m914a();
        this.f667g = c0523a.f655g;
        this.f668h = c0523a.f656h;
        this.f669i = c0523a.f657i;
        this.f670j = c0523a.f658j;
        this.f671k = c0523a.f659k;
        this.f672l = c0523a.f660l;
    }

    public aa m775a() {
        return this.f661a;
    }

    public int m778b() {
        return this.f663c;
    }

    public boolean m779c() {
        return this.f663c >= 200 && this.f663c < 300;
    }

    public String m780d() {
        return this.f664d;
    }

    public C0552r m781e() {
        return this.f665e;
    }

    @Nullable
    public String m776a(String str) {
        return m777a(str, null);
    }

    @Nullable
    public String m777a(String str, @Nullable String str2) {
        str = this.f666f.m922a(str);
        return str != null ? str : str2;
    }

    public C0554s m782f() {
        return this.f666f;
    }

    @Nullable
    public ad m783g() {
        return this.f667g;
    }

    public C0523a m784h() {
        return new C0523a(this);
    }

    @Nullable
    public ac m785i() {
        return this.f669i;
    }

    @Nullable
    public ac m786j() {
        return this.f670j;
    }

    public C0530d m787k() {
        C0530d c0530d = this.f673m;
        if (c0530d != null) {
            return c0530d;
        }
        c0530d = C0530d.m803a(this.f666f);
        this.f673m = c0530d;
        return c0530d;
    }

    public long m788l() {
        return this.f671k;
    }

    public long m789m() {
        return this.f672l;
    }

    public void close() {
        if (this.f667g == null) {
            throw new IllegalStateException("response is not eligible for a body and must not be closed");
        }
        this.f667g.close();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Response{protocol=");
        stringBuilder.append(this.f662b);
        stringBuilder.append(", code=");
        stringBuilder.append(this.f663c);
        stringBuilder.append(", message=");
        stringBuilder.append(this.f664d);
        stringBuilder.append(", url=");
        stringBuilder.append(this.f661a.m751a());
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
