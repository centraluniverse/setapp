package p021c;

import android.support.v4.internal.view.SupportMenu;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import p021c.p022a.C0488c;
import p125d.C2453c;

/* compiled from: HttpUrl */
public final class C0557t {
    private static final char[] f856d = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    final String f857a;
    final String f858b;
    final int f859c;
    private final String f860e;
    private final String f861f;
    private final List<String> f862g;
    @Nullable
    private final List<String> f863h;
    @Nullable
    private final String f864i;
    private final String f865j;

    /* compiled from: HttpUrl */
    public static final class C0556a {
        @Nullable
        String f848a;
        String f849b = "";
        String f850c = "";
        @Nullable
        String f851d;
        int f852e = -1;
        final List<String> f853f = new ArrayList();
        @Nullable
        List<String> f854g;
        @Nullable
        String f855h;

        /* compiled from: HttpUrl */
        enum C0555a {
            SUCCESS,
            MISSING_SCHEME,
            UNSUPPORTED_SCHEME,
            INVALID_PORT,
            INVALID_HOST
        }

        public C0556a() {
            this.f853f.add("");
        }

        public C0556a m939a(String str) {
            if (str == null) {
                throw new NullPointerException("scheme == null");
            }
            if (str.equalsIgnoreCase("http")) {
                this.f848a = "http";
            } else if (str.equalsIgnoreCase("https")) {
                this.f848a = "https";
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("unexpected scheme: ");
                stringBuilder.append(str);
                throw new IllegalArgumentException(stringBuilder.toString());
            }
            return this;
        }

        public C0556a m942b(String str) {
            if (str == null) {
                throw new NullPointerException("username == null");
            }
            this.f849b = C0557t.m951a(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
        }

        public C0556a m944c(String str) {
            if (str == null) {
                throw new NullPointerException("password == null");
            }
            this.f850c = C0557t.m951a(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
        }

        public C0556a m946d(String str) {
            if (str == null) {
                throw new NullPointerException("host == null");
            }
            String e = C0556a.m932e(str, 0, str.length());
            if (e == null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("unexpected host: ");
                stringBuilder.append(str);
                throw new IllegalArgumentException(stringBuilder.toString());
            }
            this.f851d = e;
            return this;
        }

        public C0556a m938a(int i) {
            if (i > 0) {
                if (i <= SupportMenu.USER_MASK) {
                    this.f852e = i;
                    return this;
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("unexpected port: ");
            stringBuilder.append(i);
            throw new IllegalArgumentException(stringBuilder.toString());
        }

        int m936a() {
            return this.f852e != -1 ? this.f852e : C0557t.m948a(this.f848a);
        }

        public C0556a m947e(@Nullable String str) {
            if (str != null) {
                str = C0557t.m959b(C0557t.m951a(str, " \"'<>#", true, false, true, true));
            } else {
                str = null;
            }
            this.f854g = str;
            return this;
        }

        public C0556a m940a(String str, @Nullable String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            }
            if (this.f854g == null) {
                this.f854g = new ArrayList();
            }
            this.f854g.add(C0557t.m951a(str, " \"'<>#&=", false, false, true, true));
            str = this.f854g;
            if (str2 != null) {
                str2 = C0557t.m951a(str2, " \"'<>#&=", false, false, true, true);
            } else {
                str2 = null;
            }
            str.add(str2);
            return this;
        }

        public C0556a m943b(String str, @Nullable String str2) {
            if (str == null) {
                throw new NullPointerException("encodedName == null");
            }
            if (this.f854g == null) {
                this.f854g = new ArrayList();
            }
            this.f854g.add(C0557t.m951a(str, " \"'<>#&=", true, false, true, true));
            str = this.f854g;
            if (str2 != null) {
                str2 = C0557t.m951a(str2, " \"'<>#&=", true, false, true, true);
            } else {
                str2 = null;
            }
            str.add(str2);
            return this;
        }

        C0556a m941b() {
            int size = this.f853f.size();
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                this.f853f.set(i2, C0557t.m951a((String) this.f853f.get(i2), "[]", true, true, false, true));
            }
            if (this.f854g != null) {
                size = this.f854g.size();
                while (i < size) {
                    String str = (String) this.f854g.get(i);
                    if (str != null) {
                        this.f854g.set(i, C0557t.m951a(str, "\\^`{|}", true, true, true, true));
                    }
                    i++;
                }
            }
            if (this.f855h != null) {
                this.f855h = C0557t.m951a(this.f855h, " \"#<>\\^`{|}", true, true, false, false);
            }
            return this;
        }

        public C0557t m945c() {
            if (this.f848a == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.f851d != null) {
                return new C0557t(this);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(this.f848a);
            stringBuilder.append("://");
            if (!(this.f849b.isEmpty() && this.f850c.isEmpty())) {
                stringBuilder.append(this.f849b);
                if (!this.f850c.isEmpty()) {
                    stringBuilder.append(':');
                    stringBuilder.append(this.f850c);
                }
                stringBuilder.append('@');
            }
            if (this.f851d.indexOf(58) != -1) {
                stringBuilder.append('[');
                stringBuilder.append(this.f851d);
                stringBuilder.append(']');
            } else {
                stringBuilder.append(this.f851d);
            }
            int a = m936a();
            if (a != C0557t.m948a(this.f848a)) {
                stringBuilder.append(':');
                stringBuilder.append(a);
            }
            C0557t.m957a(stringBuilder, this.f853f);
            if (this.f854g != null) {
                stringBuilder.append('?');
                C0557t.m960b(stringBuilder, this.f854g);
            }
            if (this.f855h != null) {
                stringBuilder.append('#');
                stringBuilder.append(this.f855h);
            }
            return stringBuilder.toString();
        }

        C0555a m937a(@Nullable C0557t c0557t, String str) {
            int a;
            int i;
            C0556a c0556a = this;
            C0557t c0557t2 = c0557t;
            String str2 = str;
            int a2 = C0488c.m502a(str2, 0, str.length());
            int b = C0488c.m526b(str2, a2, str.length());
            if (C0556a.m928b(str2, a2, b) != -1) {
                if (str2.regionMatches(true, a2, "https:", 0, 6)) {
                    c0556a.f848a = "https";
                    a2 += "https:".length();
                } else {
                    if (!str2.regionMatches(true, a2, "http:", 0, 5)) {
                        return C0555a.UNSUPPORTED_SCHEME;
                    }
                    c0556a.f848a = "http";
                    a2 += "http:".length();
                }
            } else if (c0557t2 == null) {
                return C0555a.MISSING_SCHEME;
            } else {
                c0556a.f848a = c0557t2.f857a;
            }
            int c = C0556a.m929c(str2, a2, b);
            char c2 = '#';
            if (c < 2 && c0557t2 != null) {
                if (c0557t2.f857a.equals(c0556a.f848a)) {
                    c0556a.f849b = c0557t.m967d();
                    c0556a.f850c = c0557t.m968e();
                    c0556a.f851d = c0557t2.f858b;
                    c0556a.f852e = c0557t2.f859c;
                    c0556a.f853f.clear();
                    c0556a.f853f.addAll(c0557t.m972i());
                    if (a2 == b || str2.charAt(a2) == '#') {
                        m947e(c0557t.m974k());
                    }
                    int i2 = a2;
                    a = C0488c.m504a(str2, i2, b, "?#");
                    m926a(str2, i2, a);
                    if (a < b || str2.charAt(a) != '?') {
                        i = a;
                    } else {
                        i = C0488c.m503a(str2, a, b, '#');
                        c0556a.f854g = C0557t.m959b(C0557t.m949a(str2, a + 1, i, " \"'<>#", true, false, true, true, null));
                    }
                    if (i < b && str2.charAt(i) == '#') {
                        c0556a.f855h = C0557t.m949a(str2, i + 1, b, "", true, false, false, false, null);
                    }
                    return C0555a.SUCCESS;
                }
            }
            int i3 = 0;
            int i4 = i3;
            c = a2 + c;
            while (true) {
                a2 = C0488c.m504a(str2, c, b, "@/\\?#");
                char charAt = a2 != b ? str2.charAt(a2) : '￿';
                if (!(charAt == '￿' || charAt == r13 || charAt == '/' || charAt == '\\')) {
                    switch (charAt) {
                        case '?':
                            break;
                        case '@':
                            if (i3 == 0) {
                                int a3 = C0488c.m503a(str2, c, a2, ':');
                                int i5 = a3;
                                i2 = a2;
                                String a4 = C0557t.m949a(str2, c, a3, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                                if (i4 != 0) {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    stringBuilder.append(c0556a.f849b);
                                    stringBuilder.append("%40");
                                    stringBuilder.append(a4);
                                    a4 = stringBuilder.toString();
                                }
                                c0556a.f849b = a4;
                                if (i5 != i2) {
                                    c0556a.f850c = C0557t.m949a(str2, i5 + 1, i2, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                                    i3 = 1;
                                }
                                i4 = 1;
                            } else {
                                i2 = a2;
                                StringBuilder stringBuilder2 = new StringBuilder();
                                stringBuilder2.append(c0556a.f850c);
                                stringBuilder2.append("%40");
                                stringBuilder2.append(C0557t.m949a(str2, c, i2, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null));
                                c0556a.f850c = stringBuilder2.toString();
                            }
                            c = i2 + 1;
                            continue;
                        default:
                            continue;
                    }
                }
                i2 = a2;
                a = C0556a.m930d(str2, c, i2);
                int i6 = a + 1;
                if (i6 < i2) {
                    c0556a.f851d = C0556a.m932e(str2, c, a);
                    c0556a.f852e = C0556a.m933f(str2, i6, i2);
                    if (c0556a.f852e == -1) {
                        return C0555a.INVALID_PORT;
                    }
                }
                c0556a.f851d = C0556a.m932e(str2, c, a);
                c0556a.f852e = C0557t.m948a(c0556a.f848a);
                if (c0556a.f851d == null) {
                    return C0555a.INVALID_HOST;
                }
                a = C0488c.m504a(str2, i2, b, "?#");
                m926a(str2, i2, a);
                if (a < b) {
                }
                i = a;
                c0556a.f855h = C0557t.m949a(str2, i + 1, b, "", true, false, false, false, null);
                return C0555a.SUCCESS;
                c2 = '#';
            }
        }

        private void m926a(String str, int i, int i2) {
            if (i != i2) {
                int i3;
                boolean z;
                char charAt = str.charAt(i);
                if (charAt != '/') {
                    if (charAt != '\\') {
                        this.f853f.set(this.f853f.size() - 1, "");
                        while (true) {
                            i3 = i;
                            if (i3 < i2) {
                                i = C0488c.m504a(str, i3, i2, "/\\");
                                z = i >= i2;
                                m927a(str, i3, i, z, true);
                                if (z) {
                                    i++;
                                }
                            } else {
                                return;
                            }
                        }
                    }
                }
                this.f853f.clear();
                this.f853f.add("");
                i++;
                while (true) {
                    i3 = i;
                    if (i3 < i2) {
                        i = C0488c.m504a(str, i3, i2, "/\\");
                        if (i >= i2) {
                        }
                        m927a(str, i3, i, z, true);
                        if (z) {
                            i++;
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        private void m927a(String str, int i, int i2, boolean z, boolean z2) {
            str = C0557t.m949a(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true, null);
            if (m934f(str) == 0) {
                if (m935g(str) != 0) {
                    m931d();
                    return;
                }
                if (((String) this.f853f.get(this.f853f.size() - 1)).isEmpty() != 0) {
                    this.f853f.set(this.f853f.size() - 1, str);
                } else {
                    this.f853f.add(str);
                }
                if (z) {
                    this.f853f.add("");
                }
            }
        }

        private boolean m934f(String str) {
            if (!str.equals(".")) {
                if (str.equalsIgnoreCase("%2e") == null) {
                    return null;
                }
            }
            return true;
        }

        private boolean m935g(String str) {
            if (!(str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e"))) {
                if (str.equalsIgnoreCase("%2e%2e") == null) {
                    return null;
                }
            }
            return true;
        }

        private void m931d() {
            if (!((String) this.f853f.remove(this.f853f.size() - 1)).isEmpty() || this.f853f.isEmpty()) {
                this.f853f.add("");
            } else {
                this.f853f.set(this.f853f.size() - 1, "");
            }
        }

        private static int m928b(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if (charAt < 'a' || charAt > 'z') {
                if (charAt >= 'A') {
                    if (charAt > 'Z') {
                    }
                }
                return -1;
            }
            while (true) {
                i++;
                if (i < i2) {
                    charAt = str.charAt(i);
                    if ((charAt < 'a' || charAt > 'z') && ((charAt < 'A' || charAt > 'Z') && !((charAt >= '0' && charAt <= '9') || charAt == '+' || charAt == '-'))) {
                        if (charAt != '.') {
                            break;
                        }
                    }
                } else {
                    return -1;
                }
            }
            if (charAt == ':') {
                return i;
            }
            return -1;
        }

        private static int m929c(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        private static int m930d(String str, int i, int i2) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt == ':') {
                    return i;
                }
                if (charAt == '[') {
                    do {
                        i++;
                        if (i >= i2) {
                            break;
                        }
                    } while (str.charAt(i) != ']');
                }
                i++;
            }
            return i2;
        }

        private static String m932e(String str, int i, int i2) {
            return C0488c.m509a(C0557t.m950a(str, i, i2, false));
        }

        private static int m933f(java.lang.String r10, int r11, int r12) {
            /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
            /*
            r0 = -1;
            r4 = "";	 Catch:{ NumberFormatException -> 0x001c }
            r5 = 0;	 Catch:{ NumberFormatException -> 0x001c }
            r6 = 0;	 Catch:{ NumberFormatException -> 0x001c }
            r7 = 0;	 Catch:{ NumberFormatException -> 0x001c }
            r8 = 1;	 Catch:{ NumberFormatException -> 0x001c }
            r9 = 0;	 Catch:{ NumberFormatException -> 0x001c }
            r1 = r10;	 Catch:{ NumberFormatException -> 0x001c }
            r2 = r11;	 Catch:{ NumberFormatException -> 0x001c }
            r3 = r12;	 Catch:{ NumberFormatException -> 0x001c }
            r10 = p021c.C0557t.m949a(r1, r2, r3, r4, r5, r6, r7, r8, r9);	 Catch:{ NumberFormatException -> 0x001c }
            r10 = java.lang.Integer.parseInt(r10);	 Catch:{ NumberFormatException -> 0x001c }
            if (r10 <= 0) goto L_0x001b;
        L_0x0015:
            r11 = 65535; // 0xffff float:9.1834E-41 double:3.23786E-319;
            if (r10 > r11) goto L_0x001b;
        L_0x001a:
            return r10;
        L_0x001b:
            return r0;
        L_0x001c:
            return r0;
            */
            throw new UnsupportedOperationException("Method not decompiled: c.t.a.f(java.lang.String, int, int):int");
        }
    }

    C0557t(C0556a c0556a) {
        this.f857a = c0556a.f848a;
        this.f860e = C0557t.m953a(c0556a.f849b, false);
        this.f861f = C0557t.m953a(c0556a.f850c, false);
        this.f858b = c0556a.f851d;
        this.f859c = c0556a.m936a();
        this.f862g = m954a(c0556a.f853f, false);
        String str = null;
        this.f863h = c0556a.f854g != null ? m954a(c0556a.f854g, true) : null;
        if (c0556a.f855h != null) {
            str = C0557t.m953a(c0556a.f855h, false);
        }
        this.f864i = str;
        this.f865j = c0556a.toString();
    }

    public java.net.URI m962a() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r4 = this;
        r0 = r4.m978o();
        r0 = r0.m941b();
        r0 = r0.toString();
        r1 = new java.net.URI;	 Catch:{ URISyntaxException -> 0x0012 }
        r1.<init>(r0);	 Catch:{ URISyntaxException -> 0x0012 }
        return r1;
    L_0x0012:
        r1 = move-exception;
        r2 = "[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]";	 Catch:{ Exception -> 0x0020 }
        r3 = "";	 Catch:{ Exception -> 0x0020 }
        r0 = r0.replaceAll(r2, r3);	 Catch:{ Exception -> 0x0020 }
        r0 = java.net.URI.create(r0);	 Catch:{ Exception -> 0x0020 }
        return r0;
    L_0x0020:
        r0 = new java.lang.RuntimeException;
        r0.<init>(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.t.a():java.net.URI");
    }

    public String m963b() {
        return this.f857a;
    }

    public boolean m965c() {
        return this.f857a.equals("https");
    }

    public String m967d() {
        if (this.f860e.isEmpty()) {
            return "";
        }
        int length = this.f857a.length() + 3;
        return this.f865j.substring(length, C0488c.m504a(this.f865j, length, this.f865j.length(), ":@"));
    }

    public String m968e() {
        if (this.f861f.isEmpty()) {
            return "";
        }
        return this.f865j.substring(this.f865j.indexOf(58, this.f857a.length() + 3) + 1, this.f865j.indexOf(64));
    }

    public String m969f() {
        return this.f858b;
    }

    public int m970g() {
        return this.f859c;
    }

    public static int m948a(String str) {
        if (str.equals("http")) {
            return 80;
        }
        return str.equals("https") != null ? 443 : -1;
    }

    public String m971h() {
        int indexOf = this.f865j.indexOf(47, this.f857a.length() + 3);
        return this.f865j.substring(indexOf, C0488c.m504a(this.f865j, indexOf, this.f865j.length(), "?#"));
    }

    static void m957a(StringBuilder stringBuilder, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            stringBuilder.append('/');
            stringBuilder.append((String) list.get(i));
        }
    }

    public List<String> m972i() {
        int indexOf = this.f865j.indexOf(47, this.f857a.length() + 3);
        int a = C0488c.m504a(this.f865j, indexOf, this.f865j.length(), "?#");
        List<String> arrayList = new ArrayList();
        while (indexOf < a) {
            indexOf++;
            int a2 = C0488c.m503a(this.f865j, indexOf, a, '/');
            arrayList.add(this.f865j.substring(indexOf, a2));
            indexOf = a2;
        }
        return arrayList;
    }

    public List<String> m973j() {
        return this.f862g;
    }

    @Nullable
    public String m974k() {
        if (this.f863h == null) {
            return null;
        }
        int indexOf = this.f865j.indexOf(63) + 1;
        return this.f865j.substring(indexOf, C0488c.m503a(this.f865j, indexOf + 1, this.f865j.length(), '#'));
    }

    static void m960b(StringBuilder stringBuilder, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i += 2) {
            String str = (String) list.get(i);
            String str2 = (String) list.get(i + 1);
            if (i > 0) {
                stringBuilder.append('&');
            }
            stringBuilder.append(str);
            if (str2 != null) {
                stringBuilder.append('=');
                stringBuilder.append(str2);
            }
        }
    }

    static List<String> m959b(String str) {
        List<String> arrayList = new ArrayList();
        int i = 0;
        while (i <= str.length()) {
            int indexOf = str.indexOf(38, i);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i);
            if (indexOf2 != -1) {
                if (indexOf2 <= indexOf) {
                    arrayList.add(str.substring(i, indexOf2));
                    arrayList.add(str.substring(indexOf2 + 1, indexOf));
                    i = indexOf + 1;
                }
            }
            arrayList.add(str.substring(i, indexOf));
            arrayList.add(null);
            i = indexOf + 1;
        }
        return arrayList;
    }

    @Nullable
    public String m975l() {
        if (this.f863h == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        C0557t.m960b(stringBuilder, this.f863h);
        return stringBuilder.toString();
    }

    @Nullable
    public String m976m() {
        if (this.f864i == null) {
            return null;
        }
        return this.f865j.substring(this.f865j.indexOf(35) + 1);
    }

    public String m977n() {
        return m966d("/...").m942b("").m944c("").m945c().toString();
    }

    @Nullable
    public C0557t m964c(String str) {
        str = m966d(str);
        return str != null ? str.m945c() : null;
    }

    public C0556a m978o() {
        C0556a c0556a = new C0556a();
        c0556a.f848a = this.f857a;
        c0556a.f849b = m967d();
        c0556a.f850c = m968e();
        c0556a.f851d = this.f858b;
        c0556a.f852e = this.f859c != C0557t.m948a(this.f857a) ? this.f859c : -1;
        c0556a.f853f.clear();
        c0556a.f853f.addAll(m972i());
        c0556a.m947e(m974k());
        c0556a.f855h = m976m();
        return c0556a;
    }

    @Nullable
    public C0556a m966d(String str) {
        C0556a c0556a = new C0556a();
        return c0556a.m937a(this, str) == C0555a.SUCCESS ? c0556a : null;
    }

    @Nullable
    public static C0557t m961e(String str) {
        C0556a c0556a = new C0556a();
        if (c0556a.m937a(null, str) == C0555a.SUCCESS) {
            return c0556a.m945c();
        }
        return null;
    }

    public boolean equals(@Nullable Object obj) {
        return (!(obj instanceof C0557t) || ((C0557t) obj).f865j.equals(this.f865j) == null) ? null : true;
    }

    public int hashCode() {
        return this.f865j.hashCode();
    }

    public String toString() {
        return this.f865j;
    }

    static String m953a(String str, boolean z) {
        return C0557t.m950a(str, 0, str.length(), z);
    }

    private List<String> m954a(List<String> list, boolean z) {
        int size = list.size();
        List arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            String str = (String) list.get(i);
            arrayList.add(str != null ? C0557t.m953a(str, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    static String m950a(String str, int i, int i2, boolean z) {
        int i3 = i;
        while (i3 < i2) {
            char charAt = str.charAt(i3);
            if (charAt != '%') {
                if (charAt != '+' || !z) {
                    i3++;
                }
            }
            C2453c c2453c = new C2453c();
            c2453c.m7714a(str, i, i3);
            C0557t.m956a(c2453c, str, i3, i2, z);
            return c2453c.m7772s();
        }
        return str.substring(i, i2);
    }

    static void m956a(C2453c c2453c, String str, int i, int i2, boolean z) {
        while (i < i2) {
            int codePointAt = str.codePointAt(i);
            if (codePointAt == 37) {
                int i3 = i + 2;
                if (i3 < i2) {
                    int a = C0488c.m501a(str.charAt(i + 1));
                    int a2 = C0488c.m501a(str.charAt(i3));
                    if (!(a == -1 || a2 == -1)) {
                        c2453c.m7725b((a << 4) + a2);
                        i = i3;
                        i += Character.charCount(codePointAt);
                    }
                    c2453c.m7710a(codePointAt);
                    i += Character.charCount(codePointAt);
                }
            }
            if (codePointAt == 43 && z) {
                c2453c.m7725b(32);
                i += Character.charCount(codePointAt);
            }
            c2453c.m7710a(codePointAt);
            i += Character.charCount(codePointAt);
        }
    }

    static boolean m958a(String str, int i, int i2) {
        int i3 = i + 2;
        if (i3 >= i2 || str.charAt(i) != 37 || C0488c.m501a(str.charAt(i + 1)) == -1 || C0488c.m501a(str.charAt(i3)) == -1) {
            return false;
        }
        return true;
    }

    static String m949a(String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        String str3 = str;
        int i3 = i2;
        int i4 = i;
        while (i4 < i3) {
            String str4;
            int codePointAt = str3.codePointAt(i4);
            if (codePointAt < 32 || codePointAt == 127 || (codePointAt >= 128 && z4)) {
                str4 = str2;
            } else {
                str4 = str2;
                if (str4.indexOf(codePointAt) == -1 && (codePointAt != 37 || (z && (!z2 || C0557t.m958a(str3, i4, i3))))) {
                    if (codePointAt != 43 || !z3) {
                        i4 += Character.charCount(codePointAt);
                    }
                }
            }
            C2453c c2453c = new C2453c();
            c2453c.m7714a(str3, i, i4);
            C0557t.m955a(c2453c, str3, i4, i3, str4, z, z2, z3, z4, charset);
            return c2453c.m7772s();
        }
        return str3.substring(i, i3);
    }

    static void m955a(C2453c c2453c, String str, int i, int i2, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        C2453c c2453c2 = null;
        while (i < i2) {
            int codePointAt = str.codePointAt(i);
            if (z) {
                if (!(codePointAt == 9 || codePointAt == 10 || codePointAt == 12)) {
                    if (codePointAt == 13) {
                    }
                }
                i += Character.charCount(codePointAt);
            }
            if (codePointAt == 43 && z3) {
                c2453c.m7713a(z ? "+" : "%2B");
                i += Character.charCount(codePointAt);
            } else {
                int i3;
                if (codePointAt >= 32 && codePointAt != 127 && ((codePointAt < 128 || !z4) && str2.indexOf(codePointAt) == -1)) {
                    if (codePointAt == 37) {
                        if (z) {
                            if (z2 && !C0557t.m958a(str, i, i2)) {
                            }
                        }
                    }
                    c2453c.m7710a(codePointAt);
                    i += Character.charCount(codePointAt);
                }
                if (c2453c2 == null) {
                    c2453c2 = new C2453c();
                }
                if (charset != null) {
                    if (!charset.equals(C0488c.f475e)) {
                        c2453c2.m7715a(str, i, Character.charCount(codePointAt) + i, charset);
                        while (!c2453c2.mo2525f()) {
                            i3 = c2453c2.mo2529i() & 255;
                            c2453c.m7725b(37);
                            c2453c.m7725b(f856d[(i3 >> 4) & 15]);
                            c2453c.m7725b(f856d[i3 & 15]);
                        }
                        i += Character.charCount(codePointAt);
                    }
                }
                c2453c2.m7710a(codePointAt);
                while (!c2453c2.mo2525f()) {
                    i3 = c2453c2.mo2529i() & 255;
                    c2453c.m7725b(37);
                    c2453c.m7725b(f856d[(i3 >> 4) & 15]);
                    c2453c.m7725b(f856d[i3 & 15]);
                }
                i += Character.charCount(codePointAt);
            }
        }
    }

    static String m952a(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        return C0557t.m949a(str, 0, str.length(), str2, z, z2, z3, z4, charset);
    }

    static String m951a(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return C0557t.m949a(str, 0, str.length(), str2, z, z2, z3, z4, null);
    }
}
