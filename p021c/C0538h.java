package p021c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: CipherSuite */
public final class C0538h {
    public static final C0538h f728A = C0538h.m831a("TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA", 38);
    public static final C0538h f729B = C0538h.m831a("TLS_KRB5_EXPORT_WITH_RC4_40_SHA", 40);
    public static final C0538h f730C = C0538h.m831a("TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5", 41);
    public static final C0538h f731D = C0538h.m831a("TLS_KRB5_EXPORT_WITH_RC4_40_MD5", 43);
    public static final C0538h f732E = C0538h.m831a("TLS_RSA_WITH_AES_128_CBC_SHA", 47);
    public static final C0538h f733F = C0538h.m831a("TLS_DHE_DSS_WITH_AES_128_CBC_SHA", 50);
    public static final C0538h f734G = C0538h.m831a("TLS_DHE_RSA_WITH_AES_128_CBC_SHA", 51);
    public static final C0538h f735H = C0538h.m831a("TLS_DH_anon_WITH_AES_128_CBC_SHA", 52);
    public static final C0538h f736I = C0538h.m831a("TLS_RSA_WITH_AES_256_CBC_SHA", 53);
    public static final C0538h f737J = C0538h.m831a("TLS_DHE_DSS_WITH_AES_256_CBC_SHA", 56);
    public static final C0538h f738K = C0538h.m831a("TLS_DHE_RSA_WITH_AES_256_CBC_SHA", 57);
    public static final C0538h f739L = C0538h.m831a("TLS_DH_anon_WITH_AES_256_CBC_SHA", 58);
    public static final C0538h f740M = C0538h.m831a("TLS_RSA_WITH_NULL_SHA256", 59);
    public static final C0538h f741N = C0538h.m831a("TLS_RSA_WITH_AES_128_CBC_SHA256", 60);
    public static final C0538h f742O = C0538h.m831a("TLS_RSA_WITH_AES_256_CBC_SHA256", 61);
    public static final C0538h f743P = C0538h.m831a("TLS_DHE_DSS_WITH_AES_128_CBC_SHA256", 64);
    public static final C0538h f744Q = C0538h.m831a("TLS_RSA_WITH_CAMELLIA_128_CBC_SHA", 65);
    public static final C0538h f745R = C0538h.m831a("TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA", 68);
    public static final C0538h f746S = C0538h.m831a("TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA", 69);
    public static final C0538h f747T = C0538h.m831a("TLS_DHE_RSA_WITH_AES_128_CBC_SHA256", 103);
    public static final C0538h f748U = C0538h.m831a("TLS_DHE_DSS_WITH_AES_256_CBC_SHA256", 106);
    public static final C0538h f749V = C0538h.m831a("TLS_DHE_RSA_WITH_AES_256_CBC_SHA256", 107);
    public static final C0538h f750W = C0538h.m831a("TLS_DH_anon_WITH_AES_128_CBC_SHA256", 108);
    public static final C0538h f751X = C0538h.m831a("TLS_DH_anon_WITH_AES_256_CBC_SHA256", 109);
    public static final C0538h f752Y = C0538h.m831a("TLS_RSA_WITH_CAMELLIA_256_CBC_SHA", 132);
    public static final C0538h f753Z = C0538h.m831a("TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA", 135);
    static final Comparator<String> f754a = new C05371();
    public static final C0538h aA = C0538h.m831a("TLS_ECDH_RSA_WITH_NULL_SHA", 49163);
    public static final C0538h aB = C0538h.m831a("TLS_ECDH_RSA_WITH_RC4_128_SHA", 49164);
    public static final C0538h aC = C0538h.m831a("TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA", 49165);
    public static final C0538h aD = C0538h.m831a("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA", 49166);
    public static final C0538h aE = C0538h.m831a("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA", 49167);
    public static final C0538h aF = C0538h.m831a("TLS_ECDHE_RSA_WITH_NULL_SHA", 49168);
    public static final C0538h aG = C0538h.m831a("TLS_ECDHE_RSA_WITH_RC4_128_SHA", 49169);
    public static final C0538h aH = C0538h.m831a("TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA", 49170);
    public static final C0538h aI = C0538h.m831a("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", 49171);
    public static final C0538h aJ = C0538h.m831a("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA", 49172);
    public static final C0538h aK = C0538h.m831a("TLS_ECDH_anon_WITH_NULL_SHA", 49173);
    public static final C0538h aL = C0538h.m831a("TLS_ECDH_anon_WITH_RC4_128_SHA", 49174);
    public static final C0538h aM = C0538h.m831a("TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA", 49175);
    public static final C0538h aN = C0538h.m831a("TLS_ECDH_anon_WITH_AES_128_CBC_SHA", 49176);
    public static final C0538h aO = C0538h.m831a("TLS_ECDH_anon_WITH_AES_256_CBC_SHA", 49177);
    public static final C0538h aP = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", 49187);
    public static final C0538h aQ = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", 49188);
    public static final C0538h aR = C0538h.m831a("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256", 49189);
    public static final C0538h aS = C0538h.m831a("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384", 49190);
    public static final C0538h aT = C0538h.m831a("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", 49191);
    public static final C0538h aU = C0538h.m831a("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", 49192);
    public static final C0538h aV = C0538h.m831a("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256", 49193);
    public static final C0538h aW = C0538h.m831a("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384", 49194);
    public static final C0538h aX = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", 49195);
    public static final C0538h aY = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", 49196);
    public static final C0538h aZ = C0538h.m831a("TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256", 49197);
    public static final C0538h aa = C0538h.m831a("TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA", 136);
    public static final C0538h ab = C0538h.m831a("TLS_PSK_WITH_RC4_128_SHA", 138);
    public static final C0538h ac = C0538h.m831a("TLS_PSK_WITH_3DES_EDE_CBC_SHA", 139);
    public static final C0538h ad = C0538h.m831a("TLS_PSK_WITH_AES_128_CBC_SHA", 140);
    public static final C0538h ae = C0538h.m831a("TLS_PSK_WITH_AES_256_CBC_SHA", 141);
    public static final C0538h af = C0538h.m831a("TLS_RSA_WITH_SEED_CBC_SHA", 150);
    public static final C0538h ag = C0538h.m831a("TLS_RSA_WITH_AES_128_GCM_SHA256", 156);
    public static final C0538h ah = C0538h.m831a("TLS_RSA_WITH_AES_256_GCM_SHA384", 157);
    public static final C0538h ai = C0538h.m831a("TLS_DHE_RSA_WITH_AES_128_GCM_SHA256", 158);
    public static final C0538h aj = C0538h.m831a("TLS_DHE_RSA_WITH_AES_256_GCM_SHA384", 159);
    public static final C0538h ak = C0538h.m831a("TLS_DHE_DSS_WITH_AES_128_GCM_SHA256", 162);
    public static final C0538h al = C0538h.m831a("TLS_DHE_DSS_WITH_AES_256_GCM_SHA384", 163);
    public static final C0538h am = C0538h.m831a("TLS_DH_anon_WITH_AES_128_GCM_SHA256", 166);
    public static final C0538h an = C0538h.m831a("TLS_DH_anon_WITH_AES_256_GCM_SHA384", 167);
    public static final C0538h ao = C0538h.m831a("TLS_EMPTY_RENEGOTIATION_INFO_SCSV", 255);
    public static final C0538h ap = C0538h.m831a("TLS_FALLBACK_SCSV", 22016);
    public static final C0538h aq = C0538h.m831a("TLS_ECDH_ECDSA_WITH_NULL_SHA", 49153);
    public static final C0538h ar = C0538h.m831a("TLS_ECDH_ECDSA_WITH_RC4_128_SHA", 49154);
    public static final C0538h as = C0538h.m831a("TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA", 49155);
    public static final C0538h at = C0538h.m831a("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA", 49156);
    public static final C0538h au = C0538h.m831a("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA", 49157);
    public static final C0538h av = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_NULL_SHA", 49158);
    public static final C0538h aw = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_RC4_128_SHA", 49159);
    public static final C0538h ax = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA", 49160);
    public static final C0538h ay = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", 49161);
    public static final C0538h az = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", 49162);
    public static final C0538h f755b = C0538h.m831a("SSL_RSA_WITH_NULL_MD5", 1);
    public static final C0538h ba = C0538h.m831a("TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384", 49198);
    public static final C0538h bb = C0538h.m831a("TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", 49199);
    public static final C0538h bc = C0538h.m831a("TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", 49200);
    public static final C0538h bd = C0538h.m831a("TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256", 49201);
    public static final C0538h be = C0538h.m831a("TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384", 49202);
    public static final C0538h bf = C0538h.m831a("TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA", 49205);
    public static final C0538h bg = C0538h.m831a("TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA", 49206);
    public static final C0538h bh = C0538h.m831a("TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256", 52392);
    public static final C0538h bi = C0538h.m831a("TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256", 52393);
    private static final Map<String, C0538h> bk = new TreeMap(f754a);
    public static final C0538h f756c = C0538h.m831a("SSL_RSA_WITH_NULL_SHA", 2);
    public static final C0538h f757d = C0538h.m831a("SSL_RSA_EXPORT_WITH_RC4_40_MD5", 3);
    public static final C0538h f758e = C0538h.m831a("SSL_RSA_WITH_RC4_128_MD5", 4);
    public static final C0538h f759f = C0538h.m831a("SSL_RSA_WITH_RC4_128_SHA", 5);
    public static final C0538h f760g = C0538h.m831a("SSL_RSA_EXPORT_WITH_DES40_CBC_SHA", 8);
    public static final C0538h f761h = C0538h.m831a("SSL_RSA_WITH_DES_CBC_SHA", 9);
    public static final C0538h f762i = C0538h.m831a("SSL_RSA_WITH_3DES_EDE_CBC_SHA", 10);
    public static final C0538h f763j = C0538h.m831a("SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA", 17);
    public static final C0538h f764k = C0538h.m831a("SSL_DHE_DSS_WITH_DES_CBC_SHA", 18);
    public static final C0538h f765l = C0538h.m831a("SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA", 19);
    public static final C0538h f766m = C0538h.m831a("SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", 20);
    public static final C0538h f767n = C0538h.m831a("SSL_DHE_RSA_WITH_DES_CBC_SHA", 21);
    public static final C0538h f768o = C0538h.m831a("SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA", 22);
    public static final C0538h f769p = C0538h.m831a("SSL_DH_anon_EXPORT_WITH_RC4_40_MD5", 23);
    public static final C0538h f770q = C0538h.m831a("SSL_DH_anon_WITH_RC4_128_MD5", 24);
    public static final C0538h f771r = C0538h.m831a("SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA", 25);
    public static final C0538h f772s = C0538h.m831a("SSL_DH_anon_WITH_DES_CBC_SHA", 26);
    public static final C0538h f773t = C0538h.m831a("SSL_DH_anon_WITH_3DES_EDE_CBC_SHA", 27);
    public static final C0538h f774u = C0538h.m831a("TLS_KRB5_WITH_DES_CBC_SHA", 30);
    public static final C0538h f775v = C0538h.m831a("TLS_KRB5_WITH_3DES_EDE_CBC_SHA", 31);
    public static final C0538h f776w = C0538h.m831a("TLS_KRB5_WITH_RC4_128_SHA", 32);
    public static final C0538h f777x = C0538h.m831a("TLS_KRB5_WITH_DES_CBC_MD5", 34);
    public static final C0538h f778y = C0538h.m831a("TLS_KRB5_WITH_3DES_EDE_CBC_MD5", 35);
    public static final C0538h f779z = C0538h.m831a("TLS_KRB5_WITH_RC4_128_MD5", 36);
    final String bj;

    /* compiled from: CipherSuite */
    class C05371 implements Comparator<String> {
        C05371() {
        }

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return m829a((String) obj, (String) obj2);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m829a(java.lang.String r7, java.lang.String r8) {
            /*
            r6 = this;
            r0 = r7.length();
            r1 = r8.length();
            r0 = java.lang.Math.min(r0, r1);
            r1 = 4;
        L_0x000d:
            r2 = -1;
            r3 = 1;
            if (r1 >= r0) goto L_0x0023;
        L_0x0011:
            r4 = r7.charAt(r1);
            r5 = r8.charAt(r1);
            if (r4 == r5) goto L_0x0020;
        L_0x001b:
            if (r4 >= r5) goto L_0x001e;
        L_0x001d:
            goto L_0x001f;
        L_0x001e:
            r2 = r3;
        L_0x001f:
            return r2;
        L_0x0020:
            r1 = r1 + 1;
            goto L_0x000d;
        L_0x0023:
            r7 = r7.length();
            r8 = r8.length();
            if (r7 == r8) goto L_0x0032;
        L_0x002d:
            if (r7 >= r8) goto L_0x0030;
        L_0x002f:
            goto L_0x0031;
        L_0x0030:
            r2 = r3;
        L_0x0031:
            return r2;
        L_0x0032:
            r7 = 0;
            return r7;
            */
            throw new UnsupportedOperationException("Method not decompiled: c.h.1.a(java.lang.String, java.lang.String):int");
        }
    }

    public static synchronized C0538h m830a(String str) {
        C0538h c0538h;
        synchronized (C0538h.class) {
            c0538h = (C0538h) bk.get(str);
            if (c0538h == null) {
                c0538h = new C0538h(str);
                bk.put(str, c0538h);
            }
        }
        return c0538h;
    }

    static List<C0538h> m832a(String... strArr) {
        List arrayList = new ArrayList(strArr.length);
        for (String a : strArr) {
            arrayList.add(C0538h.m830a(a));
        }
        return Collections.unmodifiableList(arrayList);
    }

    private C0538h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.bj = str;
    }

    private static C0538h m831a(String str, int i) {
        return C0538h.m830a(str);
    }

    public String toString() {
        return this.bj;
    }
}
