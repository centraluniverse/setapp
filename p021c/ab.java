package p021c;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import javax.annotation.Nullable;
import p021c.p022a.C0488c;
import p125d.C1624f;
import p125d.C1625m;
import p125d.C2283d;

/* compiled from: RequestBody */
public abstract class ab {

    /* compiled from: RequestBody */
    class C18691 extends ab {
        final /* synthetic */ C0560v f4675a;
        final /* synthetic */ C1624f f4676b;

        C18691(C0560v c0560v, C1624f c1624f) {
            this.f4675a = c0560v;
            this.f4676b = c1624f;
        }

        @Nullable
        public C0560v contentType() {
            return this.f4675a;
        }

        public long contentLength() throws IOException {
            return (long) this.f4676b.mo1912g();
        }

        public void writeTo(C2283d c2283d) throws IOException {
            c2283d.mo2517b(this.f4676b);
        }
    }

    /* compiled from: RequestBody */
    class C18702 extends ab {
        final /* synthetic */ C0560v f4677a;
        final /* synthetic */ int f4678b;
        final /* synthetic */ byte[] f4679c;
        final /* synthetic */ int f4680d;

        C18702(C0560v c0560v, int i, byte[] bArr, int i2) {
            this.f4677a = c0560v;
            this.f4678b = i;
            this.f4679c = bArr;
            this.f4680d = i2;
        }

        @Nullable
        public C0560v contentType() {
            return this.f4677a;
        }

        public long contentLength() {
            return (long) this.f4678b;
        }

        public void writeTo(C2283d c2283d) throws IOException {
            c2283d.mo2521c(this.f4679c, this.f4680d, this.f4678b);
        }
    }

    /* compiled from: RequestBody */
    class C18713 extends ab {
        final /* synthetic */ C0560v f4681a;
        final /* synthetic */ File f4682b;

        C18713(C0560v c0560v, File file) {
            this.f4681a = c0560v;
            this.f4682b = file;
        }

        @Nullable
        public C0560v contentType() {
            return this.f4681a;
        }

        public long contentLength() {
            return this.f4682b.length();
        }

        public void writeTo(C2283d c2283d) throws IOException {
            Closeable closeable = null;
            try {
                Closeable a = C1625m.m4845a(this.f4682b);
                try {
                    c2283d.mo2511a(a);
                    C0488c.m517a(a);
                } catch (Throwable th) {
                    c2283d = th;
                    closeable = a;
                    C0488c.m517a(closeable);
                    throw c2283d;
                }
            } catch (Throwable th2) {
                c2283d = th2;
                C0488c.m517a(closeable);
                throw c2283d;
            }
        }
    }

    public long contentLength() throws IOException {
        return -1;
    }

    @Nullable
    public abstract C0560v contentType();

    public abstract void writeTo(C2283d c2283d) throws IOException;

    public static ab create(@Nullable C0560v c0560v, String str) {
        Charset charset = C0488c.f475e;
        if (c0560v != null) {
            charset = c0560v.m989b();
            if (charset == null) {
                charset = C0488c.f475e;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(c0560v);
                stringBuilder.append("; charset=utf-8");
                c0560v = C0560v.m986a(stringBuilder.toString());
            }
        }
        return ab.create(c0560v, str.getBytes(charset));
    }

    public static ab create(@Nullable C0560v c0560v, C1624f c1624f) {
        return new C18691(c0560v, c1624f);
    }

    public static ab create(@Nullable C0560v c0560v, byte[] bArr) {
        return ab.create(c0560v, bArr, 0, bArr.length);
    }

    public static ab create(@Nullable C0560v c0560v, byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new NullPointerException("content == null");
        }
        C0488c.m516a((long) bArr.length, (long) i, (long) i2);
        return new C18702(c0560v, i2, bArr, i);
    }

    public static ab create(@Nullable C0560v c0560v, File file) {
        if (file != null) {
            return new C18713(c0560v, file);
        }
        throw new NullPointerException("content == null");
    }
}
