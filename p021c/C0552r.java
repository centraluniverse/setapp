package p021c;

import java.security.cert.Certificate;
import java.util.List;
import javax.annotation.Nullable;

/* compiled from: Handshake */
public final class C0552r {
    private final af f836a;
    private final C0538h f837b;
    private final List<Certificate> f838c;
    private final List<Certificate> f839d;

    private C0552r(af afVar, C0538h c0538h, List<Certificate> list, List<Certificate> list2) {
        this.f836a = afVar;
        this.f837b = c0538h;
        this.f838c = list;
        this.f839d = list2;
    }

    public static p021c.C0552r m908a(javax.net.ssl.SSLSession r4) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r0 = r4.getCipherSuite();
        if (r0 != 0) goto L_0x000e;
    L_0x0006:
        r4 = new java.lang.IllegalStateException;
        r0 = "cipherSuite == null";
        r4.<init>(r0);
        throw r4;
    L_0x000e:
        r0 = p021c.C0538h.m830a(r0);
        r1 = r4.getProtocol();
        if (r1 != 0) goto L_0x0020;
    L_0x0018:
        r4 = new java.lang.IllegalStateException;
        r0 = "tlsVersion == null";
        r4.<init>(r0);
        throw r4;
    L_0x0020:
        r1 = p021c.af.m794a(r1);
        r2 = r4.getPeerCertificates();	 Catch:{ SSLPeerUnverifiedException -> 0x0029 }
        goto L_0x002a;
    L_0x0029:
        r2 = 0;
    L_0x002a:
        if (r2 == 0) goto L_0x0031;
    L_0x002c:
        r2 = p021c.p022a.C0488c.m514a(r2);
        goto L_0x0035;
    L_0x0031:
        r2 = java.util.Collections.emptyList();
    L_0x0035:
        r4 = r4.getLocalCertificates();
        if (r4 == 0) goto L_0x0040;
    L_0x003b:
        r4 = p021c.p022a.C0488c.m514a(r4);
        goto L_0x0044;
    L_0x0040:
        r4 = java.util.Collections.emptyList();
    L_0x0044:
        r3 = new c.r;
        r3.<init>(r1, r0, r2, r4);
        return r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.r.a(javax.net.ssl.SSLSession):c.r");
    }

    public C0538h m909a() {
        return this.f837b;
    }

    public List<Certificate> m910b() {
        return this.f838c;
    }

    public boolean equals(@Nullable Object obj) {
        boolean z = false;
        if (!(obj instanceof C0552r)) {
            return false;
        }
        C0552r c0552r = (C0552r) obj;
        if (this.f836a.equals(c0552r.f836a) && this.f837b.equals(c0552r.f837b) && this.f838c.equals(c0552r.f838c) && this.f839d.equals(c0552r.f839d) != null) {
            z = true;
        }
        return z;
    }

    public int hashCode() {
        return (31 * (((((527 + this.f836a.hashCode()) * 31) + this.f837b.hashCode()) * 31) + this.f838c.hashCode())) + this.f839d.hashCode();
    }
}
