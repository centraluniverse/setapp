package p021c;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;
import p021c.p022a.C0488c;
import p125d.C2283d;
import p125d.C2453c;

/* compiled from: FormBody */
public final class C1880q extends ab {
    private static final C0560v f4690a = C0560v.m986a("application/x-www-form-urlencoded");
    private final List<String> f4691b;
    private final List<String> f4692c;

    /* compiled from: FormBody */
    public static final class C0551a {
        private final List<String> f833a;
        private final List<String> f834b;
        private final Charset f835c;

        public C0551a() {
            this(null);
        }

        public C0551a(Charset charset) {
            this.f833a = new ArrayList();
            this.f834b = new ArrayList();
            this.f835c = charset;
        }

        public C0551a m905a(String str, String str2) {
            this.f833a.add(C0557t.m952a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.f835c));
            this.f834b.add(C0557t.m952a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.f835c));
            return this;
        }

        public C0551a m907b(String str, String str2) {
            this.f833a.add(C0557t.m952a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.f835c));
            this.f834b.add(C0557t.m952a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.f835c));
            return this;
        }

        public C1880q m906a() {
            return new C1880q(this.f833a, this.f834b);
        }
    }

    C1880q(List<String> list, List<String> list2) {
        this.f4691b = C0488c.m513a((List) list);
        this.f4692c = C0488c.m513a((List) list2);
    }

    public C0560v contentType() {
        return f4690a;
    }

    public long contentLength() {
        return m5186a(null, true);
    }

    public void writeTo(C2283d c2283d) throws IOException {
        m5186a(c2283d, false);
    }

    private long m5186a(@Nullable C2283d c2283d, boolean z) {
        if (z) {
            c2283d = new C2453c();
        } else {
            c2283d = c2283d.mo2516b();
        }
        int size = this.f4691b.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                c2283d.m7725b(38);
            }
            c2283d.m7713a((String) this.f4691b.get(i));
            c2283d.m7725b(61);
            c2283d.m7713a((String) this.f4692c.get(i));
        }
        if (!z) {
            return 0;
        }
        long a = c2283d.m7705a();
        c2283d.m7776w();
        return a;
    }
}
