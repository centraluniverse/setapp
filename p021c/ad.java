package p021c;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import javax.annotation.Nullable;
import p021c.p022a.C0488c;
import p125d.C2284e;
import p125d.C2453c;

/* compiled from: ResponseBody */
public abstract class ad implements Closeable {
    private Reader reader;

    /* compiled from: ResponseBody */
    static final class C0524a extends Reader {
        private final C2284e f674a;
        private final Charset f675b;
        private boolean f676c;
        private Reader f677d;

        C0524a(C2284e c2284e, Charset charset) {
            this.f674a = c2284e;
            this.f675b = charset;
        }

        public int read(char[] cArr, int i, int i2) throws IOException {
            if (this.f676c) {
                throw new IOException("Stream closed");
            }
            Reader reader = this.f677d;
            if (reader == null) {
                Reader inputStreamReader = new InputStreamReader(this.f674a.mo2526g(), C0488c.m512a(this.f674a, this.f675b));
                this.f677d = inputStreamReader;
                reader = inputStreamReader;
            }
            return reader.read(cArr, i, i2);
        }

        public void close() throws IOException {
            this.f676c = true;
            if (this.f677d != null) {
                this.f677d.close();
            } else {
                this.f674a.close();
            }
        }
    }

    /* compiled from: ResponseBody */
    class C18721 extends ad {
        final /* synthetic */ C0560v f4683a;
        final /* synthetic */ long f4684b;
        final /* synthetic */ C2284e f4685c;

        C18721(C0560v c0560v, long j, C2284e c2284e) {
            this.f4683a = c0560v;
            this.f4684b = j;
            this.f4685c = c2284e;
        }

        @Nullable
        public C0560v contentType() {
            return this.f4683a;
        }

        public long contentLength() {
            return this.f4684b;
        }

        public C2284e source() {
            return this.f4685c;
        }
    }

    public abstract long contentLength();

    @Nullable
    public abstract C0560v contentType();

    public abstract C2284e source();

    public final InputStream byteStream() {
        return source().mo2526g();
    }

    public final byte[] bytes() throws IOException {
        long contentLength = contentLength();
        if (contentLength > 2147483647L) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Cannot buffer entire body for content length: ");
            stringBuilder.append(contentLength);
            throw new IOException(stringBuilder.toString());
        }
        Closeable source = source();
        try {
            byte[] v = source.mo2545v();
            if (contentLength == -1 || contentLength == ((long) v.length)) {
                return v;
            }
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Content-Length (");
            stringBuilder2.append(contentLength);
            stringBuilder2.append(") and stream length (");
            stringBuilder2.append(v.length);
            stringBuilder2.append(") disagree");
            throw new IOException(stringBuilder2.toString());
        } finally {
            C0488c.m517a(source);
        }
    }

    public final Reader charStream() {
        Reader reader = this.reader;
        if (reader != null) {
            return reader;
        }
        reader = new C0524a(source(), charset());
        this.reader = reader;
        return reader;
    }

    public final String string() throws IOException {
        Closeable source = source();
        try {
            String a = source.mo2512a(C0488c.m512a((C2284e) source, charset()));
            return a;
        } finally {
            C0488c.m517a(source);
        }
    }

    private Charset charset() {
        C0560v contentType = contentType();
        return contentType != null ? contentType.m988a(C0488c.f475e) : C0488c.f475e;
    }

    public void close() {
        C0488c.m517a(source());
    }

    public static ad create(@Nullable C0560v c0560v, String str) {
        Charset charset = C0488c.f475e;
        if (c0560v != null) {
            charset = c0560v.m989b();
            if (charset == null) {
                charset = C0488c.f475e;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(c0560v);
                stringBuilder.append("; charset=utf-8");
                c0560v = C0560v.m986a(stringBuilder.toString());
            }
        }
        str = new C2453c().m7716a(str, charset);
        return ad.create(c0560v, str.m7705a(), str);
    }

    public static ad create(@Nullable C0560v c0560v, byte[] bArr) {
        return ad.create(c0560v, (long) bArr.length, new C2453c().m7726b(bArr));
    }

    public static ad create(@Nullable C0560v c0560v, long j, C2284e c2284e) {
        if (c2284e != null) {
            return new C18721(c0560v, j, c2284e);
        }
        throw new NullPointerException("source == null");
    }
}
