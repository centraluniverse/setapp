package p021c;

import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import p021c.p022a.C0488c;
import p021c.p022a.p024b.C0471d;
import p021c.p022a.p024b.C0476g;
import p021c.p022a.p024b.C0476g.C0475a;
import p021c.p022a.p024b.C1834c;
import p021c.p022a.p029g.C0515e;

/* compiled from: ConnectionPool */
public final class C0541j {
    static final /* synthetic */ boolean f781c = true;
    private static final Executor f782d = new ThreadPoolExecutor(0, ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, 60, TimeUnit.SECONDS, new SynchronousQueue(), C0488c.m515a("OkHttp ConnectionPool", (boolean) f781c));
    final C0471d f783a;
    boolean f784b;
    private final int f785e;
    private final long f786f;
    private final Runnable f787g;
    private final Deque<C1834c> f788h;

    /* compiled from: ConnectionPool */
    class C05401 implements Runnable {
        final /* synthetic */ C0541j f780a;

        C05401(C0541j c0541j) {
            this.f780a = c0541j;
        }

        public void run() {
            /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
            /*
            r8 = this;
        L_0x0000:
            r0 = r8.f780a;
            r1 = java.lang.System.nanoTime();
            r0 = r0.m836a(r1);
            r2 = -1;
            r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1));
            if (r4 != 0) goto L_0x0011;
        L_0x0010:
            return;
        L_0x0011:
            r2 = 0;
            r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1));
            if (r4 <= 0) goto L_0x0000;
        L_0x0017:
            r2 = 1000000; // 0xf4240 float:1.401298E-39 double:4.940656E-318;
            r4 = r0 / r2;
            r2 = r2 * r4;
            r6 = r0 - r2;
            r0 = r8.f780a;
            monitor-enter(r0);
            r1 = r8.f780a;	 Catch:{ InterruptedException -> 0x002b }
            r2 = (int) r6;	 Catch:{ InterruptedException -> 0x002b }
            r1.wait(r4, r2);	 Catch:{ InterruptedException -> 0x002b }
            goto L_0x002b;
        L_0x0029:
            r1 = move-exception;
            goto L_0x002d;
        L_0x002b:
            monitor-exit(r0);	 Catch:{ all -> 0x0029 }
            goto L_0x0000;	 Catch:{ all -> 0x0029 }
        L_0x002d:
            monitor-exit(r0);	 Catch:{ all -> 0x0029 }
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: c.j.1.run():void");
        }
    }

    public C0541j() {
        this(5, 5, TimeUnit.MINUTES);
    }

    public C0541j(int i, long j, TimeUnit timeUnit) {
        this.f787g = new C05401(this);
        this.f788h = new ArrayDeque();
        this.f783a = new C0471d();
        this.f785e = i;
        this.f786f = timeUnit.toNanos(j);
        if (j <= 0) {
            timeUnit = new StringBuilder();
            timeUnit.append("keepAliveDuration <= 0: ");
            timeUnit.append(j);
            throw new IllegalArgumentException(timeUnit.toString());
        }
    }

    @Nullable
    C1834c m837a(C0521a c0521a, C0476g c0476g, ae aeVar) {
        if (f781c || Thread.holdsLock(this)) {
            for (C1834c c1834c : this.f788h) {
                if (c1834c.m5056a(c0521a, aeVar)) {
                    c0476g.m464a(c1834c, f781c);
                    return c1834c;
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    @Nullable
    Socket m838a(C0521a c0521a, C0476g c0476g) {
        if (f781c || Thread.holdsLock(this)) {
            for (C1834c c1834c : this.f788h) {
                if (c1834c.m5056a(c0521a, null) && c1834c.m5063f() && c1834c != c0476g.m467b()) {
                    return c0476g.m463a(c1834c);
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    void m839a(C1834c c1834c) {
        if (f781c || Thread.holdsLock(this)) {
            if (!this.f784b) {
                this.f784b = f781c;
                f782d.execute(this.f787g);
            }
            this.f788h.add(c1834c);
            return;
        }
        throw new AssertionError();
    }

    boolean m840b(C1834c c1834c) {
        if (f781c || Thread.holdsLock(this)) {
            if (!c1834c.f4538a) {
                if (this.f785e != 0) {
                    notifyAll();
                    return null;
                }
            }
            this.f788h.remove(c1834c);
            return f781c;
        }
        throw new AssertionError();
    }

    long m836a(long j) {
        synchronized (this) {
            long j2 = Long.MIN_VALUE;
            int i = 0;
            C1834c c1834c = null;
            int i2 = i;
            for (C1834c c1834c2 : this.f788h) {
                if (m835a(c1834c2, j) > 0) {
                    i++;
                } else {
                    i2++;
                    long j3 = j - c1834c2.f4542e;
                    if (j3 > j2) {
                        c1834c = c1834c2;
                        j2 = j3;
                    }
                }
            }
            if (j2 < this.f786f) {
                if (i2 <= this.f785e) {
                    if (i2 > 0) {
                        long j4 = this.f786f - j2;
                        return j4;
                    } else if (i > 0) {
                        j = this.f786f;
                        return j;
                    } else {
                        this.f784b = false;
                        return -1;
                    }
                }
            }
            this.f788h.remove(c1834c);
            C0488c.m518a(c1834c.m5061d());
            return 0;
        }
    }

    private int m835a(C1834c c1834c, long j) {
        List list = c1834c.f4541d;
        int i = 0;
        while (i < list.size()) {
            Reference reference = (Reference) list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                C0475a c0475a = (C0475a) reference;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("A connection to ");
                stringBuilder.append(c1834c.mo1271a().m790a().m730a());
                stringBuilder.append(" was leaked. Did you forget to close a response body?");
                C0515e.m695b().mo1320a(stringBuilder.toString(), c0475a.f445a);
                list.remove(i);
                c1834c.f4538a = f781c;
                if (list.isEmpty()) {
                    c1834c.f4542e = j - this.f786f;
                    return 0;
                }
            }
        }
        return list.size();
    }
}
