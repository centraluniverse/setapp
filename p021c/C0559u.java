package p021c;

import java.io.IOException;
import javax.annotation.Nullable;

/* compiled from: Interceptor */
public interface C0559u {

    /* compiled from: Interceptor */
    public interface C0558a {
        aa mo1275a();

        ac mo1276a(aa aaVar) throws IOException;

        @Nullable
        C0539i mo1277b();

        int mo1278c();

        int mo1279d();

        int mo1280e();
    }

    ac mo1270a(C0558a c0558a) throws IOException;
}
