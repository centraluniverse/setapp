package p021c;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLSocket;
import p021c.p022a.C0488c;

/* compiled from: ConnectionSpec */
public final class C0543k {
    public static final C0543k f793a = new C0542a(true).m843a(f796h).m842a(af.TLS_1_3, af.TLS_1_2, af.TLS_1_1, af.TLS_1_0).m841a(true).m845a();
    public static final C0543k f794b = new C0542a(f793a).m842a(af.TLS_1_0).m841a(true).m845a();
    public static final C0543k f795c = new C0542a(false).m845a();
    private static final C0538h[] f796h = new C0538h[]{C0538h.aX, C0538h.bb, C0538h.aY, C0538h.bc, C0538h.bi, C0538h.bh, C0538h.ay, C0538h.aI, C0538h.az, C0538h.aJ, C0538h.ag, C0538h.ah, C0538h.f732E, C0538h.f736I, C0538h.f762i};
    final boolean f797d;
    final boolean f798e;
    @Nullable
    final String[] f799f;
    @Nullable
    final String[] f800g;

    /* compiled from: ConnectionSpec */
    public static final class C0542a {
        boolean f789a;
        @Nullable
        String[] f790b;
        @Nullable
        String[] f791c;
        boolean f792d;

        C0542a(boolean z) {
            this.f789a = z;
        }

        public C0542a(C0543k c0543k) {
            this.f789a = c0543k.f797d;
            this.f790b = c0543k.f799f;
            this.f791c = c0543k.f800g;
            this.f792d = c0543k.f798e;
        }

        public C0542a m843a(C0538h... c0538hArr) {
            if (this.f789a) {
                String[] strArr = new String[c0538hArr.length];
                for (int i = 0; i < c0538hArr.length; i++) {
                    strArr[i] = c0538hArr[i].bj;
                }
                return m844a(strArr);
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        public C0542a m844a(String... strArr) {
            if (!this.f789a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one cipher suite is required");
            } else {
                this.f790b = (String[]) strArr.clone();
                return this;
            }
        }

        public C0542a m842a(af... afVarArr) {
            if (this.f789a) {
                String[] strArr = new String[afVarArr.length];
                for (int i = 0; i < afVarArr.length; i++) {
                    strArr[i] = afVarArr[i].f687f;
                }
                return m846b(strArr);
            }
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }

        public C0542a m846b(String... strArr) {
            if (!this.f789a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length == 0) {
                throw new IllegalArgumentException("At least one TLS version is required");
            } else {
                this.f791c = (String[]) strArr.clone();
                return this;
            }
        }

        public C0542a m841a(boolean z) {
            if (this.f789a) {
                this.f792d = z;
                return this;
            }
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }

        public C0543k m845a() {
            return new C0543k(this);
        }
    }

    C0543k(C0542a c0542a) {
        this.f797d = c0542a.f789a;
        this.f799f = c0542a.f790b;
        this.f800g = c0542a.f791c;
        this.f798e = c0542a.f792d;
    }

    public boolean m849a() {
        return this.f797d;
    }

    @Nullable
    public List<C0538h> m851b() {
        return this.f799f != null ? C0538h.m832a(this.f799f) : null;
    }

    @Nullable
    public List<af> m852c() {
        return this.f800g != null ? af.m795a(this.f800g) : null;
    }

    public boolean m853d() {
        return this.f798e;
    }

    void m848a(SSLSocket sSLSocket, boolean z) {
        z = m847b(sSLSocket, z);
        if (z.f800g != null) {
            sSLSocket.setEnabledProtocols(z.f800g);
        }
        if (z.f799f != null) {
            sSLSocket.setEnabledCipherSuites(z.f799f);
        }
    }

    private C0543k m847b(SSLSocket sSLSocket, boolean z) {
        String[] a;
        String[] a2;
        if (this.f799f != null) {
            a = C0488c.m523a(C0538h.f754a, sSLSocket.getEnabledCipherSuites(), this.f799f);
        } else {
            a = sSLSocket.getEnabledCipherSuites();
        }
        if (this.f800g != null) {
            a2 = C0488c.m523a(C0488c.f478h, sSLSocket.getEnabledProtocols(), this.f800g);
        } else {
            a2 = sSLSocket.getEnabledProtocols();
        }
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        boolean a3 = C0488c.m506a(C0538h.f754a, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && !a3) {
            a = C0488c.m524a(a, supportedCipherSuites[a3]);
        }
        return new C0542a(this).m844a(a).m846b(a2).m845a();
    }

    public boolean m850a(SSLSocket sSLSocket) {
        if (!this.f797d) {
            return false;
        }
        if (this.f800g != null && !C0488c.m528b(C0488c.f478h, this.f800g, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        if (this.f799f == null || C0488c.m528b(C0538h.f754a, this.f799f, sSLSocket.getEnabledCipherSuites()) != null) {
            return true;
        }
        return false;
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof C0543k)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        C0543k c0543k = (C0543k) obj;
        if (this.f797d != c0543k.f797d) {
            return false;
        }
        return !this.f797d || (Arrays.equals(this.f799f, c0543k.f799f) && Arrays.equals(this.f800g, c0543k.f800g) && this.f798e == c0543k.f798e);
    }

    public int hashCode() {
        return this.f797d ? (31 * (((527 + Arrays.hashCode(this.f799f)) * 31) + Arrays.hashCode(this.f800g))) + (this.f798e ^ 1) : 17;
    }

    public String toString() {
        if (!this.f797d) {
            return "ConnectionSpec()";
        }
        String obj = this.f799f != null ? m851b().toString() : "[all enabled]";
        String obj2 = this.f800g != null ? m852c().toString() : "[all enabled]";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ConnectionSpec(cipherSuites=");
        stringBuilder.append(obj);
        stringBuilder.append(", tlsVersions=");
        stringBuilder.append(obj2);
        stringBuilder.append(", supportsTlsExtensions=");
        stringBuilder.append(this.f798e);
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
