package p021c;

import javax.annotation.Nullable;
import p021c.C0554s.C0553a;
import p021c.p022a.p025c.C0484f;

/* compiled from: Request */
public final class aa {
    final C0557t f643a;
    final String f644b;
    final C0554s f645c;
    @Nullable
    final ab f646d;
    final Object f647e;
    private volatile C0530d f648f;

    /* compiled from: Request */
    public static class C0522a {
        C0557t f638a;
        String f639b;
        C0553a f640c;
        ab f641d;
        Object f642e;

        public C0522a() {
            this.f639b = "GET";
            this.f640c = new C0553a();
        }

        C0522a(aa aaVar) {
            this.f638a = aaVar.f643a;
            this.f639b = aaVar.f644b;
            this.f641d = aaVar.f646d;
            this.f642e = aaVar.f647e;
            this.f640c = aaVar.f645c.m923b();
        }

        public C0522a m744a(C0557t c0557t) {
            if (c0557t == null) {
                throw new NullPointerException("url == null");
            }
            this.f638a = c0557t;
            return this;
        }

        public C0522a m745a(String str) {
            if (str == null) {
                throw new NullPointerException("url == null");
            }
            StringBuilder stringBuilder;
            if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("http:");
                stringBuilder.append(str.substring(3));
                str = stringBuilder.toString();
            } else {
                if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("https:");
                    stringBuilder.append(str.substring(4));
                    str = stringBuilder.toString();
                }
            }
            C0557t e = C0557t.m961e(str);
            if (e != null) {
                return m744a(e);
            }
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("unexpected url: ");
            stringBuilder2.append(str);
            throw new IllegalArgumentException(stringBuilder2.toString());
        }

        public C0522a m747a(String str, String str2) {
            this.f640c.m917c(str, str2);
            return this;
        }

        public C0522a m750b(String str, String str2) {
            this.f640c.m913a(str, str2);
            return this;
        }

        public C0522a m749b(String str) {
            this.f640c.m915b(str);
            return this;
        }

        public C0522a m743a(C0554s c0554s) {
            this.f640c = c0554s.m923b();
            return this;
        }

        public C0522a m742a(C0530d c0530d) {
            String c0530d2 = c0530d.toString();
            if (c0530d2.isEmpty()) {
                return m749b("Cache-Control");
            }
            return m747a("Cache-Control", c0530d2);
        }

        public C0522a m746a(String str, @Nullable ab abVar) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (abVar != null && !C0484f.m494c(str)) {
                r0 = new StringBuilder();
                r0.append("method ");
                r0.append(str);
                r0.append(" must not have a request body.");
                throw new IllegalArgumentException(r0.toString());
            } else if (abVar == null && C0484f.m493b(str)) {
                r0 = new StringBuilder();
                r0.append("method ");
                r0.append(str);
                r0.append(" must have a request body.");
                throw new IllegalArgumentException(r0.toString());
            } else {
                this.f639b = str;
                this.f641d = abVar;
                return this;
            }
        }

        public aa m748a() {
            if (this.f638a != null) {
                return new aa(this);
            }
            throw new IllegalStateException("url == null");
        }
    }

    aa(C0522a c0522a) {
        this.f643a = c0522a.f638a;
        this.f644b = c0522a.f639b;
        this.f645c = c0522a.f640c.m914a();
        this.f646d = c0522a.f641d;
        this.f647e = c0522a.f642e != null ? c0522a.f642e : this;
    }

    public C0557t m751a() {
        return this.f643a;
    }

    public String m753b() {
        return this.f644b;
    }

    public C0554s m754c() {
        return this.f645c;
    }

    public String m752a(String str) {
        return this.f645c.m922a(str);
    }

    @Nullable
    public ab m755d() {
        return this.f646d;
    }

    public C0522a m756e() {
        return new C0522a(this);
    }

    public C0530d m757f() {
        C0530d c0530d = this.f648f;
        if (c0530d != null) {
            return c0530d;
        }
        c0530d = C0530d.m803a(this.f645c);
        this.f648f = c0530d;
        return c0530d;
    }

    public boolean m758g() {
        return this.f643a.m965c();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Request{method=");
        stringBuilder.append(this.f644b);
        stringBuilder.append(", url=");
        stringBuilder.append(this.f643a);
        stringBuilder.append(", tag=");
        stringBuilder.append(this.f647e != this ? this.f647e : null);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
