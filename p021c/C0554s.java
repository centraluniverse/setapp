package p021c;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import p021c.p022a.C0488c;

/* compiled from: Headers */
public final class C0554s {
    private final String[] f841a;

    /* compiled from: Headers */
    public static final class C0553a {
        final List<String> f840a = new ArrayList(20);

        C0553a m912a(String str) {
            int indexOf = str.indexOf(":", 1);
            if (indexOf != -1) {
                return m916b(str.substring(0, indexOf), str.substring(indexOf + 1));
            }
            if (str.startsWith(":")) {
                return m916b("", str.substring(1));
            }
            return m916b("", str);
        }

        public C0553a m913a(String str, String str2) {
            m911d(str, str2);
            return m916b(str, str2);
        }

        C0553a m916b(String str, String str2) {
            this.f840a.add(str);
            this.f840a.add(str2.trim());
            return this;
        }

        public C0553a m915b(String str) {
            int i = 0;
            while (i < this.f840a.size()) {
                if (str.equalsIgnoreCase((String) this.f840a.get(i))) {
                    this.f840a.remove(i);
                    this.f840a.remove(i);
                    i -= 2;
                }
                i += 2;
            }
            return this;
        }

        public C0553a m917c(String str, String str2) {
            m911d(str, str2);
            m915b(str);
            m916b(str, str2);
            return this;
        }

        private void m911d(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str.isEmpty()) {
                throw new IllegalArgumentException("name is empty");
            } else {
                char charAt;
                int length = str.length();
                int i = 0;
                while (i < length) {
                    charAt = str.charAt(i);
                    if (charAt > ' ') {
                        if (charAt < '') {
                            i++;
                        }
                    }
                    throw new IllegalArgumentException(C0488c.m510a("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str));
                }
                if (str2 == null) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("value for name ");
                    stringBuilder.append(str);
                    stringBuilder.append(" == null");
                    throw new NullPointerException(stringBuilder.toString());
                }
                length = str2.length();
                i = 0;
                while (i < length) {
                    charAt = str2.charAt(i);
                    if ((charAt > '\u001f' || charAt == '\t') && charAt < '') {
                        i++;
                    } else {
                        throw new IllegalArgumentException(C0488c.m510a("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt), Integer.valueOf(i), str, str2));
                    }
                }
            }
        }

        public C0554s m914a() {
            return new C0554s(this);
        }
    }

    C0554s(C0553a c0553a) {
        this.f841a = (String[]) c0553a.f840a.toArray(new String[c0553a.f840a.size()]);
    }

    private C0554s(String[] strArr) {
        this.f841a = strArr;
    }

    @Nullable
    public String m922a(String str) {
        return C0554s.m919a(this.f841a, str);
    }

    public int m920a() {
        return this.f841a.length / 2;
    }

    public String m921a(int i) {
        return this.f841a[i * 2];
    }

    public String m924b(int i) {
        return this.f841a[(i * 2) + 1];
    }

    public List<String> m925b(String str) {
        int a = m920a();
        List list = null;
        for (int i = 0; i < a; i++) {
            if (str.equalsIgnoreCase(m921a(i))) {
                if (list == null) {
                    list = new ArrayList(2);
                }
                list.add(m924b(i));
            }
        }
        if (list != null) {
            return Collections.unmodifiableList(list);
        }
        return Collections.emptyList();
    }

    public C0553a m923b() {
        C0553a c0553a = new C0553a();
        Collections.addAll(c0553a.f840a, this.f841a);
        return c0553a;
    }

    public boolean equals(@Nullable Object obj) {
        return (!(obj instanceof C0554s) || Arrays.equals(((C0554s) obj).f841a, this.f841a) == null) ? null : true;
    }

    public int hashCode() {
        return Arrays.hashCode(this.f841a);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        int a = m920a();
        for (int i = 0; i < a; i++) {
            stringBuilder.append(m921a(i));
            stringBuilder.append(": ");
            stringBuilder.append(m924b(i));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    private static String m919a(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    public static C0554s m918a(String... strArr) {
        if (strArr == null) {
            throw new NullPointerException("namesAndValues == null");
        } else if (strArr.length % 2 != 0) {
            throw new IllegalArgumentException("Expected alternating header names and values");
        } else {
            int i;
            strArr = (String[]) strArr.clone();
            for (i = 0; i < strArr.length; i++) {
                if (strArr[i] == null) {
                    throw new IllegalArgumentException("Headers cannot be null");
                }
                strArr[i] = strArr[i].trim();
            }
            i = 0;
            while (i < strArr.length) {
                String str = strArr[i];
                String str2 = strArr[i + 1];
                if (str.length() != 0 && str.indexOf(0) == -1) {
                    if (str2.indexOf(0) == -1) {
                        i += 2;
                    }
                }
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Unexpected header: ");
                stringBuilder.append(str);
                stringBuilder.append(": ");
                stringBuilder.append(str2);
                throw new IllegalArgumentException(stringBuilder.toString());
            }
            return new C0554s(strArr);
        }
    }
}
