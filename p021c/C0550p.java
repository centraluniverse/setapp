package p021c;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import javax.annotation.Nullable;

/* compiled from: EventListener */
public abstract class C0550p {
    public static final C0550p f832a = new C18781();

    /* compiled from: EventListener */
    public interface C0549a {
        C0550p mo1334a(C0532e c0532e);
    }

    /* compiled from: EventListener */
    class C18781 extends C0550p {
        C18781() {
        }
    }

    /* compiled from: EventListener */
    class C18792 implements C0549a {
        final /* synthetic */ C0550p f4689a;

        C18792(C0550p c0550p) {
            this.f4689a = c0550p;
        }

        public C0550p mo1334a(C0532e c0532e) {
            return this.f4689a;
        }
    }

    public void m885a(C0532e c0532e) {
    }

    public void m886a(C0532e c0532e, long j) {
    }

    public void m887a(C0532e c0532e, aa aaVar) {
    }

    public void m888a(C0532e c0532e, ac acVar) {
    }

    public void m889a(C0532e c0532e, C0539i c0539i) {
    }

    public void m890a(C0532e c0532e, @Nullable C0552r c0552r) {
    }

    public void m891a(C0532e c0532e, IOException iOException) {
    }

    public void m892a(C0532e c0532e, String str) {
    }

    public void m893a(C0532e c0532e, String str, @Nullable List<InetAddress> list) {
    }

    public void m894a(C0532e c0532e, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    public void m895a(C0532e c0532e, InetSocketAddress inetSocketAddress, @Nullable Proxy proxy, @Nullable C0564y c0564y) {
    }

    public void m896a(C0532e c0532e, InetSocketAddress inetSocketAddress, @Nullable Proxy proxy, @Nullable C0564y c0564y, @Nullable IOException iOException) {
    }

    public void m897b(C0532e c0532e) {
    }

    public void m898b(C0532e c0532e, long j) {
    }

    public void m899b(C0532e c0532e, C0539i c0539i) {
    }

    public void m900c(C0532e c0532e) {
    }

    public void m901d(C0532e c0532e) {
    }

    public void m902e(C0532e c0532e) {
    }

    public void m903f(C0532e c0532e) {
    }

    public void m904g(C0532e c0532e) {
    }

    static C0549a m884a(C0550p c0550p) {
        return new C18792(c0550p);
    }
}
