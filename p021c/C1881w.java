package p021c;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.Nullable;
import p021c.p022a.C0488c;
import p125d.C1624f;
import p125d.C2283d;
import p125d.C2453c;

/* compiled from: MultipartBody */
public final class C1881w extends ab {
    public static final C0560v f4693a = C0560v.m986a("multipart/mixed");
    public static final C0560v f4694b = C0560v.m986a("multipart/alternative");
    public static final C0560v f4695c = C0560v.m986a("multipart/digest");
    public static final C0560v f4696d = C0560v.m986a("multipart/parallel");
    public static final C0560v f4697e = C0560v.m986a("multipart/form-data");
    private static final byte[] f4698f = new byte[]{(byte) 58, (byte) 32};
    private static final byte[] f4699g = new byte[]{(byte) 13, (byte) 10};
    private static final byte[] f4700h = new byte[]{(byte) 45, (byte) 45};
    private final C1624f f4701i;
    private final C0560v f4702j;
    private final C0560v f4703k;
    private final List<C0562b> f4704l;
    private long f4705m = -1;

    /* compiled from: MultipartBody */
    public static final class C0561a {
        private final C1624f f872a;
        private C0560v f873b;
        private final List<C0562b> f874c;

        public C0561a() {
            this(UUID.randomUUID().toString());
        }

        public C0561a(String str) {
            this.f873b = C1881w.f4693a;
            this.f874c = new ArrayList();
            this.f872a = C1624f.m4820a(str);
        }

        public C0561a m991a(C0560v c0560v) {
            if (c0560v == null) {
                throw new NullPointerException("type == null");
            } else if (c0560v.m987a().equals("multipart")) {
                this.f873b = c0560v;
                return this;
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("multipart != ");
                stringBuilder.append(c0560v);
                throw new IllegalArgumentException(stringBuilder.toString());
            }
        }

        public C0561a m990a(@Nullable C0554s c0554s, ab abVar) {
            return m992a(C0562b.m994a(c0554s, abVar));
        }

        public C0561a m992a(C0562b c0562b) {
            if (c0562b == null) {
                throw new NullPointerException("part == null");
            }
            this.f874c.add(c0562b);
            return this;
        }

        public C1881w m993a() {
            if (!this.f874c.isEmpty()) {
                return new C1881w(this.f872a, this.f873b, this.f874c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }
    }

    /* compiled from: MultipartBody */
    public static final class C0562b {
        @Nullable
        final C0554s f875a;
        final ab f876b;

        public static C0562b m994a(@Nullable C0554s c0554s, ab abVar) {
            if (abVar == null) {
                throw new NullPointerException("body == null");
            } else if (c0554s != null && c0554s.m922a("Content-Type") != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (c0554s == null || c0554s.m922a("Content-Length") == null) {
                return new C0562b(c0554s, abVar);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }

        private C0562b(@Nullable C0554s c0554s, ab abVar) {
            this.f875a = c0554s;
            this.f876b = abVar;
        }
    }

    C1881w(C1624f c1624f, C0560v c0560v, List<C0562b> list) {
        this.f4701i = c1624f;
        this.f4702j = c0560v;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(c0560v);
        stringBuilder.append("; boundary=");
        stringBuilder.append(c1624f.mo1902a());
        this.f4703k = C0560v.m986a(stringBuilder.toString());
        this.f4704l = C0488c.m513a((List) list);
    }

    public C0560v contentType() {
        return this.f4703k;
    }

    public long contentLength() throws IOException {
        long j = this.f4705m;
        if (j != -1) {
            return j;
        }
        j = m5187a(null, true);
        this.f4705m = j;
        return j;
    }

    public void writeTo(C2283d c2283d) throws IOException {
        m5187a(c2283d, false);
    }

    private long m5187a(@Nullable C2283d c2283d, boolean z) throws IOException {
        C2453c c2453c;
        long a;
        if (z) {
            c2283d = new C2453c();
            c2453c = c2283d;
        } else {
            c2453c = null;
        }
        int size = this.f4704l.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            C0562b c0562b = (C0562b) this.f4704l.get(i);
            C0554s c0554s = c0562b.f875a;
            ab abVar = c0562b.f876b;
            c2283d.mo2520c(f4700h);
            c2283d.mo2517b(this.f4701i);
            c2283d.mo2520c(f4699g);
            if (c0554s != null) {
                int a2 = c0554s.m920a();
                for (int i2 = 0; i2 < a2; i2++) {
                    c2283d.mo2518b(c0554s.m921a(i2)).mo2520c(f4698f).mo2518b(c0554s.m924b(i2)).mo2520c(f4699g);
                }
            }
            C0560v contentType = abVar.contentType();
            if (contentType != null) {
                c2283d.mo2518b("Content-Type: ").mo2518b(contentType.toString()).mo2520c(f4699g);
            }
            long contentLength = abVar.contentLength();
            if (contentLength != -1) {
                c2283d.mo2518b("Content-Length: ").mo2540o(contentLength).mo2520c(f4699g);
            } else if (z) {
                c2453c.m7776w();
                return -1;
            }
            c2283d.mo2520c(f4699g);
            if (z) {
                j += contentLength;
            } else {
                abVar.writeTo(c2283d);
            }
            c2283d.mo2520c(f4699g);
        }
        c2283d.mo2520c(f4700h);
        c2283d.mo2517b(this.f4701i);
        c2283d.mo2520c(f4700h);
        c2283d.mo2520c(f4699g);
        if (z) {
            a = j + c2453c.m7705a();
            c2453c.m7776w();
        } else {
            a = j;
        }
        return a;
    }
}
