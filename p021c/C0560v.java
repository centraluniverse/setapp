package p021c;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;

/* compiled from: MediaType */
public final class C0560v {
    private static final Pattern f866a = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");
    private static final Pattern f867b = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");
    private final String f868c;
    private final String f869d;
    private final String f870e;
    @Nullable
    private final String f871f;

    private C0560v(String str, String str2, String str3, @Nullable String str4) {
        this.f868c = str;
        this.f869d = str2;
        this.f870e = str3;
        this.f871f = str4;
    }

    @Nullable
    public static C0560v m986a(String str) {
        Matcher matcher = f866a.matcher(str);
        if (!matcher.lookingAt()) {
            return null;
        }
        String toLowerCase = matcher.group(1).toLowerCase(Locale.US);
        String toLowerCase2 = matcher.group(2).toLowerCase(Locale.US);
        Matcher matcher2 = f867b.matcher(str);
        String str2 = null;
        for (int end = matcher.end(); end < str.length(); end = matcher2.end()) {
            matcher2.region(end, str.length());
            if (!matcher2.lookingAt()) {
                return null;
            }
            String group = matcher2.group(1);
            if (group != null) {
                if (group.equalsIgnoreCase("charset")) {
                    group = matcher2.group(2);
                    if (group == null) {
                        group = matcher2.group(3);
                    } else if (group.startsWith("'") && group.endsWith("'") && group.length() > 2) {
                        group = group.substring(1, group.length() - 1);
                    }
                    if (str2 != null && !group.equalsIgnoreCase(str2)) {
                        return null;
                    }
                    str2 = group;
                }
            }
        }
        return new C0560v(str, toLowerCase, toLowerCase2, str2);
    }

    public String m987a() {
        return this.f869d;
    }

    @Nullable
    public Charset m989b() {
        return m988a(null);
    }

    @javax.annotation.Nullable
    public java.nio.charset.Charset m988a(@javax.annotation.Nullable java.nio.charset.Charset r2) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r1 = this;
        r0 = r1.f871f;	 Catch:{ IllegalArgumentException -> 0x000c }
        if (r0 == 0) goto L_0x000b;	 Catch:{ IllegalArgumentException -> 0x000c }
    L_0x0004:
        r0 = r1.f871f;	 Catch:{ IllegalArgumentException -> 0x000c }
        r0 = java.nio.charset.Charset.forName(r0);	 Catch:{ IllegalArgumentException -> 0x000c }
        r2 = r0;
    L_0x000b:
        return r2;
    L_0x000c:
        return r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.v.a(java.nio.charset.Charset):java.nio.charset.Charset");
    }

    public String toString() {
        return this.f868c;
    }

    public boolean equals(@Nullable Object obj) {
        return (!(obj instanceof C0560v) || ((C0560v) obj).f868c.equals(this.f868c) == null) ? null : true;
    }

    public int hashCode() {
        return this.f868c.hashCode();
    }
}
