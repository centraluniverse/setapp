package p021c;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import p021c.p022a.p023a.C0467d;
import p021c.p022a.p023a.C0468e;

/* compiled from: Cache */
public final class C0528c implements Closeable, Flushable {
    final C0468e f695a;
    final C0467d f696b;

    public void flush() throws IOException {
        this.f696b.flush();
    }

    public void close() throws IOException {
        this.f696b.close();
    }
}
