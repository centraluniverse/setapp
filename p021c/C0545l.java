package p021c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import p021c.p022a.C0488c;
import p021c.p022a.p025c.C0482d;

/* compiled from: Cookie */
public final class C0545l {
    private static final Pattern f810a = Pattern.compile("(\\d{2,4})[^\\d]*");
    private static final Pattern f811b = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");
    private static final Pattern f812c = Pattern.compile("(\\d{1,2})[^\\d]*");
    private static final Pattern f813d = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");
    private final String f814e;
    private final String f815f;
    private final long f816g;
    private final String f817h;
    private final String f818i;
    private final boolean f819j;
    private final boolean f820k;
    private final boolean f821l;
    private final boolean f822m;

    /* compiled from: Cookie */
    public static final class C0544a {
        String f801a;
        String f802b;
        long f803c = 253402300799999L;
        String f804d;
        String f805e = "/";
        boolean f806f;
        boolean f807g;
        boolean f808h;
        boolean f809i;

        public C0544a m855a(String str) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str.trim().equals(str)) {
                this.f801a = str;
                return this;
            } else {
                throw new IllegalArgumentException("name is not trimmed");
            }
        }

        public C0544a m857b(String str) {
            if (str == null) {
                throw new NullPointerException("value == null");
            } else if (str.trim().equals(str)) {
                this.f802b = str;
                return this;
            } else {
                throw new IllegalArgumentException("value is not trimmed");
            }
        }

        public C0544a m858c(String str) {
            return m854a(str, false);
        }

        private C0544a m854a(String str, boolean z) {
            if (str == null) {
                throw new NullPointerException("domain == null");
            }
            String a = C0488c.m509a(str);
            if (a == null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("unexpected domain: ");
                stringBuilder.append(str);
                throw new IllegalArgumentException(stringBuilder.toString());
            }
            this.f804d = a;
            this.f809i = z;
            return this;
        }

        public C0545l m856a() {
            return new C0545l(this);
        }
    }

    private C0545l(String str, String str2, long j, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f814e = str;
        this.f815f = str2;
        this.f816g = j;
        this.f817h = str3;
        this.f818i = str4;
        this.f819j = z;
        this.f820k = z2;
        this.f822m = z3;
        this.f821l = z4;
    }

    C0545l(C0544a c0544a) {
        if (c0544a.f801a == null) {
            throw new NullPointerException("builder.name == null");
        } else if (c0544a.f802b == null) {
            throw new NullPointerException("builder.value == null");
        } else if (c0544a.f804d == null) {
            throw new NullPointerException("builder.domain == null");
        } else {
            this.f814e = c0544a.f801a;
            this.f815f = c0544a.f802b;
            this.f816g = c0544a.f803c;
            this.f817h = c0544a.f804d;
            this.f818i = c0544a.f805e;
            this.f819j = c0544a.f806f;
            this.f820k = c0544a.f807g;
            this.f821l = c0544a.f808h;
            this.f822m = c0544a.f809i;
        }
    }

    public String m867a() {
        return this.f814e;
    }

    public String m869b() {
        return this.f815f;
    }

    private static boolean m865a(String str, String str2) {
        if (str.equals(str2)) {
            return true;
        }
        if (str.endsWith(str2) && str.charAt((str.length() - str2.length()) - 1) == 46 && C0488c.m530c(str) == null) {
            return true;
        }
        return null;
    }

    @Nullable
    public static C0545l m863a(C0557t c0557t, String str) {
        return C0545l.m862a(System.currentTimeMillis(), c0557t, str);
    }

    @javax.annotation.Nullable
    static p021c.C0545l m862a(long r26, p021c.C0557t r28, java.lang.String r29) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r2 = r29;
        r3 = r29.length();
        r4 = 59;
        r5 = 0;
        r6 = p021c.p022a.C0488c.m503a(r2, r5, r3, r4);
        r7 = 61;
        r8 = p021c.p022a.C0488c.m503a(r2, r5, r6, r7);
        r9 = 0;
        if (r8 != r6) goto L_0x0017;
    L_0x0016:
        return r9;
    L_0x0017:
        r11 = p021c.p022a.C0488c.m529c(r2, r5, r8);
        r10 = r11.isEmpty();
        if (r10 != 0) goto L_0x014e;
    L_0x0021:
        r10 = p021c.p022a.C0488c.m525b(r11);
        r12 = -1;
        if (r10 == r12) goto L_0x002a;
    L_0x0028:
        goto L_0x014e;
    L_0x002a:
        r10 = 1;
        r8 = r8 + r10;
        r8 = p021c.p022a.C0488c.m529c(r2, r8, r6);
        r13 = p021c.p022a.C0488c.m525b(r8);
        if (r13 == r12) goto L_0x0037;
    L_0x0036:
        return r9;
    L_0x0037:
        r6 = r6 + r10;
        r12 = -1;
        r14 = 253402300799999; // 0xe677d21fdbff float:-1.71647681E11 double:1.251973714024093E-309;
        r18 = r5;
        r19 = r18;
        r24 = r19;
        r21 = r9;
        r20 = r10;
        r16 = r12;
        r22 = r14;
        r10 = r21;
    L_0x004f:
        if (r6 >= r3) goto L_0x00c4;
    L_0x0051:
        r9 = p021c.p022a.C0488c.m503a(r2, r6, r3, r4);
        r4 = p021c.p022a.C0488c.m503a(r2, r6, r9, r7);
        r6 = p021c.p022a.C0488c.m529c(r2, r6, r4);
        if (r4 >= r9) goto L_0x0066;
    L_0x005f:
        r4 = r4 + 1;
        r4 = p021c.p022a.C0488c.m529c(r2, r4, r9);
        goto L_0x0068;
    L_0x0066:
        r4 = "";
    L_0x0068:
        r7 = "expires";
        r7 = r6.equalsIgnoreCase(r7);
        if (r7 == 0) goto L_0x007d;
    L_0x0070:
        r6 = r4.length();	 Catch:{ IllegalArgumentException -> 0x00bc }
        r6 = p021c.C0545l.m861a(r4, r5, r6);	 Catch:{ IllegalArgumentException -> 0x00bc }
        r22 = r6;
    L_0x007a:
        r24 = 1;
        goto L_0x00bc;
    L_0x007d:
        r7 = "max-age";
        r7 = r6.equalsIgnoreCase(r7);
        if (r7 == 0) goto L_0x008c;
    L_0x0085:
        r6 = p021c.C0545l.m860a(r4);	 Catch:{  }
        r16 = r6;
        goto L_0x007a;
    L_0x008c:
        r7 = "domain";
        r7 = r6.equalsIgnoreCase(r7);
        if (r7 == 0) goto L_0x009c;
    L_0x0094:
        r4 = p021c.C0545l.m866b(r4);	 Catch:{ IllegalArgumentException -> 0x00bc }
        r10 = r4;
        r20 = r5;
        goto L_0x00bc;
    L_0x009c:
        r7 = "path";
        r7 = r6.equalsIgnoreCase(r7);
        if (r7 == 0) goto L_0x00a7;
    L_0x00a4:
        r21 = r4;
        goto L_0x00bc;
    L_0x00a7:
        r4 = "secure";
        r4 = r6.equalsIgnoreCase(r4);
        if (r4 == 0) goto L_0x00b2;
    L_0x00af:
        r18 = 1;
        goto L_0x00bc;
    L_0x00b2:
        r4 = "httponly";
        r4 = r6.equalsIgnoreCase(r4);
        if (r4 == 0) goto L_0x00bc;
    L_0x00ba:
        r19 = 1;
    L_0x00bc:
        r6 = r9 + 1;
        r4 = 59;
        r7 = 61;
        r9 = 0;
        goto L_0x004f;
    L_0x00c4:
        r2 = -9223372036854775808;
        r4 = (r16 > r2 ? 1 : (r16 == r2 ? 0 : -1));
        if (r4 != 0) goto L_0x00cc;
    L_0x00ca:
        r13 = r2;
        goto L_0x00f1;
    L_0x00cc:
        r2 = (r16 > r12 ? 1 : (r16 == r12 ? 0 : -1));
        if (r2 == 0) goto L_0x00ef;
    L_0x00d0:
        r2 = 9223372036854775; // 0x20c49ba5e353f7 float:-3.943512E-16 double:4.663754807431093E-308;
        r4 = (r16 > r2 ? 1 : (r16 == r2 ? 0 : -1));
        if (r4 > 0) goto L_0x00de;
    L_0x00d9:
        r2 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r16 = r16 * r2;
        goto L_0x00e3;
    L_0x00de:
        r16 = 9223372036854775807; // 0x7fffffffffffffff float:NaN double:NaN;
    L_0x00e3:
        r2 = r26 + r16;
        r4 = (r2 > r26 ? 1 : (r2 == r26 ? 0 : -1));
        if (r4 < 0) goto L_0x00ed;
    L_0x00e9:
        r0 = (r2 > r14 ? 1 : (r2 == r14 ? 0 : -1));
        if (r0 <= 0) goto L_0x00ca;
    L_0x00ed:
        r13 = r14;
        goto L_0x00f1;
    L_0x00ef:
        r13 = r22;
    L_0x00f1:
        r0 = r28.m969f();
        if (r10 != 0) goto L_0x00fa;
    L_0x00f7:
        r15 = r0;
        r1 = 0;
        goto L_0x0104;
    L_0x00fa:
        r1 = p021c.C0545l.m865a(r0, r10);
        if (r1 != 0) goto L_0x0102;
    L_0x0100:
        r1 = 0;
        return r1;
    L_0x0102:
        r1 = 0;
        r15 = r10;
    L_0x0104:
        r0 = r0.length();
        r2 = r15.length();
        if (r0 == r2) goto L_0x0119;
    L_0x010e:
        r0 = p021c.p022a.p030h.C0516a.m707a();
        r0 = r0.m712a(r15);
        if (r0 != 0) goto L_0x0119;
    L_0x0118:
        return r1;
    L_0x0119:
        r9 = r21;
        if (r9 == 0) goto L_0x0129;
    L_0x011d:
        r0 = "/";
        r0 = r9.startsWith(r0);
        if (r0 != 0) goto L_0x0126;
    L_0x0125:
        goto L_0x0129;
    L_0x0126:
        r16 = r9;
        goto L_0x013e;
    L_0x0129:
        r0 = r28.m971h();
        r1 = 47;
        r1 = r0.lastIndexOf(r1);
        if (r1 == 0) goto L_0x013a;
    L_0x0135:
        r0 = r0.substring(r5, r1);
        goto L_0x013c;
    L_0x013a:
        r0 = "/";
    L_0x013c:
        r16 = r0;
    L_0x013e:
        r0 = new c.l;
        r10 = r0;
        r12 = r8;
        r17 = r18;
        r18 = r19;
        r19 = r20;
        r20 = r24;
        r10.<init>(r11, r12, r13, r15, r16, r17, r18, r19, r20);
        return r0;
    L_0x014e:
        r0 = r9;
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.l.a(long, c.t, java.lang.String):c.l");
    }

    private static long m861a(String str, int i, int i2) {
        i = C0545l.m859a(str, i, i2, false);
        Matcher matcher = f813d.matcher(str);
        int i3 = -1;
        int i4 = i3;
        int i5 = i4;
        int i6 = i5;
        int i7 = i6;
        int i8 = i7;
        while (i < i2) {
            int a = C0545l.m859a(str, i + 1, i2, true);
            matcher.region(i, a);
            if (i3 == -1 && matcher.usePattern(f813d).matches() != 0) {
                i = Integer.parseInt(matcher.group(1));
                i3 = Integer.parseInt(matcher.group(2));
                i8 = Integer.parseInt(matcher.group(3));
                i7 = i3;
                i3 = i;
            } else if (i5 == -1 && matcher.usePattern(f812c).matches() != 0) {
                i5 = Integer.parseInt(matcher.group(1));
            } else if (i6 == -1 && matcher.usePattern(f811b).matches() != 0) {
                i6 = f811b.pattern().indexOf(matcher.group(1).toLowerCase(Locale.US)) / 4;
            } else if (i4 == -1 && matcher.usePattern(f810a).matches() != 0) {
                i4 = Integer.parseInt(matcher.group(1));
            }
            i = C0545l.m859a(str, a + 1, i2, false);
        }
        if (i4 >= 70 && i4 <= 99) {
            i4 += 1900;
        }
        if (i4 >= 0 && i4 <= 69) {
            i4 += 2000;
        }
        if (i4 < 1601) {
            throw new IllegalArgumentException();
        } else if (i6 == -1) {
            throw new IllegalArgumentException();
        } else {
            if (i5 >= 1) {
                if (i5 <= 31) {
                    if (i3 >= 0) {
                        if (i3 <= 23) {
                            if (i7 >= 0) {
                                if (i7 <= 59) {
                                    if (i8 >= 0) {
                                        if (i8 <= 59) {
                                            str = new GregorianCalendar(C0488c.f477g);
                                            str.setLenient(false);
                                            str.set(1, i4);
                                            str.set(2, i6 - 1);
                                            str.set(5, i5);
                                            str.set(11, i3);
                                            str.set(12, i7);
                                            str.set(13, i8);
                                            str.set(14, 0);
                                            return str.getTimeInMillis();
                                        }
                                    }
                                    throw new IllegalArgumentException();
                                }
                            }
                            throw new IllegalArgumentException();
                        }
                    }
                    throw new IllegalArgumentException();
                }
            }
            throw new IllegalArgumentException();
        }
    }

    private static int m859a(String str, int i, int i2, boolean z) {
        while (i < i2) {
            int i3;
            char charAt = str.charAt(i);
            if ((charAt >= ' ' || charAt == '\t') && charAt < '' && ((charAt < '0' || charAt > '9') && ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')))) {
                if (charAt != ':') {
                    i3 = 0;
                    if (i3 == (z ^ 1)) {
                        return i;
                    }
                    i++;
                }
            }
            i3 = 1;
            if (i3 == (z ^ 1)) {
                return i;
            }
            i++;
        }
        return i2;
    }

    private static long m860a(String str) {
        long j = Long.MIN_VALUE;
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 0) {
                j = parseLong;
            }
            return j;
        } catch (NumberFormatException e) {
            if (str.matches("-?\\d+")) {
                if (str.startsWith("-") == null) {
                    j = Long.MAX_VALUE;
                }
                return j;
            }
            throw e;
        }
    }

    private static String m866b(String str) {
        if (str.endsWith(".")) {
            throw new IllegalArgumentException();
        }
        if (str.startsWith(".")) {
            str = str.substring(1);
        }
        str = C0488c.m509a(str);
        if (str != null) {
            return str;
        }
        throw new IllegalArgumentException();
    }

    public static List<C0545l> m864a(C0557t c0557t, C0554s c0554s) {
        c0554s = c0554s.m925b("Set-Cookie");
        int size = c0554s.size();
        List list = null;
        for (int i = 0; i < size; i++) {
            C0545l a = C0545l.m863a(c0557t, (String) c0554s.get(i));
            if (a != null) {
                if (list == null) {
                    list = new ArrayList();
                }
                list.add(a);
            }
        }
        if (list != null) {
            return Collections.unmodifiableList(list);
        }
        return Collections.emptyList();
    }

    public String toString() {
        return m868a(false);
    }

    String m868a(boolean z) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.f814e);
        stringBuilder.append('=');
        stringBuilder.append(this.f815f);
        if (this.f821l) {
            if (this.f816g == Long.MIN_VALUE) {
                stringBuilder.append("; max-age=0");
            } else {
                stringBuilder.append("; expires=");
                stringBuilder.append(C0482d.m482a(new Date(this.f816g)));
            }
        }
        if (!this.f822m) {
            stringBuilder.append("; domain=");
            if (z) {
                stringBuilder.append(".");
            }
            stringBuilder.append(this.f817h);
        }
        stringBuilder.append("; path=");
        stringBuilder.append(this.f818i);
        if (this.f819j) {
            stringBuilder.append("; secure");
        }
        if (this.f820k) {
            stringBuilder.append("; httponly");
        }
        return stringBuilder.toString();
    }

    public boolean equals(@Nullable Object obj) {
        boolean z = false;
        if (!(obj instanceof C0545l)) {
            return false;
        }
        C0545l c0545l = (C0545l) obj;
        if (c0545l.f814e.equals(this.f814e) && c0545l.f815f.equals(this.f815f) && c0545l.f817h.equals(this.f817h) && c0545l.f818i.equals(this.f818i) && c0545l.f816g == this.f816g && c0545l.f819j == this.f819j && c0545l.f820k == this.f820k && c0545l.f821l == this.f821l && c0545l.f822m == this.f822m) {
            z = true;
        }
        return z;
    }

    public int hashCode() {
        return (31 * (((((((((((((((527 + this.f814e.hashCode()) * 31) + this.f815f.hashCode()) * 31) + this.f817h.hashCode()) * 31) + this.f818i.hashCode()) * 31) + ((int) (this.f816g ^ (this.f816g >>> 32)))) * 31) + (this.f819j ^ 1)) * 31) + (this.f820k ^ 1)) * 31) + (this.f821l ^ 1))) + (this.f822m ^ 1);
    }
}
