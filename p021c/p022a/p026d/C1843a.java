package p021c.p022a.p026d;

import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import p021c.C0554s;
import p021c.C0554s.C0553a;
import p021c.C0557t;
import p021c.C1883x;
import p021c.aa;
import p021c.ac;
import p021c.ac.C0523a;
import p021c.ad;
import p021c.p022a.C0469a;
import p021c.p022a.C0488c;
import p021c.p022a.p024b.C0476g;
import p021c.p022a.p024b.C1834c;
import p021c.p022a.p025c.C0480c;
import p021c.p022a.p025c.C0483e;
import p021c.p022a.p025c.C0485i;
import p021c.p022a.p025c.C0486k;
import p021c.p022a.p025c.C1838h;
import p125d.C1625m;
import p125d.C1629s;
import p125d.C1630t;
import p125d.C1631u;
import p125d.C2283d;
import p125d.C2284e;
import p125d.C2288j;
import p125d.C2453c;

/* compiled from: Http1Codec */
public final class C1843a implements C0480c {
    final C1883x f4585a;
    final C0476g f4586b;
    final C2284e f4587c;
    final C2283d f4588d;
    int f4589e = 0;

    /* compiled from: Http1Codec */
    private abstract class C1840a implements C1630t {
        protected final C2288j f4574a;
        protected boolean f4575b;
        protected long f4576c;
        final /* synthetic */ C1843a f4577d;

        private C1840a(C1843a c1843a) {
            this.f4577d = c1843a;
            this.f4574a = new C2288j(this.f4577d.f4587c.timeout());
            this.f4576c = 0;
        }

        public C1631u timeout() {
            return this.f4574a;
        }

        public long read(C2453c c2453c, long j) throws IOException {
            try {
                c2453c = this.f4577d.f4587c.read(c2453c, j);
                if (c2453c > 0) {
                    this.f4576c += c2453c;
                }
                return c2453c;
            } catch (C2453c c2453c2) {
                m5087a(0, c2453c2);
                throw c2453c2;
            }
        }

        protected final void m5087a(boolean z, IOException iOException) throws IOException {
            if (this.f4577d.f4589e != 6) {
                if (this.f4577d.f4589e != 5) {
                    iOException = new StringBuilder();
                    iOException.append("state: ");
                    iOException.append(this.f4577d.f4589e);
                    throw new IllegalStateException(iOException.toString());
                }
                this.f4577d.m5098a(this.f4574a);
                this.f4577d.f4589e = 6;
                if (this.f4577d.f4586b != null) {
                    this.f4577d.f4586b.m466a(z ^ 1, this.f4577d, this.f4576c, iOException);
                }
            }
        }
    }

    /* compiled from: Http1Codec */
    private final class C1841b implements C1629s {
        final /* synthetic */ C1843a f4578a;
        private final C2288j f4579b = new C2288j(this.f4578a.f4588d.timeout());
        private boolean f4580c;

        C1841b(C1843a c1843a) {
            this.f4578a = c1843a;
        }

        public C1631u timeout() {
            return this.f4579b;
        }

        public void mo1284a(C2453c c2453c, long j) throws IOException {
            if (this.f4580c) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                this.f4578a.f4588d.mo2538n(j);
                this.f4578a.f4588d.mo2518b("\r\n");
                this.f4578a.f4588d.mo1284a(c2453c, j);
                this.f4578a.f4588d.mo2518b("\r\n");
            }
        }

        public synchronized void flush() throws IOException {
            if (!this.f4580c) {
                this.f4578a.f4588d.flush();
            }
        }

        public synchronized void close() throws IOException {
            if (!this.f4580c) {
                this.f4580c = true;
                this.f4578a.f4588d.mo2518b("0\r\n\r\n");
                this.f4578a.m5098a(this.f4579b);
                this.f4578a.f4589e = 3;
            }
        }
    }

    /* compiled from: Http1Codec */
    private final class C1842d implements C1629s {
        final /* synthetic */ C1843a f4581a;
        private final C2288j f4582b = new C2288j(this.f4581a.f4588d.timeout());
        private boolean f4583c;
        private long f4584d;

        C1842d(C1843a c1843a, long j) {
            this.f4581a = c1843a;
            this.f4584d = j;
        }

        public C1631u timeout() {
            return this.f4582b;
        }

        public void mo1284a(C2453c c2453c, long j) throws IOException {
            if (this.f4583c) {
                throw new IllegalStateException("closed");
            }
            C0488c.m516a(c2453c.m7705a(), 0, j);
            if (j > this.f4584d) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("expected ");
                stringBuilder.append(this.f4584d);
                stringBuilder.append(" bytes but received ");
                stringBuilder.append(j);
                throw new ProtocolException(stringBuilder.toString());
            }
            this.f4581a.f4588d.mo1284a(c2453c, j);
            this.f4584d -= j;
        }

        public void flush() throws IOException {
            if (!this.f4583c) {
                this.f4581a.f4588d.flush();
            }
        }

        public void close() throws IOException {
            if (!this.f4583c) {
                this.f4583c = true;
                if (this.f4584d > 0) {
                    throw new ProtocolException("unexpected end of stream");
                }
                this.f4581a.m5098a(this.f4582b);
                this.f4581a.f4589e = 3;
            }
        }
    }

    /* compiled from: Http1Codec */
    private class C2360c extends C1840a {
        final /* synthetic */ C1843a f5832e;
        private final C0557t f5833f;
        private long f5834g = -1;
        private boolean f5835h = true;

        C2360c(C1843a c1843a, C0557t c0557t) {
            this.f5832e = c1843a;
            super();
            this.f5833f = c0557t;
        }

        public long read(C2453c c2453c, long j) throws IOException {
            if (j < 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("byteCount < 0: ");
                stringBuilder.append(j);
                throw new IllegalArgumentException(stringBuilder.toString());
            } else if (this.b) {
                throw new IllegalStateException("closed");
            } else if (!this.f5835h) {
                return -1;
            } else {
                if (this.f5834g == 0 || this.f5834g == -1) {
                    m7012a();
                    if (!this.f5835h) {
                        return -1;
                    }
                }
                c2453c = super.read(c2453c, Math.min(j, this.f5834g));
                if (c2453c == -1) {
                    c2453c = new ProtocolException("unexpected end of stream");
                    m5087a(0, c2453c);
                    throw c2453c;
                }
                this.f5834g -= c2453c;
                return c2453c;
            }
        }

        private void m7012a() throws IOException {
            if (this.f5834g != -1) {
                this.f5832e.f4587c.mo2544t();
            }
            try {
                this.f5834g = this.f5832e.f4587c.mo2543q();
                String trim = this.f5832e.f4587c.mo2544t().trim();
                if (this.f5834g >= 0) {
                    if (trim.isEmpty() || trim.startsWith(";")) {
                        if (this.f5834g == 0) {
                            this.f5835h = false;
                            C0483e.m489a(this.f5832e.f4585a.m5206f(), this.f5833f, this.f5832e.m5102d());
                            m5087a(true, null);
                            return;
                        }
                        return;
                    }
                }
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("expected chunk size and optional extensions but was \"");
                stringBuilder.append(this.f5834g);
                stringBuilder.append(trim);
                stringBuilder.append("\"");
                throw new ProtocolException(stringBuilder.toString());
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }

        public void close() throws IOException {
            if (!this.b) {
                if (this.f5835h && !C0488c.m519a((C1630t) this, 100, TimeUnit.MILLISECONDS)) {
                    m5087a(false, null);
                }
                this.b = true;
            }
        }
    }

    /* compiled from: Http1Codec */
    private class C2361e extends C1840a {
        final /* synthetic */ C1843a f5836e;
        private long f5837f;

        C2361e(C1843a c1843a, long j) throws IOException {
            this.f5836e = c1843a;
            super();
            this.f5837f = j;
            if (this.f5837f == 0) {
                m5087a(true, null);
            }
        }

        public long read(C2453c c2453c, long j) throws IOException {
            if (j < 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("byteCount < 0: ");
                stringBuilder.append(j);
                throw new IllegalArgumentException(stringBuilder.toString());
            } else if (this.b) {
                throw new IllegalStateException("closed");
            } else if (this.f5837f == 0) {
                return -1;
            } else {
                c2453c = super.read(c2453c, Math.min(this.f5837f, j));
                if (c2453c == -1) {
                    c2453c = new ProtocolException("unexpected end of stream");
                    m5087a(0, c2453c);
                    throw c2453c;
                }
                this.f5837f -= c2453c;
                if (this.f5837f == 0) {
                    m5087a(true, null);
                }
                return c2453c;
            }
        }

        public void close() throws IOException {
            if (!this.b) {
                if (!(this.f5837f == 0 || C0488c.m519a((C1630t) this, 100, TimeUnit.MILLISECONDS))) {
                    m5087a(false, null);
                }
                this.b = true;
            }
        }
    }

    /* compiled from: Http1Codec */
    private class C2362f extends C1840a {
        final /* synthetic */ C1843a f5838e;
        private boolean f5839f;

        C2362f(C1843a c1843a) {
            this.f5838e = c1843a;
            super();
        }

        public long read(C2453c c2453c, long j) throws IOException {
            if (j < 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("byteCount < 0: ");
                stringBuilder.append(j);
                throw new IllegalArgumentException(stringBuilder.toString());
            } else if (this.b) {
                throw new IllegalStateException("closed");
            } else if (this.f5839f) {
                return -1;
            } else {
                c2453c = super.read(c2453c, j);
                if (c2453c != -1) {
                    return c2453c;
                }
                this.f5839f = true;
                m5087a(true, 0);
                return -1;
            }
        }

        public void close() throws IOException {
            if (!this.b) {
                if (!this.f5839f) {
                    m5087a(false, null);
                }
                this.b = true;
            }
        }
    }

    public C1843a(C1883x c1883x, C0476g c0476g, C2284e c2284e, C2283d c2283d) {
        this.f4585a = c1883x;
        this.f4586b = c0476g;
        this.f4587c = c2284e;
        this.f4588d = c2283d;
    }

    public C1629s mo1290a(aa aaVar, long j) {
        if ("chunked".equalsIgnoreCase(aaVar.m752a("Transfer-Encoding")) != null) {
            return m5103e();
        }
        if (j != -1) {
            return m5092a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    public void mo1294c() {
        C1834c b = this.f4586b.m467b();
        if (b != null) {
            b.m5060c();
        }
    }

    public void mo1292a(aa aaVar) throws IOException {
        m5097a(aaVar.m754c(), C0485i.m497a(aaVar, this.f4586b.m467b().mo1271a().m791b().type()));
    }

    public ad mo1289a(ac acVar) throws IOException {
        this.f4586b.f449c.m903f(this.f4586b.f448b);
        String a = acVar.m776a("Content-Type");
        if (!C0483e.m491b(acVar)) {
            return new C1838h(a, 0, C1625m.m4841a(m5099b(0)));
        }
        if ("chunked".equalsIgnoreCase(acVar.m776a("Transfer-Encoding"))) {
            return new C1838h(a, -1, C1625m.m4841a(m5094a(acVar.m775a().m751a())));
        }
        long a2 = C0483e.m486a(acVar);
        if (a2 != -1) {
            return new C1838h(a, a2, C1625m.m4841a(m5099b(a2)));
        }
        return new C1838h(a, -1, C1625m.m4841a(m5104f()));
    }

    public void mo1291a() throws IOException {
        this.f4588d.flush();
    }

    public void mo1293b() throws IOException {
        this.f4588d.flush();
    }

    public void m5097a(C0554s c0554s, String str) throws IOException {
        if (this.f4589e != 0) {
            str = new StringBuilder();
            str.append("state: ");
            str.append(this.f4589e);
            throw new IllegalStateException(str.toString());
        }
        this.f4588d.mo2518b(str).mo2518b("\r\n");
        int a = c0554s.m920a();
        for (int i = null; i < a; i++) {
            this.f4588d.mo2518b(c0554s.m921a(i)).mo2518b(": ").mo2518b(c0554s.m924b(i)).mo2518b("\r\n");
        }
        this.f4588d.mo2518b("\r\n");
        this.f4589e = 1;
    }

    public C0523a mo1288a(boolean z) throws IOException {
        if (this.f4589e == 1 || this.f4589e == 3) {
            try {
                C0486k a = C0486k.m500a(this.f4587c.mo2544t());
                C0523a a2 = new C0523a().m768a(a.f468a).m761a(a.f469b).m769a(a.f470c).m767a(m5102d());
                if (z && a.f469b) {
                    return false;
                }
                this.f4589e = true;
                return a2;
            } catch (boolean z2) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("unexpected end of stream on ");
                stringBuilder.append(this.f4586b);
                IOException iOException = new IOException(stringBuilder.toString());
                iOException.initCause(z2);
                throw iOException;
            }
        }
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("state: ");
        stringBuilder2.append(this.f4589e);
        throw new IllegalStateException(stringBuilder2.toString());
    }

    public C0554s m5102d() throws IOException {
        C0553a c0553a = new C0553a();
        while (true) {
            String t = this.f4587c.mo2544t();
            if (t.length() == 0) {
                return c0553a.m914a();
            }
            C0469a.f427a.mo1340a(c0553a, t);
        }
    }

    public C1629s m5103e() {
        if (this.f4589e != 1) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("state: ");
            stringBuilder.append(this.f4589e);
            throw new IllegalStateException(stringBuilder.toString());
        }
        this.f4589e = 2;
        return new C1841b(this);
    }

    public C1629s m5092a(long j) {
        if (this.f4589e != 1) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("state: ");
            stringBuilder.append(this.f4589e);
            throw new IllegalStateException(stringBuilder.toString());
        }
        this.f4589e = 2;
        return new C1842d(this, j);
    }

    public C1630t m5099b(long j) throws IOException {
        if (this.f4589e != 4) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("state: ");
            stringBuilder.append(this.f4589e);
            throw new IllegalStateException(stringBuilder.toString());
        }
        this.f4589e = 5;
        return new C2361e(this, j);
    }

    public C1630t m5094a(C0557t c0557t) throws IOException {
        if (this.f4589e != 4) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("state: ");
            stringBuilder.append(this.f4589e);
            throw new IllegalStateException(stringBuilder.toString());
        }
        this.f4589e = 5;
        return new C2360c(this, c0557t);
    }

    public C1630t m5104f() throws IOException {
        if (this.f4589e != 4) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("state: ");
            stringBuilder.append(this.f4589e);
            throw new IllegalStateException(stringBuilder.toString());
        } else if (this.f4586b == null) {
            throw new IllegalStateException("streamAllocation == null");
        } else {
            this.f4589e = 5;
            this.f4586b.m469d();
            return new C2362f(this);
        }
    }

    void m5098a(C2288j c2288j) {
        C1631u a = c2288j.m6947a();
        c2288j.m6946a(C1631u.f4415c);
        a.mo1895f();
        a.k_();
    }
}
