package p021c.p022a;

/* compiled from: NamedRunnable */
public abstract class C0477b implements Runnable {
    protected final String f461b;

    protected abstract void mo1295c();

    public C0477b(String str, Object... objArr) {
        this.f461b = C0488c.m510a(str, objArr);
    }

    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.f461b);
        try {
            mo1295c();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
