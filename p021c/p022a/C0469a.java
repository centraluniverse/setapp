package p021c.p022a;

import java.net.Socket;
import javax.net.ssl.SSLSocket;
import p021c.C0521a;
import p021c.C0541j;
import p021c.C0543k;
import p021c.C0554s.C0553a;
import p021c.ac.C0523a;
import p021c.ae;
import p021c.p022a.p024b.C0471d;
import p021c.p022a.p024b.C0476g;
import p021c.p022a.p024b.C1834c;

/* compiled from: Internal */
public abstract class C0469a {
    public static C0469a f427a;

    public abstract int mo1335a(C0523a c0523a);

    public abstract C1834c mo1336a(C0541j c0541j, C0521a c0521a, C0476g c0476g, ae aeVar);

    public abstract C0471d mo1337a(C0541j c0541j);

    public abstract Socket mo1338a(C0541j c0541j, C0521a c0521a, C0476g c0476g);

    public abstract void mo1339a(C0543k c0543k, SSLSocket sSLSocket, boolean z);

    public abstract void mo1340a(C0553a c0553a, String str);

    public abstract void mo1341a(C0553a c0553a, String str, String str2);

    public abstract boolean mo1342a(C0521a c0521a, C0521a c0521a2);

    public abstract boolean mo1343a(C0541j c0541j, C1834c c1834c);

    public abstract void mo1344b(C0541j c0541j, C1834c c1834c);
}
