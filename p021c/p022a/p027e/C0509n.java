package p021c.p022a.p027e;

import android.support.v4.internal.view.SupportMenu;
import java.util.Arrays;

/* compiled from: Settings */
public final class C0509n {
    private int f596a;
    private final int[] f597b = new int[10];

    void m670a() {
        this.f596a = 0;
        Arrays.fill(this.f597b, 0);
    }

    C0509n m669a(int i, int i2) {
        if (i >= 0) {
            if (i < this.f597b.length) {
                this.f596a = (1 << i) | this.f596a;
                this.f597b[i] = i2;
                return this;
            }
        }
        return this;
    }

    boolean m672a(int i) {
        return ((1 << i) & this.f596a) != 0;
    }

    int m674b(int i) {
        return this.f597b[i];
    }

    int m673b() {
        return Integer.bitCount(this.f596a);
    }

    int m675c() {
        return (this.f596a & 2) != 0 ? this.f597b[1] : -1;
    }

    int m676c(int i) {
        return (this.f596a & 16) != 0 ? this.f597b[4] : i;
    }

    int m678d(int i) {
        return (this.f596a & 32) != 0 ? this.f597b[5] : i;
    }

    int m677d() {
        return (this.f596a & 128) != 0 ? this.f597b[7] : SupportMenu.USER_MASK;
    }

    void m671a(C0509n c0509n) {
        for (int i = 0; i < 10; i++) {
            if (c0509n.m672a(i)) {
                m669a(i, c0509n.m674b(i));
            }
        }
    }
}
