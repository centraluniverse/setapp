package p021c.p022a.p027e;

import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import p021c.C0554s;
import p021c.C0554s.C0553a;
import p021c.C0559u.C0558a;
import p021c.C0564y;
import p021c.C1883x;
import p021c.aa;
import p021c.ac;
import p021c.ac.C0523a;
import p021c.ad;
import p021c.p022a.C0469a;
import p021c.p022a.C0488c;
import p021c.p022a.p024b.C0476g;
import p021c.p022a.p025c.C0480c;
import p021c.p022a.p025c.C0483e;
import p021c.p022a.p025c.C0485i;
import p021c.p022a.p025c.C0486k;
import p021c.p022a.p025c.C1838h;
import p125d.C1624f;
import p125d.C1625m;
import p125d.C1629s;
import p125d.C1630t;
import p125d.C2287i;
import p125d.C2453c;

/* compiled from: Http2Codec */
public final class C1844f implements C0480c {
    private static final C1624f f4590b = C1624f.m4820a("connection");
    private static final C1624f f4591c = C1624f.m4820a("host");
    private static final C1624f f4592d = C1624f.m4820a("keep-alive");
    private static final C1624f f4593e = C1624f.m4820a("proxy-connection");
    private static final C1624f f4594f = C1624f.m4820a("transfer-encoding");
    private static final C1624f f4595g = C1624f.m4820a("te");
    private static final C1624f f4596h = C1624f.m4820a("encoding");
    private static final C1624f f4597i = C1624f.m4820a("upgrade");
    private static final List<C1624f> f4598j = C0488c.m514a(f4590b, f4591c, f4592d, f4593e, f4595g, f4594f, f4596h, f4597i, C0493c.f499c, C0493c.f500d, C0493c.f501e, C0493c.f502f);
    private static final List<C1624f> f4599k = C0488c.m514a(f4590b, f4591c, f4592d, f4593e, f4595g, f4594f, f4596h, f4597i);
    final C0476g f4600a;
    private final C1883x f4601l;
    private final C0558a f4602m;
    private final C0500g f4603n;
    private C0503i f4604o;

    /* compiled from: Http2Codec */
    class C2363a extends C2287i {
        boolean f5840a = null;
        long f5841b = null;
        final /* synthetic */ C1844f f5842c;

        C2363a(C1844f c1844f, C1630t c1630t) {
            this.f5842c = c1844f;
            super(c1630t);
        }

        public long read(C2453c c2453c, long j) throws IOException {
            try {
                c2453c = delegate().read(c2453c, j);
                if (c2453c > 0) {
                    this.f5841b += c2453c;
                }
                return c2453c;
            } catch (C2453c c2453c2) {
                m7013a(c2453c2);
                throw c2453c2;
            }
        }

        public void close() throws IOException {
            super.close();
            m7013a(null);
        }

        private void m7013a(IOException iOException) {
            if (!this.f5840a) {
                this.f5840a = true;
                this.f5842c.f4600a.m466a(false, this.f5842c, this.f5841b, iOException);
            }
        }
    }

    public C1844f(C1883x c1883x, C0558a c0558a, C0476g c0476g, C0500g c0500g) {
        this.f4601l = c1883x;
        this.f4602m = c0558a;
        this.f4600a = c0476g;
        this.f4603n = c0500g;
    }

    public C1629s mo1290a(aa aaVar, long j) {
        return this.f4604o.m634h();
    }

    public void mo1292a(aa aaVar) throws IOException {
        if (this.f4604o == null) {
            this.f4604o = this.f4603n.m574a(C1844f.m5106b(aaVar), aaVar.m755d() != null);
            this.f4604o.m631e().mo1893a((long) this.f4602m.mo1279d(), TimeUnit.MILLISECONDS);
            this.f4604o.m632f().mo1893a((long) this.f4602m.mo1280e(), TimeUnit.MILLISECONDS);
        }
    }

    public void mo1291a() throws IOException {
        this.f4603n.m587b();
    }

    public void mo1293b() throws IOException {
        this.f4604o.m634h().close();
    }

    public C0523a mo1288a(boolean z) throws IOException {
        C0523a a = C1844f.m5105a(this.f4604o.m630d());
        return (z && C0469a.f427a.mo1335a(a)) ? false : a;
    }

    public static List<C0493c> m5106b(aa aaVar) {
        C0554s c = aaVar.m754c();
        List<C0493c> arrayList = new ArrayList(c.m920a() + 4);
        arrayList.add(new C0493c(C0493c.f499c, aaVar.m753b()));
        arrayList.add(new C0493c(C0493c.f500d, C0485i.m498a(aaVar.m751a())));
        String a = aaVar.m752a("Host");
        if (a != null) {
            arrayList.add(new C0493c(C0493c.f502f, a));
        }
        arrayList.add(new C0493c(C0493c.f501e, aaVar.m751a().m963b()));
        int a2 = c.m920a();
        for (int i = null; i < a2; i++) {
            C1624f a3 = C1624f.m4820a(c.m921a(i).toLowerCase(Locale.US));
            if (!f4598j.contains(a3)) {
                arrayList.add(new C0493c(a3, c.m924b(i)));
            }
        }
        return arrayList;
    }

    public static C0523a m5105a(List<C0493c> list) throws IOException {
        C0553a c0553a = new C0553a();
        int size = list.size();
        C0553a c0553a2 = c0553a;
        C0486k c0486k = null;
        for (int i = 0; i < size; i++) {
            C0493c c0493c = (C0493c) list.get(i);
            if (c0493c != null) {
                C1624f c1624f = c0493c.f503g;
                String a = c0493c.f504h.mo1902a();
                if (c1624f.equals(C0493c.f498b)) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("HTTP/1.1 ");
                    stringBuilder.append(a);
                    c0486k = C0486k.m500a(stringBuilder.toString());
                } else if (!f4599k.contains(c1624f)) {
                    C0469a.f427a.mo1341a(c0553a2, c1624f.mo1902a(), a);
                }
            } else if (c0486k != null && c0486k.f469b == 100) {
                c0553a2 = new C0553a();
                c0486k = null;
            }
        }
        if (c0486k != null) {
            return new C0523a().m768a(C0564y.HTTP_2).m761a(c0486k.f469b).m769a(c0486k.f470c).m767a(c0553a2.m914a());
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    public ad mo1289a(ac acVar) throws IOException {
        this.f4600a.f449c.m903f(this.f4600a.f448b);
        return new C1838h(acVar.m776a("Content-Type"), C0483e.m486a(acVar), C1625m.m4841a(new C2363a(this, this.f4604o.m633g())));
    }

    public void mo1294c() {
        if (this.f4604o != null) {
            this.f4604o.m626b(C0492b.CANCEL);
        }
    }
}
