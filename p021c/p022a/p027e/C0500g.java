package p021c.p022a.p027e;

import android.support.v4.internal.view.SupportMenu;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import p021c.p022a.C0477b;
import p021c.p022a.C0488c;
import p021c.p022a.p027e.C0502h.C0501b;
import p125d.C1624f;
import p125d.C2283d;
import p125d.C2284e;
import p125d.C2453c;

/* compiled from: Http2Connection */
public final class C0500g implements Closeable {
    static final ExecutorService f538a = new ThreadPoolExecutor(0, ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, 60, TimeUnit.SECONDS, new SynchronousQueue(), C0488c.m515a("OkHttp Http2Connection", (boolean) f539s));
    static final /* synthetic */ boolean f539s = true;
    final boolean f540b;
    final C0499b f541c;
    final Map<Integer, C0503i> f542d = new LinkedHashMap();
    final String f543e;
    int f544f;
    int f545g;
    boolean f546h;
    final C0508m f547i;
    long f548j = 0;
    long f549k;
    C0509n f550l = new C0509n();
    final C0509n f551m = new C0509n();
    boolean f552n = false;
    final Socket f553o;
    final C0504j f554p;
    final C1856c f555q;
    final Set<Integer> f556r = new LinkedHashSet();
    private final ExecutorService f557t;
    private Map<Integer, C0507l> f558u;
    private int f559v;

    /* compiled from: Http2Connection */
    public static class C0498a {
        Socket f530a;
        String f531b;
        C2284e f532c;
        C2283d f533d;
        C0499b f534e = C0499b.f537f;
        C0508m f535f = C0508m.f595a;
        boolean f536g;

        public C0498a(boolean z) {
            this.f536g = z;
        }

        public C0498a m567a(Socket socket, String str, C2284e c2284e, C2283d c2283d) {
            this.f530a = socket;
            this.f531b = str;
            this.f532c = c2284e;
            this.f533d = c2283d;
            return this;
        }

        public C0498a m566a(C0499b c0499b) {
            this.f534e = c0499b;
            return this;
        }

        public C0500g m568a() {
            return new C0500g(this);
        }
    }

    /* compiled from: Http2Connection */
    public static abstract class C0499b {
        public static final C0499b f537f = new C18521();

        /* compiled from: Http2Connection */
        class C18521 extends C0499b {
            C18521() {
            }

            public void mo1273a(C0503i c0503i) throws IOException {
                c0503i.m623a(C0492b.REFUSED_STREAM);
            }
        }

        public void mo1272a(C0500g c0500g) {
        }

        public abstract void mo1273a(C0503i c0503i) throws IOException;
    }

    /* compiled from: Http2Connection */
    class C1856c extends C0477b implements C0501b {
        final C0502h f4636a;
        final /* synthetic */ C0500g f4637c;

        public void mo1296a() {
        }

        public void mo1297a(int i, int i2, int i3, boolean z) {
        }

        C1856c(C0500g c0500g, C0502h c0502h) {
            this.f4637c = c0500g;
            super("OkHttp %s", c0500g.f543e);
            this.f4636a = c0502h;
        }

        protected void mo1295c() {
            /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
            /*
            r5 = this;
            r0 = p021c.p022a.p027e.C0492b.INTERNAL_ERROR;
            r1 = p021c.p022a.p027e.C0492b.INTERNAL_ERROR;
            r2 = r5.f4636a;	 Catch:{ IOException -> 0x001e }
            r2.m618a(r5);	 Catch:{ IOException -> 0x001e }
        L_0x0009:
            r2 = r5.f4636a;	 Catch:{ IOException -> 0x001e }
            r3 = 0;	 Catch:{ IOException -> 0x001e }
            r2 = r2.m619a(r3, r5);	 Catch:{ IOException -> 0x001e }
            if (r2 == 0) goto L_0x0013;	 Catch:{ IOException -> 0x001e }
        L_0x0012:
            goto L_0x0009;	 Catch:{ IOException -> 0x001e }
        L_0x0013:
            r2 = p021c.p022a.p027e.C0492b.NO_ERROR;	 Catch:{ IOException -> 0x001e }
            r0 = p021c.p022a.p027e.C0492b.CANCEL;	 Catch:{ IOException -> 0x001a }
            r1 = r5.f4637c;	 Catch:{ IOException -> 0x0027 }
            goto L_0x0024;
        L_0x001a:
            r0 = r2;
            goto L_0x001e;
        L_0x001c:
            r2 = move-exception;
            goto L_0x0031;
        L_0x001e:
            r2 = p021c.p022a.p027e.C0492b.PROTOCOL_ERROR;	 Catch:{ all -> 0x001c }
            r0 = p021c.p022a.p027e.C0492b.PROTOCOL_ERROR;	 Catch:{ all -> 0x002d }
            r1 = r5.f4637c;	 Catch:{ IOException -> 0x0027 }
        L_0x0024:
            r1.m583a(r2, r0);	 Catch:{ IOException -> 0x0027 }
        L_0x0027:
            r0 = r5.f4636a;
            p021c.p022a.C0488c.m517a(r0);
            return;
        L_0x002d:
            r0 = move-exception;
            r4 = r2;
            r2 = r0;
            r0 = r4;
        L_0x0031:
            r3 = r5.f4637c;	 Catch:{ IOException -> 0x0036 }
            r3.m583a(r0, r1);	 Catch:{ IOException -> 0x0036 }
        L_0x0036:
            r0 = r5.f4636a;
            p021c.p022a.C0488c.m517a(r0);
            throw r2;
            */
            throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.c.c():void");
        }

        public void mo1304a(boolean z, int i, C2284e c2284e, int i2) throws IOException {
            if (this.f4637c.m594d(i)) {
                this.f4637c.m577a(i, c2284e, i2, z);
                return;
            }
            C0503i a = this.f4637c.m573a(i);
            if (a == null) {
                this.f4637c.m576a(i, C0492b.PROTOCOL_ERROR);
                c2284e.mo2531i((long) i2);
                return;
            }
            a.m624a(c2284e, i2);
            if (z) {
                a.m635i();
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo1303a(boolean r10, int r11, int r12, java.util.List<p021c.p022a.p027e.C0493c> r13) {
            /*
            r9 = this;
            r12 = r9.f4637c;
            r12 = r12.m594d(r11);
            if (r12 == 0) goto L_0x000e;
        L_0x0008:
            r12 = r9.f4637c;
            r12.m579a(r11, r13, r10);
            return;
        L_0x000e:
            r12 = r9.f4637c;
            monitor-enter(r12);
            r0 = r9.f4637c;	 Catch:{ all -> 0x0078 }
            r0 = r0.m573a(r11);	 Catch:{ all -> 0x0078 }
            if (r0 != 0) goto L_0x006e;
        L_0x0019:
            r0 = r9.f4637c;	 Catch:{ all -> 0x0078 }
            r0 = r0.f546h;	 Catch:{ all -> 0x0078 }
            if (r0 == 0) goto L_0x0021;
        L_0x001f:
            monitor-exit(r12);	 Catch:{ all -> 0x0078 }
            return;
        L_0x0021:
            r0 = r9.f4637c;	 Catch:{ all -> 0x0078 }
            r0 = r0.f544f;	 Catch:{ all -> 0x0078 }
            if (r11 > r0) goto L_0x0029;
        L_0x0027:
            monitor-exit(r12);	 Catch:{ all -> 0x0078 }
            return;
        L_0x0029:
            r0 = r11 % 2;
            r1 = r9.f4637c;	 Catch:{ all -> 0x0078 }
            r1 = r1.f545g;	 Catch:{ all -> 0x0078 }
            r2 = 2;
            r1 = r1 % r2;
            if (r0 != r1) goto L_0x0035;
        L_0x0033:
            monitor-exit(r12);	 Catch:{ all -> 0x0078 }
            return;
        L_0x0035:
            r0 = new c.a.e.i;	 Catch:{ all -> 0x0078 }
            r5 = r9.f4637c;	 Catch:{ all -> 0x0078 }
            r6 = 0;
            r3 = r0;
            r4 = r11;
            r7 = r10;
            r8 = r13;
            r3.<init>(r4, r5, r6, r7, r8);	 Catch:{ all -> 0x0078 }
            r10 = r9.f4637c;	 Catch:{ all -> 0x0078 }
            r10.f544f = r11;	 Catch:{ all -> 0x0078 }
            r10 = r9.f4637c;	 Catch:{ all -> 0x0078 }
            r10 = r10.f542d;	 Catch:{ all -> 0x0078 }
            r13 = java.lang.Integer.valueOf(r11);	 Catch:{ all -> 0x0078 }
            r10.put(r13, r0);	 Catch:{ all -> 0x0078 }
            r10 = p021c.p022a.p027e.C0500g.f538a;	 Catch:{ all -> 0x0078 }
            r13 = new c.a.e.g$c$1;	 Catch:{ all -> 0x0078 }
            r1 = "OkHttp %s stream %d";
            r2 = new java.lang.Object[r2];	 Catch:{ all -> 0x0078 }
            r3 = 0;
            r4 = r9.f4637c;	 Catch:{ all -> 0x0078 }
            r4 = r4.f543e;	 Catch:{ all -> 0x0078 }
            r2[r3] = r4;	 Catch:{ all -> 0x0078 }
            r3 = 1;
            r11 = java.lang.Integer.valueOf(r11);	 Catch:{ all -> 0x0078 }
            r2[r3] = r11;	 Catch:{ all -> 0x0078 }
            r13.<init>(r9, r1, r2, r0);	 Catch:{ all -> 0x0078 }
            r10.execute(r13);	 Catch:{ all -> 0x0078 }
            monitor-exit(r12);	 Catch:{ all -> 0x0078 }
            return;
        L_0x006e:
            monitor-exit(r12);	 Catch:{ all -> 0x0078 }
            r0.m625a(r13);
            if (r10 == 0) goto L_0x0077;
        L_0x0074:
            r0.m635i();
        L_0x0077:
            return;
        L_0x0078:
            r10 = move-exception;
            monitor-exit(r12);	 Catch:{ all -> 0x0078 }
            throw r10;
            */
            throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.c.a(boolean, int, int, java.util.List):void");
        }

        public void mo1300a(int i, C0492b c0492b) {
            if (this.f4637c.m594d(i)) {
                this.f4637c.m592c(i, c0492b);
                return;
            }
            i = this.f4637c.m586b(i);
            if (i != 0) {
                i.m628c(c0492b);
            }
        }

        public void mo1305a(boolean z, C0509n c0509n) {
            synchronized (this.f4637c) {
                long j;
                boolean d = this.f4637c.f551m.m677d();
                if (z) {
                    this.f4637c.f551m.m670a();
                }
                this.f4637c.f551m.m671a(c0509n);
                m5125a(c0509n);
                z = this.f4637c.f551m.m677d();
                C0503i[] c0503iArr = null;
                if (z || z == d) {
                    j = false;
                } else {
                    j = (long) (z - d);
                    if (!this.f4637c.f552n) {
                        this.f4637c.m581a(j);
                        this.f4637c.f552n = C0500g.f539s;
                    }
                    if (!this.f4637c.f542d.isEmpty()) {
                        c0503iArr = (C0503i[]) this.f4637c.f542d.values().toArray(new C0503i[this.f4637c.f542d.size()]);
                    }
                }
                ExecutorService executorService = C0500g.f538a;
                Object[] objArr = new Object[1];
                int i = 0;
                objArr[0] = this.f4637c.f543e;
                executorService.execute(new C0477b(this, "OkHttp %s settings", objArr) {
                    final /* synthetic */ C1856c f4633a;

                    public void mo1295c() {
                        this.f4633a.f4637c.f541c.mo1272a(this.f4633a.f4637c);
                    }
                });
            }
            if (c0503iArr != null && j != false) {
                int length = c0503iArr.length;
                while (i < length) {
                    C0503i c0503i = c0503iArr[i];
                    synchronized (c0503i) {
                        c0503i.m622a(j);
                    }
                    i++;
                }
            }
        }

        private void m5125a(final C0509n c0509n) {
            C0500g.f538a.execute(new C0477b(this, "OkHttp %s ACK Settings", new Object[]{this.f4637c.f543e}) {
                final /* synthetic */ C1856c f4635c;

                public void mo1295c() {
                    /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                    /*
                    r2 = this;
                    r0 = r2.f4635c;	 Catch:{ IOException -> 0x000b }
                    r0 = r0.f4637c;	 Catch:{ IOException -> 0x000b }
                    r0 = r0.f554p;	 Catch:{ IOException -> 0x000b }
                    r1 = r7;	 Catch:{ IOException -> 0x000b }
                    r0.m648a(r1);	 Catch:{ IOException -> 0x000b }
                L_0x000b:
                    return;
                    */
                    throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.c.3.c():void");
                }
            });
        }

        public void mo1302a(boolean z, int i, int i2) {
            if (z) {
                z = this.f4637c.m590c(i);
                if (z) {
                    z.m663b();
                    return;
                }
                return;
            }
            this.f4637c.m585a((boolean) C0500g.f539s, i, i2, null);
        }

        public void mo1301a(int i, C0492b c0492b, C1624f c1624f) {
            c1624f.mo1912g();
            synchronized (this.f4637c) {
                C0503i[] c0503iArr = (C0503i[]) this.f4637c.f542d.values().toArray(new C0503i[this.f4637c.f542d.size()]);
                this.f4637c.f546h = C0500g.f539s;
            }
            for (C0503i c0503i : c0503iArr) {
                if (c0503i.m621a() > i && c0503i.m629c()) {
                    c0503i.m628c(C0492b.REFUSED_STREAM);
                    this.f4637c.m586b(c0503i.m621a());
                }
            }
        }

        public void mo1299a(int i, long j) {
            if (i == 0) {
                synchronized (this.f4637c) {
                    i = this.f4637c;
                    i.f549k += j;
                    this.f4637c.notifyAll();
                }
                return;
            }
            i = this.f4637c.m573a(i);
            if (i != 0) {
                synchronized (i) {
                    i.m622a(j);
                }
            }
        }

        public void mo1298a(int i, int i2, List<C0493c> list) {
            this.f4637c.m578a(i2, (List) list);
        }
    }

    boolean m594d(int i) {
        return (i == 0 || (i & 1) != 0) ? false : f539s;
    }

    C0500g(C0498a c0498a) {
        this.f547i = c0498a.f535f;
        this.f540b = c0498a.f536g;
        this.f541c = c0498a.f534e;
        int i = 2;
        this.f545g = c0498a.f536g ? 1 : 2;
        if (c0498a.f536g) {
            this.f545g += 2;
        }
        if (c0498a.f536g) {
            i = 1;
        }
        this.f559v = i;
        if (c0498a.f536g) {
            this.f550l.m669a(7, 16777216);
        }
        this.f543e = c0498a.f531b;
        this.f557t = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), C0488c.m515a(C0488c.m510a("OkHttp %s Push Observer", this.f543e), (boolean) f539s));
        this.f551m.m669a(7, SupportMenu.USER_MASK);
        this.f551m.m669a(5, 16384);
        this.f549k = (long) this.f551m.m677d();
        this.f553o = c0498a.f530a;
        this.f554p = new C0504j(c0498a.f533d, this.f540b);
        this.f555q = new C1856c(this, new C0502h(c0498a.f532c, this.f540b));
    }

    synchronized C0503i m573a(int i) {
        return (C0503i) this.f542d.get(Integer.valueOf(i));
    }

    synchronized C0503i m586b(int i) {
        C0503i c0503i;
        c0503i = (C0503i) this.f542d.remove(Integer.valueOf(i));
        notifyAll();
        return c0503i;
    }

    public synchronized int m572a() {
        return this.f551m.m676c(ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
    }

    public C0503i m574a(List<C0493c> list, boolean z) throws IOException {
        return m571b(0, list, z);
    }

    private C0503i m571b(int i, List<C0493c> list, boolean z) throws IOException {
        C0503i c0503i;
        boolean z2 = z ^ 1;
        synchronized (this.f554p) {
            synchronized (this) {
                if (this.f546h) {
                    throw new C0491a();
                }
                int i2 = this.f545g;
                this.f545g += 2;
                c0503i = new C0503i(i2, this, z2, false, list);
                if (z && this.f549k != 0) {
                    if (c0503i.f567b != 0) {
                        z = false;
                        if (c0503i.m627b()) {
                            this.f542d.put(Integer.valueOf(i2), c0503i);
                        }
                    }
                }
                z = f539s;
                if (c0503i.m627b()) {
                    this.f542d.put(Integer.valueOf(i2), c0503i);
                }
            }
            if (i == 0) {
                this.f554p.m650a(z2, i2, i, (List) list);
            } else if (this.f540b) {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            } else {
                this.f554p.m644a(i, i2, (List) list);
            }
        }
        if (z) {
            this.f554p.m653b();
        }
        return c0503i;
    }

    public void m580a(int r11, boolean r12, p125d.C2453c r13, long r14) throws java.io.IOException {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r10 = this;
        r0 = 0;
        r2 = (r14 > r0 ? 1 : (r14 == r0 ? 0 : -1));
        r3 = 0;
        if (r2 != 0) goto L_0x000d;
    L_0x0007:
        r14 = r10.f554p;
        r14.m651a(r12, r11, r13, r3);
        return;
    L_0x000d:
        r2 = (r14 > r0 ? 1 : (r14 == r0 ? 0 : -1));
        if (r2 <= 0) goto L_0x0065;
    L_0x0011:
        monitor-enter(r10);
    L_0x0012:
        r4 = r10.f549k;	 Catch:{ InterruptedException -> 0x005d }
        r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1));	 Catch:{ InterruptedException -> 0x005d }
        if (r2 > 0) goto L_0x0030;	 Catch:{ InterruptedException -> 0x005d }
    L_0x0018:
        r2 = r10.f542d;	 Catch:{ InterruptedException -> 0x005d }
        r4 = java.lang.Integer.valueOf(r11);	 Catch:{ InterruptedException -> 0x005d }
        r2 = r2.containsKey(r4);	 Catch:{ InterruptedException -> 0x005d }
        if (r2 != 0) goto L_0x002c;	 Catch:{ InterruptedException -> 0x005d }
    L_0x0024:
        r11 = new java.io.IOException;	 Catch:{ InterruptedException -> 0x005d }
        r12 = "stream closed";	 Catch:{ InterruptedException -> 0x005d }
        r11.<init>(r12);	 Catch:{ InterruptedException -> 0x005d }
        throw r11;	 Catch:{ InterruptedException -> 0x005d }
    L_0x002c:
        r10.wait();	 Catch:{ InterruptedException -> 0x005d }
        goto L_0x0012;
    L_0x0030:
        r4 = r10.f549k;	 Catch:{ all -> 0x005b }
        r4 = java.lang.Math.min(r14, r4);	 Catch:{ all -> 0x005b }
        r2 = (int) r4;	 Catch:{ all -> 0x005b }
        r4 = r10.f554p;	 Catch:{ all -> 0x005b }
        r4 = r4.m655c();	 Catch:{ all -> 0x005b }
        r2 = java.lang.Math.min(r2, r4);	 Catch:{ all -> 0x005b }
        r4 = r10.f549k;	 Catch:{ all -> 0x005b }
        r6 = (long) r2;	 Catch:{ all -> 0x005b }
        r8 = r4 - r6;	 Catch:{ all -> 0x005b }
        r10.f549k = r8;	 Catch:{ all -> 0x005b }
        monitor-exit(r10);	 Catch:{ all -> 0x005b }
        r4 = r14 - r6;
        r14 = r10.f554p;
        if (r12 == 0) goto L_0x0055;
    L_0x004f:
        r15 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1));
        if (r15 != 0) goto L_0x0055;
    L_0x0053:
        r15 = 1;
        goto L_0x0056;
    L_0x0055:
        r15 = r3;
    L_0x0056:
        r14.m651a(r15, r11, r13, r2);
        r14 = r4;
        goto L_0x000d;
    L_0x005b:
        r11 = move-exception;
        goto L_0x0063;
    L_0x005d:
        r11 = new java.io.InterruptedIOException;	 Catch:{ all -> 0x005b }
        r11.<init>();	 Catch:{ all -> 0x005b }
        throw r11;	 Catch:{ all -> 0x005b }
    L_0x0063:
        monitor-exit(r10);	 Catch:{ all -> 0x005b }
        throw r11;
    L_0x0065:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.a(int, boolean, d.c, long):void");
    }

    void m581a(long j) {
        this.f549k += j;
        if (j > 0) {
            notifyAll();
        }
    }

    void m576a(int i, C0492b c0492b) {
        final int i2 = i;
        final C0492b c0492b2 = c0492b;
        f538a.execute(new C0477b(this, "OkHttp %s stream %d", new Object[]{this.f543e, Integer.valueOf(i)}) {
            final /* synthetic */ C0500g f4607d;

            public void mo1295c() {
                /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                /*
                r3 = this;
                r0 = r3.f4607d;	 Catch:{ IOException -> 0x0009 }
                r1 = r5;	 Catch:{ IOException -> 0x0009 }
                r2 = r6;	 Catch:{ IOException -> 0x0009 }
                r0.m588b(r1, r2);	 Catch:{ IOException -> 0x0009 }
            L_0x0009:
                return;
                */
                throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.1.c():void");
            }
        });
    }

    void m588b(int i, C0492b c0492b) throws IOException {
        this.f554p.m646a(i, c0492b);
    }

    void m575a(int i, long j) {
        final int i2 = i;
        final long j2 = j;
        f538a.execute(new C0477b(this, "OkHttp Window Update %s stream %d", new Object[]{this.f543e, Integer.valueOf(i)}) {
            final /* synthetic */ C0500g f4610d;

            public void mo1295c() {
                /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                /*
                r4 = this;
                r0 = r4.f4610d;	 Catch:{ IOException -> 0x000b }
                r0 = r0.f554p;	 Catch:{ IOException -> 0x000b }
                r1 = r5;	 Catch:{ IOException -> 0x000b }
                r2 = r6;	 Catch:{ IOException -> 0x000b }
                r0.m645a(r1, r2);	 Catch:{ IOException -> 0x000b }
            L_0x000b:
                return;
                */
                throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.2.c():void");
            }
        });
    }

    void m585a(boolean z, int i, int i2, C0507l c0507l) {
        final boolean z2 = z;
        final int i3 = i;
        final int i4 = i2;
        final C0507l c0507l2 = c0507l;
        f538a.execute(new C0477b(this, "OkHttp %s ping %08x%08x", new Object[]{this.f543e, Integer.valueOf(i), Integer.valueOf(i2)}) {
            final /* synthetic */ C0500g f4615f;

            public void mo1295c() {
                /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                /*
                r5 = this;
                r0 = r5.f4615f;	 Catch:{ IOException -> 0x000d }
                r1 = r5;	 Catch:{ IOException -> 0x000d }
                r2 = r6;	 Catch:{ IOException -> 0x000d }
                r3 = r7;	 Catch:{ IOException -> 0x000d }
                r4 = r8;	 Catch:{ IOException -> 0x000d }
                r0.m589b(r1, r2, r3, r4);	 Catch:{ IOException -> 0x000d }
            L_0x000d:
                return;
                */
                throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.3.c():void");
            }
        });
    }

    void m589b(boolean z, int i, int i2, C0507l c0507l) throws IOException {
        synchronized (this.f554p) {
            if (c0507l != null) {
                c0507l.m662a();
            }
            this.f554p.m649a(z, i, i2);
        }
    }

    synchronized C0507l m590c(int i) {
        return this.f558u != null ? (C0507l) this.f558u.remove(Integer.valueOf(i)) : 0;
    }

    public void m587b() throws IOException {
        this.f554p.m653b();
    }

    public void m582a(C0492b c0492b) throws IOException {
        synchronized (this.f554p) {
            synchronized (this) {
                if (this.f546h) {
                    return;
                }
                this.f546h = f539s;
                int i = this.f544f;
                this.f554p.m647a(i, c0492b, C0488c.f471a);
            }
        }
    }

    public void close() throws IOException {
        m583a(C0492b.NO_ERROR, C0492b.CANCEL);
    }

    void m583a(C0492b c0492b, C0492b c0492b2) throws IOException {
        if (f539s || !Thread.holdsLock(this)) {
            C0503i[] c0503iArr;
            Map map = null;
            try {
                m582a(c0492b);
                c0492b = null;
            } catch (IOException e) {
                c0492b = e;
            }
            synchronized (this) {
                if (this.f542d.isEmpty()) {
                    c0503iArr = null;
                } else {
                    c0503iArr = (C0503i[]) this.f542d.values().toArray(new C0503i[this.f542d.size()]);
                    this.f542d.clear();
                }
                if (this.f558u != null) {
                    C0507l[] c0507lArr = (C0507l[]) this.f558u.values().toArray(new C0507l[this.f558u.size()]);
                    this.f558u = null;
                    map = c0507lArr;
                }
            }
            int i = 0;
            if (c0503iArr != null) {
                C0492b c0492b3 = c0492b;
                for (C0503i a : c0503iArr) {
                    try {
                        a.m623a(c0492b2);
                    } catch (IOException e2) {
                        if (c0492b3 != null) {
                            c0492b3 = e2;
                        }
                    }
                }
                c0492b = c0492b3;
            }
            if (map != null) {
                c0492b2 = map.length;
                while (i < c0492b2) {
                    map[i].m664c();
                    i++;
                }
            }
            try {
                this.f554p.close();
            } catch (C0492b c0492b22) {
                if (c0492b == null) {
                    c0492b = c0492b22;
                }
            }
            try {
                this.f553o.close();
            } catch (IOException e3) {
                c0492b = e3;
            }
            if (c0492b != null) {
                throw c0492b;
            }
            return;
        }
        throw new AssertionError();
    }

    public void m591c() throws IOException {
        m584a((boolean) f539s);
    }

    void m584a(boolean z) throws IOException {
        if (z) {
            this.f554p.m641a();
            this.f554p.m654b(this.f550l);
            z = this.f550l.m677d();
            if (!z) {
                this.f554p.m645a(0, (long) (z - true));
            }
        }
        new Thread(this.f555q).start();
    }

    public synchronized boolean m593d() {
        return this.f546h;
    }

    void m578a(int i, List<C0493c> list) {
        synchronized (this) {
            if (this.f556r.contains(Integer.valueOf(i))) {
                m576a(i, C0492b.PROTOCOL_ERROR);
                return;
            }
            this.f556r.add(Integer.valueOf(i));
            final int i2 = i;
            final List<C0493c> list2 = list;
            this.f557t.execute(new C0477b(this, "OkHttp %s Push Request[%s]", new Object[]{this.f543e, Integer.valueOf(i)}) {
                final /* synthetic */ C0500g f4618d;

                public void mo1295c() {
                    /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                    /*
                    r3 = this;
                    r0 = r3.f4618d;
                    r0 = r0.f547i;
                    r1 = r5;
                    r2 = r6;
                    r0 = r0.mo1308a(r1, r2);
                    if (r0 == 0) goto L_0x002e;
                L_0x000e:
                    r0 = r3.f4618d;	 Catch:{ IOException -> 0x002e }
                    r0 = r0.f554p;	 Catch:{ IOException -> 0x002e }
                    r1 = r5;	 Catch:{ IOException -> 0x002e }
                    r2 = p021c.p022a.p027e.C0492b.CANCEL;	 Catch:{ IOException -> 0x002e }
                    r0.m646a(r1, r2);	 Catch:{ IOException -> 0x002e }
                    r0 = r3.f4618d;	 Catch:{ IOException -> 0x002e }
                    monitor-enter(r0);	 Catch:{ IOException -> 0x002e }
                    r1 = r3.f4618d;	 Catch:{ all -> 0x002b }
                    r1 = r1.f556r;	 Catch:{ all -> 0x002b }
                    r2 = r5;	 Catch:{ all -> 0x002b }
                    r2 = java.lang.Integer.valueOf(r2);	 Catch:{ all -> 0x002b }
                    r1.remove(r2);	 Catch:{ all -> 0x002b }
                    monitor-exit(r0);	 Catch:{ all -> 0x002b }
                    goto L_0x002e;	 Catch:{ all -> 0x002b }
                L_0x002b:
                    r1 = move-exception;	 Catch:{ all -> 0x002b }
                    monitor-exit(r0);	 Catch:{ all -> 0x002b }
                    throw r1;	 Catch:{ IOException -> 0x002e }
                L_0x002e:
                    return;
                    */
                    throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.4.c():void");
                }
            });
        }
    }

    void m579a(int i, List<C0493c> list, boolean z) {
        final int i2 = i;
        final List<C0493c> list2 = list;
        final boolean z2 = z;
        this.f557t.execute(new C0477b(this, "OkHttp %s Push Headers[%s]", new Object[]{this.f543e, Integer.valueOf(i)}) {
            final /* synthetic */ C0500g f4622e;

            public void mo1295c() {
                /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                /*
                r4 = this;
                r0 = r4.f4622e;
                r0 = r0.f547i;
                r1 = r5;
                r2 = r6;
                r3 = r7;
                r0 = r0.mo1309a(r1, r2, r3);
                if (r0 == 0) goto L_0x001b;
            L_0x0010:
                r1 = r4.f4622e;	 Catch:{ IOException -> 0x0036 }
                r1 = r1.f554p;	 Catch:{ IOException -> 0x0036 }
                r2 = r5;	 Catch:{ IOException -> 0x0036 }
                r3 = p021c.p022a.p027e.C0492b.CANCEL;	 Catch:{ IOException -> 0x0036 }
                r1.m646a(r2, r3);	 Catch:{ IOException -> 0x0036 }
            L_0x001b:
                if (r0 != 0) goto L_0x0021;	 Catch:{ IOException -> 0x0036 }
            L_0x001d:
                r0 = r7;	 Catch:{ IOException -> 0x0036 }
                if (r0 == 0) goto L_0x0036;	 Catch:{ IOException -> 0x0036 }
            L_0x0021:
                r0 = r4.f4622e;	 Catch:{ IOException -> 0x0036 }
                monitor-enter(r0);	 Catch:{ IOException -> 0x0036 }
                r1 = r4.f4622e;	 Catch:{ all -> 0x0033 }
                r1 = r1.f556r;	 Catch:{ all -> 0x0033 }
                r2 = r5;	 Catch:{ all -> 0x0033 }
                r2 = java.lang.Integer.valueOf(r2);	 Catch:{ all -> 0x0033 }
                r1.remove(r2);	 Catch:{ all -> 0x0033 }
                monitor-exit(r0);	 Catch:{ all -> 0x0033 }
                goto L_0x0036;	 Catch:{ all -> 0x0033 }
            L_0x0033:
                r1 = move-exception;	 Catch:{ all -> 0x0033 }
                monitor-exit(r0);	 Catch:{ all -> 0x0033 }
                throw r1;	 Catch:{ IOException -> 0x0036 }
            L_0x0036:
                return;
                */
                throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.5.c():void");
            }
        });
    }

    void m577a(int i, C2284e c2284e, int i2, boolean z) throws IOException {
        final C2453c c2453c = new C2453c();
        long j = (long) i2;
        c2284e.mo2513a(j);
        c2284e.read(c2453c, j);
        if (c2453c.m7705a() != j) {
            c2284e = new StringBuilder();
            c2284e.append(c2453c.m7705a());
            c2284e.append(" != ");
            c2284e.append(i2);
            throw new IOException(c2284e.toString());
        }
        final int i3 = i;
        final int i4 = i2;
        final boolean z2 = z;
        this.f557t.execute(new C0477b(this, "OkHttp %s Push Data[%s]", new Object[]{this.f543e, Integer.valueOf(i)}) {
            final /* synthetic */ C0500g f4627f;

            public void mo1295c() {
                /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                /*
                r5 = this;
                r0 = r5.f4627f;	 Catch:{ IOException -> 0x0038 }
                r0 = r0.f547i;	 Catch:{ IOException -> 0x0038 }
                r1 = r4;	 Catch:{ IOException -> 0x0038 }
                r2 = r5;	 Catch:{ IOException -> 0x0038 }
                r3 = r6;	 Catch:{ IOException -> 0x0038 }
                r4 = r7;	 Catch:{ IOException -> 0x0038 }
                r0 = r0.mo1307a(r1, r2, r3, r4);	 Catch:{ IOException -> 0x0038 }
                if (r0 == 0) goto L_0x001d;	 Catch:{ IOException -> 0x0038 }
            L_0x0012:
                r1 = r5.f4627f;	 Catch:{ IOException -> 0x0038 }
                r1 = r1.f554p;	 Catch:{ IOException -> 0x0038 }
                r2 = r4;	 Catch:{ IOException -> 0x0038 }
                r3 = p021c.p022a.p027e.C0492b.CANCEL;	 Catch:{ IOException -> 0x0038 }
                r1.m646a(r2, r3);	 Catch:{ IOException -> 0x0038 }
            L_0x001d:
                if (r0 != 0) goto L_0x0023;	 Catch:{ IOException -> 0x0038 }
            L_0x001f:
                r0 = r7;	 Catch:{ IOException -> 0x0038 }
                if (r0 == 0) goto L_0x0038;	 Catch:{ IOException -> 0x0038 }
            L_0x0023:
                r0 = r5.f4627f;	 Catch:{ IOException -> 0x0038 }
                monitor-enter(r0);	 Catch:{ IOException -> 0x0038 }
                r1 = r5.f4627f;	 Catch:{ all -> 0x0035 }
                r1 = r1.f556r;	 Catch:{ all -> 0x0035 }
                r2 = r4;	 Catch:{ all -> 0x0035 }
                r2 = java.lang.Integer.valueOf(r2);	 Catch:{ all -> 0x0035 }
                r1.remove(r2);	 Catch:{ all -> 0x0035 }
                monitor-exit(r0);	 Catch:{ all -> 0x0035 }
                goto L_0x0038;	 Catch:{ all -> 0x0035 }
            L_0x0035:
                r1 = move-exception;	 Catch:{ all -> 0x0035 }
                monitor-exit(r0);	 Catch:{ all -> 0x0035 }
                throw r1;	 Catch:{ IOException -> 0x0038 }
            L_0x0038:
                return;
                */
                throw new UnsupportedOperationException("Method not decompiled: c.a.e.g.6.c():void");
            }
        });
    }

    void m592c(int i, C0492b c0492b) {
        final int i2 = i;
        final C0492b c0492b2 = c0492b;
        this.f557t.execute(new C0477b(this, "OkHttp %s Push Reset[%s]", new Object[]{this.f543e, Integer.valueOf(i)}) {
            final /* synthetic */ C0500g f4630d;

            public void mo1295c() {
                this.f4630d.f547i.mo1306a(i2, c0492b2);
                synchronized (this.f4630d) {
                    this.f4630d.f556r.remove(Integer.valueOf(i2));
                }
            }
        });
    }
}
