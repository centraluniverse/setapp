package p021c.p022a.p027e;

import java.io.IOException;

/* compiled from: StreamResetException */
public final class C0510o extends IOException {
    public final C0492b f598a;

    public C0510o(C0492b c0492b) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("stream was reset: ");
        stringBuilder.append(c0492b);
        super(stringBuilder.toString());
        this.f598a = c0492b;
    }
}
