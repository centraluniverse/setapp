package p021c.p022a.p027e;

import java.io.IOException;
import java.util.List;
import p125d.C2284e;

/* compiled from: PushObserver */
public interface C0508m {
    public static final C0508m f595a = new C18601();

    /* compiled from: PushObserver */
    class C18601 implements C0508m {
        public void mo1306a(int i, C0492b c0492b) {
        }

        public boolean mo1308a(int i, List<C0493c> list) {
            return true;
        }

        public boolean mo1309a(int i, List<C0493c> list, boolean z) {
            return true;
        }

        C18601() {
        }

        public boolean mo1307a(int i, C2284e c2284e, int i2, boolean z) throws IOException {
            c2284e.mo2531i((long) i2);
            return true;
        }
    }

    void mo1306a(int i, C0492b c0492b);

    boolean mo1307a(int i, C2284e c2284e, int i2, boolean z) throws IOException;

    boolean mo1308a(int i, List<C0493c> list);

    boolean mo1309a(int i, List<C0493c> list, boolean z);
}
