package p021c.p022a.p027e;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import p021c.p022a.C0488c;
import p021c.p022a.p027e.C0496d.C0495b;
import p125d.C2283d;
import p125d.C2453c;

/* compiled from: Http2Writer */
final class C0504j implements Closeable {
    private static final Logger f578b = Logger.getLogger(C0497e.class.getName());
    final C0495b f579a = new C0495b(this.f582e);
    private final C2283d f580c;
    private final boolean f581d;
    private final C2453c f582e = new C2453c();
    private int f583f = 16384;
    private boolean f584g;

    C0504j(C2283d c2283d, boolean z) {
        this.f580c = c2283d;
        this.f581d = z;
    }

    public synchronized void m641a() throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        } else if (this.f581d) {
            if (f578b.isLoggable(Level.FINE)) {
                f578b.fine(C0488c.m510a(">> CONNECTION %s", C0497e.f526a.mo1909e()));
            }
            this.f580c.mo2520c(C0497e.f526a.mo1913h());
            this.f580c.flush();
        }
    }

    public synchronized void m648a(C0509n c0509n) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        }
        this.f583f = c0509n.m678d(this.f583f);
        if (c0509n.m675c() != -1) {
            this.f579a.m556a(c0509n.m675c());
        }
        m643a(0, 0, (byte) 4, (byte) 1);
        this.f580c.flush();
    }

    public synchronized void m644a(int i, int i2, List<C0493c> list) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        }
        this.f579a.m559a((List) list);
        long a = this.f582e.m7705a();
        list = (int) Math.min((long) (this.f583f - 4), a);
        long j = (long) list;
        m643a(i, list + 4, (byte) 5, a == j ? (byte) 4 : (byte) 0);
        this.f580c.mo2530i(i2 & ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
        this.f580c.mo1284a(this.f582e, j);
        if (a > j) {
            m640b(i, a - j);
        }
    }

    public synchronized void m653b() throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        }
        this.f580c.flush();
    }

    public synchronized void m650a(boolean z, int i, int i2, List<C0493c> list) throws IOException {
        if (this.f584g != 0) {
            throw new IOException("closed");
        }
        m652a(z, i, (List) list);
    }

    public synchronized void m646a(int i, C0492b c0492b) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        } else if (c0492b.f496g == -1) {
            throw new IllegalArgumentException();
        } else {
            m643a(i, 4, (byte) 3, (byte) 0);
            this.f580c.mo2530i(c0492b.f496g);
            this.f580c.flush();
        }
    }

    public int m655c() {
        return this.f583f;
    }

    public synchronized void m651a(boolean z, int i, C2453c c2453c, int i2) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        }
        byte b = (byte) 0;
        if (z) {
            b = (byte) true;
        }
        m642a(i, b, c2453c, i2);
    }

    void m642a(int i, byte b, C2453c c2453c, int i2) throws IOException {
        m643a(i, i2, (byte) 0, b);
        if (i2 > 0) {
            this.f580c.mo1284a(c2453c, (long) i2);
        }
    }

    public synchronized void m654b(C0509n c0509n) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        }
        int i = (byte) 0;
        m643a(0, c0509n.m673b() * 6, (byte) 4, (byte) 0);
        while (i < 10) {
            if (c0509n.m672a(i)) {
                int i2 = i == 4 ? 3 : i == 7 ? 4 : i;
                this.f580c.mo2532j(i2);
                this.f580c.mo2530i(c0509n.m674b(i));
            }
            i++;
        }
        this.f580c.flush();
    }

    public synchronized void m649a(boolean z, int i, int i2) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        }
        m643a(0, 8, (byte) 6, (byte) z);
        this.f580c.mo2530i(i);
        this.f580c.mo2530i(i2);
        this.f580c.flush();
    }

    public synchronized void m647a(int i, C0492b c0492b, byte[] bArr) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        } else if (c0492b.f496g == -1) {
            throw C0497e.m562a("errorCode.httpCode == -1", new Object[0]);
        } else {
            m643a(0, 8 + bArr.length, (byte) 7, (byte) 0);
            this.f580c.mo2530i(i);
            this.f580c.mo2530i(c0492b.f496g);
            if (bArr.length > 0) {
                this.f580c.mo2520c(bArr);
            }
            this.f580c.flush();
        }
    }

    public synchronized void m645a(int i, long j) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        }
        if (j != 0) {
            if (j <= 2147483647L) {
                m643a(i, 4, (byte) 8, (byte) 0);
                this.f580c.mo2530i((int) j);
                this.f580c.flush();
            }
        }
        throw C0497e.m562a("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
    }

    public void m643a(int i, int i2, byte b, byte b2) throws IOException {
        if (f578b.isLoggable(Level.FINE)) {
            f578b.fine(C0497e.m564a(false, i, i2, b, b2));
        }
        if (i2 > this.f583f) {
            throw C0497e.m562a("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(this.f583f), Integer.valueOf(i2));
        } else if ((Integer.MIN_VALUE & i) != 0) {
            throw C0497e.m562a("reserved bit set: %s", Integer.valueOf(i));
        } else {
            C0504j.m639a(this.f580c, i2);
            this.f580c.mo2535k(b & 255);
            this.f580c.mo2535k(b2 & 255);
            this.f580c.mo2530i(i & (byte) -1);
        }
    }

    public synchronized void close() throws IOException {
        this.f584g = true;
        this.f580c.close();
    }

    private static void m639a(C2283d c2283d, int i) throws IOException {
        c2283d.mo2535k((i >>> 16) & 255);
        c2283d.mo2535k((i >>> 8) & 255);
        c2283d.mo2535k(i & 255);
    }

    private void m640b(int i, long j) throws IOException {
        while (j > 0) {
            int min = (int) Math.min((long) this.f583f, j);
            long j2 = (long) min;
            long j3 = j - j2;
            m643a(i, min, (byte) 9, j3 == 0 ? (byte) 4 : (byte) 0);
            this.f580c.mo1284a(this.f582e, j2);
            j = j3;
        }
    }

    void m652a(boolean z, int i, List<C0493c> list) throws IOException {
        if (this.f584g) {
            throw new IOException("closed");
        }
        this.f579a.m559a((List) list);
        long a = this.f582e.m7705a();
        int min = (int) Math.min((long) this.f583f, a);
        long j = (long) min;
        byte b = a == j ? (byte) 4 : (byte) 0;
        if (z) {
            b = (byte) (b | 1);
        }
        m643a(i, min, (byte) true, b);
        this.f580c.mo1284a(this.f582e, j);
        if (a > j) {
            m640b(i, a - j);
        }
    }
}
