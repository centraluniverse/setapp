package p021c.p022a.p027e;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import p021c.p022a.C0488c;
import p125d.C1624f;
import p125d.C1625m;
import p125d.C1630t;
import p125d.C2284e;
import p125d.C2453c;

/* compiled from: Hpack */
final class C0496d {
    static final C0493c[] f524a = new C0493c[]{new C0493c(C0493c.f502f, ""), new C0493c(C0493c.f499c, "GET"), new C0493c(C0493c.f499c, "POST"), new C0493c(C0493c.f500d, "/"), new C0493c(C0493c.f500d, "/index.html"), new C0493c(C0493c.f501e, "http"), new C0493c(C0493c.f501e, "https"), new C0493c(C0493c.f498b, "200"), new C0493c(C0493c.f498b, "204"), new C0493c(C0493c.f498b, "206"), new C0493c(C0493c.f498b, "304"), new C0493c(C0493c.f498b, "400"), new C0493c(C0493c.f498b, "404"), new C0493c(C0493c.f498b, "500"), new C0493c("accept-charset", ""), new C0493c("accept-encoding", "gzip, deflate"), new C0493c("accept-language", ""), new C0493c("accept-ranges", ""), new C0493c("accept", ""), new C0493c("access-control-allow-origin", ""), new C0493c("age", ""), new C0493c("allow", ""), new C0493c("authorization", ""), new C0493c("cache-control", ""), new C0493c("content-disposition", ""), new C0493c("content-encoding", ""), new C0493c("content-language", ""), new C0493c("content-length", ""), new C0493c("content-location", ""), new C0493c("content-range", ""), new C0493c("content-type", ""), new C0493c("cookie", ""), new C0493c("date", ""), new C0493c("etag", ""), new C0493c("expect", ""), new C0493c("expires", ""), new C0493c("from", ""), new C0493c("host", ""), new C0493c("if-match", ""), new C0493c("if-modified-since", ""), new C0493c("if-none-match", ""), new C0493c("if-range", ""), new C0493c("if-unmodified-since", ""), new C0493c("last-modified", ""), new C0493c("link", ""), new C0493c("location", ""), new C0493c("max-forwards", ""), new C0493c("proxy-authenticate", ""), new C0493c("proxy-authorization", ""), new C0493c("range", ""), new C0493c("referer", ""), new C0493c("refresh", ""), new C0493c("retry-after", ""), new C0493c("server", ""), new C0493c("set-cookie", ""), new C0493c("strict-transport-security", ""), new C0493c("transfer-encoding", ""), new C0493c("user-agent", ""), new C0493c("vary", ""), new C0493c("via", ""), new C0493c("www-authenticate", "")};
    static final Map<C1624f, Integer> f525b = C0496d.m561a();

    /* compiled from: Hpack */
    static final class C0494a {
        C0493c[] f506a;
        int f507b;
        int f508c;
        int f509d;
        private final List<C0493c> f510e;
        private final C2284e f511f;
        private final int f512g;
        private int f513h;

        C0494a(int i, C1630t c1630t) {
            this(i, i, c1630t);
        }

        C0494a(int i, int i2, C1630t c1630t) {
            this.f510e = new ArrayList();
            this.f506a = new C0493c[8];
            this.f507b = this.f506a.length - 1;
            this.f508c = 0;
            this.f509d = 0;
            this.f512g = i;
            this.f513h = i2;
            this.f511f = C1625m.m4841a(c1630t);
        }

        private void m539d() {
            if (this.f513h >= this.f509d) {
                return;
            }
            if (this.f513h == 0) {
                m541e();
            } else {
                m535a(this.f509d - this.f513h);
            }
        }

        private void m541e() {
            Arrays.fill(this.f506a, null);
            this.f507b = this.f506a.length - 1;
            this.f508c = 0;
            this.f509d = 0;
        }

        private int m535a(int i) {
            int i2 = 0;
            if (i > 0) {
                for (int length = this.f506a.length - 1; length >= this.f507b && i > 0; length--) {
                    i -= this.f506a[length].f505i;
                    this.f509d -= this.f506a[length].f505i;
                    this.f508c--;
                    i2++;
                }
                System.arraycopy(this.f506a, this.f507b + 1, this.f506a, (this.f507b + 1) + i2, this.f508c);
                this.f507b += i2;
            }
            return i2;
        }

        void m549a() throws IOException {
            while (!this.f511f.mo2525f()) {
                int i = this.f511f.mo2529i() & 255;
                if (i == 128) {
                    throw new IOException("index == 0");
                } else if ((i & 128) == 128) {
                    m537b(m548a(i, 127) - 1);
                } else if (i == 64) {
                    m545g();
                } else if ((i & 64) == 64) {
                    m542e(m548a(i, 63) - 1);
                } else if ((i & 32) == 32) {
                    this.f513h = m548a(i, 31);
                    if (this.f513h >= 0) {
                        if (this.f513h <= this.f512g) {
                            m539d();
                        }
                    }
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Invalid dynamic table size update ");
                    stringBuilder.append(this.f513h);
                    throw new IOException(stringBuilder.toString());
                } else {
                    if (i != 16) {
                        if (i != 0) {
                            m540d(m548a(i, 15) - 1);
                        }
                    }
                    m544f();
                }
            }
        }

        public List<C0493c> m550b() {
            List arrayList = new ArrayList(this.f510e);
            this.f510e.clear();
            return arrayList;
        }

        private void m537b(int i) throws IOException {
            if (m546g(i)) {
                this.f510e.add(C0496d.f524a[i]);
                return;
            }
            int c = m538c(i - C0496d.f524a.length);
            if (c >= 0) {
                if (c <= this.f506a.length - 1) {
                    this.f510e.add(this.f506a[c]);
                    return;
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Header index too large ");
            stringBuilder.append(i + 1);
            throw new IOException(stringBuilder.toString());
        }

        private int m538c(int i) {
            return (this.f507b + 1) + i;
        }

        private void m540d(int i) throws IOException {
            this.f510e.add(new C0493c(m543f(i), m551c()));
        }

        private void m544f() throws IOException {
            this.f510e.add(new C0493c(C0496d.m560a(m551c()), m551c()));
        }

        private void m542e(int i) throws IOException {
            m536a(-1, new C0493c(m543f(i), m551c()));
        }

        private void m545g() throws IOException {
            m536a(-1, new C0493c(C0496d.m560a(m551c()), m551c()));
        }

        private C1624f m543f(int i) {
            if (m546g(i)) {
                return C0496d.f524a[i].f503g;
            }
            return this.f506a[m538c(i - C0496d.f524a.length)].f503g;
        }

        private boolean m546g(int i) {
            return i >= 0 && i <= C0496d.f524a.length - 1;
        }

        private void m536a(int i, C0493c c0493c) {
            this.f510e.add(c0493c);
            int i2 = c0493c.f505i;
            if (i != -1) {
                i2 -= this.f506a[m538c(i)].f505i;
            }
            if (i2 > this.f513h) {
                m541e();
                return;
            }
            int a = m535a((this.f509d + i2) - this.f513h);
            if (i == -1) {
                if (this.f508c + 1 > this.f506a.length) {
                    i = new C0493c[(this.f506a.length * 2)];
                    System.arraycopy(this.f506a, 0, i, this.f506a.length, this.f506a.length);
                    this.f507b = this.f506a.length - 1;
                    this.f506a = i;
                }
                i = this.f507b;
                this.f507b = i - 1;
                this.f506a[i] = c0493c;
                this.f508c++;
            } else {
                this.f506a[i + (m538c(i) + a)] = c0493c;
            }
            this.f509d += i2;
        }

        private int m547h() throws IOException {
            return this.f511f.mo2529i() & 255;
        }

        int m548a(int i, int i2) throws IOException {
            i &= i2;
            if (i < i2) {
                return i;
            }
            i = 0;
            while (true) {
                int h = m547h();
                if ((h & 128) == 0) {
                    return i2 + (h << i);
                }
                i2 += (h & 127) << i;
                i += 7;
            }
        }

        C1624f m551c() throws IOException {
            int h = m547h();
            Object obj = (h & 128) == 128 ? 1 : null;
            h = m548a(h, 127);
            if (obj != null) {
                return C1624f.m4821a(C0506k.m656a().m661a(this.f511f.mo2528h((long) h)));
            }
            return this.f511f.mo2522d((long) h);
        }
    }

    /* compiled from: Hpack */
    static final class C0495b {
        int f514a;
        int f515b;
        C0493c[] f516c;
        int f517d;
        int f518e;
        int f519f;
        private final C2453c f520g;
        private final boolean f521h;
        private int f522i;
        private boolean f523j;

        C0495b(C2453c c2453c) {
            this(4096, true, c2453c);
        }

        C0495b(int i, boolean z, C2453c c2453c) {
            this.f522i = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            this.f516c = new C0493c[8];
            this.f517d = this.f516c.length - 1;
            this.f518e = 0;
            this.f519f = 0;
            this.f514a = i;
            this.f515b = i;
            this.f521h = z;
            this.f520g = c2453c;
        }

        private void m552a() {
            Arrays.fill(this.f516c, null);
            this.f517d = this.f516c.length - 1;
            this.f518e = 0;
            this.f519f = 0;
        }

        private int m554b(int i) {
            int i2 = 0;
            if (i > 0) {
                for (int length = this.f516c.length - 1; length >= this.f517d && i > 0; length--) {
                    i -= this.f516c[length].f505i;
                    this.f519f -= this.f516c[length].f505i;
                    this.f518e--;
                    i2++;
                }
                System.arraycopy(this.f516c, this.f517d + 1, this.f516c, (this.f517d + 1) + i2, this.f518e);
                Arrays.fill(this.f516c, this.f517d + 1, (this.f517d + 1) + i2, null);
                this.f517d += i2;
            }
            return i2;
        }

        private void m553a(C0493c c0493c) {
            int i = c0493c.f505i;
            if (i > this.f515b) {
                m552a();
                return;
            }
            m554b((this.f519f + i) - this.f515b);
            if (this.f518e + 1 > this.f516c.length) {
                Object obj = new C0493c[(this.f516c.length * 2)];
                System.arraycopy(this.f516c, 0, obj, this.f516c.length, this.f516c.length);
                this.f517d = this.f516c.length - 1;
                this.f516c = obj;
            }
            int i2 = this.f517d;
            this.f517d = i2 - 1;
            this.f516c[i2] = c0493c;
            this.f518e++;
            this.f519f += i;
        }

        void m559a(List<C0493c> list) throws IOException {
            if (this.f523j) {
                if (this.f522i < this.f515b) {
                    m557a(this.f522i, 31, 32);
                }
                this.f523j = false;
                this.f522i = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
                m557a(this.f515b, 31, 32);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                int intValue;
                int i2;
                int length;
                int i3;
                C0493c c0493c = (C0493c) list.get(i);
                C1624f f = c0493c.f503g.mo1911f();
                C1624f c1624f = c0493c.f504h;
                Integer num = (Integer) C0496d.f525b.get(f);
                if (num != null) {
                    intValue = num.intValue() + 1;
                    if (intValue > 1 && intValue < 8) {
                        if (!C0488c.m521a(C0496d.f524a[intValue - 1].f504h, (Object) c1624f)) {
                            if (C0488c.m521a(C0496d.f524a[intValue].f504h, (Object) c1624f)) {
                                i2 = intValue;
                                intValue++;
                                if (intValue == -1) {
                                    length = this.f516c.length;
                                    for (i3 = this.f517d + 1; i3 < length; i3++) {
                                        if (C0488c.m521a(this.f516c[i3].f503g, (Object) f)) {
                                            if (!C0488c.m521a(this.f516c[i3].f504h, (Object) c1624f)) {
                                                intValue = C0496d.f524a.length + (i3 - this.f517d);
                                                break;
                                            } else if (i2 != -1) {
                                                i2 = (i3 - this.f517d) + C0496d.f524a.length;
                                            }
                                        }
                                    }
                                }
                                if (intValue == -1) {
                                    m557a(intValue, 127, 128);
                                } else if (i2 == -1) {
                                    this.f520g.m7725b(64);
                                    m558a(f);
                                    m558a(c1624f);
                                    m553a(c0493c);
                                } else if (f.m4831a(C0493c.f497a) || C0493c.f502f.equals(f)) {
                                    m557a(i2, 63, 64);
                                    m558a(c1624f);
                                    m553a(c0493c);
                                } else {
                                    m557a(i2, 15, 0);
                                    m558a(c1624f);
                                }
                            }
                        }
                    }
                    i2 = intValue;
                    intValue = -1;
                    if (intValue == -1) {
                        length = this.f516c.length;
                        for (i3 = this.f517d + 1; i3 < length; i3++) {
                            if (C0488c.m521a(this.f516c[i3].f503g, (Object) f)) {
                                if (!C0488c.m521a(this.f516c[i3].f504h, (Object) c1624f)) {
                                    intValue = C0496d.f524a.length + (i3 - this.f517d);
                                    break;
                                } else if (i2 != -1) {
                                    i2 = (i3 - this.f517d) + C0496d.f524a.length;
                                }
                            }
                        }
                    }
                    if (intValue == -1) {
                        m557a(intValue, 127, 128);
                    } else if (i2 == -1) {
                        if (f.m4831a(C0493c.f497a)) {
                        }
                        m557a(i2, 63, 64);
                        m558a(c1624f);
                        m553a(c0493c);
                    } else {
                        this.f520g.m7725b(64);
                        m558a(f);
                        m558a(c1624f);
                        m553a(c0493c);
                    }
                } else {
                    intValue = -1;
                }
                i2 = intValue;
                if (intValue == -1) {
                    length = this.f516c.length;
                    for (i3 = this.f517d + 1; i3 < length; i3++) {
                        if (C0488c.m521a(this.f516c[i3].f503g, (Object) f)) {
                            if (!C0488c.m521a(this.f516c[i3].f504h, (Object) c1624f)) {
                                intValue = C0496d.f524a.length + (i3 - this.f517d);
                                break;
                            } else if (i2 != -1) {
                                i2 = (i3 - this.f517d) + C0496d.f524a.length;
                            }
                        }
                    }
                }
                if (intValue == -1) {
                    m557a(intValue, 127, 128);
                } else if (i2 == -1) {
                    this.f520g.m7725b(64);
                    m558a(f);
                    m558a(c1624f);
                    m553a(c0493c);
                } else {
                    if (f.m4831a(C0493c.f497a)) {
                    }
                    m557a(i2, 63, 64);
                    m558a(c1624f);
                    m553a(c0493c);
                }
            }
        }

        void m557a(int i, int i2, int i3) {
            if (i < i2) {
                this.f520g.m7725b(i | i3);
                return;
            }
            this.f520g.m7725b(i3 | i2);
            i -= i2;
            while (i >= 128) {
                this.f520g.m7725b(128 | (i & 127));
                i >>>= 7;
            }
            this.f520g.m7725b(i);
        }

        void m558a(C1624f c1624f) throws IOException {
            if (!this.f521h || C0506k.m656a().m659a(c1624f) >= c1624f.mo1912g()) {
                m557a(c1624f.mo1912g(), 127, 0);
                this.f520g.m7712a(c1624f);
                return;
            }
            Object c2453c = new C2453c();
            C0506k.m656a().m660a(c1624f, c2453c);
            c1624f = c2453c.m7771r();
            m557a(c1624f.mo1912g(), 127, 128);
            this.f520g.m7712a(c1624f);
        }

        void m556a(int i) {
            this.f514a = i;
            i = Math.min(i, 16384);
            if (this.f515b != i) {
                if (i < this.f515b) {
                    this.f522i = Math.min(this.f522i, i);
                }
                this.f523j = true;
                this.f515b = i;
                m555b();
            }
        }

        private void m555b() {
            if (this.f515b >= this.f519f) {
                return;
            }
            if (this.f515b == 0) {
                m552a();
            } else {
                m554b(this.f519f - this.f515b);
            }
        }
    }

    private static Map<C1624f, Integer> m561a() {
        Map linkedHashMap = new LinkedHashMap(f524a.length);
        for (int i = 0; i < f524a.length; i++) {
            if (!linkedHashMap.containsKey(f524a[i].f503g)) {
                linkedHashMap.put(f524a[i].f503g, Integer.valueOf(i));
            }
        }
        return Collections.unmodifiableMap(linkedHashMap);
    }

    static C1624f m560a(C1624f c1624f) throws IOException {
        int g = c1624f.mo1912g();
        int i = 0;
        while (i < g) {
            byte a = c1624f.mo1900a(i);
            if (a < (byte) 65 || a > (byte) 90) {
                i++;
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("PROTOCOL_ERROR response malformed: mixed case name: ");
                stringBuilder.append(c1624f.mo1902a());
                throw new IOException(stringBuilder.toString());
            }
        }
        return c1624f;
    }
}
