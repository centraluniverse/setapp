package p021c.p022a.p027e;

/* compiled from: ErrorCode */
public enum C0492b {
    NO_ERROR(0),
    PROTOCOL_ERROR(1),
    INTERNAL_ERROR(2),
    FLOW_CONTROL_ERROR(3),
    REFUSED_STREAM(7),
    CANCEL(8);
    
    public final int f496g;

    private C0492b(int i) {
        this.f496g = i;
    }

    public static C0492b m534a(int i) {
        for (C0492b c0492b : C0492b.values()) {
            if (c0492b.f496g == i) {
                return c0492b;
            }
        }
        return 0;
    }
}
