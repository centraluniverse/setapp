package p021c.p022a.p027e;

import java.util.concurrent.CountDownLatch;

/* compiled from: Ping */
final class C0507l {
    private final CountDownLatch f592a = new CountDownLatch(1);
    private long f593b = -1;
    private long f594c = -1;

    C0507l() {
    }

    void m662a() {
        if (this.f593b != -1) {
            throw new IllegalStateException();
        }
        this.f593b = System.nanoTime();
    }

    void m663b() {
        if (this.f594c == -1) {
            if (this.f593b != -1) {
                this.f594c = System.nanoTime();
                this.f592a.countDown();
                return;
            }
        }
        throw new IllegalStateException();
    }

    void m664c() {
        if (this.f594c == -1) {
            if (this.f593b != -1) {
                this.f594c = this.f593b - 1;
                this.f592a.countDown();
                return;
            }
        }
        throw new IllegalStateException();
    }
}
