package p021c.p022a.p027e;

import android.support.v4.view.ViewCompat;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import p021c.p022a.C0488c;
import p021c.p022a.p027e.C0496d.C0494a;
import p125d.C1624f;
import p125d.C1630t;
import p125d.C1631u;
import p125d.C2284e;
import p125d.C2453c;

/* compiled from: Http2Reader */
final class C0502h implements Closeable {
    static final Logger f560a = Logger.getLogger(C0497e.class.getName());
    final C0494a f561b = new C0494a(4096, this.f563d);
    private final C2284e f562c;
    private final C1857a f563d = new C1857a(this.f562c);
    private final boolean f564e;

    /* compiled from: Http2Reader */
    interface C0501b {
        void mo1296a();

        void mo1297a(int i, int i2, int i3, boolean z);

        void mo1298a(int i, int i2, List<C0493c> list) throws IOException;

        void mo1299a(int i, long j);

        void mo1300a(int i, C0492b c0492b);

        void mo1301a(int i, C0492b c0492b, C1624f c1624f);

        void mo1302a(boolean z, int i, int i2);

        void mo1303a(boolean z, int i, int i2, List<C0493c> list);

        void mo1304a(boolean z, int i, C2284e c2284e, int i2) throws IOException;

        void mo1305a(boolean z, C0509n c0509n);
    }

    /* compiled from: Http2Reader */
    static final class C1857a implements C1630t {
        int f4638a;
        byte f4639b;
        int f4640c;
        int f4641d;
        short f4642e;
        private final C2284e f4643f;

        public void close() throws IOException {
        }

        C1857a(C2284e c2284e) {
            this.f4643f = c2284e;
        }

        public long read(C2453c c2453c, long j) throws IOException {
            while (this.f4641d == 0) {
                this.f4643f.mo2531i((long) this.f4642e);
                this.f4642e = (short) 0;
                if ((this.f4639b & 4) != 0) {
                    return -1;
                }
                m5137a();
            }
            c2453c = this.f4643f.read(c2453c, Math.min(j, (long) this.f4641d));
            if (c2453c == -1) {
                return -1;
            }
            this.f4641d = (int) (((long) this.f4641d) - c2453c);
            return c2453c;
        }

        public C1631u timeout() {
            return this.f4643f.timeout();
        }

        private void m5137a() throws IOException {
            int i = this.f4640c;
            int a = C0502h.m606a(this.f4643f);
            this.f4641d = a;
            this.f4638a = a;
            byte i2 = (byte) (this.f4643f.mo2529i() & 255);
            this.f4639b = (byte) (this.f4643f.mo2529i() & 255);
            if (C0502h.f560a.isLoggable(Level.FINE)) {
                C0502h.f560a.fine(C0497e.m564a(true, this.f4640c, this.f4638a, i2, this.f4639b));
            }
            this.f4640c = this.f4643f.mo2534k() & ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            if (i2 != (byte) 9) {
                throw C0497e.m565b("%s != TYPE_CONTINUATION", Byte.valueOf(i2));
            } else if (this.f4640c != i) {
                throw C0497e.m565b("TYPE_CONTINUATION streamId changed", new Object[0]);
            }
        }
    }

    C0502h(C2284e c2284e, boolean z) {
        this.f562c = c2284e;
        this.f564e = z;
    }

    public void m618a(C0501b c0501b) throws IOException {
        if (!this.f564e) {
            c0501b = this.f562c.mo2522d((long) C0497e.f526a.mo1912g());
            if (f560a.isLoggable(Level.FINE)) {
                f560a.fine(C0488c.m510a("<< CONNECTION %s", c0501b.mo1909e()));
            }
            if (!C0497e.f526a.equals(c0501b)) {
                throw C0497e.m565b("Expected a connection header but was %s", c0501b.mo1902a());
            }
        } else if (m619a(true, c0501b) == null) {
            throw C0497e.m565b("Required SETTINGS preface not received", new Object[0]);
        }
    }

    public boolean m619a(boolean r7, p021c.p022a.p027e.C0502h.C0501b r8) throws java.io.IOException {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r6 = this;
        r0 = 0;
        r1 = r6.f562c;	 Catch:{ IOException -> 0x0097 }
        r2 = 9;	 Catch:{ IOException -> 0x0097 }
        r1.mo2513a(r2);	 Catch:{ IOException -> 0x0097 }
        r1 = r6.f562c;
        r1 = p021c.p022a.p027e.C0502h.m606a(r1);
        r2 = 1;
        if (r1 < 0) goto L_0x0088;
    L_0x0011:
        r3 = 16384; // 0x4000 float:2.2959E-41 double:8.0948E-320;
        if (r1 <= r3) goto L_0x0017;
    L_0x0015:
        goto L_0x0088;
    L_0x0017:
        r3 = r6.f562c;
        r3 = r3.mo2529i();
        r3 = r3 & 255;
        r3 = (byte) r3;
        if (r7 == 0) goto L_0x0034;
    L_0x0022:
        r7 = 4;
        if (r3 == r7) goto L_0x0034;
    L_0x0025:
        r7 = "Expected a SETTINGS frame but was %s";
        r8 = new java.lang.Object[r2];
        r1 = java.lang.Byte.valueOf(r3);
        r8[r0] = r1;
        r7 = p021c.p022a.p027e.C0497e.m565b(r7, r8);
        throw r7;
    L_0x0034:
        r7 = r6.f562c;
        r7 = r7.mo2529i();
        r7 = r7 & 255;
        r7 = (byte) r7;
        r0 = r6.f562c;
        r0 = r0.mo2534k();
        r4 = 2147483647; // 0x7fffffff float:NaN double:1.060997895E-314;
        r0 = r0 & r4;
        r4 = f560a;
        r5 = java.util.logging.Level.FINE;
        r4 = r4.isLoggable(r5);
        if (r4 == 0) goto L_0x005a;
    L_0x0051:
        r4 = f560a;
        r5 = p021c.p022a.p027e.C0497e.m564a(r2, r0, r1, r3, r7);
        r4.fine(r5);
    L_0x005a:
        switch(r3) {
            case 0: goto L_0x0084;
            case 1: goto L_0x0080;
            case 2: goto L_0x007c;
            case 3: goto L_0x0078;
            case 4: goto L_0x0074;
            case 5: goto L_0x0070;
            case 6: goto L_0x006c;
            case 7: goto L_0x0068;
            case 8: goto L_0x0064;
            default: goto L_0x005d;
        };
    L_0x005d:
        r7 = r6.f562c;
        r0 = (long) r1;
        r7.mo2531i(r0);
        goto L_0x0087;
    L_0x0064:
        r6.m617i(r8, r1, r7, r0);
        goto L_0x0087;
    L_0x0068:
        r6.m616h(r8, r1, r7, r0);
        goto L_0x0087;
    L_0x006c:
        r6.m615g(r8, r1, r7, r0);
        goto L_0x0087;
    L_0x0070:
        r6.m614f(r8, r1, r7, r0);
        goto L_0x0087;
    L_0x0074:
        r6.m613e(r8, r1, r7, r0);
        goto L_0x0087;
    L_0x0078:
        r6.m612d(r8, r1, r7, r0);
        goto L_0x0087;
    L_0x007c:
        r6.m611c(r8, r1, r7, r0);
        goto L_0x0087;
    L_0x0080:
        r6.m609a(r8, r1, r7, r0);
        goto L_0x0087;
    L_0x0084:
        r6.m610b(r8, r1, r7, r0);
    L_0x0087:
        return r2;
    L_0x0088:
        r7 = "FRAME_SIZE_ERROR: %s";
        r8 = new java.lang.Object[r2];
        r1 = java.lang.Integer.valueOf(r1);
        r8[r0] = r1;
        r7 = p021c.p022a.p027e.C0497e.m565b(r7, r8);
        throw r7;
    L_0x0097:
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.e.h.a(boolean, c.a.e.h$b):boolean");
    }

    private void m609a(C0501b c0501b, int i, byte b, int i2) throws IOException {
        short s = (short) 0;
        if (i2 == 0) {
            throw C0497e.m565b("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        }
        boolean z = (b & 1) != 0;
        if ((b & 8) != 0) {
            s = (short) (this.f562c.mo2529i() & 255);
        }
        if ((b & 32) != 0) {
            m608a(c0501b, i2);
            i -= 5;
        }
        c0501b.mo1303a(z, i2, (int) (byte) -1, m607a(C0502h.m605a(i, b, s), s, b, i2));
    }

    private List<C0493c> m607a(int i, short s, byte b, int i2) throws IOException {
        C1857a c1857a = this.f563d;
        this.f563d.f4641d = i;
        c1857a.f4638a = i;
        this.f563d.f4642e = s;
        this.f563d.f4639b = b;
        this.f563d.f4640c = i2;
        this.f561b.m549a();
        return this.f561b.m550b();
    }

    private void m610b(C0501b c0501b, int i, byte b, int i2) throws IOException {
        short s = (short) 0;
        if (i2 == 0) {
            throw C0497e.m565b("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        }
        short s2 = (short) 1;
        boolean z = (b & 1) != 0;
        if ((b & 32) == 0) {
            s2 = (short) 0;
        }
        if (s2 != (short) 0) {
            throw C0497e.m565b("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
        }
        if ((b & 8) != 0) {
            s = (short) (this.f562c.mo2529i() & 255);
        }
        c0501b.mo1304a(z, i2, this.f562c, C0502h.m605a(i, b, s));
        this.f562c.mo2531i((long) s);
    }

    private void m611c(C0501b c0501b, int i, byte b, int i2) throws IOException {
        if (i != 5) {
            throw C0497e.m565b("TYPE_PRIORITY length: %d != 5", new Object[]{Integer.valueOf(i)});
        } else if (i2 == 0) {
            throw C0497e.m565b("TYPE_PRIORITY streamId == 0", new Object[0]);
        } else {
            m608a(c0501b, i2);
        }
    }

    private void m608a(C0501b c0501b, int i) throws IOException {
        int k = this.f562c.mo2534k();
        c0501b.mo1297a(i, k & ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, (this.f562c.mo2529i() & 255) + 1, (Integer.MIN_VALUE & k) != 0);
    }

    private void m612d(C0501b c0501b, int i, byte b, int i2) throws IOException {
        if (i != 4) {
            throw C0497e.m565b("TYPE_RST_STREAM length: %d != 4", new Object[]{Integer.valueOf(i)});
        } else if (i2 == 0) {
            throw C0497e.m565b("TYPE_RST_STREAM streamId == 0", new Object[0]);
        } else {
            C0492b a = C0492b.m534a(this.f562c.mo2534k());
            if (a == null) {
                throw C0497e.m565b("TYPE_RST_STREAM unexpected error code: %d", new Object[]{Integer.valueOf(i)});
            } else {
                c0501b.mo1300a(i2, a);
            }
        }
    }

    private void m613e(C0501b c0501b, int i, byte b, int i2) throws IOException {
        if (i2 != 0) {
            throw C0497e.m565b("TYPE_SETTINGS streamId != 0", new Object[0]);
        } else if ((b & (byte) 1) != (byte) 0) {
            if (i != 0) {
                throw C0497e.m565b("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
            }
            c0501b.mo1296a();
        } else if (i % 6 != (byte) 0) {
            throw C0497e.m565b("TYPE_SETTINGS length %% 6 != 0: %s", new Object[]{Integer.valueOf(i)});
        } else {
            C0509n c0509n = new C0509n();
            for (int i3 = 0; i3 < i; i3 += 6) {
                int j = this.f562c.mo2533j();
                int k = this.f562c.mo2534k();
                switch (j) {
                    case 1:
                    case 6:
                        break;
                    case 2:
                        if (!(k == 0 || k == 1)) {
                            throw C0497e.m565b("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                        }
                    case 3:
                        j = 4;
                        break;
                    case 4:
                        j = 7;
                        if (k >= 0) {
                            break;
                        }
                        throw C0497e.m565b("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                    case 5:
                        if (k >= 16384 && k <= ViewCompat.MEASURED_SIZE_MASK) {
                            break;
                        }
                        throw C0497e.m565b("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", new Object[]{Integer.valueOf(k)});
                        break;
                    default:
                        break;
                }
                c0509n.m669a(j, k);
            }
            c0501b.mo1305a(false, c0509n);
        }
    }

    private void m614f(C0501b c0501b, int i, byte b, int i2) throws IOException {
        short s = (short) 0;
        if (i2 == 0) {
            throw C0497e.m565b("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        }
        if ((b & 8) != 0) {
            s = (short) (this.f562c.mo2529i() & 255);
        }
        c0501b.mo1298a(i2, this.f562c.mo2534k() & ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, m607a(C0502h.m605a(i - 4, b, s), s, b, i2));
    }

    private void m615g(C0501b c0501b, int i, byte b, int i2) throws IOException {
        boolean z = false;
        if (i != 8) {
            throw C0497e.m565b("TYPE_PING length != 8: %s", new Object[]{Integer.valueOf(i)});
        } else if (i2 != 0) {
            throw C0497e.m565b("TYPE_PING streamId != 0", new Object[0]);
        } else {
            i = this.f562c.mo2534k();
            i2 = this.f562c.mo2534k();
            if ((b & (byte) 1) != (byte) 0) {
                z = true;
            }
            c0501b.mo1302a(z, i, i2);
        }
    }

    private void m616h(C0501b c0501b, int i, byte b, int i2) throws IOException {
        if (i < 8) {
            throw C0497e.m565b("TYPE_GOAWAY length < 8: %s", new Object[]{Integer.valueOf(i)});
        } else if (i2 != 0) {
            throw C0497e.m565b("TYPE_GOAWAY streamId != 0", new Object[0]);
        } else {
            i2 = this.f562c.mo2534k();
            i -= 8;
            C0492b a = C0492b.m534a(this.f562c.mo2534k());
            if (a == null) {
                throw C0497e.m565b("TYPE_GOAWAY unexpected error code: %d", new Object[]{Integer.valueOf(r2)});
            }
            C1624f c1624f = C1624f.f4400b;
            if (i > 0) {
                c1624f = this.f562c.mo2522d((long) i);
            }
            c0501b.mo1301a(i2, a, c1624f);
        }
    }

    private void m617i(C0501b c0501b, int i, byte b, int i2) throws IOException {
        if (i != 4) {
            throw C0497e.m565b("TYPE_WINDOW_UPDATE length !=4: %s", new Object[]{Integer.valueOf(i)});
        }
        long k = ((long) this.f562c.mo2534k()) & 2147483647L;
        if (k == 0) {
            throw C0497e.m565b("windowSizeIncrement was 0", new Object[]{Long.valueOf(k)});
        } else {
            c0501b.mo1299a(i2, k);
        }
    }

    public void close() throws IOException {
        this.f562c.close();
    }

    static int m606a(C2284e c2284e) throws IOException {
        return (c2284e.mo2529i() & 255) | (((c2284e.mo2529i() & 255) << 16) | ((c2284e.mo2529i() & 255) << 8));
    }

    static int m605a(int i, byte b, short s) throws IOException {
        if ((b & 8) != (byte) 0) {
            short s2 = i - 1;
        }
        if (s <= s2) {
            return (short) (s2 - s);
        }
        throw C0497e.m565b("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(s2));
    }
}
