package p021c.p022a.p027e;

import java.io.IOException;
import p021c.p022a.C0488c;
import p125d.C1624f;

/* compiled from: Http2 */
public final class C0497e {
    static final C1624f f526a = C1624f.m4820a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");
    static final String[] f527b = new String[64];
    static final String[] f528c = new String[256];
    private static final String[] f529d;

    static {
        r0 = new String[10];
        int i = 0;
        r0[0] = "DATA";
        r0[1] = "HEADERS";
        r0[2] = "PRIORITY";
        r0[3] = "RST_STREAM";
        r0[4] = "SETTINGS";
        r0[5] = "PUSH_PROMISE";
        r0[6] = "PING";
        r0[7] = "GOAWAY";
        r0[8] = "WINDOW_UPDATE";
        r0[9] = "CONTINUATION";
        f529d = r0;
        for (int i2 = 0; i2 < f528c.length; i2++) {
            f528c[i2] = C0488c.m510a("%8s", Integer.toBinaryString(i2)).replace(' ', '0');
        }
        f527b[0] = "";
        f527b[1] = "END_STREAM";
        int[] iArr = new int[]{1};
        f527b[8] = "PADDED";
        for (int i3 : iArr) {
            String[] strArr = f527b;
            int i4 = i3 | 8;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(f527b[i3]);
            stringBuilder.append("|PADDED");
            strArr[i4] = stringBuilder.toString();
        }
        f527b[4] = "END_HEADERS";
        f527b[32] = "PRIORITY";
        f527b[36] = "END_HEADERS|PRIORITY";
        for (int i5 : new int[]{4, 32, 36}) {
            for (int i6 : iArr) {
                String[] strArr2 = f527b;
                int i7 = i6 | i5;
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append(f527b[i6]);
                stringBuilder2.append('|');
                stringBuilder2.append(f527b[i5]);
                strArr2[i7] = stringBuilder2.toString();
                strArr2 = f527b;
                i7 |= 8;
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append(f527b[i6]);
                stringBuilder2.append('|');
                stringBuilder2.append(f527b[i5]);
                stringBuilder2.append("|PADDED");
                strArr2[i7] = stringBuilder2.toString();
            }
        }
        while (i < f527b.length) {
            if (f527b[i] == null) {
                f527b[i] = f528c[i];
            }
            i++;
        }
    }

    private C0497e() {
    }

    static IllegalArgumentException m562a(String str, Object... objArr) {
        throw new IllegalArgumentException(C0488c.m510a(str, objArr));
    }

    static IOException m565b(String str, Object... objArr) throws IOException {
        throw new IOException(C0488c.m510a(str, objArr));
    }

    static String m564a(boolean z, int i, int i2, byte b, byte b2) {
        String a = b < f529d.length ? f529d[b] : C0488c.m510a("0x%02x", Byte.valueOf(b));
        b = C0497e.m563a(b, b2);
        String str = "%s 0x%08x %5d %-13s %s";
        Object[] objArr = new Object[5];
        objArr[0] = z ? "<<" : ">>";
        objArr[1] = Integer.valueOf(i);
        objArr[true] = Integer.valueOf(i2);
        objArr[true] = a;
        objArr[true] = b;
        return C0488c.m510a(str, objArr);
    }

    static String m563a(byte b, byte b2) {
        if (b2 == (byte) 0) {
            return "";
        }
        switch (b) {
            case (byte) 2:
            case (byte) 3:
            case (byte) 7:
            case (byte) 8:
                return f528c[b2];
            case (byte) 4:
            case (byte) 6:
                return b2 == (byte) 1 ? "ACK" : f528c[b2];
            default:
                String str;
                if (b2 < f527b.length) {
                    str = f527b[b2];
                } else {
                    str = f528c[b2];
                }
                if (b != (byte) 5 || (b2 & 4) == 0) {
                    return (b != (byte) 0 || (b2 & 32) == (byte) 0) ? str : str.replace("PRIORITY", "COMPRESSED");
                } else {
                    return str.replace("HEADERS", "PUSH_PROMISE");
                }
        }
    }
}
