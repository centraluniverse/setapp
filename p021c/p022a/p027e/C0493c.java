package p021c.p022a.p027e;

import p021c.p022a.C0488c;
import p125d.C1624f;

/* compiled from: Header */
public final class C0493c {
    public static final C1624f f497a = C1624f.m4820a(":");
    public static final C1624f f498b = C1624f.m4820a(":status");
    public static final C1624f f499c = C1624f.m4820a(":method");
    public static final C1624f f500d = C1624f.m4820a(":path");
    public static final C1624f f501e = C1624f.m4820a(":scheme");
    public static final C1624f f502f = C1624f.m4820a(":authority");
    public final C1624f f503g;
    public final C1624f f504h;
    final int f505i;

    public C0493c(String str, String str2) {
        this(C1624f.m4820a(str), C1624f.m4820a(str2));
    }

    public C0493c(C1624f c1624f, String str) {
        this(c1624f, C1624f.m4820a(str));
    }

    public C0493c(C1624f c1624f, C1624f c1624f2) {
        this.f503g = c1624f;
        this.f504h = c1624f2;
        this.f505i = (32 + c1624f.mo1912g()) + c1624f2.mo1912g();
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (!(obj instanceof C0493c)) {
            return false;
        }
        C0493c c0493c = (C0493c) obj;
        if (this.f503g.equals(c0493c.f503g) && this.f504h.equals(c0493c.f504h) != null) {
            z = true;
        }
        return z;
    }

    public int hashCode() {
        return (31 * (527 + this.f503g.hashCode())) + this.f504h.hashCode();
    }

    public String toString() {
        return C0488c.m510a("%s: %s", this.f503g.mo1902a(), this.f504h.mo1902a());
    }
}
