package p021c.p022a.p027e;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.EOFException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import p125d.C1629s;
import p125d.C1630t;
import p125d.C1631u;
import p125d.C2282a;
import p125d.C2284e;
import p125d.C2453c;

/* compiled from: Http2Stream */
public final class C0503i {
    static final /* synthetic */ boolean f565i = true;
    long f566a = 0;
    long f567b;
    final int f568c;
    final C0500g f569d;
    final C1858a f570e;
    final C2364c f571f = new C2364c(this);
    final C2364c f572g = new C2364c(this);
    C0492b f573h = null;
    private final List<C0493c> f574j;
    private List<C0493c> f575k;
    private boolean f576l;
    private final C1859b f577m;

    /* compiled from: Http2Stream */
    final class C1858a implements C1629s {
        static final /* synthetic */ boolean f4644c = true;
        boolean f4645a;
        boolean f4646b;
        final /* synthetic */ C0503i f4647d;
        private final C2453c f4648e = new C2453c();

        static {
            Class cls = C0503i.class;
        }

        C1858a(C0503i c0503i) {
            this.f4647d = c0503i;
        }

        public void mo1284a(C2453c c2453c, long j) throws IOException {
            if (f4644c || !Thread.holdsLock(this.f4647d)) {
                this.f4648e.mo1284a(c2453c, j);
                while (this.f4648e.m7705a() >= PlaybackStateCompat.ACTION_PREPARE) {
                    m5138a(null);
                }
                return;
            }
            throw new AssertionError();
        }

        private void m5138a(boolean z) throws IOException {
            synchronized (this.f4647d) {
                this.f4647d.f572g.m6902c();
                while (this.f4647d.f567b <= 0 && !this.f4646b && !this.f4645a && this.f4647d.f573h == null) {
                    try {
                        this.f4647d.m638l();
                    } finally {
                        this.f4647d.f572g.m7016b();
                    }
                }
                this.f4647d.m637k();
                long min = Math.min(this.f4647d.f567b, this.f4648e.m7705a());
                C0503i c0503i = this.f4647d;
                c0503i.f567b -= min;
            }
            this.f4647d.f572g.m6902c();
            try {
                C0500g c0500g = this.f4647d.f569d;
                int i = this.f4647d.f568c;
                z = (z && min == this.f4648e.m7705a()) ? f4644c : false;
                c0500g.m580a(i, z, this.f4648e, min);
            } finally {
                this.f4647d.f572g.m7016b();
            }
        }

        public void flush() throws IOException {
            if (f4644c || !Thread.holdsLock(this.f4647d)) {
                synchronized (this.f4647d) {
                    this.f4647d.m637k();
                }
                while (this.f4648e.m7705a() > 0) {
                    m5138a(false);
                    this.f4647d.f569d.m587b();
                }
                return;
            }
            throw new AssertionError();
        }

        public C1631u timeout() {
            return this.f4647d.f572g;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() throws java.io.IOException {
            /*
            r8 = this;
            r0 = f4644c;
            if (r0 != 0) goto L_0x0012;
        L_0x0004:
            r0 = r8.f4647d;
            r0 = java.lang.Thread.holdsLock(r0);
            if (r0 == 0) goto L_0x0012;
        L_0x000c:
            r0 = new java.lang.AssertionError;
            r0.<init>();
            throw r0;
        L_0x0012:
            r0 = r8.f4647d;
            monitor-enter(r0);
            r1 = r8.f4645a;	 Catch:{ all -> 0x0064 }
            if (r1 == 0) goto L_0x001b;
        L_0x0019:
            monitor-exit(r0);	 Catch:{ all -> 0x0064 }
            return;
        L_0x001b:
            monitor-exit(r0);	 Catch:{ all -> 0x0064 }
            r0 = r8.f4647d;
            r0 = r0.f570e;
            r0 = r0.f4646b;
            r1 = 1;
            if (r0 != 0) goto L_0x004e;
        L_0x0025:
            r0 = r8.f4648e;
            r2 = r0.m7705a();
            r4 = 0;
            r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1));
            if (r0 <= 0) goto L_0x003f;
        L_0x0031:
            r0 = r8.f4648e;
            r2 = r0.m7705a();
            r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1));
            if (r0 <= 0) goto L_0x004e;
        L_0x003b:
            r8.m5138a(r1);
            goto L_0x0031;
        L_0x003f:
            r0 = r8.f4647d;
            r2 = r0.f569d;
            r0 = r8.f4647d;
            r3 = r0.f568c;
            r4 = 1;
            r5 = 0;
            r6 = 0;
            r2.m580a(r3, r4, r5, r6);
        L_0x004e:
            r2 = r8.f4647d;
            monitor-enter(r2);
            r8.f4645a = r1;	 Catch:{ all -> 0x0061 }
            monitor-exit(r2);	 Catch:{ all -> 0x0061 }
            r0 = r8.f4647d;
            r0 = r0.f569d;
            r0.m587b();
            r0 = r8.f4647d;
            r0.m636j();
            return;
        L_0x0061:
            r0 = move-exception;
            monitor-exit(r2);	 Catch:{ all -> 0x0061 }
            throw r0;
        L_0x0064:
            r1 = move-exception;
            monitor-exit(r0);	 Catch:{ all -> 0x0064 }
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: c.a.e.i.a.close():void");
        }
    }

    /* compiled from: Http2Stream */
    private final class C1859b implements C1630t {
        static final /* synthetic */ boolean f4649c = true;
        boolean f4650a;
        boolean f4651b;
        final /* synthetic */ C0503i f4652d;
        private final C2453c f4653e = new C2453c();
        private final C2453c f4654f = new C2453c();
        private final long f4655g;

        static {
            Class cls = C0503i.class;
        }

        C1859b(C0503i c0503i, long j) {
            this.f4652d = c0503i;
            this.f4655g = j;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public long read(p125d.C2453c r8, long r9) throws java.io.IOException {
            /*
            r7 = this;
            r0 = 0;
            r2 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1));
            if (r2 >= 0) goto L_0x001d;
        L_0x0006:
            r8 = new java.lang.IllegalArgumentException;
            r0 = new java.lang.StringBuilder;
            r0.<init>();
            r1 = "byteCount < 0: ";
            r0.append(r1);
            r0.append(r9);
            r9 = r0.toString();
            r8.<init>(r9);
            throw r8;
        L_0x001d:
            r2 = r7.f4652d;
            monitor-enter(r2);
            r7.m5140a();	 Catch:{ all -> 0x00b4 }
            r7.m5141b();	 Catch:{ all -> 0x00b4 }
            r3 = r7.f4654f;	 Catch:{ all -> 0x00b4 }
            r3 = r3.m7705a();	 Catch:{ all -> 0x00b4 }
            r5 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1));
            if (r5 != 0) goto L_0x0034;
        L_0x0030:
            r8 = -1;
            monitor-exit(r2);	 Catch:{ all -> 0x00b4 }
            return r8;
        L_0x0034:
            r3 = r7.f4654f;	 Catch:{ all -> 0x00b4 }
            r4 = r7.f4654f;	 Catch:{ all -> 0x00b4 }
            r4 = r4.m7705a();	 Catch:{ all -> 0x00b4 }
            r9 = java.lang.Math.min(r9, r4);	 Catch:{ all -> 0x00b4 }
            r8 = r3.read(r8, r9);	 Catch:{ all -> 0x00b4 }
            r10 = r7.f4652d;	 Catch:{ all -> 0x00b4 }
            r3 = r10.f566a;	 Catch:{ all -> 0x00b4 }
            r5 = r3 + r8;
            r10.f566a = r5;	 Catch:{ all -> 0x00b4 }
            r10 = r7.f4652d;	 Catch:{ all -> 0x00b4 }
            r3 = r10.f566a;	 Catch:{ all -> 0x00b4 }
            r10 = r7.f4652d;	 Catch:{ all -> 0x00b4 }
            r10 = r10.f569d;	 Catch:{ all -> 0x00b4 }
            r10 = r10.f550l;	 Catch:{ all -> 0x00b4 }
            r10 = r10.m677d();	 Catch:{ all -> 0x00b4 }
            r10 = r10 / 2;
            r5 = (long) r10;	 Catch:{ all -> 0x00b4 }
            r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1));
            if (r10 < 0) goto L_0x0074;
        L_0x0061:
            r10 = r7.f4652d;	 Catch:{ all -> 0x00b4 }
            r10 = r10.f569d;	 Catch:{ all -> 0x00b4 }
            r3 = r7.f4652d;	 Catch:{ all -> 0x00b4 }
            r3 = r3.f568c;	 Catch:{ all -> 0x00b4 }
            r4 = r7.f4652d;	 Catch:{ all -> 0x00b4 }
            r4 = r4.f566a;	 Catch:{ all -> 0x00b4 }
            r10.m575a(r3, r4);	 Catch:{ all -> 0x00b4 }
            r10 = r7.f4652d;	 Catch:{ all -> 0x00b4 }
            r10.f566a = r0;	 Catch:{ all -> 0x00b4 }
        L_0x0074:
            monitor-exit(r2);	 Catch:{ all -> 0x00b4 }
            r10 = r7.f4652d;
            r10 = r10.f569d;
            monitor-enter(r10);
            r2 = r7.f4652d;	 Catch:{ all -> 0x00b1 }
            r2 = r2.f569d;	 Catch:{ all -> 0x00b1 }
            r3 = r2.f548j;	 Catch:{ all -> 0x00b1 }
            r5 = r3 + r8;
            r2.f548j = r5;	 Catch:{ all -> 0x00b1 }
            r2 = r7.f4652d;	 Catch:{ all -> 0x00b1 }
            r2 = r2.f569d;	 Catch:{ all -> 0x00b1 }
            r2 = r2.f548j;	 Catch:{ all -> 0x00b1 }
            r4 = r7.f4652d;	 Catch:{ all -> 0x00b1 }
            r4 = r4.f569d;	 Catch:{ all -> 0x00b1 }
            r4 = r4.f550l;	 Catch:{ all -> 0x00b1 }
            r4 = r4.m677d();	 Catch:{ all -> 0x00b1 }
            r4 = r4 / 2;
            r4 = (long) r4;	 Catch:{ all -> 0x00b1 }
            r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1));
            if (r6 < 0) goto L_0x00af;
        L_0x009b:
            r2 = r7.f4652d;	 Catch:{ all -> 0x00b1 }
            r2 = r2.f569d;	 Catch:{ all -> 0x00b1 }
            r3 = 0;
            r4 = r7.f4652d;	 Catch:{ all -> 0x00b1 }
            r4 = r4.f569d;	 Catch:{ all -> 0x00b1 }
            r4 = r4.f548j;	 Catch:{ all -> 0x00b1 }
            r2.m575a(r3, r4);	 Catch:{ all -> 0x00b1 }
            r2 = r7.f4652d;	 Catch:{ all -> 0x00b1 }
            r2 = r2.f569d;	 Catch:{ all -> 0x00b1 }
            r2.f548j = r0;	 Catch:{ all -> 0x00b1 }
        L_0x00af:
            monitor-exit(r10);	 Catch:{ all -> 0x00b1 }
            return r8;
        L_0x00b1:
            r8 = move-exception;
            monitor-exit(r10);	 Catch:{ all -> 0x00b1 }
            throw r8;
        L_0x00b4:
            r8 = move-exception;
            monitor-exit(r2);	 Catch:{ all -> 0x00b4 }
            throw r8;
            */
            throw new UnsupportedOperationException("Method not decompiled: c.a.e.i.b.read(d.c, long):long");
        }

        private void m5140a() throws IOException {
            this.f4652d.f571f.m6902c();
            while (this.f4654f.m7705a() == 0 && !this.f4651b && !this.f4650a && this.f4652d.f573h == null) {
                try {
                    this.f4652d.m638l();
                } catch (Throwable th) {
                    this.f4652d.f571f.m7016b();
                }
            }
            this.f4652d.f571f.m7016b();
        }

        void m5142a(C2284e c2284e, long j) throws IOException {
            if (f4649c || !Thread.holdsLock(this.f4652d)) {
                while (j > 0) {
                    boolean z;
                    Object obj;
                    Object obj2;
                    synchronized (this.f4652d) {
                        z = this.f4651b;
                        obj = null;
                        obj2 = j + this.f4654f.m7705a() > this.f4655g ? 1 : null;
                    }
                    if (obj2 != null) {
                        c2284e.mo2531i(j);
                        this.f4652d.m626b(C0492b.FLOW_CONTROL_ERROR);
                        return;
                    } else if (z) {
                        c2284e.mo2531i(j);
                        return;
                    } else {
                        long read = c2284e.read(this.f4653e, j);
                        if (read == -1) {
                            throw new EOFException();
                        }
                        long j2 = j - read;
                        synchronized (this.f4652d) {
                            if (this.f4654f.m7705a() == 0) {
                                obj = 1;
                            }
                            this.f4654f.mo2511a(this.f4653e);
                            if (obj != null) {
                                this.f4652d.notifyAll();
                            }
                        }
                        j = j2;
                    }
                }
                return;
            }
            throw new AssertionError();
        }

        public C1631u timeout() {
            return this.f4652d.f571f;
        }

        public void close() throws IOException {
            synchronized (this.f4652d) {
                this.f4650a = f4649c;
                this.f4654f.m7776w();
                this.f4652d.notifyAll();
            }
            this.f4652d.m636j();
        }

        private void m5141b() throws IOException {
            if (this.f4650a) {
                throw new IOException("stream closed");
            } else if (this.f4652d.f573h != null) {
                throw new C0510o(this.f4652d.f573h);
            }
        }
    }

    /* compiled from: Http2Stream */
    class C2364c extends C2282a {
        final /* synthetic */ C0503i f5843a;

        C2364c(C0503i c0503i) {
            this.f5843a = c0503i;
        }

        protected void mo2448a() {
            this.f5843a.m626b(C0492b.CANCEL);
        }

        protected IOException mo2447a(IOException iOException) {
            IOException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        public void m7016b() throws IOException {
            if (h_()) {
                throw mo2447a(null);
            }
        }
    }

    C0503i(int i, C0500g c0500g, boolean z, boolean z2, List<C0493c> list) {
        if (c0500g == null) {
            throw new NullPointerException("connection == null");
        } else if (list == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.f568c = i;
            this.f569d = c0500g;
            this.f567b = (long) c0500g.f551m.m677d();
            this.f577m = new C1859b(this, (long) c0500g.f550l.m677d());
            this.f570e = new C1858a(this);
            this.f577m.f4651b = z2;
            this.f570e.f4646b = z;
            this.f574j = list;
        }
    }

    public int m621a() {
        return this.f568c;
    }

    public synchronized boolean m627b() {
        if (this.f573h != null) {
            return false;
        }
        if ((this.f577m.f4651b || this.f577m.f4650a) && ((this.f570e.f4646b || this.f570e.f4645a) && this.f576l)) {
            return false;
        }
        return f565i;
    }

    public boolean m629c() {
        if (this.f569d.f540b == ((this.f568c & 1) == 1 ? f565i : false)) {
            return f565i;
        }
        return false;
    }

    public synchronized List<C0493c> m630d() throws IOException {
        List<C0493c> list;
        if (m629c()) {
            this.f571f.m6902c();
            while (this.f575k == null && this.f573h == null) {
                try {
                    m638l();
                } finally {
                    this.f571f.m7016b();
                }
            }
            list = this.f575k;
            if (list != null) {
                this.f575k = null;
            } else {
                throw new C0510o(this.f573h);
            }
        }
        throw new IllegalStateException("servers cannot read response headers");
        return list;
    }

    public C1631u m631e() {
        return this.f571f;
    }

    public C1631u m632f() {
        return this.f572g;
    }

    public C1630t m633g() {
        return this.f577m;
    }

    public C1629s m634h() {
        synchronized (this) {
            if (this.f576l || m629c()) {
            } else {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.f570e;
    }

    public void m623a(C0492b c0492b) throws IOException {
        if (m620d(c0492b)) {
            this.f569d.m588b(this.f568c, c0492b);
        }
    }

    public void m626b(C0492b c0492b) {
        if (m620d(c0492b)) {
            this.f569d.m576a(this.f568c, c0492b);
        }
    }

    private boolean m620d(C0492b c0492b) {
        if (f565i || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.f573h != null) {
                    return false;
                } else if (this.f577m.f4651b && this.f570e.f4646b) {
                    return false;
                } else {
                    this.f573h = c0492b;
                    notifyAll();
                    this.f569d.m586b(this.f568c);
                    return f565i;
                }
            }
        }
        throw new AssertionError();
    }

    void m625a(List<C0493c> list) {
        if (f565i || !Thread.holdsLock(this)) {
            boolean z;
            synchronized (this) {
                z = f565i;
                this.f576l = f565i;
                if (this.f575k == null) {
                    this.f575k = list;
                    z = m627b();
                    notifyAll();
                } else {
                    List arrayList = new ArrayList();
                    arrayList.addAll(this.f575k);
                    arrayList.add(null);
                    arrayList.addAll(list);
                    this.f575k = arrayList;
                }
            }
            if (!z) {
                this.f569d.m586b(this.f568c);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    void m624a(C2284e c2284e, int i) throws IOException {
        if (f565i || !Thread.holdsLock(this)) {
            this.f577m.m5142a(c2284e, (long) i);
            return;
        }
        throw new AssertionError();
    }

    void m635i() {
        if (f565i || !Thread.holdsLock(this)) {
            boolean b;
            synchronized (this) {
                this.f577m.f4651b = f565i;
                b = m627b();
                notifyAll();
            }
            if (!b) {
                this.f569d.m586b(this.f568c);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    synchronized void m628c(C0492b c0492b) {
        if (this.f573h == null) {
            this.f573h = c0492b;
            notifyAll();
        }
    }

    void m636j() throws IOException {
        if (f565i || !Thread.holdsLock(this)) {
            Object obj;
            boolean b;
            synchronized (this) {
                obj = (!this.f577m.f4651b && this.f577m.f4650a && (this.f570e.f4646b || this.f570e.f4645a)) ? 1 : null;
                b = m627b();
            }
            if (obj != null) {
                m623a(C0492b.CANCEL);
                return;
            } else if (!b) {
                this.f569d.m586b(this.f568c);
                return;
            } else {
                return;
            }
        }
        throw new AssertionError();
    }

    void m622a(long j) {
        this.f567b += j;
        if (j > 0) {
            notifyAll();
        }
    }

    void m637k() throws IOException {
        if (this.f570e.f4645a) {
            throw new IOException("stream closed");
        } else if (this.f570e.f4646b) {
            throw new IOException("stream finished");
        } else if (this.f573h != null) {
            throw new C0510o(this.f573h);
        }
    }

    void m638l() throws java.io.InterruptedIOException {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r1 = this;
        r1.wait();	 Catch:{ InterruptedException -> 0x0004 }
        return;
    L_0x0004:
        r0 = new java.io.InterruptedIOException;
        r0.<init>();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.e.i.l():void");
    }
}
