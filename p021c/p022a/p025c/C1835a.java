package p021c.p022a.p025c;

import java.io.IOException;
import java.util.List;
import p008b.p009a.p010a.p011a.p012a.p014b.C0356a;
import p021c.C0545l;
import p021c.C0546m;
import p021c.C0559u;
import p021c.C0559u.C0558a;
import p021c.C0560v;
import p021c.aa;
import p021c.aa.C0522a;
import p021c.ab;
import p021c.ac;
import p021c.ac.C0523a;
import p021c.p022a.C0488c;
import p021c.p022a.C0490d;
import p125d.C1625m;
import p125d.C1630t;
import p125d.C2289k;

/* compiled from: BridgeInterceptor */
public final class C1835a implements C0559u {
    private final C0546m f4552a;

    public C1835a(C0546m c0546m) {
        this.f4552a = c0546m;
    }

    public ac mo1270a(C0558a c0558a) throws IOException {
        aa a = c0558a.mo1275a();
        C0522a e = a.m756e();
        ab d = a.m755d();
        if (d != null) {
            C0560v contentType = d.contentType();
            if (contentType != null) {
                e.m747a("Content-Type", contentType.toString());
            }
            long contentLength = d.contentLength();
            if (contentLength != -1) {
                e.m747a("Content-Length", Long.toString(contentLength));
                e.m749b("Transfer-Encoding");
            } else {
                e.m747a("Transfer-Encoding", "chunked");
                e.m749b("Content-Length");
            }
        }
        boolean z = false;
        if (a.m752a("Host") == null) {
            e.m747a("Host", C0488c.m508a(a.m751a(), false));
        }
        if (a.m752a("Connection") == null) {
            e.m747a("Connection", "Keep-Alive");
        }
        if (a.m752a("Accept-Encoding") == null && a.m752a("Range") == null) {
            z = true;
            e.m747a("Accept-Encoding", "gzip");
        }
        List a2 = this.f4552a.mo1331a(a.m751a());
        if (!a2.isEmpty()) {
            e.m747a("Cookie", m5064a(a2));
        }
        if (a.m752a(C0356a.HEADER_USER_AGENT) == null) {
            e.m747a(C0356a.HEADER_USER_AGENT, C0490d.m533a());
        }
        c0558a = c0558a.mo1276a(e.m748a());
        C0483e.m489a(this.f4552a, a.m751a(), c0558a.m782f());
        C0523a a3 = c0558a.m784h().m763a(a);
        if (z && "gzip".equalsIgnoreCase(c0558a.m776a("Content-Encoding")) && C0483e.m491b(c0558a)) {
            C1630t c2289k = new C2289k(c0558a.m783g().source());
            a3.m767a(c0558a.m782f().m923b().m915b("Content-Encoding").m915b("Content-Length").m914a());
            a3.m765a(new C1838h(c0558a.m776a("Content-Type"), -1, C1625m.m4841a(c2289k)));
        }
        return a3.m771a();
    }

    private String m5064a(List<C0545l> list) {
        StringBuilder stringBuilder = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                stringBuilder.append("; ");
            }
            C0545l c0545l = (C0545l) list.get(i);
            stringBuilder.append(c0545l.m867a());
            stringBuilder.append('=');
            stringBuilder.append(c0545l.m869b());
        }
        return stringBuilder.toString();
    }
}
