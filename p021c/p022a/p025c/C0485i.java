package p021c.p022a.p025c;

import java.net.Proxy.Type;
import p021c.C0557t;
import p021c.aa;

/* compiled from: RequestLine */
public final class C0485i {
    public static String m497a(aa aaVar, Type type) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(aaVar.m753b());
        stringBuilder.append(' ');
        if (C0485i.m499b(aaVar, type) != null) {
            stringBuilder.append(aaVar.m751a());
        } else {
            stringBuilder.append(C0485i.m498a(aaVar.m751a()));
        }
        stringBuilder.append(" HTTP/1.1");
        return stringBuilder.toString();
    }

    private static boolean m499b(aa aaVar, Type type) {
        return (aaVar.m758g() == null && type == Type.HTTP) ? true : null;
    }

    public static String m498a(C0557t c0557t) {
        String h = c0557t.m971h();
        c0557t = c0557t.m974k();
        if (c0557t == null) {
            return h;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(h);
        stringBuilder.append('?');
        stringBuilder.append(c0557t);
        return stringBuilder.toString();
    }
}
