package p021c.p022a.p025c;

import java.io.IOException;
import java.net.ProtocolException;
import p021c.C0559u;
import p021c.C0559u.C0558a;
import p021c.aa;
import p021c.ac;
import p021c.ac.C0523a;
import p021c.p022a.C0488c;
import p021c.p022a.p024b.C0476g;
import p021c.p022a.p024b.C1834c;
import p125d.C1625m;
import p125d.C1629s;
import p125d.C2283d;
import p125d.C2286h;
import p125d.C2453c;

/* compiled from: CallServerInterceptor */
public final class C1836b implements C0559u {
    private final boolean f4553a;

    /* compiled from: CallServerInterceptor */
    static final class C2359a extends C2286h {
        long f5831a;

        C2359a(C1629s c1629s) {
            super(c1629s);
        }

        public void mo1284a(C2453c c2453c, long j) throws IOException {
            super.mo1284a(c2453c, j);
            this.f5831a += j;
        }
    }

    public C1836b(boolean z) {
        this.f4553a = z;
    }

    public ac mo1270a(C0558a c0558a) throws IOException {
        ac a;
        C1837g c1837g = (C1837g) c0558a;
        C0480c g = c1837g.m5075g();
        C0476g f = c1837g.m5074f();
        C1834c c1834c = (C1834c) c1837g.mo1277b();
        aa a2 = c1837g.mo1275a();
        long currentTimeMillis = System.currentTimeMillis();
        c1837g.m5077i().m900c(c1837g.m5076h());
        g.mo1292a(a2);
        c1837g.m5077i().m887a(c1837g.m5076h(), a2);
        C0523a c0523a = null;
        if (C0484f.m494c(a2.m753b()) && a2.m755d() != null) {
            if ("100-continue".equalsIgnoreCase(a2.m752a("Expect"))) {
                g.mo1291a();
                c1837g.m5077i().m902e(c1837g.m5076h());
                c0523a = g.mo1288a(true);
            }
            if (c0523a == null) {
                c1837g.m5077i().m901d(c1837g.m5076h());
                C1629s c2359a = new C2359a(g.mo1290a(a2, a2.m755d().contentLength()));
                C2283d a3 = C1625m.m4840a(c2359a);
                a2.m755d().writeTo(a3);
                a3.close();
                c1837g.m5077i().m886a(c1837g.m5076h(), c2359a.f5831a);
            } else if (!c1834c.m5063f()) {
                f.m469d();
            }
        }
        g.mo1293b();
        if (c0523a == null) {
            c1837g.m5077i().m902e(c1837g.m5076h());
            c0523a = g.mo1288a(false);
        }
        ac a4 = c0523a.m763a(a2).m766a(f.m467b().m5062e()).m762a(currentTimeMillis).m772b(System.currentTimeMillis()).m771a();
        c1837g.m5077i().m888a(c1837g.m5076h(), a4);
        c0558a = a4.m778b();
        if (this.f4553a && c0558a == 101) {
            a = a4.m784h().m765a(C0488c.f473c).m771a();
        } else {
            a = a4.m784h().m765a(g.mo1289a(a4)).m771a();
        }
        if ("close".equalsIgnoreCase(a.m775a().m752a("Connection")) || "close".equalsIgnoreCase(a.m776a("Connection"))) {
            f.m469d();
        }
        if ((c0558a != 204 && c0558a != 205) || a.m783g().contentLength() <= 0) {
            return a;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("HTTP ");
        stringBuilder.append(c0558a);
        stringBuilder.append(" had non-zero Content-Length: ");
        stringBuilder.append(a.m783g().contentLength());
        throw new ProtocolException(stringBuilder.toString());
    }
}
