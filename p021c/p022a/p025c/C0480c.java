package p021c.p022a.p025c;

import java.io.IOException;
import p021c.aa;
import p021c.ac;
import p021c.ac.C0523a;
import p021c.ad;
import p125d.C1629s;

/* compiled from: HttpCodec */
public interface C0480c {
    C0523a mo1288a(boolean z) throws IOException;

    ad mo1289a(ac acVar) throws IOException;

    C1629s mo1290a(aa aaVar, long j);

    void mo1291a() throws IOException;

    void mo1292a(aa aaVar) throws IOException;

    void mo1293b() throws IOException;

    void mo1294c();
}
