package p021c.p022a.p025c;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import p021c.p022a.C0488c;

/* compiled from: HttpDate */
public final class C0482d {
    private static final ThreadLocal<DateFormat> f464a = new C04811();
    private static final String[] f465b = new String[]{"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z", "EEE MMM d yyyy HH:mm:ss z"};
    private static final DateFormat[] f466c = new DateFormat[f465b.length];

    /* compiled from: HttpDate */
    class C04811 extends ThreadLocal<DateFormat> {
        C04811() {
        }

        protected /* synthetic */ Object initialValue() {
            return m481a();
        }

        protected DateFormat m481a() {
            DateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
            simpleDateFormat.setLenient(false);
            simpleDateFormat.setTimeZone(C0488c.f477g);
            return simpleDateFormat;
        }
    }

    public static Date m483a(String str) {
        if (str.length() == 0) {
            return null;
        }
        ParsePosition parsePosition = new ParsePosition(0);
        Date parse = ((DateFormat) f464a.get()).parse(str, parsePosition);
        if (parsePosition.getIndex() == str.length()) {
            return parse;
        }
        synchronized (f465b) {
            int length = f465b.length;
            for (int i = 0; i < length; i++) {
                DateFormat dateFormat = f466c[i];
                if (dateFormat == null) {
                    dateFormat = new SimpleDateFormat(f465b[i], Locale.US);
                    dateFormat.setTimeZone(C0488c.f477g);
                    f466c[i] = dateFormat;
                }
                parsePosition.setIndex(0);
                Date parse2 = dateFormat.parse(str, parsePosition);
                if (parsePosition.getIndex() != 0) {
                    return parse2;
                }
            }
            return null;
        }
    }

    public static String m482a(Date date) {
        return ((DateFormat) f464a.get()).format(date);
    }
}
