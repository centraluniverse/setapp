package p021c.p022a.p025c;

/* compiled from: HttpMethod */
public final class C0484f {
    public static boolean m492a(String str) {
        if (!(str.equals("POST") || str.equals("PATCH") || str.equals("PUT") || str.equals("DELETE"))) {
            if (str.equals("MOVE") == null) {
                return null;
            }
        }
        return true;
    }

    public static boolean m493b(String str) {
        if (!(str.equals("POST") || str.equals("PUT") || str.equals("PATCH") || str.equals("PROPPATCH"))) {
            if (str.equals("REPORT") == null) {
                return null;
            }
        }
        return true;
    }

    public static boolean m494c(String str) {
        if (!(C0484f.m493b(str) || str.equals("OPTIONS") || str.equals("DELETE") || str.equals("PROPFIND") || str.equals("MKCOL"))) {
            if (str.equals("LOCK") == null) {
                return null;
            }
        }
        return true;
    }

    public static boolean m495d(String str) {
        return str.equals("PROPFIND");
    }

    public static boolean m496e(String str) {
        return str.equals("PROPFIND") ^ 1;
    }
}
