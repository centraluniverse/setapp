package p021c.p022a.p025c;

import javax.annotation.Nullable;
import p021c.C0560v;
import p021c.ad;
import p125d.C2284e;

/* compiled from: RealResponseBody */
public final class C1838h extends ad {
    @Nullable
    private final String f4566a;
    private final long f4567b;
    private final C2284e f4568c;

    public C1838h(@Nullable String str, long j, C2284e c2284e) {
        this.f4566a = str;
        this.f4567b = j;
        this.f4568c = c2284e;
    }

    public C0560v contentType() {
        return this.f4566a != null ? C0560v.m986a(this.f4566a) : null;
    }

    public long contentLength() {
        return this.f4567b;
    }

    public C2284e source() {
        return this.f4568c;
    }
}
