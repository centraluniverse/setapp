package p021c.p022a.p025c;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.HttpRetryException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import p021c.C0521a;
import p021c.C0532e;
import p021c.C0536g;
import p021c.C0539i;
import p021c.C0550p;
import p021c.C0557t;
import p021c.C0559u;
import p021c.C0559u.C0558a;
import p021c.C1883x;
import p021c.aa;
import p021c.aa.C0522a;
import p021c.ab;
import p021c.ac;
import p021c.ae;
import p021c.p022a.C0488c;
import p021c.p022a.p024b.C0472e;
import p021c.p022a.p024b.C0476g;
import p021c.p022a.p027e.C0491a;

/* compiled from: RetryAndFollowUpInterceptor */
public final class C1839j implements C0559u {
    private final C1883x f4569a;
    private final boolean f4570b;
    private C0476g f4571c;
    private Object f4572d;
    private volatile boolean f4573e;

    public C1839j(C1883x c1883x, boolean z) {
        this.f4569a = c1883x;
        this.f4570b = z;
    }

    public void m5084a() {
        this.f4573e = true;
        C0476g c0476g = this.f4571c;
        if (c0476g != null) {
            c0476g.m470e();
        }
    }

    public boolean m5086b() {
        return this.f4573e;
    }

    public void m5085a(Object obj) {
        this.f4572d = obj;
    }

    public ac mo1270a(C0558a c0558a) throws IOException {
        aa a = c0558a.mo1275a();
        C1837g c1837g = (C1837g) c0558a;
        C0532e h = c1837g.m5076h();
        C0550p i = c1837g.m5077i();
        this.f4571c = new C0476g(this.f4569a.m5216p(), m5078a(a.m751a()), h, i, this.f4572d);
        int i2 = 0;
        ac acVar = null;
        while (!this.f4573e) {
            try {
                ac a2 = c1837g.m5069a(a, this.f4571c, null, null);
                ac a3 = acVar != null ? a2.m784h().m774c(acVar.m784h().m765a(null).m771a()).m771a() : a2;
                aa a4 = m5079a(a3);
                if (a4 == null) {
                    if (this.f4570b == null) {
                        this.f4571c.m468c();
                    }
                    return a3;
                }
                C0488c.m517a(a3.m783g());
                int i3 = i2 + 1;
                if (i3 > 20) {
                    this.f4571c.m468c();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Too many follow-up requests: ");
                    stringBuilder.append(i3);
                    throw new ProtocolException(stringBuilder.toString());
                } else if (a4.m755d() instanceof C0487l) {
                    this.f4571c.m468c();
                    throw new HttpRetryException("Cannot retry streamed HTTP body", a3.m778b());
                } else {
                    if (!m5080a(a3, a4.m751a())) {
                        this.f4571c.m468c();
                        this.f4571c = new C0476g(this.f4569a.m5216p(), m5078a(a4.m751a()), h, i, this.f4572d);
                    } else if (this.f4571c.m461a() != null) {
                        StringBuilder stringBuilder2 = new StringBuilder();
                        stringBuilder2.append("Closing the body of ");
                        stringBuilder2.append(a3);
                        stringBuilder2.append(" didn't close its backing stream. Bad interceptor?");
                        throw new IllegalStateException(stringBuilder2.toString());
                    }
                    acVar = a3;
                    a = a4;
                    i2 = i3;
                }
            } catch (C0472e e) {
                if (!m5082a(e.m442a(), false, a)) {
                    throw e.m442a();
                }
            } catch (IOException e2) {
                if (!m5082a(e2, !(e2 instanceof C0491a), a)) {
                    throw e2;
                }
            } catch (Throwable th) {
                this.f4571c.m465a(null);
                this.f4571c.m468c();
            }
        }
        this.f4571c.m468c();
        throw new IOException("Canceled");
    }

    private C0521a m5078a(C0557t c0557t) {
        HostnameVerifier l;
        SSLSocketFactory sSLSocketFactory;
        C0536g m;
        C1839j c1839j = this;
        if (c0557t.m965c()) {
            SSLSocketFactory k = c1839j.f4569a.m5211k();
            l = c1839j.f4569a.m5212l();
            sSLSocketFactory = k;
            m = c1839j.f4569a.m5213m();
        } else {
            sSLSocketFactory = null;
            l = sSLSocketFactory;
            m = l;
        }
        return new C0521a(c0557t.m969f(), c0557t.m970g(), c1839j.f4569a.m5209i(), c1839j.f4569a.m5210j(), sSLSocketFactory, l, m, c1839j.f4569a.m5215o(), c1839j.f4569a.m5204d(), c1839j.f4569a.m5221u(), c1839j.f4569a.m5222v(), c1839j.f4569a.m5205e());
    }

    private boolean m5082a(IOException iOException, boolean z, aa aaVar) {
        this.f4571c.m465a(iOException);
        if (!this.f4569a.m5219s()) {
            return false;
        }
        if ((z && (aaVar.m755d() instanceof C0487l) != null) || m5081a(iOException, z) == null || this.f4571c.m471f() == null) {
            return false;
        }
        return true;
    }

    private boolean m5081a(IOException iOException, boolean z) {
        boolean z2 = false;
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (iOException instanceof InterruptedIOException) {
            if (!((iOException instanceof SocketTimeoutException) == null || z)) {
                z2 = true;
            }
            return z2;
        } else if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && (iOException instanceof SSLPeerUnverifiedException) == null) {
            return true;
        } else {
            return false;
        }
    }

    private aa m5079a(ac acVar) throws IOException {
        if (acVar == null) {
            throw new IllegalStateException();
        }
        C0539i b = this.f4571c.m467b();
        ab abVar = null;
        ae a = b != null ? b.mo1271a() : null;
        int b2 = acVar.m778b();
        String b3 = acVar.m775a().m753b();
        switch (b2) {
            case 300:
            case 301:
            case 302:
            case 303:
                break;
            case 307:
            case 308:
                if (!(b3.equals("GET") || b3.equals("HEAD"))) {
                    return null;
                }
            case 401:
                return this.f4569a.m5214n().mo1329a(a, acVar);
            case 407:
                Proxy b4;
                if (a != null) {
                    b4 = a.m791b();
                } else {
                    b4 = this.f4569a.m5204d();
                }
                if (b4.type() == Type.HTTP) {
                    return this.f4569a.m5215o().mo1329a(a, acVar);
                }
                throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
            case 408:
                if (!this.f4569a.m5219s() || (acVar.m775a().m755d() instanceof C0487l)) {
                    return null;
                }
                if (acVar.m786j() == null || acVar.m786j().m778b() != 408) {
                    return acVar.m775a();
                }
                return null;
            default:
                return null;
        }
        if (!this.f4569a.m5218r()) {
            return null;
        }
        String a2 = acVar.m776a("Location");
        if (a2 == null) {
            return null;
        }
        C0557t c = acVar.m775a().m751a().m964c(a2);
        if (c == null) {
            return null;
        }
        if (!c.m963b().equals(acVar.m775a().m751a().m963b()) && !this.f4569a.m5217q()) {
            return null;
        }
        C0522a e = acVar.m775a().m756e();
        if (C0484f.m494c(b3)) {
            boolean d = C0484f.m495d(b3);
            if (C0484f.m496e(b3)) {
                e.m746a("GET", null);
            } else {
                if (d) {
                    abVar = acVar.m775a().m755d();
                }
                e.m746a(b3, abVar);
            }
            if (!d) {
                e.m749b("Transfer-Encoding");
                e.m749b("Content-Length");
                e.m749b("Content-Type");
            }
        }
        if (m5080a(acVar, c) == null) {
            e.m749b("Authorization");
        }
        return e.m744a(c).m748a();
    }

    private boolean m5080a(ac acVar, C0557t c0557t) {
        acVar = acVar.m775a().m751a();
        return (acVar.m969f().equals(c0557t.m969f()) && acVar.m970g() == c0557t.m970g() && acVar.m963b().equals(c0557t.m963b()) != null) ? true : null;
    }
}
