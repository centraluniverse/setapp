package p021c.p022a.p025c;

import java.io.IOException;
import java.util.List;
import p021c.C0532e;
import p021c.C0539i;
import p021c.C0550p;
import p021c.C0559u;
import p021c.C0559u.C0558a;
import p021c.aa;
import p021c.ac;
import p021c.p022a.p024b.C0476g;
import p021c.p022a.p024b.C1834c;

/* compiled from: RealInterceptorChain */
public final class C1837g implements C0558a {
    private final List<C0559u> f4554a;
    private final C0476g f4555b;
    private final C0480c f4556c;
    private final C1834c f4557d;
    private final int f4558e;
    private final aa f4559f;
    private final C0532e f4560g;
    private final C0550p f4561h;
    private final int f4562i;
    private final int f4563j;
    private final int f4564k;
    private int f4565l;

    public C1837g(List<C0559u> list, C0476g c0476g, C0480c c0480c, C1834c c1834c, int i, aa aaVar, C0532e c0532e, C0550p c0550p, int i2, int i3, int i4) {
        this.f4554a = list;
        this.f4557d = c1834c;
        this.f4555b = c0476g;
        this.f4556c = c0480c;
        this.f4558e = i;
        this.f4559f = aaVar;
        this.f4560g = c0532e;
        this.f4561h = c0550p;
        this.f4562i = i2;
        this.f4563j = i3;
        this.f4564k = i4;
    }

    public C0539i mo1277b() {
        return this.f4557d;
    }

    public int mo1278c() {
        return this.f4562i;
    }

    public int mo1279d() {
        return this.f4563j;
    }

    public int mo1280e() {
        return this.f4564k;
    }

    public C0476g m5074f() {
        return this.f4555b;
    }

    public C0480c m5075g() {
        return this.f4556c;
    }

    public C0532e m5076h() {
        return this.f4560g;
    }

    public C0550p m5077i() {
        return this.f4561h;
    }

    public aa mo1275a() {
        return this.f4559f;
    }

    public ac mo1276a(aa aaVar) throws IOException {
        return m5069a(aaVar, this.f4555b, this.f4556c, this.f4557d);
    }

    public ac m5069a(aa aaVar, C0476g c0476g, C0480c c0480c, C1834c c1834c) throws IOException {
        if (this.f4558e >= this.f4554a.size()) {
            throw new AssertionError();
        }
        r0.f4565l++;
        StringBuilder stringBuilder;
        if (r0.f4556c != null && !r0.f4557d.m5057a(aaVar.m751a())) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("network interceptor ");
            stringBuilder.append(r0.f4554a.get(r0.f4558e - 1));
            stringBuilder.append(" must retain the same host and port");
            throw new IllegalStateException(stringBuilder.toString());
        } else if (r0.f4556c == null || r0.f4565l <= 1) {
            C1837g c1837g = new C1837g(r0.f4554a, c0476g, c0480c, c1834c, r0.f4558e + 1, aaVar, r0.f4560g, r0.f4561h, r0.f4562i, r0.f4563j, r0.f4564k);
            C0559u c0559u = (C0559u) r0.f4554a.get(r0.f4558e);
            ac a = c0559u.mo1270a(c1837g);
            StringBuilder stringBuilder2;
            if (c0480c != null && r0.f4558e + 1 < r0.f4554a.size() && c1837g.f4565l != 1) {
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("network interceptor ");
                stringBuilder2.append(c0559u);
                stringBuilder2.append(" must call proceed() exactly once");
                throw new IllegalStateException(stringBuilder2.toString());
            } else if (a == null) {
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("interceptor ");
                stringBuilder2.append(c0559u);
                stringBuilder2.append(" returned null");
                throw new NullPointerException(stringBuilder2.toString());
            } else if (a.m783g() != null) {
                return a;
            } else {
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("interceptor ");
                stringBuilder2.append(c0559u);
                stringBuilder2.append(" returned a response with no body");
                throw new IllegalStateException(stringBuilder2.toString());
            }
        } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("network interceptor ");
            stringBuilder.append(r0.f4554a.get(r0.f4558e - 1));
            stringBuilder.append(" must call proceed() exactly once");
            throw new IllegalStateException(stringBuilder.toString());
        }
    }
}
