package p021c.p022a.p024b;

import java.io.IOException;
import p021c.C0559u;
import p021c.C0559u.C0558a;
import p021c.C1883x;
import p021c.aa;
import p021c.ac;
import p021c.p022a.p025c.C1837g;

/* compiled from: ConnectInterceptor */
public final class C1833a implements C0559u {
    public final C1883x f4537a;

    public C1833a(C1883x c1883x) {
        this.f4537a = c1883x;
    }

    public ac mo1270a(C0558a c0558a) throws IOException {
        C1837g c1837g = (C1837g) c0558a;
        aa a = c1837g.mo1275a();
        C0476g f = c1837g.m5074f();
        return c1837g.m5069a(a, f, f.m462a(this.f4537a, c0558a, a.m753b().equals("GET") ^ 1), f.m467b());
    }
}
