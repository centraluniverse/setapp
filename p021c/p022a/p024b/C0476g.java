package p021c.p022a.p024b;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.Socket;
import p021c.C0521a;
import p021c.C0532e;
import p021c.C0539i;
import p021c.C0541j;
import p021c.C0550p;
import p021c.C0559u.C0558a;
import p021c.C1883x;
import p021c.ae;
import p021c.p022a.C0469a;
import p021c.p022a.C0488c;
import p021c.p022a.p024b.C0474f.C0473a;
import p021c.p022a.p025c.C0480c;
import p021c.p022a.p027e.C0491a;
import p021c.p022a.p027e.C0492b;
import p021c.p022a.p027e.C0510o;

/* compiled from: StreamAllocation */
public final class C0476g {
    static final /* synthetic */ boolean f446d = true;
    public final C0521a f447a;
    public final C0532e f448b;
    public final C0550p f449c;
    private C0473a f450e;
    private ae f451f;
    private final C0541j f452g;
    private final Object f453h;
    private final C0474f f454i;
    private int f455j;
    private C1834c f456k;
    private boolean f457l;
    private boolean f458m;
    private boolean f459n;
    private C0480c f460o;

    /* compiled from: StreamAllocation */
    public static final class C0475a extends WeakReference<C0476g> {
        public final Object f445a;

        C0475a(C0476g c0476g, Object obj) {
            super(c0476g);
            this.f445a = obj;
        }
    }

    public C0476g(C0541j c0541j, C0521a c0521a, C0532e c0532e, C0550p c0550p, Object obj) {
        this.f452g = c0541j;
        this.f447a = c0521a;
        this.f448b = c0532e;
        this.f449c = c0550p;
        this.f454i = new C0474f(c0521a, m460h(), c0532e, c0550p);
        this.f453h = obj;
    }

    public C0480c m462a(C1883x c1883x, C0558a c0558a, boolean z) {
        try {
            c1883x = m456a(c0558a.mo1278c(), c0558a.mo1279d(), c0558a.mo1280e(), c1883x.m5219s(), z).m5051a(c1883x, c0558a, this);
            synchronized (this.f452g) {
                this.f460o = c1883x;
            }
            return c1883x;
        } catch (C1883x c1883x2) {
            throw new C0472e(c1883x2);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private p021c.p022a.p024b.C1834c m456a(int r4, int r5, int r6, boolean r7, boolean r8) throws java.io.IOException {
        /*
        r3 = this;
    L_0x0000:
        r0 = r3.m455a(r4, r5, r6, r7);
        r1 = r3.f452g;
        monitor-enter(r1);
        r2 = r0.f4539b;	 Catch:{ all -> 0x0019 }
        if (r2 != 0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r1);	 Catch:{ all -> 0x0019 }
        return r0;
    L_0x000d:
        monitor-exit(r1);	 Catch:{ all -> 0x0019 }
        r1 = r0.m5058a(r8);
        if (r1 != 0) goto L_0x0018;
    L_0x0014:
        r3.m469d();
        goto L_0x0000;
    L_0x0018:
        return r0;
    L_0x0019:
        r4 = move-exception;
        monitor-exit(r1);	 Catch:{ all -> 0x0019 }
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.b.g.a(int, int, int, boolean, boolean):c.a.b.c");
    }

    private C1834c m455a(int i, int i2, int i3, boolean z) throws IOException {
        Socket g;
        int i4;
        C0539i c0539i;
        ae aeVar;
        boolean z2;
        synchronized (this.f452g) {
            Socket socket;
            C1834c c1834c;
            try {
                if (r1.f458m) {
                    throw new IllegalStateException("released");
                } else if (r1.f460o != null) {
                    throw new IllegalStateException("codec != null");
                } else if (r1.f459n) {
                    throw new IOException("Canceled");
                } else {
                    c1834c = r1.f456k;
                    g = m459g();
                    i4 = r1.f456k;
                    socket = null;
                    if (i4 != null) {
                        c1834c = r1.f456k;
                    } else {
                        i4 = c1834c;
                        c1834c = null;
                    }
                    if (!r1.f457l) {
                        i4 = socket;
                    }
                    boolean z3 = false;
                    if (c1834c == null) {
                        C0469a c0469a = C0469a.f427a;
                        C0541j c0541j = r1.f452g;
                        int i5 = r1.f447a;
                        c0469a.mo1336a(c0541j, i5, r1, socket);
                        if (r1.f456k != null) {
                            c0539i = r1.f456k;
                            aeVar = socket;
                            z2 = f446d;
                        } else {
                            aeVar = r1.f451f;
                            c0539i = c1834c;
                        }
                    } else {
                        c0539i = c1834c;
                        aeVar = socket;
                    }
                    z2 = false;
                }
            } finally {
                boolean z4 = 
/*
Method generation error in method: c.a.b.g.a(int, int, int, boolean):c.a.b.c
jadx.core.utils.exceptions.CodegenException: Error generate insn: ?: MERGE  (r0_7 'z4' boolean) = (r0_6 'z4' boolean), (r6_5 boolean) in method: c.a.b.g.a(int, int, int, boolean):c.a.b.c
	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:226)
	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:203)
	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:100)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:50)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:93)
	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:297)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:63)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:93)
	at jadx.core.codegen.RegionGen.makeSynchronizedRegion(RegionGen.java:227)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:183)
	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:328)
	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:265)
	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:228)
	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:118)
	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:83)
	at jadx.core.codegen.CodeGen.visit(CodeGen.java:19)
	at jadx.core.ProcessClass.process(ProcessClass.java:43)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
Caused by: jadx.core.utils.exceptions.CodegenException: MERGE can be used only in fallback mode
	at jadx.core.codegen.InsnGen.fallbackOnlyInsn(InsnGen.java:530)
	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:514)
	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
	... 28 more

*/

                private Socket m459g() {
                    if (f446d || Thread.holdsLock(this.f452g)) {
                        C1834c c1834c = this.f456k;
                        return (c1834c == null || !c1834c.f4538a) ? null : m457a(false, false, (boolean) f446d);
                    } else {
                        throw new AssertionError();
                    }
                }

                public void m466a(boolean z, C0480c c0480c, long j, IOException iOException) {
                    Socket a;
                    this.f449c.m898b(this.f448b, j);
                    synchronized (this.f452g) {
                        if (c0480c != null) {
                            if (c0480c == this.f460o) {
                                if (!z) {
                                    C1834c c1834c = this.f456k;
                                    c1834c.f4539b++;
                                }
                                C0539i c0539i = this.f456k;
                                a = m457a(z, false, (boolean) f446d);
                                if (this.f456k != null) {
                                    c0539i = null;
                                }
                                c0480c = this.f458m;
                            }
                        }
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("expected ");
                        stringBuilder.append(this.f460o);
                        stringBuilder.append(" but was ");
                        stringBuilder.append(c0480c);
                        throw new IllegalStateException(stringBuilder.toString());
                    }
                    C0488c.m518a(a);
                    if (c0539i != null) {
                        this.f449c.m899b(this.f448b, c0539i);
                    }
                    if (iOException != null) {
                        this.f449c.m891a(this.f448b, iOException);
                    } else if (c0480c != null) {
                        this.f449c.m904g(this.f448b);
                    }
                }

                public C0480c m461a() {
                    C0480c c0480c;
                    synchronized (this.f452g) {
                        c0480c = this.f460o;
                    }
                    return c0480c;
                }

                private C0471d m460h() {
                    return C0469a.f427a.mo1337a(this.f452g);
                }

                public synchronized C1834c m467b() {
                    return this.f456k;
                }

                public void m468c() {
                    Socket a;
                    synchronized (this.f452g) {
                        C0539i c0539i = this.f456k;
                        a = m457a(false, (boolean) f446d, false);
                        if (this.f456k != null) {
                            c0539i = null;
                        }
                    }
                    C0488c.m518a(a);
                    if (c0539i != null) {
                        this.f449c.m899b(this.f448b, c0539i);
                    }
                }

                public void m469d() {
                    Socket a;
                    synchronized (this.f452g) {
                        C0539i c0539i = this.f456k;
                        a = m457a((boolean) f446d, false, false);
                        if (this.f456k != null) {
                            c0539i = null;
                        }
                    }
                    C0488c.m518a(a);
                    if (c0539i != null) {
                        this.f449c.m899b(this.f448b, c0539i);
                    }
                }

                private Socket m457a(boolean z, boolean z2, boolean z3) {
                    if (f446d || Thread.holdsLock(this.f452g)) {
                        if (z3) {
                            this.f460o = null;
                        }
                        if (z2) {
                            this.f458m = f446d;
                        }
                        if (this.f456k) {
                            if (z) {
                                this.f456k.f4538a = f446d;
                            }
                            if (!this.f460o && (this.f458m || this.f456k.f4538a)) {
                                m458b(this.f456k);
                                if (this.f456k.f4541d.isEmpty()) {
                                    this.f456k.f4542e = System.nanoTime();
                                    if (C0469a.f427a.mo1343a(this.f452g, this.f456k)) {
                                        z = this.f456k.m5061d();
                                        this.f456k = null;
                                        return z;
                                    }
                                }
                                z = false;
                                this.f456k = null;
                                return z;
                            }
                        }
                        return false;
                    }
                    throw new AssertionError();
                }

                public void m470e() {
                    synchronized (this.f452g) {
                        this.f459n = f446d;
                        C0480c c0480c = this.f460o;
                        C1834c c1834c = this.f456k;
                    }
                    if (c0480c != null) {
                        c0480c.mo1294c();
                    } else if (c1834c != null) {
                        c1834c.m5060c();
                    }
                }

                public void m465a(IOException iOException) {
                    C0539i c0539i;
                    Socket a;
                    synchronized (this.f452g) {
                        boolean z;
                        if (iOException instanceof C0510o) {
                            C0510o c0510o = (C0510o) iOException;
                            if (c0510o.f598a == C0492b.REFUSED_STREAM) {
                                this.f455j++;
                            }
                            if (c0510o.f598a != C0492b.REFUSED_STREAM || this.f455j > 1) {
                                this.f451f = null;
                            }
                            z = null;
                            c0539i = this.f456k;
                            a = m457a(z, false, (boolean) f446d);
                            if (!(this.f456k == null && this.f457l)) {
                                c0539i = null;
                            }
                        } else {
                            if (this.f456k != null && (!this.f456k.m5063f() || (iOException instanceof C0491a))) {
                                if (this.f456k.f4539b == 0) {
                                    if (!(this.f451f == null || iOException == null)) {
                                        this.f454i.m452a(this.f451f, iOException);
                                    }
                                    this.f451f = null;
                                }
                            }
                            z = null;
                            c0539i = this.f456k;
                            a = m457a(z, false, (boolean) f446d);
                            c0539i = null;
                        }
                        z = 1;
                        c0539i = this.f456k;
                        a = m457a(z, false, (boolean) f446d);
                        c0539i = null;
                    }
                    C0488c.m518a(a);
                    if (c0539i != null) {
                        this.f449c.m899b(this.f448b, c0539i);
                    }
                }

                public void m464a(C1834c c1834c, boolean z) {
                    if (!f446d && !Thread.holdsLock(this.f452g)) {
                        throw new AssertionError();
                    } else if (this.f456k != null) {
                        throw new IllegalStateException();
                    } else {
                        this.f456k = c1834c;
                        this.f457l = z;
                        c1834c.f4541d.add(new C0475a(this, this.f453h));
                    }
                }

                private void m458b(C1834c c1834c) {
                    int size = c1834c.f4541d.size();
                    for (int i = 0; i < size; i++) {
                        if (((Reference) c1834c.f4541d.get(i)).get() == this) {
                            c1834c.f4541d.remove(i);
                            return;
                        }
                    }
                    throw new IllegalStateException();
                }

                public Socket m463a(C1834c c1834c) {
                    if (f446d || Thread.holdsLock(this.f452g)) {
                        if (this.f460o == null) {
                            if (this.f456k.f4541d.size() == 1) {
                                Reference reference = (Reference) this.f456k.f4541d.get(0);
                                Socket a = m457a((boolean) f446d, false, false);
                                this.f456k = c1834c;
                                c1834c.f4541d.add(reference);
                                return a;
                            }
                        }
                        throw new IllegalStateException();
                    }
                    throw new AssertionError();
                }

                public boolean m471f() {
                    if (this.f451f == null && (this.f450e == null || !this.f450e.m444a())) {
                        if (!this.f454i.m453a()) {
                            return false;
                        }
                    }
                    return f446d;
                }

                public String toString() {
                    C1834c b = m467b();
                    return b != null ? b.toString() : this.f447a.toString();
                }
            }
