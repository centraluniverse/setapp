package p021c.p022a.p024b;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;
import p021c.C0543k;
import p021c.p022a.C0469a;

/* compiled from: ConnectionSpecSelector */
public final class C0470b {
    private final List<C0543k> f428a;
    private int f429b = 0;
    private boolean f430c;
    private boolean f431d;

    public C0470b(List<C0543k> list) {
        this.f428a = list;
    }

    public C0543k m436a(SSLSocket sSLSocket) throws IOException {
        C0543k c0543k;
        int size = this.f428a.size();
        for (int i = this.f429b; i < size; i++) {
            c0543k = (C0543k) this.f428a.get(i);
            if (c0543k.m850a(sSLSocket)) {
                this.f429b = i + 1;
                break;
            }
        }
        c0543k = null;
        if (c0543k == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unable to find acceptable protocols. isFallback=");
            stringBuilder.append(this.f431d);
            stringBuilder.append(", modes=");
            stringBuilder.append(this.f428a);
            stringBuilder.append(", supported protocols=");
            stringBuilder.append(Arrays.toString(sSLSocket.getEnabledProtocols()));
            throw new UnknownServiceException(stringBuilder.toString());
        }
        this.f430c = m435b(sSLSocket);
        C0469a.f427a.mo1339a(c0543k, sSLSocket, this.f431d);
        return c0543k;
    }

    public boolean m437a(IOException iOException) {
        boolean z = true;
        this.f431d = true;
        if (!this.f430c || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        boolean z2 = iOException instanceof SSLHandshakeException;
        if ((z2 && (iOException.getCause() instanceof CertificateException)) || (iOException instanceof SSLPeerUnverifiedException)) {
            return false;
        }
        if (!z2) {
            if ((iOException instanceof SSLProtocolException) == null) {
                z = false;
            }
        }
        return z;
    }

    private boolean m435b(SSLSocket sSLSocket) {
        for (int i = this.f429b; i < this.f428a.size(); i++) {
            if (((C0543k) this.f428a.get(i)).m850a(sSLSocket)) {
                return true;
            }
        }
        return null;
    }
}
