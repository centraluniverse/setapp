package p021c.p022a.p024b;

import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownServiceException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import p008b.p009a.p010a.p011a.p012a.p014b.C0356a;
import p021c.C0521a;
import p021c.C0532e;
import p021c.C0536g;
import p021c.C0539i;
import p021c.C0541j;
import p021c.C0543k;
import p021c.C0550p;
import p021c.C0552r;
import p021c.C0557t;
import p021c.C0559u.C0558a;
import p021c.C0564y;
import p021c.C1883x;
import p021c.aa;
import p021c.aa.C0522a;
import p021c.ac;
import p021c.ae;
import p021c.p022a.C0488c;
import p021c.p022a.C0490d;
import p021c.p022a.p025c.C0480c;
import p021c.p022a.p025c.C0483e;
import p021c.p022a.p026d.C1843a;
import p021c.p022a.p027e.C0492b;
import p021c.p022a.p027e.C0500g;
import p021c.p022a.p027e.C0500g.C0498a;
import p021c.p022a.p027e.C0500g.C0499b;
import p021c.p022a.p027e.C0503i;
import p021c.p022a.p027e.C1844f;
import p021c.p022a.p029g.C0515e;
import p021c.p022a.p031i.C0519e;
import p125d.C1625m;
import p125d.C1630t;
import p125d.C2283d;
import p125d.C2284e;

/* compiled from: RealConnection */
public final class C1834c extends C0499b implements C0539i {
    public boolean f4538a;
    public int f4539b;
    public int f4540c = 1;
    public final List<Reference<C0476g>> f4541d = new ArrayList();
    public long f4542e = Long.MAX_VALUE;
    private final C0541j f4543g;
    private final ae f4544h;
    private Socket f4545i;
    private Socket f4546j;
    private C0552r f4547k;
    private C0564y f4548l;
    private C0500g f4549m;
    private C2284e f4550n;
    private C2283d f4551o;

    public C1834c(C0541j c0541j, ae aeVar) {
        this.f4543g = c0541j;
        this.f4544h = aeVar;
    }

    public void m5053a(int i, int i2, int i3, boolean z, C0532e c0532e, C0550p c0550p) {
        IOException iOException;
        IOException e;
        C0532e c0532e2 = c0532e;
        C0550p c0550p2 = c0550p;
        if (this.f4548l != null) {
            throw new IllegalStateException("already connected");
        }
        List f = r7.f4544h.m790a().m736f();
        C0470b c0470b = new C0470b(f);
        if (r7.f4544h.m790a().m739i() == null) {
            if (f.contains(C0543k.f795c)) {
                String f2 = r7.f4544h.m790a().m730a().m969f();
                if (!C0515e.m695b().mo1324b(f2)) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("CLEARTEXT communication to ");
                    stringBuilder.append(f2);
                    stringBuilder.append(" not permitted by network security policy");
                    throw new C0472e(new UnknownServiceException(stringBuilder.toString()));
                }
            }
            throw new C0472e(new UnknownServiceException("CLEARTEXT communication not enabled for client"));
        }
        C0472e c0472e = null;
        do {
            int i4;
            int i5;
            try {
                if (r7.f4544h.m793d()) {
                    m5046a(i, i2, i3, c0532e2, c0550p2);
                    if (r7.f4545i != null) {
                        i4 = i;
                        i5 = i2;
                    } else if (!r7.f4544h.m793d() && r7.f4545i == null) {
                        throw new C0472e(new ProtocolException("Too many tunnel connections attempted: 21"));
                    } else if (r7.f4549m != null) {
                        synchronized (r7.f4543g) {
                            try {
                                r7.f4540c = r7.f4549m.m572a();
                            } catch (Throwable th) {
                                Throwable th2 = th;
                            }
                        }
                        return;
                    } else {
                        return;
                    }
                }
                try {
                    m5047a(i, i2, c0532e2, c0550p2);
                } catch (IOException e2) {
                    e = e2;
                }
                m5049a(c0470b, c0532e2, c0550p2);
                c0550p2.m895a(c0532e2, r7.f4544h.m792c(), r7.f4544h.m791b(), r7.f4548l);
                if (!r7.f4544h.m793d()) {
                }
                if (r7.f4549m != null) {
                    synchronized (r7.f4543g) {
                        r7.f4540c = r7.f4549m.m572a();
                    }
                    return;
                }
                return;
            } catch (IOException e3) {
                e = e3;
                i4 = i;
                i5 = i2;
                iOException = e;
                C0488c.m518a(r7.f4546j);
                C0488c.m518a(r7.f4545i);
                r7.f4546j = null;
                r7.f4545i = null;
                r7.f4550n = null;
                r7.f4551o = null;
                r7.f4547k = null;
                r7.f4548l = null;
                r7.f4549m = null;
                c0550p2.m896a(c0532e2, r7.f4544h.m792c(), r7.f4544h.m791b(), null, iOException);
                if (c0472e == null) {
                    c0472e = new C0472e(iOException);
                } else {
                    c0472e.m443a(iOException);
                }
                if (!z) {
                    break;
                } else if (!c0470b.m437a(iOException)) {
                }
                throw c0472e;
            }
        } while (c0470b.m437a(iOException));
        throw c0472e;
    }

    private void m5046a(int i, int i2, int i3, C0532e c0532e, C0550p c0550p) throws IOException {
        aa g = m5050g();
        C0557t a = g.m751a();
        int i4 = 0;
        while (i4 < 21) {
            m5047a(i, i2, c0532e, c0550p);
            g = m5045a(i2, i3, g, a);
            if (g != null) {
                C0488c.m518a(this.f4545i);
                this.f4545i = null;
                this.f4551o = null;
                this.f4550n = null;
                c0550p.m895a(c0532e, this.f4544h.m792c(), this.f4544h.m791b(), null);
                i4++;
            } else {
                return;
            }
        }
    }

    private void m5047a(int i, int i2, C0532e c0532e, C0550p c0550p) throws IOException {
        Socket socket;
        Proxy b = this.f4544h.m791b();
        C0521a a = this.f4544h.m790a();
        if (b.type() != Type.DIRECT) {
            if (b.type() != Type.HTTP) {
                socket = new Socket(b);
                this.f4545i = socket;
                c0550p.m894a(c0532e, this.f4544h.m792c(), b);
                this.f4545i.setSoTimeout(i2);
                C0515e.m695b().mo1321a(this.f4545i, this.f4544h.m792c(), i);
                this.f4550n = C1625m.m4841a(C1625m.m4850b(this.f4545i));
                this.f4551o = C1625m.m4840a(C1625m.m4844a(this.f4545i));
            }
        }
        socket = a.m733c().createSocket();
        this.f4545i = socket;
        c0550p.m894a(c0532e, this.f4544h.m792c(), b);
        this.f4545i.setSoTimeout(i2);
        try {
            C0515e.m695b().mo1321a(this.f4545i, this.f4544h.m792c(), i);
            try {
                this.f4550n = C1625m.m4841a(C1625m.m4850b(this.f4545i));
                this.f4551o = C1625m.m4840a(C1625m.m4844a(this.f4545i));
            } catch (int i3) {
                if ("throw with null exception".equals(i3.getMessage()) != 0) {
                    throw new IOException(i3);
                }
            }
        } catch (int i32) {
            c0532e = new StringBuilder();
            c0532e.append("Failed to connect to ");
            c0532e.append(this.f4544h.m792c());
            i2 = new ConnectException(c0532e.toString());
            i2.initCause(i32);
            throw i2;
        }
    }

    private void m5049a(C0470b c0470b, C0532e c0532e, C0550p c0550p) throws IOException {
        if (this.f4544h.m790a().m739i() == null) {
            this.f4548l = C0564y.HTTP_1_1;
            this.f4546j = this.f4545i;
            return;
        }
        c0550p.m897b(c0532e);
        m5048a(c0470b);
        c0550p.m890a(c0532e, this.f4547k);
        if (this.f4548l == C0564y.HTTP_2) {
            this.f4546j.setSoTimeout(null);
            this.f4549m = new C0498a(true).m567a(this.f4546j, this.f4544h.m790a().m730a().m969f(), this.f4550n, this.f4551o).m566a(this).m568a();
            this.f4549m.m591c();
        }
    }

    private void m5048a(C0470b c0470b) throws IOException {
        AssertionError e;
        C0521a a = this.f4544h.m790a();
        String str = null;
        try {
            Socket socket = (SSLSocket) a.m739i().createSocket(this.f4545i, a.m730a().m969f(), a.m730a().m970g(), true);
            try {
                c0470b = c0470b.m436a((SSLSocket) socket);
                if (c0470b.m853d()) {
                    C0515e.m695b().mo1322a((SSLSocket) socket, a.m730a().m969f(), a.m735e());
                }
                socket.startHandshake();
                C0552r a2 = C0552r.m908a(socket.getSession());
                if (a.m740j().verify(a.m730a().m969f(), socket.getSession())) {
                    a.m741k().m828a(a.m730a().m969f(), a2.m910b());
                    if (c0470b.m853d() != null) {
                        str = C0515e.m695b().mo1318a((SSLSocket) socket);
                    }
                    this.f4546j = socket;
                    this.f4550n = C1625m.m4841a(C1625m.m4850b(this.f4546j));
                    this.f4551o = C1625m.m4840a(C1625m.m4844a(this.f4546j));
                    this.f4547k = a2;
                    if (str != null) {
                        c0470b = C0564y.m1005a(str);
                    } else {
                        c0470b = C0564y.HTTP_1_1;
                    }
                    this.f4548l = c0470b;
                    if (socket != null) {
                        C0515e.m695b().mo1325b((SSLSocket) socket);
                        return;
                    }
                    return;
                }
                Certificate certificate = (X509Certificate) a2.m910b().get(0);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Hostname ");
                stringBuilder.append(a.m730a().m969f());
                stringBuilder.append(" not verified:\n    certificate: ");
                stringBuilder.append(C0536g.m824a(certificate));
                stringBuilder.append("\n    DN: ");
                stringBuilder.append(certificate.getSubjectDN().getName());
                stringBuilder.append("\n    subjectAltNames: ");
                stringBuilder.append(C0519e.m723a(certificate));
                throw new SSLPeerUnverifiedException(stringBuilder.toString());
            } catch (AssertionError e2) {
                e = e2;
                str = socket;
                try {
                    if (C0488c.m520a(e)) {
                        throw new IOException(e);
                    }
                    throw e;
                } catch (Throwable th) {
                    c0470b = th;
                    socket = str;
                    if (socket != null) {
                        C0515e.m695b().mo1325b((SSLSocket) socket);
                    }
                    C0488c.m518a(socket);
                    throw c0470b;
                }
            } catch (Throwable th2) {
                c0470b = th2;
                if (socket != null) {
                    C0515e.m695b().mo1325b((SSLSocket) socket);
                }
                C0488c.m518a(socket);
                throw c0470b;
            }
        } catch (AssertionError e3) {
            e = e3;
            if (C0488c.m520a(e)) {
                throw new IOException(e);
            }
            throw e;
        }
    }

    private aa m5045a(int i, int i2, aa aaVar, C0557t c0557t) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CONNECT ");
        stringBuilder.append(C0488c.m508a(c0557t, true));
        stringBuilder.append(" HTTP/1.1");
        String stringBuilder2 = stringBuilder.toString();
        while (true) {
            C1843a c1843a = new C1843a(null, null, this.f4550n, this.f4551o);
            this.f4550n.timeout().mo1893a((long) i, TimeUnit.MILLISECONDS);
            this.f4551o.timeout().mo1893a((long) i2, TimeUnit.MILLISECONDS);
            c1843a.m5097a(aaVar.m754c(), stringBuilder2);
            c1843a.mo1293b();
            ac a = c1843a.mo1288a(false).m763a(aaVar).m771a();
            long a2 = C0483e.m486a(a);
            if (a2 == -1) {
                a2 = 0;
            }
            C1630t b = c1843a.m5099b(a2);
            C0488c.m527b(b, (int) ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, TimeUnit.MILLISECONDS);
            b.close();
            int b2 = a.m778b();
            if (b2 == 200) {
                break;
            } else if (b2 != 407) {
                i2 = new StringBuilder();
                i2.append("Unexpected response code for CONNECT: ");
                i2.append(a.m778b());
                throw new IOException(i2.toString());
            } else {
                aa a3 = this.f4544h.m790a().m734d().mo1329a(this.f4544h, a);
                if (a3 == null) {
                    throw new IOException("Failed to authenticate with proxy");
                } else if ("close".equalsIgnoreCase(a.m776a("Connection")) != null) {
                    return a3;
                } else {
                    aaVar = a3;
                }
            }
        }
        if (this.f4550n.mo2516b().mo2525f() != 0) {
            if (this.f4551o.mo2516b().mo2525f() != 0) {
                return null;
            }
        }
        throw new IOException("TLS tunnel buffered too many bytes!");
    }

    private aa m5050g() {
        return new C0522a().m744a(this.f4544h.m790a().m730a()).m747a("Host", C0488c.m508a(this.f4544h.m790a().m730a(), true)).m747a("Proxy-Connection", "Keep-Alive").m747a(C0356a.HEADER_USER_AGENT, C0490d.m533a()).m748a();
    }

    public boolean m5056a(p021c.C0521a r5, @javax.annotation.Nullable p021c.ae r6) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r4 = this;
        r0 = r4.f4541d;
        r0 = r0.size();
        r1 = r4.f4540c;
        r2 = 0;
        if (r0 >= r1) goto L_0x00a6;
    L_0x000b:
        r0 = r4.f4538a;
        if (r0 == 0) goto L_0x0011;
    L_0x000f:
        goto L_0x00a6;
    L_0x0011:
        r0 = p021c.p022a.C0469a.f427a;
        r1 = r4.f4544h;
        r1 = r1.m790a();
        r0 = r0.mo1342a(r1, r5);
        if (r0 != 0) goto L_0x0020;
    L_0x001f:
        return r2;
    L_0x0020:
        r0 = r5.m730a();
        r0 = r0.m969f();
        r1 = r4.mo1271a();
        r1 = r1.m790a();
        r1 = r1.m730a();
        r1 = r1.m969f();
        r0 = r0.equals(r1);
        r1 = 1;
        if (r0 == 0) goto L_0x0040;
    L_0x003f:
        return r1;
    L_0x0040:
        r0 = r4.f4549m;
        if (r0 != 0) goto L_0x0045;
    L_0x0044:
        return r2;
    L_0x0045:
        if (r6 != 0) goto L_0x0048;
    L_0x0047:
        return r2;
    L_0x0048:
        r0 = r6.m791b();
        r0 = r0.type();
        r3 = java.net.Proxy.Type.DIRECT;
        if (r0 == r3) goto L_0x0055;
    L_0x0054:
        return r2;
    L_0x0055:
        r0 = r4.f4544h;
        r0 = r0.m791b();
        r0 = r0.type();
        r3 = java.net.Proxy.Type.DIRECT;
        if (r0 == r3) goto L_0x0064;
    L_0x0063:
        return r2;
    L_0x0064:
        r0 = r4.f4544h;
        r0 = r0.m792c();
        r3 = r6.m792c();
        r0 = r0.equals(r3);
        if (r0 != 0) goto L_0x0075;
    L_0x0074:
        return r2;
    L_0x0075:
        r6 = r6.m790a();
        r6 = r6.m740j();
        r0 = p021c.p022a.p031i.C0519e.f626a;
        if (r6 == r0) goto L_0x0082;
    L_0x0081:
        return r2;
    L_0x0082:
        r6 = r5.m730a();
        r6 = r4.m5057a(r6);
        if (r6 != 0) goto L_0x008d;
    L_0x008c:
        return r2;
    L_0x008d:
        r6 = r5.m741k();	 Catch:{ SSLPeerUnverifiedException -> 0x00a5 }
        r5 = r5.m730a();	 Catch:{ SSLPeerUnverifiedException -> 0x00a5 }
        r5 = r5.m969f();	 Catch:{ SSLPeerUnverifiedException -> 0x00a5 }
        r0 = r4.m5062e();	 Catch:{ SSLPeerUnverifiedException -> 0x00a5 }
        r0 = r0.m910b();	 Catch:{ SSLPeerUnverifiedException -> 0x00a5 }
        r6.m828a(r5, r0);	 Catch:{ SSLPeerUnverifiedException -> 0x00a5 }
        return r1;
    L_0x00a5:
        return r2;
    L_0x00a6:
        return r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.b.c.a(c.a, c.ae):boolean");
    }

    public boolean m5057a(C0557t c0557t) {
        if (c0557t.m970g() != this.f4544h.m790a().m730a().m970g()) {
            return false;
        }
        boolean z = true;
        if (c0557t.m969f().equals(this.f4544h.m790a().m730a().m969f())) {
            return true;
        }
        if (this.f4547k == null || C0519e.f626a.m728a(c0557t.m969f(), (X509Certificate) this.f4547k.m910b().get(0)) == null) {
            z = false;
        }
        return z;
    }

    public C0480c m5051a(C1883x c1883x, C0558a c0558a, C0476g c0476g) throws SocketException {
        if (this.f4549m != null) {
            return new C1844f(c1883x, c0558a, c0476g, this.f4549m);
        }
        this.f4546j.setSoTimeout(c0558a.mo1279d());
        this.f4550n.timeout().mo1893a((long) c0558a.mo1279d(), TimeUnit.MILLISECONDS);
        this.f4551o.timeout().mo1893a((long) c0558a.mo1280e(), TimeUnit.MILLISECONDS);
        return new C1843a(c1883x, c0476g, this.f4550n, this.f4551o);
    }

    public ae mo1271a() {
        return this.f4544h;
    }

    public void m5060c() {
        C0488c.m518a(this.f4545i);
    }

    public Socket m5061d() {
        return this.f4546j;
    }

    public boolean m5058a(boolean r5) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r4 = this;
        r0 = r4.f4546j;
        r0 = r0.isClosed();
        r1 = 0;
        if (r0 != 0) goto L_0x0051;
    L_0x0009:
        r0 = r4.f4546j;
        r0 = r0.isInputShutdown();
        if (r0 != 0) goto L_0x0051;
    L_0x0011:
        r0 = r4.f4546j;
        r0 = r0.isOutputShutdown();
        if (r0 == 0) goto L_0x001a;
    L_0x0019:
        goto L_0x0051;
    L_0x001a:
        r0 = r4.f4549m;
        r2 = 1;
        if (r0 == 0) goto L_0x0027;
    L_0x001f:
        r5 = r4.f4549m;
        r5 = r5.m593d();
        r5 = r5 ^ r2;
        return r5;
    L_0x0027:
        if (r5 == 0) goto L_0x0050;
    L_0x0029:
        r5 = r4.f4546j;	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        r5 = r5.getSoTimeout();	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        r0 = r4.f4546j;	 Catch:{ all -> 0x0048 }
        r0.setSoTimeout(r2);	 Catch:{ all -> 0x0048 }
        r0 = r4.f4550n;	 Catch:{ all -> 0x0048 }
        r0 = r0.mo2525f();	 Catch:{ all -> 0x0048 }
        if (r0 == 0) goto L_0x0042;
    L_0x003c:
        r0 = r4.f4546j;	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        r0.setSoTimeout(r5);	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        return r1;	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
    L_0x0042:
        r0 = r4.f4546j;	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        r0.setSoTimeout(r5);	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        return r2;	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
    L_0x0048:
        r0 = move-exception;	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        r3 = r4.f4546j;	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        r3.setSoTimeout(r5);	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
        throw r0;	 Catch:{ SocketTimeoutException -> 0x0050, IOException -> 0x004f }
    L_0x004f:
        return r1;
    L_0x0050:
        return r2;
    L_0x0051:
        return r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.b.c.a(boolean):boolean");
    }

    public void mo1273a(C0503i c0503i) throws IOException {
        c0503i.m623a(C0492b.REFUSED_STREAM);
    }

    public void mo1272a(C0500g c0500g) {
        synchronized (this.f4543g) {
            this.f4540c = c0500g.m572a();
        }
    }

    public C0552r m5062e() {
        return this.f4547k;
    }

    public boolean m5063f() {
        return this.f4549m != null;
    }

    public C0564y mo1274b() {
        return this.f4548l;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Connection{");
        stringBuilder.append(this.f4544h.m790a().m730a().m969f());
        stringBuilder.append(":");
        stringBuilder.append(this.f4544h.m790a().m730a().m970g());
        stringBuilder.append(", proxy=");
        stringBuilder.append(this.f4544h.m791b());
        stringBuilder.append(" hostAddress=");
        stringBuilder.append(this.f4544h.m792c());
        stringBuilder.append(" cipherSuite=");
        stringBuilder.append(this.f4547k != null ? this.f4547k.m909a() : "none");
        stringBuilder.append(" protocol=");
        stringBuilder.append(this.f4548l);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
