package p021c.p022a.p024b;

import android.support.v4.internal.view.SupportMenu;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import p021c.C0521a;
import p021c.C0532e;
import p021c.C0550p;
import p021c.C0557t;
import p021c.ae;
import p021c.p022a.C0488c;

/* compiled from: RouteSelector */
public final class C0474f {
    private final C0521a f437a;
    private final C0471d f438b;
    private final C0532e f439c;
    private final C0550p f440d;
    private List<Proxy> f441e = Collections.emptyList();
    private int f442f;
    private List<InetSocketAddress> f443g = Collections.emptyList();
    private final List<ae> f444h = new ArrayList();

    /* compiled from: RouteSelector */
    public static final class C0473a {
        private final List<ae> f435a;
        private int f436b = 0;

        C0473a(List<ae> list) {
            this.f435a = list;
        }

        public boolean m444a() {
            return this.f436b < this.f435a.size();
        }

        public ae m445b() {
            if (m444a()) {
                List list = this.f435a;
                int i = this.f436b;
                this.f436b = i + 1;
                return (ae) list.get(i);
            }
            throw new NoSuchElementException();
        }

        public List<ae> m446c() {
            return new ArrayList(this.f435a);
        }
    }

    public C0474f(C0521a c0521a, C0471d c0471d, C0532e c0532e, C0550p c0550p) {
        this.f437a = c0521a;
        this.f438b = c0471d;
        this.f439c = c0532e;
        this.f440d = c0550p;
        m448a(c0521a.m730a(), c0521a.m738h());
    }

    public boolean m453a() {
        if (!m450c()) {
            if (this.f444h.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public C0473a m454b() throws IOException {
        if (m453a()) {
            List arrayList = new ArrayList();
            while (m450c()) {
                Proxy d = m451d();
                int size = this.f443g.size();
                for (int i = 0; i < size; i++) {
                    ae aeVar = new ae(this.f437a, d, (InetSocketAddress) this.f443g.get(i));
                    if (this.f438b.m440c(aeVar)) {
                        this.f444h.add(aeVar);
                    } else {
                        arrayList.add(aeVar);
                    }
                }
                if (!arrayList.isEmpty()) {
                    break;
                }
            }
            if (arrayList.isEmpty()) {
                arrayList.addAll(this.f444h);
                this.f444h.clear();
            }
            return new C0473a(arrayList);
        }
        throw new NoSuchElementException();
    }

    public void m452a(ae aeVar, IOException iOException) {
        if (!(aeVar.m791b().type() == Type.DIRECT || this.f437a.m737g() == null)) {
            this.f437a.m737g().connectFailed(this.f437a.m730a().m962a(), aeVar.m791b().address(), iOException);
        }
        this.f438b.m438a(aeVar);
    }

    private void m448a(C0557t c0557t, Proxy proxy) {
        if (proxy != null) {
            this.f441e = Collections.singletonList(proxy);
        } else {
            List select = this.f437a.m737g().select(c0557t.m962a());
            if (select == null || select.isEmpty() != null) {
                c0557t = C0488c.m514a(Proxy.NO_PROXY);
            } else {
                c0557t = C0488c.m513a(select);
            }
            this.f441e = c0557t;
        }
        this.f442f = 0;
    }

    private boolean m450c() {
        return this.f442f < this.f441e.size();
    }

    private Proxy m451d() throws IOException {
        if (m450c()) {
            List list = this.f441e;
            int i = this.f442f;
            this.f442f = i + 1;
            Proxy proxy = (Proxy) list.get(i);
            m449a(proxy);
            return proxy;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("No route to ");
        stringBuilder.append(this.f437a.m730a().m969f());
        stringBuilder.append("; exhausted proxy configurations: ");
        stringBuilder.append(this.f441e);
        throw new SocketException(stringBuilder.toString());
    }

    private void m449a(Proxy proxy) throws IOException {
        String a;
        int port;
        List a2;
        int size;
        int i;
        StringBuilder stringBuilder;
        this.f443g = new ArrayList();
        if (proxy.type() != Type.DIRECT) {
            if (proxy.type() != Type.SOCKS) {
                SocketAddress address = proxy.address();
                if (address instanceof InetSocketAddress) {
                    InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
                    a = C0474f.m447a(inetSocketAddress);
                    port = inetSocketAddress.getPort();
                    if (port >= 1) {
                        if (port > SupportMenu.USER_MASK) {
                            if (proxy.type() != Type.SOCKS) {
                                this.f443g.add(InetSocketAddress.createUnresolved(a, port));
                                return;
                            }
                            this.f440d.m892a(this.f439c, a);
                            a2 = this.f437a.m732b().mo1333a(a);
                            if (a2.isEmpty()) {
                                this.f440d.m893a(this.f439c, a, a2);
                                size = a2.size();
                                for (i = 0; i < size; i++) {
                                    this.f443g.add(new InetSocketAddress((InetAddress) a2.get(i), port));
                                }
                                return;
                            }
                            StringBuilder stringBuilder2 = new StringBuilder();
                            stringBuilder2.append(this.f437a.m732b());
                            stringBuilder2.append(" returned no addresses for ");
                            stringBuilder2.append(a);
                            throw new UnknownHostException(stringBuilder2.toString());
                        }
                    }
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("No route to ");
                    stringBuilder.append(a);
                    stringBuilder.append(":");
                    stringBuilder.append(port);
                    stringBuilder.append("; port is out of range");
                    throw new SocketException(stringBuilder.toString());
                }
                StringBuilder stringBuilder3 = new StringBuilder();
                stringBuilder3.append("Proxy.address() is not an InetSocketAddress: ");
                stringBuilder3.append(address.getClass());
                throw new IllegalArgumentException(stringBuilder3.toString());
            }
        }
        a = this.f437a.m730a().m969f();
        port = this.f437a.m730a().m970g();
        if (port >= 1) {
            if (port > SupportMenu.USER_MASK) {
                if (proxy.type() != Type.SOCKS) {
                    this.f440d.m892a(this.f439c, a);
                    a2 = this.f437a.m732b().mo1333a(a);
                    if (a2.isEmpty()) {
                        this.f440d.m893a(this.f439c, a, a2);
                        size = a2.size();
                        for (i = 0; i < size; i++) {
                            this.f443g.add(new InetSocketAddress((InetAddress) a2.get(i), port));
                        }
                        return;
                    }
                    StringBuilder stringBuilder22 = new StringBuilder();
                    stringBuilder22.append(this.f437a.m732b());
                    stringBuilder22.append(" returned no addresses for ");
                    stringBuilder22.append(a);
                    throw new UnknownHostException(stringBuilder22.toString());
                }
                this.f443g.add(InetSocketAddress.createUnresolved(a, port));
                return;
            }
        }
        stringBuilder = new StringBuilder();
        stringBuilder.append("No route to ");
        stringBuilder.append(a);
        stringBuilder.append(":");
        stringBuilder.append(port);
        stringBuilder.append("; port is out of range");
        throw new SocketException(stringBuilder.toString());
    }

    static String m447a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        if (address == null) {
            return inetSocketAddress.getHostName();
        }
        return address.getHostAddress();
    }
}
