package p021c.p022a.p024b;

import java.util.LinkedHashSet;
import java.util.Set;
import p021c.ae;

/* compiled from: RouteDatabase */
public final class C0471d {
    private final Set<ae> f432a = new LinkedHashSet();

    public synchronized void m438a(ae aeVar) {
        this.f432a.add(aeVar);
    }

    public synchronized void m439b(ae aeVar) {
        this.f432a.remove(aeVar);
    }

    public synchronized boolean m440c(ae aeVar) {
        return this.f432a.contains(aeVar);
    }
}
