package p021c.p022a.p023a;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;
import p021c.p022a.p028f.C0511a;
import p125d.C2283d;

/* compiled from: DiskLruCache */
public final class C0467d implements Closeable, Flushable {
    static final Pattern f412a = Pattern.compile("[a-z0-9_-]{1,120}");
    static final /* synthetic */ boolean f413j = true;
    final C0511a f414b;
    final int f415c;
    C2283d f416d;
    final LinkedHashMap<String, C0466b> f417e;
    int f418f;
    boolean f419g;
    boolean f420h;
    boolean f421i;
    private long f422k;
    private long f423l;
    private long f424m;
    private final Executor f425n;
    private final Runnable f426o;

    /* compiled from: DiskLruCache */
    public final class C0465a {
        final C0466b f401a;
        final boolean[] f402b;
        final /* synthetic */ C0467d f403c;
        private boolean f404d;

        void m410a() {
            /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
            /*
            r3 = this;
            r0 = r3.f401a;
            r0 = r0.f410f;
            if (r0 != r3) goto L_0x0022;
        L_0x0006:
            r0 = 0;
        L_0x0007:
            r1 = r3.f403c;
            r1 = r1.f415c;
            if (r0 >= r1) goto L_0x001d;
        L_0x000d:
            r1 = r3.f403c;	 Catch:{ IOException -> 0x001a }
            r1 = r1.f414b;	 Catch:{ IOException -> 0x001a }
            r2 = r3.f401a;	 Catch:{ IOException -> 0x001a }
            r2 = r2.f408d;	 Catch:{ IOException -> 0x001a }
            r2 = r2[r0];	 Catch:{ IOException -> 0x001a }
            r1.mo1310a(r2);	 Catch:{ IOException -> 0x001a }
        L_0x001a:
            r0 = r0 + 1;
            goto L_0x0007;
        L_0x001d:
            r0 = r3.f401a;
            r1 = 0;
            r0.f410f = r1;
        L_0x0022:
            return;
            */
            throw new UnsupportedOperationException("Method not decompiled: c.a.a.d.a.a():void");
        }

        public void m411b() throws IOException {
            synchronized (this.f403c) {
                if (this.f404d) {
                    throw new IllegalStateException();
                }
                if (this.f401a.f410f == this) {
                    this.f403c.m414a(this, false);
                }
                this.f404d = C0467d.f413j;
            }
        }
    }

    /* compiled from: DiskLruCache */
    private final class C0466b {
        final String f405a;
        final long[] f406b;
        final File[] f407c;
        final File[] f408d;
        boolean f409e;
        C0465a f410f;
        long f411g;

        void m412a(C2283d c2283d) throws IOException {
            for (long o : this.f406b) {
                c2283d.mo2535k(32).mo2540o(o);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    synchronized void m414a(p021c.p022a.p023a.C0467d.C0465a r12, boolean r13) throws java.io.IOException {
        /*
        r11 = this;
        monitor-enter(r11);
        r0 = r12.f401a;	 Catch:{ all -> 0x00fe }
        r1 = r0.f410f;	 Catch:{ all -> 0x00fe }
        if (r1 == r12) goto L_0x000d;
    L_0x0007:
        r12 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x00fe }
        r12.<init>();	 Catch:{ all -> 0x00fe }
        throw r12;	 Catch:{ all -> 0x00fe }
    L_0x000d:
        r1 = 0;
        if (r13 == 0) goto L_0x004d;
    L_0x0010:
        r2 = r0.f409e;	 Catch:{ all -> 0x00fe }
        if (r2 != 0) goto L_0x004d;
    L_0x0014:
        r2 = r1;
    L_0x0015:
        r3 = r11.f415c;	 Catch:{ all -> 0x00fe }
        if (r2 >= r3) goto L_0x004d;
    L_0x0019:
        r3 = r12.f402b;	 Catch:{ all -> 0x00fe }
        r3 = r3[r2];	 Catch:{ all -> 0x00fe }
        if (r3 != 0) goto L_0x0039;
    L_0x001f:
        r12.m411b();	 Catch:{ all -> 0x00fe }
        r12 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x00fe }
        r13 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fe }
        r13.<init>();	 Catch:{ all -> 0x00fe }
        r0 = "Newly created entry didn't create value for index ";
        r13.append(r0);	 Catch:{ all -> 0x00fe }
        r13.append(r2);	 Catch:{ all -> 0x00fe }
        r13 = r13.toString();	 Catch:{ all -> 0x00fe }
        r12.<init>(r13);	 Catch:{ all -> 0x00fe }
        throw r12;	 Catch:{ all -> 0x00fe }
    L_0x0039:
        r3 = r11.f414b;	 Catch:{ all -> 0x00fe }
        r4 = r0.f408d;	 Catch:{ all -> 0x00fe }
        r4 = r4[r2];	 Catch:{ all -> 0x00fe }
        r3 = r3.mo1312b(r4);	 Catch:{ all -> 0x00fe }
        if (r3 != 0) goto L_0x004a;
    L_0x0045:
        r12.m411b();	 Catch:{ all -> 0x00fe }
        monitor-exit(r11);
        return;
    L_0x004a:
        r2 = r2 + 1;
        goto L_0x0015;
    L_0x004d:
        r12 = r11.f415c;	 Catch:{ all -> 0x00fe }
        if (r1 >= r12) goto L_0x0087;
    L_0x0051:
        r12 = r0.f408d;	 Catch:{ all -> 0x00fe }
        r12 = r12[r1];	 Catch:{ all -> 0x00fe }
        if (r13 == 0) goto L_0x007f;
    L_0x0057:
        r2 = r11.f414b;	 Catch:{ all -> 0x00fe }
        r2 = r2.mo1312b(r12);	 Catch:{ all -> 0x00fe }
        if (r2 == 0) goto L_0x0084;
    L_0x005f:
        r2 = r0.f407c;	 Catch:{ all -> 0x00fe }
        r2 = r2[r1];	 Catch:{ all -> 0x00fe }
        r3 = r11.f414b;	 Catch:{ all -> 0x00fe }
        r3.mo1311a(r12, r2);	 Catch:{ all -> 0x00fe }
        r12 = r0.f406b;	 Catch:{ all -> 0x00fe }
        r3 = r12[r1];	 Catch:{ all -> 0x00fe }
        r12 = r11.f414b;	 Catch:{ all -> 0x00fe }
        r5 = r12.mo1313c(r2);	 Catch:{ all -> 0x00fe }
        r12 = r0.f406b;	 Catch:{ all -> 0x00fe }
        r12[r1] = r5;	 Catch:{ all -> 0x00fe }
        r7 = r11.f423l;	 Catch:{ all -> 0x00fe }
        r9 = r7 - r3;
        r2 = r9 + r5;
        r11.f423l = r2;	 Catch:{ all -> 0x00fe }
        goto L_0x0084;
    L_0x007f:
        r2 = r11.f414b;	 Catch:{ all -> 0x00fe }
        r2.mo1310a(r12);	 Catch:{ all -> 0x00fe }
    L_0x0084:
        r1 = r1 + 1;
        goto L_0x004d;
    L_0x0087:
        r12 = r11.f418f;	 Catch:{ all -> 0x00fe }
        r1 = 1;
        r12 = r12 + r1;
        r11.f418f = r12;	 Catch:{ all -> 0x00fe }
        r12 = 0;
        r0.f410f = r12;	 Catch:{ all -> 0x00fe }
        r12 = r0.f409e;	 Catch:{ all -> 0x00fe }
        r12 = r12 | r13;
        r2 = 10;
        r3 = 32;
        if (r12 == 0) goto L_0x00c4;
    L_0x0099:
        r0.f409e = r1;	 Catch:{ all -> 0x00fe }
        r12 = r11.f416d;	 Catch:{ all -> 0x00fe }
        r1 = "CLEAN";
        r12 = r12.mo2518b(r1);	 Catch:{ all -> 0x00fe }
        r12.mo2535k(r3);	 Catch:{ all -> 0x00fe }
        r12 = r11.f416d;	 Catch:{ all -> 0x00fe }
        r1 = r0.f405a;	 Catch:{ all -> 0x00fe }
        r12.mo2518b(r1);	 Catch:{ all -> 0x00fe }
        r12 = r11.f416d;	 Catch:{ all -> 0x00fe }
        r0.m412a(r12);	 Catch:{ all -> 0x00fe }
        r12 = r11.f416d;	 Catch:{ all -> 0x00fe }
        r12.mo2535k(r2);	 Catch:{ all -> 0x00fe }
        if (r13 == 0) goto L_0x00e2;
    L_0x00b9:
        r12 = r11.f424m;	 Catch:{ all -> 0x00fe }
        r1 = 1;
        r3 = r12 + r1;
        r11.f424m = r3;	 Catch:{ all -> 0x00fe }
        r0.f411g = r12;	 Catch:{ all -> 0x00fe }
        goto L_0x00e2;
    L_0x00c4:
        r12 = r11.f417e;	 Catch:{ all -> 0x00fe }
        r13 = r0.f405a;	 Catch:{ all -> 0x00fe }
        r12.remove(r13);	 Catch:{ all -> 0x00fe }
        r12 = r11.f416d;	 Catch:{ all -> 0x00fe }
        r13 = "REMOVE";
        r12 = r12.mo2518b(r13);	 Catch:{ all -> 0x00fe }
        r12.mo2535k(r3);	 Catch:{ all -> 0x00fe }
        r12 = r11.f416d;	 Catch:{ all -> 0x00fe }
        r13 = r0.f405a;	 Catch:{ all -> 0x00fe }
        r12.mo2518b(r13);	 Catch:{ all -> 0x00fe }
        r12 = r11.f416d;	 Catch:{ all -> 0x00fe }
        r12.mo2535k(r2);	 Catch:{ all -> 0x00fe }
    L_0x00e2:
        r12 = r11.f416d;	 Catch:{ all -> 0x00fe }
        r12.flush();	 Catch:{ all -> 0x00fe }
        r12 = r11.f423l;	 Catch:{ all -> 0x00fe }
        r0 = r11.f422k;	 Catch:{ all -> 0x00fe }
        r2 = (r12 > r0 ? 1 : (r12 == r0 ? 0 : -1));
        if (r2 > 0) goto L_0x00f5;
    L_0x00ef:
        r12 = r11.m415a();	 Catch:{ all -> 0x00fe }
        if (r12 == 0) goto L_0x00fc;
    L_0x00f5:
        r12 = r11.f425n;	 Catch:{ all -> 0x00fe }
        r13 = r11.f426o;	 Catch:{ all -> 0x00fe }
        r12.execute(r13);	 Catch:{ all -> 0x00fe }
    L_0x00fc:
        monitor-exit(r11);
        return;
    L_0x00fe:
        r12 = move-exception;
        monitor-exit(r11);
        throw r12;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.a.d.a(c.a.a.d$a, boolean):void");
    }

    boolean m415a() {
        return (this.f418f < 2000 || this.f418f < this.f417e.size()) ? false : f413j;
    }

    boolean m416a(C0466b c0466b) throws IOException {
        if (c0466b.f410f != null) {
            c0466b.f410f.m410a();
        }
        for (int i = 0; i < this.f415c; i++) {
            this.f414b.mo1310a(c0466b.f407c[i]);
            this.f423l -= c0466b.f406b[i];
            c0466b.f406b[i] = 0;
        }
        this.f418f++;
        this.f416d.mo2518b("REMOVE").mo2535k(32).mo2518b(c0466b.f405a).mo2535k(10);
        this.f417e.remove(c0466b.f405a);
        if (m415a() != null) {
            this.f425n.execute(this.f426o);
        }
        return f413j;
    }

    public synchronized boolean m417b() {
        return this.f420h;
    }

    private synchronized void m413d() {
        if (m417b()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    public synchronized void flush() throws IOException {
        if (this.f419g) {
            m413d();
            m418c();
            this.f416d.flush();
        }
    }

    public synchronized void close() throws IOException {
        if (this.f419g) {
            if (!this.f420h) {
                for (C0466b c0466b : (C0466b[]) this.f417e.values().toArray(new C0466b[this.f417e.size()])) {
                    if (c0466b.f410f != null) {
                        c0466b.f410f.m411b();
                    }
                }
                m418c();
                this.f416d.close();
                this.f416d = null;
                this.f420h = f413j;
                return;
            }
        }
        this.f420h = f413j;
    }

    void m418c() throws IOException {
        while (this.f423l > this.f422k) {
            m416a((C0466b) this.f417e.values().iterator().next());
        }
        this.f421i = false;
    }
}
