package p021c.p022a.p023a;

import com.solaredge.common.models.ValidationResult;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import p021c.C0530d;
import p021c.C0554s.C0553a;
import p021c.aa;
import p021c.ac;
import p021c.ac.C0523a;
import p021c.p022a.C0469a;
import p021c.p022a.p025c.C0482d;
import p021c.p022a.p025c.C0483e;

/* compiled from: CacheStrategy */
public final class C0464c {
    @Nullable
    public final aa f399a;
    @Nullable
    public final ac f400b;

    /* compiled from: CacheStrategy */
    public static class C0463a {
        final long f387a;
        final aa f388b;
        final ac f389c;
        private Date f390d;
        private String f391e;
        private Date f392f;
        private String f393g;
        private Date f394h;
        private long f395i;
        private long f396j;
        private String f397k;
        private int f398l = -1;

        public C0463a(long j, aa aaVar, ac acVar) {
            this.f387a = j;
            this.f388b = aaVar;
            this.f389c = acVar;
            if (acVar != null) {
                this.f395i = acVar.m788l();
                this.f396j = acVar.m789m();
                j = acVar.m782f();
                aaVar = j.m920a();
                for (int i = 0; i < aaVar; i++) {
                    acVar = j.m921a(i);
                    String b = j.m924b(i);
                    if ("Date".equalsIgnoreCase(acVar)) {
                        this.f390d = C0482d.m483a(b);
                        this.f391e = b;
                    } else if ("Expires".equalsIgnoreCase(acVar)) {
                        this.f394h = C0482d.m483a(b);
                    } else if ("Last-Modified".equalsIgnoreCase(acVar)) {
                        this.f392f = C0482d.m483a(b);
                        this.f393g = b;
                    } else if ("ETag".equalsIgnoreCase(acVar)) {
                        this.f397k = b;
                    } else if ("Age".equalsIgnoreCase(acVar) != null) {
                        this.f398l = C0483e.m490b(b, -1);
                    }
                }
            }
        }

        public C0464c m408a() {
            C0464c b = m404b();
            return (b.f399a == null || !this.f388b.m757f().m813i()) ? b : new C0464c(null, null);
        }

        private C0464c m404b() {
            if (this.f389c == null) {
                return new C0464c(this.f388b, null);
            }
            if (this.f388b.m758g() && this.f389c.m781e() == null) {
                return new C0464c(this.f388b, null);
            }
            if (!C0464c.m409a(this.f389c, this.f388b)) {
                return new C0464c(this.f388b, null);
            }
            C0530d f = this.f388b.m757f();
            if (!f.m805a()) {
                if (!C0463a.m403a(this.f388b)) {
                    C0530d k = this.f389c.m787k();
                    if (k.m814j()) {
                        return new C0464c(null, this.f389c);
                    }
                    String str;
                    String str2;
                    long d = m406d();
                    long c = m405c();
                    if (f.m807c() != -1) {
                        c = Math.min(c, TimeUnit.SECONDS.toMillis((long) f.m807c()));
                    }
                    long j = 0;
                    long toMillis = f.m812h() != -1 ? TimeUnit.SECONDS.toMillis((long) f.m812h()) : 0;
                    if (!(k.m810f() || f.m811g() == -1)) {
                        j = TimeUnit.SECONDS.toMillis((long) f.m811g());
                    }
                    if (!k.m805a()) {
                        long j2 = d + toMillis;
                        if (j2 < c + j) {
                            C0523a h = this.f389c.m784h();
                            if (j2 >= c) {
                                h.m770a(ValidationResult.RESULT_WARNING, "110 HttpURLConnection \"Response is stale\"");
                            }
                            if (d > 86400000 && m407e()) {
                                h.m770a(ValidationResult.RESULT_WARNING, "113 HttpURLConnection \"Heuristic expiration\"");
                            }
                            return new C0464c(null, h.m771a());
                        }
                    }
                    if (this.f397k != null) {
                        str = "If-None-Match";
                        str2 = this.f397k;
                    } else if (this.f392f != null) {
                        str = "If-Modified-Since";
                        str2 = this.f393g;
                    } else if (this.f390d == null) {
                        return new C0464c(this.f388b, null);
                    } else {
                        str = "If-Modified-Since";
                        str2 = this.f391e;
                    }
                    C0553a b = this.f388b.m754c().m923b();
                    C0469a.f427a.mo1341a(b, str, str2);
                    return new C0464c(this.f388b.m756e().m743a(b.m914a()).m748a(), this.f389c);
                }
            }
            return new C0464c(this.f388b, null);
        }

        private long m405c() {
            C0530d k = this.f389c.m787k();
            if (k.m807c() != -1) {
                return TimeUnit.SECONDS.toMillis((long) k.m807c());
            }
            long j = 0;
            long time;
            long time2;
            if (this.f394h != null) {
                if (this.f390d != null) {
                    time = this.f390d.getTime();
                } else {
                    time = this.f396j;
                }
                time2 = this.f394h.getTime() - time;
                if (time2 > 0) {
                    j = time2;
                }
                return j;
            } else if (this.f392f == null || this.f389c.m775a().m751a().m975l() != null) {
                return 0;
            } else {
                if (this.f390d != null) {
                    time = this.f390d.getTime();
                } else {
                    time = this.f395i;
                }
                time2 = time - this.f392f.getTime();
                if (time2 > 0) {
                    j = time2 / 10;
                }
                return j;
            }
        }

        private long m406d() {
            long j = 0;
            if (this.f390d != null) {
                j = Math.max(0, this.f396j - this.f390d.getTime());
            }
            if (this.f398l != -1) {
                j = Math.max(j, TimeUnit.SECONDS.toMillis((long) this.f398l));
            }
            return (j + (this.f396j - this.f395i)) + (this.f387a - this.f396j);
        }

        private boolean m407e() {
            return this.f389c.m787k().m807c() == -1 && this.f394h == null;
        }

        private static boolean m403a(aa aaVar) {
            if (aaVar.m752a("If-Modified-Since") == null) {
                if (aaVar.m752a("If-None-Match") == null) {
                    return null;
                }
            }
            return true;
        }
    }

    C0464c(aa aaVar, ac acVar) {
        this.f399a = aaVar;
        this.f400b = acVar;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m409a(p021c.ac r3, p021c.aa r4) {
        /*
        r0 = r3.m778b();
        r1 = 0;
        switch(r0) {
            case 200: goto L_0x0030;
            case 203: goto L_0x0030;
            case 204: goto L_0x0030;
            case 300: goto L_0x0030;
            case 301: goto L_0x0030;
            case 302: goto L_0x0009;
            case 307: goto L_0x0009;
            case 308: goto L_0x0030;
            case 404: goto L_0x0030;
            case 405: goto L_0x0030;
            case 410: goto L_0x0030;
            case 414: goto L_0x0030;
            case 501: goto L_0x0030;
            default: goto L_0x0008;
        };
    L_0x0008:
        goto L_0x0046;
    L_0x0009:
        r0 = "Expires";
        r0 = r3.m776a(r0);
        if (r0 != 0) goto L_0x0030;
    L_0x0011:
        r0 = r3.m787k();
        r0 = r0.m807c();
        r2 = -1;
        if (r0 != r2) goto L_0x0030;
    L_0x001c:
        r0 = r3.m787k();
        r0 = r0.m809e();
        if (r0 != 0) goto L_0x0030;
    L_0x0026:
        r0 = r3.m787k();
        r0 = r0.m808d();
        if (r0 == 0) goto L_0x0046;
    L_0x0030:
        r3 = r3.m787k();
        r3 = r3.m806b();
        if (r3 != 0) goto L_0x0045;
    L_0x003a:
        r3 = r4.m757f();
        r3 = r3.m806b();
        if (r3 != 0) goto L_0x0045;
    L_0x0044:
        r1 = 1;
    L_0x0045:
        return r1;
    L_0x0046:
        return r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.a.c.a(c.ac, c.aa):boolean");
    }
}
