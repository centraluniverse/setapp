package p021c.p022a.p023a;

import com.solaredge.common.models.ValidationResult;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import p021c.C0554s;
import p021c.C0554s.C0553a;
import p021c.C0559u;
import p021c.ac;
import p021c.p022a.C0469a;
import p021c.p022a.C0488c;
import p021c.p022a.p025c.C1838h;
import p125d.C1625m;
import p125d.C1629s;
import p125d.C1630t;
import p125d.C1631u;
import p125d.C2283d;
import p125d.C2284e;
import p125d.C2453c;

/* compiled from: CacheInterceptor */
public final class C1832a implements C0559u {
    final C0468e f4536a;

    public C1832a(C0468e c0468e) {
        this.f4536a = c0468e;
    }

    public p021c.ac mo1270a(p021c.C0559u.C0558a r6) throws java.io.IOException {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r5 = this;
        r0 = r5.f4536a;
        if (r0 == 0) goto L_0x000f;
    L_0x0004:
        r0 = r5.f4536a;
        r1 = r6.mo1275a();
        r0 = r0.m420a(r1);
        goto L_0x0010;
    L_0x000f:
        r0 = 0;
    L_0x0010:
        r1 = java.lang.System.currentTimeMillis();
        r3 = new c.a.a.c$a;
        r4 = r6.mo1275a();
        r3.<init>(r1, r4, r0);
        r1 = r3.m408a();
        r2 = r1.f399a;
        r3 = r1.f400b;
        r4 = r5.f4536a;
        if (r4 == 0) goto L_0x002e;
    L_0x0029:
        r4 = r5.f4536a;
        r4.m422a(r1);
    L_0x002e:
        if (r0 == 0) goto L_0x0039;
    L_0x0030:
        if (r3 != 0) goto L_0x0039;
    L_0x0032:
        r1 = r0.m783g();
        p021c.p022a.C0488c.m517a(r1);
    L_0x0039:
        if (r2 != 0) goto L_0x0075;
    L_0x003b:
        if (r3 != 0) goto L_0x0075;
    L_0x003d:
        r0 = new c.ac$a;
        r0.<init>();
        r6 = r6.mo1275a();
        r6 = r0.m763a(r6);
        r0 = p021c.C0564y.HTTP_1_1;
        r6 = r6.m768a(r0);
        r0 = 504; // 0x1f8 float:7.06E-43 double:2.49E-321;
        r6 = r6.m761a(r0);
        r0 = "Unsatisfiable Request (only-if-cached)";
        r6 = r6.m769a(r0);
        r0 = p021c.p022a.C0488c.f473c;
        r6 = r6.m765a(r0);
        r0 = -1;
        r6 = r6.m762a(r0);
        r0 = java.lang.System.currentTimeMillis();
        r6 = r6.m772b(r0);
        r6 = r6.m771a();
        return r6;
    L_0x0075:
        if (r2 != 0) goto L_0x0088;
    L_0x0077:
        r6 = r3.m784h();
        r0 = p021c.p022a.p023a.C1832a.m5040a(r3);
        r6 = r6.m773b(r0);
        r6 = r6.m771a();
        return r6;
    L_0x0088:
        r6 = r6.mo1276a(r2);	 Catch:{ all -> 0x0135 }
        if (r6 != 0) goto L_0x0097;
    L_0x008e:
        if (r0 == 0) goto L_0x0097;
    L_0x0090:
        r0 = r0.m783g();
        p021c.p022a.C0488c.m517a(r0);
    L_0x0097:
        if (r3 == 0) goto L_0x00f2;
    L_0x0099:
        r0 = r6.m778b();
        r1 = 304; // 0x130 float:4.26E-43 double:1.5E-321;
        if (r0 != r1) goto L_0x00eb;
    L_0x00a1:
        r0 = r3.m784h();
        r1 = r3.m782f();
        r2 = r6.m782f();
        r1 = p021c.p022a.p023a.C1832a.m5041a(r1, r2);
        r0 = r0.m767a(r1);
        r1 = r6.m788l();
        r0 = r0.m762a(r1);
        r1 = r6.m789m();
        r0 = r0.m772b(r1);
        r1 = p021c.p022a.p023a.C1832a.m5040a(r3);
        r0 = r0.m773b(r1);
        r1 = p021c.p022a.p023a.C1832a.m5040a(r6);
        r0 = r0.m764a(r1);
        r0 = r0.m771a();
        r6 = r6.m783g();
        r6.close();
        r6 = r5.f4536a;
        r6.m421a();
        r6 = r5.f4536a;
        r6.m423a(r3, r0);
        return r0;
    L_0x00eb:
        r0 = r3.m783g();
        p021c.p022a.C0488c.m517a(r0);
    L_0x00f2:
        r0 = r6.m784h();
        r1 = p021c.p022a.p023a.C1832a.m5040a(r3);
        r0 = r0.m773b(r1);
        r6 = p021c.p022a.p023a.C1832a.m5040a(r6);
        r6 = r0.m764a(r6);
        r6 = r6.m771a();
        r0 = r5.f4536a;
        if (r0 == 0) goto L_0x0134;
    L_0x010e:
        r0 = p021c.p022a.p025c.C0483e.m491b(r6);
        if (r0 == 0) goto L_0x0125;
    L_0x0114:
        r0 = p021c.p022a.p023a.C0464c.m409a(r6, r2);
        if (r0 == 0) goto L_0x0125;
    L_0x011a:
        r0 = r5.f4536a;
        r0 = r0.m419a(r6);
        r6 = r5.m5039a(r0, r6);
        return r6;
    L_0x0125:
        r0 = r2.m753b();
        r0 = p021c.p022a.p025c.C0484f.m492a(r0);
        if (r0 == 0) goto L_0x0134;
    L_0x012f:
        r0 = r5.f4536a;	 Catch:{ IOException -> 0x0134 }
        r0.m424b(r2);	 Catch:{ IOException -> 0x0134 }
    L_0x0134:
        return r6;
    L_0x0135:
        r6 = move-exception;
        if (r0 == 0) goto L_0x013f;
    L_0x0138:
        r0 = r0.m783g();
        p021c.p022a.C0488c.m517a(r0);
    L_0x013f:
        throw r6;
        */
        throw new UnsupportedOperationException("Method not decompiled: c.a.a.a.a(c.u$a):c.ac");
    }

    private static ac m5040a(ac acVar) {
        return (acVar == null || acVar.m783g() == null) ? acVar : acVar.m784h().m765a(null).m771a();
    }

    private ac m5039a(final C0462b c0462b, ac acVar) throws IOException {
        if (c0462b == null) {
            return acVar;
        }
        C1629s a = c0462b.m401a();
        if (a == null) {
            return acVar;
        }
        final C2284e source = acVar.m783g().source();
        final C2283d a2 = C1625m.m4840a(a);
        C1630t c18311 = new C1630t(this) {
            boolean f4531a;
            final /* synthetic */ C1832a f4535e;

            public long read(C2453c c2453c, long j) throws IOException {
                try {
                    j = source.read(c2453c, j);
                    if (j == -1) {
                        if (this.f4531a == null) {
                            this.f4531a = true;
                            a2.close();
                        }
                        return -1;
                    }
                    c2453c.m7711a(a2.mo2516b(), c2453c.m7705a() - j, j);
                    a2.mo2546z();
                    return j;
                } catch (C2453c c2453c2) {
                    if (this.f4531a == null) {
                        this.f4531a = true;
                        c0462b.m402b();
                    }
                    throw c2453c2;
                }
            }

            public C1631u timeout() {
                return source.timeout();
            }

            public void close() throws IOException {
                if (!(this.f4531a || C0488c.m519a((C1630t) this, 100, TimeUnit.MILLISECONDS))) {
                    this.f4531a = true;
                    c0462b.m402b();
                }
                source.close();
            }
        };
        return acVar.m784h().m765a(new C1838h(acVar.m776a("Content-Type"), acVar.m783g().contentLength(), C1625m.m4841a(c18311))).m771a();
    }

    private static C0554s m5041a(C0554s c0554s, C0554s c0554s2) {
        C0553a c0553a = new C0553a();
        int a = c0554s.m920a();
        int i = 0;
        for (int i2 = 0; i2 < a; i2++) {
            String a2 = c0554s.m921a(i2);
            String b = c0554s.m924b(i2);
            if (!ValidationResult.RESULT_WARNING.equalsIgnoreCase(a2) || !b.startsWith("1")) {
                if (!C1832a.m5042a(a2) || c0554s2.m922a(a2) == null) {
                    C0469a.f427a.mo1341a(c0553a, a2, b);
                }
            }
        }
        c0554s = c0554s2.m920a();
        while (i < c0554s) {
            String a3 = c0554s2.m921a(i);
            if (!"Content-Length".equalsIgnoreCase(a3)) {
                if (C1832a.m5042a(a3)) {
                    C0469a.f427a.mo1341a(c0553a, a3, c0554s2.m924b(i));
                }
            }
            i++;
        }
        return c0553a.m914a();
    }

    static boolean m5042a(String str) {
        return ("Connection".equalsIgnoreCase(str) || "Keep-Alive".equalsIgnoreCase(str) || "Proxy-Authenticate".equalsIgnoreCase(str) || "Proxy-Authorization".equalsIgnoreCase(str) || "TE".equalsIgnoreCase(str) || "Trailers".equalsIgnoreCase(str) || "Transfer-Encoding".equalsIgnoreCase(str) || "Upgrade".equalsIgnoreCase(str) != null) ? null : true;
    }
}
