package p021c.p022a.p031i;

import javax.security.auth.x500.X500Principal;

/* compiled from: DistinguishedNameParser */
final class C0518d {
    private final String f619a;
    private final int f620b = this.f619a.length();
    private int f621c;
    private int f622d;
    private int f623e;
    private int f624f;
    private char[] f625g;

    C0518d(X500Principal x500Principal) {
        this.f619a = x500Principal.getName("RFC2253");
    }

    private String m716a() {
        while (this.f621c < this.f620b && this.f625g[this.f621c] == ' ') {
            this.f621c++;
        }
        if (this.f621c == this.f620b) {
            return null;
        }
        this.f622d = this.f621c;
        this.f621c++;
        while (this.f621c < this.f620b && this.f625g[this.f621c] != '=' && this.f625g[this.f621c] != ' ') {
            this.f621c++;
        }
        if (this.f621c >= this.f620b) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected end of DN: ");
            stringBuilder.append(this.f619a);
            throw new IllegalStateException(stringBuilder.toString());
        }
        this.f623e = this.f621c;
        if (this.f625g[this.f621c] == ' ') {
            while (this.f621c < this.f620b && this.f625g[this.f621c] != '=' && this.f625g[this.f621c] == ' ') {
                this.f621c++;
            }
            if (this.f625g[this.f621c] != '=' || this.f621c == this.f620b) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("Unexpected end of DN: ");
                stringBuilder.append(this.f619a);
                throw new IllegalStateException(stringBuilder.toString());
            }
        }
        this.f621c++;
        while (this.f621c < this.f620b && this.f625g[this.f621c] == ' ') {
            this.f621c++;
        }
        if (this.f623e - this.f622d > 4 && this.f625g[this.f622d + 3] == '.' && ((this.f625g[this.f622d] == 'O' || this.f625g[this.f622d] == 'o') && ((this.f625g[this.f622d + 1] == 'I' || this.f625g[this.f622d + 1] == 'i') && (this.f625g[this.f622d + 2] == 'D' || this.f625g[this.f622d + 2] == 'd')))) {
            this.f622d += 4;
        }
        return new String(this.f625g, this.f622d, this.f623e - this.f622d);
    }

    private String m717b() {
        this.f621c++;
        this.f622d = this.f621c;
        this.f623e = this.f622d;
        while (this.f621c != this.f620b) {
            if (this.f625g[this.f621c] == '\"') {
                this.f621c++;
                while (this.f621c < this.f620b && this.f625g[this.f621c] == ' ') {
                    this.f621c++;
                }
                return new String(this.f625g, this.f622d, this.f623e - this.f622d);
            }
            if (this.f625g[this.f621c] == '\\') {
                this.f625g[this.f623e] = m720e();
            } else {
                this.f625g[this.f623e] = this.f625g[this.f621c];
            }
            this.f621c++;
            this.f623e++;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected end of DN: ");
        stringBuilder.append(this.f619a);
        throw new IllegalStateException(stringBuilder.toString());
    }

    private String m718c() {
        if (this.f621c + 4 >= this.f620b) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected end of DN: ");
            stringBuilder.append(this.f619a);
            throw new IllegalStateException(stringBuilder.toString());
        }
        int i;
        int i2;
        this.f622d = this.f621c;
        this.f621c++;
        while (this.f621c != this.f620b && this.f625g[this.f621c] != '+' && this.f625g[this.f621c] != ',') {
            byte[] bArr;
            int i3;
            if (this.f625g[this.f621c] == ';') {
                break;
            } else if (this.f625g[this.f621c] == ' ') {
                this.f623e = this.f621c;
                this.f621c++;
                while (this.f621c < this.f620b && this.f625g[this.f621c] == ' ') {
                    this.f621c++;
                }
                i = this.f623e - this.f622d;
                if (i >= 5) {
                    if ((i & 1) == 0) {
                        bArr = new byte[(i / 2)];
                        i2 = this.f622d + 1;
                        for (i3 = 0; i3 < bArr.length; i3++) {
                            bArr[i3] = (byte) m715a(i2);
                            i2 += 2;
                        }
                        return new String(this.f625g, this.f622d, i);
                    }
                }
                stringBuilder = new StringBuilder();
                stringBuilder.append("Unexpected end of DN: ");
                stringBuilder.append(this.f619a);
                throw new IllegalStateException(stringBuilder.toString());
            } else {
                if (this.f625g[this.f621c] >= 'A' && this.f625g[this.f621c] <= 'F') {
                    char[] cArr = this.f625g;
                    i3 = this.f621c;
                    cArr[i3] = (char) (cArr[i3] + 32);
                }
                this.f621c++;
            }
        }
        this.f623e = this.f621c;
        i = this.f623e - this.f622d;
        if (i >= 5) {
            if ((i & 1) == 0) {
                bArr = new byte[(i / 2)];
                i2 = this.f622d + 1;
                for (i3 = 0; i3 < bArr.length; i3++) {
                    bArr[i3] = (byte) m715a(i2);
                    i2 += 2;
                }
                return new String(this.f625g, this.f622d, i);
            }
        }
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected end of DN: ");
        stringBuilder.append(this.f619a);
        throw new IllegalStateException(stringBuilder.toString());
    }

    private String m719d() {
        this.f622d = this.f621c;
        this.f623e = this.f621c;
        while (this.f621c < this.f620b) {
            char c = this.f625g[this.f621c];
            char[] cArr;
            if (c != ' ') {
                if (c != ';') {
                    int i;
                    if (c != '\\') {
                        switch (c) {
                            case '+':
                            case ',':
                                break;
                            default:
                                cArr = this.f625g;
                                i = this.f623e;
                                this.f623e = i + 1;
                                cArr[i] = this.f625g[this.f621c];
                                this.f621c++;
                                continue;
                        }
                    } else {
                        cArr = this.f625g;
                        i = this.f623e;
                        this.f623e = i + 1;
                        cArr[i] = m720e();
                        this.f621c++;
                    }
                }
                return new String(this.f625g, this.f622d, this.f623e - this.f622d);
            }
            this.f624f = this.f623e;
            this.f621c++;
            cArr = this.f625g;
            int i2 = this.f623e;
            this.f623e = i2 + 1;
            cArr[i2] = ' ';
            while (this.f621c < this.f620b && this.f625g[this.f621c] == ' ') {
                cArr = this.f625g;
                i2 = this.f623e;
                this.f623e = i2 + 1;
                cArr[i2] = ' ';
                this.f621c++;
            }
            if (this.f621c == this.f620b || this.f625g[this.f621c] == ',' || this.f625g[this.f621c] == '+' || this.f625g[this.f621c] == ';') {
                return new String(this.f625g, this.f622d, this.f624f - this.f622d);
            }
        }
        return new String(this.f625g, this.f622d, this.f623e - this.f622d);
    }

    private char m720e() {
        this.f621c++;
        if (this.f621c == this.f620b) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected end of DN: ");
            stringBuilder.append(this.f619a);
            throw new IllegalStateException(stringBuilder.toString());
        }
        char c = this.f625g[this.f621c];
        if (!(c == ' ' || c == '%' || c == '\\' || c == '_')) {
            switch (c) {
                case '\"':
                case '#':
                    break;
                default:
                    switch (c) {
                        case '*':
                        case '+':
                        case ',':
                            break;
                        default:
                            switch (c) {
                                case ';':
                                case '<':
                                case '=':
                                case '>':
                                    break;
                                default:
                                    return m721f();
                            }
                    }
            }
        }
        return this.f625g[this.f621c];
    }

    private char m721f() {
        int a = m715a(this.f621c);
        this.f621c++;
        if (a < 128) {
            return (char) a;
        }
        if (a < 192 || a > 247) {
            return '?';
        }
        int i;
        if (a <= 223) {
            a &= 31;
            i = 1;
        } else if (a <= 239) {
            i = 2;
            a &= 15;
        } else {
            i = 3;
            a &= 7;
        }
        int i2 = 0;
        while (i2 < i) {
            this.f621c++;
            if (this.f621c != this.f620b) {
                if (this.f625g[this.f621c] == '\\') {
                    this.f621c++;
                    int a2 = m715a(this.f621c);
                    this.f621c++;
                    if ((a2 & 192) != 128) {
                        return '?';
                    }
                    a = (a << 6) + (a2 & 63);
                    i2++;
                }
            }
            return '?';
        }
        return (char) a;
    }

    private int m715a(int i) {
        int i2 = i + 1;
        if (i2 >= this.f620b) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Malformed DN: ");
            stringBuilder.append(this.f619a);
            throw new IllegalStateException(stringBuilder.toString());
        }
        i = this.f625g[i];
        if (i >= 48 && i <= 57) {
            i -= 48;
        } else if (i >= 97 && i <= 102) {
            i -= 87;
        } else if (i < 65 || i > 70) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Malformed DN: ");
            stringBuilder.append(this.f619a);
            throw new IllegalStateException(stringBuilder.toString());
        } else {
            i -= 55;
        }
        char c = this.f625g[i2];
        if (c >= '0' && c <= '9') {
            i2 = c - 48;
        } else if (c >= 'a' && c <= 'f') {
            i2 = c - 87;
        } else if (c < 'A' || c > 'F') {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Malformed DN: ");
            stringBuilder.append(this.f619a);
            throw new IllegalStateException(stringBuilder.toString());
        } else {
            i2 = c - 55;
        }
        return (i << 4) + i2;
    }

    public String m722a(String str) {
        this.f621c = 0;
        this.f622d = 0;
        this.f623e = 0;
        this.f624f = 0;
        this.f625g = this.f619a.toCharArray();
        String a = m716a();
        if (a == null) {
            return null;
        }
        StringBuilder stringBuilder;
        do {
            String str2 = "";
            if (this.f621c == this.f620b) {
                return null;
            }
            switch (this.f625g[this.f621c]) {
                case '\"':
                    str2 = m717b();
                    break;
                case '#':
                    str2 = m718c();
                    break;
                case '+':
                case ',':
                case ';':
                    break;
                default:
                    str2 = m719d();
                    break;
            }
            if (str.equalsIgnoreCase(a)) {
                return str2;
            }
            if (this.f621c >= this.f620b) {
                return null;
            }
            if (this.f625g[this.f621c] != ',') {
                if (this.f625g[this.f621c] != ';') {
                    if (this.f625g[this.f621c] != '+') {
                        stringBuilder = new StringBuilder();
                        stringBuilder.append("Malformed DN: ");
                        stringBuilder.append(this.f619a);
                        throw new IllegalStateException(stringBuilder.toString());
                    }
                }
            }
            this.f621c++;
            a = m716a();
        } while (a != null);
        stringBuilder = new StringBuilder();
        stringBuilder.append("Malformed DN: ");
        stringBuilder.append(this.f619a);
        throw new IllegalStateException(stringBuilder.toString());
    }
}
