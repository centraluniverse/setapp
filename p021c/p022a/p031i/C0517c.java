package p021c.p022a.p031i;

import java.security.cert.Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.X509TrustManager;
import p021c.p022a.p029g.C0515e;

/* compiled from: CertificateChainCleaner */
public abstract class C0517c {
    public abstract List<Certificate> mo1314a(List<Certificate> list, String str) throws SSLPeerUnverifiedException;

    public static C0517c m713a(X509TrustManager x509TrustManager) {
        return C0515e.m695b().mo1316a(x509TrustManager);
    }
}
