package p021c.p022a.p029g;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import p021c.C0564y;
import p021c.C1883x;
import p021c.p022a.p031i.C0517c;
import p021c.p022a.p031i.C0520f;
import p021c.p022a.p031i.C1867a;
import p021c.p022a.p031i.C1868b;
import p125d.C2453c;

/* compiled from: Platform */
public class C0515e {
    private static final C0515e f609a = C0515e.m693a();
    private static final Logger f610b = Logger.getLogger(C1883x.class.getName());

    public String mo1318a(SSLSocket sSLSocket) {
        return null;
    }

    public void mo1322a(SSLSocket sSLSocket, String str, List<C0564y> list) {
    }

    public void mo1325b(SSLSocket sSLSocket) {
    }

    public boolean mo1324b(String str) {
        return true;
    }

    public static C0515e m695b() {
        return f609a;
    }

    public void mo1321a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    public void mo1319a(int i, String str, Throwable th) {
        f610b.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    public Object mo1317a(String str) {
        return f610b.isLoggable(Level.FINE) ? new Throwable(str) : null;
    }

    public void mo1320a(String str, Object obj) {
        if (obj == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append(" To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);");
            str = stringBuilder.toString();
        }
        mo1319a(5, str, (Throwable) obj);
    }

    public static List<String> m694a(List<C0564y> list) {
        List<String> arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            C0564y c0564y = (C0564y) list.get(i);
            if (c0564y != C0564y.HTTP_1_0) {
                arrayList.add(c0564y.toString());
            }
        }
        return arrayList;
    }

    public C0517c mo1316a(X509TrustManager x509TrustManager) {
        return new C1867a(mo1323b(x509TrustManager));
    }

    private static C0515e m693a() {
        C0515e a = C1864a.m5153a();
        if (a != null) {
            return a;
        }
        a = C1865b.m5166a();
        if (a != null) {
            return a;
        }
        a = C1866c.m5169a();
        if (a != null) {
            return a;
        }
        return new C0515e();
    }

    static byte[] m696b(List<C0564y> list) {
        C2453c c2453c = new C2453c();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            C0564y c0564y = (C0564y) list.get(i);
            if (c0564y != C0564y.HTTP_1_0) {
                c2453c.m7725b(c0564y.toString().length());
                c2453c.m7713a(c0564y.toString());
            }
        }
        return c2453c.mo2545v();
    }

    public C0520f mo1323b(X509TrustManager x509TrustManager) {
        return new C1868b(x509TrustManager.getAcceptedIssuers());
    }
}
