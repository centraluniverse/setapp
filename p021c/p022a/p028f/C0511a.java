package p021c.p022a.p028f;

import java.io.File;
import java.io.IOException;

/* compiled from: FileSystem */
public interface C0511a {
    public static final C0511a f599a = new C18611();

    /* compiled from: FileSystem */
    class C18611 implements C0511a {
        C18611() {
        }

        public void mo1310a(File file) throws IOException {
            if (!file.delete() && file.exists()) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("failed to delete ");
                stringBuilder.append(file);
                throw new IOException(stringBuilder.toString());
            }
        }

        public boolean mo1312b(File file) {
            return file.exists();
        }

        public long mo1313c(File file) {
            return file.length();
        }

        public void mo1311a(File file, File file2) throws IOException {
            mo1310a(file2);
            if (!file.renameTo(file2)) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("failed to rename ");
                stringBuilder.append(file);
                stringBuilder.append(" to ");
                stringBuilder.append(file2);
                throw new IOException(stringBuilder.toString());
            }
        }
    }

    void mo1310a(File file) throws IOException;

    void mo1311a(File file, File file2) throws IOException;

    boolean mo1312b(File file);

    long mo1313c(File file);
}
