package p008b.p009a.p010a.p011a;

/* compiled from: InitializationCallback */
public interface C0456f<T> {
    public static final C0456f f383d = new C1829a();

    /* compiled from: InitializationCallback */
    public static class C1829a implements C0456f<Object> {
        public void mo1261a(Exception exception) {
        }

        public void mo1262a(Object obj) {
        }

        private C1829a() {
        }
    }

    void mo1261a(Exception exception);

    void mo1262a(T t);
}
