package p008b.p009a.p010a.p011a;

/* compiled from: KitInfo */
public class C0460k {
    private final String f384a;
    private final String f385b;
    private final String f386c;

    public C0460k(String str, String str2, String str3) {
        this.f384a = str;
        this.f385b = str2;
        this.f386c = str3;
    }

    public String m387a() {
        return this.f384a;
    }

    public String m388b() {
        return this.f385b;
    }

    public String m389c() {
        return this.f386c;
    }
}
