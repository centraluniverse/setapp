package p008b.p009a.p010a.p011a;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ActivityLifecycleManager */
public class C0450a {
    private final Application f355a;
    private C0352a f356b;

    /* compiled from: ActivityLifecycleManager */
    private static class C0352a {
        private final Set<ActivityLifecycleCallbacks> f117a = new HashSet();
        private final Application f118b;

        C0352a(Application application) {
            this.f118b = application;
        }

        @TargetApi(14)
        private void m82a() {
            for (ActivityLifecycleCallbacks unregisterActivityLifecycleCallbacks : this.f117a) {
                this.f118b.unregisterActivityLifecycleCallbacks(unregisterActivityLifecycleCallbacks);
            }
        }

        @TargetApi(14)
        private boolean m85a(final C0353b c0353b) {
            if (this.f118b == null) {
                return null;
            }
            ActivityLifecycleCallbacks c03511 = new ActivityLifecycleCallbacks(this) {
                final /* synthetic */ C0352a f116b;

                public void onActivityCreated(Activity activity, Bundle bundle) {
                    c0353b.onActivityCreated(activity, bundle);
                }

                public void onActivityStarted(Activity activity) {
                    c0353b.onActivityStarted(activity);
                }

                public void onActivityResumed(Activity activity) {
                    c0353b.onActivityResumed(activity);
                }

                public void onActivityPaused(Activity activity) {
                    c0353b.onActivityPaused(activity);
                }

                public void onActivityStopped(Activity activity) {
                    c0353b.onActivityStopped(activity);
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                    c0353b.onActivitySaveInstanceState(activity, bundle);
                }

                public void onActivityDestroyed(Activity activity) {
                    c0353b.onActivityDestroyed(activity);
                }
            };
            this.f118b.registerActivityLifecycleCallbacks(c03511);
            this.f117a.add(c03511);
            return true;
        }
    }

    /* compiled from: ActivityLifecycleManager */
    public static abstract class C0353b {
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    public C0450a(Context context) {
        this.f355a = (Application) context.getApplicationContext();
        if (VERSION.SDK_INT >= 14) {
            this.f356b = new C0352a(this.f355a);
        }
    }

    public boolean m354a(C0353b c0353b) {
        return (this.f356b == null || this.f356b.m85a(c0353b) == null) ? null : true;
    }

    public void m353a() {
        if (this.f356b != null) {
            this.f356b.m82a();
        }
    }
}
