package p008b.p009a.p010a.p011a;

import android.util.Log;

/* compiled from: DefaultLogger */
public class C1826b implements C0461l {
    private int f4515a;

    public C1826b(int i) {
        this.f4515a = i;
    }

    public C1826b() {
        this.f4515a = 4;
    }

    public boolean mo1251a(String str, int i) {
        return this.f4515a <= i ? true : null;
    }

    public void mo1250a(String str, String str2, Throwable th) {
        if (mo1251a(str, 3)) {
            Log.d(str, str2, th);
        }
    }

    public void m5019b(String str, String str2, Throwable th) {
        if (mo1251a(str, 2)) {
            Log.v(str, str2, th);
        }
    }

    public void m5021c(String str, String str2, Throwable th) {
        if (mo1251a(str, 4)) {
            Log.i(str, str2, th);
        }
    }

    public void mo1255d(String str, String str2, Throwable th) {
        if (mo1251a(str, 5)) {
            Log.w(str, str2, th);
        }
    }

    public void mo1257e(String str, String str2, Throwable th) {
        if (mo1251a(str, 6)) {
            Log.e(str, str2, th);
        }
    }

    public void mo1249a(String str, String str2) {
        mo1250a(str, str2, null);
    }

    public void mo1252b(String str, String str2) {
        m5019b(str, str2, null);
    }

    public void mo1253c(String str, String str2) {
        m5021c(str, str2, null);
    }

    public void mo1254d(String str, String str2) {
        mo1255d(str, str2, null);
    }

    public void mo1256e(String str, String str2) {
        mo1257e(str, str2, null);
    }

    public void mo1247a(int i, String str, String str2) {
        mo1248a(i, str, str2, false);
    }

    public void mo1248a(int i, String str, String str2, boolean z) {
        if (z || mo1251a(str, i)) {
            Log.println(i, str, str2);
        }
    }
}
