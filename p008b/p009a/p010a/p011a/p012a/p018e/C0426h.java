package p008b.p009a.p010a.p011a.p012a.p018e;

import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import p008b.p009a.p010a.p011a.C0452c;

/* compiled from: PinningTrustManager */
class C0426h implements X509TrustManager {
    private static final X509Certificate[] f263a = new X509Certificate[0];
    private final TrustManager[] f264b;
    private final C0427i f265c;
    private final long f266d;
    private final List<byte[]> f267e = new LinkedList();
    private final Set<X509Certificate> f268f = Collections.synchronizedSet(new HashSet());

    public C0426h(C0427i c0427i, C0425g c0425g) {
        this.f264b = m329a(c0427i);
        this.f265c = c0427i;
        this.f266d = c0425g.getPinCreationTimeInMillis();
        c0427i = c0425g.getPins();
        for (String a : c0427i) {
            this.f267e.add(m328a(a));
        }
    }

    private TrustManager[] m329a(C0427i c0427i) {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance("X509");
            instance.init(c0427i.f269a);
            return instance.getTrustManagers();
        } catch (C0427i c0427i2) {
            throw new AssertionError(c0427i2);
        } catch (C0427i c0427i22) {
            throw new AssertionError(c0427i22);
        }
    }

    private boolean m327a(X509Certificate x509Certificate) throws CertificateException {
        try {
            x509Certificate = MessageDigest.getInstance("SHA1").digest(x509Certificate.getPublicKey().getEncoded());
            for (byte[] equals : this.f267e) {
                if (Arrays.equals(equals, x509Certificate)) {
                    return true;
                }
            }
            return null;
        } catch (X509Certificate x509Certificate2) {
            throw new CertificateException(x509Certificate2);
        }
    }

    private void m326a(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        for (TrustManager trustManager : this.f264b) {
            ((X509TrustManager) trustManager).checkServerTrusted(x509CertificateArr, str);
        }
    }

    private void m325a(X509Certificate[] x509CertificateArr) throws CertificateException {
        if (this.f266d == -1 || System.currentTimeMillis() - this.f266d <= 15552000000L) {
            x509CertificateArr = C0415a.m260a(x509CertificateArr, this.f265c);
            int length = x509CertificateArr.length;
            int i = 0;
            while (i < length) {
                if (!m327a(x509CertificateArr[i])) {
                    i++;
                } else {
                    return;
                }
            }
            throw new CertificateException("No valid pins found in chain!");
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Certificate pins are stale, (");
        stringBuilder.append(System.currentTimeMillis() - this.f266d);
        stringBuilder.append(" millis vs ");
        stringBuilder.append(15552000000L);
        stringBuilder.append(" millis) falling back to system trust.");
        C0452c.m367h().mo1254d("Fabric", stringBuilder.toString());
    }

    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        throw new CertificateException("Client certificates not supported!");
    }

    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        if (!this.f268f.contains(x509CertificateArr[0])) {
            m326a(x509CertificateArr, str);
            m325a(x509CertificateArr);
            this.f268f.add(x509CertificateArr[0]);
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        return f263a;
    }

    private byte[] m328a(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }
}
