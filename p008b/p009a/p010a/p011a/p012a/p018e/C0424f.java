package p008b.p009a.p010a.p011a.p012a.p018e;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

/* compiled from: NetworkUtils */
public final class C0424f {
    public static final SSLSocketFactory m324a(C0425g c0425g) throws KeyManagementException, NoSuchAlgorithmException {
        SSLContext instance = SSLContext.getInstance("TLS");
        C0426h c0426h = new C0426h(new C0427i(c0425g.getKeyStoreStream(), c0425g.getKeyStorePassword()), c0425g);
        instance.init(null, new TrustManager[]{c0426h}, null);
        return instance.getSocketFactory();
    }
}
