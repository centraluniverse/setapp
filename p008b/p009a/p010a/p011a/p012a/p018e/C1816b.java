package p008b.p009a.p010a.p011a.p012a.p018e;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import p008b.p009a.p010a.p011a.C0461l;
import p008b.p009a.p010a.p011a.C1826b;

/* compiled from: DefaultHttpRequestFactory */
public class C1816b implements C0423e {
    private final C0461l f4495a;
    private C0425g f4496b;
    private SSLSocketFactory f4497c;
    private boolean f4498d;

    public C1816b() {
        this(new C1826b());
    }

    public C1816b(C0461l c0461l) {
        this.f4495a = c0461l;
    }

    public void mo1233a(C0425g c0425g) {
        if (this.f4496b != c0425g) {
            this.f4496b = c0425g;
            m4967a();
        }
    }

    private synchronized void m4967a() {
        this.f4498d = false;
        this.f4497c = null;
    }

    public C0422d mo1231a(C0417c c0417c, String str) {
        return mo1232a(c0417c, str, Collections.emptyMap());
    }

    public C0422d mo1232a(C0417c c0417c, String str, Map<String, String> map) {
        switch (c0417c) {
            case GET:
                c0417c = C0422d.m268a((CharSequence) str, (Map) map, true);
                break;
            case POST:
                c0417c = C0422d.m273b((CharSequence) str, (Map) map, true);
                break;
            case PUT:
                c0417c = C0422d.m276d((CharSequence) str);
                break;
            case DELETE:
                c0417c = C0422d.m277e((CharSequence) str);
                break;
            default:
                throw new IllegalArgumentException("Unsupported HTTP method!");
        }
        if (!(m4968a(str) == null || this.f4496b == null)) {
            str = m4969b();
            if (str != null) {
                ((HttpsURLConnection) c0417c.m295a()).setSSLSocketFactory(str);
            }
        }
        return c0417c;
    }

    private boolean m4968a(String str) {
        return (str == null || str.toLowerCase(Locale.US).startsWith("https") == null) ? null : true;
    }

    private synchronized SSLSocketFactory m4969b() {
        if (this.f4497c == null && !this.f4498d) {
            this.f4497c = m4970c();
        }
        return this.f4497c;
    }

    private synchronized SSLSocketFactory m4970c() {
        SSLSocketFactory a;
        this.f4498d = true;
        try {
            a = C0424f.m324a(this.f4496b);
            this.f4495a.mo1249a("Fabric", "Custom SSL pinning enabled");
        } catch (Throwable e) {
            this.f4495a.mo1257e("Fabric", "Exception while validating pinned certs", e);
            return null;
        }
        return a;
    }
}
