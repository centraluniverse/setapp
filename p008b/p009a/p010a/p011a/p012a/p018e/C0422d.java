package p008b.p009a.p010a.p011a.p012a.p018e;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URL;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

/* compiled from: HttpRequest */
public class C0422d {
    private static final String[] f251b = new String[0];
    private static C0418b f252c = C0418b.f249a;
    public final URL f253a;
    private HttpURLConnection f254d = null;
    private final String f255e;
    private C0421e f256f;
    private boolean f257g;
    private boolean f258h = true;
    private boolean f259i = false;
    private int f260j = 8192;
    private String f261k;
    private int f262l;

    /* compiled from: HttpRequest */
    public interface C0418b {
        public static final C0418b f249a = new C18181();

        /* compiled from: HttpRequest */
        static class C18181 implements C0418b {
            C18181() {
            }

            public HttpURLConnection mo1235a(URL url) throws IOException {
                return (HttpURLConnection) url.openConnection();
            }

            public HttpURLConnection mo1236a(URL url, Proxy proxy) throws IOException {
                return (HttpURLConnection) url.openConnection(proxy);
            }
        }

        HttpURLConnection mo1235a(URL url) throws IOException;

        HttpURLConnection mo1236a(URL url, Proxy proxy) throws IOException;
    }

    /* compiled from: HttpRequest */
    public static class C0419c extends RuntimeException {
        public /* synthetic */ Throwable getCause() {
            return m263a();
        }

        protected C0419c(IOException iOException) {
            super(iOException);
        }

        public IOException m263a() {
            return (IOException) super.getCause();
        }
    }

    /* compiled from: HttpRequest */
    protected static abstract class C0420d<V> implements Callable<V> {
        protected abstract V mo2441b() throws C0419c, IOException;

        protected abstract void mo1234c() throws IOException;

        protected C0420d() {
        }

        public V call() throws C0419c {
            Throwable th;
            Object obj = 1;
            try {
                V b = mo2441b();
                try {
                    mo1234c();
                    return b;
                } catch (IOException e) {
                    throw new C0419c(e);
                }
            } catch (C0419c e2) {
                throw e2;
            } catch (IOException e3) {
                throw new C0419c(e3);
            } catch (Throwable th2) {
                th = th2;
                mo1234c();
                throw th;
            }
        }
    }

    /* compiled from: HttpRequest */
    public static class C0421e extends BufferedOutputStream {
        private final CharsetEncoder f250a;

        public C0421e(OutputStream outputStream, String str, int i) {
            super(outputStream, i);
            this.f250a = Charset.forName(C0422d.m279f(str)).newEncoder();
        }

        public C0421e m266a(String str) throws IOException {
            str = this.f250a.encode(CharBuffer.wrap(str));
            super.write(str.array(), 0, str.limit());
            return this;
        }
    }

    /* compiled from: HttpRequest */
    protected static abstract class C1817a<V> extends C0420d<V> {
        private final Closeable f4499a;
        private final boolean f4500b;

        protected C1817a(Closeable closeable, boolean z) {
            this.f4499a = closeable;
            this.f4500b = z;
        }

        protected void mo1234c() throws java.io.IOException {
            /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
            /*
            r1 = this;
            r0 = r1.f4499a;
            r0 = r0 instanceof java.io.Flushable;
            if (r0 == 0) goto L_0x000d;
        L_0x0006:
            r0 = r1.f4499a;
            r0 = (java.io.Flushable) r0;
            r0.flush();
        L_0x000d:
            r0 = r1.f4500b;
            if (r0 == 0) goto L_0x0017;
        L_0x0011:
            r0 = r1.f4499a;	 Catch:{ IOException -> 0x001c }
            r0.close();	 Catch:{ IOException -> 0x001c }
            goto L_0x001c;
        L_0x0017:
            r0 = r1.f4499a;
            r0.close();
        L_0x001c:
            return;
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.e.d.a.c():void");
        }
    }

    private static String m279f(String str) {
        return (str == null || str.length() <= 0) ? "UTF-8" : str;
    }

    private static StringBuilder m271a(String str, StringBuilder stringBuilder) {
        if (str.indexOf(58) + 2 == str.lastIndexOf(47)) {
            stringBuilder.append('/');
        }
        return stringBuilder;
    }

    private static StringBuilder m274b(String str, StringBuilder stringBuilder) {
        int indexOf = str.indexOf(63);
        int length = stringBuilder.length() - 1;
        if (indexOf == -1) {
            stringBuilder.append('?');
        } else if (indexOf < length && str.charAt(length) != 38) {
            stringBuilder.append('&');
        }
        return stringBuilder;
    }

    public static String m269a(CharSequence charSequence) throws C0419c {
        try {
            URL url = new URL(charSequence.toString());
            charSequence = url.getHost();
            int port = url.getPort();
            if (port != -1) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(charSequence);
                stringBuilder.append(':');
                stringBuilder.append(Integer.toString(port));
                charSequence = stringBuilder.toString();
            }
            try {
                charSequence = new URI(url.getProtocol(), charSequence, url.getPath(), url.getQuery(), null).toASCIIString();
                int indexOf = charSequence.indexOf(63);
                if (indexOf <= 0) {
                    return charSequence;
                }
                indexOf++;
                if (indexOf >= charSequence.length()) {
                    return charSequence;
                }
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append(charSequence.substring(0, indexOf));
                stringBuilder2.append(charSequence.substring(indexOf).replace("+", "%2B"));
                return stringBuilder2.toString();
            } catch (CharSequence charSequence2) {
                IOException iOException = new IOException("Parsing URI failed");
                iOException.initCause(charSequence2);
                throw new C0419c(iOException);
            }
        } catch (CharSequence charSequence22) {
            throw new C0419c(charSequence22);
        }
    }

    public static String m270a(CharSequence charSequence, Map<?, ?> map) {
        String charSequence2 = charSequence.toString();
        if (map != null) {
            if (!map.isEmpty()) {
                StringBuilder stringBuilder = new StringBuilder(charSequence2);
                C0422d.m271a(charSequence2, stringBuilder);
                C0422d.m274b(charSequence2, stringBuilder);
                charSequence = map.entrySet().iterator();
                Entry entry = (Entry) charSequence.next();
                stringBuilder.append(entry.getKey().toString());
                stringBuilder.append('=');
                map = entry.getValue();
                if (map != null) {
                    stringBuilder.append(map);
                }
                while (charSequence.hasNext() != null) {
                    stringBuilder.append('&');
                    entry = (Entry) charSequence.next();
                    stringBuilder.append(entry.getKey().toString());
                    stringBuilder.append('=');
                    map = entry.getValue();
                    if (map != null) {
                        stringBuilder.append(map);
                    }
                }
                return stringBuilder.toString();
            }
        }
        return charSequence2;
    }

    public static C0422d m272b(CharSequence charSequence) throws C0419c {
        return new C0422d(charSequence, "GET");
    }

    public static C0422d m268a(CharSequence charSequence, Map<?, ?> map, boolean z) {
        charSequence = C0422d.m270a(charSequence, (Map) map);
        if (z) {
            charSequence = C0422d.m269a(charSequence);
        }
        return C0422d.m272b(charSequence);
    }

    public static C0422d m275c(CharSequence charSequence) throws C0419c {
        return new C0422d(charSequence, "POST");
    }

    public static C0422d m273b(CharSequence charSequence, Map<?, ?> map, boolean z) {
        charSequence = C0422d.m270a(charSequence, (Map) map);
        if (z) {
            charSequence = C0422d.m269a(charSequence);
        }
        return C0422d.m275c(charSequence);
    }

    public static C0422d m276d(CharSequence charSequence) throws C0419c {
        return new C0422d(charSequence, "PUT");
    }

    public static C0422d m277e(CharSequence charSequence) throws C0419c {
        return new C0422d(charSequence, "DELETE");
    }

    public C0422d(CharSequence charSequence, String str) throws C0419c {
        try {
            this.f253a = new URL(charSequence.toString());
            this.f255e = str;
        } catch (CharSequence charSequence2) {
            throw new C0419c(charSequence2);
        }
    }

    private Proxy m280q() {
        return new Proxy(Type.HTTP, new InetSocketAddress(this.f261k, this.f262l));
    }

    private HttpURLConnection m281r() {
        try {
            HttpURLConnection a;
            if (this.f261k != null) {
                a = f252c.mo1236a(this.f253a, m280q());
            } else {
                a = f252c.mo1235a(this.f253a);
            }
            a.setRequestMethod(this.f255e);
            return a;
        } catch (IOException e) {
            throw new C0419c(e);
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(m320p());
        stringBuilder.append(' ');
        stringBuilder.append(m319o());
        return stringBuilder.toString();
    }

    public HttpURLConnection m295a() {
        if (this.f254d == null) {
            this.f254d = m281r();
        }
        return this.f254d;
    }

    public int m296b() throws C0419c {
        try {
            m315k();
            return m295a().getResponseCode();
        } catch (IOException e) {
            throw new C0419c(e);
        }
    }

    public boolean m302c() throws C0419c {
        return 200 == m296b();
    }

    protected ByteArrayOutputStream m305d() {
        int j = m314j();
        if (j > 0) {
            return new ByteArrayOutputStream(j);
        }
        return new ByteArrayOutputStream();
    }

    public String m294a(String str) throws C0419c {
        OutputStream d = m305d();
        try {
            m284a(m310f(), d);
            return d.toString(C0422d.m279f(str));
        } catch (String str2) {
            throw new C0419c(str2);
        }
    }

    public String m307e() throws C0419c {
        return m294a(m312h());
    }

    public BufferedInputStream m310f() throws C0419c {
        return new BufferedInputStream(m311g(), this.f260j);
    }

    public InputStream m311g() throws C0419c {
        InputStream inputStream;
        if (m296b() < 400) {
            try {
                inputStream = m295a().getInputStream();
            } catch (IOException e) {
                throw new C0419c(e);
            }
        }
        inputStream = m295a().getErrorStream();
        if (inputStream == null) {
            try {
                inputStream = m295a().getInputStream();
            } catch (IOException e2) {
                throw new C0419c(e2);
            }
        }
        if (this.f259i) {
            if ("gzip".equals(m313i())) {
                try {
                    return new GZIPInputStream(inputStream);
                } catch (IOException e22) {
                    throw new C0419c(e22);
                }
            }
        }
        return inputStream;
    }

    public C0422d m283a(int i) {
        m295a().setConnectTimeout(i);
        return this;
    }

    public C0422d m286a(String str, String str2) {
        m295a().setRequestProperty(str, str2);
        return this;
    }

    public C0422d m292a(Entry<String, String> entry) {
        return m286a((String) entry.getKey(), (String) entry.getValue());
    }

    public String m298b(String str) throws C0419c {
        m316l();
        return m295a().getHeaderField(str);
    }

    public int m300c(String str) throws C0419c {
        return m282a(str, -1);
    }

    public int m282a(String str, int i) throws C0419c {
        m316l();
        return m295a().getHeaderFieldInt(str, i);
    }

    public String m299b(String str, String str2) {
        return m301c(m298b(str), str2);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected java.lang.String m301c(java.lang.String r9, java.lang.String r10) {
        /*
        r8 = this;
        r0 = 0;
        if (r9 == 0) goto L_0x0071;
    L_0x0003:
        r1 = r9.length();
        if (r1 != 0) goto L_0x000a;
    L_0x0009:
        goto L_0x0071;
    L_0x000a:
        r1 = r9.length();
        r2 = 59;
        r3 = r9.indexOf(r2);
        r4 = 1;
        r3 = r3 + r4;
        if (r3 == 0) goto L_0x0070;
    L_0x0018:
        if (r3 != r1) goto L_0x001b;
    L_0x001a:
        goto L_0x0070;
    L_0x001b:
        r5 = r9.indexOf(r2, r3);
        r6 = -1;
        if (r5 != r6) goto L_0x0023;
    L_0x0022:
        r5 = r1;
    L_0x0023:
        if (r3 >= r5) goto L_0x006f;
    L_0x0025:
        r7 = 61;
        r7 = r9.indexOf(r7, r3);
        if (r7 == r6) goto L_0x0066;
    L_0x002d:
        if (r7 >= r5) goto L_0x0066;
    L_0x002f:
        r3 = r9.substring(r3, r7);
        r3 = r3.trim();
        r3 = r10.equals(r3);
        if (r3 == 0) goto L_0x0066;
    L_0x003d:
        r7 = r7 + 1;
        r3 = r9.substring(r7, r5);
        r3 = r3.trim();
        r7 = r3.length();
        if (r7 == 0) goto L_0x0066;
    L_0x004d:
        r9 = 2;
        if (r7 <= r9) goto L_0x0065;
    L_0x0050:
        r9 = 0;
        r9 = r3.charAt(r9);
        r10 = 34;
        if (r10 != r9) goto L_0x0065;
    L_0x0059:
        r7 = r7 - r4;
        r9 = r3.charAt(r7);
        if (r10 != r9) goto L_0x0065;
    L_0x0060:
        r9 = r3.substring(r4, r7);
        return r9;
    L_0x0065:
        return r3;
    L_0x0066:
        r3 = r5 + 1;
        r5 = r9.indexOf(r2, r3);
        if (r5 != r6) goto L_0x0023;
    L_0x006e:
        goto L_0x0022;
    L_0x006f:
        return r0;
    L_0x0070:
        return r0;
    L_0x0071:
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.e.d.c(java.lang.String, java.lang.String):java.lang.String");
    }

    public String m312h() {
        return m299b("Content-Type", "charset");
    }

    public C0422d m293a(boolean z) {
        m295a().setUseCaches(z);
        return this;
    }

    public String m313i() {
        return m298b("Content-Encoding");
    }

    public C0422d m303d(String str) {
        return m304d(str, null);
    }

    public C0422d m304d(String str, String str2) {
        if (str2 == null || str2.length() <= 0) {
            return m286a("Content-Type", str);
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append("; charset=");
        stringBuilder.append(str2);
        return m286a("Content-Type", stringBuilder.toString());
    }

    public int m314j() {
        return m300c("Content-Length");
    }

    protected C0422d m284a(InputStream inputStream, OutputStream outputStream) throws IOException {
        final InputStream inputStream2 = inputStream;
        final OutputStream outputStream2 = outputStream;
        return (C0422d) new C1817a<C0422d>(this, inputStream, this.f258h) {
            final /* synthetic */ C0422d f5829c;

            public /* synthetic */ Object mo2441b() throws C0419c, IOException {
                return m7001a();
            }

            public C0422d m7001a() throws IOException {
                byte[] bArr = new byte[this.f5829c.f260j];
                while (true) {
                    int read = inputStream2.read(bArr);
                    if (read == -1) {
                        return this.f5829c;
                    }
                    outputStream2.write(bArr, 0, read);
                }
            }
        }.call();
    }

    protected p008b.p009a.p010a.p011a.p012a.p018e.C0422d m315k() throws java.io.IOException {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r2 = this;
        r0 = r2.f256f;
        if (r0 != 0) goto L_0x0005;
    L_0x0004:
        return r2;
    L_0x0005:
        r0 = r2.f257g;
        if (r0 == 0) goto L_0x0010;
    L_0x0009:
        r0 = r2.f256f;
        r1 = "\r\n--00content0boundary00--\r\n";
        r0.m266a(r1);
    L_0x0010:
        r0 = r2.f258h;
        if (r0 == 0) goto L_0x001a;
    L_0x0014:
        r0 = r2.f256f;	 Catch:{ IOException -> 0x001f }
        r0.close();	 Catch:{ IOException -> 0x001f }
        goto L_0x001f;
    L_0x001a:
        r0 = r2.f256f;
        r0.close();
    L_0x001f:
        r0 = 0;
        r2.f256f = r0;
        return r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.e.d.k():b.a.a.a.a.e.d");
    }

    protected C0422d m316l() throws C0419c {
        try {
            return m315k();
        } catch (IOException e) {
            throw new C0419c(e);
        }
    }

    protected C0422d m317m() throws IOException {
        if (this.f256f != null) {
            return this;
        }
        m295a().setDoOutput(true);
        this.f256f = new C0421e(m295a().getOutputStream(), m301c(m295a().getRequestProperty("Content-Type"), "charset"), this.f260j);
        return this;
    }

    protected C0422d m318n() throws IOException {
        if (this.f257g) {
            this.f256f.m266a("\r\n--00content0boundary00\r\n");
        } else {
            this.f257g = true;
            m303d("multipart/form-data; boundary=00content0boundary00").m317m();
            this.f256f.m266a("--00content0boundary00\r\n");
        }
        return this;
    }

    protected C0422d m288a(String str, String str2, String str3) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("form-data; name=\"");
        stringBuilder.append(str);
        if (str2 != null) {
            stringBuilder.append("\"; filename=\"");
            stringBuilder.append(str2);
        }
        stringBuilder.append('\"');
        m309f("Content-Disposition", stringBuilder.toString());
        if (str3 != null) {
            m309f("Content-Type", str3);
        }
        return m308f((CharSequence) "\r\n");
    }

    public C0422d m306e(String str, String str2) {
        return m297b(str, null, str2);
    }

    public C0422d m297b(String str, String str2, String str3) throws C0419c {
        return m291a(str, str2, null, str3);
    }

    public C0422d m291a(String str, String str2, String str3, String str4) throws C0419c {
        try {
            m318n();
            m288a(str, str2, str3);
            this.f256f.m266a(str4);
            return this;
        } catch (String str5) {
            throw new C0419c(str5);
        }
    }

    public C0422d m285a(String str, Number number) throws C0419c {
        return m287a(str, null, number);
    }

    public C0422d m287a(String str, String str2, Number number) throws C0419c {
        return m297b(str, str2, number != null ? number.toString() : null);
    }

    public p008b.p009a.p010a.p011a.p012a.p018e.C0422d m289a(java.lang.String r4, java.lang.String r5, java.lang.String r6, java.io.File r7) throws p008b.p009a.p010a.p011a.p012a.p018e.C0422d.C0419c {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r3 = this;
        r0 = 0;
        r1 = new java.io.BufferedInputStream;	 Catch:{ IOException -> 0x001d }
        r2 = new java.io.FileInputStream;	 Catch:{ IOException -> 0x001d }
        r2.<init>(r7);	 Catch:{ IOException -> 0x001d }
        r1.<init>(r2);	 Catch:{ IOException -> 0x001d }
        r4 = r3.m290a(r4, r5, r6, r1);	 Catch:{ IOException -> 0x0018, all -> 0x0015 }
        if (r1 == 0) goto L_0x0014;
    L_0x0011:
        r1.close();	 Catch:{ IOException -> 0x0014 }
    L_0x0014:
        return r4;
    L_0x0015:
        r4 = move-exception;
        r0 = r1;
        goto L_0x0024;
    L_0x0018:
        r4 = move-exception;
        r0 = r1;
        goto L_0x001e;
    L_0x001b:
        r4 = move-exception;
        goto L_0x0024;
    L_0x001d:
        r4 = move-exception;
    L_0x001e:
        r5 = new b.a.a.a.a.e.d$c;	 Catch:{ all -> 0x001b }
        r5.<init>(r4);	 Catch:{ all -> 0x001b }
        throw r5;	 Catch:{ all -> 0x001b }
    L_0x0024:
        if (r0 == 0) goto L_0x0029;
    L_0x0026:
        r0.close();	 Catch:{ IOException -> 0x0029 }
    L_0x0029:
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.e.d.a(java.lang.String, java.lang.String, java.lang.String, java.io.File):b.a.a.a.a.e.d");
    }

    public C0422d m290a(String str, String str2, String str3, InputStream inputStream) throws C0419c {
        try {
            m318n();
            m288a(str, str2, str3);
            m284a(inputStream, this.f256f);
            return this;
        } catch (String str4) {
            throw new C0419c(str4);
        }
    }

    public C0422d m309f(String str, String str2) throws C0419c {
        return m308f((CharSequence) str).m308f((CharSequence) ": ").m308f((CharSequence) str2).m308f((CharSequence) "\r\n");
    }

    public C0422d m308f(CharSequence charSequence) throws C0419c {
        try {
            m317m();
            this.f256f.m266a(charSequence.toString());
            return this;
        } catch (CharSequence charSequence2) {
            throw new C0419c(charSequence2);
        }
    }

    public URL m319o() {
        return m295a().getURL();
    }

    public String m320p() {
        return m295a().getRequestMethod();
    }
}
