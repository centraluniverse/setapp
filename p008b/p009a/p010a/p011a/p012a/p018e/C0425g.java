package p008b.p009a.p010a.p011a.p012a.p018e;

import java.io.InputStream;

/* compiled from: PinningInfoProvider */
public interface C0425g {
    String getKeyStorePassword();

    InputStream getKeyStoreStream();

    long getPinCreationTimeInMillis();

    String[] getPins();
}
