package p008b.p009a.p010a.p011a.p012a.p015c;

/* compiled from: Priority */
public enum C0399e {
    LOW,
    NORMAL,
    HIGH,
    IMMEDIATE;

    static <Y> int m244a(C0401i c0401i, Y y) {
        if (y instanceof C0401i) {
            y = ((C0401i) y).getPriority();
        } else {
            y = NORMAL;
        }
        return y.ordinal() - c0401i.getPriority().ordinal();
    }
}
