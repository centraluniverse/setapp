package p008b.p009a.p010a.p011a.p012a.p015c;

import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import p008b.p009a.p010a.p011a.p012a.p015c.C0395a.C0390d;

/* compiled from: PriorityAsyncTask */
public abstract class C1812f<Params, Progress, Result> extends C0395a<Params, Progress, Result> implements C0396b<C0404l>, C0401i, C0404l {
    private final C1814j f4487a = new C1814j();

    /* compiled from: PriorityAsyncTask */
    private static class C0400a<Result> implements Executor {
        private final Executor f232a;
        private final C1812f f233b;

        public C0400a(Executor executor, C1812f c1812f) {
            this.f232a = executor;
            this.f233b = c1812f;
        }

        public void execute(Runnable runnable) {
            this.f232a.execute(new C1813h<Result>(this, runnable, null) {
                final /* synthetic */ C0400a f5826a;

                public <T extends C0396b<C0404l> & C0401i & C0404l> T mo2439a() {
                    return this.f5826a.f233b;
                }
            });
        }
    }

    public /* synthetic */ void addDependency(Object obj) {
        m4949a((C0404l) obj);
    }

    public final void m4950a(ExecutorService executorService, Params... paramsArr) {
        super.m223a(new C0400a(executorService, this), (Object[]) paramsArr);
    }

    public int compareTo(Object obj) {
        return C0399e.m244a(this, obj);
    }

    public void m4949a(C0404l c0404l) {
        if (m228b() != C0390d.PENDING) {
            throw new IllegalStateException("Must not add Dependency after task is running");
        }
        ((C0396b) ((C0401i) m4951e())).addDependency(c0404l);
    }

    public Collection<C0404l> getDependencies() {
        return ((C0396b) ((C0401i) m4951e())).getDependencies();
    }

    public boolean areDependenciesMet() {
        return ((C0396b) ((C0401i) m4951e())).areDependenciesMet();
    }

    public C0399e getPriority() {
        return ((C0401i) m4951e()).getPriority();
    }

    public void setFinished(boolean z) {
        ((C0404l) ((C0401i) m4951e())).setFinished(z);
    }

    public boolean isFinished() {
        return ((C0404l) ((C0401i) m4951e())).isFinished();
    }

    public void setError(Throwable th) {
        ((C0404l) ((C0401i) m4951e())).setError(th);
    }

    public <T extends C0396b<C0404l> & C0401i & C0404l> T m4951e() {
        return this.f4487a;
    }
}
