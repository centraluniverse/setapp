package p008b.p009a.p010a.p011a.p012a.p015c;

/* compiled from: Task */
public interface C0404l {
    boolean isFinished();

    void setError(Throwable th);

    void setFinished(boolean z);
}
