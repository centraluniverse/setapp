package p008b.p009a.p010a.p011a.p012a.p015c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: PriorityTask */
public class C1814j implements C0396b<C0404l>, C0401i, C0404l {
    private final List<C0404l> dependencies = new ArrayList();
    private final AtomicBoolean hasRun = new AtomicBoolean(false);
    private final AtomicReference<Throwable> throwable = new AtomicReference(null);

    public synchronized Collection<C0404l> getDependencies() {
        return Collections.unmodifiableCollection(this.dependencies);
    }

    public synchronized void addDependency(C0404l c0404l) {
        this.dependencies.add(c0404l);
    }

    public boolean areDependenciesMet() {
        for (C0404l isFinished : getDependencies()) {
            if (!isFinished.isFinished()) {
                return false;
            }
        }
        return true;
    }

    public synchronized void setFinished(boolean z) {
        this.hasRun.set(z);
    }

    public boolean isFinished() {
        return this.hasRun.get();
    }

    public C0399e getPriority() {
        return C0399e.NORMAL;
    }

    public void setError(Throwable th) {
        this.throwable.set(th);
    }

    public Throwable getError() {
        return (Throwable) this.throwable.get();
    }

    public int compareTo(Object obj) {
        return C0399e.m244a(this, obj);
    }

    public static boolean isProperDelegate(java.lang.Object r3) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r0 = 0;
        r1 = r3;	 Catch:{ ClassCastException -> 0x0011 }
        r1 = (p008b.p009a.p010a.p011a.p012a.p015c.C0396b) r1;	 Catch:{ ClassCastException -> 0x0011 }
        r2 = r3;	 Catch:{ ClassCastException -> 0x0011 }
        r2 = (p008b.p009a.p010a.p011a.p012a.p015c.C0404l) r2;	 Catch:{ ClassCastException -> 0x0011 }
        r3 = (p008b.p009a.p010a.p011a.p012a.p015c.C0401i) r3;	 Catch:{ ClassCastException -> 0x0011 }
        if (r1 == 0) goto L_0x0010;
    L_0x000b:
        if (r2 == 0) goto L_0x0010;
    L_0x000d:
        if (r3 == 0) goto L_0x0010;
    L_0x000f:
        r0 = 1;
    L_0x0010:
        return r0;
    L_0x0011:
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.c.j.isProperDelegate(java.lang.Object):boolean");
    }
}
