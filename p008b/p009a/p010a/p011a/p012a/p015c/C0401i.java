package p008b.p009a.p010a.p011a.p012a.p015c;

/* compiled from: PriorityProvider */
public interface C0401i<T> extends Comparable<T> {
    C0399e getPriority();
}
