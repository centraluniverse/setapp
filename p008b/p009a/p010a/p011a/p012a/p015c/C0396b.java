package p008b.p009a.p010a.p011a.p012a.p015c;

import java.util.Collection;

/* compiled from: Dependency */
public interface C0396b<T> {
    void addDependency(T t);

    boolean areDependenciesMet();

    Collection<T> getDependencies();
}
