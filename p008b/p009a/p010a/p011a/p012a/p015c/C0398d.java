package p008b.p009a.p010a.p011a.p012a.p015c;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* compiled from: DependsOn */
public @interface C0398d {
    Class<?>[] m243a();
}
