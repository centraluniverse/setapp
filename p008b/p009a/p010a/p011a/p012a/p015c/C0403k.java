package p008b.p009a.p010a.p011a.p012a.p015c;

import android.annotation.TargetApi;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: PriorityThreadPoolExecutor */
public class C0403k extends ThreadPoolExecutor {
    private static final int f235a = Runtime.getRuntime().availableProcessors();
    private static final int f236b = (f235a + 1);
    private static final int f237c = ((f235a * 2) + 1);

    /* compiled from: PriorityThreadPoolExecutor */
    protected static final class C0402a implements ThreadFactory {
        private final int f234a;

        public C0402a(int i) {
            this.f234a = i;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setPriority(this.f234a);
            thread.setName("Queue");
            return thread;
        }
    }

    public /* synthetic */ BlockingQueue getQueue() {
        return m248b();
    }

    <T extends Runnable & C0396b & C0404l & C0401i> C0403k(int i, int i2, long j, TimeUnit timeUnit, C0397c<T> c0397c, ThreadFactory threadFactory) {
        super(i, i2, j, timeUnit, c0397c, threadFactory);
        prestartAllCoreThreads();
    }

    public static <T extends Runnable & C0396b & C0404l & C0401i> C0403k m247a(int i, int i2) {
        return new C0403k(i, i2, 1, TimeUnit.SECONDS, new C0397c(), new C0402a(10));
    }

    public static C0403k m246a() {
        return C0403k.m247a(f236b, f237c);
    }

    protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new C1813h(runnable, t);
    }

    protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new C1813h(callable);
    }

    @TargetApi(9)
    public void execute(Runnable runnable) {
        if (C1814j.isProperDelegate(runnable)) {
            super.execute(runnable);
        } else {
            super.execute(newTaskFor(runnable, null));
        }
    }

    protected void afterExecute(Runnable runnable, Throwable th) {
        C0404l c0404l = (C0404l) runnable;
        c0404l.setFinished(true);
        c0404l.setError(th);
        m248b().m242d();
        super.afterExecute(runnable, th);
    }

    public C0397c m248b() {
        return (C0397c) super.getQueue();
    }
}
