package p008b.p009a.p010a.p011a.p012a.p015c;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* compiled from: PriorityFutureTask */
public class C1813h<V> extends FutureTask<V> implements C0396b<C0404l>, C0401i, C0404l {
    final Object f4488b;

    public /* synthetic */ void addDependency(Object obj) {
        m4954a((C0404l) obj);
    }

    public C1813h(Callable<V> callable) {
        super(callable);
        this.f4488b = m4953a((Object) callable);
    }

    public C1813h(Runnable runnable, V v) {
        super(runnable, v);
        this.f4488b = m4953a((Object) runnable);
    }

    public int compareTo(Object obj) {
        return ((C0401i) mo2439a()).compareTo(obj);
    }

    public void m4954a(C0404l c0404l) {
        ((C0396b) ((C0401i) mo2439a())).addDependency(c0404l);
    }

    public Collection<C0404l> getDependencies() {
        return ((C0396b) ((C0401i) mo2439a())).getDependencies();
    }

    public boolean areDependenciesMet() {
        return ((C0396b) ((C0401i) mo2439a())).areDependenciesMet();
    }

    public C0399e getPriority() {
        return ((C0401i) mo2439a()).getPriority();
    }

    public void setFinished(boolean z) {
        ((C0404l) ((C0401i) mo2439a())).setFinished(z);
    }

    public boolean isFinished() {
        return ((C0404l) ((C0401i) mo2439a())).isFinished();
    }

    public void setError(Throwable th) {
        ((C0404l) ((C0401i) mo2439a())).setError(th);
    }

    public <T extends C0396b<C0404l> & C0401i & C0404l> T mo2439a() {
        return (C0396b) this.f4488b;
    }

    protected <T extends C0396b<C0404l> & C0401i & C0404l> T m4953a(Object obj) {
        if (C1814j.isProperDelegate(obj)) {
            return (C0396b) obj;
        }
        return new C1814j();
    }
}
