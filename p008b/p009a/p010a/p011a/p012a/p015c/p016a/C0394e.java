package p008b.p009a.p010a.p011a.p012a.p015c.p016a;

/* compiled from: RetryState */
public class C0394e {
    private final int f208a;
    private final C0392a f209b;
    private final C0393d f210c;

    public C0394e(C0392a c0392a, C0393d c0393d) {
        this(0, c0392a, c0393d);
    }

    public C0394e(int i, C0392a c0392a, C0393d c0393d) {
        this.f208a = i;
        this.f209b = c0392a;
        this.f210c = c0393d;
    }

    public long m213a() {
        return this.f209b.getDelayMillis(this.f208a);
    }

    public C0394e m214b() {
        return new C0394e(this.f208a + 1, this.f209b, this.f210c);
    }

    public C0394e m215c() {
        return new C0394e(this.f209b, this.f210c);
    }
}
