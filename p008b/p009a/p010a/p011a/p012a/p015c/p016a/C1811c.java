package p008b.p009a.p010a.p011a.p012a.p015c.p016a;

/* compiled from: ExponentialBackoff */
public class C1811c implements C0392a {
    private final long f4485a;
    private final int f4486b;

    public C1811c(long j, int i) {
        this.f4485a = j;
        this.f4486b = i;
    }

    public long getDelayMillis(int i) {
        return (long) (((double) this.f4485a) * Math.pow((double) this.f4486b, (double) i));
    }
}
