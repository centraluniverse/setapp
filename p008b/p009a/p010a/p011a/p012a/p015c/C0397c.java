package p008b.p009a.p010a.p011a.p012a.p015c;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: DependencyPriorityBlockingQueue */
public class C0397c<E extends C0396b & C0404l & C0401i> extends PriorityBlockingQueue<E> {
    final Queue<E> f225a = new LinkedList();
    private final ReentrantLock f226b = new ReentrantLock();

    public /* synthetic */ Object peek() {
        return m239b();
    }

    public /* synthetic */ Object poll() {
        return m241c();
    }

    public /* synthetic */ Object poll(long j, TimeUnit timeUnit) throws InterruptedException {
        return m235a(j, timeUnit);
    }

    public /* synthetic */ Object take() throws InterruptedException {
        return m233a();
    }

    public E m233a() throws InterruptedException {
        return m240b(0, null, null);
    }

    public E m239b() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r2 = this;
        r0 = 1;
        r1 = 0;
        r0 = r2.m240b(r0, r1, r1);	 Catch:{ InterruptedException -> 0x0007 }
        return r0;
    L_0x0007:
        return r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.c.c.b():E");
    }

    public E m235a(long j, TimeUnit timeUnit) throws InterruptedException {
        return m240b(3, Long.valueOf(j), timeUnit);
    }

    public E m241c() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r2 = this;
        r0 = 2;
        r1 = 0;
        r0 = r2.m240b(r0, r1, r1);	 Catch:{ InterruptedException -> 0x0007 }
        return r0;
    L_0x0007:
        return r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.c.c.c():E");
    }

    public int size() {
        try {
            this.f226b.lock();
            int size = this.f225a.size() + super.size();
            return size;
        } finally {
            this.f226b.unlock();
        }
    }

    public <T> T[] toArray(T[] tArr) {
        try {
            this.f226b.lock();
            tArr = m238a(super.toArray(tArr), this.f225a.toArray(tArr));
            return tArr;
        } finally {
            this.f226b.unlock();
        }
    }

    public Object[] toArray() {
        try {
            this.f226b.lock();
            Object[] a = m238a(super.toArray(), this.f225a.toArray());
            return a;
        } finally {
            this.f226b.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection) {
        int drainTo;
        try {
            this.f226b.lock();
            drainTo = super.drainTo(collection) + this.f225a.size();
            while (!this.f225a.isEmpty()) {
                collection.add(this.f225a.poll());
            }
            return drainTo;
        } finally {
            drainTo = this.f226b;
            drainTo.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection, int i) {
        try {
            this.f226b.lock();
            int drainTo = super.drainTo(collection, i);
            while (!this.f225a.isEmpty() && drainTo <= i) {
                collection.add(this.f225a.poll());
                drainTo++;
            }
            this.f226b.unlock();
            return drainTo;
        } catch (Throwable th) {
            this.f226b.unlock();
        }
    }

    public boolean contains(Object obj) {
        try {
            this.f226b.lock();
            if (!super.contains(obj)) {
                if (this.f225a.contains(obj) == null) {
                    obj = null;
                    this.f226b.unlock();
                    return obj;
                }
            }
            obj = true;
            this.f226b.unlock();
            return obj;
        } catch (Throwable th) {
            this.f226b.unlock();
        }
    }

    public void clear() {
        try {
            this.f226b.lock();
            this.f225a.clear();
            super.clear();
        } finally {
            this.f226b.unlock();
        }
    }

    public boolean remove(Object obj) {
        try {
            this.f226b.lock();
            if (!super.remove(obj)) {
                if (this.f225a.remove(obj) == null) {
                    obj = null;
                    this.f226b.unlock();
                    return obj;
                }
            }
            obj = true;
            this.f226b.unlock();
            return obj;
        } catch (Throwable th) {
            this.f226b.unlock();
        }
    }

    public boolean removeAll(Collection<?> collection) {
        try {
            this.f226b.lock();
            collection = this.f225a.removeAll(collection) | super.removeAll(collection);
            return collection;
        } finally {
            this.f226b.unlock();
        }
    }

    E m234a(int i, Long l, TimeUnit timeUnit) throws InterruptedException {
        switch (i) {
            case 0:
                i = (C0396b) super.take();
                break;
            case 1:
                i = (C0396b) super.peek();
                break;
            case 2:
                i = (C0396b) super.poll();
                break;
            case 3:
                i = (C0396b) super.poll(l.longValue(), timeUnit);
                break;
            default:
                return null;
        }
        return i;
    }

    boolean m236a(int i, E e) {
        try {
            this.f226b.lock();
            if (i == 1) {
                super.remove(e);
            }
            i = this.f225a.offer(e);
            return i;
        } finally {
            this.f226b.unlock();
        }
    }

    E m240b(int i, Long l, TimeUnit timeUnit) throws InterruptedException {
        C0396b a;
        while (true) {
            a = m234a(i, l, timeUnit);
            if (a == null) {
                break;
            } else if (m237a(a)) {
                break;
            } else {
                m236a(i, a);
            }
        }
        return a;
    }

    boolean m237a(E e) {
        return e.areDependenciesMet();
    }

    public void m242d() {
        try {
            this.f226b.lock();
            Iterator it = this.f225a.iterator();
            while (it.hasNext()) {
                C0396b c0396b = (C0396b) it.next();
                if (m237a(c0396b)) {
                    super.offer(c0396b);
                    it.remove();
                }
            }
        } finally {
            this.f226b.unlock();
        }
    }

    <T> T[] m238a(T[] tArr, T[] tArr2) {
        int length = tArr.length;
        int length2 = tArr2.length;
        Object[] objArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), length + length2);
        System.arraycopy(tArr, 0, objArr, 0, length);
        System.arraycopy(tArr2, 0, objArr, length, length2);
        return objArr;
    }
}
