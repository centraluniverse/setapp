package p008b.p009a.p010a.p011a.p012a.p015c;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: AsyncTask */
public abstract class C0395a<Params, Progress, Result> {
    private static final int f211a = Runtime.getRuntime().availableProcessors();
    public static final Executor f212b = new ThreadPoolExecutor(f214d, f215e, 1, TimeUnit.SECONDS, f217g, f216f);
    public static final Executor f213c = new C0389c();
    private static final int f214d = (f211a + 1);
    private static final int f215e = ((f211a * 2) + 1);
    private static final ThreadFactory f216f = new C03831();
    private static final BlockingQueue<Runnable> f217g = new LinkedBlockingQueue(128);
    private static final C0387b f218h = new C0387b();
    private static volatile Executor f219i = f213c;
    private final C0391e<Params, Result> f220j = new C18092(this);
    private final FutureTask<Result> f221k = new FutureTask<Result>(this, this.f220j) {
        final /* synthetic */ C0395a f195a;

        protected void done() {
            /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
            /*
            r3 = this;
            r0 = r3.f195a;	 Catch:{ InterruptedException -> 0x001e, ExecutionException -> 0x0011, CancellationException -> 0x000a }
            r1 = r3.get();	 Catch:{ InterruptedException -> 0x001e, ExecutionException -> 0x0011, CancellationException -> 0x000a }
            r0.m220c(r1);	 Catch:{ InterruptedException -> 0x001e, ExecutionException -> 0x0011, CancellationException -> 0x000a }
            goto L_0x0024;
        L_0x000a:
            r0 = r3.f195a;
            r1 = 0;
            r0.m220c(r1);
            goto L_0x0024;
        L_0x0011:
            r0 = move-exception;
            r1 = new java.lang.RuntimeException;
            r2 = "An error occured while executing doInBackground()";
            r0 = r0.getCause();
            r1.<init>(r2, r0);
            throw r1;
        L_0x001e:
            r0 = move-exception;
            r1 = "AsyncTask";
            android.util.Log.w(r1, r0);
        L_0x0024:
            return;
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.c.a.3.done():void");
        }
    };
    private volatile C0390d f222l = C0390d.PENDING;
    private final AtomicBoolean f223m = new AtomicBoolean();
    private final AtomicBoolean f224n = new AtomicBoolean();

    /* compiled from: AsyncTask */
    static class C03831 implements ThreadFactory {
        private final AtomicInteger f194a = new AtomicInteger(1);

        C03831() {
        }

        public Thread newThread(Runnable runnable) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("AsyncTask #");
            stringBuilder.append(this.f194a.getAndIncrement());
            return new Thread(runnable, stringBuilder.toString());
        }
    }

    /* compiled from: AsyncTask */
    private static class C0386a<Data> {
        final C0395a f197a;
        final Data[] f198b;

        C0386a(C0395a c0395a, Data... dataArr) {
            this.f197a = c0395a;
            this.f198b = dataArr;
        }
    }

    /* compiled from: AsyncTask */
    private static class C0387b extends Handler {
        public C0387b() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            C0386a c0386a = (C0386a) message.obj;
            switch (message.what) {
                case 1:
                    c0386a.f197a.m222e(c0386a.f198b[0]);
                    return;
                case 2:
                    c0386a.f197a.m230b(c0386a.f198b);
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: AsyncTask */
    private static class C0389c implements Executor {
        final LinkedList<Runnable> f201a;
        Runnable f202b;

        private C0389c() {
            this.f201a = new LinkedList();
        }

        public synchronized void execute(final Runnable runnable) {
            this.f201a.offer(new Runnable(this) {
                final /* synthetic */ C0389c f200b;

                public void run() {
                    try {
                        runnable.run();
                    } finally {
                        this.f200b.m212a();
                    }
                }
            });
            if (this.f202b == null) {
                m212a();
            }
        }

        protected synchronized void m212a() {
            Runnable runnable = (Runnable) this.f201a.poll();
            this.f202b = runnable;
            if (runnable != null) {
                C0395a.f212b.execute(this.f202b);
            }
        }
    }

    /* compiled from: AsyncTask */
    public enum C0390d {
        PENDING,
        RUNNING,
        FINISHED
    }

    /* compiled from: AsyncTask */
    private static abstract class C0391e<Params, Result> implements Callable<Result> {
        Params[] f207b;

        private C0391e() {
        }
    }

    /* compiled from: AsyncTask */
    class C18092 extends C0391e<Params, Result> {
        final /* synthetic */ C0395a f4483a;

        C18092(C0395a c0395a) {
            this.f4483a = c0395a;
            super();
        }

        public Result call() throws Exception {
            this.f4483a.f224n.set(true);
            Process.setThreadPriority(10);
            return this.f4483a.m221d(this.f4483a.mo2443a(this.b));
        }
    }

    protected abstract Result mo2443a(Params... paramsArr);

    protected void mo2444a() {
    }

    protected void mo2445a(Result result) {
    }

    protected void m230b(Progress... progressArr) {
    }

    protected void m231c() {
    }

    private void m220c(Result result) {
        if (!this.f224n.get()) {
            m221d(result);
        }
    }

    private Result m221d(Result result) {
        f218h.obtainMessage(1, new C0386a(this, result)).sendToTarget();
        return result;
    }

    public final C0390d m228b() {
        return this.f222l;
    }

    protected void mo2446b(Result result) {
        m231c();
    }

    public final boolean m232d() {
        return this.f223m.get();
    }

    public final boolean m227a(boolean z) {
        this.f223m.set(true);
        return this.f221k.cancel(z);
    }

    public final C0395a<Params, Progress, Result> m223a(Executor executor, Params... paramsArr) {
        if (this.f222l != C0390d.PENDING) {
            switch (this.f222l) {
                case RUNNING:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case FINISHED:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
                default:
                    break;
            }
        }
        this.f222l = C0390d.RUNNING;
        mo2444a();
        this.f220j.f207b = paramsArr;
        executor.execute(this.f221k);
        return this;
    }

    private void m222e(Result result) {
        if (m232d()) {
            mo2446b((Object) result);
        } else {
            mo2445a((Object) result);
        }
        this.f222l = C0390d.FINISHED;
    }
}
