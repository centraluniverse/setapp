package p008b.p009a.p010a.p011a.p012a.p020g;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import org.json.JSONObject;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.C0458i;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;
import p008b.p009a.p010a.p011a.p012a.p019f.C1819b;

/* compiled from: DefaultCachedSettingsIo */
class C1822i implements C0435g {
    private final C0458i f4507a;

    public C1822i(C0458i c0458i) {
        this.f4507a = c0458i;
    }

    public JSONObject mo1241a() {
        Throwable e;
        C0452c.m367h().mo1249a("Fabric", "Reading cached settings...");
        Closeable closeable = null;
        Closeable fileInputStream;
        try {
            JSONObject jSONObject;
            File file = new File(new C1819b(this.f4507a).mo1237a(), "com.crashlytics.settings.json");
            if (file.exists()) {
                fileInputStream = new FileInputStream(file);
                try {
                    jSONObject = new JSONObject(C0367i.m116a((InputStream) fileInputStream));
                    closeable = fileInputStream;
                } catch (Exception e2) {
                    e = e2;
                    try {
                        C0452c.m367h().mo1257e("Fabric", "Failed to fetch cached settings", e);
                        C0367i.m127a(fileInputStream, "Error while closing settings cache file.");
                        return null;
                    } catch (Throwable th) {
                        e = th;
                        closeable = fileInputStream;
                        C0367i.m127a(closeable, "Error while closing settings cache file.");
                        throw e;
                    }
                }
            }
            C0452c.m367h().mo1249a("Fabric", "No cached settings found.");
            jSONObject = null;
            C0367i.m127a(closeable, "Error while closing settings cache file.");
            return jSONObject;
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            C0452c.m367h().mo1257e("Fabric", "Failed to fetch cached settings", e);
            C0367i.m127a(fileInputStream, "Error while closing settings cache file.");
            return null;
        } catch (Throwable th2) {
            e = th2;
            C0367i.m127a(closeable, "Error while closing settings cache file.");
            throw e;
        }
    }

    public void mo1242a(long j, JSONObject jSONObject) {
        Throwable e;
        C0452c.m367h().mo1249a("Fabric", "Writing settings to cache file...");
        if (jSONObject != null) {
            Closeable closeable = null;
            try {
                jSONObject.put("expires_at", j);
                Closeable fileWriter = new FileWriter(new File(new C1819b(this.f4507a).mo1237a(), "com.crashlytics.settings.json"));
                try {
                    fileWriter.write(jSONObject.toString());
                    fileWriter.flush();
                    C0367i.m127a(fileWriter, "Failed to close settings writer.");
                } catch (Exception e2) {
                    e = e2;
                    closeable = fileWriter;
                    try {
                        C0452c.m367h().mo1257e("Fabric", "Failed to cache settings", e);
                        C0367i.m127a(closeable, "Failed to close settings writer.");
                    } catch (Throwable th) {
                        e = th;
                        C0367i.m127a(closeable, "Failed to close settings writer.");
                        throw e;
                    }
                } catch (Throwable th2) {
                    e = th2;
                    closeable = fileWriter;
                    C0367i.m127a(closeable, "Failed to close settings writer.");
                    throw e;
                }
            } catch (Exception e3) {
                e = e3;
                C0452c.m367h().mo1257e("Fabric", "Failed to cache settings", e);
                C0367i.m127a(closeable, "Failed to close settings writer.");
            }
        }
    }
}
