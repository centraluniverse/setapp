package p008b.p009a.p010a.p011a.p012a.p020g;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.C0458i;
import p008b.p009a.p010a.p011a.p012a.p014b.C0356a;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;
import p008b.p009a.p010a.p011a.p012a.p018e.C0417c;
import p008b.p009a.p010a.p011a.p012a.p018e.C0422d;
import p008b.p009a.p010a.p011a.p012a.p018e.C0422d.C0419c;
import p008b.p009a.p010a.p011a.p012a.p018e.C0423e;

/* compiled from: DefaultSettingsSpiCall */
class C1825l extends C0356a implements C0449x {
    boolean m5012a(int i) {
        if (!(i == 200 || i == 201 || i == 202)) {
            if (i != 203) {
                return false;
            }
        }
        return true;
    }

    public C1825l(C0458i c0458i, String str, String str2, C0423e c0423e) {
        this(c0458i, str, str2, c0423e, C0417c.GET);
    }

    C1825l(C0458i c0458i, String str, String str2, C0423e c0423e, C0417c c0417c) {
        super(c0458i, str, str2, c0423e, c0417c);
    }

    public JSONObject mo1246a(C0448w c0448w) {
        StringBuilder stringBuilder;
        Throwable e;
        Throwable th;
        try {
            Map b = m5009b(c0448w);
            C0422d httpRequest = getHttpRequest(b);
            try {
                c0448w = m5006a(httpRequest, c0448w);
                try {
                    StringBuilder stringBuilder2 = new StringBuilder();
                    stringBuilder2.append("Requesting settings from ");
                    stringBuilder2.append(getUrl());
                    C0452c.m367h().mo1249a("Fabric", stringBuilder2.toString());
                    stringBuilder2 = new StringBuilder();
                    stringBuilder2.append("Settings query params were: ");
                    stringBuilder2.append(b);
                    C0452c.m367h().mo1249a("Fabric", stringBuilder2.toString());
                    JSONObject a = m5010a((C0422d) c0448w);
                    if (c0448w != null) {
                        stringBuilder = new StringBuilder();
                        stringBuilder.append("Settings request ID: ");
                        stringBuilder.append(c0448w.m298b(C0356a.HEADER_REQUEST_ID));
                        C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
                    }
                    return a;
                } catch (C0419c e2) {
                    e = e2;
                    try {
                        C0452c.m367h().mo1257e("Fabric", "Settings request failed.", e);
                        if (c0448w != null) {
                            return null;
                        }
                        stringBuilder = new StringBuilder();
                        stringBuilder.append("Settings request ID: ");
                        stringBuilder.append(c0448w.m298b(C0356a.HEADER_REQUEST_ID));
                        C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        if (c0448w != null) {
                            stringBuilder = new StringBuilder();
                            stringBuilder.append("Settings request ID: ");
                            stringBuilder.append(c0448w.m298b(C0356a.HEADER_REQUEST_ID));
                            C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
                        }
                        throw th;
                    }
                }
            } catch (C0419c e3) {
                e = e3;
                c0448w = httpRequest;
                C0452c.m367h().mo1257e("Fabric", "Settings request failed.", e);
                if (c0448w != null) {
                    return null;
                }
                stringBuilder = new StringBuilder();
                stringBuilder.append("Settings request ID: ");
                stringBuilder.append(c0448w.m298b(C0356a.HEADER_REQUEST_ID));
                C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
                return null;
            } catch (Throwable th3) {
                th = th3;
                c0448w = httpRequest;
                if (c0448w != null) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("Settings request ID: ");
                    stringBuilder.append(c0448w.m298b(C0356a.HEADER_REQUEST_ID));
                    C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
                }
                throw th;
            }
        } catch (C0419c e4) {
            e = e4;
            c0448w = null;
            C0452c.m367h().mo1257e("Fabric", "Settings request failed.", e);
            if (c0448w != null) {
                return null;
            }
            stringBuilder = new StringBuilder();
            stringBuilder.append("Settings request ID: ");
            stringBuilder.append(c0448w.m298b(C0356a.HEADER_REQUEST_ID));
            C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
            return null;
        } catch (C0448w c0448w2) {
            th = c0448w2;
            c0448w2 = null;
            if (c0448w2 != null) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("Settings request ID: ");
                stringBuilder.append(c0448w2.m298b(C0356a.HEADER_REQUEST_ID));
                C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
            }
            throw th;
        }
    }

    JSONObject m5010a(C0422d c0422d) {
        int b = c0422d.m296b();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Settings result was: ");
        stringBuilder.append(b);
        C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
        if (m5012a(b)) {
            return m5007a(c0422d.m307e());
        }
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Failed to retrieve settings from ");
        stringBuilder2.append(getUrl());
        C0452c.m367h().mo1256e("Fabric", stringBuilder2.toString());
        return null;
    }

    private JSONObject m5007a(String str) {
        try {
            return new JSONObject(str);
        } catch (Throwable e) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Failed to parse settings JSON from ");
            stringBuilder.append(getUrl());
            C0452c.m367h().mo1250a("Fabric", stringBuilder.toString(), e);
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Settings response ");
            stringBuilder2.append(str);
            C0452c.m367h().mo1249a("Fabric", stringBuilder2.toString());
            return null;
        }
    }

    private Map<String, String> m5009b(C0448w c0448w) {
        Map<String, String> hashMap = new HashMap();
        hashMap.put("build_version", c0448w.f352j);
        hashMap.put("display_version", c0448w.f351i);
        hashMap.put("source", Integer.toString(c0448w.f353k));
        if (c0448w.f354l != null) {
            hashMap.put("icon_hash", c0448w.f354l);
        }
        String str = c0448w.f350h;
        if (!C0367i.m140c(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    private C0422d m5006a(C0422d c0422d, C0448w c0448w) {
        m5008a(c0422d, C0356a.HEADER_API_KEY, c0448w.f343a);
        m5008a(c0422d, C0356a.HEADER_CLIENT_TYPE, C0356a.ANDROID_CLIENT_TYPE);
        m5008a(c0422d, C0356a.HEADER_CLIENT_VERSION, this.kit.getVersion());
        m5008a(c0422d, C0356a.HEADER_ACCEPT, C0356a.ACCEPT_JSON_VALUE);
        m5008a(c0422d, "X-CRASHLYTICS-DEVICE-MODEL", c0448w.f344b);
        m5008a(c0422d, "X-CRASHLYTICS-OS-BUILD-VERSION", c0448w.f345c);
        m5008a(c0422d, "X-CRASHLYTICS-OS-DISPLAY-VERSION", c0448w.f346d);
        m5008a(c0422d, "X-CRASHLYTICS-ADVERTISING-TOKEN", c0448w.f347e);
        m5008a(c0422d, "X-CRASHLYTICS-INSTALLATION-ID", c0448w.f348f);
        m5008a(c0422d, "X-CRASHLYTICS-ANDROID-ID", c0448w.f349g);
        return c0422d;
    }

    private void m5008a(C0422d c0422d, String str, String str2) {
        if (str2 != null) {
            c0422d.m286a(str, str2);
        }
    }
}
