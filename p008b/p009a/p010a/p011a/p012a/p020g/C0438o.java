package p008b.p009a.p010a.p011a.p012a.p020g;

/* compiled from: PromptSettingsData */
public class C0438o {
    public final String f310a;
    public final String f311b;
    public final String f312c;
    public final boolean f313d;
    public final String f314e;
    public final boolean f315f;
    public final String f316g;

    public C0438o(String str, String str2, String str3, boolean z, String str4, boolean z2, String str5) {
        this.f310a = str;
        this.f311b = str2;
        this.f312c = str3;
        this.f313d = z;
        this.f314e = str4;
        this.f315f = z2;
        this.f316g = str5;
    }
}
