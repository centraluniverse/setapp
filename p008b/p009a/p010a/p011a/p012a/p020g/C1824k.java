package p008b.p009a.p010a.p011a.p012a.p020g;

import com.crashlytics.android.beta.BuildConfig;
import org.json.JSONException;
import org.json.JSONObject;
import p008b.p009a.p010a.p011a.p012a.p014b.C0369k;
import p008b.p009a.p010a.p011a.p012a.p017d.C0409b;

/* compiled from: DefaultSettingsJsonTransform */
class C1824k implements C0447v {
    C1824k() {
    }

    public C0445t mo1245a(C0369k c0369k, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new C0445t(m4997a(c0369k, (long) optInt2, jSONObject), m4998a(jSONObject.getJSONObject("app")), m5002e(jSONObject.getJSONObject("session")), m5003f(jSONObject.getJSONObject("prompt")), m5000c(jSONObject.getJSONObject("features")), m5001d(jSONObject.getJSONObject("analytics")), m5004g(jSONObject.getJSONObject(BuildConfig.ARTIFACT_ID)), optInt, optInt2);
    }

    private C0433e m4998a(JSONObject jSONObject) throws JSONException {
        String string = jSONObject.getString("identifier");
        String string2 = jSONObject.getString("status");
        String string3 = jSONObject.getString("url");
        String string4 = jSONObject.getString("reports_url");
        boolean optBoolean = jSONObject.optBoolean("update_required", false);
        jSONObject = (jSONObject.has("icon") && jSONObject.getJSONObject("icon").has("hash")) ? m4999b(jSONObject.getJSONObject("icon")) : null;
        return new C0433e(string, string2, string3, string4, optBoolean, jSONObject);
    }

    private C0431c m4999b(JSONObject jSONObject) throws JSONException {
        return new C0431c(jSONObject.getString("hash"), jSONObject.getInt("width"), jSONObject.getInt("height"));
    }

    private C0436m m5000c(JSONObject jSONObject) {
        return new C0436m(jSONObject.optBoolean("prompt_enabled", false), jSONObject.optBoolean("collect_logged_exceptions", true), jSONObject.optBoolean("collect_reports", true), jSONObject.optBoolean("collect_analytics", false));
    }

    private C0430b m5001d(JSONObject jSONObject) {
        return new C0430b(jSONObject.optString("url", "https://e.crashlytics.com/spi/v2/events"), jSONObject.optInt("flush_interval_secs", 600), jSONObject.optInt("max_byte_size_per_file", C0409b.MAX_BYTE_SIZE_PER_FILE), jSONObject.optInt("max_file_count_per_send", 1), jSONObject.optInt("max_pending_send_file_count", 100), jSONObject.optBoolean("track_custom_events", true), jSONObject.optBoolean("track_predefined_events", true), jSONObject.optInt("sampling_rate", 1), jSONObject.optBoolean("flush_on_background", true));
    }

    private C0439p m5002e(JSONObject jSONObject) throws JSONException {
        return new C0439p(jSONObject.optInt("log_buffer_size", 64000), jSONObject.optInt("max_chained_exception_depth", 8), jSONObject.optInt("max_custom_exception_events", 64), jSONObject.optInt("max_custom_key_value_pairs", 64), jSONObject.optInt("identifier_mask", 255), jSONObject.optBoolean("send_session_without_crash", false), jSONObject.optInt("max_complete_sessions_count", 4));
    }

    private C0438o m5003f(JSONObject jSONObject) throws JSONException {
        return new C0438o(jSONObject.optString("title", "Send Crash Report?"), jSONObject.optString("message", "Looks like we crashed! Please help us fix the problem by sending a crash report."), jSONObject.optString("send_button_title", "Send"), jSONObject.optBoolean("show_cancel_button", true), jSONObject.optString("cancel_button_title", "Don't Send"), jSONObject.optBoolean("show_always_send_button", true), jSONObject.optString("always_send_button_title", "Always Send"));
    }

    private C0434f m5004g(JSONObject jSONObject) throws JSONException {
        return new C0434f(jSONObject.optString("update_endpoint", C0446u.f342a), jSONObject.optInt("update_suspend_duration", 3600));
    }

    private long m4997a(C0369k c0369k, long j, JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("expires_at")) {
            return jSONObject.getLong("expires_at");
        }
        return c0369k.mo1213a() + (j * 1000);
    }
}
