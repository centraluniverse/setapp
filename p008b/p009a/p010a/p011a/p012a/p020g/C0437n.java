package p008b.p009a.p010a.p011a.p012a.p020g;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;

/* compiled from: IconRequest */
public class C0437n {
    public final String f306a;
    public final int f307b;
    public final int f308c;
    public final int f309d;

    public C0437n(String str, int i, int i2, int i3) {
        this.f306a = str;
        this.f307b = i;
        this.f308c = i2;
        this.f309d = i3;
    }

    public static C0437n m340a(Context context, String str) {
        if (str != null) {
            try {
                int l = C0367i.m149l(context);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("App icon resource ID is ");
                stringBuilder.append(l);
                C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), l, options);
                return new C0437n(str, l, options.outWidth, options.outHeight);
            } catch (Context context2) {
                C0452c.m367h().mo1257e("Fabric", "Failed to load icon", context2);
            }
        }
        return null;
    }
}
