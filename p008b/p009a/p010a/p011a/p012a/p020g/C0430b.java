package p008b.p009a.p010a.p011a.p012a.p020g;

/* compiled from: AnalyticsSettingsData */
public class C0430b {
    public final String f271a;
    public final int f272b;
    public final int f273c;
    public final int f274d;
    public final int f275e;
    public final boolean f276f;
    public final boolean f277g;
    public final boolean f278h;
    public final int f279i;

    public C0430b(String str, int i, int i2, int i3, int i4, boolean z, boolean z2, int i5, boolean z3) {
        this.f271a = str;
        this.f272b = i;
        this.f273c = i2;
        this.f274d = i3;
        this.f275e = i4;
        this.f276f = z;
        this.f277g = z2;
        this.f279i = i5;
        this.f278h = z3;
    }
}
