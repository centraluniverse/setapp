package p008b.p009a.p010a.p011a.p012a.p020g;

import android.content.res.Resources.NotFoundException;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Locale;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.C0458i;
import p008b.p009a.p010a.p011a.C0460k;
import p008b.p009a.p010a.p011a.p012a.p014b.C0356a;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;
import p008b.p009a.p010a.p011a.p012a.p014b.C0381r;
import p008b.p009a.p010a.p011a.p012a.p018e.C0417c;
import p008b.p009a.p010a.p011a.p012a.p018e.C0422d;
import p008b.p009a.p010a.p011a.p012a.p018e.C0423e;

/* compiled from: AbstractAppSpiCall */
abstract class C1821a extends C0356a {
    public C1821a(C0458i c0458i, String str, String str2, C0423e c0423e, C0417c c0417c) {
        super(c0458i, str, str2, c0423e, c0417c);
    }

    public boolean mo2442a(C0432d c0432d) {
        C0422d b = m4983b(m4982a(getHttpRequest(), c0432d), c0432d);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sending app info to ");
        stringBuilder.append(getUrl());
        C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
        if (c0432d.f292j != null) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("App icon hash is ");
            stringBuilder.append(c0432d.f292j.f306a);
            C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
            stringBuilder = new StringBuilder();
            stringBuilder.append("App icon size is ");
            stringBuilder.append(c0432d.f292j.f308c);
            stringBuilder.append("x");
            stringBuilder.append(c0432d.f292j.f309d);
            C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
        }
        c0432d = b.m296b();
        String str = "POST".equals(b.m320p()) ? "Create" : "Update";
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(str);
        stringBuilder2.append(" app request ID: ");
        stringBuilder2.append(b.m298b(C0356a.HEADER_REQUEST_ID));
        C0452c.m367h().mo1249a("Fabric", stringBuilder2.toString());
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append("Result was ");
        stringBuilder3.append(c0432d);
        C0452c.m367h().mo1249a("Fabric", stringBuilder3.toString());
        return C0381r.m208a(c0432d) == null ? true : null;
    }

    private C0422d m4982a(C0422d c0422d, C0432d c0432d) {
        return c0422d.m286a(C0356a.HEADER_API_KEY, c0432d.f283a).m286a(C0356a.HEADER_CLIENT_TYPE, C0356a.ANDROID_CLIENT_TYPE).m286a(C0356a.HEADER_CLIENT_VERSION, this.kit.getVersion());
    }

    private C0422d m4983b(C0422d c0422d, C0432d c0432d) {
        Throwable e;
        StringBuilder stringBuilder;
        c0422d = c0422d.m306e("app[identifier]", c0432d.f284b).m306e("app[name]", c0432d.f288f).m306e("app[display_version]", c0432d.f285c).m306e("app[build_version]", c0432d.f286d).m285a("app[source]", Integer.valueOf(c0432d.f289g)).m306e("app[minimum_sdk_version]", c0432d.f290h).m306e("app[built_sdk_version]", c0432d.f291i);
        if (!C0367i.m140c(c0432d.f287e)) {
            c0422d.m306e("app[instance_identifier]", c0432d.f287e);
        }
        if (c0432d.f292j != null) {
            Closeable openRawResource;
            try {
                openRawResource = this.kit.getContext().getResources().openRawResource(c0432d.f292j.f307b);
                try {
                    c0422d.m306e("app[icon][hash]", c0432d.f292j.f306a).m290a("app[icon][data]", "icon.png", "application/octet-stream", (InputStream) openRawResource).m285a("app[icon][width]", Integer.valueOf(c0432d.f292j.f308c)).m285a("app[icon][height]", Integer.valueOf(c0432d.f292j.f309d));
                } catch (NotFoundException e2) {
                    e = e2;
                    try {
                        stringBuilder = new StringBuilder();
                        stringBuilder.append("Failed to find app icon with resource ID: ");
                        stringBuilder.append(c0432d.f292j.f307b);
                        C0452c.m367h().mo1257e("Fabric", stringBuilder.toString(), e);
                        C0367i.m127a(openRawResource, "Failed to close app icon InputStream.");
                        if (c0432d.f293k != null) {
                            for (C0460k c0460k : c0432d.f293k) {
                                c0422d.m306e(m4984a(c0460k), c0460k.m388b());
                                c0422d.m306e(m4986b(c0460k), c0460k.m389c());
                            }
                        }
                        return c0422d;
                    } catch (Throwable th) {
                        c0422d = th;
                        C0367i.m127a(openRawResource, "Failed to close app icon InputStream.");
                        throw c0422d;
                    }
                }
            } catch (Throwable e3) {
                Throwable th2 = e3;
                openRawResource = null;
                e = th2;
                stringBuilder = new StringBuilder();
                stringBuilder.append("Failed to find app icon with resource ID: ");
                stringBuilder.append(c0432d.f292j.f307b);
                C0452c.m367h().mo1257e("Fabric", stringBuilder.toString(), e);
                C0367i.m127a(openRawResource, "Failed to close app icon InputStream.");
                if (c0432d.f293k != null) {
                    for (C0460k c0460k2 : c0432d.f293k) {
                        c0422d.m306e(m4984a(c0460k2), c0460k2.m388b());
                        c0422d.m306e(m4986b(c0460k2), c0460k2.m389c());
                    }
                }
                return c0422d;
            } catch (Throwable th3) {
                c0422d = th3;
                openRawResource = null;
                C0367i.m127a(openRawResource, "Failed to close app icon InputStream.");
                throw c0422d;
            }
            C0367i.m127a(openRawResource, "Failed to close app icon InputStream.");
        }
        if (c0432d.f293k != null) {
            for (C0460k c0460k22 : c0432d.f293k) {
                c0422d.m306e(m4984a(c0460k22), c0460k22.m388b());
                c0422d.m306e(m4986b(c0460k22), c0460k22.m389c());
            }
        }
        return c0422d;
    }

    String m4984a(C0460k c0460k) {
        return String.format(Locale.US, "app[build][libraries][%s][version]", new Object[]{c0460k.m387a()});
    }

    String m4986b(C0460k c0460k) {
        return String.format(Locale.US, "app[build][libraries][%s][type]", new Object[]{c0460k.m387a()});
    }
}
