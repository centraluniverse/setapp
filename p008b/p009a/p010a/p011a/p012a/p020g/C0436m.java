package p008b.p009a.p010a.p011a.p012a.p020g;

/* compiled from: FeaturesSettingsData */
public class C0436m {
    public final boolean f302a;
    public final boolean f303b;
    public final boolean f304c;
    public final boolean f305d;

    public C0436m(boolean z, boolean z2, boolean z3, boolean z4) {
        this.f302a = z;
        this.f303b = z2;
        this.f304c = z3;
        this.f305d = z4;
    }
}
