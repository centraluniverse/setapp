package p008b.p009a.p010a.p011a.p012a.p020g;

/* compiled from: SettingsCacheBehavior */
public enum C0443r {
    USE_CACHE,
    SKIP_CACHE_LOOKUP,
    IGNORE_CACHE_EXPIRATION
}
