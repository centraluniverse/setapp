package p008b.p009a.p010a.p011a.p012a.p020g;

/* compiled from: AppSettingsData */
public class C0433e {
    public final String f294a;
    public final String f295b;
    public final String f296c;
    public final String f297d;
    public final boolean f298e;
    public final C0431c f299f;

    public C0433e(String str, String str2, String str3, String str4, boolean z, C0431c c0431c) {
        this.f294a = str;
        this.f295b = str2;
        this.f296c = str3;
        this.f297d = str4;
        this.f298e = z;
        this.f299f = c0431c;
    }
}
