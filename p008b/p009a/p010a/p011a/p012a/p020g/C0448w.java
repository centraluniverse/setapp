package p008b.p009a.p010a.p011a.p012a.p020g;

/* compiled from: SettingsRequest */
public class C0448w {
    public final String f343a;
    public final String f344b;
    public final String f345c;
    public final String f346d;
    public final String f347e;
    public final String f348f;
    public final String f349g;
    public final String f350h;
    public final String f351i;
    public final String f352j;
    public final int f353k;
    public final String f354l;

    public C0448w(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, int i, String str11) {
        this.f343a = str;
        this.f344b = str2;
        this.f345c = str3;
        this.f346d = str4;
        this.f347e = str5;
        this.f348f = str6;
        this.f349g = str7;
        this.f350h = str8;
        this.f351i = str9;
        this.f352j = str10;
        this.f353k = i;
        this.f354l = str11;
    }
}
