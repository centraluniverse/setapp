package p008b.p009a.p010a.p011a.p012a.p020g;

/* compiled from: SettingsData */
public class C0445t {
    public final C0433e f333a;
    public final C0439p f334b;
    public final C0438o f335c;
    public final C0436m f336d;
    public final C0430b f337e;
    public final C0434f f338f;
    public final long f339g;
    public final int f340h;
    public final int f341i;

    public C0445t(long j, C0433e c0433e, C0439p c0439p, C0438o c0438o, C0436m c0436m, C0430b c0430b, C0434f c0434f, int i, int i2) {
        this.f339g = j;
        this.f333a = c0433e;
        this.f334b = c0439p;
        this.f335c = c0438o;
        this.f336d = c0436m;
        this.f340h = i;
        this.f341i = i2;
        this.f337e = c0430b;
        this.f338f = c0434f;
    }

    public boolean m350a(long j) {
        return this.f339g < j ? 1 : 0;
    }
}
