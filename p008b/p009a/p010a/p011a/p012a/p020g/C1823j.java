package p008b.p009a.p010a.p011a.p012a.p020g;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import org.json.JSONException;
import org.json.JSONObject;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.C0458i;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;
import p008b.p009a.p010a.p011a.p012a.p014b.C0369k;
import p008b.p009a.p010a.p011a.p012a.p019f.C0429c;
import p008b.p009a.p010a.p011a.p012a.p019f.C1820d;

/* compiled from: DefaultSettingsController */
class C1823j implements C0444s {
    private final C0448w f4508a;
    private final C0447v f4509b;
    private final C0369k f4510c;
    private final C0435g f4511d;
    private final C0449x f4512e;
    private final C0458i f4513f;
    private final C0429c f4514g = new C1820d(this.f4513f);

    public C1823j(C0458i c0458i, C0448w c0448w, C0369k c0369k, C0447v c0447v, C0435g c0435g, C0449x c0449x) {
        this.f4513f = c0458i;
        this.f4508a = c0448w;
        this.f4510c = c0369k;
        this.f4509b = c0447v;
        this.f4511d = c0435g;
        this.f4512e = c0449x;
    }

    public C0445t mo1243a() {
        return mo1244a(C0443r.USE_CACHE);
    }

    public C0445t mo1244a(C0443r c0443r) {
        C0443r c0443r2 = null;
        try {
            if (!(C0452c.m368i() || m4996d())) {
                c0443r2 = m4990b(c0443r);
            }
            if (c0443r2 == null) {
                c0443r = this.f4512e.mo1246a(this.f4508a);
                if (c0443r != null) {
                    C0445t a = this.f4509b.mo1245a(this.f4510c, c0443r);
                    try {
                        this.f4511d.mo1242a(a.f339g, c0443r);
                        m4989a(c0443r, "Loaded settings: ");
                        m4993a(m4994b());
                        c0443r2 = a;
                    } catch (Exception e) {
                        c0443r = e;
                        c0443r2 = a;
                        C0452c.m367h().mo1257e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", c0443r);
                        return c0443r2;
                    }
                }
            }
            if (c0443r2 == null) {
                return m4990b(C0443r.IGNORE_CACHE_EXPIRATION);
            }
        } catch (Exception e2) {
            c0443r = e2;
            C0452c.m367h().mo1257e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", c0443r);
            return c0443r2;
        }
        return c0443r2;
    }

    private C0445t m4990b(C0443r c0443r) {
        C0445t c0445t = null;
        try {
            if (C0443r.SKIP_CACHE_LOOKUP.equals(c0443r)) {
                return null;
            }
            JSONObject a = this.f4511d.mo1241a();
            if (a != null) {
                C0445t a2 = this.f4509b.mo1245a(this.f4510c, a);
                if (a2 != null) {
                    m4989a(a, "Loaded cached settings: ");
                    long a3 = this.f4510c.mo1213a();
                    if (C0443r.IGNORE_CACHE_EXPIRATION.equals(c0443r) == null) {
                        if (a2.m350a(a3) != null) {
                            C0452c.m367h().mo1249a("Fabric", "Cached settings have expired.");
                            return null;
                        }
                    }
                    try {
                        C0452c.m367h().mo1249a("Fabric", "Returning cached settings.");
                        return a2;
                    } catch (Exception e) {
                        c0443r = e;
                        c0445t = a2;
                        C0452c.m367h().mo1257e("Fabric", "Failed to get cached settings", c0443r);
                        return c0445t;
                    }
                }
                C0452c.m367h().mo1257e("Fabric", "Failed to transform cached settings data.", null);
                return null;
            }
            C0452c.m367h().mo1249a("Fabric", "No cached settings data found.");
            return null;
        } catch (Exception e2) {
            c0443r = e2;
            C0452c.m367h().mo1257e("Fabric", "Failed to get cached settings", c0443r);
            return c0445t;
        }
    }

    private void m4989a(JSONObject jSONObject, String str) throws JSONException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(jSONObject.toString());
        C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
    }

    String m4994b() {
        return C0367i.m122a(C0367i.m150m(this.f4513f.getContext()));
    }

    String m4995c() {
        return this.f4514g.mo1238a().getString("existing_instance_identifier", "");
    }

    @SuppressLint({"CommitPrefEdits"})
    boolean m4993a(String str) {
        Editor b = this.f4514g.mo1240b();
        b.putString("existing_instance_identifier", str);
        return this.f4514g.mo1239a(b);
    }

    boolean m4996d() {
        return m4995c().equals(m4994b()) ^ 1;
    }
}
