package p008b.p009a.p010a.p011a.p012a.p020g;

import java.util.Collection;
import p008b.p009a.p010a.p011a.C0460k;

/* compiled from: AppRequestData */
public class C0432d {
    public final String f283a;
    public final String f284b;
    public final String f285c;
    public final String f286d;
    public final String f287e;
    public final String f288f;
    public final int f289g;
    public final String f290h;
    public final String f291i;
    public final C0437n f292j;
    public final Collection<C0460k> f293k;

    public C0432d(String str, String str2, String str3, String str4, String str5, String str6, int i, String str7, String str8, C0437n c0437n, Collection<C0460k> collection) {
        this.f283a = str;
        this.f284b = str2;
        this.f285c = str3;
        this.f286d = str4;
        this.f287e = str5;
        this.f288f = str6;
        this.f289g = i;
        this.f290h = str7;
        this.f291i = str8;
        this.f292j = c0437n;
        this.f293k = collection;
    }
}
