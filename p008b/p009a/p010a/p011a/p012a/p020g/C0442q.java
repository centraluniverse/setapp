package p008b.p009a.p010a.p011a.p012a.p020g;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import p008b.p009a.p010a.p011a.C0452c;

/* compiled from: Settings */
public class C0442q {
    private final AtomicReference<C0445t> f325a;
    private final CountDownLatch f326b;
    private C0444s f327c;
    private boolean f328d;

    /* compiled from: Settings */
    static class C0441a {
        private static final C0442q f324a = new C0442q();
    }

    public static C0442q m342a() {
        return C0441a.f324a;
    }

    private C0442q() {
        this.f325a = new AtomicReference();
        this.f326b = new CountDownLatch(1);
        this.f328d = false;
    }

    public synchronized p008b.p009a.p010a.p011a.p012a.p020g.C0442q m344a(p008b.p009a.p010a.p011a.C0458i r25, p008b.p009a.p010a.p011a.p012a.p014b.C0375o r26, p008b.p009a.p010a.p011a.p012a.p018e.C0423e r27, java.lang.String r28, java.lang.String r29, java.lang.String r30) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.utils.BlockUtils.getBlockByInsn(BlockUtils.java:172)
	at jadx.core.dex.visitors.ssa.EliminatePhiNodes.replaceMerge(EliminatePhiNodes.java:90)
	at jadx.core.dex.visitors.ssa.EliminatePhiNodes.replaceMergeInstructions(EliminatePhiNodes.java:68)
	at jadx.core.dex.visitors.ssa.EliminatePhiNodes.visit(EliminatePhiNodes.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r24 = this;
        r1 = r24;
        r3 = r25;
        monitor-enter(r24);
        r2 = r1.f328d;	 Catch:{ all -> 0x00a1 }
        if (r2 == 0) goto L_0x000b;
    L_0x0009:
        monitor-exit(r24);
        return r1;
    L_0x000b:
        r2 = r1.f327c;	 Catch:{ all -> 0x00a1 }
        r9 = 1;	 Catch:{ all -> 0x00a1 }
        if (r2 != 0) goto L_0x009d;	 Catch:{ all -> 0x00a1 }
    L_0x0010:
        r2 = r25.getContext();	 Catch:{ all -> 0x00a1 }
        r4 = r26.m168c();	 Catch:{ all -> 0x00a1 }
        r5 = new b.a.a.a.a.b.g;	 Catch:{ all -> 0x00a1 }
        r5.<init>();	 Catch:{ all -> 0x00a1 }
        r11 = r5.m102a(r2);	 Catch:{ all -> 0x00a1 }
        r5 = r26.m175j();	 Catch:{ all -> 0x00a1 }
        r6 = new b.a.a.a.a.b.s;	 Catch:{ all -> 0x00a1 }
        r6.<init>();	 Catch:{ all -> 0x00a1 }
        r7 = new b.a.a.a.a.g.k;	 Catch:{ all -> 0x00a1 }
        r7.<init>();	 Catch:{ all -> 0x00a1 }
        r8 = new b.a.a.a.a.g.i;	 Catch:{ all -> 0x00a1 }
        r8.<init>(r3);	 Catch:{ all -> 0x00a1 }
        r22 = p008b.p009a.p010a.p011a.p012a.p014b.C0367i.m148k(r2);	 Catch:{ all -> 0x00a1 }
        r10 = java.util.Locale.US;	 Catch:{ all -> 0x00a1 }
        r12 = "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings";	 Catch:{ all -> 0x00a1 }
        r13 = new java.lang.Object[r9];	 Catch:{ all -> 0x00a1 }
        r14 = 0;	 Catch:{ all -> 0x00a1 }
        r13[r14] = r4;	 Catch:{ all -> 0x00a1 }
        r4 = java.lang.String.format(r10, r12, r13);	 Catch:{ all -> 0x00a1 }
        r15 = new b.a.a.a.a.g.l;	 Catch:{ all -> 0x00a1 }
        r10 = r27;	 Catch:{ all -> 0x00a1 }
        r12 = r30;	 Catch:{ all -> 0x00a1 }
        r15.<init>(r3, r12, r4, r10);	 Catch:{ all -> 0x00a1 }
        r12 = r26.m172g();	 Catch:{ all -> 0x00a1 }
        r13 = r26.m171f();	 Catch:{ all -> 0x00a1 }
        r4 = r26.m170e();	 Catch:{ all -> 0x00a1 }
        r16 = r26.m178m();	 Catch:{ all -> 0x00a1 }
        r17 = r26.m167b();	 Catch:{ all -> 0x00a1 }
        r18 = r26.m179n();	 Catch:{ all -> 0x00a1 }
        r10 = new java.lang.String[r9];	 Catch:{ all -> 0x00a1 }
        r2 = p008b.p009a.p010a.p011a.p012a.p014b.C0367i.m150m(r2);	 Catch:{ all -> 0x00a1 }
        r10[r14] = r2;	 Catch:{ all -> 0x00a1 }
        r2 = p008b.p009a.p010a.p011a.p012a.p014b.C0367i.m122a(r10);	 Catch:{ all -> 0x00a1 }
        r5 = p008b.p009a.p010a.p011a.p012a.p014b.C0370l.m155a(r5);	 Catch:{ all -> 0x00a1 }
        r21 = r5.m156a();	 Catch:{ all -> 0x00a1 }
        r5 = new b.a.a.a.a.g.w;	 Catch:{ all -> 0x00a1 }
        r10 = r5;	 Catch:{ all -> 0x00a1 }
        r14 = r4;	 Catch:{ all -> 0x00a1 }
        r23 = r15;	 Catch:{ all -> 0x00a1 }
        r15 = r16;	 Catch:{ all -> 0x00a1 }
        r16 = r17;	 Catch:{ all -> 0x00a1 }
        r17 = r18;	 Catch:{ all -> 0x00a1 }
        r18 = r2;	 Catch:{ all -> 0x00a1 }
        r19 = r29;	 Catch:{ all -> 0x00a1 }
        r20 = r28;	 Catch:{ all -> 0x00a1 }
        r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22);	 Catch:{ all -> 0x00a1 }
        r10 = new b.a.a.a.a.g.j;	 Catch:{ all -> 0x00a1 }
        r2 = r10;	 Catch:{ all -> 0x00a1 }
        r4 = r5;	 Catch:{ all -> 0x00a1 }
        r5 = r6;	 Catch:{ all -> 0x00a1 }
        r6 = r7;	 Catch:{ all -> 0x00a1 }
        r7 = r8;	 Catch:{ all -> 0x00a1 }
        r8 = r23;	 Catch:{ all -> 0x00a1 }
        r2.<init>(r3, r4, r5, r6, r7, r8);	 Catch:{ all -> 0x00a1 }
        r1.f327c = r10;	 Catch:{ all -> 0x00a1 }
    L_0x009d:
        r1.f328d = r9;	 Catch:{ all -> 0x00a1 }
        monitor-exit(r24);
        return r1;
    L_0x00a1:
        r0 = move-exception;
        r2 = r0;
        monitor-exit(r24);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.g.q.a(b.a.a.a.i, b.a.a.a.a.b.o, b.a.a.a.a.e.e, java.lang.String, java.lang.String, java.lang.String):b.a.a.a.a.g.q");
    }

    public p008b.p009a.p010a.p011a.p012a.p020g.C0445t m345b() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r3 = this;
        r0 = r3.f326b;	 Catch:{ InterruptedException -> 0x000e }
        r0.await();	 Catch:{ InterruptedException -> 0x000e }
        r0 = r3.f325a;	 Catch:{ InterruptedException -> 0x000e }
        r0 = r0.get();	 Catch:{ InterruptedException -> 0x000e }
        r0 = (p008b.p009a.p010a.p011a.p012a.p020g.C0445t) r0;	 Catch:{ InterruptedException -> 0x000e }
        return r0;
    L_0x000e:
        r0 = p008b.p009a.p010a.p011a.C0452c.m367h();
        r1 = "Fabric";
        r2 = "Interrupted while waiting for settings data.";
        r0.mo1256e(r1, r2);
        r0 = 0;
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.g.q.b():b.a.a.a.a.g.t");
    }

    public synchronized boolean m346c() {
        C0445t a;
        a = this.f327c.mo1243a();
        m343a(a);
        return a != null;
    }

    public synchronized boolean m347d() {
        C0445t a;
        a = this.f327c.mo1244a(C0443r.SKIP_CACHE_LOOKUP);
        m343a(a);
        if (a == null) {
            C0452c.m367h().mo1257e("Fabric", "Failed to force reload of settings from Crashlytics.", null);
        }
        return a != null;
    }

    private void m343a(C0445t c0445t) {
        this.f325a.set(c0445t);
        this.f326b.countDown();
    }
}
