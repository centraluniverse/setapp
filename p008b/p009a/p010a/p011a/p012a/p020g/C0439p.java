package p008b.p009a.p010a.p011a.p012a.p020g;

/* compiled from: SessionSettingsData */
public class C0439p {
    public final int f317a;
    public final int f318b;
    public final int f319c;
    public final int f320d;
    public final int f321e;
    public final boolean f322f;
    public final int f323g;

    public C0439p(int i, int i2, int i3, int i4, int i5, boolean z, int i6) {
        this.f317a = i;
        this.f318b = i2;
        this.f319c = i3;
        this.f320d = i4;
        this.f321e = i5;
        this.f322f = z;
        this.f323g = i6;
    }
}
