package p008b.p009a.p010a.p011a.p012a.p014b;

/* compiled from: DeliveryMechanism */
public enum C0370l {
    DEVELOPER(1),
    USER_SIDELOAD(2),
    TEST_DISTRIBUTION(3),
    APP_STORE(4);
    
    private final int f149e;

    private C0370l(int i) {
        this.f149e = i;
    }

    public int m156a() {
        return this.f149e;
    }

    public String toString() {
        return Integer.toString(this.f149e);
    }

    public static C0370l m155a(String str) {
        if ("io.crash.air".equals(str)) {
            return TEST_DISTRIBUTION;
        }
        if (str != null) {
            return APP_STORE;
        }
        return DEVELOPER;
    }
}
