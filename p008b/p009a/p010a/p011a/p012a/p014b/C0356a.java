package p008b.p009a.p010a.p011a.p012a.p014b;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;
import p008b.p009a.p010a.p011a.C0458i;
import p008b.p009a.p010a.p011a.p012a.p018e.C0417c;
import p008b.p009a.p010a.p011a.p012a.p018e.C0422d;
import p008b.p009a.p010a.p011a.p012a.p018e.C0423e;

/* compiled from: AbstractSpiCall */
public abstract class C0356a {
    public static final String ACCEPT_JSON_VALUE = "application/json";
    public static final String ANDROID_CLIENT_TYPE = "android";
    public static final String CLS_ANDROID_SDK_DEVELOPER_TOKEN = "470fa2b4ae81cd56ecbcda9735803434cec591fa";
    public static final String CRASHLYTICS_USER_AGENT = "Crashlytics Android SDK/";
    public static final int DEFAULT_TIMEOUT = 10000;
    public static final String HEADER_ACCEPT = "Accept";
    public static final String HEADER_API_KEY = "X-CRASHLYTICS-API-KEY";
    public static final String HEADER_CLIENT_TYPE = "X-CRASHLYTICS-API-CLIENT-TYPE";
    public static final String HEADER_CLIENT_VERSION = "X-CRASHLYTICS-API-CLIENT-VERSION";
    public static final String HEADER_DEVELOPER_TOKEN = "X-CRASHLYTICS-DEVELOPER-TOKEN";
    public static final String HEADER_REQUEST_ID = "X-REQUEST-ID";
    public static final String HEADER_USER_AGENT = "User-Agent";
    private static final Pattern PROTOCOL_AND_HOST_PATTERN = Pattern.compile("http(s?)://[^\\/]+", 2);
    protected final C0458i kit;
    private final C0417c method;
    private final String protocolAndHostOverride;
    private final C0423e requestFactory;
    private final String url;

    public C0356a(C0458i c0458i, String str, String str2, C0423e c0423e, C0417c c0417c) {
        if (str2 == null) {
            throw new IllegalArgumentException("url must not be null.");
        } else if (c0423e == null) {
            throw new IllegalArgumentException("requestFactory must not be null.");
        } else {
            this.kit = c0458i;
            this.protocolAndHostOverride = str;
            this.url = overrideProtocolAndHost(str2);
            this.requestFactory = c0423e;
            this.method = c0417c;
        }
    }

    protected String getUrl() {
        return this.url;
    }

    protected C0422d getHttpRequest() {
        return getHttpRequest(Collections.emptyMap());
    }

    protected C0422d getHttpRequest(Map<String, String> map) {
        map = this.requestFactory.mo1232a(this.method, getUrl(), map).m293a(false).m283a((int) DEFAULT_TIMEOUT);
        String str = HEADER_USER_AGENT;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(CRASHLYTICS_USER_AGENT);
        stringBuilder.append(this.kit.getVersion());
        return map.m286a(str, stringBuilder.toString()).m286a(HEADER_DEVELOPER_TOKEN, CLS_ANDROID_SDK_DEVELOPER_TOKEN);
    }

    private String overrideProtocolAndHost(String str) {
        return !C0367i.m140c(this.protocolAndHostOverride) ? PROTOCOL_AND_HOST_PATTERN.matcher(str).replaceFirst(this.protocolAndHostOverride) : str;
    }
}
