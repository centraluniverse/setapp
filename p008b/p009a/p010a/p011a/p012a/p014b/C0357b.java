package p008b.p009a.p010a.p011a.p012a.p014b;

/* compiled from: AdvertisingInfo */
class C0357b {
    public final String f119a;
    public final boolean f120b;

    C0357b(String str, boolean z) {
        this.f119a = str;
        this.f120b = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null) {
            if (getClass() == obj.getClass()) {
                C0357b c0357b = (C0357b) obj;
                if (this.f120b != c0357b.f120b) {
                    return false;
                }
                if (this.f119a == null) {
                    return c0357b.f119a == null;
                } else {
                    if (this.f119a.equals(c0357b.f119a) == null) {
                    }
                }
            }
        }
        return false;
    }

    public int hashCode() {
        return (31 * (this.f119a != null ? this.f119a.hashCode() : 0)) + this.f120b;
    }
}
