package p008b.p009a.p010a.p011a.p012a.p014b;

import android.os.SystemClock;
import android.util.Log;

/* compiled from: TimingMetric */
public class C0382t {
    private final String f189a;
    private final String f190b;
    private final boolean f191c;
    private long f192d;
    private long f193e;

    public C0382t(String str, String str2) {
        this.f189a = str;
        this.f190b = str2;
        this.f191c = Log.isLoggable(str2, 2) ^ 1;
    }

    public synchronized void m210a() {
        if (!this.f191c) {
            this.f192d = SystemClock.elapsedRealtime();
            this.f193e = 0;
        }
    }

    public synchronized void m211b() {
        if (!this.f191c) {
            if (this.f193e == 0) {
                this.f193e = SystemClock.elapsedRealtime() - this.f192d;
                m209c();
            }
        }
    }

    private void m209c() {
        String str = this.f190b;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.f189a);
        stringBuilder.append(": ");
        stringBuilder.append(this.f193e);
        stringBuilder.append("ms");
        Log.v(str, stringBuilder.toString());
    }
}
