package p008b.p009a.p010a.p011a.p012a.p014b;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;
import p008b.p009a.p010a.p011a.C0452c;

/* compiled from: CommonUtils */
public class C0367i {
    public static final Comparator<File> f138a = new C03651();
    private static Boolean f139b = null;
    private static final char[] f140c = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static long f141d = -1;

    /* compiled from: CommonUtils */
    static class C03651 implements Comparator<File> {
        C03651() {
        }

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return m106a((File) obj, (File) obj2);
        }

        public int m106a(File file, File file2) {
            return (int) (file.lastModified() - file2.lastModified());
        }
    }

    /* compiled from: CommonUtils */
    enum C0366a {
        X86_32,
        X86_64,
        ARM_UNKNOWN,
        PPC,
        PPC64,
        ARMV6,
        ARMV7,
        UNKNOWN,
        ARMV7S,
        ARM64;
        
        private static final Map<String, C0366a> f136k = null;

        static {
            f136k = new HashMap(4);
            f136k.put("armeabi-v7a", ARMV7);
            f136k.put("armeabi", ARMV6);
            f136k.put("arm64-v8a", ARM64);
            f136k.put("x86", X86_32);
        }

        static C0366a m107a() {
            Object obj = Build.CPU_ABI;
            if (TextUtils.isEmpty(obj)) {
                C0452c.m367h().mo1249a("Fabric", "Architecture#getValue()::Build.CPU_ABI returned null or empty");
                return UNKNOWN;
            }
            C0366a c0366a = (C0366a) f136k.get(obj.toLowerCase(Locale.US));
            if (c0366a == null) {
                c0366a = UNKNOWN;
            }
            return c0366a;
        }
    }

    public static String m134b(int i) {
        switch (i) {
            case 2:
                return "V";
            case 3:
                return "D";
            case 4:
                return "I";
            case 5:
                return "W";
            case 6:
                return "E";
            case 7:
                return "A";
            default:
                return "?";
        }
    }

    public static SharedPreferences m113a(Context context) {
        return context.getSharedPreferences("com.crashlytics.prefs", 0);
    }

    public static String m115a(File file, String str) {
        Closeable bufferedReader;
        String str2 = null;
        if (file.exists()) {
            try {
                String[] split;
                bufferedReader = new BufferedReader(new FileReader(file), 1024);
                while (true) {
                    try {
                        CharSequence readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        split = Pattern.compile("\\s*:\\s*").split(readLine, 2);
                        if (split.length > 1 && split[0].equals(str)) {
                            break;
                        }
                    } catch (Exception e) {
                        str = e;
                    }
                }
                str2 = split[1];
            } catch (Exception e2) {
                str = e2;
                bufferedReader = null;
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Error parsing ");
                    stringBuilder.append(file);
                    C0452c.m367h().mo1257e("Fabric", stringBuilder.toString(), str);
                    C0367i.m127a(bufferedReader, "Failed to close system file reader.");
                    return str2;
                } catch (Throwable th) {
                    file = th;
                    C0367i.m127a(bufferedReader, "Failed to close system file reader.");
                    throw file;
                }
            } catch (Throwable th2) {
                file = th2;
                bufferedReader = null;
                C0367i.m127a(bufferedReader, "Failed to close system file reader.");
                throw file;
            }
            C0367i.m127a(bufferedReader, "Failed to close system file reader.");
        }
        return str2;
    }

    public static int m108a() {
        return C0366a.m107a().ordinal();
    }

    public static synchronized long m131b() {
        long j;
        synchronized (C0367i.class) {
            if (f141d == -1) {
                j = 0;
                Object a = C0367i.m115a(new File("/proc/meminfo"), "MemTotal");
                if (!TextUtils.isEmpty(a)) {
                    String toUpperCase = a.toUpperCase(Locale.US);
                    try {
                        long a2;
                        if (toUpperCase.endsWith("KB")) {
                            a2 = C0367i.m111a(toUpperCase, "KB", 1024);
                        } else if (toUpperCase.endsWith("MB")) {
                            a2 = C0367i.m111a(toUpperCase, "MB", 1048576);
                        } else if (toUpperCase.endsWith("GB")) {
                            a2 = C0367i.m111a(toUpperCase, "GB", 1073741824);
                        } else {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append("Unexpected meminfo format while computing RAM: ");
                            stringBuilder.append(toUpperCase);
                            C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
                        }
                        j = a2;
                    } catch (Throwable e) {
                        StringBuilder stringBuilder2 = new StringBuilder();
                        stringBuilder2.append("Unexpected meminfo format while computing RAM: ");
                        stringBuilder2.append(toUpperCase);
                        C0452c.m367h().mo1257e("Fabric", stringBuilder2.toString(), e);
                    }
                }
                f141d = j;
            }
            j = f141d;
        }
        return j;
    }

    static long m111a(String str, String str2, int i) {
        return Long.parseLong(str.split(str2)[null].trim()) * ((long) i);
    }

    public static RunningAppProcessInfo m112a(String str, Context context) {
        Context<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.processName.equals(str)) {
                    return runningAppProcessInfo;
                }
            }
        }
        return null;
    }

    public static String m116a(InputStream inputStream) throws IOException {
        inputStream = new Scanner(inputStream).useDelimiter("\\A");
        return inputStream.hasNext() ? inputStream.next() : "";
    }

    public static String m118a(String str) {
        return C0367i.m119a(str, "SHA-1");
    }

    public static String m136b(InputStream inputStream) {
        return C0367i.m117a(inputStream, "SHA-1");
    }

    private static String m117a(InputStream inputStream, String str) {
        try {
            str = MessageDigest.getInstance("SHA-1");
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    return C0367i.m120a(str.digest());
                }
                str.update(bArr, 0, read);
            }
        } catch (InputStream inputStream2) {
            C0452c.m367h().mo1257e("Fabric", "Could not calculate hash for app icon.", inputStream2);
            return "";
        }
    }

    private static String m121a(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.update(bArr);
            return C0367i.m120a(instance.digest());
        } catch (byte[] bArr2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Could not create hashing algorithm: ");
            stringBuilder.append(str);
            stringBuilder.append(", returning empty string.");
            C0452c.m367h().mo1257e("Fabric", stringBuilder.toString(), bArr2);
            return "";
        }
    }

    private static String m119a(String str, String str2) {
        return C0367i.m121a(str.getBytes(), str2);
    }

    public static String m122a(String... strArr) {
        String str = null;
        if (strArr != null) {
            if (strArr.length != 0) {
                List<String> arrayList = new ArrayList();
                for (String str2 : strArr) {
                    if (str2 != null) {
                        arrayList.add(str2.replace("-", "").toLowerCase(Locale.US));
                    }
                }
                Collections.sort(arrayList);
                strArr = new StringBuilder();
                for (String append : arrayList) {
                    strArr.append(append);
                }
                String stringBuilder = strArr.toString();
                if (stringBuilder.length() > 0) {
                    str = C0367i.m118a(stringBuilder);
                }
                return str;
            }
        }
        return null;
    }

    public static long m132b(Context context) {
        MemoryInfo memoryInfo = new MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    public static long m133b(String str) {
        StatFs statFs = new StatFs(str);
        long blockSize = (long) statFs.getBlockSize();
        return (((long) statFs.getBlockCount()) * blockSize) - (blockSize * ((long) statFs.getAvailableBlocks()));
    }

    public static Float m137c(Context context) {
        context = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (context == null) {
            return null;
        }
        return Float.valueOf(((float) context.getIntExtra("level", -1)) / ((float) context.getIntExtra("scale", -1)));
    }

    public static boolean m141d(Context context) {
        boolean z = false;
        if (C0367i.m143f(context)) {
            return false;
        }
        if (((SensorManager) context.getSystemService("sensor")).getDefaultSensor(8) != null) {
            z = true;
        }
        return z;
    }

    public static void m124a(Context context, String str) {
        if (C0367i.m142e(context) != null) {
            C0452c.m367h().mo1249a("Fabric", str);
        }
    }

    public static void m125a(Context context, String str, Throwable th) {
        if (C0367i.m142e(context) != null) {
            C0452c.m367h().mo1256e("Fabric", str);
        }
    }

    public static void m123a(Context context, int i, String str, String str2) {
        if (C0367i.m142e(context) != null) {
            C0452c.m367h().mo1247a(i, "Fabric", str2);
        }
    }

    public static boolean m142e(Context context) {
        if (f139b == null) {
            f139b = Boolean.valueOf(C0367i.m130a(context, "com.crashlytics.Trace", false));
        }
        return f139b.booleanValue();
    }

    public static boolean m130a(Context context, String str, boolean z) {
        if (context != null) {
            Resources resources = context.getResources();
            if (resources != null) {
                int a = C0367i.m109a(context, str, "bool");
                if (a > 0) {
                    return resources.getBoolean(a);
                }
                str = C0367i.m109a(context, str, "string");
                if (str > null) {
                    return Boolean.parseBoolean(context.getString(str));
                }
            }
        }
        return z;
    }

    public static int m109a(Context context, String str, String str2) {
        return context.getResources().getIdentifier(str, str2, C0367i.m147j(context));
    }

    public static boolean m143f(Context context) {
        context = Secure.getString(context.getContentResolver(), "android_id");
        if (!("sdk".equals(Build.PRODUCT) || "google_sdk".equals(Build.PRODUCT))) {
            if (context != null) {
                return null;
            }
        }
        return true;
    }

    public static boolean m144g(Context context) {
        context = C0367i.m143f(context);
        String str = Build.TAGS;
        if ((context == null && str != null && str.contains("test-keys")) || new File("/system/app/Superuser.apk").exists()) {
            return true;
        }
        File file = new File("/system/xbin/su");
        if (context != null || file.exists() == null) {
            return null;
        }
        return true;
    }

    public static boolean m138c() {
        if (!Debug.isDebuggerConnected()) {
            if (!Debug.waitingForDebugger()) {
                return false;
            }
        }
        return true;
    }

    public static int m145h(Context context) {
        int i = C0367i.m143f(context) ? 1 : 0;
        if (C0367i.m144g(context) != null) {
            i |= 2;
        }
        return C0367i.m138c() != null ? i | 4 : i;
    }

    public static int m110a(Context context, boolean z) {
        context = C0367i.m137c(context);
        if (z) {
            if (context != null) {
                if (((double) context.floatValue()) >= 99.0d) {
                    return 3;
                }
                return ((double) context.floatValue()) < 4636666922610458624 ? 2 : null;
            }
        }
        return 1;
    }

    public static String m120a(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = bArr[i] & 255;
            int i3 = i * 2;
            cArr[i3] = f140c[i2 >>> 4];
            cArr[i3 + 1] = f140c[i2 & 15];
        }
        return new String(cArr);
    }

    public static boolean m146i(Context context) {
        return (context.getApplicationInfo().flags & 2) != null ? true : null;
    }

    public static String m135b(Context context, String str) {
        str = C0367i.m109a(context, str, "string");
        return str > null ? context.getString(str) : "";
    }

    public static void m127a(Closeable closeable, String str) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Closeable closeable2) {
                C0452c.m367h().mo1257e("Fabric", str, closeable2);
            }
        }
    }

    public static void m128a(Flushable flushable, String str) {
        if (flushable != null) {
            try {
                flushable.flush();
            } catch (Flushable flushable2) {
                C0452c.m367h().mo1257e("Fabric", str, flushable2);
            }
        }
    }

    public static boolean m140c(String str) {
        if (str != null) {
            if (str.length() != null) {
                return null;
            }
        }
        return true;
    }

    public static String m114a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("value must be zero or greater");
        }
        return String.format(Locale.US, "%1$10s", new Object[]{Integer.valueOf(i)}).replace(' ', '0');
    }

    public static String m147j(Context context) {
        int i = context.getApplicationContext().getApplicationInfo().icon;
        if (i > 0) {
            return context.getResources().getResourcePackageName(i);
        }
        return context.getPackageName();
    }

    public static void m129a(InputStream inputStream, OutputStream outputStream, byte[] bArr) throws IOException {
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static String m148k(Context context) {
        Throwable e;
        Throwable th;
        String str = null;
        Closeable openRawResource;
        try {
            openRawResource = context.getResources().openRawResource(C0367i.m149l(context));
            try {
                String b = C0367i.m136b((InputStream) openRawResource);
                if (!C0367i.m140c(b)) {
                    str = b;
                }
                C0367i.m127a(openRawResource, "Failed to close icon input stream.");
                return str;
            } catch (Exception e2) {
                e = e2;
                try {
                    C0452c.m367h().mo1257e("Fabric", "Could not calculate hash for app icon.", e);
                    C0367i.m127a(openRawResource, "Failed to close icon input stream.");
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    C0367i.m127a(openRawResource, "Failed to close icon input stream.");
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            openRawResource = null;
            C0452c.m367h().mo1257e("Fabric", "Could not calculate hash for app icon.", e);
            C0367i.m127a(openRawResource, "Failed to close icon input stream.");
            return null;
        } catch (Context context2) {
            th = context2;
            openRawResource = null;
            C0367i.m127a(openRawResource, "Failed to close icon input stream.");
            throw th;
        }
    }

    public static int m149l(Context context) {
        return context.getApplicationContext().getApplicationInfo().icon;
    }

    public static String m150m(Context context) {
        int a = C0367i.m109a(context, "io.fabric.android.build_id", "string");
        if (a == 0) {
            a = C0367i.m109a(context, "com.crashlytics.android.build_id", "string");
        }
        if (a == 0) {
            return null;
        }
        context = context.getResources().getString(a);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Build ID is: ");
        stringBuilder.append(context);
        C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
        return context;
    }

    public static void m126a(java.io.Closeable r0) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        if (r0 == 0) goto L_0x0008;
    L_0x0002:
        r0.close();	 Catch:{ RuntimeException -> 0x0006, Exception -> 0x0008 }
        goto L_0x0008;
    L_0x0006:
        r0 = move-exception;
        throw r0;
    L_0x0008:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.b.i.a(java.io.Closeable):void");
    }

    public static boolean m139c(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == null ? true : null;
    }

    public static boolean m151n(Context context) {
        boolean z = true;
        if (!C0367i.m139c(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return true;
        }
        context = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (context == null || context.isConnectedOrConnecting() == null) {
            z = false;
        }
        return z;
    }
}
