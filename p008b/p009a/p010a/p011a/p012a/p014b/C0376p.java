package p008b.p009a.p010a.p011a.p012a.p014b;

import android.content.Context;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.p012a.p013a.C0355d;
import p008b.p009a.p010a.p011a.p012a.p013a.C2351b;

/* compiled from: InstallerPackageNameProvider */
public class C0376p {
    private final C0355d<String> f174a = new C18061(this);
    private final C2351b<String> f175b = new C2351b();

    /* compiled from: InstallerPackageNameProvider */
    class C18061 implements C0355d<String> {
        final /* synthetic */ C0376p f4479a;

        C18061(C0376p c0376p) {
            this.f4479a = c0376p;
        }

        public /* synthetic */ Object load(Context context) throws Exception {
            return m4947a(context);
        }

        public String m4947a(Context context) throws Exception {
            context = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return context == null ? "" : context;
        }
    }

    public String m180a(Context context) {
        try {
            context = (String) this.f175b.mo1208a(context, this.f174a);
            if ("".equals(context)) {
                context = null;
            }
            return context;
        } catch (Context context2) {
            C0452c.m367h().mo1257e("Fabric", "Failed to determine installer package name", context2);
            return null;
        }
    }
}
