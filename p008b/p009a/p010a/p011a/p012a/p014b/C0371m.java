package p008b.p009a.p010a.p011a.p012a.p014b;

import java.util.Map;
import p008b.p009a.p010a.p011a.p012a.p014b.C0375o.C0374a;

/* compiled from: DeviceIdentifierProvider */
public interface C0371m {
    Map<C0374a, String> getDeviceIdentifiers();
}
