package p008b.p009a.p010a.p011a.p012a.p014b;

import android.content.Context;
import android.text.TextUtils;
import p008b.p009a.p010a.p011a.C0452c;

/* compiled from: ApiKey */
public class C0363g {
    protected String m101a() {
        return "Fabric could not be initialized, API key missing from AndroidManifest.xml. Add the following tag to your Application element \n\t<meta-data android:name=\"io.fabric.ApiKey\" android:value=\"YOUR_API_KEY\"/>";
    }

    public String m102a(Context context) {
        Object b = m103b(context);
        if (TextUtils.isEmpty(b)) {
            b = m104c(context);
        }
        if (TextUtils.isEmpty(b)) {
            m105d(context);
        }
        return b;
    }

    protected String m103b(Context context) {
        StringBuilder stringBuilder;
        String str = null;
        try {
            context = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (context == null) {
                return null;
            }
            String string = context.getString("io.fabric.ApiKey");
            if (string != null) {
                return string;
            }
            try {
                C0452c.m367h().mo1249a("Fabric", "Falling back to Crashlytics key lookup from Manifest");
                return context.getString("com.crashlytics.ApiKey");
            } catch (Exception e) {
                context = e;
                str = string;
                stringBuilder = new StringBuilder();
                stringBuilder.append("Caught non-fatal exception while retrieving apiKey: ");
                stringBuilder.append(context);
                C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
                return str;
            }
        } catch (Exception e2) {
            context = e2;
            stringBuilder = new StringBuilder();
            stringBuilder.append("Caught non-fatal exception while retrieving apiKey: ");
            stringBuilder.append(context);
            C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
            return str;
        }
    }

    protected String m104c(Context context) {
        int a = C0367i.m109a(context, "io.fabric.ApiKey", "string");
        if (a == 0) {
            C0452c.m367h().mo1249a("Fabric", "Falling back to Crashlytics key lookup from Strings");
            a = C0367i.m109a(context, "com.crashlytics.ApiKey", "string");
        }
        return a != 0 ? context.getResources().getString(a) : null;
    }

    protected void m105d(Context context) {
        if (!C0452c.m368i()) {
            if (C0367i.m146i(context) == null) {
                C0452c.m367h().mo1256e("Fabric", m101a());
                return;
            }
        }
        throw new IllegalArgumentException(m101a());
    }
}
