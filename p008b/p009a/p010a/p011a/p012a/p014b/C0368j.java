package p008b.p009a.p010a.p011a.p012a.p014b;

/* compiled from: Crash */
public abstract class C0368j {
    private final String f142a;
    private final String f143b;

    /* compiled from: Crash */
    public static class C1802a extends C0368j {
        public C1802a(String str, String str2) {
            super(str, str2);
        }
    }

    /* compiled from: Crash */
    public static class C1803b extends C0368j {
        public C1803b(String str, String str2) {
            super(str, str2);
        }
    }

    public C0368j(String str, String str2) {
        this.f142a = str;
        this.f143b = str2;
    }

    public String m152a() {
        return this.f142a;
    }

    public String m153b() {
        return this.f143b;
    }
}
