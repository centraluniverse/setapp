package p008b.p009a.p010a.p011a.p012a.p014b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.p012a.p019f.C0429c;
import p008b.p009a.p010a.p011a.p012a.p019f.C1820d;

/* compiled from: AdvertisingInfoProvider */
class C0358c {
    private final Context f121a;
    private final C0429c f122b;

    public C0358c(Context context) {
        this.f121a = context.getApplicationContext();
        this.f122b = new C1820d(context, "TwitterAdvertisingInfoPreferences");
    }

    public C0357b m93a() {
        C0357b b = m94b();
        if (m91c(b)) {
            C0452c.m367h().mo1249a("Fabric", "Using AdvertisingInfo from Preference Store");
            m88a(b);
            return b;
        }
        b = m92e();
        m90b(b);
        return b;
    }

    private void m88a(final C0357b c0357b) {
        new Thread(new C0364h(this) {
            final /* synthetic */ C0358c f4470b;

            public void onRun() {
                C0357b a = this.f4470b.m92e();
                if (!c0357b.equals(a)) {
                    C0452c.m367h().mo1249a("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
                    this.f4470b.m90b(a);
                }
            }
        }).start();
    }

    @SuppressLint({"CommitPrefEdits"})
    private void m90b(C0357b c0357b) {
        if (m91c(c0357b)) {
            this.f122b.mo1239a(this.f122b.mo1240b().putString("advertising_id", c0357b.f119a).putBoolean("limit_ad_tracking_enabled", c0357b.f120b));
        } else {
            this.f122b.mo1239a(this.f122b.mo1240b().remove("advertising_id").remove("limit_ad_tracking_enabled"));
        }
    }

    protected C0357b m94b() {
        return new C0357b(this.f122b.mo1238a().getString("advertising_id", ""), this.f122b.mo1238a().getBoolean("limit_ad_tracking_enabled", false));
    }

    public C0362f m95c() {
        return new C1800d(this.f121a);
    }

    public C0362f m96d() {
        return new C1801e(this.f121a);
    }

    private boolean m91c(C0357b c0357b) {
        return (c0357b == null || TextUtils.isEmpty(c0357b.f119a) != null) ? null : true;
    }

    private C0357b m92e() {
        C0357b a = m95c().mo1210a();
        if (m91c(a)) {
            C0452c.m367h().mo1249a("Fabric", "Using AdvertisingInfo from Reflection Provider");
        } else {
            a = m96d().mo1210a();
            if (m91c(a)) {
                C0452c.m367h().mo1249a("Fabric", "Using AdvertisingInfo from Service Provider");
            } else {
                C0452c.m367h().mo1249a("Fabric", "AdvertisingInfo not present");
            }
        }
        return a;
    }
}
