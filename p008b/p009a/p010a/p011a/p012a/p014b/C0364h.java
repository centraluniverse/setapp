package p008b.p009a.p010a.p011a.p012a.p014b;

import android.os.Process;

/* compiled from: BackgroundPriorityRunnable */
public abstract class C0364h implements Runnable {
    protected abstract void onRun();

    public final void run() {
        Process.setThreadPriority(10);
        onRun();
    }
}
