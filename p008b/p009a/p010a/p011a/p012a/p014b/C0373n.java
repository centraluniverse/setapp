package p008b.p009a.p010a.p011a.p012a.p014b;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: ExecutorUtils */
public final class C0373n {
    public static ExecutorService m157a(String str) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(C0373n.m161c(str));
        C0373n.m158a(str, newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    public static ScheduledExecutorService m160b(String str) {
        Object newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor(C0373n.m161c(str));
        C0373n.m158a(str, newSingleThreadScheduledExecutor);
        return newSingleThreadScheduledExecutor;
    }

    public static final ThreadFactory m161c(final String str) {
        final AtomicLong atomicLong = new AtomicLong(1);
        return new ThreadFactory() {
            public Thread newThread(final Runnable runnable) {
                runnable = Executors.defaultThreadFactory().newThread(new C0364h(this) {
                    final /* synthetic */ C03721 f4474b;

                    public void onRun() {
                        runnable.run();
                    }
                });
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(str);
                stringBuilder.append(atomicLong.getAndIncrement());
                runnable.setName(stringBuilder.toString());
                return runnable;
            }
        };
    }

    private static final void m158a(String str, ExecutorService executorService) {
        C0373n.m159a(str, executorService, 2, TimeUnit.SECONDS);
    }

    public static final void m159a(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
        Runtime runtime = Runtime.getRuntime();
        final String str2 = str;
        final ExecutorService executorService2 = executorService;
        final long j2 = j;
        final TimeUnit timeUnit2 = timeUnit;
        Runnable c18052 = new C0364h() {
            public void onRun() {
                /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                /*
                r7 = this;
                r0 = p008b.p009a.p010a.p011a.C0452c.m367h();	 Catch:{ InterruptedException -> 0x004f }
                r1 = "Fabric";	 Catch:{ InterruptedException -> 0x004f }
                r2 = new java.lang.StringBuilder;	 Catch:{ InterruptedException -> 0x004f }
                r2.<init>();	 Catch:{ InterruptedException -> 0x004f }
                r3 = "Executing shutdown hook for ";	 Catch:{ InterruptedException -> 0x004f }
                r2.append(r3);	 Catch:{ InterruptedException -> 0x004f }
                r3 = r3;	 Catch:{ InterruptedException -> 0x004f }
                r2.append(r3);	 Catch:{ InterruptedException -> 0x004f }
                r2 = r2.toString();	 Catch:{ InterruptedException -> 0x004f }
                r0.mo1249a(r1, r2);	 Catch:{ InterruptedException -> 0x004f }
                r0 = r4;	 Catch:{ InterruptedException -> 0x004f }
                r0.shutdown();	 Catch:{ InterruptedException -> 0x004f }
                r0 = r4;	 Catch:{ InterruptedException -> 0x004f }
                r1 = r5;	 Catch:{ InterruptedException -> 0x004f }
                r3 = r7;	 Catch:{ InterruptedException -> 0x004f }
                r0 = r0.awaitTermination(r1, r3);	 Catch:{ InterruptedException -> 0x004f }
                if (r0 != 0) goto L_0x006d;	 Catch:{ InterruptedException -> 0x004f }
            L_0x002d:
                r0 = p008b.p009a.p010a.p011a.C0452c.m367h();	 Catch:{ InterruptedException -> 0x004f }
                r1 = "Fabric";	 Catch:{ InterruptedException -> 0x004f }
                r2 = new java.lang.StringBuilder;	 Catch:{ InterruptedException -> 0x004f }
                r2.<init>();	 Catch:{ InterruptedException -> 0x004f }
                r3 = r3;	 Catch:{ InterruptedException -> 0x004f }
                r2.append(r3);	 Catch:{ InterruptedException -> 0x004f }
                r3 = " did not shut down in the allocated time. Requesting immediate shutdown.";	 Catch:{ InterruptedException -> 0x004f }
                r2.append(r3);	 Catch:{ InterruptedException -> 0x004f }
                r2 = r2.toString();	 Catch:{ InterruptedException -> 0x004f }
                r0.mo1249a(r1, r2);	 Catch:{ InterruptedException -> 0x004f }
                r0 = r4;	 Catch:{ InterruptedException -> 0x004f }
                r0.shutdownNow();	 Catch:{ InterruptedException -> 0x004f }
                goto L_0x006d;
            L_0x004f:
                r0 = p008b.p009a.p010a.p011a.C0452c.m367h();
                r1 = "Fabric";
                r2 = java.util.Locale.US;
                r3 = "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.";
                r4 = 1;
                r4 = new java.lang.Object[r4];
                r5 = 0;
                r6 = r3;
                r4[r5] = r6;
                r2 = java.lang.String.format(r2, r3, r4);
                r0.mo1249a(r1, r2);
                r0 = r4;
                r0.shutdownNow();
            L_0x006d:
                return;
                */
                throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.b.n.2.onRun():void");
            }
        };
        executorService = new StringBuilder();
        executorService.append("Crashlytics Shutdown Hook for ");
        executorService.append(str);
        runtime.addShutdownHook(new Thread(c18052, executorService.toString()));
    }
}
