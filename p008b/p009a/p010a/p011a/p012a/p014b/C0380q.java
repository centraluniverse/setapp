package p008b.p009a.p010a.p011a.p012a.p014b;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.WritableByteChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: QueueFile */
public class C0380q implements Closeable {
    private static final Logger f182b = Logger.getLogger(C0380q.class.getName());
    int f183a;
    private final RandomAccessFile f184c;
    private int f185d;
    private C0377a f186e;
    private C0377a f187f;
    private final byte[] f188g = new byte[16];

    /* compiled from: QueueFile */
    static class C0377a {
        static final C0377a f176a = new C0377a(0, 0);
        final int f177b;
        final int f178c;

        C0377a(int i, int i2) {
            this.f177b = i;
            this.f178c = i2;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(getClass().getSimpleName());
            stringBuilder.append("[position = ");
            stringBuilder.append(this.f177b);
            stringBuilder.append(", length = ");
            stringBuilder.append(this.f178c);
            stringBuilder.append("]");
            return stringBuilder.toString();
        }
    }

    /* compiled from: QueueFile */
    private final class C0378b extends InputStream {
        final /* synthetic */ C0380q f179a;
        private int f180b;
        private int f181c;

        private C0378b(C0380q c0380q, C0377a c0377a) {
            this.f179a = c0380q;
            this.f180b = c0380q.m191b(c0377a.f177b + 4);
            this.f181c = c0377a.f178c;
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            C0380q.m193b(bArr, "buffer");
            if ((i | i2) >= 0) {
                if (i2 <= bArr.length - i) {
                    if (this.f181c <= 0) {
                        return -1;
                    }
                    if (i2 > this.f181c) {
                        i2 = this.f181c;
                    }
                    this.f179a.m194b(this.f180b, bArr, i, i2);
                    this.f180b = this.f179a.m191b(this.f180b + i2);
                    this.f181c -= i2;
                    return i2;
                }
            }
            throw new ArrayIndexOutOfBoundsException();
        }

        public int read() throws IOException {
            if (this.f181c == 0) {
                return -1;
            }
            this.f179a.f184c.seek((long) this.f180b);
            int read = this.f179a.f184c.read();
            this.f180b = this.f179a.m191b(this.f180b + 1);
            this.f181c--;
            return read;
        }
    }

    /* compiled from: QueueFile */
    public interface C0379c {
        void read(InputStream inputStream, int i) throws IOException;
    }

    public C0380q(File file) throws IOException {
        if (!file.exists()) {
            C0380q.m189a(file);
        }
        this.f184c = C0380q.m192b(file);
        m198e();
    }

    private static void m195b(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    private static void m190a(byte[] bArr, int... iArr) {
        int i = 0;
        int length = iArr.length;
        int i2 = 0;
        while (i < length) {
            C0380q.m195b(bArr, i2, iArr[i]);
            i2 += 4;
            i++;
        }
    }

    private static int m182a(byte[] bArr, int i) {
        return ((((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << 16)) + ((bArr[i + 2] & 255) << 8)) + (bArr[i + 3] & 255);
    }

    private void m198e() throws IOException {
        this.f184c.seek(0);
        this.f184c.readFully(this.f188g);
        this.f183a = C0380q.m182a(this.f188g, 0);
        if (((long) this.f183a) > this.f184c.length()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("File is truncated. Expected length: ");
            stringBuilder.append(this.f183a);
            stringBuilder.append(", Actual length: ");
            stringBuilder.append(this.f184c.length());
            throw new IOException(stringBuilder.toString());
        }
        this.f185d = C0380q.m182a(this.f188g, 4);
        int a = C0380q.m182a(this.f188g, 8);
        int a2 = C0380q.m182a(this.f188g, 12);
        this.f186e = m183a(a);
        this.f187f = m183a(a2);
    }

    private void m186a(int i, int i2, int i3, int i4) throws IOException {
        C0380q.m190a(this.f188g, i, i2, i3, i4);
        this.f184c.seek(0);
        this.f184c.write(this.f188g);
    }

    private C0377a m183a(int i) throws IOException {
        if (i == 0) {
            return C0377a.f176a;
        }
        this.f184c.seek((long) i);
        return new C0377a(i, this.f184c.readInt());
    }

    private static void m189a(File file) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(file.getPath());
        stringBuilder.append(".tmp");
        File file2 = new File(stringBuilder.toString());
        RandomAccessFile b = C0380q.m192b(file2);
        try {
            b.setLength(PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM);
            b.seek(0);
            byte[] bArr = new byte[16];
            C0380q.m190a(bArr, 4096, 0, 0, 0);
            b.write(bArr);
            if (file2.renameTo(file) == null) {
                throw new IOException("Rename failed!");
            }
        } finally {
            b.close();
        }
    }

    private static RandomAccessFile m192b(File file) throws FileNotFoundException {
        return new RandomAccessFile(file, "rwd");
    }

    private int m191b(int i) {
        return i < this.f183a ? i : (16 + i) - this.f183a;
    }

    private void m187a(int i, byte[] bArr, int i2, int i3) throws IOException {
        i = m191b(i);
        if (i + i3 <= this.f183a) {
            this.f184c.seek((long) i);
            this.f184c.write(bArr, i2, i3);
            return;
        }
        int i4 = this.f183a - i;
        this.f184c.seek((long) i);
        this.f184c.write(bArr, i2, i4);
        this.f184c.seek(16);
        this.f184c.write(bArr, i2 + i4, i3 - i4);
    }

    private void m194b(int i, byte[] bArr, int i2, int i3) throws IOException {
        i = m191b(i);
        if (i + i3 <= this.f183a) {
            this.f184c.seek((long) i);
            this.f184c.readFully(bArr, i2, i3);
            return;
        }
        int i4 = this.f183a - i;
        this.f184c.seek((long) i);
        this.f184c.readFully(bArr, i2, i4);
        this.f184c.seek(16);
        this.f184c.readFully(bArr, i2 + i4, i3 - i4);
    }

    public void m202a(byte[] bArr) throws IOException {
        m203a(bArr, 0, bArr.length);
    }

    public synchronized void m203a(byte[] bArr, int i, int i2) throws IOException {
        C0380q.m193b(bArr, "buffer");
        if ((i | i2) >= 0) {
            if (i2 <= bArr.length - i) {
                int i3;
                m196c(i2);
                boolean b = m205b();
                if (b) {
                    i3 = 16;
                } else {
                    i3 = m191b((this.f187f.f177b + 4) + this.f187f.f178c);
                }
                C0377a c0377a = new C0377a(i3, i2);
                C0380q.m195b(this.f188g, 0, i2);
                m187a(c0377a.f177b, this.f188g, 0, 4);
                m187a(c0377a.f177b + 4, bArr, i, i2);
                m186a(this.f183a, this.f185d + 1, b ? c0377a.f177b : this.f186e.f177b, c0377a.f177b);
                this.f187f = c0377a;
                this.f185d++;
                if (b) {
                    this.f186e = this.f187f;
                }
            }
        }
        throw new IndexOutOfBoundsException();
    }

    public int m200a() {
        if (this.f185d == 0) {
            return 16;
        }
        if (this.f187f.f177b >= this.f186e.f177b) {
            return (((this.f187f.f177b - this.f186e.f177b) + 4) + this.f187f.f178c) + 16;
        }
        return (((this.f187f.f177b + 4) + this.f187f.f178c) + this.f183a) - this.f186e.f177b;
    }

    private int m199f() {
        return this.f183a - m200a();
    }

    public synchronized boolean m205b() {
        return this.f185d == 0;
    }

    private void m196c(int i) throws IOException {
        i += 4;
        int f = m199f();
        if (f < i) {
            int i2 = this.f183a;
            do {
                f += i2;
                i2 <<= 1;
            } while (f < i);
            m197d(i2);
            i = m191b((this.f187f.f177b + 4) + this.f187f.f178c);
            if (i < this.f186e.f177b) {
                WritableByteChannel channel = this.f184c.getChannel();
                channel.position((long) this.f183a);
                long j = (long) (i - 4);
                if (channel.transferTo(16, j, channel) != j) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            if (this.f187f.f177b < this.f186e.f177b) {
                i = (this.f183a + this.f187f.f177b) - 16;
                m186a(i2, this.f185d, this.f186e.f177b, i);
                this.f187f = new C0377a(i, this.f187f.f178c);
            } else {
                m186a(i2, this.f185d, this.f186e.f177b, this.f187f.f177b);
            }
            this.f183a = i2;
        }
    }

    private void m197d(int i) throws IOException {
        this.f184c.setLength((long) i);
        this.f184c.getChannel().force(true);
    }

    public synchronized void m201a(C0379c c0379c) throws IOException {
        int i = this.f186e.f177b;
        for (int i2 = 0; i2 < this.f185d; i2++) {
            C0377a a = m183a(i);
            c0379c.read(new C0378b(a), a.f178c);
            i = m191b((a.f177b + 4) + a.f178c);
        }
    }

    private static <T> T m193b(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    public synchronized void m206c() throws IOException {
        if (m205b()) {
            throw new NoSuchElementException();
        } else if (this.f185d == 1) {
            m207d();
        } else {
            int b = m191b((this.f186e.f177b + 4) + this.f186e.f178c);
            m194b(b, this.f188g, 0, 4);
            int a = C0380q.m182a(this.f188g, 0);
            m186a(this.f183a, this.f185d - 1, b, this.f187f.f177b);
            this.f185d--;
            this.f186e = new C0377a(b, a);
        }
    }

    public synchronized void m207d() throws IOException {
        m186a(4096, 0, 0, 0);
        this.f185d = 0;
        this.f186e = C0377a.f176a;
        this.f187f = C0377a.f176a;
        if (this.f183a > 4096) {
            m197d(4096);
        }
        this.f183a = 4096;
    }

    public synchronized void close() throws IOException {
        this.f184c.close();
    }

    public boolean m204a(int i, int i2) {
        return (m200a() + 4) + i <= i2;
    }

    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getClass().getSimpleName());
        stringBuilder.append('[');
        stringBuilder.append("fileLength=");
        stringBuilder.append(this.f183a);
        stringBuilder.append(", size=");
        stringBuilder.append(this.f185d);
        stringBuilder.append(", first=");
        stringBuilder.append(this.f186e);
        stringBuilder.append(", last=");
        stringBuilder.append(this.f187f);
        stringBuilder.append(", element lengths=[");
        try {
            m201a(new C0379c(this) {
                boolean f4480a = true;
                final /* synthetic */ C0380q f4482c;

                public void read(InputStream inputStream, int i) throws IOException {
                    if (this.f4480a != null) {
                        this.f4480a = null;
                    } else {
                        stringBuilder.append(", ");
                    }
                    stringBuilder.append(i);
                }
            });
        } catch (Throwable e) {
            f182b.log(Level.WARNING, "read error", e);
        }
        stringBuilder.append("]]");
        return stringBuilder.toString();
    }
}
