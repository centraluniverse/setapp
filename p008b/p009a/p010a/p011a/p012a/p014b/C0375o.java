package p008b.p009a.p010a.p011a.p012a.p014b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.C0458i;

/* compiled from: IdManager */
public class C0375o {
    private static final Pattern f161d = Pattern.compile("[^\\p{Alnum}]");
    private static final String f162e = Pattern.quote("/");
    C0358c f163a;
    C0357b f164b;
    boolean f165c;
    private final ReentrantLock f166f = new ReentrantLock();
    private final C0376p f167g;
    private final boolean f168h;
    private final boolean f169i;
    private final Context f170j;
    private final String f171k;
    private final String f172l;
    private final Collection<C0458i> f173m;

    /* compiled from: IdManager */
    public enum C0374a {
        WIFI_MAC_ADDRESS(1),
        BLUETOOTH_MAC_ADDRESS(2),
        FONT_TOKEN(53),
        ANDROID_ID(100),
        ANDROID_DEVICE_ID(101),
        ANDROID_SERIAL(102),
        ANDROID_ADVERTISING_ID(103);
        
        public final int f160h;

        private C0374a(int i) {
            this.f160h = i;
        }
    }

    public C0375o(Context context, String str, String str2, Collection<C0458i> collection) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("appIdentifier must not be null");
        } else if (collection == null) {
            throw new IllegalArgumentException("kits must not be null");
        } else {
            this.f170j = context;
            this.f171k = str;
            this.f172l = str2;
            this.f173m = collection;
            this.f167g = new C0376p();
            this.f163a = new C0358c(context);
            this.f168h = C0367i.m130a(context, "com.crashlytics.CollectDeviceIdentifiers", true);
            if (this.f168h == null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Device ID collection disabled for ");
                stringBuilder.append(context.getPackageName());
                C0452c.m367h().mo1249a("Fabric", stringBuilder.toString());
            }
            this.f169i = C0367i.m130a(context, "com.crashlytics.CollectUserIdentifiers", true);
            if (this.f169i == null) {
                collection = new StringBuilder();
                collection.append("User information collection disabled for ");
                collection.append(context.getPackageName());
                C0452c.m367h().mo1249a("Fabric", collection.toString());
            }
        }
    }

    public boolean m166a() {
        return this.f169i;
    }

    private String m163a(String str) {
        return str == null ? null : f161d.matcher(str).replaceAll("").toLowerCase(Locale.US);
    }

    public String m167b() {
        String str = this.f172l;
        if (str != null) {
            return str;
        }
        SharedPreferences a = C0367i.m113a(this.f170j);
        String string = a.getString("crashlytics.installation.id", null);
        return string == null ? m162a(a) : string;
    }

    public String m168c() {
        return this.f171k;
    }

    public String m169d() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(m170e());
        stringBuilder.append("/");
        stringBuilder.append(m171f());
        return stringBuilder.toString();
    }

    public String m170e() {
        return m165b(VERSION.RELEASE);
    }

    public String m171f() {
        return m165b(VERSION.INCREMENTAL);
    }

    public String m172g() {
        return String.format(Locale.US, "%s/%s", new Object[]{m165b(Build.MANUFACTURER), m165b(Build.MODEL)});
    }

    private String m165b(String str) {
        return str.replaceAll(f162e, "");
    }

    public String m173h() {
        String str = "";
        if (!this.f168h) {
            return str;
        }
        str = m179n();
        if (str != null) {
            return str;
        }
        SharedPreferences a = C0367i.m113a(this.f170j);
        String string = a.getString("crashlytics.installation.id", null);
        return string == null ? m162a(a) : string;
    }

    private String m162a(SharedPreferences sharedPreferences) {
        this.f166f.lock();
        try {
            String string = sharedPreferences.getString("crashlytics.installation.id", null);
            if (string == null) {
                string = m163a(UUID.randomUUID().toString());
                sharedPreferences.edit().putString("crashlytics.installation.id", string).commit();
            }
            this.f166f.unlock();
            return string;
        } catch (Throwable th) {
            this.f166f.unlock();
        }
    }

    public Map<C0374a, String> m174i() {
        Map hashMap = new HashMap();
        for (C0458i c0458i : this.f173m) {
            if (c0458i instanceof C0371m) {
                for (Entry entry : ((C0371m) c0458i).getDeviceIdentifiers().entrySet()) {
                    m164a(hashMap, (C0374a) entry.getKey(), (String) entry.getValue());
                }
            }
        }
        m164a(hashMap, C0374a.ANDROID_ID, m179n());
        m164a(hashMap, C0374a.ANDROID_ADVERTISING_ID, m178m());
        return Collections.unmodifiableMap(hashMap);
    }

    public String m175j() {
        return this.f167g.m180a(this.f170j);
    }

    synchronized C0357b m176k() {
        if (!this.f165c) {
            this.f164b = this.f163a.m93a();
            this.f165c = true;
        }
        return this.f164b;
    }

    public Boolean m177l() {
        if (this.f168h) {
            C0357b k = m176k();
            if (k != null) {
                return Boolean.valueOf(k.f120b);
            }
        }
        return null;
    }

    public String m178m() {
        if (this.f168h) {
            C0357b k = m176k();
            if (k != null) {
                return k.f119a;
            }
        }
        return null;
    }

    private void m164a(Map<C0374a, String> map, C0374a c0374a, String str) {
        if (str != null) {
            map.put(c0374a, str);
        }
    }

    public String m179n() {
        if (this.f168h) {
            String string = Secure.getString(this.f170j.getContentResolver(), "android_id");
            if (!"9774d56d682e549c".equals(string)) {
                return m163a(string);
            }
        }
        return null;
    }
}
