package p008b.p009a.p010a.p011a.p012a.p019f;

import android.content.Context;
import java.io.File;
import p008b.p009a.p010a.p011a.C0452c;
import p008b.p009a.p010a.p011a.C0458i;

/* compiled from: FileStoreImpl */
public class C1819b implements C0428a {
    private final Context f4501a;
    private final String f4502b;
    private final String f4503c;

    public C1819b(C0458i c0458i) {
        if (c0458i.getContext() == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f4501a = c0458i.getContext();
        this.f4502b = c0458i.getPath();
        c0458i = new StringBuilder();
        c0458i.append("Android/");
        c0458i.append(this.f4501a.getPackageName());
        this.f4503c = c0458i.toString();
    }

    public File mo1237a() {
        return m4978a(this.f4501a.getFilesDir());
    }

    File m4978a(File file) {
        if (file != null) {
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    C0452c.m367h().mo1254d("Fabric", "Couldn't create file");
                }
            }
            return file;
        }
        C0452c.m367h().mo1249a("Fabric", "Null File");
        return null;
    }
}
