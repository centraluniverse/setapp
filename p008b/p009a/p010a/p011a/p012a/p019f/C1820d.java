package p008b.p009a.p010a.p011a.p012a.p019f;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import p008b.p009a.p010a.p011a.C0458i;

/* compiled from: PreferenceStoreImpl */
public class C1820d implements C0429c {
    private final SharedPreferences f4504a;
    private final String f4505b;
    private final Context f4506c;

    public C1820d(Context context, String str) {
        if (context == null) {
            throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
        }
        this.f4506c = context;
        this.f4505b = str;
        this.f4504a = this.f4506c.getSharedPreferences(this.f4505b, 0);
    }

    @Deprecated
    public C1820d(C0458i c0458i) {
        this(c0458i.getContext(), c0458i.getClass().getName());
    }

    public SharedPreferences mo1238a() {
        return this.f4504a;
    }

    public Editor mo1240b() {
        return this.f4504a.edit();
    }

    @TargetApi(9)
    public boolean mo1239a(Editor editor) {
        if (VERSION.SDK_INT < 9) {
            return editor.commit();
        }
        editor.apply();
        return true;
    }
}
