package p008b.p009a.p010a.p011a.p012a.p019f;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/* compiled from: PreferenceStore */
public interface C0429c {
    SharedPreferences mo1238a();

    boolean mo1239a(Editor editor);

    Editor mo1240b();
}
