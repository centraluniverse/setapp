package p008b.p009a.p010a.p011a.p012a.p013a;

import android.content.Context;

/* compiled from: AbstractValueCache */
public abstract class C1798a<T> implements C0354c<T> {
    private final C0354c<T> f4468a;

    protected abstract T mo2437a(Context context);

    protected abstract void mo2438a(Context context, T t);

    public C1798a(C0354c<T> c0354c) {
        this.f4468a = c0354c;
    }

    public final synchronized T mo1208a(Context context, C0355d<T> c0355d) throws Exception {
        T a;
        a = mo2437a(context);
        if (a == null) {
            a = this.f4468a != null ? this.f4468a.mo1208a(context, c0355d) : c0355d.load(context);
            m4937b(context, a);
        }
        return a;
    }

    private void m4937b(Context context, T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        mo2438a(context, (Object) t);
    }
}
