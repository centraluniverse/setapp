package p008b.p009a.p010a.p011a.p012a.p013a;

import android.content.Context;

/* compiled from: ValueCache */
public interface C0354c<T> {
    T mo1208a(Context context, C0355d<T> c0355d) throws Exception;
}
