package p008b.p009a.p010a.p011a.p012a.p013a;

import android.content.Context;

/* compiled from: MemoryValueCache */
public class C2351b<T> extends C1798a<T> {
    private T f5825a;

    public C2351b() {
        this(null);
    }

    public C2351b(C0354c<T> c0354c) {
        super(c0354c);
    }

    protected T mo2437a(Context context) {
        return this.f5825a;
    }

    protected void mo2438a(Context context, T t) {
        this.f5825a = t;
    }
}
