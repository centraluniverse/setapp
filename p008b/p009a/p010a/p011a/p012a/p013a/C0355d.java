package p008b.p009a.p010a.p011a.p012a.p013a;

import android.content.Context;

/* compiled from: ValueLoader */
public interface C0355d<T> {
    T load(Context context) throws Exception;
}
