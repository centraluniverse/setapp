package p008b.p009a.p010a.p011a.p012a.p017d;

import android.content.Context;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;
import p008b.p009a.p010a.p011a.p012a.p014b.C0380q;

/* compiled from: QueueFileEventStorage */
public class C1815h implements C0410c {
    private final Context f4489a;
    private final File f4490b;
    private final String f4491c;
    private final File f4492d;
    private C0380q f4493e = new C0380q(this.f4492d);
    private File f4494f;

    public C1815h(Context context, File file, String str, String str2) throws IOException {
        this.f4489a = context;
        this.f4490b = file;
        this.f4491c = str2;
        this.f4492d = new File(this.f4490b, str);
        m4956e();
    }

    private void m4956e() {
        this.f4494f = new File(this.f4490b, this.f4491c);
        if (!this.f4494f.exists()) {
            this.f4494f.mkdirs();
        }
    }

    public void mo1226a(byte[] bArr) throws IOException {
        this.f4493e.m202a(bArr);
    }

    public int mo1222a() {
        return this.f4493e.m200a();
    }

    public void mo1224a(String str) throws IOException {
        this.f4493e.close();
        m4955a(this.f4492d, new File(this.f4494f, str));
        this.f4493e = new C0380q(this.f4492d);
    }

    private void m4955a(File file, File file2) throws IOException {
        Closeable fileInputStream;
        Closeable a;
        Closeable closeable = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                a = mo2440a(file2);
            } catch (Throwable th) {
                file2 = th;
                C0367i.m127a(fileInputStream, "Failed to close file input stream");
                C0367i.m127a(closeable, "Failed to close output stream");
                file.delete();
                throw file2;
            }
            try {
                C0367i.m129a((InputStream) fileInputStream, (OutputStream) a, new byte[1024]);
                C0367i.m127a(fileInputStream, "Failed to close file input stream");
                C0367i.m127a(a, "Failed to close output stream");
                file.delete();
            } catch (Throwable th2) {
                Throwable th3 = th2;
                closeable = a;
                file2 = th3;
                C0367i.m127a(fileInputStream, "Failed to close file input stream");
                C0367i.m127a(closeable, "Failed to close output stream");
                file.delete();
                throw file2;
            }
        } catch (Throwable th4) {
            file2 = th4;
            fileInputStream = null;
            C0367i.m127a(fileInputStream, "Failed to close file input stream");
            C0367i.m127a(closeable, "Failed to close output stream");
            file.delete();
            throw file2;
        }
    }

    public OutputStream mo2440a(File file) throws IOException {
        return new FileOutputStream(file);
    }

    public List<File> mo1223a(int i) {
        List<File> arrayList = new ArrayList();
        for (Object add : this.f4494f.listFiles()) {
            arrayList.add(add);
            if (arrayList.size() >= i) {
                break;
            }
        }
        return arrayList;
    }

    public void mo1225a(List<File> list) {
        for (File file : list) {
            C0367i.m124a(this.f4489a, String.format("deleting sent analytics file %s", new Object[]{file.getName()}));
            file.delete();
        }
    }

    public List<File> mo1229c() {
        return Arrays.asList(this.f4494f.listFiles());
    }

    public void mo1230d() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
*/
        /*
        r1 = this;
        r0 = r1.f4493e;	 Catch:{ IOException -> 0x0005 }
        r0.close();	 Catch:{ IOException -> 0x0005 }
    L_0x0005:
        r0 = r1.f4492d;
        r0.delete();
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.d.h.d():void");
    }

    public boolean mo1228b() {
        return this.f4493e.m205b();
    }

    public boolean mo1227a(int i, int i2) {
        return this.f4493e.m204a(i, i2);
    }
}
