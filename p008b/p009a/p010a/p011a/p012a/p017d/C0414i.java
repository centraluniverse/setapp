package p008b.p009a.p010a.p011a.p012a.p017d;

import android.content.Context;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;

/* compiled from: TimeBasedFileRollOverRunnable */
public class C0414i implements Runnable {
    private final Context f241a;
    private final C0412e f242b;

    public C0414i(Context context, C0412e c0412e) {
        this.f241a = context;
        this.f242b = c0412e;
    }

    public void run() {
        try {
            C0367i.m124a(this.f241a, "Performing time based file roll over.");
            if (!this.f242b.rollFileOver()) {
                this.f242b.cancelTimeBasedFileRollOver();
            }
        } catch (Throwable e) {
            C0367i.m125a(this.f241a, "Failed to roll over file", e);
        }
    }
}
