package p008b.p009a.p010a.p011a.p012a.p017d;

import android.content.Context;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;
import p008b.p009a.p010a.p011a.p012a.p014b.C0369k;

/* compiled from: EventsFilesManager */
public abstract class C0409b<T> {
    public static final int MAX_BYTE_SIZE_PER_FILE = 8000;
    public static final int MAX_FILES_IN_BATCH = 1;
    public static final int MAX_FILES_TO_KEEP = 100;
    public static final String ROLL_OVER_FILE_NAME_SEPARATOR = "_";
    protected final Context context;
    protected final C0369k currentTimeProvider;
    private final int defaultMaxFilesToKeep;
    protected final C0410c eventStorage;
    protected volatile long lastRollOverTime;
    protected final List<C0411d> rollOverListeners = new CopyOnWriteArrayList();
    protected final C0406a<T> transform;

    /* compiled from: EventsFilesManager */
    class C04071 implements Comparator<C0408a> {
        final /* synthetic */ C0409b f238a;

        C04071(C0409b c0409b) {
            this.f238a = c0409b;
        }

        public /* synthetic */ int compare(Object obj, Object obj2) {
            return m249a((C0408a) obj, (C0408a) obj2);
        }

        public int m249a(C0408a c0408a, C0408a c0408a2) {
            return (int) (c0408a.f240b - c0408a2.f240b);
        }
    }

    /* compiled from: EventsFilesManager */
    static class C0408a {
        final File f239a;
        final long f240b;

        public C0408a(File file, long j) {
            this.f239a = file;
            this.f240b = j;
        }
    }

    protected abstract String generateUniqueRollOverFileName();

    protected int getMaxByteSizePerFile() {
        return MAX_BYTE_SIZE_PER_FILE;
    }

    public C0409b(Context context, C0406a<T> c0406a, C0369k c0369k, C0410c c0410c, int i) throws IOException {
        this.context = context.getApplicationContext();
        this.transform = c0406a;
        this.eventStorage = c0410c;
        this.currentTimeProvider = c0369k;
        this.lastRollOverTime = this.currentTimeProvider.mo1213a();
        this.defaultMaxFilesToKeep = i;
    }

    public void writeEvent(T t) throws IOException {
        byte[] toBytes = this.transform.toBytes(t);
        rollFileOverIfNeeded(toBytes.length);
        this.eventStorage.mo1226a(toBytes);
    }

    public void registerRollOverListener(C0411d c0411d) {
        if (c0411d != null) {
            this.rollOverListeners.add(c0411d);
        }
    }

    public boolean rollFileOver() throws IOException {
        String str;
        boolean z = true;
        if (this.eventStorage.mo1228b()) {
            str = null;
            z = false;
        } else {
            str = generateUniqueRollOverFileName();
            this.eventStorage.mo1224a(str);
            C0367i.m123a(this.context, 4, "Fabric", String.format(Locale.US, "generated new file %s", new Object[]{str}));
            this.lastRollOverTime = this.currentTimeProvider.mo1213a();
        }
        triggerRollOverOnListeners(str);
        return z;
    }

    private void rollFileOverIfNeeded(int i) throws IOException {
        if (!this.eventStorage.mo1227a(i, getMaxByteSizePerFile())) {
            C0367i.m123a(this.context, 4, "Fabric", String.format(Locale.US, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", new Object[]{Integer.valueOf(this.eventStorage.mo1222a()), Integer.valueOf(i), Integer.valueOf(getMaxByteSizePerFile())}));
            rollFileOver();
        }
    }

    protected int getMaxFilesToKeep() {
        return this.defaultMaxFilesToKeep;
    }

    public long getLastRollOverTime() {
        return this.lastRollOverTime;
    }

    private void triggerRollOverOnListeners(String str) {
        for (C0411d onRollOver : this.rollOverListeners) {
            try {
                onRollOver.onRollOver(str);
            } catch (Throwable e) {
                C0367i.m125a(this.context, "One of the roll over listeners threw an exception", e);
            }
        }
    }

    public List<File> getBatchOfFilesToSend() {
        return this.eventStorage.mo1223a(1);
    }

    public void deleteSentFiles(List<File> list) {
        this.eventStorage.mo1225a((List) list);
    }

    public void deleteAllEventsFiles() {
        this.eventStorage.mo1225a(this.eventStorage.mo1229c());
        this.eventStorage.mo1230d();
    }

    public void deleteOldestInRollOverIfOverMax() {
        List<File> c = this.eventStorage.mo1229c();
        int maxFilesToKeep = getMaxFilesToKeep();
        if (c.size() > maxFilesToKeep) {
            int size = c.size() - maxFilesToKeep;
            C0367i.m124a(this.context, String.format(Locale.US, "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files", new Object[]{Integer.valueOf(c.size()), Integer.valueOf(maxFilesToKeep), Integer.valueOf(size)}));
            TreeSet treeSet = new TreeSet(new C04071(this));
            for (File file : c) {
                treeSet.add(new C0408a(file, parseCreationTimestampFromFileName(file.getName())));
            }
            List c2 = new ArrayList();
            Iterator it = treeSet.iterator();
            while (it.hasNext()) {
                c2.add(((C0408a) it.next()).f239a);
                if (c2.size() == size) {
                    break;
                }
            }
            this.eventStorage.mo1225a(c2);
        }
    }

    public long parseCreationTimestampFromFileName(java.lang.String r5) {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r4 = this;
        r0 = "_";
        r5 = r5.split(r0);
        r0 = 0;
        r2 = 3;
        r3 = r5.length;
        if (r3 == r2) goto L_0x000d;
    L_0x000c:
        return r0;
    L_0x000d:
        r2 = 2;
        r5 = r5[r2];	 Catch:{ NumberFormatException -> 0x0019 }
        r5 = java.lang.Long.valueOf(r5);	 Catch:{ NumberFormatException -> 0x0019 }
        r2 = r5.longValue();	 Catch:{ NumberFormatException -> 0x0019 }
        return r2;
    L_0x0019:
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.a.d.b.parseCreationTimestampFromFileName(java.lang.String):long");
    }
}
