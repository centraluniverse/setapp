package p008b.p009a.p010a.p011a.p012a.p017d;

import android.content.Context;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

/* compiled from: GZIPQueueFileEventStorage */
public class C2354g extends C1815h {
    public C2354g(Context context, File file, String str, String str2) throws IOException {
        super(context, file, str, str2);
    }

    public OutputStream mo2440a(File file) throws IOException {
        return new GZIPOutputStream(new FileOutputStream(file));
    }
}
