package p008b.p009a.p010a.p011a.p012a.p017d;

import java.io.File;
import java.io.IOException;
import java.util.List;

/* compiled from: EventsStorage */
public interface C0410c {
    int mo1222a();

    List<File> mo1223a(int i);

    void mo1224a(String str) throws IOException;

    void mo1225a(List<File> list);

    void mo1226a(byte[] bArr) throws IOException;

    boolean mo1227a(int i, int i2);

    boolean mo1228b();

    List<File> mo1229c();

    void mo1230d();
}
