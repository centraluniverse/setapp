package p008b.p009a.p010a.p011a.p012a.p017d;

import java.io.IOException;

/* compiled from: FileRollOverManager */
public interface C0412e {
    void cancelTimeBasedFileRollOver();

    boolean rollFileOver() throws IOException;
}
