package p008b.p009a.p010a.p011a.p012a.p017d;

import java.io.IOException;

/* compiled from: EventTransform */
public interface C0406a<T> {
    byte[] toBytes(T t) throws IOException;
}
