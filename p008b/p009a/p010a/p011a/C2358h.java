package p008b.p009a.p010a.p011a;

import p008b.p009a.p010a.p011a.p012a.p014b.C0382t;
import p008b.p009a.p010a.p011a.p012a.p015c.C0399e;
import p008b.p009a.p010a.p011a.p012a.p015c.C0405m;
import p008b.p009a.p010a.p011a.p012a.p015c.C1812f;

/* compiled from: InitializationTask */
class C2358h<Result> extends C1812f<Void, Void, Result> {
    final C0458i<Result> f5830a;

    public C2358h(C0458i<Result> c0458i) {
        this.f5830a = c0458i;
    }

    protected void mo2444a() {
        super.mo2444a();
        C0382t a = m7005a("onPreExecute");
        try {
            boolean onPreExecute = this.f5830a.onPreExecute();
            a.m211b();
            if (onPreExecute) {
                return;
            }
        } catch (C0405m e) {
            throw e;
        } catch (Throwable e2) {
            C0452c.m367h().mo1257e("Fabric", "Failure onPreExecute()", e2);
            a.m211b();
        } catch (Throwable th) {
            a.m211b();
            m227a(true);
        }
        m227a(true);
    }

    protected Result m7007a(Void... voidArr) {
        voidArr = m7005a("doInBackground");
        Result doInBackground = !m232d() ? this.f5830a.doInBackground() : null;
        voidArr.m211b();
        return doInBackground;
    }

    protected void mo2445a(Result result) {
        this.f5830a.onPostExecute(result);
        this.f5830a.initializationCallback.mo1262a((Object) result);
    }

    protected void mo2446b(Result result) {
        this.f5830a.onCancelled(result);
        result = new StringBuilder();
        result.append(this.f5830a.getIdentifier());
        result.append(" Initialization was cancelled");
        this.f5830a.initializationCallback.mo1261a(new C0457g(result.toString()));
    }

    public C0399e getPriority() {
        return C0399e.HIGH;
    }

    private C0382t m7005a(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.f5830a.getIdentifier());
        stringBuilder.append(".");
        stringBuilder.append(str);
        C0382t c0382t = new C0382t(stringBuilder.toString(), "KitInitialization");
        c0382t.m210a();
        return c0382t;
    }
}
