package p008b.p009a.p010a.p011a;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import p008b.p009a.p010a.p011a.p012a.p014b.C0363g;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;
import p008b.p009a.p010a.p011a.p012a.p014b.C0370l;
import p008b.p009a.p010a.p011a.p012a.p018e.C0423e;
import p008b.p009a.p010a.p011a.p012a.p018e.C1816b;
import p008b.p009a.p010a.p011a.p012a.p020g.C0432d;
import p008b.p009a.p010a.p011a.p012a.p020g.C0433e;
import p008b.p009a.p010a.p011a.p012a.p020g.C0437n;
import p008b.p009a.p010a.p011a.p012a.p020g.C0442q;
import p008b.p009a.p010a.p011a.p012a.p020g.C0445t;
import p008b.p009a.p010a.p011a.p012a.p020g.C2356h;
import p008b.p009a.p010a.p011a.p012a.p020g.C2357y;

/* compiled from: Onboarding */
class C1830m extends C0458i<Boolean> {
    private final C0423e f4520a = new C1816b();
    private PackageManager f4521b;
    private String f4522c;
    private PackageInfo f4523d;
    private String f4524e;
    private String f4525f;
    private String f4526g;
    private String f4527h;
    private String f4528i;
    private final Future<Map<String, C0460k>> f4529j;
    private final Collection<C0458i> f4530k;

    public String getIdentifier() {
        return "io.fabric.sdk.android:fabric";
    }

    public String getVersion() {
        return "1.3.16.dev";
    }

    protected /* synthetic */ Object doInBackground() {
        return m5036a();
    }

    public C1830m(Future<Map<String, C0460k>> future, Collection<C0458i> collection) {
        this.f4529j = future;
        this.f4530k = collection;
    }

    protected boolean onPreExecute() {
        try {
            this.f4526g = getIdManager().m175j();
            this.f4521b = getContext().getPackageManager();
            this.f4522c = getContext().getPackageName();
            this.f4523d = this.f4521b.getPackageInfo(this.f4522c, 0);
            this.f4524e = Integer.toString(this.f4523d.versionCode);
            this.f4525f = this.f4523d.versionName == null ? "0.0" : this.f4523d.versionName;
            this.f4527h = this.f4521b.getApplicationLabel(getContext().getApplicationInfo()).toString();
            this.f4528i = Integer.toString(getContext().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (Throwable e) {
            C0452c.m367h().mo1257e("Fabric", "Failed init", e);
            return false;
        }
    }

    protected Boolean m5036a() {
        boolean a;
        String k = C0367i.m148k(getContext());
        C0445t c = m5034c();
        if (c != null) {
            try {
                Map map;
                if (this.f4529j != null) {
                    map = (Map) this.f4529j.get();
                } else {
                    map = new HashMap();
                }
                a = m5032a(k, c.f333a, m5037a(map, this.f4530k).values());
            } catch (Throwable e) {
                C0452c.m367h().mo1257e("Fabric", "Error performing auto configuration.", e);
            }
            return Boolean.valueOf(a);
        }
        a = false;
        return Boolean.valueOf(a);
    }

    private C0445t m5034c() {
        try {
            C0442q.m342a().m344a(this, this.idManager, this.f4520a, this.f4524e, this.f4525f, m5038b()).m346c();
            return C0442q.m342a().m345b();
        } catch (Throwable e) {
            C0452c.m367h().mo1257e("Fabric", "Error dealing with settings", e);
            return null;
        }
    }

    Map<String, C0460k> m5037a(Map<String, C0460k> map, Collection<C0458i> collection) {
        for (C0458i c0458i : collection) {
            if (!map.containsKey(c0458i.getIdentifier())) {
                map.put(c0458i.getIdentifier(), new C0460k(c0458i.getIdentifier(), c0458i.getVersion(), "binary"));
            }
        }
        return map;
    }

    private boolean m5032a(String str, C0433e c0433e, Collection<C0460k> collection) {
        if ("new".equals(c0433e.f295b)) {
            if (m5033b(str, c0433e, collection) != null) {
                return C0442q.m342a().m347d();
            }
            C0452c.m367h().mo1257e("Fabric", "Failed to create app with Crashlytics service.", null);
            return null;
        } else if ("configured".equals(c0433e.f295b)) {
            return C0442q.m342a().m347d();
        } else {
            if (c0433e.f298e) {
                C0452c.m367h().mo1249a("Fabric", "Server says an update is required - forcing a full App update.");
                m5035c(str, c0433e, collection);
            }
            return true;
        }
    }

    private boolean m5033b(String str, C0433e c0433e, Collection<C0460k> collection) {
        return new C2356h(this, m5038b(), c0433e.f296c, this.f4520a).mo2442a(m5030a(C0437n.m340a(getContext(), str), (Collection) collection));
    }

    private boolean m5035c(String str, C0433e c0433e, Collection<C0460k> collection) {
        return m5031a(c0433e, C0437n.m340a(getContext(), str), (Collection) collection);
    }

    private boolean m5031a(C0433e c0433e, C0437n c0437n, Collection<C0460k> collection) {
        return new C2357y(this, m5038b(), c0433e.f296c, this.f4520a).mo2442a(m5030a(c0437n, (Collection) collection));
    }

    private C0432d m5030a(C0437n c0437n, Collection<C0460k> collection) {
        return new C0432d(new C0363g().m102a(getContext()), getIdManager().m168c(), this.f4525f, this.f4524e, C0367i.m122a(C0367i.m150m(r1)), this.f4527h, C0370l.m155a(this.f4526g).m156a(), this.f4528i, "0", c0437n, collection);
    }

    String m5038b() {
        return C0367i.m135b(getContext(), "com.crashlytics.ApiEndpoint");
    }
}
