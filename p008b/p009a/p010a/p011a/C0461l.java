package p008b.p009a.p010a.p011a;

/* compiled from: Logger */
public interface C0461l {
    void mo1247a(int i, String str, String str2);

    void mo1248a(int i, String str, String str2, boolean z);

    void mo1249a(String str, String str2);

    void mo1250a(String str, String str2, Throwable th);

    boolean mo1251a(String str, int i);

    void mo1252b(String str, String str2);

    void mo1253c(String str, String str2);

    void mo1254d(String str, String str2);

    void mo1255d(String str, String str2, Throwable th);

    void mo1256e(String str, String str2);

    void mo1257e(String str, String str2, Throwable th);
}
