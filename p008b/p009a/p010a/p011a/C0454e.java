package p008b.p009a.p010a.p011a;

import android.text.TextUtils;
import java.io.Closeable;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import p008b.p009a.p010a.p011a.p012a.p014b.C0367i;

/* compiled from: FabricKitsFinder */
class C0454e implements Callable<Map<String, C0460k>> {
    final String f382a;

    public /* synthetic */ Object call() throws Exception {
        return m383a();
    }

    C0454e(String str) {
        this.f382a = str;
    }

    public java.util.Map<java.lang.String, p008b.p009a.p010a.p011a.C0460k> m383a() throws java.lang.Exception {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r13 = this;
        r0 = new java.util.HashMap;
        r0.<init>();
        r1 = android.os.SystemClock.elapsedRealtime();
        r3 = r13.m384b();
        r4 = r3.entries();
        r5 = 0;
        r6 = r5;
    L_0x0013:
        r7 = r4.hasMoreElements();
        if (r7 == 0) goto L_0x006a;
    L_0x0019:
        r6 = r6 + 1;
        r7 = r4.nextElement();
        r7 = (java.util.zip.ZipEntry) r7;
        r8 = r7.getName();
        r9 = "fabric/";
        r8 = r8.startsWith(r9);
        if (r8 == 0) goto L_0x0013;
    L_0x002d:
        r8 = r7.getName();
        r8 = r8.length();
        r9 = "fabric/";
        r9 = r9.length();
        if (r8 <= r9) goto L_0x0013;
    L_0x003d:
        r7 = r13.m382a(r7, r3);
        if (r7 == 0) goto L_0x0013;
    L_0x0043:
        r8 = r7.m387a();
        r0.put(r8, r7);
        r8 = p008b.p009a.p010a.p011a.C0452c.m367h();
        r9 = "Fabric";
        r10 = "Found kit:[%s] version:[%s]";
        r11 = 2;
        r11 = new java.lang.Object[r11];
        r12 = r7.m387a();
        r11[r5] = r12;
        r7 = r7.m388b();
        r12 = 1;
        r11[r12] = r7;
        r7 = java.lang.String.format(r10, r11);
        r8.mo1252b(r9, r7);
        goto L_0x0013;
    L_0x006a:
        if (r3 == 0) goto L_0x006f;
    L_0x006c:
        r3.close();	 Catch:{ IOException -> 0x006f }
    L_0x006f:
        r3 = p008b.p009a.p010a.p011a.C0452c.m367h();
        r4 = "Fabric";
        r5 = new java.lang.StringBuilder;
        r5.<init>();
        r7 = "finish scanning in ";
        r5.append(r7);
        r7 = android.os.SystemClock.elapsedRealtime();
        r9 = r7 - r1;
        r5.append(r9);
        r1 = " reading:";
        r5.append(r1);
        r5.append(r6);
        r1 = r5.toString();
        r3.mo1252b(r4, r1);
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.a.e.a():java.util.Map<java.lang.String, b.a.a.a.k>");
    }

    private C0460k m382a(ZipEntry zipEntry, ZipFile zipFile) {
        Closeable inputStream;
        Throwable e;
        StringBuilder stringBuilder;
        try {
            inputStream = zipFile.getInputStream(zipEntry);
            try {
                Properties properties = new Properties();
                properties.load(inputStream);
                Object property = properties.getProperty("fabric-identifier");
                Object property2 = properties.getProperty("fabric-version");
                String property3 = properties.getProperty("fabric-build-type");
                if (!TextUtils.isEmpty(property)) {
                    if (!TextUtils.isEmpty(property2)) {
                        C0460k c0460k = new C0460k(property, property2, property3);
                        C0367i.m126a(inputStream);
                        return c0460k;
                    }
                }
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append("Invalid format of fabric file,");
                stringBuilder2.append(zipEntry.getName());
                throw new IllegalStateException(stringBuilder2.toString());
            } catch (IOException e2) {
                e = e2;
                try {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("Error when parsing fabric properties ");
                    stringBuilder.append(zipEntry.getName());
                    C0452c.m367h().mo1257e("Fabric", stringBuilder.toString(), e);
                    C0367i.m126a(inputStream);
                    return null;
                } catch (Throwable th) {
                    zipEntry = th;
                    C0367i.m126a(inputStream);
                    throw zipEntry;
                }
            }
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
            stringBuilder = new StringBuilder();
            stringBuilder.append("Error when parsing fabric properties ");
            stringBuilder.append(zipEntry.getName());
            C0452c.m367h().mo1257e("Fabric", stringBuilder.toString(), e);
            C0367i.m126a(inputStream);
            return null;
        } catch (Throwable th2) {
            zipEntry = th2;
            inputStream = null;
            C0367i.m126a(inputStream);
            throw zipEntry;
        }
    }

    protected ZipFile m384b() throws IOException {
        return new ZipFile(this.f382a);
    }
}
