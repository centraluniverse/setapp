package p008b.p009a.p010a.p011a;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import p008b.p009a.p010a.p011a.C0450a.C0353b;
import p008b.p009a.p010a.p011a.p012a.p014b.C0375o;
import p008b.p009a.p010a.p011a.p012a.p015c.C0398d;
import p008b.p009a.p010a.p011a.p012a.p015c.C0403k;
import p008b.p009a.p010a.p011a.p012a.p015c.C0405m;

/* compiled from: Fabric */
public class C0452c {
    static volatile C0452c f366a;
    static final C0461l f367b = new C1826b();
    final C0461l f368c;
    final boolean f369d;
    private final Context f370e;
    private final Map<Class<? extends C0458i>, C0458i> f371f;
    private final ExecutorService f372g;
    private final Handler f373h;
    private final C0456f<C0452c> f374i;
    private final C0456f<?> f375j;
    private final C0375o f376k;
    private C0450a f377l;
    private WeakReference<Activity> f378m;
    private AtomicBoolean f379n = new AtomicBoolean(null);

    /* compiled from: Fabric */
    public static class C0451a {
        private final Context f357a;
        private C0458i[] f358b;
        private C0403k f359c;
        private Handler f360d;
        private C0461l f361e;
        private boolean f362f;
        private String f363g;
        private String f364h;
        private C0456f<C0452c> f365i;

        public C0451a(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.f357a = context;
        }

        public C0451a m355a(C0458i... c0458iArr) {
            if (this.f358b != null) {
                throw new IllegalStateException("Kits already set.");
            }
            this.f358b = c0458iArr;
            return this;
        }

        public C0452c m356a() {
            HashMap hashMap;
            if (this.f359c == null) {
                this.f359c = C0403k.m246a();
            }
            if (this.f360d == null) {
                this.f360d = new Handler(Looper.getMainLooper());
            }
            if (this.f361e == null) {
                if (this.f362f) {
                    this.f361e = new C1826b(3);
                } else {
                    this.f361e = new C1826b();
                }
            }
            if (this.f364h == null) {
                this.f364h = this.f357a.getPackageName();
            }
            if (this.f365i == null) {
                this.f365i = C0456f.f383d;
            }
            if (this.f358b == null) {
                hashMap = new HashMap();
            } else {
                hashMap = C0452c.m364b(Arrays.asList(this.f358b));
            }
            HashMap hashMap2 = hashMap;
            return new C0452c(this.f357a, hashMap2, this.f359c, this.f360d, this.f361e, this.f362f, this.f365i, new C0375o(this.f357a, this.f364h, this.f363g, hashMap2.values()));
        }
    }

    /* compiled from: Fabric */
    class C18271 extends C0353b {
        final /* synthetic */ C0452c f4516a;

        C18271(C0452c c0452c) {
            this.f4516a = c0452c;
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            this.f4516a.m371a(activity);
        }

        public void onActivityStarted(Activity activity) {
            this.f4516a.m371a(activity);
        }

        public void onActivityResumed(Activity activity) {
            this.f4516a.m371a(activity);
        }
    }

    public String m377c() {
        return "1.3.16.dev";
    }

    public String m378d() {
        return "io.fabric.sdk.android:fabric";
    }

    static C0452c m357a() {
        if (f366a != null) {
            return f366a;
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    C0452c(Context context, Map<Class<? extends C0458i>, C0458i> map, C0403k c0403k, Handler handler, C0461l c0461l, boolean z, C0456f c0456f, C0375o c0375o) {
        this.f370e = context.getApplicationContext();
        this.f371f = map;
        this.f372g = c0403k;
        this.f373h = handler;
        this.f368c = c0461l;
        this.f369d = z;
        this.f374i = c0456f;
        this.f375j = m372a(map.size());
        this.f376k = c0375o;
        m371a(m365c(context));
    }

    public static C0452c m358a(Context context, C0458i... c0458iArr) {
        if (f366a == null) {
            synchronized (C0452c.class) {
                if (f366a == null) {
                    C0452c.m366c(new C0451a(context).m355a(c0458iArr).m356a());
                }
            }
        }
        return f366a;
    }

    private static void m366c(C0452c c0452c) {
        f366a = c0452c;
        c0452c.m370k();
    }

    public C0452c m371a(Activity activity) {
        this.f378m = new WeakReference(activity);
        return this;
    }

    public Activity m375b() {
        return this.f378m != null ? (Activity) this.f378m.get() : null;
    }

    private void m370k() {
        this.f377l = new C0450a(this.f370e);
        this.f377l.m354a(new C18271(this));
        m373a(this.f370e);
    }

    void m373a(Context context) {
        Future b = m376b(context);
        Collection g = m381g();
        C1830m c1830m = new C1830m(b, g);
        List<C0458i> arrayList = new ArrayList(g);
        Collections.sort(arrayList);
        c1830m.injectParameters(context, this, C0456f.f383d, this.f376k);
        for (C0458i injectParameters : arrayList) {
            injectParameters.injectParameters(context, this, this.f375j, this.f376k);
        }
        c1830m.initialize();
        if (C0452c.m367h().mo1251a("Fabric", 3) != null) {
            context = new StringBuilder("Initializing ");
            context.append(m378d());
            context.append(" [Version: ");
            context.append(m377c());
            context.append("], with the following kits:\n");
        } else {
            context = null;
        }
        for (C0458i c0458i : arrayList) {
            c0458i.initializationTask.m4949a(c1830m.initializationTask);
            m374a(this.f371f, c0458i);
            c0458i.initialize();
            if (context != null) {
                context.append(c0458i.getIdentifier());
                context.append(" [Version: ");
                context.append(c0458i.getVersion());
                context.append("]\n");
            }
        }
        if (context != null) {
            C0452c.m367h().mo1249a("Fabric", context.toString());
        }
    }

    void m374a(Map<Class<? extends C0458i>, C0458i> map, C0458i c0458i) {
        C0398d c0398d = c0458i.dependsOnAnnotation;
        if (c0398d != null) {
            for (Class cls : c0398d.m243a()) {
                if (cls.isInterface()) {
                    for (C0458i c0458i2 : map.values()) {
                        if (cls.isAssignableFrom(c0458i2.getClass())) {
                            c0458i.initializationTask.m4949a(c0458i2.initializationTask);
                        }
                    }
                } else if (((C0458i) map.get(cls)) == null) {
                    throw new C0405m("Referenced Kit was null, does the kit exist?");
                } else {
                    c0458i.initializationTask.m4949a(((C0458i) map.get(cls)).initializationTask);
                }
            }
        }
    }

    private Activity m365c(Context context) {
        return context instanceof Activity ? (Activity) context : null;
    }

    public C0450a m379e() {
        return this.f377l;
    }

    public ExecutorService m380f() {
        return this.f372g;
    }

    public Collection<C0458i> m381g() {
        return this.f371f.values();
    }

    public static <T extends C0458i> T m359a(Class<T> cls) {
        return (C0458i) C0452c.m357a().f371f.get(cls);
    }

    public static C0461l m367h() {
        if (f366a == null) {
            return f367b;
        }
        return f366a.f368c;
    }

    public static boolean m368i() {
        if (f366a == null) {
            return false;
        }
        return f366a.f369d;
    }

    public static boolean m369j() {
        return f366a != null && f366a.f379n.get();
    }

    private static Map<Class<? extends C0458i>, C0458i> m364b(Collection<? extends C0458i> collection) {
        Map hashMap = new HashMap(collection.size());
        C0452c.m362a(hashMap, (Collection) collection);
        return hashMap;
    }

    private static void m362a(Map<Class<? extends C0458i>, C0458i> map, Collection<? extends C0458i> collection) {
        for (C0458i c0458i : collection) {
            map.put(c0458i.getClass(), c0458i);
            if (c0458i instanceof C0459j) {
                C0452c.m362a((Map) map, ((C0459j) c0458i).getKits());
            }
        }
    }

    C0456f<?> m372a(final int i) {
        return new C0456f(this) {
            final CountDownLatch f4517a = new CountDownLatch(i);
            final /* synthetic */ C0452c f4519c;

            public void mo1262a(Object obj) {
                this.f4517a.countDown();
                if (this.f4517a.getCount() == 0) {
                    this.f4519c.f379n.set(true);
                    this.f4519c.f374i.mo1262a(this.f4519c);
                }
            }

            public void mo1261a(Exception exception) {
                this.f4519c.f374i.mo1261a(exception);
            }
        };
    }

    Future<Map<String, C0460k>> m376b(Context context) {
        return m380f().submit(new C0454e(context.getPackageCodePath()));
    }
}
