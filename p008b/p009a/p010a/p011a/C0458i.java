package p008b.p009a.p010a.p011a;

import android.content.Context;
import java.io.File;
import java.util.Collection;
import p008b.p009a.p010a.p011a.p012a.p014b.C0375o;
import p008b.p009a.p010a.p011a.p012a.p015c.C0398d;
import p008b.p009a.p010a.p011a.p012a.p015c.C0404l;

/* compiled from: Kit */
public abstract class C0458i<Result> implements Comparable<C0458i> {
    Context context;
    final C0398d dependsOnAnnotation = ((C0398d) getClass().getAnnotation(C0398d.class));
    C0452c fabric;
    C0375o idManager;
    C0456f<Result> initializationCallback;
    C2358h<Result> initializationTask = new C2358h(this);

    protected abstract Result doInBackground();

    public abstract String getIdentifier();

    public abstract String getVersion();

    protected void onCancelled(Result result) {
    }

    protected void onPostExecute(Result result) {
    }

    protected boolean onPreExecute() {
        return true;
    }

    void injectParameters(Context context, C0452c c0452c, C0456f<Result> c0456f, C0375o c0375o) {
        this.fabric = c0452c;
        this.context = new C0453d(context, getIdentifier(), getPath());
        this.initializationCallback = c0456f;
        this.idManager = c0375o;
    }

    final void initialize() {
        this.initializationTask.m4950a(this.fabric.m380f(), (Void) null);
    }

    protected C0375o getIdManager() {
        return this.idManager;
    }

    public Context getContext() {
        return this.context;
    }

    public C0452c getFabric() {
        return this.fabric;
    }

    public String getPath() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(".Fabric");
        stringBuilder.append(File.separator);
        stringBuilder.append(getIdentifier());
        return stringBuilder.toString();
    }

    public int compareTo(C0458i c0458i) {
        if (containsAnnotatedDependency(c0458i)) {
            return 1;
        }
        if (c0458i.containsAnnotatedDependency(this)) {
            return -1;
        }
        if (hasAnnotatedDependency() && !c0458i.hasAnnotatedDependency()) {
            return 1;
        }
        if (hasAnnotatedDependency() || c0458i.hasAnnotatedDependency() == null) {
            return null;
        }
        return -1;
    }

    boolean containsAnnotatedDependency(C0458i c0458i) {
        if (hasAnnotatedDependency()) {
            for (Class isAssignableFrom : this.dependsOnAnnotation.m243a()) {
                if (isAssignableFrom.isAssignableFrom(c0458i.getClass())) {
                    return true;
                }
            }
        }
        return false;
    }

    boolean hasAnnotatedDependency() {
        return this.dependsOnAnnotation != null;
    }

    protected Collection<C0404l> getDependencies() {
        return this.initializationTask.getDependencies();
    }
}
