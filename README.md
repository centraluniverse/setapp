# Android SetAPP infestigation

A few functions seem to be using the standard SEProtocol over wifi with the device
set as hotspot. It appears however other then a firmware update most of the talking
happens via the cloud, rather then with the device directly.

## Firmware

Firmware files to update the inverters are supplied under assets in the form of spff (Solaredge Platform Firmware File?) files.
There are also 'activation' files for each part/serial, which seem to be very small files (configuration files?).

## Packet data
The SE Protocol is then wrapped into protobuf packets and what not. The conversion happens in com/solaredge/apps/activator/proto/general_types/XPacket.java where the known
src, dst, id, opcode, len and payload is sent/received, so that is a good starting point to dive deeper.

## Internal type names
The android app introduces a few new product and controller types/family names. See com/solaredge/apps/activator/proto/general_types/ProductType.java, com/solaredge/apps/activator/proto/general_types/ControllerType.java and com/solaredge/apps/activator/proto/general_types/ProductFamily.java
