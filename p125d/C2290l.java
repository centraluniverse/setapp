package p125d;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.Inflater;

/* compiled from: InflaterSource */
public final class C2290l implements C1630t {
    private final C2284e f5763a;
    private final Inflater f5764b;
    private int f5765c;
    private boolean f5766d;

    public C2290l(C1630t c1630t, Inflater inflater) {
        this(C1625m.m4841a(c1630t), inflater);
    }

    C2290l(C2284e c2284e, Inflater inflater) {
        if (c2284e == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f5763a = c2284e;
            this.f5764b = inflater;
        }
    }

    public long read(C2453c c2453c, long j) throws IOException {
        if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("byteCount < 0: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        } else if (this.f5766d) {
            throw new IllegalStateException("closed");
        } else if (j == 0) {
            return 0;
        } else {
            do {
                j = m6958a();
                try {
                    C1627p f = c2453c.m7742f(1);
                    int inflate = this.f5764b.inflate(f.f4406a, f.f4408c, 8192 - f.f4408c);
                    if (inflate > 0) {
                        f.f4408c += inflate;
                        long j2 = (long) inflate;
                        c2453c.f6388b += j2;
                        return j2;
                    }
                    if (!this.f5764b.finished()) {
                        if (this.f5764b.needsDictionary()) {
                        }
                    }
                    m6957b();
                    if (f.f4407b == f.f4408c) {
                        c2453c.f6387a = f.m4853a();
                        C1628q.m4859a(f);
                    }
                    return -1;
                } catch (C2453c c2453c2) {
                    throw new IOException(c2453c2);
                }
            } while (j == null);
            throw new EOFException("source exhausted prematurely");
        }
    }

    public boolean m6958a() throws IOException {
        if (!this.f5764b.needsInput()) {
            return false;
        }
        m6957b();
        if (this.f5764b.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.f5763a.mo2525f()) {
            return true;
        } else {
            C1627p c1627p = this.f5763a.mo2516b().f6387a;
            this.f5765c = c1627p.f4408c - c1627p.f4407b;
            this.f5764b.setInput(c1627p.f4406a, c1627p.f4407b, this.f5765c);
            return false;
        }
    }

    private void m6957b() throws IOException {
        if (this.f5765c != 0) {
            int remaining = this.f5765c - this.f5764b.getRemaining();
            this.f5765c -= remaining;
            this.f5763a.mo2531i((long) remaining);
        }
    }

    public C1631u timeout() {
        return this.f5763a.timeout();
    }

    public void close() throws IOException {
        if (!this.f5766d) {
            this.f5764b.end();
            this.f5766d = true;
            this.f5763a.close();
        }
    }
}
