package p125d;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/* compiled from: ForwardingTimeout */
public class C2288j extends C1631u {
    private C1631u f5757a;

    public C2288j(C1631u c1631u) {
        if (c1631u == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f5757a = c1631u;
    }

    public final C1631u m6947a() {
        return this.f5757a;
    }

    public final C2288j m6946a(C1631u c1631u) {
        if (c1631u == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f5757a = c1631u;
        return this;
    }

    public C1631u mo1893a(long j, TimeUnit timeUnit) {
        return this.f5757a.mo1893a(j, timeUnit);
    }

    public long i_() {
        return this.f5757a.i_();
    }

    public boolean j_() {
        return this.f5757a.j_();
    }

    public long mo1894d() {
        return this.f5757a.mo1894d();
    }

    public C1631u mo1892a(long j) {
        return this.f5757a.mo1892a(j);
    }

    public C1631u k_() {
        return this.f5757a.k_();
    }

    public C1631u mo1895f() {
        return this.f5757a.mo1895f();
    }

    public void mo1896g() throws IOException {
        this.f5757a.mo1896g();
    }
}
