package p125d;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;

/* compiled from: Okio */
public final class C1625m {
    static final Logger f4404a = Logger.getLogger(C1625m.class.getName());

    /* compiled from: Okio */
    class C22911 implements C1629s {
        final /* synthetic */ C1631u f5767a;
        final /* synthetic */ OutputStream f5768b;

        C22911(C1631u c1631u, OutputStream outputStream) {
            this.f5767a = c1631u;
            this.f5768b = outputStream;
        }

        public void mo1284a(C2453c c2453c, long j) throws IOException {
            C1632v.m4869a(c2453c.f6388b, 0, j);
            while (j > 0) {
                this.f5767a.mo1896g();
                C1627p c1627p = c2453c.f6387a;
                int min = (int) Math.min(j, (long) (c1627p.f4408c - c1627p.f4407b));
                this.f5768b.write(c1627p.f4406a, c1627p.f4407b, min);
                c1627p.f4407b += min;
                long j2 = (long) min;
                long j3 = j - j2;
                c2453c.f6388b -= j2;
                if (c1627p.f4407b == c1627p.f4408c) {
                    c2453c.f6387a = c1627p.m4853a();
                    C1628q.m4859a(c1627p);
                }
                j = j3;
            }
        }

        public void flush() throws IOException {
            this.f5768b.flush();
        }

        public void close() throws IOException {
            this.f5768b.close();
        }

        public C1631u timeout() {
            return this.f5767a;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("sink(");
            stringBuilder.append(this.f5768b);
            stringBuilder.append(")");
            return stringBuilder.toString();
        }
    }

    /* compiled from: Okio */
    class C22922 implements C1630t {
        final /* synthetic */ C1631u f5769a;
        final /* synthetic */ InputStream f5770b;

        C22922(C1631u c1631u, InputStream inputStream) {
            this.f5769a = c1631u;
            this.f5770b = inputStream;
        }

        public long read(C2453c c2453c, long j) throws IOException {
            if (j < 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("byteCount < 0: ");
                stringBuilder.append(j);
                throw new IllegalArgumentException(stringBuilder.toString());
            } else if (j == 0) {
                return 0;
            } else {
                try {
                    this.f5769a.mo1896g();
                    C1627p f = c2453c.m7742f(1);
                    j = this.f5770b.read(f.f4406a, f.f4408c, (int) Math.min(j, (long) (8192 - f.f4408c)));
                    if (j == -1) {
                        return -1;
                    }
                    f.f4408c += j;
                    j = (long) j;
                    c2453c.f6388b += j;
                    return j;
                } catch (AssertionError e) {
                    if (C1625m.m4848a(e) != null) {
                        throw new IOException(e);
                    }
                    throw e;
                }
            }
        }

        public void close() throws IOException {
            this.f5770b.close();
        }

        public C1631u timeout() {
            return this.f5769a;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("source(");
            stringBuilder.append(this.f5770b);
            stringBuilder.append(")");
            return stringBuilder.toString();
        }
    }

    /* compiled from: Okio */
    class C24543 extends C2282a {
        final /* synthetic */ Socket f6389a;

        C24543(Socket socket) {
            this.f6389a = socket;
        }

        protected IOException mo2447a(@Nullable IOException iOException) {
            IOException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        protected void mo2448a() {
            Logger logger;
            Level level;
            StringBuilder stringBuilder;
            try {
                this.f6389a.close();
            } catch (Throwable e) {
                logger = C1625m.f4404a;
                level = Level.WARNING;
                stringBuilder = new StringBuilder();
                stringBuilder.append("Failed to close timed out socket ");
                stringBuilder.append(this.f6389a);
                logger.log(level, stringBuilder.toString(), e);
            } catch (AssertionError e2) {
                if (C1625m.m4848a(e2)) {
                    logger = C1625m.f4404a;
                    level = Level.WARNING;
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("Failed to close timed out socket ");
                    stringBuilder.append(this.f6389a);
                    logger.log(level, stringBuilder.toString(), e2);
                    return;
                }
                throw e2;
            }
        }
    }

    private C1625m() {
    }

    public static C2284e m4841a(C1630t c1630t) {
        return new C2456o(c1630t);
    }

    public static C2283d m4840a(C1629s c1629s) {
        return new C2455n(c1629s);
    }

    public static C1629s m4842a(OutputStream outputStream) {
        return C1625m.m4843a(outputStream, new C1631u());
    }

    private static C1629s m4843a(OutputStream outputStream, C1631u c1631u) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (c1631u != null) {
            return new C22911(c1631u, outputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static C1629s m4844a(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        C1631u c = C1625m.m4851c(socket);
        return c.m6896a(C1625m.m4843a(socket.getOutputStream(), c));
    }

    public static C1630t m4846a(InputStream inputStream) {
        return C1625m.m4847a(inputStream, new C1631u());
    }

    private static C1630t m4847a(InputStream inputStream, C1631u c1631u) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (c1631u != null) {
            return new C22922(c1631u, inputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static C1630t m4845a(File file) throws FileNotFoundException {
        if (file != null) {
            return C1625m.m4846a(new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    public static C1629s m4849b(File file) throws FileNotFoundException {
        if (file != null) {
            return C1625m.m4842a(new FileOutputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    public static C1629s m4852c(File file) throws FileNotFoundException {
        if (file != null) {
            return C1625m.m4842a(new FileOutputStream(file, true));
        }
        throw new IllegalArgumentException("file == null");
    }

    public static C1630t m4850b(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        C1631u c = C1625m.m4851c(socket);
        return c.m6897a(C1625m.m4847a(socket.getInputStream(), c));
    }

    private static C2282a m4851c(Socket socket) {
        return new C24543(socket);
    }

    static boolean m4848a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || assertionError.getMessage().contains("getsockname failed") == null) ? null : true;
    }
}
