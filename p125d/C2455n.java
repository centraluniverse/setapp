package p125d;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;

/* compiled from: RealBufferedSink */
final class C2455n implements C2283d {
    public final C2453c f6390a = new C2453c();
    public final C1629s f6391b;
    boolean f6392c;

    C2455n(C1629s c1629s) {
        if (c1629s == null) {
            throw new NullPointerException("sink == null");
        }
        this.f6391b = c1629s;
    }

    public C2453c mo2516b() {
        return this.f6390a;
    }

    public void mo1284a(C2453c c2453c, long j) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.mo1284a(c2453c, j);
        mo2546z();
    }

    public C2283d mo2517b(C1624f c1624f) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7712a(c1624f);
        return mo2546z();
    }

    public C2283d mo2518b(String str) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7713a(str);
        return mo2546z();
    }

    public C2283d mo2520c(byte[] bArr) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7726b(bArr);
        return mo2546z();
    }

    public C2283d mo2521c(byte[] bArr, int i, int i2) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7727b(bArr, i, i2);
        return mo2546z();
    }

    public long mo2511a(C1630t c1630t) throws IOException {
        if (c1630t == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long read = c1630t.read(this.f6390a, PlaybackStateCompat.ACTION_PLAY_FROM_URI);
            if (read == -1) {
                return j;
            }
            long j2 = j + read;
            mo2546z();
            j = j2;
        }
    }

    public C2283d mo2535k(int i) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7725b(i);
        return mo2546z();
    }

    public C2283d mo2532j(int i) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7732c(i);
        return mo2546z();
    }

    public C2283d mo2530i(int i) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7737d(i);
        return mo2546z();
    }

    public C2283d mo2527h(int i) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7739e(i);
        return mo2546z();
    }

    public C2283d mo2542p(long j) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7758k(j);
        return mo2546z();
    }

    public C2283d mo2540o(long j) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7761l(j);
        return mo2546z();
    }

    public C2283d mo2538n(long j) throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        this.f6390a.m7762m(j);
        return mo2546z();
    }

    public C2283d mo2546z() throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        long h = this.f6390a.m7748h();
        if (h > 0) {
            this.f6391b.mo1284a(this.f6390a, h);
        }
        return this;
    }

    public C2283d mo2523e() throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        long a = this.f6390a.m7705a();
        if (a > 0) {
            this.f6391b.mo1284a(this.f6390a, a);
        }
        return this;
    }

    public void flush() throws IOException {
        if (this.f6392c) {
            throw new IllegalStateException("closed");
        }
        if (this.f6390a.f6388b > 0) {
            this.f6391b.mo1284a(this.f6390a, this.f6390a.f6388b);
        }
        this.f6391b.flush();
    }

    public void close() throws IOException {
        if (!this.f6392c) {
            Throwable th = null;
            try {
                if (this.f6390a.f6388b > 0) {
                    this.f6391b.mo1284a(this.f6390a, this.f6390a.f6388b);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f6391b.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.f6392c = true;
            if (th != null) {
                C1632v.m4870a(th);
            }
        }
    }

    public C1631u timeout() {
        return this.f6391b.timeout();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("buffer(");
        stringBuilder.append(this.f6391b);
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
