package p125d;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

/* compiled from: Timeout */
public class C1631u {
    public static final C1631u f4415c = new C22941();
    private boolean f4416a;
    private long f4417b;
    private long f4418d;

    /* compiled from: Timeout */
    class C22941 extends C1631u {
        public C1631u mo1892a(long j) {
            return this;
        }

        public C1631u mo1893a(long j, TimeUnit timeUnit) {
            return this;
        }

        public void mo1896g() throws IOException {
        }

        C22941() {
        }
    }

    public C1631u mo1893a(long j, TimeUnit timeUnit) {
        if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("timeout < 0: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            this.f4418d = timeUnit.toNanos(j);
            return this;
        }
    }

    public long i_() {
        return this.f4418d;
    }

    public boolean j_() {
        return this.f4416a;
    }

    public long mo1894d() {
        if (this.f4416a) {
            return this.f4417b;
        }
        throw new IllegalStateException("No deadline");
    }

    public C1631u mo1892a(long j) {
        this.f4416a = true;
        this.f4417b = j;
        return this;
    }

    public C1631u k_() {
        this.f4418d = 0;
        return this;
    }

    public C1631u mo1895f() {
        this.f4416a = false;
        return this;
    }

    public void mo1896g() throws IOException {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f4416a && this.f4417b - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }
}
