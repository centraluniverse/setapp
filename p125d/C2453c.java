package p125d;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

/* compiled from: Buffer */
public final class C2453c implements C2283d, C2284e, Cloneable {
    private static final byte[] f6386c = new byte[]{(byte) 48, (byte) 49, (byte) 50, (byte) 51, (byte) 52, (byte) 53, (byte) 54, (byte) 55, (byte) 56, (byte) 57, (byte) 97, (byte) 98, (byte) 99, (byte) 100, (byte) 101, (byte) 102};
    @Nullable
    C1627p f6387a;
    long f6388b;

    /* compiled from: Buffer */
    class C16221 extends OutputStream {
        final /* synthetic */ C2453c f4397a;

        public void close() {
        }

        public void flush() {
        }

        C16221(C2453c c2453c) {
            this.f4397a = c2453c;
        }

        public void write(int i) {
            this.f4397a.m7725b((byte) i);
        }

        public void write(byte[] bArr, int i, int i2) {
            this.f4397a.m7727b(bArr, i, i2);
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(this.f4397a);
            stringBuilder.append(".outputStream()");
            return stringBuilder.toString();
        }
    }

    /* compiled from: Buffer */
    class C16232 extends InputStream {
        final /* synthetic */ C2453c f4398a;

        public void close() {
        }

        C16232(C2453c c2453c) {
            this.f4398a = c2453c;
        }

        public int read() {
            return this.f4398a.f6388b > 0 ? this.f4398a.mo2529i() & 255 : -1;
        }

        public int read(byte[] bArr, int i, int i2) {
            return this.f4398a.m7704a(bArr, i, i2);
        }

        public int available() {
            return (int) Math.min(this.f4398a.f6388b, 2147483647L);
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(this.f4398a);
            stringBuilder.append(".inputStream()");
            return stringBuilder.toString();
        }
    }

    public C2453c mo2516b() {
        return this;
    }

    public void close() {
    }

    public C2453c m7736d() {
        return this;
    }

    public C2283d mo2523e() {
        return this;
    }

    public void flush() {
    }

    public /* synthetic */ C2283d mo2517b(C1624f c1624f) throws IOException {
        return m7712a(c1624f);
    }

    public /* synthetic */ C2283d mo2518b(String str) throws IOException {
        return m7713a(str);
    }

    public /* synthetic */ C2283d mo2520c(byte[] bArr) throws IOException {
        return m7726b(bArr);
    }

    public /* synthetic */ C2283d mo2521c(byte[] bArr, int i, int i2) throws IOException {
        return m7727b(bArr, i, i2);
    }

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        return m7777x();
    }

    public /* synthetic */ C2283d mo2527h(int i) throws IOException {
        return m7739e(i);
    }

    public /* synthetic */ C2283d mo2530i(int i) throws IOException {
        return m7737d(i);
    }

    public /* synthetic */ C2283d mo2532j(int i) throws IOException {
        return m7732c(i);
    }

    public /* synthetic */ C2283d mo2535k(int i) throws IOException {
        return m7725b(i);
    }

    public /* synthetic */ C2283d mo2538n(long j) throws IOException {
        return m7762m(j);
    }

    public /* synthetic */ C2283d mo2540o(long j) throws IOException {
        return m7761l(j);
    }

    public /* synthetic */ C2283d mo2542p(long j) throws IOException {
        return m7758k(j);
    }

    public /* synthetic */ C2283d mo2546z() throws IOException {
        return m7736d();
    }

    public long m7705a() {
        return this.f6388b;
    }

    public OutputStream m7735c() {
        return new C16221(this);
    }

    public boolean mo2525f() {
        return this.f6388b == 0;
    }

    public void mo2513a(long j) throws EOFException {
        if (this.f6388b < j) {
            throw new EOFException();
        }
    }

    public boolean mo2519b(long j) {
        return this.f6388b >= j ? 1 : 0;
    }

    public InputStream mo2526g() {
        return new C16232(this);
    }

    public C2453c m7711a(C2453c c2453c, long j, long j2) {
        if (c2453c == null) {
            throw new IllegalArgumentException("out == null");
        }
        C1632v.m4869a(this.f6388b, j, j2);
        if (j2 == 0) {
            return this;
        }
        c2453c.f6388b += j2;
        C1627p c1627p = this.f6387a;
        while (j >= ((long) (c1627p.f4408c - c1627p.f4407b))) {
            long j3 = j - ((long) (c1627p.f4408c - c1627p.f4407b));
            c1627p = c1627p.f4411f;
            j = j3;
        }
        while (j2 > 0) {
            C1627p c1627p2 = new C1627p(c1627p);
            c1627p2.f4407b = (int) (((long) c1627p2.f4407b) + j);
            c1627p2.f4408c = Math.min(c1627p2.f4407b + ((int) j2), c1627p2.f4408c);
            if (c2453c.f6387a == null) {
                c1627p2.f4412g = c1627p2;
                c1627p2.f4411f = c1627p2;
                c2453c.f6387a = c1627p2;
            } else {
                c2453c.f6387a.f4412g.m4855a(c1627p2);
            }
            long j4 = j2 - ((long) (c1627p2.f4408c - c1627p2.f4407b));
            c1627p = c1627p.f4411f;
            j = 0;
            j2 = j4;
        }
        return this;
    }

    public long m7748h() {
        long j = this.f6388b;
        if (j == 0) {
            return 0;
        }
        C1627p c1627p = this.f6387a.f4412g;
        if (c1627p.f4408c < 8192 && c1627p.f4410e) {
            j -= (long) (c1627p.f4408c - c1627p.f4407b);
        }
        return j;
    }

    public byte mo2529i() {
        if (this.f6388b == 0) {
            throw new IllegalStateException("size == 0");
        }
        C1627p c1627p = this.f6387a;
        int i = c1627p.f4407b;
        int i2 = c1627p.f4408c;
        int i3 = i + 1;
        byte b = c1627p.f4406a[i];
        this.f6388b--;
        if (i3 == i2) {
            this.f6387a = c1627p.m4853a();
            C1628q.m4859a(c1627p);
        } else {
            c1627p.f4407b = i3;
        }
        return b;
    }

    public byte m7731c(long j) {
        C1632v.m4869a(this.f6388b, j, 1);
        C1627p c1627p = this.f6387a;
        while (true) {
            long j2 = (long) (c1627p.f4408c - c1627p.f4407b);
            if (j < j2) {
                return c1627p.f4406a[c1627p.f4407b + ((int) j)];
            }
            long j3 = j - j2;
            c1627p = c1627p.f4411f;
            j = j3;
        }
    }

    public short mo2533j() {
        if (this.f6388b < 2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("size < 2: ");
            stringBuilder.append(this.f6388b);
            throw new IllegalStateException(stringBuilder.toString());
        }
        C1627p c1627p = this.f6387a;
        int i = c1627p.f4407b;
        int i2 = c1627p.f4408c;
        if (i2 - i < 2) {
            return (short) (((mo2529i() & 255) << 8) | (mo2529i() & 255));
        }
        byte[] bArr = c1627p.f4406a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        i = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
        this.f6388b -= 2;
        if (i4 == i2) {
            this.f6387a = c1627p.m4853a();
            C1628q.m4859a(c1627p);
        } else {
            c1627p.f4407b = i4;
        }
        return (short) i;
    }

    public int mo2534k() {
        if (this.f6388b < 4) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("size < 4: ");
            stringBuilder.append(this.f6388b);
            throw new IllegalStateException(stringBuilder.toString());
        }
        C1627p c1627p = this.f6387a;
        int i = c1627p.f4407b;
        int i2 = c1627p.f4408c;
        if (i2 - i < 4) {
            return ((((mo2529i() & 255) << 24) | ((mo2529i() & 255) << 16)) | ((mo2529i() & 255) << 8)) | (mo2529i() & 255);
        }
        byte[] bArr = c1627p.f4406a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        i = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
        i3 = i4 + 1;
        i |= (bArr[i4] & 255) << 8;
        i4 = i3 + 1;
        i |= bArr[i3] & 255;
        this.f6388b -= 4;
        if (i4 == i2) {
            this.f6387a = c1627p.m4853a();
            C1628q.m4859a(c1627p);
        } else {
            c1627p.f4407b = i4;
        }
        return i;
    }

    public long m7760l() {
        if (this.f6388b < 8) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("size < 8: ");
            stringBuilder.append(r0.f6388b);
            throw new IllegalStateException(stringBuilder.toString());
        }
        C1627p c1627p = r0.f6387a;
        int i = c1627p.f4407b;
        int i2 = c1627p.f4408c;
        if (i2 - i < 8) {
            return ((((long) mo2534k()) & 4294967295L) << 32) | (((long) mo2534k()) & 4294967295L);
        }
        byte[] bArr = c1627p.f4406a;
        int i3 = i + 1;
        i = i3 + 1;
        i3 = i + 1;
        i = i3 + 1;
        int i4 = i + 1;
        i = i4 + 1;
        i4 = i + 1;
        i = i4 + 1;
        long j = ((((((((((long) bArr[i]) & 255) << 56) | ((((long) bArr[i3]) & 255) << 48)) | ((((long) bArr[i]) & 255) << 40)) | ((((long) bArr[i3]) & 255) << 32)) | ((((long) bArr[i]) & 255) << 24)) | ((((long) bArr[i4]) & 255) << 16)) | ((((long) bArr[i]) & 255) << 8)) | (((long) bArr[i4]) & 255);
        r0.f6388b -= 8;
        if (i == i2) {
            r0.f6387a = c1627p.m4853a();
            C1628q.m4859a(c1627p);
        } else {
            c1627p.f4407b = i;
        }
        return j;
    }

    public short mo2536m() {
        return C1632v.m4868a(mo2533j());
    }

    public int mo2537n() {
        return C1632v.m4866a(mo2534k());
    }

    public long mo2539o() {
        return C1632v.m4867a(m7760l());
    }

    public long mo2541p() {
        long j = 0;
        if (this.f6388b == 0) {
            throw new IllegalStateException("size == 0");
        }
        int i = 0;
        long j2 = -7;
        int i2 = 0;
        int i3 = i2;
        do {
            C1627p c1627p = r0.f6387a;
            byte[] bArr = c1627p.f4406a;
            int i4 = c1627p.f4407b;
            int i5 = c1627p.f4408c;
            while (i4 < i5) {
                byte b = bArr[i4];
                if (b >= (byte) 48 && b <= (byte) 57) {
                    int i6 = 48 - b;
                    if (j >= -922337203685477580L) {
                        if (j != -922337203685477580L || ((long) i6) >= j2) {
                            j = (j * 10) + ((long) i6);
                        }
                    }
                    C2453c b2 = new C2453c().m7761l(j).m7725b((int) b);
                    if (i2 == 0) {
                        b2.mo2529i();
                    }
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Number too large: ");
                    stringBuilder.append(b2.m7772s());
                    throw new NumberFormatException(stringBuilder.toString());
                } else if (b != (byte) 45 || i != 0) {
                    if (i != 0) {
                        i3 = 1;
                        if (i4 != i5) {
                            r0.f6387a = c1627p.m4853a();
                            C1628q.m4859a(c1627p);
                        } else {
                            c1627p.f4407b = i4;
                        }
                        if (i3 == 0) {
                            break;
                        }
                    } else {
                        StringBuilder stringBuilder2 = new StringBuilder();
                        stringBuilder2.append("Expected leading [0-9] or '-' character but was 0x");
                        stringBuilder2.append(Integer.toHexString(b));
                        throw new NumberFormatException(stringBuilder2.toString());
                    }
                } else {
                    j2--;
                    i2 = 1;
                }
                i4++;
                i++;
            }
            if (i4 != i5) {
                c1627p.f4407b = i4;
            } else {
                r0.f6387a = c1627p.m4853a();
                C1628q.m4859a(c1627p);
            }
            if (i3 == 0) {
                break;
            }
        } while (r0.f6387a != null);
        r0.f6388b -= (long) i;
        return i2 != 0 ? j : -j;
    }

    public long mo2543q() {
        if (this.f6388b == 0) {
            throw new IllegalStateException("size == 0");
        }
        int i = 0;
        int i2 = 0;
        long j = 0;
        do {
            C1627p c1627p = r0.f6387a;
            byte[] bArr = c1627p.f4406a;
            int i3 = c1627p.f4407b;
            int i4 = c1627p.f4408c;
            while (i3 < i4) {
                int i5;
                int i6 = bArr[i3];
                if (i6 >= (byte) 48 && i6 <= (byte) 57) {
                    i5 = i6 - 48;
                } else if (i6 >= (byte) 97 && i6 <= (byte) 102) {
                    i5 = (i6 - 97) + 10;
                } else if (i6 < (byte) 65 || i6 > (byte) 70) {
                    if (i != 0) {
                        i2 = 1;
                        if (i3 != i4) {
                            r0.f6387a = c1627p.m4853a();
                            C1628q.m4859a(c1627p);
                        } else {
                            c1627p.f4407b = i3;
                        }
                        if (i2 == 0) {
                            break;
                        }
                    } else {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("Expected leading [0-9a-fA-F] character but was 0x");
                        stringBuilder.append(Integer.toHexString(i6));
                        throw new NumberFormatException(stringBuilder.toString());
                    }
                } else {
                    i5 = (i6 - 65) + 10;
                }
                if ((j & -1152921504606846976L) != 0) {
                    C2453c b = new C2453c().m7762m(j).m7725b(i6);
                    StringBuilder stringBuilder2 = new StringBuilder();
                    stringBuilder2.append("Number too large: ");
                    stringBuilder2.append(b.m7772s());
                    throw new NumberFormatException(stringBuilder2.toString());
                }
                i3++;
                i++;
                j = (j << 4) | ((long) i5);
            }
            if (i3 != i4) {
                c1627p.f4407b = i3;
            } else {
                r0.f6387a = c1627p.m4853a();
                C1628q.m4859a(c1627p);
            }
            if (i2 == 0) {
                break;
            }
        } while (r0.f6387a != null);
        r0.f6388b -= (long) i;
        return j;
    }

    public C1624f m7771r() {
        return new C1624f(mo2545v());
    }

    public C1624f mo2522d(long j) throws EOFException {
        return new C1624f(mo2528h(j));
    }

    public long mo2510a(C1629s c1629s) throws IOException {
        long j = this.f6388b;
        if (j > 0) {
            c1629s.mo1284a(this, j);
        }
        return j;
    }

    public String m7772s() {
        try {
            return m7717a(this.f6388b, C1632v.f4419a);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public String mo2524e(long j) throws EOFException {
        return m7717a(j, C1632v.f4419a);
    }

    public String mo2512a(Charset charset) {
        try {
            return m7717a(this.f6388b, charset);
        } catch (Charset charset2) {
            throw new AssertionError(charset2);
        }
    }

    public String m7717a(long j, Charset charset) throws EOFException {
        C1632v.m4869a(this.f6388b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("byteCount > Integer.MAX_VALUE: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        } else if (j == 0) {
            return "";
        } else {
            C1627p c1627p = this.f6387a;
            if (((long) c1627p.f4407b) + j > ((long) c1627p.f4408c)) {
                return new String(mo2528h(j), charset);
            }
            String str = new String(c1627p.f4406a, c1627p.f4407b, (int) j, charset);
            c1627p.f4407b = (int) (((long) c1627p.f4407b) + j);
            this.f6388b -= j;
            if (c1627p.f4407b == c1627p.f4408c) {
                this.f6387a = c1627p.m4853a();
                C1628q.m4859a(c1627p);
            }
            return str;
        }
    }

    public String mo2544t() throws EOFException {
        return m7743f(Long.MAX_VALUE);
    }

    public String m7743f(long j) throws EOFException {
        if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("limit < 0: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        }
        long j2 = Long.MAX_VALUE;
        if (j != Long.MAX_VALUE) {
            j2 = j + 1;
        }
        long a = m7707a((byte) 10, 0, j2);
        if (a != -1) {
            return m7747g(a);
        }
        if (j2 < m7705a() && m7731c(j2 - 1) == (byte) 13 && m7731c(j2) == (byte) 10) {
            return m7747g(j2);
        }
        C2453c c2453c = new C2453c();
        m7711a(c2453c, 0, Math.min(32, m7705a()));
        stringBuilder = new StringBuilder();
        stringBuilder.append("\\n not found: limit=");
        stringBuilder.append(Math.min(m7705a(), j));
        stringBuilder.append(" content=");
        stringBuilder.append(c2453c.m7771r().mo1909e());
        stringBuilder.append(8230);
        throw new EOFException(stringBuilder.toString());
    }

    String m7747g(long j) throws EOFException {
        if (j > 0) {
            long j2 = j - 1;
            if (m7731c(j2) == (byte) 13) {
                j = mo2524e(j2);
                mo2531i(2);
                return j;
            }
        }
        j = mo2524e(j);
        mo2531i(1);
        return j;
    }

    public int m7774u() throws EOFException {
        if (this.f6388b == 0) {
            throw new EOFException();
        }
        int i;
        int i2;
        byte c = m7731c(0);
        int i3 = 1;
        int i4;
        if ((c & 128) == 0) {
            i = c & 127;
            i4 = 0;
            i2 = 1;
        } else if ((c & 224) == 192) {
            i = c & 31;
            i2 = 2;
            i4 = 128;
        } else if ((c & 240) == 224) {
            i = c & 15;
            i2 = 3;
            i4 = 2048;
        } else if ((c & 248) == 240) {
            i = c & 7;
            i2 = 4;
            i4 = 65536;
        } else {
            mo2531i(1);
            return 65533;
        }
        long j = (long) i2;
        if (this.f6388b < j) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("size < ");
            stringBuilder.append(i2);
            stringBuilder.append(": ");
            stringBuilder.append(this.f6388b);
            stringBuilder.append(" (to read code point prefixed 0x");
            stringBuilder.append(Integer.toHexString(c));
            stringBuilder.append(")");
            throw new EOFException(stringBuilder.toString());
        }
        while (i3 < i2) {
            long j2 = (long) i3;
            c = m7731c(j2);
            if ((c & 192) == 128) {
                i = (i << 6) | (c & 63);
                i3++;
            } else {
                mo2531i(j2);
                return 65533;
            }
        }
        mo2531i(j);
        if (i > 1114111) {
            return 65533;
        }
        if ((i < 55296 || i > 57343) && i >= r6) {
            return i;
        }
        return 65533;
    }

    public byte[] mo2545v() {
        try {
            return mo2528h(this.f6388b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public byte[] mo2528h(long j) throws EOFException {
        C1632v.m4869a(this.f6388b, 0, j);
        if (j > 2147483647L) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("byteCount > Integer.MAX_VALUE: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        }
        byte[] bArr = new byte[((int) j)];
        mo2514a(bArr);
        return bArr;
    }

    public void mo2514a(byte[] bArr) throws EOFException {
        int i = 0;
        while (i < bArr.length) {
            int a = m7704a(bArr, i, bArr.length - i);
            if (a == -1) {
                throw new EOFException();
            }
            i += a;
        }
    }

    public int m7704a(byte[] bArr, int i, int i2) {
        C1632v.m4869a((long) bArr.length, (long) i, (long) i2);
        C1627p c1627p = this.f6387a;
        if (c1627p == null) {
            return -1;
        }
        i2 = Math.min(i2, c1627p.f4408c - c1627p.f4407b);
        System.arraycopy(c1627p.f4406a, c1627p.f4407b, bArr, i, i2);
        c1627p.f4407b += i2;
        this.f6388b -= (long) i2;
        if (c1627p.f4407b == c1627p.f4408c) {
            this.f6387a = c1627p.m4853a();
            C1628q.m4859a(c1627p);
        }
        return i2;
    }

    public void m7776w() {
        try {
            mo2531i(this.f6388b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public void mo2531i(long j) throws EOFException {
        while (j > 0) {
            if (this.f6387a == null) {
                throw new EOFException();
            }
            int min = (int) Math.min(j, (long) (this.f6387a.f4408c - this.f6387a.f4407b));
            long j2 = (long) min;
            this.f6388b -= j2;
            long j3 = j - j2;
            j = this.f6387a;
            j.f4407b += min;
            if (this.f6387a.f4407b == this.f6387a.f4408c) {
                j = this.f6387a;
                this.f6387a = j.m4853a();
                C1628q.m4859a(j);
            }
            j = j3;
        }
    }

    public C2453c m7712a(C1624f c1624f) {
        if (c1624f == null) {
            throw new IllegalArgumentException("byteString == null");
        }
        c1624f.mo1903a(this);
        return this;
    }

    public C2453c m7713a(String str) {
        return m7714a(str, 0, str.length());
    }

    public C2453c m7714a(String str, int i, int i2) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            i2 = new StringBuilder();
            i2.append("beginIndex < 0: ");
            i2.append(i);
            throw new IllegalArgumentException(i2.toString());
        } else if (i2 < i) {
            r0 = new StringBuilder();
            r0.append("endIndex < beginIndex: ");
            r0.append(i2);
            r0.append(" < ");
            r0.append(i);
            throw new IllegalArgumentException(r0.toString());
        } else if (i2 > str.length()) {
            r0 = new StringBuilder();
            r0.append("endIndex > string.length: ");
            r0.append(i2);
            r0.append(" > ");
            r0.append(str.length());
            throw new IllegalArgumentException(r0.toString());
        } else {
            while (i < i2) {
                char charAt = str.charAt(i);
                int i3;
                int min;
                if (charAt < '') {
                    C1627p f = m7742f(1);
                    byte[] bArr = f.f4406a;
                    i3 = f.f4408c - i;
                    min = Math.min(i2, 8192 - i3);
                    int i4 = i + 1;
                    bArr[i + i3] = (byte) charAt;
                    while (i4 < min) {
                        i = str.charAt(i4);
                        if (i >= 128) {
                            break;
                        }
                        int i5 = i4 + 1;
                        bArr[i4 + i3] = (byte) i;
                        i4 = i5;
                    }
                    i3 = (i3 + i4) - f.f4408c;
                    f.f4408c += i3;
                    this.f6388b += (long) i3;
                    i = i4;
                } else if (charAt < 'ࠀ') {
                    m7725b((charAt >> 6) | 192);
                    m7725b((charAt & 63) | 128);
                    i++;
                } else {
                    if (charAt >= '?') {
                        if (charAt <= '?') {
                            i3 = i + 1;
                            if (i3 < i2) {
                                min = str.charAt(i3);
                            } else {
                                min = 0;
                            }
                            if (charAt <= '?' && min >= 56320) {
                                if (min <= 57343) {
                                    int i6 = 65536 + (((charAt & -55297) << 10) | (-56321 & min));
                                    m7725b((i6 >> 18) | 240);
                                    m7725b(((i6 >> 12) & 63) | 128);
                                    m7725b(((i6 >> 6) & 63) | 128);
                                    m7725b((i6 & 63) | 128);
                                    i += 2;
                                }
                            }
                            m7725b(63);
                            i = i3;
                        }
                    }
                    m7725b((charAt >> 12) | 224);
                    m7725b(((charAt >> 6) & 63) | 128);
                    m7725b((charAt & 63) | 128);
                    i++;
                }
            }
            return this;
        }
    }

    public C2453c m7710a(int i) {
        if (i < 128) {
            m7725b(i);
        } else if (i < 2048) {
            m7725b((i >> 6) | 192);
            m7725b((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                m7725b((i >> 12) | 224);
                m7725b(((i >> 6) & 63) | 128);
                m7725b((i & 63) | 128);
            } else {
                m7725b(63);
            }
        } else if (i <= 1114111) {
            m7725b((i >> 18) | 240);
            m7725b(((i >> 12) & 63) | 128);
            m7725b(((i >> 6) & 63) | 128);
            m7725b((i & 63) | 128);
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected code point: ");
            stringBuilder.append(Integer.toHexString(i));
            throw new IllegalArgumentException(stringBuilder.toString());
        }
        return this;
    }

    public C2453c m7716a(String str, Charset charset) {
        return m7715a(str, 0, str.length(), charset);
    }

    public C2453c m7715a(String str, int i, int i2, Charset charset) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            i2 = new StringBuilder();
            i2.append("beginIndex < 0: ");
            i2.append(i);
            throw new IllegalAccessError(i2.toString());
        } else if (i2 < i) {
            charset = new StringBuilder();
            charset.append("endIndex < beginIndex: ");
            charset.append(i2);
            charset.append(" < ");
            charset.append(i);
            throw new IllegalArgumentException(charset.toString());
        } else if (i2 > str.length()) {
            charset = new StringBuilder();
            charset.append("endIndex > string.length: ");
            charset.append(i2);
            charset.append(" > ");
            charset.append(str.length());
            throw new IllegalArgumentException(charset.toString());
        } else if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (charset.equals(C1632v.f4419a)) {
            return m7714a(str, i, i2);
        } else {
            str = str.substring(i, i2).getBytes(charset);
            return m7727b(str, 0, str.length);
        }
    }

    public C2453c m7726b(byte[] bArr) {
        if (bArr != null) {
            return m7727b(bArr, 0, bArr.length);
        }
        throw new IllegalArgumentException("source == null");
    }

    public C2453c m7727b(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = (long) i2;
        C1632v.m4869a((long) bArr.length, (long) i, j);
        i2 += i;
        while (i < i2) {
            C1627p f = m7742f(1);
            int min = Math.min(i2 - i, 8192 - f.f4408c);
            System.arraycopy(bArr, i, f.f4406a, f.f4408c, min);
            i += min;
            f.f4408c += min;
        }
        this.f6388b += j;
        return this;
    }

    public long mo2511a(C1630t c1630t) throws IOException {
        if (c1630t == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long read = c1630t.read(this, PlaybackStateCompat.ACTION_PLAY_FROM_URI);
            if (read == -1) {
                return j;
            }
            j += read;
        }
    }

    public C2453c m7725b(int i) {
        C1627p f = m7742f(1);
        byte[] bArr = f.f4406a;
        int i2 = f.f4408c;
        f.f4408c = i2 + 1;
        bArr[i2] = (byte) i;
        this.f6388b++;
        return this;
    }

    public C2453c m7732c(int i) {
        C1627p f = m7742f(2);
        byte[] bArr = f.f4406a;
        int i2 = f.f4408c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        i2 = i3 + 1;
        bArr[i3] = (byte) (i & 255);
        f.f4408c = i2;
        this.f6388b += 2;
        return this;
    }

    public C2453c m7737d(int i) {
        C1627p f = m7742f(4);
        byte[] bArr = f.f4406a;
        int i2 = f.f4408c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        i2 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        i2 = i3 + 1;
        bArr[i3] = (byte) (i & 255);
        f.f4408c = i2;
        this.f6388b += 4;
        return this;
    }

    public C2453c m7739e(int i) {
        return m7737d(C1632v.m4866a(i));
    }

    public C2453c m7754j(long j) {
        C1627p f = m7742f(8);
        byte[] bArr = f.f4406a;
        int i = f.f4408c;
        int i2 = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 56) & 255));
        i = i2 + 1;
        bArr[i2] = (byte) ((int) ((j >>> 48) & 255));
        i2 = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 40) & 255));
        i = i2 + 1;
        bArr[i2] = (byte) ((int) ((j >>> 32) & 255));
        i2 = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 24) & 255));
        i = i2 + 1;
        bArr[i2] = (byte) ((int) ((j >>> 16) & 255));
        i2 = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 8) & 255));
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((int) (j & 255));
        f.f4408c = i3;
        this.f6388b += 8;
        return this;
    }

    public C2453c m7758k(long j) {
        return m7754j(C1632v.m4867a(j));
    }

    public C2453c m7761l(long j) {
        if (j == 0) {
            return m7725b((int) 48);
        }
        int i = 0;
        int i2 = 1;
        if (j < 0) {
            j = -j;
            if (j < 0) {
                return m7713a("-9223372036854775808");
            }
            i = 1;
        }
        if (j >= 100000000) {
            i2 = j < 1000000000000L ? j < 10000000000L ? j < 1000000000 ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
        } else if (j >= 10000) {
            i2 = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
        } else if (j >= 100) {
            i2 = j < 1000 ? 3 : 4;
        } else if (j >= 10) {
            i2 = 2;
        }
        if (i != 0) {
            i2++;
        }
        C1627p f = m7742f(i2);
        byte[] bArr = f.f4406a;
        int i3 = f.f4408c + i2;
        while (j != 0) {
            i3--;
            bArr[i3] = f6386c[(int) (j % 10)];
            j /= 10;
        }
        if (i != 0) {
            bArr[i3 - 1] = 45;
        }
        f.f4408c += i2;
        this.f6388b += (long) i2;
        return this;
    }

    public C2453c m7762m(long j) {
        if (j == 0) {
            return m7725b((int) 48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        C1627p f = m7742f(numberOfTrailingZeros);
        byte[] bArr = f.f4406a;
        int i = f.f4408c;
        for (int i2 = (f.f4408c + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = f6386c[(int) (j & 15)];
            j >>>= 4;
        }
        f.f4408c += numberOfTrailingZeros;
        this.f6388b += (long) numberOfTrailingZeros;
        return this;
    }

    C1627p m7742f(int i) {
        if (i >= 1) {
            if (i <= 8192) {
                C1627p c1627p;
                if (this.f6387a == null) {
                    this.f6387a = C1628q.m4858a();
                    i = this.f6387a;
                    C1627p c1627p2 = this.f6387a;
                    c1627p = this.f6387a;
                    c1627p2.f4412g = c1627p;
                    i.f4411f = c1627p;
                    return c1627p;
                }
                c1627p = this.f6387a.f4412g;
                if (c1627p.f4408c + i > 8192 || c1627p.f4410e == 0) {
                    c1627p = c1627p.m4855a(C1628q.m4858a());
                }
                return c1627p;
            }
        }
        throw new IllegalArgumentException();
    }

    public void mo1284a(C2453c c2453c, long j) {
        if (c2453c == null) {
            throw new IllegalArgumentException("source == null");
        } else if (c2453c == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            C1632v.m4869a(c2453c.f6388b, 0, j);
            while (j > 0) {
                C1627p c1627p;
                if (j < ((long) (c2453c.f6387a.f4408c - c2453c.f6387a.f4407b))) {
                    c1627p = this.f6387a != null ? this.f6387a.f4412g : null;
                    if (c1627p != null && c1627p.f4410e) {
                        int i;
                        long j2 = j + ((long) c1627p.f4408c);
                        if (c1627p.f4409d) {
                            i = 0;
                        } else {
                            i = c1627p.f4407b;
                        }
                        if (j2 - ((long) i) <= PlaybackStateCompat.ACTION_PLAY_FROM_URI) {
                            c2453c.f6387a.m4856a(c1627p, (int) j);
                            c2453c.f6388b -= j;
                            this.f6388b += j;
                            return;
                        }
                    }
                    c2453c.f6387a = c2453c.f6387a.m4854a((int) j);
                }
                c1627p = c2453c.f6387a;
                long j3 = (long) (c1627p.f4408c - c1627p.f4407b);
                c2453c.f6387a = c1627p.m4853a();
                if (this.f6387a == null) {
                    this.f6387a = c1627p;
                    c1627p = this.f6387a;
                    C1627p c1627p2 = this.f6387a;
                    C1627p c1627p3 = this.f6387a;
                    c1627p2.f4412g = c1627p3;
                    c1627p.f4411f = c1627p3;
                } else {
                    this.f6387a.f4412g.m4855a(c1627p).m4857b();
                }
                c2453c.f6388b -= j3;
                this.f6388b += j3;
                j -= j3;
            }
        }
    }

    public long read(C2453c c2453c, long j) {
        if (c2453c == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("byteCount < 0: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        } else if (this.f6388b == 0) {
            return -1;
        } else {
            if (j > this.f6388b) {
                j = this.f6388b;
            }
            c2453c.mo1284a(this, j);
            return j;
        }
    }

    public long mo2509a(byte b) {
        return m7707a(b, 0, Long.MAX_VALUE);
    }

    public long m7707a(byte b, long j, long j2) {
        C2453c c2453c = this;
        long j3 = 0;
        if (j >= 0) {
            if (j2 >= j) {
                long j4 = j2 > c2453c.f6388b ? c2453c.f6388b : j2;
                if (j == j4) {
                    return -1;
                }
                C1627p c1627p = c2453c.f6387a;
                if (c1627p == null) {
                    return -1;
                }
                if (c2453c.f6388b - j >= j) {
                    while (true) {
                        long j5 = j3 + ((long) (c1627p.f4408c - c1627p.f4407b));
                        if (j5 >= j) {
                            break;
                        }
                        c1627p = c1627p.f4411f;
                        j3 = j5;
                    }
                } else {
                    j3 = c2453c.f6388b;
                    while (j3 > j) {
                        c1627p = c1627p.f4412g;
                        j3 -= (long) (c1627p.f4408c - c1627p.f4407b);
                    }
                }
                long j6 = j;
                while (j3 < j4) {
                    byte[] bArr = c1627p.f4406a;
                    int min = (int) Math.min((long) c1627p.f4408c, (((long) c1627p.f4407b) + j4) - j3);
                    for (int i = (int) ((((long) c1627p.f4407b) + j6) - j3); i < min; i++) {
                        if (bArr[i] == b) {
                            return ((long) (i - c1627p.f4407b)) + j3;
                        }
                    }
                    byte b2 = b;
                    long j7 = j3 + ((long) (c1627p.f4408c - c1627p.f4407b));
                    c1627p = c1627p.f4411f;
                    j6 = j7;
                    j3 = j6;
                }
                return -1;
            }
        }
        throw new IllegalArgumentException(String.format("size=%s fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(c2453c.f6388b), Long.valueOf(j), Long.valueOf(j2)}));
    }

    public boolean mo2515a(long j, C1624f c1624f) {
        return m7723a(j, c1624f, 0, c1624f.mo1912g());
    }

    public boolean m7723a(long j, C1624f c1624f, int i, int i2) {
        if (j >= 0 && i >= 0 && i2 >= 0 && this.f6388b - j >= ((long) i2)) {
            if (c1624f.mo1912g() - i >= i2) {
                for (int i3 = 0; i3 < i2; i3++) {
                    if (m7731c(j + ((long) i3)) != c1624f.mo1900a(i + i3)) {
                        return false;
                    }
                }
                return 1;
            }
        }
        return false;
    }

    public C1631u timeout() {
        return C1631u.f4415c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2453c)) {
            return false;
        }
        C2453c c2453c = (C2453c) obj;
        if (this.f6388b != c2453c.f6388b) {
            return false;
        }
        long j = 0;
        if (this.f6388b == 0) {
            return true;
        }
        C1627p c1627p = this.f6387a;
        obj = c2453c.f6387a;
        int i = c1627p.f4407b;
        int i2 = obj.f4407b;
        while (j < this.f6388b) {
            long min = (long) Math.min(c1627p.f4408c - i, obj.f4408c - i2);
            int i3 = i2;
            i2 = i;
            i = 0;
            while (((long) i) < min) {
                int i4 = i2 + 1;
                int i5 = i3 + 1;
                if (c1627p.f4406a[i2] != obj.f4406a[i3]) {
                    return false;
                }
                i++;
                i2 = i4;
                i3 = i5;
            }
            if (i2 == c1627p.f4408c) {
                c1627p = c1627p.f4411f;
                i = c1627p.f4407b;
            } else {
                i = i2;
            }
            if (i3 == obj.f4408c) {
                obj = obj.f4411f;
                i2 = obj.f4407b;
            } else {
                i2 = i3;
            }
            j += min;
        }
        return true;
    }

    public int hashCode() {
        C1627p c1627p = this.f6387a;
        if (c1627p == null) {
            return 0;
        }
        int i = 1;
        do {
            for (int i2 = c1627p.f4407b; i2 < c1627p.f4408c; i2++) {
                i = c1627p.f4406a[i2] + (31 * i);
            }
            c1627p = c1627p.f4411f;
        } while (c1627p != this.f6387a);
        return i;
    }

    public String toString() {
        return m7778y().toString();
    }

    public C2453c m7777x() {
        C2453c c2453c = new C2453c();
        if (this.f6388b == 0) {
            return c2453c;
        }
        c2453c.f6387a = new C1627p(this.f6387a);
        C1627p c1627p = c2453c.f6387a;
        C1627p c1627p2 = c2453c.f6387a;
        C1627p c1627p3 = c2453c.f6387a;
        c1627p2.f4412g = c1627p3;
        c1627p.f4411f = c1627p3;
        c1627p = this.f6387a;
        while (true) {
            c1627p = c1627p.f4411f;
            if (c1627p != this.f6387a) {
                c2453c.f6387a.f4412g.m4855a(new C1627p(c1627p));
            } else {
                c2453c.f6388b = this.f6388b;
                return c2453c;
            }
        }
    }

    public C1624f m7778y() {
        if (this.f6388b <= 2147483647L) {
            return m7745g((int) this.f6388b);
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("size > Integer.MAX_VALUE: ");
        stringBuilder.append(this.f6388b);
        throw new IllegalArgumentException(stringBuilder.toString());
    }

    public C1624f m7745g(int i) {
        if (i == 0) {
            return C1624f.f4400b;
        }
        return new C2293r(this, i);
    }
}
