package p125d;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/* compiled from: RealBufferedSource */
final class C2456o implements C2284e {
    public final C2453c f6393a = new C2453c();
    public final C1630t f6394b;
    boolean f6395c;

    /* compiled from: RealBufferedSource */
    class C16261 extends InputStream {
        final /* synthetic */ C2456o f4405a;

        C16261(C2456o c2456o) {
            this.f4405a = c2456o;
        }

        public int read() throws IOException {
            if (this.f4405a.f6395c) {
                throw new IOException("closed");
            } else if (this.f4405a.f6393a.f6388b == 0 && this.f4405a.f6394b.read(this.f4405a.f6393a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                return -1;
            } else {
                return this.f4405a.f6393a.mo2529i() & 255;
            }
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            if (this.f4405a.f6395c) {
                throw new IOException("closed");
            }
            C1632v.m4869a((long) bArr.length, (long) i, (long) i2);
            if (this.f4405a.f6393a.f6388b == 0 && this.f4405a.f6394b.read(this.f4405a.f6393a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                return -1;
            }
            return this.f4405a.f6393a.m7704a(bArr, i, i2);
        }

        public int available() throws IOException {
            if (!this.f4405a.f6395c) {
                return (int) Math.min(this.f4405a.f6393a.f6388b, 2147483647L);
            }
            throw new IOException("closed");
        }

        public void close() throws IOException {
            this.f4405a.close();
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(this.f4405a);
            stringBuilder.append(".inputStream()");
            return stringBuilder.toString();
        }
    }

    C2456o(C1630t c1630t) {
        if (c1630t == null) {
            throw new NullPointerException("source == null");
        }
        this.f6394b = c1630t;
    }

    public C2453c mo2516b() {
        return this.f6393a;
    }

    public long read(C2453c c2453c, long j) throws IOException {
        if (c2453c == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("byteCount < 0: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        } else if (this.f6395c) {
            throw new IllegalStateException("closed");
        } else if (this.f6393a.f6388b == 0 && this.f6394b.read(this.f6393a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
            return -1;
        } else {
            return this.f6393a.read(c2453c, Math.min(j, this.f6393a.f6388b));
        }
    }

    public boolean mo2525f() throws IOException {
        if (!this.f6395c) {
            return this.f6393a.mo2525f() && this.f6394b.read(this.f6393a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1;
        } else {
            throw new IllegalStateException("closed");
        }
    }

    public void mo2513a(long j) throws IOException {
        if (mo2519b(j) == null) {
            throw new EOFException();
        }
    }

    public boolean mo2519b(long j) throws IOException {
        if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("byteCount < 0: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        } else if (this.f6395c) {
            throw new IllegalStateException("closed");
        } else {
            while (this.f6393a.f6388b < j) {
                if (this.f6394b.read(this.f6393a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return 0;
                }
            }
            return 1;
        }
    }

    public byte mo2529i() throws IOException {
        mo2513a(1);
        return this.f6393a.mo2529i();
    }

    public C1624f mo2522d(long j) throws IOException {
        mo2513a(j);
        return this.f6393a.mo2522d(j);
    }

    public byte[] mo2545v() throws IOException {
        this.f6393a.mo2511a(this.f6394b);
        return this.f6393a.mo2545v();
    }

    public byte[] mo2528h(long j) throws IOException {
        mo2513a(j);
        return this.f6393a.mo2528h(j);
    }

    public void mo2514a(byte[] bArr) throws IOException {
        try {
            mo2513a((long) bArr.length);
            this.f6393a.mo2514a(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (this.f6393a.f6388b > 0) {
                int a = this.f6393a.m7704a(bArr, i, (int) this.f6393a.f6388b);
                if (a == -1) {
                    throw new AssertionError();
                }
                i += a;
            }
            throw e;
        }
    }

    public long mo2510a(C1629s c1629s) throws IOException {
        if (c1629s == null) {
            throw new IllegalArgumentException("sink == null");
        }
        long j = 0;
        while (this.f6394b.read(this.f6393a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) != -1) {
            long h = this.f6393a.m7748h();
            if (h > 0) {
                long j2 = j + h;
                c1629s.mo1284a(this.f6393a, h);
                j = j2;
            }
        }
        if (this.f6393a.m7705a() <= 0) {
            return j;
        }
        h = j + this.f6393a.m7705a();
        c1629s.mo1284a(this.f6393a, this.f6393a.m7705a());
        return h;
    }

    public String mo2524e(long j) throws IOException {
        mo2513a(j);
        return this.f6393a.mo2524e(j);
    }

    public String mo2512a(Charset charset) throws IOException {
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        }
        this.f6393a.mo2511a(this.f6394b);
        return this.f6393a.mo2512a(charset);
    }

    public String mo2544t() throws IOException {
        return m7808c(Long.MAX_VALUE);
    }

    public String m7808c(long j) throws IOException {
        if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("limit < 0: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        }
        long j2 = j == Long.MAX_VALUE ? Long.MAX_VALUE : j + 1;
        long a = m7799a((byte) 10, 0, j2);
        if (a != -1) {
            return this.f6393a.m7747g(a);
        }
        if (j2 < Long.MAX_VALUE && mo2519b(j2) && this.f6393a.m7731c(j2 - 1) == (byte) 13 && mo2519b(j2 + 1) && this.f6393a.m7731c(j2) == (byte) 10) {
            return this.f6393a.m7747g(j2);
        }
        C2453c c2453c = new C2453c();
        this.f6393a.m7711a(c2453c, 0, Math.min(32, this.f6393a.m7705a()));
        stringBuilder = new StringBuilder();
        stringBuilder.append("\\n not found: limit=");
        stringBuilder.append(Math.min(this.f6393a.m7705a(), j));
        stringBuilder.append(" content=");
        stringBuilder.append(c2453c.m7771r().mo1909e());
        stringBuilder.append(8230);
        throw new EOFException(stringBuilder.toString());
    }

    public short mo2533j() throws IOException {
        mo2513a(2);
        return this.f6393a.mo2533j();
    }

    public short mo2536m() throws IOException {
        mo2513a(2);
        return this.f6393a.mo2536m();
    }

    public int mo2534k() throws IOException {
        mo2513a(4);
        return this.f6393a.mo2534k();
    }

    public int mo2537n() throws IOException {
        mo2513a(4);
        return this.f6393a.mo2537n();
    }

    public long mo2539o() throws IOException {
        mo2513a(8);
        return this.f6393a.mo2539o();
    }

    public long mo2541p() throws IOException {
        mo2513a(1);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (!mo2519b((long) i2)) {
                break;
            }
            byte c = this.f6393a.m7731c((long) i);
            if (c < (byte) 48 || c > (byte) 57) {
                if (i != 0) {
                    break;
                } else if (c != (byte) 45) {
                    break;
                }
            }
            i = i2;
        }
        if (i == 0) {
            throw new NumberFormatException(String.format("Expected leading [0-9] or '-' character but was %#x", new Object[]{Byte.valueOf(c)}));
        }
        return this.f6393a.mo2541p();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long mo2543q() throws java.io.IOException {
        /*
        r6 = this;
        r0 = 1;
        r6.mo2513a(r0);
        r0 = 0;
        r1 = r0;
    L_0x0007:
        r2 = r1 + 1;
        r3 = (long) r2;
        r3 = r6.mo2519b(r3);
        if (r3 == 0) goto L_0x0049;
    L_0x0010:
        r3 = r6.f6393a;
        r4 = (long) r1;
        r3 = r3.m7731c(r4);
        r4 = 48;
        if (r3 < r4) goto L_0x001f;
    L_0x001b:
        r4 = 57;
        if (r3 <= r4) goto L_0x0030;
    L_0x001f:
        r4 = 97;
        if (r3 < r4) goto L_0x0027;
    L_0x0023:
        r4 = 102; // 0x66 float:1.43E-43 double:5.04E-322;
        if (r3 <= r4) goto L_0x0030;
    L_0x0027:
        r4 = 65;
        if (r3 < r4) goto L_0x0032;
    L_0x002b:
        r4 = 70;
        if (r3 <= r4) goto L_0x0030;
    L_0x002f:
        goto L_0x0032;
    L_0x0030:
        r1 = r2;
        goto L_0x0007;
    L_0x0032:
        if (r1 != 0) goto L_0x0049;
    L_0x0034:
        r1 = new java.lang.NumberFormatException;
        r2 = "Expected leading [0-9a-fA-F] character but was %#x";
        r4 = 1;
        r4 = new java.lang.Object[r4];
        r3 = java.lang.Byte.valueOf(r3);
        r4[r0] = r3;
        r0 = java.lang.String.format(r2, r4);
        r1.<init>(r0);
        throw r1;
    L_0x0049:
        r0 = r6.f6393a;
        r0 = r0.mo2543q();
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: d.o.q():long");
    }

    public void mo2531i(long j) throws IOException {
        if (this.f6395c) {
            throw new IllegalStateException("closed");
        }
        while (j > 0) {
            if (this.f6393a.f6388b == 0 && this.f6394b.read(this.f6393a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                throw new EOFException();
            }
            long min = Math.min(j, this.f6393a.m7705a());
            this.f6393a.mo2531i(min);
            j -= min;
        }
    }

    public long mo2509a(byte b) throws IOException {
        return m7799a(b, 0, Long.MAX_VALUE);
    }

    public long m7799a(byte b, long j, long j2) throws IOException {
        if (this.f6395c) {
            throw new IllegalStateException("closed");
        }
        if (j >= 0) {
            if (j2 >= j) {
                while (j < j2) {
                    long a = this.f6393a.m7707a(b, j, j2);
                    if (a != -1) {
                        return a;
                    }
                    a = this.f6393a.f6388b;
                    if (a < j2) {
                        if (this.f6394b.read(this.f6393a, PlaybackStateCompat.ACTION_PLAY_FROM_URI) != -1) {
                            j = Math.max(j, a);
                        }
                    }
                    return -1;
                }
                return -1;
            }
        }
        throw new IllegalArgumentException(String.format("fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(j), Long.valueOf(j2)}));
    }

    public boolean mo2515a(long j, C1624f c1624f) throws IOException {
        return m7805a(j, c1624f, 0, c1624f.mo1912g());
    }

    public boolean m7805a(long j, C1624f c1624f, int i, int i2) throws IOException {
        if (this.f6395c) {
            throw new IllegalStateException("closed");
        }
        if (j >= 0 && i >= 0 && i2 >= 0) {
            if (c1624f.mo1912g() - i >= i2) {
                int i3 = 0;
                while (i3 < i2) {
                    long j2 = j + ((long) i3);
                    if (!mo2519b(j2 + 1) || this.f6393a.m7731c(j2) != c1624f.mo1900a(i + i3)) {
                        return false;
                    }
                    i3++;
                }
                return 1;
            }
        }
        return false;
    }

    public InputStream mo2526g() {
        return new C16261(this);
    }

    public void close() throws IOException {
        if (!this.f6395c) {
            this.f6395c = true;
            this.f6394b.close();
            this.f6393a.m7776w();
        }
    }

    public C1631u timeout() {
        return this.f6394b.timeout();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("buffer(");
        stringBuilder.append(this.f6394b);
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
