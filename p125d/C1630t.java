package p125d;

import java.io.Closeable;
import java.io.IOException;

/* compiled from: Source */
public interface C1630t extends Closeable {
    void close() throws IOException;

    long read(C2453c c2453c, long j) throws IOException;

    C1631u timeout();
}
