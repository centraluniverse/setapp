package p125d;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/* compiled from: BufferedSource */
public interface C2284e extends C1630t {
    long mo2509a(byte b) throws IOException;

    long mo2510a(C1629s c1629s) throws IOException;

    String mo2512a(Charset charset) throws IOException;

    void mo2513a(long j) throws IOException;

    void mo2514a(byte[] bArr) throws IOException;

    boolean mo2515a(long j, C1624f c1624f) throws IOException;

    C2453c mo2516b();

    boolean mo2519b(long j) throws IOException;

    C1624f mo2522d(long j) throws IOException;

    String mo2524e(long j) throws IOException;

    boolean mo2525f() throws IOException;

    InputStream mo2526g();

    byte[] mo2528h(long j) throws IOException;

    byte mo2529i() throws IOException;

    void mo2531i(long j) throws IOException;

    short mo2533j() throws IOException;

    int mo2534k() throws IOException;

    short mo2536m() throws IOException;

    int mo2537n() throws IOException;

    long mo2539o() throws IOException;

    long mo2541p() throws IOException;

    long mo2543q() throws IOException;

    String mo2544t() throws IOException;

    byte[] mo2545v() throws IOException;
}
