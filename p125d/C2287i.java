package p125d;

import java.io.IOException;

/* compiled from: ForwardingSource */
public abstract class C2287i implements C1630t {
    private final C1630t delegate;

    public C2287i(C1630t c1630t) {
        if (c1630t == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.delegate = c1630t;
    }

    public final C1630t delegate() {
        return this.delegate;
    }

    public long read(C2453c c2453c, long j) throws IOException {
        return this.delegate.read(c2453c, j);
    }

    public C1631u timeout() {
        return this.delegate.timeout();
    }

    public void close() throws IOException {
        this.delegate.close();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getClass().getSimpleName());
        stringBuilder.append("(");
        stringBuilder.append(this.delegate.toString());
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
