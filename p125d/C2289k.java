package p125d;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

/* compiled from: GzipSource */
public final class C2289k implements C1630t {
    private int f5758a = 0;
    private final C2284e f5759b;
    private final Inflater f5760c;
    private final C2290l f5761d;
    private final CRC32 f5762e = new CRC32();

    public C2289k(C1630t c1630t) {
        if (c1630t == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f5760c = new Inflater(true);
        this.f5759b = C1625m.m4841a(c1630t);
        this.f5761d = new C2290l(this.f5759b, this.f5760c);
    }

    public long read(C2453c c2453c, long j) throws IOException {
        if (j < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("byteCount < 0: ");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
        } else if (j == 0) {
            return 0;
        } else {
            if (this.f5758a == 0) {
                m6953a();
                this.f5758a = 1;
            }
            if (this.f5758a == 1) {
                long j2 = c2453c.f6388b;
                j = this.f5761d.read(c2453c, j);
                if (j != -1) {
                    m6954a(c2453c, j2, j);
                    return j;
                }
                this.f5758a = 2;
            }
            if (this.f5758a == 2) {
                m6956b();
                this.f5758a = 3;
                if (this.f5759b.mo2525f() == null) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    private void m6953a() throws IOException {
        this.f5759b.mo2513a(10);
        byte c = this.f5759b.mo2516b().m7731c(3);
        int i = ((c >> 1) & 1) == 1 ? 1 : (byte) 0;
        if (i != 0) {
            m6954a(r6.f5759b.mo2516b(), 0, 10);
        }
        m6955a("ID1ID2", 8075, r6.f5759b.mo2533j());
        r6.f5759b.mo2531i(8);
        if (((c >> 2) & 1) == 1) {
            r6.f5759b.mo2513a(2);
            if (i != 0) {
                m6954a(r6.f5759b.mo2516b(), 0, 2);
            }
            long m = (long) r6.f5759b.mo2516b().mo2536m();
            r6.f5759b.mo2513a(m);
            if (i != 0) {
                m6954a(r6.f5759b.mo2516b(), 0, m);
            }
            r6.f5759b.mo2531i(m);
        }
        if (((c >> 3) & 1) == 1) {
            long a = r6.f5759b.mo2509a((byte) 0);
            if (a == -1) {
                throw new EOFException();
            }
            if (i != 0) {
                m6954a(r6.f5759b.mo2516b(), 0, a + 1);
            }
            r6.f5759b.mo2531i(a + 1);
        }
        if (((c >> 4) & 1) == 1) {
            long a2 = r6.f5759b.mo2509a((byte) 0);
            if (a2 == -1) {
                throw new EOFException();
            }
            if (i != 0) {
                m6954a(r6.f5759b.mo2516b(), 0, a2 + 1);
            }
            r6.f5759b.mo2531i(a2 + 1);
        }
        if (i != 0) {
            m6955a("FHCRC", r6.f5759b.mo2536m(), (short) ((int) r6.f5762e.getValue()));
            r6.f5762e.reset();
        }
    }

    private void m6956b() throws IOException {
        m6955a("CRC", this.f5759b.mo2537n(), (int) this.f5762e.getValue());
        m6955a("ISIZE", this.f5759b.mo2537n(), (int) this.f5760c.getBytesWritten());
    }

    public C1631u timeout() {
        return this.f5759b.timeout();
    }

    public void close() throws IOException {
        this.f5761d.close();
    }

    private void m6954a(C2453c c2453c, long j, long j2) {
        c2453c = c2453c.f6387a;
        while (j >= ((long) (c2453c.f4408c - c2453c.f4407b))) {
            long j3 = j - ((long) (c2453c.f4408c - c2453c.f4407b));
            c2453c = c2453c.f4411f;
            j = j3;
        }
        while (j2 > 0) {
            j = (int) (((long) c2453c.f4407b) + j);
            int min = (int) Math.min((long) (c2453c.f4408c - j), j2);
            this.f5762e.update(c2453c.f4406a, j, min);
            j3 = j2 - ((long) min);
            c2453c = c2453c.f4411f;
            j = 0;
            j2 = j3;
        }
    }

    private void m6955a(String str, int i, int i2) throws IOException {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}));
        }
    }
}
