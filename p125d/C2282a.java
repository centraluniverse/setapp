package p125d;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

/* compiled from: AsyncTimeout */
public class C2282a extends C1631u {
    private static final long f5747a = TimeUnit.SECONDS.toMillis(60);
    @Nullable
    static C2282a f5748b;
    private static final long f5749d = TimeUnit.MILLISECONDS.toNanos(f5747a);
    private boolean f5750e;
    @Nullable
    private C2282a f5751f;
    private long f5752g;

    /* compiled from: AsyncTimeout */
    private static final class C1620a extends Thread {
        C1620a() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        public void run() {
            /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:75)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
            /*
            r3 = this;
        L_0x0000:
            r0 = p125d.C2282a.class;	 Catch:{ InterruptedException -> 0x0000 }
            monitor-enter(r0);	 Catch:{ InterruptedException -> 0x0000 }
            r1 = p125d.C2282a.m6895e();	 Catch:{ all -> 0x0019 }
            if (r1 != 0) goto L_0x000b;	 Catch:{ all -> 0x0019 }
        L_0x0009:
            monitor-exit(r0);	 Catch:{ all -> 0x0019 }
            goto L_0x0000;	 Catch:{ all -> 0x0019 }
        L_0x000b:
            r2 = p125d.C2282a.f5748b;	 Catch:{ all -> 0x0019 }
            if (r1 != r2) goto L_0x0014;	 Catch:{ all -> 0x0019 }
        L_0x000f:
            r1 = 0;	 Catch:{ all -> 0x0019 }
            p125d.C2282a.f5748b = r1;	 Catch:{ all -> 0x0019 }
            monitor-exit(r0);	 Catch:{ all -> 0x0019 }
            return;	 Catch:{ all -> 0x0019 }
        L_0x0014:
            monitor-exit(r0);	 Catch:{ all -> 0x0019 }
            r1.mo2448a();	 Catch:{ InterruptedException -> 0x0000 }
            goto L_0x0000;
        L_0x0019:
            r1 = move-exception;
            monitor-exit(r0);	 Catch:{ all -> 0x0019 }
            throw r1;	 Catch:{ InterruptedException -> 0x0000 }
            */
            throw new UnsupportedOperationException("Method not decompiled: d.a.a.run():void");
        }
    }

    protected void mo2448a() {
    }

    public final void m6902c() {
        if (this.f5750e) {
            throw new IllegalStateException("Unbalanced enter/exit");
        }
        long i_ = i_();
        boolean j_ = j_();
        if (i_ != 0 || j_) {
            this.f5750e = true;
            C2282a.m6892a(this, i_, j_);
        }
    }

    private static synchronized void m6892a(C2282a c2282a, long j, boolean z) {
        synchronized (C2282a.class) {
            if (f5748b == null) {
                f5748b = new C2282a();
                new C1620a().start();
            }
            long nanoTime = System.nanoTime();
            if (j != 0 && z) {
                c2282a.f5752g = nanoTime + Math.min(j, c2282a.mo1894d() - nanoTime);
            } else if (j != 0) {
                c2282a.f5752g = nanoTime + j;
            } else if (z) {
                c2282a.f5752g = c2282a.mo1894d();
            } else {
                throw new AssertionError();
            }
            j = c2282a.m6894b(nanoTime);
            z = f5748b;
            while (z.f5751f != null) {
                if (j < z.f5751f.m6894b(nanoTime)) {
                    break;
                }
                z = z.f5751f;
            }
            c2282a.f5751f = z.f5751f;
            z.f5751f = c2282a;
            if (z == f5748b) {
                C2282a.class.notify();
            }
        }
    }

    public final boolean h_() {
        if (!this.f5750e) {
            return false;
        }
        this.f5750e = false;
        return C2282a.m6893a(this);
    }

    private static synchronized boolean m6893a(C2282a c2282a) {
        synchronized (C2282a.class) {
            for (C2282a c2282a2 = f5748b; c2282a2 != null; c2282a2 = c2282a2.f5751f) {
                if (c2282a2.f5751f == c2282a) {
                    c2282a2.f5751f = c2282a.f5751f;
                    c2282a.f5751f = null;
                    return null;
                }
            }
            return true;
        }
    }

    private long m6894b(long j) {
        return this.f5752g - j;
    }

    public final C1629s m6896a(final C1629s c1629s) {
        return new C1629s(this) {
            final /* synthetic */ C2282a f5744b;

            public void mo1284a(C2453c c2453c, long j) throws IOException {
                C1632v.m4869a(c2453c.f6388b, 0, j);
                while (true) {
                    long j2 = 0;
                    if (j > 0) {
                        C1627p c1627p = c2453c.f6387a;
                        while (j2 < PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                            long j3 = j2 + ((long) (c2453c.f6387a.f4408c - c2453c.f6387a.f4407b));
                            if (j3 >= j) {
                                j2 = j;
                                break;
                            } else {
                                c1627p = c1627p.f4411f;
                                j2 = j3;
                            }
                        }
                        this.f5744b.m6902c();
                        try {
                            c1629s.mo1284a(c2453c, j2);
                            long j4 = j - j2;
                            this.f5744b.m6900a((boolean) 1);
                            j = j4;
                        } catch (IOException e) {
                            throw this.f5744b.m6901b(e);
                        } catch (Throwable th) {
                            this.f5744b.m6900a(false);
                        }
                    } else {
                        return;
                    }
                }
            }

            public void flush() throws IOException {
                this.f5744b.m6902c();
                try {
                    c1629s.flush();
                    this.f5744b.m6900a(true);
                } catch (IOException e) {
                    throw this.f5744b.m6901b(e);
                } catch (Throwable th) {
                    this.f5744b.m6900a(false);
                }
            }

            public void close() throws IOException {
                this.f5744b.m6902c();
                try {
                    c1629s.close();
                    this.f5744b.m6900a(true);
                } catch (IOException e) {
                    throw this.f5744b.m6901b(e);
                } catch (Throwable th) {
                    this.f5744b.m6900a(false);
                }
            }

            public C1631u timeout() {
                return this.f5744b;
            }

            public String toString() {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("AsyncTimeout.sink(");
                stringBuilder.append(c1629s);
                stringBuilder.append(")");
                return stringBuilder.toString();
            }
        };
    }

    public final C1630t m6897a(final C1630t c1630t) {
        return new C1630t(this) {
            final /* synthetic */ C2282a f5746b;

            public long read(C2453c c2453c, long j) throws IOException {
                this.f5746b.m6902c();
                try {
                    c2453c = c1630t.read(c2453c, j);
                    this.f5746b.m6900a(true);
                    return c2453c;
                } catch (IOException e) {
                    throw this.f5746b.m6901b(e);
                } catch (Throwable th) {
                    this.f5746b.m6900a(false);
                }
            }

            public void close() throws IOException {
                try {
                    c1630t.close();
                    this.f5746b.m6900a(true);
                } catch (IOException e) {
                    throw this.f5746b.m6901b(e);
                } catch (Throwable th) {
                    this.f5746b.m6900a(false);
                }
            }

            public C1631u timeout() {
                return this.f5746b;
            }

            public String toString() {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("AsyncTimeout.source(");
                stringBuilder.append(c1630t);
                stringBuilder.append(")");
                return stringBuilder.toString();
            }
        };
    }

    final void m6900a(boolean z) throws IOException {
        if (h_() && z) {
            throw mo2447a((IOException) false);
        }
    }

    final IOException m6901b(IOException iOException) throws IOException {
        if (h_()) {
            return mo2447a(iOException);
        }
        return iOException;
    }

    protected IOException mo2447a(@Nullable IOException iOException) {
        IOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @Nullable
    static C2282a m6895e() throws InterruptedException {
        C2282a c2282a = f5748b.f5751f;
        C2282a c2282a2 = null;
        if (c2282a == null) {
            long nanoTime = System.nanoTime();
            C2282a.class.wait(f5747a);
            if (f5748b.f5751f == null && System.nanoTime() - nanoTime >= f5749d) {
                c2282a2 = f5748b;
            }
            return c2282a2;
        }
        nanoTime = c2282a.m6894b(System.nanoTime());
        if (nanoTime > 0) {
            long j = nanoTime / 1000000;
            C2282a.class.wait(j, (int) (nanoTime - (1000000 * j)));
            return null;
        }
        f5748b.f5751f = c2282a.f5751f;
        c2282a.f5751f = null;
        return c2282a;
    }
}
