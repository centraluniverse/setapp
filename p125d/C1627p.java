package p125d;

import javax.annotation.Nullable;

/* compiled from: Segment */
final class C1627p {
    final byte[] f4406a;
    int f4407b;
    int f4408c;
    boolean f4409d;
    boolean f4410e;
    C1627p f4411f;
    C1627p f4412g;

    C1627p() {
        this.f4406a = new byte[8192];
        this.f4410e = true;
        this.f4409d = false;
    }

    C1627p(C1627p c1627p) {
        this(c1627p.f4406a, c1627p.f4407b, c1627p.f4408c);
        c1627p.f4409d = true;
    }

    C1627p(byte[] bArr, int i, int i2) {
        this.f4406a = bArr;
        this.f4407b = i;
        this.f4408c = i2;
        this.f4410e = null;
        this.f4409d = 1;
    }

    @Nullable
    public C1627p m4853a() {
        C1627p c1627p = this.f4411f != this ? this.f4411f : null;
        this.f4412g.f4411f = this.f4411f;
        this.f4411f.f4412g = this.f4412g;
        this.f4411f = null;
        this.f4412g = null;
        return c1627p;
    }

    public C1627p m4855a(C1627p c1627p) {
        c1627p.f4412g = this;
        c1627p.f4411f = this.f4411f;
        this.f4411f.f4412g = c1627p;
        this.f4411f = c1627p;
        return c1627p;
    }

    public C1627p m4854a(int i) {
        if (i > 0) {
            if (i <= this.f4408c - this.f4407b) {
                C1627p c1627p;
                if (i >= 1024) {
                    c1627p = new C1627p(this);
                } else {
                    c1627p = C1628q.m4858a();
                    System.arraycopy(this.f4406a, this.f4407b, c1627p.f4406a, 0, i);
                }
                c1627p.f4408c = c1627p.f4407b + i;
                this.f4407b += i;
                this.f4412g.m4855a(c1627p);
                return c1627p;
            }
        }
        throw new IllegalArgumentException();
    }

    public void m4857b() {
        if (this.f4412g == this) {
            throw new IllegalStateException();
        } else if (this.f4412g.f4410e) {
            int i = this.f4408c - this.f4407b;
            if (i <= (8192 - this.f4412g.f4408c) + (this.f4412g.f4409d ? 0 : this.f4412g.f4407b)) {
                m4856a(this.f4412g, i);
                m4853a();
                C1628q.m4859a(this);
            }
        }
    }

    public void m4856a(C1627p c1627p, int i) {
        if (c1627p.f4410e) {
            if (c1627p.f4408c + i > 8192) {
                if (c1627p.f4409d) {
                    throw new IllegalArgumentException();
                } else if ((c1627p.f4408c + i) - c1627p.f4407b > 8192) {
                    throw new IllegalArgumentException();
                } else {
                    System.arraycopy(c1627p.f4406a, c1627p.f4407b, c1627p.f4406a, 0, c1627p.f4408c - c1627p.f4407b);
                    c1627p.f4408c -= c1627p.f4407b;
                    c1627p.f4407b = 0;
                }
            }
            System.arraycopy(this.f4406a, this.f4407b, c1627p.f4406a, c1627p.f4408c, i);
            c1627p.f4408c += i;
            this.f4407b += i;
            return;
        }
        throw new IllegalArgumentException();
    }
}
