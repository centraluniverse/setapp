package p125d;

import java.util.Arrays;

/* compiled from: SegmentedByteString */
final class C2293r extends C1624f {
    final transient byte[][] f5771f;
    final transient int[] f5772g;

    C2293r(C2453c c2453c, int i) {
        super(null);
        C1632v.m4869a(c2453c.f6388b, 0, (long) i);
        int i2 = 0;
        C1627p c1627p = c2453c.f6387a;
        int i3 = 0;
        int i4 = i3;
        while (i3 < i) {
            if (c1627p.f4408c == c1627p.f4407b) {
                throw new AssertionError("s.limit == s.pos");
            }
            i3 += c1627p.f4408c - c1627p.f4407b;
            i4++;
            c1627p = c1627p.f4411f;
        }
        this.f5771f = new byte[i4][];
        this.f5772g = new int[(i4 * 2)];
        C1627p c1627p2 = c2453c.f6387a;
        c2453c = null;
        while (i2 < i) {
            this.f5771f[c2453c] = c1627p2.f4406a;
            i2 += c1627p2.f4408c - c1627p2.f4407b;
            if (i2 > i) {
                i2 = i;
            }
            this.f5772g[c2453c] = i2;
            this.f5772g[this.f5771f.length + c2453c] = c1627p2.f4407b;
            c1627p2.f4409d = true;
            c2453c++;
            c1627p2 = c1627p2.f4411f;
        }
    }

    public String mo1902a() {
        return m6961i().mo1902a();
    }

    public String mo1906b() {
        return m6961i().mo1906b();
    }

    public String mo1909e() {
        return m6961i().mo1909e();
    }

    public C1624f mo1911f() {
        return m6961i().mo1911f();
    }

    public C1624f mo1907c() {
        return m6961i().mo1907c();
    }

    public C1624f mo1908d() {
        return m6961i().mo1908d();
    }

    public C1624f mo1901a(int i, int i2) {
        return m6961i().mo1901a(i, i2);
    }

    public byte mo1900a(int i) {
        int i2;
        C1632v.m4869a((long) this.f5772g[this.f5771f.length - 1], (long) i, 1);
        int b = m6960b(i);
        if (b == 0) {
            i2 = 0;
        } else {
            i2 = this.f5772g[b - 1];
        }
        return this.f5771f[b][(i - i2) + this.f5772g[this.f5771f.length + b]];
    }

    private int m6960b(int i) {
        i = Arrays.binarySearch(this.f5772g, 0, this.f5771f.length, i + 1);
        return i >= 0 ? i : i ^ -1;
    }

    public int mo1912g() {
        return this.f5772g[this.f5771f.length - 1];
    }

    public byte[] mo1913h() {
        int i = 0;
        Object obj = new byte[this.f5772g[this.f5771f.length - 1]];
        int length = this.f5771f.length;
        int i2 = 0;
        while (i < length) {
            int i3 = this.f5772g[length + i];
            int i4 = this.f5772g[i];
            System.arraycopy(this.f5771f[i], i3, obj, i2, i4 - i2);
            i++;
            i2 = i4;
        }
        return obj;
    }

    void mo1903a(C2453c c2453c) {
        int i = 0;
        int length = this.f5771f.length;
        int i2 = 0;
        while (i < length) {
            int i3 = this.f5772g[length + i];
            int i4 = this.f5772g[i];
            C1627p c1627p = new C1627p(this.f5771f[i], i3, (i3 + i4) - i2);
            if (c2453c.f6387a == null) {
                c1627p.f4412g = c1627p;
                c1627p.f4411f = c1627p;
                c2453c.f6387a = c1627p;
            } else {
                c2453c.f6387a.f4412g.m4855a(c1627p);
            }
            i++;
            i2 = i4;
        }
        c2453c.f6388b += (long) i2;
    }

    public boolean mo1904a(int i, C1624f c1624f, int i2, int i3) {
        if (i >= 0) {
            if (i <= mo1912g() - i3) {
                int b = m6960b(i);
                while (i3 > 0) {
                    int i4;
                    if (b == 0) {
                        i4 = 0;
                    } else {
                        i4 = this.f5772g[b - 1];
                    }
                    int min = Math.min(i3, ((this.f5772g[b] - i4) + i4) - i);
                    if (!c1624f.mo1905a(i2, this.f5771f[b], (i - i4) + this.f5772g[this.f5771f.length + b], min)) {
                        return false;
                    }
                    i += min;
                    i2 += min;
                    i3 -= min;
                    b++;
                }
                return true;
            }
        }
        return false;
    }

    public boolean mo1905a(int i, byte[] bArr, int i2, int i3) {
        if (i >= 0 && i <= mo1912g() - i3 && i2 >= 0) {
            if (i2 <= bArr.length - i3) {
                int b = m6960b(i);
                while (i3 > 0) {
                    int i4;
                    if (b == 0) {
                        i4 = 0;
                    } else {
                        i4 = this.f5772g[b - 1];
                    }
                    int min = Math.min(i3, ((this.f5772g[b] - i4) + i4) - i);
                    if (!C1632v.m4871a(this.f5771f[b], (i - i4) + this.f5772g[this.f5771f.length + b], bArr, i2, min)) {
                        return false;
                    }
                    i += min;
                    i2 += min;
                    i3 -= min;
                    b++;
                }
                return true;
            }
        }
        return false;
    }

    private C1624f m6961i() {
        return new C1624f(mo1913h());
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (obj instanceof C1624f) {
            C1624f c1624f = (C1624f) obj;
            if (c1624f.mo1912g() == mo1912g() && mo1904a(0, c1624f, 0, mo1912g()) != null) {
                return z;
            }
        }
        z = false;
        return z;
    }

    public int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        i = this.f5771f.length;
        int i3 = 1;
        int i4 = 0;
        while (i2 < i) {
            byte[] bArr = this.f5771f[i2];
            int i5 = this.f5772g[i + i2];
            int i6 = this.f5772g[i2];
            i4 = (i6 - i4) + i5;
            while (i5 < i4) {
                i3 = bArr[i5] + (31 * i3);
                i5++;
            }
            i2++;
            i4 = i6;
        }
        this.d = i3;
        return i3;
    }

    public String toString() {
        return m6961i().toString();
    }
}
