package p125d;

import java.io.IOException;
import java.util.zip.Deflater;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

/* compiled from: DeflaterSink */
public final class C2285g implements C1629s {
    private final C2283d f5753a;
    private final Deflater f5754b;
    private boolean f5755c;

    public C2285g(C1629s c1629s, Deflater deflater) {
        this(C1625m.m4840a(c1629s), deflater);
    }

    C2285g(C2283d c2283d, Deflater deflater) {
        if (c2283d == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f5753a = c2283d;
            this.f5754b = deflater;
        }
    }

    public void mo1284a(C2453c c2453c, long j) throws IOException {
        C1632v.m4869a(c2453c.f6388b, 0, j);
        while (j > 0) {
            C1627p c1627p = c2453c.f6387a;
            int min = (int) Math.min(j, (long) (c1627p.f4408c - c1627p.f4407b));
            this.f5754b.setInput(c1627p.f4406a, c1627p.f4407b, min);
            m6942a(false);
            long j2 = (long) min;
            c2453c.f6388b -= j2;
            c1627p.f4407b += min;
            if (c1627p.f4407b == c1627p.f4408c) {
                c2453c.f6387a = c1627p.m4853a();
                C1628q.m4859a(c1627p);
            }
            j -= j2;
        }
    }

    @IgnoreJRERequirement
    private void m6942a(boolean z) throws IOException {
        C2453c b = this.f5753a.mo2516b();
        while (true) {
            int deflate;
            C1627p f = b.m7742f(1);
            if (z) {
                deflate = this.f5754b.deflate(f.f4406a, f.f4408c, 8192 - f.f4408c, 2);
            } else {
                deflate = this.f5754b.deflate(f.f4406a, f.f4408c, 8192 - f.f4408c);
            }
            if (deflate > 0) {
                f.f4408c += deflate;
                b.f6388b += (long) deflate;
                this.f5753a.mo2546z();
            } else if (this.f5754b.needsInput()) {
                break;
            }
        }
        if (f.f4407b == f.f4408c) {
            b.f6387a = f.m4853a();
            C1628q.m4859a(f);
        }
    }

    public void flush() throws IOException {
        m6942a(true);
        this.f5753a.flush();
    }

    void m6943a() throws IOException {
        this.f5754b.finish();
        m6942a(false);
    }

    public void close() throws IOException {
        if (!this.f5755c) {
            Throwable th = null;
            try {
                m6943a();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f5754b.end();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            try {
                this.f5753a.close();
            } catch (Throwable th32) {
                if (th == null) {
                    th = th32;
                }
            }
            this.f5755c = true;
            if (th != null) {
                C1632v.m4870a(th);
            }
        }
    }

    public C1631u timeout() {
        return this.f5753a.timeout();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DeflaterSink(");
        stringBuilder.append(this.f5753a);
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
