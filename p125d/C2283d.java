package p125d;

import java.io.IOException;

/* compiled from: BufferedSink */
public interface C2283d extends C1629s {
    long mo2511a(C1630t c1630t) throws IOException;

    C2453c mo2516b();

    C2283d mo2517b(C1624f c1624f) throws IOException;

    C2283d mo2518b(String str) throws IOException;

    C2283d mo2520c(byte[] bArr) throws IOException;

    C2283d mo2521c(byte[] bArr, int i, int i2) throws IOException;

    C2283d mo2523e() throws IOException;

    void flush() throws IOException;

    C2283d mo2527h(int i) throws IOException;

    C2283d mo2530i(int i) throws IOException;

    C2283d mo2532j(int i) throws IOException;

    C2283d mo2535k(int i) throws IOException;

    C2283d mo2538n(long j) throws IOException;

    C2283d mo2540o(long j) throws IOException;

    C2283d mo2542p(long j) throws IOException;

    C2283d mo2546z() throws IOException;
}
