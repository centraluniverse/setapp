package p125d;

import java.io.IOException;

/* compiled from: ForwardingSink */
public abstract class C2286h implements C1629s {
    private final C1629s f5756a;

    public C2286h(C1629s c1629s) {
        if (c1629s == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f5756a = c1629s;
    }

    public void mo1284a(C2453c c2453c, long j) throws IOException {
        this.f5756a.mo1284a(c2453c, j);
    }

    public void flush() throws IOException {
        this.f5756a.flush();
    }

    public C1631u timeout() {
        return this.f5756a.timeout();
    }

    public void close() throws IOException {
        this.f5756a.close();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getClass().getSimpleName());
        stringBuilder.append("(");
        stringBuilder.append(this.f5756a.toString());
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
