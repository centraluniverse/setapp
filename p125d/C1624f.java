package p125d;

import java.io.Serializable;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.annotation.Nullable;

/* compiled from: ByteString */
public class C1624f implements Serializable, Comparable<C1624f> {
    static final char[] f4399a = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    public static final C1624f f4400b = C1624f.m4821a(new byte[0]);
    final byte[] f4401c;
    transient int f4402d;
    transient String f4403e;

    public /* synthetic */ int compareTo(Object obj) {
        return m4832b((C1624f) obj);
    }

    C1624f(byte[] bArr) {
        this.f4401c = bArr;
    }

    public static C1624f m4821a(byte... bArr) {
        if (bArr != null) {
            return new C1624f((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    public static C1624f m4820a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("s == null");
        }
        C1624f c1624f = new C1624f(str.getBytes(C1632v.f4419a));
        c1624f.f4403e = str;
        return c1624f;
    }

    public String mo1902a() {
        String str = this.f4403e;
        if (str != null) {
            return str;
        }
        str = new String(this.f4401c, C1632v.f4419a);
        this.f4403e = str;
        return str;
    }

    public String mo1906b() {
        return C1621b.m4815a(this.f4401c);
    }

    public C1624f mo1907c() {
        return m4824d("SHA-1");
    }

    public C1624f mo1908d() {
        return m4824d("SHA-256");
    }

    private C1624f m4824d(String str) {
        try {
            return C1624f.m4821a(MessageDigest.getInstance(str).digest(this.f4401c));
        } catch (String str2) {
            throw new AssertionError(str2);
        }
    }

    @Nullable
    public static C1624f m4822b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("base64 == null");
        }
        str = C1621b.m4817a(str);
        return str != null ? new C1624f(str) : null;
    }

    public String mo1909e() {
        int i = 0;
        char[] cArr = new char[(this.f4401c.length * 2)];
        byte[] bArr = this.f4401c;
        int length = bArr.length;
        int i2 = 0;
        while (i < length) {
            byte b = bArr[i];
            int i3 = i2 + 1;
            cArr[i2] = f4399a[(b >> 4) & 15];
            i2 = i3 + 1;
            cArr[i3] = f4399a[b & 15];
            i++;
        }
        return new String(cArr);
    }

    public static C1624f m4823c(String str) {
        if (str == null) {
            throw new IllegalArgumentException("hex == null");
        } else if (str.length() % 2 != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected hex string: ");
            stringBuilder.append(str);
            throw new IllegalArgumentException(stringBuilder.toString());
        } else {
            byte[] bArr = new byte[(str.length() / 2)];
            for (int i = 0; i < bArr.length; i++) {
                int i2 = i * 2;
                bArr[i] = (byte) ((C1624f.m4818a(str.charAt(i2)) << 4) + C1624f.m4818a(str.charAt(i2 + 1)));
            }
            return C1624f.m4821a(bArr);
        }
    }

    private static int m4818a(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'a' && c <= 'f') {
            return (c - 'a') + 10;
        }
        if (c >= 'A' && c <= 'F') {
            return (c - 'A') + 10;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected hex digit: ");
        stringBuilder.append(c);
        throw new IllegalArgumentException(stringBuilder.toString());
    }

    public C1624f mo1911f() {
        for (int i = 0; i < this.f4401c.length; i++) {
            byte b = this.f4401c[i];
            if (b >= (byte) 65) {
                if (b <= (byte) 90) {
                    byte[] bArr = (byte[]) this.f4401c.clone();
                    bArr[i] = (byte) (b + 32);
                    for (int i2 = i + 1; i2 < bArr.length; i2++) {
                        byte b2 = bArr[i2];
                        if (b2 >= (byte) 65) {
                            if (b2 <= (byte) 90) {
                                bArr[i2] = (byte) (b2 + 32);
                            }
                        }
                    }
                    return new C1624f(bArr);
                }
            }
        }
        return this;
    }

    public C1624f mo1901a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("beginIndex < 0");
        } else if (i2 > this.f4401c.length) {
            i2 = new StringBuilder();
            i2.append("endIndex > length(");
            i2.append(this.f4401c.length);
            i2.append(")");
            throw new IllegalArgumentException(i2.toString());
        } else {
            int i3 = i2 - i;
            if (i3 < 0) {
                throw new IllegalArgumentException("endIndex < beginIndex");
            } else if (i == 0 && i2 == this.f4401c.length) {
                return this;
            } else {
                i2 = new byte[i3];
                System.arraycopy(this.f4401c, i, i2, 0, i3);
                return new C1624f(i2);
            }
        }
    }

    public byte mo1900a(int i) {
        return this.f4401c[i];
    }

    public int mo1912g() {
        return this.f4401c.length;
    }

    public byte[] mo1913h() {
        return (byte[]) this.f4401c.clone();
    }

    void mo1903a(C2453c c2453c) {
        c2453c.m7727b(this.f4401c, 0, this.f4401c.length);
    }

    public boolean mo1904a(int i, C1624f c1624f, int i2, int i3) {
        return c1624f.mo1905a(i2, this.f4401c, i, i3);
    }

    public boolean mo1905a(int i, byte[] bArr, int i2, int i3) {
        return i >= 0 && i <= this.f4401c.length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && C1632v.m4871a(this.f4401c, i, bArr, i2, i3) != 0;
    }

    public final boolean m4831a(C1624f c1624f) {
        return mo1904a(0, c1624f, 0, c1624f.mo1912g());
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (obj instanceof C1624f) {
            C1624f c1624f = (C1624f) obj;
            if (c1624f.mo1912g() == this.f4401c.length && c1624f.mo1905a(0, this.f4401c, 0, this.f4401c.length) != null) {
                return z;
            }
        }
        z = false;
        return z;
    }

    public int hashCode() {
        int i = this.f4402d;
        if (i != 0) {
            return i;
        }
        i = Arrays.hashCode(this.f4401c);
        this.f4402d = i;
        return i;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int m4832b(p125d.C1624f r10) {
        /*
        r9 = this;
        r0 = r9.mo1912g();
        r1 = r10.mo1912g();
        r2 = java.lang.Math.min(r0, r1);
        r3 = 0;
        r4 = r3;
    L_0x000e:
        r5 = -1;
        r6 = 1;
        if (r4 >= r2) goto L_0x0028;
    L_0x0012:
        r7 = r9.mo1900a(r4);
        r7 = r7 & 255;
        r8 = r10.mo1900a(r4);
        r8 = r8 & 255;
        if (r7 != r8) goto L_0x0023;
    L_0x0020:
        r4 = r4 + 1;
        goto L_0x000e;
    L_0x0023:
        if (r7 >= r8) goto L_0x0026;
    L_0x0025:
        goto L_0x0027;
    L_0x0026:
        r5 = r6;
    L_0x0027:
        return r5;
    L_0x0028:
        if (r0 != r1) goto L_0x002b;
    L_0x002a:
        return r3;
    L_0x002b:
        if (r0 >= r1) goto L_0x002e;
    L_0x002d:
        goto L_0x002f;
    L_0x002e:
        r5 = r6;
    L_0x002f:
        return r5;
        */
        throw new UnsupportedOperationException("Method not decompiled: d.f.b(d.f):int");
    }

    public String toString() {
        if (this.f4401c.length == 0) {
            return "[size=0]";
        }
        String a = mo1902a();
        int a2 = C1624f.m4819a(a, 64);
        StringBuilder stringBuilder;
        if (a2 == -1) {
            if (this.f4401c.length <= 64) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("[hex=");
                stringBuilder.append(mo1909e());
                stringBuilder.append("]");
                a = stringBuilder.toString();
            } else {
                stringBuilder = new StringBuilder();
                stringBuilder.append("[size=");
                stringBuilder.append(this.f4401c.length);
                stringBuilder.append(" hex=");
                stringBuilder.append(mo1901a(0, 64).mo1909e());
                stringBuilder.append("…]");
                a = stringBuilder.toString();
            }
            return a;
        }
        String replace = a.substring(0, a2).replace("\\", "\\\\").replace("\n", "\\n").replace("\r", "\\r");
        if (a2 < a.length()) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("[size=");
            stringBuilder.append(this.f4401c.length);
            stringBuilder.append(" text=");
            stringBuilder.append(replace);
            stringBuilder.append("…]");
            a = stringBuilder.toString();
        } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("[text=");
            stringBuilder.append(replace);
            stringBuilder.append("]");
            a = stringBuilder.toString();
        }
        return a;
    }

    static int m4819a(String str, int i) {
        int length = str.length();
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            if (i3 == i) {
                return i2;
            }
            int codePointAt = str.codePointAt(i2);
            if ((Character.isISOControl(codePointAt) && codePointAt != 10 && codePointAt != 13) || codePointAt == 65533) {
                return -1;
            }
            i3++;
            i2 += Character.charCount(codePointAt);
        }
        return str.length();
    }
}
