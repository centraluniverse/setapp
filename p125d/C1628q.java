package p125d;

import android.support.v4.media.session.PlaybackStateCompat;
import javax.annotation.Nullable;

/* compiled from: SegmentPool */
final class C1628q {
    @Nullable
    static C1627p f4413a;
    static long f4414b;

    private C1628q() {
    }

    static C1627p m4858a() {
        synchronized (C1628q.class) {
            if (f4413a != null) {
                C1627p c1627p = f4413a;
                f4413a = c1627p.f4411f;
                c1627p.f4411f = null;
                f4414b -= PlaybackStateCompat.ACTION_PLAY_FROM_URI;
                return c1627p;
            }
            return new C1627p();
        }
    }

    static void m4859a(C1627p c1627p) {
        if (c1627p.f4411f == null) {
            if (c1627p.f4412g == null) {
                if (!c1627p.f4409d) {
                    synchronized (C1628q.class) {
                        if (f4414b + PlaybackStateCompat.ACTION_PLAY_FROM_URI > PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                            return;
                        }
                        f4414b += PlaybackStateCompat.ACTION_PLAY_FROM_URI;
                        c1627p.f4411f = f4413a;
                        c1627p.f4408c = 0;
                        c1627p.f4407b = 0;
                        f4413a = c1627p;
                        return;
                    }
                }
                return;
            }
        }
        throw new IllegalArgumentException();
    }
}
