package p125d;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

/* compiled from: Sink */
public interface C1629s extends Closeable, Flushable {
    void mo1284a(C2453c c2453c, long j) throws IOException;

    void close() throws IOException;

    void flush() throws IOException;

    C1631u timeout();
}
